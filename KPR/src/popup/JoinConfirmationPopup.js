/**
 * Created by stpl on 1/9/2017.
 */
var JoinConfirmationPopup = Popup.extend({
    _selectedRadioBtn: null,
    roomId: null,
    question: null,
    entryFeeArr: null,
    ctor: function (room, context, overlay, popupType) {
        this.entryFeeArr = room.DisplayEntryFee;

        if (this.entryFeeArr[1] == "ChipAndTicket")
            this._super(new cc.Size(510 * scaleFactor, 271 * scaleFactor), context, overlay);
        else this._super(new cc.Size(509 * scaleFactor, 206 * scaleFactor), context, overlay);

        this.question = new cc.LabelTTF("Are you sure you want to join this tournament ?", "RobotoRegular", 16 * 2 * scaleFactor);
        this.question.setScale(0.5);
        this.question.setColor(cc.color(0, 0, 0));
        this.question.setPosition(cc.p(this.width / 2, 95 * scaleFactor));
        var tourName = room.getVariable("tournamentName").value;
        var startTimeValue = room.StartTimeInUIFormat;
        this.roomId = room.id;

        this.headerLabel.setString(tourName);
        this.closeButton = new cc.Sprite(spriteFrameCache.getSpriteFrame("ResultClose.png"));
        this.closeButton.setName("CloseButton");
        this.closeButton.setPosition(this.width, this.height);
        this.addChild(this.closeButton, 2);

        this.noButton = new CreateBtn("noBtn", "noBtnOver");
        this.noButton.setName("NoButton");
        this.noButton.setPosition(this.width / 2 + 50 * scaleFactor, 44 * scaleFactor);
        this.addChild(this.noButton);

        if (popupType == "quitConfrmPopup") {
            this.yesButton = new CreateBtn("QuitBar", "Quit_over");
        } else {
            this.yesButton = new CreateBtn("yesBtn", "yesBtnOver");
        }
        this.yesButton.setName("YesButton");
        this.yesButton.setPosition(this.width / 2 - 50 * scaleFactor, 44 * scaleFactor);
        this.addChild(this.yesButton);

        var startTime = new cc.LabelTTF("Start Time:", "RobotoRegular", 15 * 2 * scaleFactor);
        startTime.setScale(0.5);
        startTime.setAnchorPoint(0, 0.5);
        startTime.setColor(cc.color(0, 0, 0));

        var startTimeText = new cc.LabelTTF(startTimeValue, "RobotoRegular", 15 * 2 * scaleFactor);
        startTimeText.setScale(0.5);
        startTimeText.setAnchorPoint(0, 0.5);
        startTimeText.setColor(cc.color(120, 136, 1)); //if else

        var entryFee = new cc.LabelTTF("Buy-In:", "RobotoRegular", 15 * 2 * scaleFactor);
        entryFee.setScale(0.5);
        entryFee.setAnchorPoint(0, 0.5);
        entryFee.setColor(cc.color(0, 0, 0));
        this.addChild(entryFee);

        var entryFeeText = new cc.LabelTTF(this.entryFeeArr[0], "RupeeFordian", 15 * 2 * scaleFactor);
        entryFeeText.setScale(0.5);
        entryFeeText.setAnchorPoint(0, 0.5);
        this.addChild(entryFeeText);

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        if (this.entryFeeArr[1] != "ChipAndTicket") {
            this.question.setPositionY(87 * scaleFactor);
            entryFee.setPosition(21 * scaleFactor, this.height - 64 * scaleFactor);
            entryFeeText.setPosition(100 * scaleFactor, 141 * scaleFactor);
            entryFeeText.setColor(cc.color(120, 136, 1));

            this.horizontalLine = new cc.DrawNode();
            this.horizontalLine.drawSegment(cc.p(26 * scaleFactor, 111 * scaleFactor), cc.p(490 * scaleFactor, 111 * scaleFactor), 0.1, cc.color(220, 220, 220));
            this.addChild(this.horizontalLine);

            var verticalLine = new cc.DrawNode();
            verticalLine.drawSegment(cc.p(203 * scaleFactor, 151 * scaleFactor), cc.p(203 * scaleFactor, 129 * scaleFactor), 0.1, cc.color(220, 220, 220));
            this.addChild(verticalLine);

            startTime.setPosition(266 * scaleFactor, this.height - 64 * scaleFactor);
            startTimeText.setPosition(350 * scaleFactor, 142 * scaleFactor);
        } else {
            entryFeeText.setColor(cc.color(0, 0, 0));
            this.radioOne = new RadioButton(this, "RadioOne");
            this.radioOne.setScale(0.98);
            this.radioOne.setSelected(true);
            this.radioOne.setPosition(cc.p(190 * scaleFactor, 147 * scaleFactor));
            this.addChild(this.radioOne);
            this._selectedRadioBtn = this.radioOne;

            this.radioTwo = new RadioButton(this, "RadioTwo");
            this.radioTwo.setScale(0.98);
            this.radioTwo.setSelected(false);
            this.radioTwo.setPosition(cc.p(339 * scaleFactor, 147 * scaleFactor));
            this.addChild(this.radioTwo);

            var circle = new cc.Sprite(spriteFrameCache.getSpriteFrame("whiteRound.png"));
            circle.setColor(cc.color(59, 95, 116));
            circle.setPosition(cc.p(275 * scaleFactor, 145 * scaleFactor));
            this.addChild(circle);

            var orText = new cc.LabelTTF("OR", "RobotoRegular", 12 * 2 * scaleFactor);
            orText.setScale(0.5);
            orText.setColor(cc.color(255, 255, 255));
            orText.setAnchorPoint(0.5, 0.5);
            orText.setPosition(circle.width / 2, circle.height / 2);
            orText.setScale(0.5);
            circle.addChild(orText, 1);

            var ticket = new cc.LabelTTF("Ticket", "RobotoRegular", 15 * 2 * scaleFactor);
            ticket.setScale(0.5);
            ticket.setColor(cc.color(0, 0, 0));
            ticket.setAnchorPoint(0, 0.5);
            ticket.setPosition(355 * scaleFactor, 146 * scaleFactor);
            this.addChild(ticket);

            var line1 = new cc.DrawNode();
            line1.drawSegment(cc.p(25 * scaleFactor, 173 * scaleFactor), cc.p(486 * scaleFactor, 173 * scaleFactor), 0.5, cc.color(210, 210, 210));
            this.addChild(line1);

            var line2 = new cc.DrawNode();
            line2.drawSegment(cc.p(25 * scaleFactor, 118 * scaleFactor), cc.p(486 * scaleFactor, 118 * scaleFactor), 0.5, cc.color(210, 210, 210));
            this.addChild(line2);

            this.question.setPositionY(86 * scaleFactor);
            entryFee.setPosition(cc.p(100 * scaleFactor, 146 * scaleFactor));
            startTime.setPosition(144 * scaleFactor, 205 * scaleFactor);
            startTimeText.setPosition(227 * scaleFactor, 206 * scaleFactor);
            entryFeeText.setPosition(210 * scaleFactor, 145 * scaleFactor);
            cc.eventManager.addListener(this.popupTouchListener.clone(), this.radioOne);
            cc.eventManager.addListener(this.popupTouchListener.clone(), this.radioTwo);
        }

        this.addChild(startTime);
        this.addChild(startTimeText);
        this.addChild(this.question);


        cc.eventManager.addListener(this.popupTouchListener.clone(), this.closeButton);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.yesButton);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.noButton);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.yesButton);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.noButton);
        return true;

    },
    radioBtnListener: function (sender) {
        this.radioOne.setSelected(!this.radioOne._isRadioSelected);
        this.radioTwo.setSelected(!this.radioTwo._isRadioSelected);

        if (this.radioOne._isRadioSelected)
            this._selectedRadioBtn = this.radioOne;
        else this._selectedRadioBtn = this.radioTwo;
    },
    yesBtnHandler: function () {
        var name = this._selectedRadioBtn && this._selectedRadioBtn._name;
        var entryFeeType;
        if (this.entryFeeArr[1] == "ChipAndTicket") {
            if (name == "RadioOne")
                entryFeeType = "Chip";
            else if (name == "RadioTwo")
                entryFeeType = "Ticket";
        } else entryFeeType = this.entryFeeArr[1];

        // TODO check by mycashBalance
        var dataObj = {};
        dataObj["feeType"] = entryFeeType;
        sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.REGISTRAION_REQ, dataObj, this.roomId);
        SoundManager.removeSound(SoundManager.extratimesound);

        cc.log("Tournament join req", dataObj);
        /*if ( Main.getInstance()._myCashBalance >= Number(_entryFee)){
         dataObj.putUtfString("feeType", feeType);
         sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.REGISTRAION_REQ, dataObj, tRoomId);
         }else{
         if(_entryFee==-1)
         getInsufficientTicketPopUp();
         else
         getAddCashPopUp();
         }*/

    },
    noBtnHandler: function () {
        this.removeFromParent(true);
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        switch (target._name) {
            case "CloseButton":
                this.removeFromParent(true);
                break;
            case "YesButton":
                this.yesBtnHandler();
                break;
            case "NoButton":
                this.noBtnHandler();
                break;
            case "RadioOne":
                target.click();
                break;
            case "RadioTwo":
                target.click();
                break;
        }
    }
});
var RadioButton = cc.Sprite.extend({
    _isRadioSelected: null,
    joinPopup: null,
    _hovering: null,
    ctor: function (context, radioBtnName) {
        this._super(spriteFrameCache.getSpriteFrame("RadioGrey.png"));
        this.joinPopup = context;
        this._isRadioSelected = false;
        this.setName(radioBtnName);
        this.setAnchorPoint(0.5, 0.5);
        this._hovering = false;
    },
    hover: function () {
    },
    setSelected: function (bool) {
        if (bool) {
            this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("RadioGreen.png"));
            this._isRadioSelected = true;
        } else {
            this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("RadioGrey.png"));
            this._isRadioSelected = false;
        }
    },
    click: function () {
        if (!this._isRadioSelected)
            this.joinPopup.radioBtnListener(this);
    }
});
// Below popup used in eligibility and cancel tournament also
var TourCommonPopup = Popup.extend({
    _name: "TourCommonPopup",
    ctor: function (msg, headerLabel, size, context, overlay, flag, isAddCash) {
        this._super(size, context, overlay);

        this.headerLabel.setString(headerLabel);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        msg == null ? msg = "" : msg;
        this.message = new cc.LabelTTF(msg, "RobotoRegular", 15 * 2 * scaleFactor, cc.TEXT_ALIGNMENT_CENTER);
        this.message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.message.setScale(0.5);
        this.message._setBoundingWidth(this.width * 2 - 30 * scaleFactor);
        this.message.setColor(cc.color(25, 25, 25));
        this.message.setPosition(cc.p(this.width / 2, this.height / 2 + 10 * scaleFactor));
        this.addChild(this.message, 2);

        var closeButton = new cc.Sprite(spriteFrameCache.getSpriteFrame("ResultClose.png"));
        closeButton.setName("CloseButton");
        closeButton.setPosition(this.width, this.height);
        this.addChild(closeButton, 4, 11);

        this.okBtn = new CreateBtn("OkBtn", "OkBtnOver", "OkButton");
        this.okBtn.nonFlashPopup = flag || null;
        this.okBtn.setPosition(cc.p(this.width / 2, 40 * scaleFactor));
        this.addChild(this.okBtn, 4, 10);

        if (isAddCash) {
            this.addCashBtn = new CreateBtn("addCash", "addCashOver", "AddCashButton");
            this.addCashBtn.setPosition(cc.p(this.width / 2 - 50, 40 * scaleFactor));
            this.okBtn.setPosition(cc.p(this.width / 2 + 50, 40 * scaleFactor));
            this.addChild(this.addCashBtn, 4, 12);
        }

        cc.eventManager.addListener(this.popupTouchListener.clone(), this.okBtn);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.okBtn);
        cc.eventManager.addListener(this.popupTouchListener.clone(), closeButton);

        this.addCashBtn && cc.eventManager.addListener(this.popupMouseListener.clone(), this.addCashBtn);
        this.addCashBtn && cc.eventManager.addListener(this.popupTouchListener.clone(), this.addCashBtn);
        return true;
    },
    openAppLink: function () {
        cc.sys.openURL("https://www.khelplayrummy.com/mobile-rummy-app?src=game-lobby");
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        switch (target.tag) {
            case 10:
                if (target.nonFlashPopup)
                    this.openAppLink();
                this.removeFromParent(true);
                break;
            case 11:
                this.removeFromParent(true);
                break;
            case 12:
                window.parent.postMessage("openCashPopup", "*");
                this.removeFromParent(true);
        }
    }
});
var WaitConfirmationPopup = TourCommonPopup.extend({
    room: null,
    ctor: function (room, waitNum, context, overlay) {
        var tourName = room.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value;
        this._super(null, tourName, new cc.Size(576 * scaleFactor, 208 * scaleFactor), context, overlay);
        this.room = room;

        var message = "You have joined the waitlist for this tournament. Your waitlist position: ";
        this.message.setString(message);
        this.message.setFontName("Robotoregular");
        this.message.setPosition(this.width / 2, 141 * scaleFactor);

        var numValue = new cc.LabelTTF(waitNum, "RobotoBold", 15 * 2 * scaleFactor);
        numValue.setScale(0.5);
        numValue.setColor(cc.color(0, 0, 0));
        numValue.setAnchorPoint(0, 0.5);
        numValue.setPosition(524 * scaleFactor, 142 * scaleFactor);
        this.addChild(numValue);


        var message2 = " If a seat is available in this tournament, seat will be confirmed to the players in the waitlist by their waitlist position."
        this.message2 = new cc.LabelTTF(message2, "RobotoRegular", 15 * 2 * scaleFactor, cc.TEXT_ALIGNMENT_CENTER);
        this.message2.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.message2.setScale(0.5);
        this.message2._setBoundingWidth(2 * this.width - 30 * scaleFactor);
        this.message2.setColor(cc.color(25, 25, 25));
        this.message2.setPosition(cc.p(/*60*/this.width / 2, 102 * scaleFactor));
        this.addChild(this.message2);

        this.okBtn.setPosition(199, 38);

        this.quitBtn = new CreateBtn("cantwait", "cantwaitHover", "CantWaitQuitBtn");
        this.quitBtn.setPosition(cc.p(325 * scaleFactor, 40 * scaleFactor));
        this.addChild(this.quitBtn);

        cc.eventManager.addListener(this.popupTouchListener.clone(), this.quitBtn);

        return true;
    },
    onTouchEndedCallBack: function (touch, event) {
        this._super(touch, event);
        var target = event.getCurrentTarget();
        switch (target._name) {
            case "CantWaitQuitBtn":
                this._gameRoom.processquitReq(this.room.id, this._selectedRadioBtn);
                //this._gameRoom.processQuitConfrmResp(this.room);
                break;
        }
    }
});
var QuitConfirmationPopup = JoinConfirmationPopup.extend({
    ctor: function (room, context, overlay, popupType) {
        this._super(room, context, overlay, popupType);
        this.question.setString("Are you sure you want to quit this tournament ?");

        this.radioOne && this.radioOne.setSelected(false);
        this.radioOne && this.radioTwo.setSelected(false);
        return true;
    },
    yesBtnHandler: function () {
        var name = this._selectedRadioBtn && this._selectedRadioBtn._name;
        var entryFeeType;
        if (this.entryFeeArr[1] == "ChipAndTicket") {
            if (name == "RadioOne")
                entryFeeType = "Chip";
            else if (name == "RadioTwo")
                entryFeeType = "Ticket";
        } else entryFeeType = this.entryFeeArr[1];
        // TODO check by mycashBalance
        var dataObj = {};
        dataObj["feeType"] = entryFeeType;
        sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_QUIT_REQUEST, dataObj, this.roomId);
    },
    radioBtnListener: function (sender) {
        //cc.log();
    }
});