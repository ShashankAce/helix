/**
 * Created by stpl on 12/6/2016.
 */
var ScoreBoard = Popup.extend({
    _gameRoom: null,
    ctor: function (context, dataObj, playerNames) {

        this._super(new cc.Size(880 * scaleFactor, 560 * scaleFactor), context, true);

        this._gameRoom = context;
        this.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);

        var totalTime = 0;  //To calculate total time of all rounds

        for (var b = 1; b <= dataObj.totRnd; b++) {
            totalTime += dataObj["rnd" + b]["time"];
        }
        totalTime *= 0.001;
        totalTime = parseInt(totalTime);

        this.headerLabel.setString("Score Board");

        this.topSilverStrip = new ccui.Layout();
        this.topSilverStrip.setContentSize(cc.size(880 * scaleFactor, 30 * scaleFactor));
        this.topSilverStrip.setAnchorPoint(0, 1);
        this.topSilverStrip.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.topSilverStrip.setBackGroundColor(cc.color(102, 102, 102));
        this.topSilverStrip.setPosition(cc.p(0, 525 * scaleFactor));
        this.addChild(this.topSilverStrip);

        var label;
        for (var p = 0; p < playerNames.length; p++) {
            label = new cc.LabelTTF(playerNames[p], "RobotoBold", 15 * 2 * scaleFactor);
            label.setScale(0.5);
            label.setAnchorPoint(0.5, 0.5);
            label.setColor(cc.color(255, 255, 255));
            label.setPosition(cc.p(((p + 1) * (658 / playerNames.length) + 222) - ((658 / playerNames.length) + 222) / 2 + 108 * scaleFactor, this.topSilverStrip.height / 2));
            this.topSilverStrip.addChild(label);
        }


        this.roundHeader = new cc.LabelTTF("Round", "RobotoBold", 15 * 2 * scaleFactor);
        this.roundHeader.setScale(0.5);
        this.roundHeader.setAnchorPoint(0, 0.5);
        this.roundHeader.setColor(cc.color(255, 255, 255));
        this.roundHeader.setPosition(cc.p(26 * scaleFactor, this.topSilverStrip.height / 2));
        this.topSilverStrip.addChild(this.roundHeader);

        this.timeHeader = new cc.LabelTTF("Time", "RobotoBold", 15 * 2 * scaleFactor);
        this.timeHeader.setScale(0.5);
        this.timeHeader.setAnchorPoint(0, 0.5);
        this.timeHeader.setColor(cc.color(255, 255, 255));
        this.timeHeader.setPosition(cc.p(140 * scaleFactor, this.topSilverStrip.height / 2));
        this.topSilverStrip.addChild(this.timeHeader);

        this.bottomSilverStrip = new ccui.Layout();
        this.bottomSilverStrip.setAnchorPoint(0, 0);
        this.bottomSilverStrip.setContentSize(cc.size(880 * scaleFactor, 30 * scaleFactor));
        this.bottomSilverStrip.setPosition(cc.p(0, 60 * scaleFactor));
        this.bottomSilverStrip.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.bottomSilverStrip.setBackGroundColor(cc.color(102, 102, 102));
        this.addChild(this.bottomSilverStrip);

        var TimeTotal = new cc.LabelTTF(totalTime + " s.", "RobotoBold", 15 * 2 * scaleFactor);  //To calculate total time
        TimeTotal.setScale(0.5);
        TimeTotal.setAnchorPoint(0, 0.5);
        TimeTotal.setColor(cc.color(255, 255, 255));
        TimeTotal.setPosition(cc.p(147 * scaleFactor, 15 * scaleFactor));
        this.bottomSilverStrip.addChild(TimeTotal);

        for (var f = 0; f < playerNames.length; f++) {
            var p = f + 1;
            label = new cc.LabelTTF(dataObj["pl" + p].toString(), "RobotoBold", 15 * 2 * scaleFactor);
            label.setScale(0.5);
            label.setAnchorPoint(0, 0.5);
            label.setColor(cc.color(255, 255, 255));
            label.setPosition(cc.p((p * (658 / playerNames.length) + 222) - ((658 / playerNames.length) + 222) / 2 + 108 * scaleFactor, 15 * scaleFactor));
            this.bottomSilverStrip.addChild(label);
        }

        this.totalScorHeader = new cc.LabelTTF("Total Score", "RobotoBold", 15 * 2 * scaleFactor);
        this.totalScorHeader.setScale(0.5);
        this.totalScorHeader.setColor(cc.color(255, 255, 255));
        this.totalScorHeader.setAnchorPoint(0, 0.5);
        this.totalScorHeader.setPosition(cc.p(15 * scaleFactor, 15 * scaleFactor));
        this.bottomSilverStrip.addChild(this.totalScorHeader);

        this.cross = new cc.Sprite(spriteFrameCache.getSpriteFrame("ResultClose.png"));
        this.cross.setName("cross");
        this.cross.setAnchorPoint(0.5, 0.5);
        this.cross.hover = function () {
        };
        this.cross.setPosition(cc.p(this.width - 3 * scaleFactor, this.height - 3 * scaleFactor));
        this.addChild(this.cross, 50);

        this.bottomStrip = new BottomStrip();
        this.bottomStrip.setPosition(cc.p(0, 0));
        this.addChild(this.bottomStrip);

        for (var y = 0; y < dataObj["totRnd"]; y++) {
            this.roundStrip = new RoundStrip(playerNames.length, y + 1, dataObj["rnd" + (y + 1)]); //y in parameter is used to refer to round Number,Third parameter is round no.
            this.roundStrip.setPosition(cc.p(0, this.height - 65 * scaleFactor - (y * 57 * scaleFactor)));
            this.addChild(this.roundStrip);
        }

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), this.cross);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.cross);

        return true;
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        if (target._name == "cross") {
            this.closeScoreBoard();
        }
    },
    closeScoreBoard: function () {
        this.removeFromParent(true);
    }

});

var BottomStrip = ccui.Layout.extend({
    ctor: function () {

        this._super();
        this.setContentSize(cc.size(880 * scaleFactor, 60 * scaleFactor));
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.setBackGroundColor(cc.color(230, 240, 245));

        this.line1 = new cc.DrawNode();
        this.line1.drawSegment(cc.p(130 * scaleFactor, 1 * scaleFactor), cc.p(130 * scaleFactor, 59 * scaleFactor), 0.5, cc.color(200, 204, 229));
        //  this.line1.setOpacity(200);
        this.addChild(this.line1);

        this.line2 = new cc.DrawNode();
        this.line2.drawSegment(cc.p(270 * scaleFactor, 1 * scaleFactor), cc.p(270 * scaleFactor, 59 * scaleFactor), 0.5, cc.color(200, 204, 229));
        this.addChild(this.line2);

        this.line3 = new cc.DrawNode();
        this.line3.drawSegment(cc.p(387 * scaleFactor, 1 * scaleFactor), cc.p(387 * scaleFactor, 59 * scaleFactor), 0.5, cc.color(200, 204, 229));
        this.addChild(this.line3);

        this.winner = new cc.LabelTTF("Winner", "RobotoRegular", 15 * 2 * scaleFactor);
        this.winner.setAnchorPoint(0, 0.5);
        this.winner.setScale(0.5);
        this.winner.setPosition(cc.p(64 * scaleFactor, 30 * scaleFactor));
        this.winner.setColor(cc.color(0, 0, 0));
        this.addChild(this.winner);

        this.prizeCup = new cc.Sprite(spriteFrameCache.getSpriteFrame("prize.png"));
        this.prizeCup.setAnchorPoint(0, 0.5);
        this.prizeCup.setPosition(cc.p(27 * scaleFactor, 30 * scaleFactor));
        this.addChild(this.prizeCup);

        this.lefttable = new cc.LabelTTF("Left Table", "RobotoRegular", 15 * 2 * scaleFactor);
        this.lefttable.setAnchorPoint(0, 0.5);
        this.lefttable.setScale(0.5);
        this.lefttable.setPosition(cc.p(188 * scaleFactor, 30 * scaleFactor));
        this.lefttable.setColor(cc.color(0, 0, 0));
        this.addChild(this.lefttable);

        this.left = new cc.Sprite(spriteFrameCache.getSpriteFrame("LeftTableIcon.png"));
        this.left.setAnchorPoint(0, 0.5);
        this.left.setPosition(cc.p(152 * scaleFactor, 30 * scaleFactor));
        this.addChild(this.left);

        this.reJoin = new cc.LabelTTF("Re-buy", "RobotoRegular", 15 * 2 * scaleFactor);
        this.reJoin.setAnchorPoint(0, 0.5);
        this.reJoin.setScale(0.5);
        this.reJoin.setPosition(cc.p(327 * scaleFactor, 30 * scaleFactor));
        this.reJoin.setColor(cc.color(0, 0, 0));
        this.addChild(this.reJoin);

        this.rejoinIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("Rejoin.png"));
        this.rejoinIcon.setAnchorPoint(0, 0.5);
        this.rejoinIcon.setPosition(cc.p(291 * scaleFactor, 30 * scaleFactor));
        this.addChild(this.rejoinIcon);

        this.gameOver = new cc.LabelTTF("Game Over", "RobotoRegular", 15 * 2 * scaleFactor);
        this.gameOver.setAnchorPoint(0, 0.5);
        this.gameOver.setScale(0.5);
        this.gameOver.setPosition(cc.p(435 * scaleFactor, 30 * scaleFactor));
        this.gameOver.setColor(cc.color(0, 0, 0));
        this.addChild(this.gameOver);

        this.gameOverIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("GameOver.png"));
        this.gameOverIcon.setAnchorPoint(0, 0.5);
        this.gameOverIcon.setPosition(cc.p(400 * scaleFactor, 30 * scaleFactor));
        this.addChild(this.gameOverIcon);

        return true;
    }

});

var RoundStrip = ccui.Layout.extend({
    ctor: function (noOfPlayers, roundNo, roundInfo) {
        this._super();
        this.setContentSize(cc.size(880 * scaleFactor, 57 * scaleFactor));
        this.setAnchorPoint(0, 1);
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.setBackGroundColor(cc.color(245, 245, 245));

        var bottomLine = new cc.DrawNode();
        bottomLine.drawSegment(cc.p(5 * scaleFactor, 0), cc.p(this.width - 5 * scaleFactor, 0), 1, cc.color(230, 230, 230));
        this.addChild(bottomLine);

        this.round = new cc.LabelTTF(roundNo, "RobotoRegular", 15 * 2 * scaleFactor);
        this.round.setScale(0.5);
        this.round.setPosition(cc.p(50 * scaleFactor, 28 * scaleFactor));
        this.round.setColor(cc.color(0, 0, 0));
        this.addChild(this.round);

        var line1 = new cc.DrawNode();
        line1.drawSegment(cc.p(110 * scaleFactor, 0), cc.p(110 * scaleFactor, 55 * scaleFactor), 1, cc.color(230, 230, 230));
        this.addChild(line1);

        var roundTimeValue = parseInt(roundInfo.time * 0.001);
        this.roundTime = new cc.LabelTTF(roundTimeValue + " s.", "RobotoRegular", 15 * 2 * scaleFactor);
        this.roundTime.setScale(0.5);
        this.roundTime.setPosition(cc.p(165 * scaleFactor, 28 * scaleFactor));
        this.roundTime.setColor(cc.color(0, 0, 0));
        this.addChild(this.roundTime);

        var line2 = new cc.DrawNode();
        line2.drawSegment(cc.p(222 * scaleFactor, 0), cc.p(222 * scaleFactor, 55 * scaleFactor), 1, cc.color(230, 230, 230));
        this.addChild(line2);

        var leftPlyrArr = roundInfo.leaveStr && roundInfo.leaveStr.split(",");
        for (var x = 1; x <= noOfPlayers; x++) {
            if (x < noOfPlayers) {
                var line = new cc.DrawNode();
                line.drawSegment(cc.p(x * (658 / noOfPlayers) + 222, 0), cc.p(x * (658 / noOfPlayers) + 222, 55), 1, cc.color(230, 230, 230));
                this.addChild(line);
            }
            if (roundInfo.winner == "pl" + x) {

                this.cup = new cc.Sprite(spriteFrameCache.getSpriteFrame("prize.png"));
                this.cup.setAnchorPoint(0.5, 0.5);
                this.cup.setPosition(cc.p((x * (658 / noOfPlayers) + 222) - ((658 / noOfPlayers) + 222) / 2 + 114 * scaleFactor, 30 * scaleFactor));
                this.addChild(this.cup);
// leaveStr
            } else if (leftPlyrArr && leftPlyrArr.indexOf("pl" + x) >= 0) {
                /* this.score = new cc.LabelTTF(roundInfo["pl" + x], "RobotoRegular", 15 * 2);
                 this.score.setScale(0.5);
                 this.score.setAnchorPoint(0.5, 0.5);
                 this.score.setColor(cc.color(0, 0, 0));
                 this.score.setPosition((x * (658 / noOfPlayers) + 222) - ((658 / noOfPlayers) + 222) / 2 + 114, 30);
                 this.addChild(this.score);*/
                this.score = new cc.LabelTTF(roundInfo["pl" + x], "RobotoRegular", 15 * 2 * scaleFactor);
                this.score.setScale(0.5);
                this.score.setAnchorPoint(0.5, 0.5);
                this.score.setColor(cc.color(0, 0, 0));
                this.score.setPosition(cc.p((x * (658 / noOfPlayers) + 222) - ((658 / noOfPlayers) + 222) / 2 + 114 * scaleFactor, 30 * scaleFactor));
                this.addChild(this.score);

                this.playerLeftIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("LeftTableIcon.png"));
                this.playerLeftIcon.setAnchorPoint(0.5, 0.5);
                this.playerLeftIcon.setPosition(cc.p((x * (658 / noOfPlayers) + 222) - ((658 / noOfPlayers) + 222) / 2 + 140* scaleFactor, 30 * scaleFactor));
                this.addChild(this.playerLeftIcon);
            } else if (roundInfo.rejoin == "pl" + x) {
                this.reJoinPlayer = new cc.Sprite(spriteFrameCache.getSpriteFrame("Rejoin.png"));
                this.reJoinPlayer.setAnchorPoint(0.5, 0.5);
                this.reJoinPlayer.setPosition(cc.p((x * (658 / noOfPlayers) + 222) - ((658 / noOfPlayers) + 222) / 2 + 114 * scaleFactor, 30 * scaleFactor));
                this.addChild(this.reJoinPlayer);
            } else if (roundInfo.gameover == "pl" + x) {
                this.over = new cc.Sprite(spriteFrameCache.getSpriteFrame("GameOver.png"));
                this.over.setAnchorPoint(0.5, 0.5);
                this.over.setPosition(cc.p((x * (658 / noOfPlayers) + 222) - ((658 / noOfPlayers) + 222) / 2 + 114 * scaleFactor, 30 * scaleFactor));
                this.addChild(this.over);

            } else {
                this.score = new cc.LabelTTF(roundInfo["pl" + x], "RobotoRegular", 15 * 2 * scaleFactor);
                this.score.setScale(0.5);
                this.score.setAnchorPoint(0.5, 0.5);
                this.score.setColor(cc.color(0, 0, 0));
                this.score.setPosition(cc.p((x * (658 / noOfPlayers) + 222) - ((658 / noOfPlayers) + 222) / 2 + 114 * scaleFactor, 30 * scaleFactor));
                this.addChild(this.score);
            }
        }
        return true;
    }
});

