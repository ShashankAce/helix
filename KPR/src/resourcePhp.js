var GAME_SIZE = {
    width: null,
    height: null
};
var res, loaderRes;
var scaleFactor = 1;
var g_resources = [];
var loader_resources = [];
(function () {

    var gameLib = {};

    //for landscape mode
    if (screen.width > screen.height) {
        gameLib.screenWidth = screen.width;
        gameLib.screenHeight = screen.height;
    }
    else {
        gameLib.screenHeight = screen.width;
        gameLib.screenWidth = screen.height;
    }

    var availGameSize = {
        /* 1: {
         width: 500,
         height: 350
         },*/
        1: {
            width: 1000,
            height: 650
        },
        4: {
            width: 4000,
            height: 2600
        }
    };

    var tempValue, gameIndex = 1, index;
    tempValue = Math.abs(gameLib.screenWidth - availGameSize[gameIndex].width) + Math.abs(gameLib.screenWidth - availGameSize[gameIndex].height);
    for (var key in availGameSize) {
        index = parseInt(key);
        var xratio = Math.abs(gameLib.screenWidth - availGameSize[index].width);
        var yratio = Math.abs(gameLib.screenHeight - availGameSize[index].height);
        if ((xratio + yratio) <= tempValue) {
            tempValue = xratio + yratio;
            gameIndex = index;
        }
    }
    scaleFactor = gameIndex;
    GAME_SIZE.width = availGameSize[gameIndex].width;
    GAME_SIZE.height = availGameSize[gameIndex].height;

    res = {
        fonts: [
            {
                type: "font",
                name: "RupeeFordian",
                srcs: ["res/fonts/Rupee_Foradian.ttf?v=" + version]
            },
            {
                type: "font",
                name: "FranklinDemi",
                srcs: ["res/fonts/FranklinGothicDemi.ttf?v=" + version]
            },
            {
                type: "font",
                name: "RobotoBold",
                srcs: ["res/fonts/Roboto-Bold.ttf?v=" + version]
            },
            {
                type: "font",
                name: "RobotoLight",
                srcs: ["res/fonts/Roboto-Light.ttf?v=" + version]
            },
            {
                type: "font",
                name: "RobotoRegular",
                srcs: ["res/fonts/Roboto-Regular.ttf?v=" + version]
            }

        ],
        atlas: {
            lobbyPlist_png: "res/lobbyPlist.png?v=" + version,
            lobbyPlist_plist: "res/lobbyPlist.plist?v=" + version,
            gameRoomPlist_png: "res/gameRoomPlist.png?v=" + version,
            gameRoomPlist_plist: "res/gameRoomPlist.plist?v=" + version,
            SmileyPlist_png: "res/SmileyPlist.png?v=" + version,
            SmileyPlist_plist: "res/SmileyPlist.plist?v=" + version,
            ButtonPlist_png: "res/ButtonsPlist.png?v=" + version,
            ButtonPlist_plist: "res/ButtonsPlist.plist?v=" + version
        },
        ccs: {
            smileyAndChat: "res/SmileyAndChat.json"
        },
        lobby: {
            logo: "res/logo.png?v=" + version,
            playBtn: "res/playBtn.png?v=" + version
        },
        table: {
            background13card: "res/13cardbg.png?v=" + version,
            editbox: "res/editbox.png?v=" + version,
            sliderBar: "res/SliderBar.png?v=" + version
        },
        winner: {
            winnerGlow: "res/winnerGlow.png?v=" + version,
            winnerLogo: "res/winnerLogo.png?v=" + version,
            winnerField: "res/winnerField.png?v=" + version
        },
        gameFaq: {
            pureSeqImg: "res/setTip.png?v=" + version,
            jokerImage: "res/JokerTip.png?v=" + version,
            doublesImage: "res/DoubleTip.png?v=" + version,
            tunnelImage: "res/TunnelTip.png?v=" + version,
            TwentyOneCardsPureSequence: "res/pureSequenceTour.png?v=" + version,
            TwentyOneCardsJokerImage: "res/8Jokers.png?v=" + version,
            TwentyOneCardsDoublesImage: "res/8Doublees.png?v=" + version,
            TwentyOneTunnelImage: "res/3Tunnelas.png?v=" + version
        },
        sound: {
            carddistributionflip: "res/Sounds/card_disribute.mp3?v=" + version,
            cardFlip: "res/Sounds/card_flip.mp3?v=" + version,
            cutthedeck: "res/Sounds/cutthedeck.mp3?v=" + version,
            dropsound: "res/Sounds/dropsound.mp3?v=" + version,
            extratimesound: "res/Sounds/extratimesound.mp3?v=" + version,
            finalwinningifme: "res/Sounds/finalwinningifme.mp3?v=" + version,
            jointable: "res/Sounds/jointable.mp3?v=" + version,
            meldingscreensound: "res/Sounds/meldingscreensound.mp3?v=" + version,
            myturnsound: "res/Sounds/myturnsound.mp3?v=" + version,
            picknddropsound: "res/Sounds/picknddropsound.mp3?v=" + version,
            validshow: "res/Sounds/validshow.mp3?v=" + version,
            wrongshow: "res/Sounds/wrongshow.mp3?v=" + version
        },
        reportProblem: {
            minus: "res/minus_icon.png?v=" + version,
            plus: "res/plus_icon.png?v=" + version,
            success: "res/success-icon.png?v=" + version
        }
    };
    loaderRes = {
        club: "res/loader/club.png?v=" + version,
        diamond: "res/loader/diamond.png?v=" + version,
        heart: "res/loader/heart.png?v=" + version,
        spade: "res/loader/spade.png?v=" + version,
        clubFilled: "res/loader/clubFilled.png?v=" + version,
        diamondFilled: "res/loader/diamondFilled.png?v=" + version,
        heartFilled: "res/loader/heartFilled.png?v=" + version,
        spadeFilled: "res/loader/spadeFilled.png?v=" + version,
        bg: "res/loader/loaderbg.png?v=" + version,
    };
    for (var key in loaderRes) {
        loader_resources.push(loaderRes[key]);
    }

    for (var i in res) {
        for (var j in res[i]) {
            g_resources.push(res[i][j]);
        }
    }

    res.video = {
        card_13: 'res/videos/13card.mp4?v=' + version,
        discard: 'res/videos/discard.mp4?v=' + version,
        group: 'res/videos/group.mp4?v=' + version,
        pick: 'res/videos/pick.mp4?v=' + version,
        sequence: 'res/videos/sequence.mp4?v=' + version,
        show: 'res/videos/show.mp4?v=' + version
    }
})();