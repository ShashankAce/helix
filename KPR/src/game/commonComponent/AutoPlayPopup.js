/**
 * Created by stpl on 3/30/2017.
 */
var AutoPlayPopup = cc.LayerColor.extend({
    ctor: function () {
        this._super(cc.color(0, 0, 0, 180 * scaleFactor));

        var msgString = "You have been put on Auto-Play for missing turn. Please click below button to back in game.";
        var autoModeMessage = new cc.LabelTTF(msgString, "RobotoRegular", 15 * scaleFactor);
        autoModeMessage.setColor(cc.color(255, 255, 255));
        autoModeMessage.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(autoModeMessage, 2);

        this.autoModeBackBtn = new cc.Sprite(spriteFrameCache.getSpriteFrame("AutoPlayBtn.png"));
        this.autoModeBackBtn.setName("AutoModeBackButton");
        this.autoModeBackBtn.setPosition(cc.p(this.width / 2, autoModeMessage.y - 50 * scaleFactor));
        this.autoModeBackBtn._isEnabled = true;
        this.autoModeBackBtn.hover = function (bool) {
            bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("AutoPlayBtnOver.png")) : this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("AutoPlayBtn.png"));
        };
        this.addChild(this.autoModeBackBtn, 2);

        this.glow = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("tabGlow.png"));
        this.glow.setPosition(cc.p(this.width / 2, autoModeMessage.y - 50 * scaleFactor));
        this.glow.setContentSize(cc.size(this.autoModeBackBtn.width + 23 * scaleFactor, this.autoModeBackBtn.height + 23 * scaleFactor));
        this.addChild(this.glow, 8, "glow");

        this.startGlow();

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                cc.log("Blank listener in autoMode");
                return true;
            }.bind(this)
        }, this);

    },
    startGlow: function () {
        this.glow.stopAllActions();
        this.glow.setColor(cc.color(199, 222, 47));
        this.glow.runAction(cc.sequence(cc.fadeIn(0.5), cc.fadeOut(0.5)).repeatForever());
    },
    removeAutoPlayLayer: function () {
        this.removeFromParent(true);
    }
});