/**
 * Created by Shashank on 6/7/2016.
 */
"use strict";
var PlayerInfo = cc.Sprite.extend({

    seat_id: null,
    player_id: null,
    player_name: null,
    player_score: null,
    player_rank : null,
    player_extraTime: null,
    isDropped: null,
    isDisconnected: null,
    isAutoPlay: null,
    isWrongShow: null,
    isWaiting: null,
    _isNext: null,

    ctor: function (spriteFrame) {
        this._super(spriteFrame);
        return true;
    },
    updateInfo: function (seatId, playerId, playerName, playerScore, isDropped, isDisconnected, isAutoPlay, isWrongShow, isWaiting) {
        this.seat_id = seatId;
        this.player_id = parseInt(playerId);
        this.player_name = playerName;
        this.player_score = playerScore;
        this.isDropped = isDropped;
        this.isDisconnected = isDisconnected;
        this.isAutoPlay = isAutoPlay;
        this.isWrongShow = isWrongShow;
        this.isWaiting = isWaiting;
    },
    getPlayer_id: function () {
        return this.player_id;
    },
    getPlayer_name: function () {
        return this.player_name;
    },
    getSeat_id: function () {
        return this.seat_id;
    },
    getPlayer_score: function () {
        return this.player_score;
    },
    getPlayer_extraTime: function () {
        return this.player_extraTime;
    },
    setSeatID: function (seatID) {
        this.seat_id = parseInt(seatID);
    },
    setScore: function (score, rank) {
        this.player_score = score + "";
        this.player_rank = rank;
        this.updateScore(score, rank);
    },
    getScore: function (score) {
        return this.player_score;
    },
    setPlayerExtraTime: function (extratime) {
        this.player_extraTime = extratime;
    },
    isAutoPlayFun: function () {
        return this.isAutoPlay;
    },
    isDisconnectedFun: function () {
        return this.isDisconnected;
    },
    isDroppedFun: function () {
        return this.isDropped;
    },
    isWrongShowFun: function () {
        return this.isWrongShow;
    },
    isWaitingFun: function () {
        return this.isWaiting;
    },
    clearAllState: function () {
        this.isDropped = false;
        this.isDisconnected = false;
        this.isAutoPlay = false;
        this.isWrongShow = false;
        this.isWaiting = false;
    },
    setWrongShow: function (wrongShow) {
        this.clearAllState();
        this.isWrongShow = wrongShow;
    },
    setDisconnected: function (disconnected) {
        this.clearAllState();
        this.isDisconnected = disconnected;
    },
    setAutoPlay: function (autoplay) {
        this.clearAllState();
        this.isAutoPlay = autoplay;
    },
    setDropped: function (dropped) {
        this.clearAllState();
        this.isDropped = dropped;
    }
});