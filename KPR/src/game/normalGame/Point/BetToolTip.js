/**
 * Created by stpl on 12/6/2016.
 */
var BetToolTip = cc.Sprite.extend({
    _delegate: null,
    _gameType: null,
    ctor: function (gameType, amount, context) {

        this._super(spriteFrameCache.getSpriteFrame("BetToolTipBg.png"));

        this._delegate = context;

        this.setAnchorPoint(0, 0.5);
        this.setVisible(true);

        this._gameType = gameType;
        var string = "", spriteName = "", spriteHoverName = "", buttonName = "";
        if (gameType.toUpperCase() == "CASH") {
            string = "Make a deposit to continue playing.";
            spriteName = "addCash";
            spriteHoverName = "addCashOver";
            buttonName = "addCashBtn";
        }
        else if (gameType.toUpperCase() == "PRACTICE") {
            string = "You need to refill your free practice chips.";
            spriteName = "refill";
            spriteHoverName = "refillover";
            buttonName = "refillBtn";
        }

        this.buttonSprite = new CreateBtn(spriteName, spriteHoverName, buttonName);
        this.buttonSprite.setAnchorPoint(0, 0.5);
        this.buttonSprite.setPosition(cc.p(25 * scaleFactor, 30 * scaleFactor));
        this.addChild(this.buttonSprite, 1);

        this.label = new cc.LabelTTF("Your balance is less than ", "RobotoRegular", 14 * 2 * scaleFactor);
        this.label.setScale(0.5);
        this.label.setAnchorPoint(0, 1);
        this.label.setColor(cc.color(0, 0, 0));
        this.label.setPosition(cc.p(20 * scaleFactor, 106 * scaleFactor));
        this.label._setBoundingWidth(400 * scaleFactor);
        this.addChild(this.label, 2);

        this.amount = new cc.LabelTTF(amount, "RobotoBold", 14 * 2 * scaleFactor);
        this.amount.setScale(0.5);
        this.amount.setColor(cc.color(0, 0, 0));
        this.amount.setAnchorPoint(0, 1);
        this.amount._setBoundingWidth(140 * scaleFactor);
        this.amount.setPosition(cc.p(this.label.x + this.label.width / 3 + 27 * scaleFactor, this.label.y));
        this.addChild(this.amount, 1);

        this.label2 = new cc.LabelTTF(string, "RobotoRegular", 14 * 2 * scaleFactor);
        this.label2.setScale(0.5);
        this.label2.setAnchorPoint(0, 1);
        this.label2.setColor(cc.color(0, 0, 0));
        this.label2.setPosition(cc.p(20 * scaleFactor, 90 * scaleFactor));
        this.label2._setBoundingWidth(400 * scaleFactor);
        this.addChild(this.label2, 1);
        
        if ('mouse' in cc.sys.capabilities)
        this.initMouseListener();

        return true;
    },
    initMouseListener: function () {

        var listener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            onMouseDown: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    this._delegate.tooTipBtnClickHandler(this._gameType);
                }
            }.bind(this)
        });
        listener.retain();
        cc.eventManager.addListener(listener.clone(), this.buttonSprite);

    }
});
