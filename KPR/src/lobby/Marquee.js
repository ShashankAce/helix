/**
 * Created by stpl on 4/19/2017.
 */
var Marquee = cc.LabelTTF.extend({
    _hovering: null,
    _lobby: null,
    _name: null,
    _link: null,
    ctor: function (dataObj, lobby) {
        var newsStr = dataObj['msg'];
        this._link = dataObj['url'];
        this._super(newsStr, "RobotoRegular", 11 * 2);
        this._lobby = lobby;
        this.setScale(0.5);
        this._hovering = false;
        this.setTag(222);
        this.setName("Marquee");
        this.resetPosition();
        return true;
    },
    resetPosition: function () {
        this.setPosition(cc.p(this._lobby.width + this.width / 4, this._lobby.buttomTabHeight + this._lobby.buttomTabHeight / 2));
    },
    appear: function () {
        this._lobby.addChild(this, 2);
        var speed = 100;
        var time1 = this.width / speed;
        var time2 = (this.width + this._lobby.width) / speed;
        var move1 = cc.moveTo(time1, cc.p(cc.winSize.width - this.width / 4, this._lobby.buttomTabHeight + this._lobby.buttomTabHeight / 2));
        var move2 = cc.moveTo(time2, cc.p(-this.width / 4, this._lobby.buttomTabHeight + this._lobby.buttomTabHeight / 2));
        var action = cc.sequence(move1, cc.callFunc(function () {
            this._lobby.initNextMarquee();
        }, this), move2, cc.callFunc(function () {
            this.resetPosition();
            this.removeFromParent(false);
            // this._lobby.initNextMarquee();
        }, this));
        action.setTag(222);
        this.runAction(action);
    },
    hover: function (bool) {
        if (bool) {
            //cc.director.getActionManager().pauseTarget(this);
        } else {
            // cc.director.getActionManager().resumeTarget(this);
        }
        this._lobby.pauseMarquee(bool);
    },
    clickHandler: function () {
        this._link && cc.sys.openURL(this._link);
    }
});

var Marquee2 = cc.LabelTTF.extend({
    _hovering: null,
    _lobby: null,
    _name: null,
    _link: null,
    ctor: function (dataObj, lobby) {
        var newsStr = dataObj['msg'];
        this._link = dataObj['url'];
        this._super(newsStr, "RobotoRegular", 11 * 2);
        this._lobby = lobby;
        this.setScale(0.5);
        this._hovering = false;
        this.setTag(222);
        this.setName("Marquee");
        this.setAnchorPoint(0, 0.5);
        return true;
    },
    hover: function (bool) {
        this._lobby.pauseMarquee(bool);
    },
    clickHandler: function () {
        this._link && cc.sys.openURL(this._link);
    }
});