/**
 * Created by stpl on 1/11/2017.
 */
var MyTables = cc.Sprite.extend({
    _lobby: null,
    ctor: function (context) {
        this._super(spriteFrameCache.getSpriteFrame("MyTableBg.png"));
        this._lobby = context;
        var pos = context.myTableTab.getPosition();
        this.setPosition(cc.p(pos.x - 5, context.height - context.myTableTab.height));
        this.setAnchorPoint(0.5, 1);
        this.setVisible(false);
        return true;
    },
    initMyTablePanel: function () {
        this.removeAllChildren(true);
        var tableIdArray = applicationFacade._gameMap;
        if (Object.keys(tableIdArray).length > 0) {
            this.displayTableName(tableIdArray);
        } else {
            var label = new cc.LabelTTF("No Table Joined Yet.", "RobotoBold", 22);
            label.setScale(0.60);
            label.setPosition(cc.p(this.width / 2, this.height / 2));
            this.addChild(label, 1);
        }
    },
    displayTableName: function (tableIdArray) {
        var marginX = 15, marginY = 15;
        var line = new cc.DrawNode();
        line.drawSegment(cc.p(marginX, this.height / 2 + marginY), cc.p(this.width - marginX, this.height / 2 + marginY), 0.5, cc.color(68, 67, 67));
        this.addChild(line, 1);

        var line2 = new cc.DrawNode();
        line2.drawSegment(cc.p(marginX, this.height / 2 - marginY), cc.p(this.width - marginX, this.height / 2 - marginY), 0.5, cc.color(68, 67, 67));
        this.addChild(line2, 1);

        var arr = [];
        for (var a in tableIdArray) {
            arr.push(tableIdArray[a].roomId);
        }


        var initialY = 20 * scaleFactor, diff = 30 * scaleFactor, i = 0;
        for (var a in tableIdArray) {
            var tableIdLabel = new cc.LabelTTF(tableIdArray[a].roomId.toString(), "RobotoBold", 22);
            tableIdLabel._roomId = tableIdArray[a].roomId;
            tableIdLabel.tableId = a;
            tableIdLabel.setName("MyTable_" + tableIdArray[a].roomId);
            tableIdLabel.setScale(0.60);
            tableIdLabel.setPosition(cc.p(this.width / 2, this.height - 5 - initialY - diff * i));
            i++;
            this.addChild(tableIdLabel, 11);
            cc.eventManager.addListener(this._lobby.lobbyTouchListener.clone(), tableIdLabel);
        }
    }
});