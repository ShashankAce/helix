/**
 * Created by stpl on 9/16/2016.
 */
"use strict";
var NormalBaseGame = MiddleBaseGame.extend({
    selfSeatPosition: null,
    _jokerSymVisble: null,
    currentPlayer: null,
    currentPickCard: null,
    _totalSeats: null,
    _isWaiting: null,
    _waitTimer: null,
    _isWatching: null,
    showValidity: null,
    WrongShowMsg: null,
    autoPlayPopup: null,
//////////////////////////////////////
    isJoinSeatConfirmed: null,
    _currentTurnID: null,
    _connectCount: null,
    _waitTime: null,
    _stopAllTimer: null,
    _seatShiftOffset: null,
    _leaveAllowed: null,
    _extraTimeToggle: null,
    _isExtraTurnTime: null,
    _showMeld: null,
    _bgCounter: null,
    _isPlayerConnect: null,
    _isShowDistribution: null,
    _requestToSever: null,
    lastPickCard: null,
    _outputStr: null,
    _isRoomActive: null,
    _leftPlArr: null,
    isDiscardHistoryAvailable: null,
    ScoreBoardPlayers: null,
    bonusToCashPopup: null,

    ctor: function (room) {
        this._super(room);

        this.isJoinSeatConfirmed = false;
        this._currentTurnID = -1;
        this._leftPlArr = [];
        this.isDiscardHistoryAvailable = false;
        this.ScoreBoardPlayers = [];
        this._seatShiftOffset = 0;
        this._leaveAllowed = true;
        this._extraTimeToggle = false;
        this._isExtraTurnTime = false;
        this._isWatching = false;
        this._dropped = false;
        this._showMeld = true;
        this._stopAllTimer = false;
        this._waitTime = 0;
        this._bgCounter = 0;
        this._isPlayerConnect = false;
        this._isShowDistribution = true;
        this._connectCount = 0;
        this._requestToSever = true;
        this.lastPickCard = "";
        this._outputStr = "";
        this._isRoomActive = true;
        this._isWaiting = false;

        cc.log("NormalBaseGame is initiated");

        if (room.getVariable("cashOrPromo") && (room.getVariable("cashOrPromo").value == "CASH") && (room.getVariable(SFSConstants.FLD_NUMOFCARD).value == 21))
            this._jokerSymVisble = false;
        else
            this._jokerSymVisble = true;

        this._userCardPanel = new UserCardPanel(this);
        this.addChild(this._userCardPanel, GameConstants.userCardPanelZorder);

        this.deckPanel = new DeckPanel(this);
        this.addChild(this.deckPanel, GameConstants.deckPanelZorder);

        this.smileyAndChatIcon = new SmileyAndChatNew(this);
        this.addChild(this.smileyAndChatIcon, 2);

        this.totCards = parseInt(room.getVariable(SFSConstants.FLD_NUMOFCARD).value);

        if (room.getVariable("cashOrPromo").value != "CASH") {
            this.showValidity = new ShowValidityLayout(this);
            this.showValidity.setPosition(cc.p(539 * scaleFactor, 47 * scaleFactor));
            this.addChild(this.showValidity, GameConstants.playerZorder - 1, "showValidity");
        }

        this.waitingMC = new cc.LabelTTF("", "RobotoRegular", 25 * 2 * scaleFactor);
        this.waitingMC.setScale(0.5);
        this.waitingMC.setVisible(false);
        this.waitingMC.setPosition(cc.p(this.width / 2, this.height / 2 + 90 * scaleFactor - yMargin));
        this.waitingMC.setColor(cc.color(145, 172, 155));
        this.addChild(this.waitingMC, 2);

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        if (this._serverRoom.getVariable("cashOrPromo").value != "CASH") {
            this.playForRealButton = new CreateBtn("PlayForCashBtn", "PlayForCashBtnOver", "playForRealButton", cc.p(800 * scaleFactor, this.height - 12 * scaleFactor));
            this.addChild(this.playForRealButton, GameConstants.headerZorder, "playForRealButton");
            cc.eventManager.addListener(this.baseGameMouseListener.clone(), this.playForRealButton);
            cc.eventManager.addListener(this.baseGameTouchListener.clone(), this.playForRealButton);
        }

        return true;
    },
    /*showDeclareInfo:function(){
     this.invalidDeclr && this.invalidDeclr.removeFromParent();
     this.invalidDeclr = new InvalidDeclaration(this);
     this.addChild(this.invalidDeclr, GameConstants.gameResultZorder+1);
     this.invalidDeclr.setPosition(this.width/2, this.height/2);
     this.invalidDeclr._init(this._validityObj);
     },*/
    sendReqToServer: function (cmd, reqObj) {
        if (!this._requestToSever)
            return;
        if ((this._myId == -1 && this._isWatching ) && (cmd == "connectAgain") && applicationFacade._tempJoinRooms.indexOf(this._serverRoom.id + "") == -1) {
            applicationFacade.updateExternalLobbyBalance();
            this.leaveRoomByMyself();
            return;
        }
        if (this._myId != -1)
            reqObj.id = this._myId;

        sfsClient.sendReqFromRoom(cmd, reqObj, this._serverRoom.id);
    },
    playForCashHandler: function () {
        var sfsObj = {};
        sfsObj.type = this._serverRoom.getVariable(SFSConstants._CASH_OR_PROMO).value;
        sfsObj.gameType = this._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
        sfsObj.numOfCard = this.totCards + "";
        applicationFacade.realPlayBtnHandler(sfsObj);
    },
    instantPlayBtnHandler: function () {
        this.insantPlayBtn && this.insantPlayBtn.removeFromParent(true);
        var sfsObj = {};
        sfsObj.type = this._serverRoom.getVariable(SFSConstants._CASH_OR_PROMO).value;
        sfsObj.gameType = this._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
        sfsObj.numOfCard = this.totCards.toString();

        if (Object.keys(applicationFacade._gameMap).length == 3) {
            if (!applicationFacade.maxJoinTablePopup) {
                applicationFacade.maxJoinTablePopup = new MaxJoinTablePopup(this);
                applicationFacade.addChild(applicationFacade.maxJoinTablePopup, 10, "maxJoinTablePopup");
            }
        } else {
            applicationFacade.instantPlayBtnHandler(sfsObj);
        }
    },
    createSpriteWithOver: function (spriteName, spriteOverName, buttonName, pos) {
        var Sprite = cc.Sprite.extend({
            spriteOverName: null,
            spriteName: null,
            _isEnabled: null,
            _hovering: null,
            ctor: function (spriteName, spriteOverName, buttonName, pos) {
                this._super(spriteFrameCache.getSpriteFrame(spriteName + ".png"));
                this.spriteName = spriteName;
                this.spriteOverName = spriteOverName;
                this._hovering = false;
                pos && this.setPosition(pos);
                this.setName(buttonName);

                return true;
            },
            hover: function (bool) {

                bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteOverName + ".png")) :
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteName + ".png"));
            },
            pauseListener: function (bool) {
                this._isEnabled = !bool;
            },
            hide: function (bool) {
                this.setVisible(!bool);
                this._isEnabled = !bool;
            }
        });
        return new Sprite(spriteName, spriteOverName, buttonName, pos);
    },
    setRoomVars: function () {
        var roomState = this._serverRoom.getVariable(SFSConstants._ROOMSTATE).value;
        if (roomState == SFSConstants.RS_CUTTHEDECK)
            return;
        if (this._serverRoom.getVariable(SFSConstants._WAITTIME) && (this._serverRoom.getVariable(SFSConstants._WAITTIME).value) && (roomState != SFSConstants.RS_RESULT))
            this.scheduleWaitTime();
    },
    processWaitTimer: function (param, room) {
        this._leftPlArr = [];
        this._isShowDistribution = true;
        this.cutDeckPanel = null;
        this._leaveAllowed = true;

        this.cutDeckPanel && this.cutDeckPanel.removeFromParent(true);
        if (this._myId != -1) {
            this.result && this.result.removeFromParent(true);
            this.winner && this.winner.removeFromParent(true);
        }


        if (this.waitingMC) {
            this.waitingMC.setString("");
            this.waitingMC.setVisible(false);
        }

        this.confirmationPopup && this.confirmationPopup.removeFromParent(true);
        this.confirmationPopup = null;

        // if (/*this._myId != -1 || */!this.winner)
        this.scheduleWaitTime();
    },
    showWaiting: function () {
        var noOfPl = this._serverRoom.getVariable(SFSConstants._NOOFPLAYERS).value;
        if (noOfPl <= 1)
            this._userCardPanel && this._userCardPanel.killDistribution();
        if (noOfPl == 1) {
            this.waitingMC.setString("Waiting for 1 more player(s)...");
            this.waitingMC.setVisible(true);
            if (this._tabMc)
                this._tabMc.subMiniTable.turnIndicator.setVisible(false);
        }
        else
            this.waitingMC.setVisible(false);
    },
    processRoomStatus: function (dataObj, room) {
        if (this._myId == -1)
            this.selfSeatPosition = 0;

        this.result && this.result.removeFromParent(true);
        this.setRoundNo(parseInt(dataObj[SFSConstants.DATA_ROUNDNO]));
        var isMyautoPl = this.parseSeat(dataObj, room);
        if (room.getVariable("roomState").value != SFSConstants.RS_JOIN) {
            if (room.getVariable("dealerId")) {
                var dealerId = room.getVariable("dealerId").value;
                this.updateDealer(dealerId);
            }
        }

        var discardStr = "";
        this._actualTurnTime = dataObj.displayTurnTime;
        this.gameInfoSprite.gameInfopopup.setTurnTime(dataObj.displayTurnTime);

        this._currentRoomState = room.getVariable(SFSConstants._ROOMSTATE).value;

        this.showWaiting();

        /*if (this._currentRoomState == SFSConstants.RS_CUTTHEDECK /!*|| this._currentRoomState == SFSConstants.RS_WATCH*!/ ||
         this._currentRoomState == SFSConstants.RS_MELD)
         this.rotateSeats();*/

        if (this._currentRoomState != SFSConstants.RS_JOIN && this._currentRoomState != SFSConstants.RS_GAMESTART)
            this.rotateSeats();

        if (this._currentRoomState != SFSConstants.RS_JOIN) {
            // this.rotateSeats();
            if (this._tabMc) {
                if (this._myId == -1 && !this._isWaiting) {
                    this._tabMc.setIcon(2);
                }
                else
                    this._tabMc.setIcon(1);
            }
        } else {
            this._tabMc && this._tabMc.setIcon(1);
            if (dataObj.timer && dataObj.plsStr && (dataObj.plsStr.indexOf(",") != -1))
                this.scheduleWaitTime(null, dataObj.timer);
        }

        if (this._serverRoom.getVariable(SFSConstants._DISCARD))
            discardStr = this._serverRoom.getVariable(SFSConstants._DISCARD).value;

        if ((discardStr) && (discardStr != "")) {
            //todo show discard panel
            /*discrdBtn.buttonMode = true;
             handleBtnListener(discrdBtn, true, discardBtnHandler);*/
        }

        if (this._currentRoomState != SFSConstants.RS_GAMESTART) {
            if (this._myId == -1) {
                this._isWatching = true;
                this._leaveAllowed = true;
            }
        }
        if (this._currentRoomState == SFSConstants.RS_GAMESTART) {
            if (this._myId == -1) {
                this._isWatching = true;
                this.waitingMC.setString("Please Wait...");
                this.waitingMC.setVisible(true);
                this._leaveAllowed = true;

                if (this.historyPanel)
                    this.historyPanel._isEnabled = false;
            }
            else {
                this._leaveAllowed = false;
                if (this.historyPanel)
                    this.historyPanel._isEnabled = true;
            }
        }
        if (this._currentRoomState == SFSConstants.RS_CUTTHEDECK) {
            this.deckPanel.initPanel();
            this.deckPanel.hideAllItems(true);
            if (this._myId == -1) {
                this.waitingMC.setString("Please Wait...");
                this.waitingMC.setVisible(true);
                this._leaveAllowed = true;
                if (this.historyPanel)
                    this.historyPanel._isEnabled = false;
                var noOfPl = this._serverRoom.getVariable(SFSConstants._NOOFPLAYERS).value;
                //todo setTimeout(watchPlrTurnHandler, (int(dataObj.getInt("timer")) + noOfPl)*1000);
            }
            else {
                this._leaveAllowed = false;
                this._canLeaveRoom = false;
                if (this.historyPanel)
                    this.historyPanel._isEnabled = true;
            }
        }
        var seatObj;

        if (this._currentRoomState == SFSConstants.RS_MELD) {
            this.initClosedCards();
            this.arrangeClosedCard();
        }

        if (this._serverRoom.getVariable(SFSConstants._TURNUID))
            this._currentTurnID = parseInt(this._serverRoom.getVariable(SFSConstants._TURNUID).value);

        // if (this._currentRoomState == SFSConstants.RS_WATCH || this._currentRoomState == SFSConstants.RS_MELD)
        //     this._currentTurnID = parseInt(this._serverRoom.getVariable(SFSConstants._TURNUID).value);

        if (this._isWatching && (this._currentRoomState == SFSConstants.RS_WATCH || this._currentRoomState == SFSConstants.RS_CUTTHEDECK)) {
            this.waitingMC.setVisible(false);

            this.initClosedCards();
            this.arrangeClosedCard();

            room.getVariable(SFSConstants._OD) && this.ODCards(room.getVariable(SFSConstants._OD).value);
            room.getVariable(SFSConstants._WILDCARD) && this.wildCard(room.getVariable(SFSConstants._WILDCARD).value);

            this.deckPanel.initPanel();
            this.deckPanel.setWildCard();
            this.deckPanel.hideAllItems(false);

            this.instantPlayEnable();
            this._leaveAllowed = true;
            this._hasDistributionComp = true;

        }

        if (this._isWaiting) {
            room.getVariable(SFSConstants._OD) && this.ODCards(room.getVariable(SFSConstants._OD).value);
            room.getVariable(SFSConstants._WILDCARD) && this.wildCard(room.getVariable(SFSConstants._WILDCARD).value);
        }

        if (this._currentRoomState == SFSConstants.RS_WATCH) {

            this._currentTurnID = parseInt(this._serverRoom.getVariable(SFSConstants._TURNUID).value);

            this.instantPlayEnable();
            this._leaveAllowed = true;
            this._hasDistributionComp = true;

            this._userCardPanel.initPanel();

            this.deckPanel.openDeck.setOpenDeckCards();
            this.deckPanel._animComp = true;

            this.handleCurrentTurn(parseInt(dataObj["timer"]));
            seatObj = this.getSeatObjByUser(this._myId);

            this.showValidity && this.showValidity._init();
            if (this._myId == -1 || (seatObj && seatObj._seatPosition != -1 && seatObj.isWrongShow)) {
                this.initClosedCards();
                this.arrangeClosedCard();
                this._userCardPanel.hideAllItems(true);
                this._userCardPanel.clearCards();
                if (this.historyPanel)
                    this.historyPanel._isEnabled = false;
                this.showValidity && this.showValidity.hide(true);
            }
            else {

                if (!this._dropped) {

                    this._userCardPanel.hideAllItems(false);

                    if (this._serverRoom.getVariable(SFSConstants._GAMETYPE).value.indexOf(SFSConstants.GT_BEST3) >= 0) {
                        this._userCardPanel.DropBtn.hide(true);
                    }

                    if (this._serverRoom.getVariable("cashOrPromo").value != "CASH") {
                        this.showValidity && this.showValidity.hide(false);
                        if (this.showValidity) {
                            if (this.totCards == 21)
                                this.showValidity.checkValidity();
                            else
                                this.showValidity.checkValidity();
                        }
                    }
                }
                else {
                    this.showValidity && this.showValidity.hide(true);
                }
                if (this.historyPanel)
                    this.historyPanel._isEnabled = true;
                //todo setting button
            }
            //todo
        }
        if (this._currentRoomState == SFSConstants.RS_MELD) {
            // this.instantPlayEnable();
            this._hasDistributionComp = true;
            this.deckPanel.initPanel();
            this.deckPanel.setWildCard();
            this.showValidity && this.showValidity._init();
            this.showValidity && this.showValidity.hide(true);
            seatObj = this.getSeatObjByUser(this._myId);
            if ((this._myId == -1) || (seatObj && seatObj._seatPosition != -1 && seatObj.isWrongShow)) {
                this.initMessagePop();
                this.messagePopUp.setMsg("Melding in progress!!!\nPlease wait while players are melding... ");
                if (this.historyPanel)
                    this.historyPanel._isEnabled = false;
            }
            else if (this.historyPanel)
                this.historyPanel._isEnabled = true;
        }
        if (isMyautoPl)
            this.enableAutoPlay();

        /*if (!this._isWatching)
         this.disableAllChairs();*/
    },
    watchPlrTurnHandler: function () {
        this._hasDistributionComp = true;
        this.deckPanel.setWildCard();
        this.deckPanel.openDeck.setOpenDeckCards(false);
        this.handleCurrentTurn();
        this.deckPanel.hideAllItems(false);
        this.instantPlayEnable();
    },
    roomVariableUpdate: function (event) {
        var changedVars = event[SFSConstants._CHANGEDVARS];
        var room = event.room;

        if (room != null) {
            if (room.groupId == "gameRoom") {
                for (var s in changedVars) {
                    cc.log("changed var: " + changedVars[s]);
                    if (changedVars[s] == SFSConstants._SESSION_ID) {
                        if (this._serverRoom.getVariable(SFSConstants._SESSION_ID))
                            this.updateHeader();
                    }
                    if (changedVars[s] == SFSConstants._WINNING) {
                        this.gameInfoSprite && this.gameInfoSprite.updatePrize();
                    } else if (changedVars[s] == SFSConstants._WILDCARD) {
                        var wildCardName = room.variables[SFSConstants._WILDCARD] && room.variables[SFSConstants._WILDCARD].value;
                        this.wildCard(wildCardName);
                    } else if (changedVars[s] == SFSConstants._OD) {
                        this.ODCards(room.variables[SFSConstants._OD].value);
                    } else if (changedVars[s] == SFSConstants._TURNUID) {
                        this.stopWaitTimer();
                        this._tabMc.subMiniTable.turnIndicator.setVisible(false);
                        this._tabMc.subMiniTable.extraMC.setVisible(false);
                        this._currentTurnID = parseInt(this._serverRoom.getVariable(SFSConstants._TURNUID).value);
                        if (this._myId != -1 && !this.autoPlayPopup) {
                            this._tabMc.setIcon(1);
                            if (applicationFacade._currScr == this)
                                this._tabMc.startGlow(1);
                            else if (this._currentTurnID == this._myId)
                                this._tabMc.startGlow(3);
                        }
                        if (this._currentTurnSeat) {
                            this._currentTurnSeat.hideTimer();
                        }
                        if (this._connectCount == 0)
                            this.handleCurrentTurn();

                        if (this._connectCount == 1)
                            this._connectCount = 2;

                        //this.handleCurrentTurn(room.variables[SFSConstants._TURNUID].value, (room.variables[SFSConstants._WAITTIME].value).toString(),KHEL_CONSTANTS.TIMER_SHOW_TIME);
                    } else if (changedVars[s] == SFSConstants._DISCARD) {
                        this.updateDiscardHistory();
                    } else if (changedVars[s] == SFSConstants._DEALERID) {
                        this.updateDealer(room.variables[SFSConstants._DEALERID].value);
                    } else if (changedVars[s] == SFSConstants._ROOMSTATE) {
                        cc.log("current state: " + this._currentRoomState);
                        if (changedVars[s] == SFSConstants._NOOFPLAYERS) {
                            this.showWaiting();
                            // this.updateNoOfPlayrs();  TODO
                        }
                        this._currentRoomState = this._serverRoom.getVariable(SFSConstants._ROOMSTATE).value;
                        if (this._currentRoomState == SFSConstants.RS_JOIN) {
                            this._leaveAllowed = true;
                        }
                        if (this._currentRoomState == SFSConstants.RS_GAMESTART) {
                            this.winner && this.winner.removeFromParent(true);
                            this.leaveTablePopup && this.leaveTablePopup.removeFromParent(true);
                            if (this._myId == -1 && !this._isWaiting) {
                                this._leaveAllowed = true;
                                this._tabMc.setIcon(2);
                            }
                            else {
                                this._canLeaveRoom = true;
                                this._leaveAllowed = false;
                            }
                            //todo mainProfileMC.joinVisible(true);
                        }
                        if (this._currentRoomState == SFSConstants.RS_CUTTHEDECK) {
                            //todo this.reArrangePopUpMC.visible = false;
                            if (this._myId == -1 && !this._isWaiting) {
                                this._leaveAllowed = true;
                                this._canLeaveRoom = true;
                            }
                            else {
                                this._leaveAllowed = false;
                                this._canLeaveRoom = true;
                            }
                            this.scheduleWaitTime();
                        }
                        if (this._currentRoomState == SFSConstants.RS_RESULT) {
                            this.stopWaitTimer();
                            if (this._myId != -1)
                                this.showNotification();
                            this._leftPlArr = [];
                            this.scoreBoardPanel && this.scoreBoardPanel.setVisible(false);
                            this._userCardPanel.meldConfirmPopUp && this._userCardPanel.meldConfirmPopUp.removeFromParent(true);
                            this._leaveAllowed = true;
                        }
                        if (this._currentRoomState == SFSConstants.RS_MELD) {
                            this.scheduleWaitTime();
                            this._leaveAllowed = true;
                        }
                        if (this._currentRoomState == SFSConstants.RS_WATCH) {
                            this.messagePopUp && this.messagePopUp.removeFromParent(true);
                            this._userCardPanel.meldConfirmPopUp && this._userCardPanel.meldConfirmPopUp.removeFromParent(true);
                            if (this._leftPlArr) {
                                var leftPlStr = this._leftPlArr.join(",");
                                var gameTypeStr = this._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
                                if ((leftPlStr) && (leftPlStr != "") && gameTypeStr.search("Pool") != -1)
                                    this.showNotification(leftPlStr + " left the table!!");
                                else
                                    this._leftPlArr = [];
                            }
                            if ((this._myId != -1) && (!this._dropped) && this._hasDistributionComp) {
                                this._userCardPanel.hideAllItems(false);
                                this._userCardPanel.meldConfirmPopUp && this._userCardPanel.meldConfirmPopUp.removeFromParent(true);
                                this.deckPanel.hideAllItems(false);
                            }
                            this._leaveAllowed = true;
                        }
                    }
                }
            }
        }
    },
    showNotification: function (msgStr) {
        if (!msgStr)
            return;
        this._super();
        this.notification = new GameNotification(msgStr);
        this.addChild(this.notification, GameConstants.notificationZorder, "notification");
        this.notification.tween();
    },
    handleCurrentTurn: function (turnTime, extraStr) {
        if (extraStr === undefined)
            extraStr = null;

        SoundManager.removeSound(SoundManager.extratimesound);

        this.pickWaitMC.setVisible(false);
        this.deckPanel.disable();
        this._userCardPanel && this._userCardPanel.dropConfirmPopup && this._userCardPanel.dropConfirmPopup.removeFromParent(true);
        this._userCardPanel._isShowReqSent = false;
        this._isExtraTurnTime = false;
        if (this.meldingPanel && !this._dropped) // if wrong show then game will continue between other players
        {
            this.meldingPanel.removeFromParent();
            this._userCardPanel.setVisible(true);
            // this._userCardPanel.dropVisible(); // this line is placed below if it worked then remove this line
            if ((this._myId != -1) && (this._serverRoom.getVariable("cashOrPromo").value != "CASH"))      //((totCards == 21)&&(_serverRoom.getVariable("cashOrPromo") != "CASH"))
            {
                this.showValidity && this.showValidity.hide(false);
                if (this.showValidity) {
                    if (this.totCards == 21)
                        this.showValidity.checkValidity();
                    else
                        this.showValidity.checkValidity();
                }
            }
            this.deckPanel.setVisible(true);
        }
        if (this._hasDistributionComp) {
            this.waitingMC.setVisible(false);
            var seatObj = this.getSeatObjByUser(this._currentTurnID);
            if (seatObj && seatObj._seatPosition != -1) {
                this._currentTurnSeat = seatObj;
                this._tabMc && this._tabMc.subMiniTable.setTurnIndicator(this._serverRoom.getVariable(SFSConstants._MAXPLAYERS).value, this._currentTurnSeat._seatPosition);
                if (this._extraTimeToggle)
                    this.scheduleWaitTime(SFSConstants.ETT_MELDEXTRATIMER, turnTime);
                else
                    this.scheduleWaitTime(extraStr, turnTime);

                if (this._currentTurnID == this._myId) {
                    SoundManager.playSound(SoundManager.myturnsound, false);
                    this.thumbnailBlink();
                    this._userCardPanel.dropConfirmPopup && this._userCardPanel.dropConfirmPopup.message.setString("Are you sure you want to drop?");
                    var card;
                    if (this._currentRoomState != SFSConstants.RS_MELD) {
                        this._userCardPanel.resetFilter();
                    }
                    if (!this._userCardPanel.isMyDrop(1) && !this.autoPlayPopup) {
                        this.deckPanel.enable();
                        this._userCardPanel.dropVisible();
                    }

                    //todo check deck panel enabled condition in case of autoplay and normal game
                    var numOfCard = this._serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;
                    if (parseInt(numOfCard) == 13) {
                        if (this._userCardPanel.cardStack.length != 0 && this._userCardPanel.cardStack.length < 14) {
                            this.deckPanel.enable();
                            this.deckPanel.glowEnabled(true);
                        }
                    } else {
                        if (this._userCardPanel.cardStack.length != 0 && this._userCardPanel.cardStack.length < 22) {
                            this.deckPanel.enable();
                            this.deckPanel.glowEnabled(true);
                        }
                    }
                }
                else {
                    if (this._myId != -1 && !this._isWaiting && !this._isWatching) {
                        this._userCardPanel.dropConfirmPopup && this._userCardPanel.dropConfirmPopup.message.setString("Are you sure you want to drop on your turn?");
                        this._userCardPanel.isMyDrop(2);
                        this._userCardPanel.dropVisible();
                        this.deckPanel.glowEnabled(false);
                    }
                }
                if (this._userCardPanel.showConfirmPopup) {
                    this._userCardPanel.showConfirmPopup.removeFromParent(true);
                    if (!this._userCardPanel.cardStack[this.totCards + 1]) {
                        this._userCardPanel.showBtn.hide(true);
                        this._userCardPanel.discardBtn.hide(true);
                    }
                }
            }
            if (this._dropped) {
                this.deckPanel.hideAllItems(false);
            }
        }
    },
    showWaitTimer: function (remainTime) {
        if (this._stopAllTimer) {
            this.stopWaitTimer();
            return;
        }
        if (this._currentRoomState == SFSConstants.RS_JOIN) {
            if (!this._leaveAllowed)
                return;
            if (this._serverRoom.getVariable(SFSConstants._NOOFPLAYERS).value > 1) {

                if (!this.confirmationPopup) {
                    this.confirmationPopup = new Confirmation(this);
                    this.addChild(this.confirmationPopup, GameConstants.confirmPopupZorder, "confirmationPopup");
                }
                this.confirmationPopup.setVisible(true);
                this.confirmationPopup.setTime(remainTime);
                this.waitingMC.setVisible(false);
            }
            if (remainTime == 0) {
                this.disableLeave();
            } else if ((remainTime - rummyData.removeLeaveTime) <= 0) {
                // this.leaveTablePopUp && this.leaveTablePopUp.hide(true);
                this.confirmationPopup && this.confirmationPopup.leaveTable && this.confirmationPopup.leaveTable.removeFromParent(true);
            }
            else
                this.confirmationPopup && this.confirmationPopup.leaveTable.setVisible(true);
        }
        var groupCards;
        if (this._currentRoomState == SFSConstants.RS_WATCH) {
            if (this._currentTurnSeat)
                this._currentTurnSeat.setTime(remainTime);
            this.setTabTimer(remainTime);
            if ((remainTime == 1 || remainTime == 3) && this._hasDistributionComp && this._userCardPanel._isVisible) {
                if (this._currentTurnID == this._myId && this._myId != -1) {
                    if (!this._userCardPanel._isShowReqSent) {
                        groupCards = this._userCardPanel.getGroupCardsStr();
                        this._userCardPanel.sendGroupCards(groupCards, true);
                    }
                }
            }
        }
        if (this._currentRoomState == SFSConstants.RS_CUTTHEDECK) {
            //todo cutDeckMC.setTime(remainTime);
            // initial timer is set by default in initCutDeckPanel
        }
        if (this._currentRoomState == SFSConstants.RS_MELD) {
            this.confirmationPopup && this.confirmationPopup.removeFromParent(true);
            if (this._userCardPanel._isVisible) {
                if (this._currentTurnSeat)
                    this._currentTurnSeat.setTime(remainTime);
                if (this._tabMc.timerTxt)
                    this._tabMc.timerTxt.setString(remainTime.toString());
                if ((remainTime == 1 || remainTime == 3)) {
                    if (this._myId != -1) {
                        groupCards = this._userCardPanel.getGroupCardsStr();
                        this._userCardPanel.sendGroupCards(groupCards, true);
                    }
                }
                if (remainTime == 0) {
                    if (this._extraTimeToggle) {
                        this.sendMeldCards();
                    }
                }
            }
            else {
                if (this._extraTimeToggle) {
                    if (this.meldingPanel && this.meldingPanel._currentFrame == 5)
                        this.meldingPanel.updateStrTimer(remainTime);
                }
            }
        }
    },
    meldingPanelResp: function () {
        this._userCardPanel.hideAllItems(true);
        this._userCardPanel.clearCards();
        this.cardString = this._userCardPanel.getGroupCardsStr(true);
        this.sendMeldCards();
    },
    sendMeldCards: function () {
        var obj = {};
        obj.cards = this.meldingPanel.getGroupCardsStr(true);
        this.sendReqToServer("meldCards", obj);
        if (this._extraTimeToggle) {
            this.scheduleWaitTime(SFSConstants._MAXEXTRATIME);
        }
        this.meldingPanel.declareBtn.hide(true);
        this.meldingPanel.removeJoker();
        if (this._currentTurnID == this._myId)
            this.meldingPanel.setMeldMsg(3, "Please wait while your opponents are melding.");
        else
            this.meldingPanel.setMeldMsg(3, this.getPlayerNameById(this._currentTurnID) + " has placed a show. Please wait while your opponents are melding.");

        if (this._currentTurnSeat) {
            this._currentTurnSeat.hideTimer();
        }
        this.sendUpdateTab();
    },
    processToss: function (data) {
        this.confirmationPopup && this.removeChild(this.confirmationPopup);
        this._super(data);
    },
    processCutTheDeck: function (data) {
        this.waitingMC.setVisible(false);
        this._super(data);
    },
    handleDistributionCompl: function () {
        this.deckPanel.hideAllItems(false);
        this.arrangeClosedCard();

        if (this._serverRoom.getVariable(SFSConstants._ROOMSTATE).value && (this._serverRoom.getVariable(SFSConstants._ROOMSTATE).value == SFSConstants.RS_RESULT)) {
            this._userCardPanel.clearCards();
            return;
        }
        if (this._userCardPanel._killDistribution) {
            //this._userCardPanel.killDistribution();
            return;
        }
        this._hasDistributionComp = true;

        if (this._myId != -1) {
            if (this._serverRoom.getVariable("cashOrPromo").value != "CASH")            //((totCards==21)&&(_serverRoom.getVariable("cashOrPromo") != "CASH"))
            {
                this.showValidity && this.showValidity.hide(false);
                if (this.showValidity) {
                    if (this.totCards == 21)
                        this.showValidity.checkValidity();
                    else
                        this.showValidity.checkValidity();
                }
            }
            if (this.historyPanel)
                this.historyPanel._isEnabled = true;
        }
        else {
            this.showValidity && this.showValidity.hide(true);
            if (this.historyPanel)
                this.historyPanel._isEnabled = false;
            this._userCardPanel.hideAllItems(true);
        }
        // this.deckPanel.openDeck && this.deckPanel.openDeck.hide(true);
        var ODStr = this._serverRoom.getVariable(SFSConstants.OPEN_DECK_CARD).value;
        var cardsArr = ODStr.split(",");

        if (cardsArr[0] != this.lastPickCard) {
            this.deckPanel.openDeck.setOpenDeckCards();
        }

        this.deckPanel.setVisible(true);
        var obj;
        for (var i = 0; i < this._playerArray.length; ++i) {
            obj = this._playerArray[i];
            if (obj && obj._seatPosition != -1 && !obj._isNext && !obj.isWrongShow) {
                //if (this._myId != obj._playerId )
                //todo show closecard for obj._seatPosition
                //closedCardsMainMC.closedCardsGroupMC.getChildByName("closedCardsMC" + obj._seatPosition).visible = true;
            }
        }
        this.discardButton._isEnabled = true;

        if (this._currentRoomState != SFSConstants.RS_MELD)
            this.handleCurrentTurn();

        if (this._isWatching)
            this.instantPlayEnable();
    },
    processUserCards: function (data, sourceRoom) {
        var gameType = this._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
        if (gameType.indexOf("Point") >= 0 && !this.GameDonotDealBtn.isVisible()) {
            this.GameDonotDealBtn.setNext(true);
            this.GameDonotDealBtn.hide(false);
        }
        this._super(data, sourceRoom);
    },
    updatePlayerInfo: function (userType, player, isDropped, isDisconnected, isAutoPlay, isWrongShow, isWaiting) {
        var sittedNoOfPlayer = this.getCountNoOfPlayers();
        switch (userType) {
            case KHEL_CONSTANTS.USER_ICON_UPDATE :
                if (isDisconnected) {
                    // player.setIcon("Disconnected", "autoPlay");
                    if (parseInt(player._playerId) == this._myId /*&& !this.reconnectBtn.isVisible()*/) {
                        // TODO reconnect button
                        player.setIcon("Disconnected", "WrongShowIcon");
                        // this.autoplayPopup =
                        /*this.dropCheckbox.setVisible(false);
                         this.pauseReconnectButton(false);
                         this.pauseDropButton(true);
                         this.pauseShowButton(true);*/
                    }
                } else if (isAutoPlay) {
                    player.setIcon("Auto Mode", "autoPlay");
                    if (parseInt(player._playerId) == this._myId /*&& !this.reconnectBtn.isVisible()*/) {
                        // TODO reconnect button
                        this.showAutoPlayMessage();
                        // this.autoplayPopup =
                        /*this.dropCheckbox.setVisible(false);
                         this.pauseReconnectButton(false);
                         this.pauseDropButton(true);
                         this.pauseShowButton(true);*/
                    }
                } else if (isWrongShow) {
                    player.setIcon("Wrong Show", "wrongShow");
                    // todo remove closed card
                    if (parseInt(player._playerId) != this._myId)
                        this.removeClosedCardForSpecificPlayer(player);
                    /* if (parseInt(player._playerId) == rummyData.playerId) {
                     this.isWrongShow = true;
                     // simple label , not toast
                     this.waitingMsg.setString("You have placed wrong show");
                     }
                     waitMsgText = true;*/
                } else if (isDropped) {
                    player.setIcon("Dropped", "DropIcon");
                    /* if (parseInt(player._playerId) == rummyData.playerId) {
                     this.isDropGame = true;
                     }
                     waitMsgText = true;*/
                } else if (isWaiting) {
                    player.setIcon("Waiting", "WaitingIcon");
                    /*if (parseInt(player._playerId) == rummyData.playerId) {
                     this.waitingMsg.setString("Waiting for 1 more player(s)...");
                     }
                     waitMsgText = true;*/
                } else {
                    player.removeIcon();
                }
                break;
            /*case KHEL_CONSTANTS.USER_JOINED :
             var amt = "";
             amt = parseInt(ptsOrAmt);
             this.currentPlayerTurn.setPlayerName(playerName + "     " + amt);
             if (isDisconnected) {
             this.currentPlayerTurn.setDisconnectedMode(true);
             } else if (isAutoPlay) {
             this.currentPlayerTurn.setAutoMode(true);
             if (parseInt(playerID) == this.getPlayerId() && !this.reconnectBtn.isVisible()) {
             this.pauseReconnectButton(false);
             this.dropCheckbox.setVisible(false);

             this.pauseDropButton(true);
             this.pauseShowButton(true);
             }
             } else if (isWrongShow) {
             this.currentPlayerTurn.setWrongShowMode(true);
             if (parseInt(playerID) == this.getPlayerId()) {
             this.isWrongShow = true;
             // simple label , not toast
             this.dropCheckbox.setVisible(false);
             this.pauseDropButton(true);
             this.waitingMsg.setString("You have placed wrong show");
             waitMsgText = true;
             }
             } else if (isDropped) {
             this.currentPlayerTurn.setPlayerDropped(true);
             if (parseInt(playerID) == this.getPlayerId()) {
             this.isDropGame = true;
             }
             waitMsgText = true;
             } else if (isWaiting) {
             this.currentPlayerTurn.setWaitingMode(true);
             if (parseInt(playerID) == this.getPlayerId()) {
             this.waitingMsg.setString("Waiting for 1 more player(s)...");
             }
             waitMsgText = true;
             } else {
             this.currentPlayerTurn.resetAllMode();
             }
             break;*/
            case KHEL_CONSTANTS.USER_LEFT:
                if (sittedNoOfPlayer == 1) {
                    this.confirmationPopup && this.confirmationPopup._leaveTable();
                }
                break;
        }

        if (this._myId != -1) {
            this.disableAllChairs();
        }

        // for showing waiting for more players
        if (sittedNoOfPlayer == 1) {
            this.waitingMC.setString("Waiting for 1 more player(s)...");
            this.waitingMC.setVisible(true);
            if (this._tabMc) {
                this._tabMc.subMiniTable.turnIndicator.setVisible(false);
                this._tabMc.subMiniTable._timeTF.setString("");
            }
        } else {
            if (this.waitingMC) {
                this.waitingMC.setString("");
                this.waitingMC.setVisible(false);
            }
            /* if (this._isWatching && this._myId == -1 && this.isCutTheDeckOver) {
             if ((this.gameType.split("-")[0].indexOf("point") >= 0) && sfsClient.mSeatedPLayers.length != 6) {
             this.waitingMsg.setString("You are currently watching this game.\n To play a game, please join the seat.");
             } else {
             this.waitingMsg.setString("You are currently watching this game.\n To play a game, please visit the lobby.");
             }
             this.waitingMsg.setVisible(true);
             }
             else if (!waitMsgText && this.waitingMsg.getString() == "Waiting for 1 more player(s)...") {
             this.waitingMsg.setVisible(false);
             }*/
        }
    },
    getCountNoOfPlayers: function () {
        var count = 0;
        for (var i = 0; i < this._playerArray.length; i++) {
            if (this._playerArray[i]._sittingStatus == 1)
                count++;
        }
        return count;
    },
    waitingMsgLabel: function (msg, pos) {
        var waitingMsg = new cc.LabelTTF(msg, "RobotoRegular", 22 * 2 * scaleFactor);
        waitingMsg.setScale(0.5);
        waitingMsg.setColor(cc.color(140, 160, 141));
        waitingMsg.setPosition(pos.x, pos.y - yMargin);
        return waitingMsg;
    },
    handleResult: function (dataObj) {
        SoundManager.removeSound(SoundManager.extratimesound);
        this.showConfirmPopup && this.showConfirmPopup.removeFromParent(true);
        this.result && this.result.removeFromParent(true);
        this._isWaiting = false;
        this.showValidity && this.showValidity.hide(true);
        this.WrongShowMsg && this.WrongShowMsg.removeWrongShowLayer();
        this.deckPanel.wildcard.removeToolTip();
        this._userCardPanel.meldConfirmPopUp && this._userCardPanel.meldConfirmPopUp.removeFromParent(true);
        this.settingPanel.isVisible() && this.settingPanel.setVisible(false);

        this.isDropped = false;
        this.isautoPlay = false;
        this.isWrongShow = false;
        this.isDisconnected = false;

        for (var i = 0; i < this._playerArray.length; i++) {
            this._playerArray[i].clearAllState();
        }

        if (this.historyPanel) {
            this.historyPanel._historyCards = "";
            this.historyPanel.removeFromParent(true);
            this.historyPanel = null;
        }

        this.showResult(dataObj);
    },
    showResult: function (param) {
        this.discardButton.isPressed(false);
        this.discardPanel && this.discardPanel.hideDiscardPanel();

        //todo this.scorePanel.visible = false;

        this.stopWaitTimer();
        this.sendUpdateTab();

        /*todo toolTip.visible = false;
         toolTip.txt.text = "";*/

        this._isShowDistribution = true;
        this.messagePopUp && this.messagePopUp.removeFromParent(true);
        this.meldingPanel && this.meldingPanel.removeFromParent(true);

        this.deckPanel.hideAllItems(true);
        this._userCardPanel.clearCards();

        //todo dropPopUp.visible = false;

        this.lastPickCard = "";
        if (this.historyPanel) {
            this.historyPanel._historyCards = "";
            this.historyPanel.removeFromParent(true);
            this.historyPanel = null;
        }
        this.scoreBoardPanel && this.scoreBoardPanel.setVisible(false);

        this.setDealerpos(false);
        for (var i = 0; i < this._playerArray.length; i++) {
            this._playerArray[i].hideTimer();
        }
        for (var i = 0; i < this._playerArray.length; i++) {
            this._playerArray[i].removeIcon();
        }

        this._currentTurnSeat = null;

        this.autoPlayPopup && this.autoPlayPopup.removeFromParent(true);
        var numOfCard = this._serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;

        this.isSeatRotated = false;

        if (parseInt(numOfCard) == 13) {
            this.result = new Result_13(param, this);
            this.addChild(this.result, GameConstants.gameResultZorder, "result");
        } else {
            this.result = new Result_21(param, this);
            if (this._wildCardStr = this._serverRoom.getVariable(SFSConstants._WILDCARD))
                this._wildCardStr = this._serverRoom.getVariable(SFSConstants._WILDCARD).value;
            this.result.setWildCard(this._wildCardStr);
            this.addChild(this.result, GameConstants.gameResultZorder, "result");
        }
        this.removeClosedCard();
    },
    showLastResult: function (param) {
        if (!param.rndId)
            param.rndId = this._currentRound;
        var numOfCard = this._serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;
        if (parseInt(numOfCard) == 13) {
            this.result = new Result_13(param, this, true);
            this.result.initCloseBtn();
            this.addChild(this.result, GameConstants.gameResultZorder, "result");
        } else {
            this.result = new Result_21(param, this, true);
            this.result.initCloseBtn();
            this.addChild(this.result, GameConstants.gameResultZorder, "result");
        }
        this.result.setWildCard(param.wildCard);
        this.settingPanel.setVisible(false);
    },
    processBonusToCash: function (amount) {
        if (!this.bonusToCashPopup) {
            this.bonusToCashPopup = new BonusToCash(amount, this);
            this.addChild(this.bonusToCashPopup, 30, "bonusToCashPopup");
            cc.eventManager.addListener(this.baseGameTouchListener.clone(), this.bonusToCashPopup.bonusCloseBtn);
        }
    },
    processGameOver: function (params) {
        SoundManager.removeSound(SoundManager.extratimesound);
        this._currentRound = 0;
        this.updateHeader();
        this.stopWaitTimer();
        this.initPopup();
        this._userCardPanel.hideAllItems(true);
        this._userCardPanel.clearCards();

        this._isWatching = true;

        this.winner = new Winner(params, this);
        this.addChild(this.winner, GameConstants.winnerScreenZorder, "winner");

        this._myId = -1;

        for (var i = 0; i < this._playerArray.length; i++) {
            cc.eventManager.removeListener(this._playerArray[i], true);
            this._playerArray[i].removeFromParent(true);
        }
        this._playerArray = [];
        this.initRoom();

    },
    enableWinnerBtnListener: function () {

        //SoundManager.callBack.playSound("WinnerSound", false, -1, 0.2);
        //SoundManager.callBack.removeSound("validshow");
        //SoundManager.callBack.removeSound("validshow2");
        //SoundManager.callBack.removeSound("wrongshow");
        //SoundManager.callBack.removeSound("wrongshow2");

        SoundManager.stopAllEffects();

        this.winner.visible = true;
        this.winner.yesButton.setVisible(true);
        this.winner.noButton.setVisible(true);

        if (_myId == -1) {
            this.winner.yesBtn.visible = false;
            this.winner.noBtn.visible = false;
            this.winner.txt.visible = false;
        }
        else {
            // SoundManager.callBack.playSound("finalwinningifme", false, -1, 0.2);
        }

    },
    sendGameFinishEvent: function (rejoinAmt, _count, _winning, duration) {
        cc.log("Sending game finish", this._actualTurnTime);

        var roomType = String(this._serverRoom.getVariable(SFSConstants.FLD_GAMETYPE).value);
        var _gameType = roomType.split("-")[1];
        cc.log(_gameType);

        if (_gameType.indexOf("101") != -1 || _gameType.indexOf("201") != -1)
            _gameType = "Pool";
        else if (_gameType.indexOf("Best of") != -1)
            _gameType = "Deals";

        /*GameMixPanel.getMixPanel().trackGameFinish({
         category: this._serverRoom.getVariable(Command.CASH_OR_PROMO).value,
         gameType: this._serverRoom.getVariable(Command.NUM_OF_CARDS).value + " Card " + _gameType,
         gameSubType: roomType.split("-")[1],
         tableDenomination: this._serverRoom.getVariable(Command.ENTRY_FEE).value,
         maxPlayer: this._serverRoom.getVariable(Command.MAX_PLAYERS).value,
         actualPlayer: this._serverRoom.getVariable(Command.NO_OF_PLS).value,
         turnTime: this._actualTurnTime,//this._serverRoom.getVariable(Command.TURN_TIME),
         wager: this._serverRoom.getVariable(Command.ENTRY_FEE).value,
         winning: _winning,
         rejoinAmt: rejoinAmt,
         rejoinCount: _count,
         playerId: this._myId + '',

         email: String(Main.getInstance()._flashVarsXml.email),//(Main.getInstance()._flashVarsXml.email?Main.getInstance()._flashVarsXml.email:""),
         mobile: String(Main.getInstance()._flashVarsXml.mobile),//(Main.getInstance()._flashVarsXml.mobile?Main.getInstance()._flashVarsXml.mobile:""),
         firstName: Main.getInstance()._flashVarsXml.firstName,//(Main.getInstance()._flashVarsXml.firstName?Main.getInstance()._flashVarsXml.firstName:""),
         lastName: Main.getInstance()._flashVarsXml.lastName,//(Main.getInstance()._flashVarsXml.lastName?Main.getInstance()._flashVarsXml.lastName:""),
         gender: Main.getInstance()._flashVarsXml.gender,//(Main.getInstance()._flashVarsXml.gender?Main.getInstance()._flashVarsXml.gender:""),
         dob: String(Main.getInstance()._flashVarsXml.DOB),//?Main.getInstance()._flashVarsXml.DOB:""),
         regDate: String(Main.getInstance()._flashVarsXml.regDate),//?Main.getInstance()._flashVarsXml.regDate:""),
         duration: duration / 1000,
         joinType: this._joinType,

         deviceType: "PC",
         osType: "NA",
         clientType: "Full-Web"
         });*/
    },
    processPlayerStrResp: function (params) {
        this.ScoreBoardPlayers = [];
        if (params.tot == 0) {
            return;
        }
        for (var p = 1; p <= params.tot; p++) {
            this.ScoreBoardPlayers.push(params["pl" + p]);
        }
        //todo show scoreboard
    },
    leaveRoomByMyself: function () {

        this._requestToSever = sfsClient.roomLeave(this._serverRoom);
        SoundManager.removeSound(SoundManager.extratimesound);

        this.stopWaitTimer();
        this.result && this.result.leaveRoom();

        this._stopAllTimer = true;
        var wagerAmt = "";

        var gameTypeStr = this._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
        var gameType = gameTypeStr.split("-")[0];
        if (gameType == "Pool")
            wagerAmt = this._serverRoom.getVariable(SFSConstants._ENTRYFEE).value;
        else
            wagerAmt = this._serverRoom.getVariable(SFSConstants._POINTVALUE).value + "";

        cc.log("In join leave:" + wagerAmt);

        if ((applicationFacade._mixpanelToggle) && (this._myId != -1)
            && (this._currentRoomState == SFSConstants.RS_JOIN) && (!this.result)) {

            cc.log("sending join leave event");
            var roomType = this._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
            var _gameType = roomType.split("-")[1];

            cc.log(_gameType);

            if (_gameType.indexOf("101") != -1 || _gameType.indexOf("201") != -1)
                _gameType = "Pool";
            else if (_gameType.indexOf("Best of") != -1)
                _gameType = "Deals";
            else if (roomType.split("-")[0].indexOf("Point") != -1)
                _gameType = "Points";

            cc.log(_gameType);
            // todo mix panel work

        }
        if (this._serverRoom.id == sfsClient.currentUser.id && this.tableId == applicationFacade._activeTableId) {
            if (applicationFacade._gameMap[this.tableId]) {
                this.tableId && applicationFacade._controlAndDisplay.removeRoomTab(this.tableId);
            }
        }
    }
});

var AutoPlayLayer = cc.Layer.extend({
    _gameRoom: null,
    ctor: function (playerInfo, tossWinnerPlayerId, context) {
        this._super();
        this._gameRoom = context;
        var tossWinnerPlayerName, tossMsg;
        for (var key in playerInfo) {
            var tossCardSprite = new CardModel(playerInfo[key]["tossCard"], false);
            tossCardSprite.setScale(0.7);
            var pos = this._gameRoom._tossCardPosition[this._gameRoom.noOfPlayer][playerInfo[key]["seatPosition"] - 1];
            tossCardSprite.setPosition(pos);
            this.addChild(tossCardSprite, GameConstants.tossZorder);
        }

        var layout = cc.LayerGradient.extend({
            ctor: function () {
                this._super(cc.color(0, 0, 0), cc.color(0, 0, 0));
                var size = cc.winSize;
                this.setPosition(cc.p(90 * scaleFactor, 290 * scaleFactor));
                this.setContentSize(cc.size(800 * scaleFactor, 80 * scaleFactor));
                this.setColorStops([{p: 0, color: new cc.Color(0, 0, 0, 0)},
                    {p: .5, color: cc.color.BLACK},
                    {p: 1, color: new cc.Color(0, 0, 0, 0)}]);
                //this.setStartOpacity(255);
                //this.setEndOpacity(0);
                this.setVector(cc.p(-1, 0));
            }
        });
        var ll = new layout();
        this.addChild(ll, 10);


        /* var layout = cc.LayerGradient.extend({
         ctor: function () {
         this._super(cc.color(0, 0, 0), cc.color(0, 0, 0));
         var size = cc.winSize;
         this.setPosition(cc.p(478, 280));
         this.setContentSize(cc.size(420, 90));
         this.setStartOpacity(0);
         this.setEndOpacity(255);
         this.setVector(cc.p(-1, 0));
         }
         });
         var ll2 = new layout();
         this.addChild(ll2, 10);
         */

        var label1 = new cc.LabelTTF("Rearranging seats please wait.", "RobotoRegular", 18 * scaleFactor);
        label1.setPosition(cc.p(this.width / 2 - 15 * scaleFactor, 340 * scaleFactor - yMargin));
        this.addChild(label1, 12);

        var label2 = new cc.LabelTTF(" You will be seated in centre bottom seat.", "RobotoRegular", 18 * scaleFactor);
        label2.setPosition(cc.p(this.width / 2 - 15 * scaleFactor, 320 * scaleFactor - yMargin));
        this.addChild(label2, 12);

        if (tossWinnerPlayerId == rummyData.playerId) {
            tossMsg = "You win the toss for seat, and will play the first turn."
        } else {
            for (var k = 0; k <= this._gameRoom._playerArray.length; k++) {
                if (this._gameRoom._playerArray[k]._playerId == tossWinnerPlayerId) {
                    tossWinnerPlayerName = this._gameRoom._playerArray[k]._playerName;
                    break;
                }
            }
            tossMsg = tossWinnerPlayerName + ", wins the toss for seat, and will play the first turn."
        }
        var tossMsgLabel = new cc.LabelTTF(tossMsg, "RobotoRegular", 18 * scaleFactor);
        tossMsgLabel.setPosition(cc.p(this.width / 2 - 15 * scaleFactor, 261 * scaleFactor - yMargin));
        this.addChild(tossMsgLabel, 12);

        return true;
    },
    removeTossLayer: function () {
        this.removeFromParent(true);
    }
});