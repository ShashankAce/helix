/**
 * Created by stpl on 9/19/2016.
 */

var KPR = {
    getGameServer: function () {
        return KPR.GAME_BASE_URL.split("/")[2];
    },
    getFlurryKeyFull: function () {
        return KPR.isDebug ? "6WCTQDXMC422B8KNZHPY" : "GN5P69ZN795HRFG86GSR";
    },
    getFlurryKeyLite: function () {
        return KPR.isDebug ? "F5GNY6GHKP6Z9HXFTGH5" : "23RXW57K2N57Z8THFDVV";
    },
    getAppsFlyerKey: function () {
        return KPR.isDebug ? "p6q2EF3Lwvvz9PmkAyihAJ" : "p6q2EF3Lwvvz9PmkAyihAJ";
    },
    // For beta and test server use "b781cf7dd3a24caddf3781155c37fa16" as Mix panel key
    //MIXPANEL_KEY = "d23286a1ed64b079b76ae13fc17b9892";

    //For production server use "b911be7347d9d95090243f3fe1005ced" as Mix panel key
    //MIXPANEL_KEY = "b911be7347d9d95090243f3fe1005ced";
    getMixPanelKey: function () {
        return KPR.isDebug ? "448ce443591656b02821b81108ce204b" : "b911be7347d9d95090243f3fe1005ced";
    }
};
KPR.isDebug = true;
KPR.GAME_STATE = {LOBBY: 0, ROOM: 1};
KPR.isFullVersion = true;

// GAME_SERVER_URL = "https=//beta.khelplayrummy.com/";
// ProductionBuild = "https://www.khelplayrummy.com/" ;
// TestBuild = "http://test.winweaver.com/"

KPR.GAME_BASE_URL = KPR.isDebug ? "http://test.winweaver.com/" : "https://www.khelplayrummy.com/";
// KPR.GAME_BASE_URL = KPR.isDebug ? "https://test.khelplay.com/" : "https://www.khelplayrummy.com/";
// KPR.GAME_BASE_URL = KPR.isDebug ? "https://192.168.134.28/" : "https://www.khelplayrummy.com/";

// domain name parameter
KPR.GAME_SERVER_URL = "test.grouprummy.com" || KPR.getGameServer();
// KPR.GAME_SERVER_URL = "test.khelplay.com" || KPR.getGameServer();
// KPR.GAME_SERVER_URL = "192.168.134.28" || KPR.getGameServer();

KPR.isFlurryEnabled = !KPR.isDebug;
KPR.isAppsFlyerEnabled = !KPR.isDebug;
KPR.isMixPanelEnabled = !KPR.isDebug;

KPR.secureKey = "25d55ad283aa400af464c76d713c07ad";
KPR.merchantKey = 4;
KPR.GAME_ENGINE_URL = KPR.GAME_BASE_URL + "RummyGameEngine/welcomeMobilePlayer.action?merchantKey=4&secureKey=25d55ad283aa400af464c76d713c07ad&device=MOBILE&token=";
KPR.GAME_ENGINE_SUB_URL = "welcomeMobilePlayer.action";
KPR.SAVE_NICKNAME_URL = "saveNickName.action";
//KPR.GAME_ENGINE_SUB_URL = "welcomeMobilePlayer.action?merchantKey=4&" + "secureKey="+KPR.secureKey+"&device=MOBILE&token=";
KPR.REST_UPLOAD_AVATAR = KPR.GAME_BASE_URL + "Weaver/service/mobile/rest/uploadAvatar";
KPR.REST_REPORT_BUG = KPR.GAME_BASE_URL + "RummyGameEngine/ImageUploadServlet";
KPR.REST_PROOF_UPLOAD = KPR.GAME_BASE_URL + "Weaver/service/mobile/rest/uploadPlayerDocument";
KPR.WIRE_TRANSFER_URL = "https://content3.khelplayrummy.com/PortalContent/portalContent/test.khelplayrummy.com/default/pc/LR/mobile_app/bank-detail.html";



