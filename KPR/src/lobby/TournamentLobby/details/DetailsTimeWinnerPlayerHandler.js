/**
 * Created by stpl on 04-Apr-17.
 */
var DetailsTimeWinnerPlayerHandler = Popup.extend({
    ctor: function (room, context) {
        this._super(new cc.Size(405 * scaleFactor, 330 * scaleFactor), context, true);

        this.headerLabel.setString("Winners");

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        this.closeBtn = new cc.Sprite(spriteFrameCache.getSpriteFrame("ResultClose.png"));
        this.closeBtn.setPosition(cc.p(this.width, this.height));
        this.addChild(this.closeBtn, 10, "closeBtn");
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.closeBtn);

        var node = new cc.DrawNode();
        node.drawRect(cc.p(0, 265 * scaleFactor), cc.p(this.width, 295 * scaleFactor), cc.color(222, 222, 222), 1, cc.color(222, 222, 222));
        this.addChild(node, 1);

        this.listViewHeader = new cc.LabelTTF("Rank", "RobotoBold", 13 * 2);
        this.listViewHeader.setScale(0.5);
        this.listViewHeader.setPosition(cc.p(24 * scaleFactor, 280 * scaleFactor));
        this.listViewHeader.setColor(cc.color(0, 0, 0));
        this.addChild(this.listViewHeader, 2);

        var verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(60 * scaleFactor, 265 * scaleFactor), cc.p(60 * scaleFactor, 295 * scaleFactor), 0.5, cc.color(240, 240, 240));
        this.addChild(verticalLine, 2);

        var label = new cc.LabelTTF("Player", "RobotoBold", 18);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(70 * scaleFactor, 280 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label, 2);

        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(140 * scaleFactor, 265 * scaleFactor), cc.p(140 * scaleFactor, 295 * scaleFactor), 0.5, cc.color(240, 240, 240));
        this.addChild(verticalLine, 2);

        label = new cc.LabelTTF("Chips", "RobotoBold", 18);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(150 * scaleFactor, 280 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label, 2);

        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(210 * scaleFactor, 265 * scaleFactor), cc.p(210 * scaleFactor, 295 * scaleFactor), 0.5, cc.color(240, 240, 240));
        this.addChild(verticalLine, 2);

        label = new cc.LabelTTF("Prize", "RobotoBold", 18);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(220 * scaleFactor, 280 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label, 2);

        this.listView = new ccui.ListView();
        this.listView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        this.listView.setGravity(ccui.ListView.GRAVITY_CENTER_HORIZONTAL);
        this.listView.setScrollBarAutoHideEnabled(true);
        this.listView.setTouchEnabled(true);
        //this.listView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        //this.listView.setBackGroundColor(cc.color(255, 255, 0));
        this.listView.setContentSize(cc.size(this.width, 265 * scaleFactor));
        this.listView.setPosition(cc.p(0, 0));
        this.addChild(this.listView, 2);

    },
    onTouchEndedCallBack: function (touch, event) {
        this._super(touch, event);
        var target = event.getCurrentTarget();
        target.getName() == "closeBtn" && this.removeFromParent(true);
    },
    /**
     * @param {String} rank
     * @param {String} plr
     * @param {String} chips
     * @param {String} prize
     * @param {String} index
     * */
    getRow: function (rank, plr, chips, prize, index) {
        var row = new ccui.Layout();
        row.setContentSize(cc.size(this.width, 30 * scaleFactor));
        row.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        if (index % 2 == 1)
            row.setBackGroundColor(cc.color(222, 222, 222));
        else row.setBackGroundColor(cc.color(255, 255, 255));

        var text = new cc.LabelTTF(rank, "RobotoRegular", 13 * 2 * scaleFactor);
        text.setScale(0.5);
        text.setAnchorPoint(0, 0.5);
        text.setPosition(cc.p(15 * scaleFactor, 15 * scaleFactor));
        text.setColor(cc.color(0, 0, 0));
        row.addChild(text, 2);

        var verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(60 * scaleFactor, 5 * scaleFactor), cc.p(60 * scaleFactor, 35 * scaleFactor), 0.5, cc.color(240, 240, 240));
        row.addChild(verticalLine, 2);

        if (plr.length > 10) {
            plr = plr.substring(0, 7);
        }
        text = new cc.LabelTTF(plr, "RobotoRegular", 13 * 2 * scaleFactor);
        text.setScale(0.5);
        text.setAnchorPoint(0, 0.5);
        text.setPosition(cc.p(70 * scaleFactor, 15 * scaleFactor));
        text.setColor(cc.color(0, 0, 0));
        row.addChild(text, 2);

        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(140 * scaleFactor, 5 * scaleFactor), cc.p(140 * scaleFactor, 35 * scaleFactor), 0.5, cc.color(240, 240, 240));
        row.addChild(verticalLine, 2);

        text = new cc.LabelTTF(chips, "RobotoRegular", 13 * 2 * scaleFactor);
        text.setScale(0.5);
        text.setAnchorPoint(0, 0.5);
        text.setPosition(cc.p(150 * scaleFactor, 15 * scaleFactor));
        text.setColor(cc.color(0, 0, 0));
        row.addChild(text, 2);

        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(210 * scaleFactor, 5 * scaleFactor), cc.p(210 * scaleFactor, 35 * scaleFactor), 0.5, cc.color(240, 240, 240));
        row.addChild(verticalLine, 2);

        text = new cc.LabelTTF(prize, "RupeeFordian", 13 * 2);
        text.setScale(0.5);
        text.setAnchorPoint(0, 0.5);
        text.setPosition(cc.p(220 * scaleFactor, 15 * scaleFactor));
        text.setColor(cc.color(0, 0, 0));
        row.addChild(text, 2);

        return row;
    },
    setRow: function (rank, str, chips, prize, index) {
        this.listView.pushBackCustomItem(this.getRow(rank, str, chips, prize, index));
    }
});