/**
 * Created by stpl on 04-Apr-17.
 */
var DetailsWinnerPlayerHandler = Popup.extend({
    ctor: function (room, context) {
        this._super(new cc.Size(305 * scaleFactor, 330 * scaleFactor), context, true);

        this.headerLabel.setString("Winners");

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        this.closeBtn = new cc.Sprite(spriteFrameCache.getSpriteFrame("ResultClose.png"));
        this.closeBtn.setPosition(cc.p(this.width, this.height));
        this.addChild(this.closeBtn, 10, "closeBtn");
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.closeBtn);

        var node = new cc.DrawNode();
        node.drawRect(cc.p(0, 265 * scaleFactor), cc.p(this.width, 295 * scaleFactor), cc.color(222, 222, 222), 1, cc.color(222, 222, 222));
        this.addChild(node, 1);

        this.listViewHeader = new cc.LabelTTF("Player", "RobotoBold", 13 * 2 * scaleFactor);
        this.listViewHeader.setScale(0.5);
        this.listViewHeader.setPosition(cc.p(34 * scaleFactor, 280 * scaleFactor));
        this.listViewHeader.setColor(cc.color(0, 0, 0));
        this.addChild(this.listViewHeader, 2);

        var verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(100 * scaleFactor, 265 * scaleFactor), cc.p(100 * scaleFactor, 295 * scaleFactor), 0.5, cc.color(240, 240, 240));
        this.addChild(verticalLine, 2);

        var label = new cc.LabelTTF("Prize", "RobotoBold", 18 * scaleFactor);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(120 * scaleFactor, 280 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label, 2);

        this.listView = new ccui.ListView();
        this.listView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        this.listView.setGravity(ccui.ListView.GRAVITY_CENTER_HORIZONTAL);
        this.listView.setScrollBarAutoHideEnabled(false);
        this.listView.setTouchEnabled(true);
        //this.listView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        //this.listView.setBackGroundColor(cc.color(255, 255, 0));
        this.listView.setContentSize(cc.size(this.width, 265 * scaleFactor));
        this.listView.setPosition(cc.p(0, 0));
        this.addChild(this.listView, 2);

    },
    onTouchEndedCallBack: function (touch, event) {
        this._super(touch, event);
        var target = event.getCurrentTarget();
        target.getName() == "closeBtn" && this.removeFromParent(true);
    },
    getRow: function (str, prize, index) {
        var row = new ccui.Layout();
        row.setContentSize(cc.size(this.width, 30 * scaleFactor));
        row.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        if (index % 2 == 1)
            row.setBackGroundColor(cc.color(222, 222, 222));
        else row.setBackGroundColor(cc.color(255, 255, 255));

        if (str.length > 10) {
            str = str.substring(0, 7);
        }

        var text = new cc.LabelTTF(str, "RobotoRegular", 13 * 2 * scaleFactor);
        text.setScale(0.5);
        text.setAnchorPoint(0, 0.5);
        text.setPosition(cc.p(15 * scaleFactor, 15 * scaleFactor));
        text.setColor(cc.color(0, 0, 0));
        row.addChild(text, 2);

        var verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(100 * scaleFactor, 5 * scaleFactor), cc.p(100 * scaleFactor, 35 * scaleFactor), 0.5, cc.color(240, 240, 240));
        row.addChild(verticalLine, 2);

        text = new cc.LabelTTF(prize, "RupeeFordian", 13 * 2 * scaleFactor);
        text.setScale(0.5);
        text.setAnchorPoint(0, 0.5);
        text.setPosition(cc.p(120 * scaleFactor, 15 * scaleFactor));
        text.setColor(cc.color(0, 0, 0));
        row.addChild(text, 2);

        return row;
    },
    setRow: function (str, prize, index) {
        this.listView.pushBackCustomItem(this.getRow(str, prize, index));
    }
});