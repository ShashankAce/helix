/**
 * Created by stpl on 1/12/2017.
 */
var TournamentDetail = Popup.extend({
    _name: "details",
    _parent: null,
    timeUpdaterInterval: null,
    ctor: function (context, room, roomBar) {
        this._super(new cc.Size(960 * scaleFactor, 550 * scaleFactor), context, true);

        this._myTournament = roomBar._myTournament;
        // this._parent = context;
        this.timeUpdaterInterval = 0;

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        this.headerLeft.setContentSize(cc.size(this.width * scaleFactor, 50 * scaleFactor));
        this.headerLeft.setPosition(cc.p(this.width / 2, this.height - this.headerLeft.height / 2 + 1 * scaleFactor));
        this.headerLabel.setString("Welcome Tournament");
        this.headerLabel.setFontSize(20 * 2 * scaleFactor);
        this.headerLabel.setPosition(cc.p(20 * scaleFactor, this.height - this.headerLeft.height / 2));

        this.closeBtn = new cc.Sprite(spriteFrameCache.getSpriteFrame("ResultClose.png"));
        this.closeBtn.setName("closeBtn");
        this.closeBtn.setPosition(this.width, this.height);
        this.addChild(this.closeBtn, 3);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.closeBtn);

        var tournamentIdHeader = new cc.LabelTTF("Tournament ID", "RobotoRegular", 18 * scaleFactor);
        tournamentIdHeader.setScale(0.7);
        tournamentIdHeader.setPosition(cc.p(450 * scaleFactor, 535 * scaleFactor));
        tournamentIdHeader.setColor(cc.color(127, 152, 166));
        this.addChild(tournamentIdHeader, 3);

        this.tournamentId = new cc.LabelTTF("15704", "RobotoRegular", 20 * scaleFactor);
        this.tournamentId.setScale(0.7);
        this.tournamentId.setPosition(cc.p(450 * scaleFactor, 518 * scaleFactor));
        this.addChild(this.tournamentId, 3);

        var entryFeeHeader = new cc.LabelTTF("Buy-In", "RobotoRegular", 18 * scaleFactor);
        entryFeeHeader.setScale(0.7);
        entryFeeHeader.setPosition(cc.p(555 * scaleFactor, 535 * scaleFactor));
        entryFeeHeader.setColor(cc.color(127, 152, 166));
        this.addChild(entryFeeHeader, 3);

        this.entryFee = new cc.LabelTTF("Free", "RupeeFordian", 20 * scaleFactor);
        this.entryFee.setScale(0.7);
        this.entryFee.setPosition(cc.p(555 * scaleFactor, 518 * scaleFactor));
        this.addChild(this.entryFee, 3);

        var statusHeader = new cc.LabelTTF("Status", "RobotoRegular", 18 * scaleFactor);
        statusHeader.setScale(0.7);
        statusHeader.setPosition(cc.p(650 * scaleFactor, 535 * scaleFactor));
        statusHeader.setColor(cc.color(127, 152, 166));
        this.addChild(statusHeader, 3);

        this.status = new cc.LabelTTF("Registering", "RobotoRegular", 20 * scaleFactor);
        this.status.setScale(0.7);
        this.status.setPosition(cc.p(650 * scaleFactor, 518 * scaleFactor));
        this.addChild(this.status, 3);

        this.statusSym = new TournamentDetailStatusSymbol(this);
        this.statusSym.setPosition(cc.p(875 * scaleFactor, 526 * scaleFactor));
        this.addChild(this.statusSym, 2);

        var joinedHeader = new cc.LabelTTF("Joined", "RobotoRegular", 18 * scaleFactor);
        joinedHeader.setScale(0.7);
        joinedHeader.setPosition(cc.p(754 * scaleFactor, 535 * scaleFactor));
        joinedHeader.setColor(cc.color(127, 152, 166));
        this.addChild(joinedHeader, 3);
        this.joined = new cc.LabelTTF("0/8", "RobotoRegular", 20 * scaleFactor);
        this.joined.setScale(0.7);
        this.joined.setPosition(cc.p(754 * scaleFactor, 518 * scaleFactor));
        this.addChild(this.joined, 3);

        //labelName, spriteObj, rect
        var whiteBox = spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png");
        whiteBox = whiteBox.getRect();
        this.tournamentStructure = new DetailTabs("Tournament Structure", {
            normal: "StructureIconOver.png",
            hover: "StructureIcon.png"
        }, cc.rect(0, 0, 40, 50), whiteBox);
        this.tournamentStructure.setPosition(cc.p(340 * scaleFactor, 450 * scaleFactor));
        this.addChild(this.tournamentStructure, 2, "tournamentStructure");
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.tournamentStructure);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.tournamentStructure);

        this.tournamentPrize = new DetailTabs("All Prizes", {
            normal: "PrizeIconOver.png",
            hover: "PrizeIcon.png"
        }, cc.rect(10, 0, 30, 50), whiteBox);
        this.tournamentPrize.setPosition(cc.p(580 * scaleFactor, 450 * scaleFactor));
        this.tournamentPrize.addWall();
        this.addChild(this.tournamentPrize, 2, "tournamentPrize");
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.tournamentPrize);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.tournamentPrize);

        this.tournamentProgress = new DetailTabs("Tournament Progress", {
            normal: "ProgressOver.png",
            hover: "ProgressWhite.png"
        }, cc.rect(10, 0, 40, 50), whiteBox);
        this.tournamentProgress.setPosition(cc.p(820 * scaleFactor, 450 * scaleFactor));
        this.addChild(this.tournamentProgress, 2, "tournamentProgress");
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.tournamentProgress);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.tournamentProgress);

        var status = room.getVariable(TOURNAMENT_CONSTANTS.STATUS).value;
        switch (status.toUpperCase()) {
            case TOURNAMENT_CONSTANTS.OPEN :
                this.statusInfo = new TournamentOpen(this);
                break;
            case TOURNAMENT_CONSTANTS.REGISTERING :	//join enable
                this.statusInfo = new TournamentRegistering(this);
                break;
            case TOURNAMENT_CONSTANTS.FREEZE :		//join in waiting
                this.statusInfo = new TournamentFreeze(this);
                break;
            case TOURNAMENT_CONSTANTS.RUNNING :		// Quit
                this.statusInfo = new TournamentRunning(this);
                break;
            case TOURNAMENT_CONSTANTS.COMPLETED:	// take seat gray
                this.statusInfo = new TournamentCompleted(this);
                break;
            case TOURNAMENT_CONSTANTS.CANCELLED :	// take seat
                this.statusInfo = new TournamentCancelled(this);
                break;
            default :
                break;
        }
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.statusInfo.instructions);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.statusInfo.instructions);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.statusInfo.prizeToolTip);
        this.addChild(this.statusInfo, 5);
        this.initRoom(room, roomBar);

        return true;
    },
    initRoom: function (room, roomBar) {

        this.TOT_ROUND = 0;
        var rId = room.id;
        this.trnmntRoom = sfsClient.getRoomById(rId);

        var dataObj = {};
        dataObj["isPrizeStructure"] = true;
        sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.PRIZE_INFO, dataObj, rId);

        this.headerLabel.setString(this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_NAME).value);
        this.tournamentId.setString(this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_ID).value);
        this.status.setString(this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.STATUS).value);
        this.joined.setString(this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.REG_NUM).value);

        this.grpType = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value;

        this.initScrollPanel(this.grpType);

        this.initTabs("tournamentStructure");

        var tempFee = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.ENTRY_FEE).value;
        var tempArr = [];
        if (tempFee) {

            tempArr = tempFee.split(",");

            if (tempArr.length == 1) {

                this.entryFee.setString(tempFee);

            } else {

                var formatArr = tempArr[0].split("|");
                var feeArr = tempArr[1].split("|");

                if (formatArr[0] == "Chip")
                    this.entryFee.setString("`" + feeArr[0]);

                else if (formatArr[0] == "Ticket") {

                    if (feeArr[0].split(":")[0] == 1)
                        this.entryFee.setString(formatArr[0]);
                    else
                        this.entryFee.setString(feeArr[0].split(":")[0] + " " + formatArr[0]);

                }
            }
        }
        this.updateJoinBtn(roomBar, room);
        this.setTournamentStatus(roomBar);
        this.updateRegPls();
        this.updateCountDown(true);

        if (this.statusInfo.tournamentStartTime) {
            if (this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.IS_WHEN_FILL) && this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.IS_WHEN_FILL).value == "YES")
                this.statusInfo.tournamentStartTime.setString("When Fill");
            else
                this.statusInfo.tournamentStartTime.setString(DateNTime.getDateInfo(this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.START_TIME).value));
        }
        this._isWinnerReqSent = false;
    },
    initTabs: function (tabType) {
        switch (tabType) {
            case "tournamentStructure":
                if (this.tournamentProgressToolTip) {
                    this.tournamentProgressToolTip.removeFromParent(true);
                }
                if (!this.tournamentStructure._isSelected) {

                    this.tournamentStructure.setSelected(true);
                    this.structure.setVisible(true);

                    this.tournamentPrize.setSelected(false);
                    this.prize.setVisible(false);

                    this.tournamentProgress.setSelected(false);
                    this.progress.setVisible(false);
                }
                break;
            case "tournamentPrize":
                if (this.tournamentProgressToolTip) {
                    this.tournamentProgressToolTip.removeFromParent(true);
                }
                if (!this.tournamentPrize._isSelected) {
                    this.tournamentPrize.setSelected(true);
                    this.prize.setVisible(true);

                    this.tournamentStructure.setSelected(false);
                    this.structure.setVisible(false);

                    this.tournamentProgress.setSelected(false);
                    this.progress.setVisible(false);
                }
                break;
            case "tournamentProgress":
                if (!this.tournamentProgress._isSelected) {
                    var state = String(this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.STATUS).value.toUpperCase());
                    if ((state == TOURNAMENT_CONSTANTS.RUNNING) || (state == TOURNAMENT_CONSTANTS.COMPLETED)) {
                        this.tournamentProgress.setSelected(true);
                        this.progress.setVisible(true);
                        this.progress.cleanUpLevelProgress();

                        this.tournamentPrize.setSelected(false);
                        this.prize.setVisible(false);

                        this.tournamentStructure.setSelected(false);
                        this.structure.setVisible(false);
                    } else {
                        if (this.tournamentProgressToolTip) {
                            this.tournamentProgressToolTip.removeFromParent(true);
                        } else {
                            this.tournamentProgressToolTip = new ToolTip(cc.size(171 * scaleFactor, 46 * scaleFactor), cc.color(255, 238, 215));
                            this.tournamentProgressToolTip.setText("Please Wait... Tournament yet to start");
                            this.tournamentProgressToolTip.setPosition(cc.p(this.tournamentProgress.x, this.tournamentProgress.y + 57 * scaleFactor));
                            this.tournamentProgressToolTip.setArrow("BOTTOM");
                            this.addChild(this.tournamentProgressToolTip, 10, "tournamentProgressToolTip");
                        }
                    }
                }
                break;
        }
    },
    initScrollPanel: function (grpType) {
        if (grpType == "Round") {
            this.structure = new RoundStructure(this, this.trnmntRoom);
            this.addChild(this.structure, 5);
            this.prize = new RoundAllPrize(this, this.trnmntRoom);
            this.addChild(this.prize, 2);
            this.progress = new RoundProgress(this, this.trnmntRoom);
            this.addChild(this.progress, 2);
        }
        else {
            this.structure = new TimeStructure(this, this.trnmntRoom);
            this.addChild(this.structure, 5);
            this.prize = new TimeAllPrize(this, this.trnmntRoom);
            this.addChild(this.prize, 2);
            this.progress = new TimeProgress(this, this.trnmntRoom);
            this.addChild(this.progress, 2);
        }
    },
    isMeInTournament: function () {
        var _isMe = false;
        if (this._myTournament == 1) {
            _isMe = true;
            this.priorityNum = TOURNAMENT_CONSTANTS.MY_TOURNAMENT;
        }
        return _isMe;
    },
    showLevelProgress: function (dataObj) {
        this.progress.showLevelProgress(dataObj);
    },
    showTableProgress: function (dataObj) {
        this.progress.showTableProgress(dataObj);
    },
    updateJoinBtn: function (roomBar, room) {
        var status = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.STATUS).value;
        this.status.setString(status);
        var _isMeInRoom = this.isMeInTournament();
        switch (status.toUpperCase()) {
            case TOURNAMENT_CONSTANTS.OPEN :    //join disable
                this.statusSym.setState(this.trnmntRoom, false, true);
                var jointxt = "Registration for this tournament will start at";//trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.JOIN_HELP_TXT);
                if (jointxt) {
                    jointxt += " " + DateNTime.getDateInfo(this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.REG_START_TIME).value) + ".";
                    // target.parent.showToolTip(true, jointxt, target, size);
                }
                break;
            case TOURNAMENT_CONSTANTS.REGISTERING :	//join enable
                this.statusSym.setState(this.trnmntRoom, _isMeInRoom, true);
                // this._evntType = ClientEvent.TOURNAMENT_JOIN;
                break;
            case TOURNAMENT_CONSTANTS.FREEZE :		//join in wating
                this.statusSym.setState(this.trnmntRoom, _isMeInRoom, true);
                this.statusSym.statusText.setColor(cc.color(168, 220, 59));
                this.statusSym.statusText.setScale(0.7);
                // this._evntType = ClientEvent.TOURNAMENT_JOIN;
                break;
            case TOURNAMENT_CONSTANTS.RUNNING :     // Quit
                this.statusSym.setState(this.trnmntRoom, _isMeInRoom, true);
                this.statusSym.statusText.setColor(cc.color(168, 220, 59));
                this.statusSym.statusText.setScale(0.7);
                break;

            case TOURNAMENT_CONSTANTS.COMPLETED:	// take seat gray
                this.statusSym.setState(this.trnmntRoom, _isMeInRoom, true);
                this.statusSym.statusText.setColor(cc.color(168, 220, 59));
                this.statusSym.statusText.setScale(0.6);
                break;

            case TOURNAMENT_CONSTANTS.CANCELLED :       // take seat
                this.removeSelf();
                this.statusSym.setState(this.trnmntRoom, _isMeInRoom, true);
                this.statusSym.statusText.setColor(cc.color(168, 220, 59));
                this.statusSym.statusText.setScale(0.6);

                var message = this.getCancelTournamentMsg(this.trnmntRoom, 0);
                var tourName = this.trnmntRoom.getVariable("tournamentName").value;

                // if (applicationFacade._lobby.tournamentLobby.tourRoomListManager._myTournamentsIds.indexOf(room.id.toString()) >= 0)
                //     this.showCancelTourPopup(message, tourName);

                break;

            default :
                //this.statusSym.statusText.setString(roomBar.statusSym.statusTF.text);
                break;
        }
        if (status.toUpperCase() != TOURNAMENT_CONSTANTS.CANCELLED)
            this.updateDetailsSidePanel(status);
    },
    showCancelTourPopup: function (message, tourName) {
        var size = new cc.Size(600 * scaleFactor, 170 * scaleFactor);
        this.tourCancelPopup && this.tourCancelPopup.removeFromParent(true);
        this.tourCancelPopup = new TourCommonPopup(message, tourName, size, this, true);
        applicationFacade._lobby.addChild(this.tourCancelPopup, GameConstants.tourPopupZorder, "tourCancelPopup");
    },
    getCancelTournamentMsg: function (room, status) {
        var message = "";
        if (status == 0) {
            if (room.getVariable(TOURNAMENT_CONSTANTS.ENTRY_FEE).value.toLowerCase() == "free")
                message = "Due to lack of minimum participants (players), this tournament has been called off.";
            else
                message = "Due to lack of minimum participants (players), this tournament has been called off. Players Buy-In would be refunded back to players account respectively.";
        }
        return message;
    },
    updateDetailsSidePanel: function (status) {
        if (this.statusInfo)
            this.statusInfo.removeFromParent(true);
        switch (status.toUpperCase()) {
            case TOURNAMENT_CONSTANTS.OPEN :
                this.statusInfo = new TournamentOpen(this);
                break;
            case TOURNAMENT_CONSTANTS.REGISTERING :	//join enable
                this.statusInfo = new TournamentRegistering(this);
                break;
            case TOURNAMENT_CONSTANTS.FREEZE :		//join in waiting
                this.statusInfo = new TournamentFreeze(this);
                break;
            case TOURNAMENT_CONSTANTS.RUNNING :		// Quit
                this.statusInfo = new TournamentRunning(this);
                break;
            case TOURNAMENT_CONSTANTS.COMPLETED:	// take seat gray
                this.statusInfo = new TournamentCompleted(this);
                break;
            case TOURNAMENT_CONSTANTS.CANCELLED :	// take seat
                this.statusInfo = new TournamentCancelled(this);
                break;
            default :
                break;
        }
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.statusInfo.instructions);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.statusInfo.instructions);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.statusInfo.prizeToolTip);
        this.addChild(this.statusInfo, 5);
        this.updateCountDown(true);
        this.setTournamentStatus();
    },
    handleStructure: function () {

    },
    handleProgress: function () {

    },
    handleRoomVarDetailUpdate: function (rmVar) {
        if (rmVar == TOURNAMENT_CONSTANTS.CUR_LEVEL) {
            if (this.structure.isVisible())
                this.structure.initRoom(this.trnmntRoom);
            if (this.progress.isVisible())
                this.progress.showCurrentRound();
        }
        else if (rmVar == TOURNAMENT_CONSTANTS.STRUCTURE) {
            if (this.structure.isVisible())
                this.structure.initRoom(this.trnmntRoom);
        }
        else if (rmVar == TOURNAMENT_CONSTANTS.WINNER_PLR_POS) {
            if (this.grpType == "Round") {
                if (this.prize.isVisible())
                    this.prize.setWinnerPosition(this.trnmntRoom);
            }
        }
        else if (rmVar == TOURNAMENT_CONSTANTS.TOTAL_PRIZE) {
            var formatStr = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.TOTAL_PRIZE).value;
            this.statusInfo.expectedPrize.setString("Prize");
            if (formatStr)
                this.statusInfo.expectedPrize.setString(String(PrizeStringCalculator.calculatePrzStr(formatStr)));
        }
        else if (rmVar == TOURNAMENT_CONSTANTS.IS_WHEN_FILL) {
            if (this.statusInfo.tournamentStartTime) {
                if (this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.IS_WHEN_FILL) && this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.IS_WHEN_FILL).value == "YES")
                    this.statusInfo.tournamentStartTime.setString("When Fill");
                else
                    this.statusInfo.tournamentStartTime.setString(DateNTime.getDateInfo(this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.START_TIME).value));
            }
        }
    },
    updateRegPls: function (regNum) {

        if (!regNum)
            regNum = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.REG_NUM).value;

        this.joined.setString(regNum + "/" + this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_REG_NUM).value);
        var status = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.STATUS).value;
        if (status.toUpperCase() != "REGISTERING")
            return;
        this.updateJoinBtn();
    },
    updateCountDown: function (bool) {

        var status = (this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.STATUS).value).toUpperCase();

        if (status == TOURNAMENT_CONSTANTS.COMPLETED || status == TOURNAMENT_CONSTANTS.CANCELLED || status == TOURNAMENT_CONSTANTS.CLOSED) {
            return;
        }

        var timeDiff;
        var date = applicationFacade._controlAndDisplay._serverDate;
        var startTime;
        var timeObj;
        var serverTime;

        if (status == TOURNAMENT_CONSTANTS.RUNNING) {

            startTime = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.START_TIME).value;
            serverTime = date.getTime() - date.getMilliseconds();
            timeDiff = serverTime - startTime;
            timeObj = DateNTime.getTimeObject(timeDiff);
            this.statusInfo.tournamentDuration.setString(timeObj["H"] + " Hour : " + timeObj["M"] + " Min : " + timeObj["S"] + " Sec");

            window.clearInterval(this.timeUpdaterInterval);
            this.timeUpdaterInterval = setInterval(function () {
                startTime = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.START_TIME).value;
                serverTime = date.getTime() - date.getMilliseconds();
                timeDiff = serverTime - startTime;
                timeObj = DateNTime.getTimeObject(timeDiff);
                this.statusInfo.tournamentDuration.setString(timeObj["H"] + " Hour : " + timeObj["M"] + " Min : " + timeObj["S"] + " Sec");
            }.bind(this), 1000);
        }
        else {

            if (!bool && date.getSeconds() != 0)
                return;

            serverTime = date.getTime() - date.getSeconds() * 1000 - date.getMilliseconds();
            startTime = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.START_TIME).value;
            timeDiff = startTime - serverTime;
            timeObj = DateNTime.getTimeObject(timeDiff);

            this.statusInfo.countDowntimer.day.setString(timeObj["D"]);
            this.statusInfo.countDowntimer.hour.setString(timeObj["H"]);
            this.statusInfo.countDowntimer.minute.setString(timeObj["M"]);

            window.clearInterval(this.timeUpdaterInterval);
            this.timeUpdaterInterval = setInterval(function () {
                serverTime = date.getTime() - date.getSeconds() * 1000 - date.getMilliseconds();
                startTime = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.START_TIME).value;
                timeDiff = startTime - serverTime;
                timeObj = DateNTime.getTimeObject(timeDiff);

                this.statusInfo.countDowntimer.day.setString(timeObj["D"]);
                this.statusInfo.countDowntimer.hour.setString(timeObj["H"]);
                this.statusInfo.countDowntimer.minute.setString(timeObj["M"]);
            }.bind(this), 1000);
        }
    },
    setTournamentStatus: function (roomBar) {

        var status, regStartTime, regCloseTime, startTime, formatStr;
        if (this.trnmntRoom.variables[TOURNAMENT_CONSTANTS.STATUS])
            status = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.STATUS).value;

        if (this.trnmntRoom.variables[TOURNAMENT_CONSTANTS.REG_START_TIME])
            regStartTime = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.REG_START_TIME).value;

        if (this.trnmntRoom.variables[TOURNAMENT_CONSTANTS.REG_END_TIME])
            regCloseTime = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.REG_END_TIME).value;

        if (this.trnmntRoom.variables[TOURNAMENT_CONSTANTS.START_TIME])
            startTime = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.START_TIME).value;

        if (this.trnmntRoom.variables[TOURNAMENT_CONSTANTS.TOTAL_PRIZE])
            formatStr = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.TOTAL_PRIZE).value;

        //this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.TOTAL_PRIZE_FORMAT) + ":" +

        var timeObj;
        this.removeQuesMark();

        if (formatStr)
            this.statusInfo.expectedPrize.setString(PrizeStringCalculator.calculatePrzStr(formatStr));
        this.statusInfo.expectedPrizeHeader.setString("Expected Prize");

        switch (status.toUpperCase()) {
            case TOURNAMENT_CONSTANTS.OPEN :
                this.addQuesMark();

                this.statusInfo.registrationStart.setString(DateNTime.getDateInfo(regStartTime));
                this.statusInfo.registrationCloses.setString(DateNTime.getDateInfo(regCloseTime));

                if (this.trnmntRoom.variables[TOURNAMENT_CONSTANTS.ESTIMATED_TIME]) {
                    timeObj = DateNTime.getTimeObject(this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.ESTIMATED_TIME).value);
                    this.statusInfo.estimatedDuration.setString(timeObj["H"] + " Hour : " + timeObj["M"] + " Minutes");
                }
                this.statusInfo.tournamentStartTime.setString(DateNTime.getDateInfo(startTime));
                break;

            case TOURNAMENT_CONSTANTS.REGISTERING :	//join enable
                this.addQuesMark();
                this.statusInfo.registrationCloses.setString(DateNTime.getDateInfo(regCloseTime));
                if (this.trnmntRoom.variables[TOURNAMENT_CONSTANTS.ESTIMATED_TIME]) {
                    timeObj = DateNTime.getTimeObject(this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.ESTIMATED_TIME).value);
                    this.statusInfo.estimatedDuration.setString(timeObj["H"] + " Hour : " + timeObj["M"] + " Minutes");
                }
                this.statusInfo.tournamentStartTime.setString(DateNTime.getDateInfo(startTime));

                break;

            case TOURNAMENT_CONSTANTS.FREEZE :		//join in waiting
                this.statusInfo.expectedPrizeHeader.setString("Prize");
                this.statusInfo.viewPlayerBtn.setColor(cc.color(255, 153, 0));
                this.statusInfo.viewPlayerBtn.setPosition(cc.p(100 * scaleFactor, 385 * scaleFactor));
                cc.eventManager.addListener(this.popupTouchListener.clone(), this.statusInfo.viewPlayerBtn);

                if (this.trnmntRoom.variables[TOURNAMENT_CONSTANTS.ESTIMATED_TIME]) {
                    timeObj = DateNTime.getTimeObject(this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.ESTIMATED_TIME).value);
                    this.statusInfo.estimatedDuration.setString(timeObj["H"] + " Hour : " + timeObj["M"] + " Min : " + timeObj["S"] + " Sec");
                }
                this.statusInfo.tournamentStartTime.setString(DateNTime.getDateInfo(startTime));
                break;

            case TOURNAMENT_CONSTANTS.RUNNING :		// Quit
                this.statusInfo.expectedPrizeHeader.setString("Prize");
                this.statusInfo.viewPlayerBtn.setColor(cc.color(255, 153, 0));
                this.statusInfo.viewPlayerBtn.setPosition(cc.p(100 * scaleFactor, 385 * scaleFactor));
                //  this.statusInfo.progressToolTip.setVisible(false);
                cc.eventManager.addListener(this.popupTouchListener.clone(), this.statusInfo.viewPlayerBtn);
                break;

            case TOURNAMENT_CONSTANTS.COMPLETED:	// take seat gray
                this.statusInfo.expectedPrizeHeader.setString("Prize");
                //   this.statusInfo.progressToolTip.setVisible(false);
                this.statusInfo.viewPlayerBtn.setString("View Winners");
                this.statusInfo.viewPlayerBtn.setColor(cc.color(255, 153, 0));
                this.statusInfo.viewPlayerBtn.setPosition(cc.p(100 * scaleFactor, 385 * scaleFactor));
                cc.eventManager.addListener(this.popupTouchListener.clone(), this.statusInfo.viewPlayerBtn);

                if (this.trnmntRoom.variables[TOURNAMENT_CONSTANTS.ESTIMATED_TIME]
                    && this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.ESTIMATED_TIME).value) {
                    timeObj = DateNTime.getTimeObject(this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.ESTIMATED_TIME).value);
                    this.statusInfo.tournamentDuration.setString(timeObj["H"] + " Hour : " + timeObj["M"] + " Min : " + timeObj["S"] + " Sec");
                }
                break;

            case TOURNAMENT_CONSTANTS.CANCELLED :	// take seat
                break;
            default :
                break;
        }
    },
    removeQuesMark: function () {

    },
    addQuesMark: function () {

    },
    onTouchEndedCallBack: function (touch, event) {

        var target = event.getCurrentTarget();
        switch (target._name) {
            case "closeBtn":
                this.removeSelf();
                break;
            case "tournamentStructure":
                this.initTabs("tournamentStructure");
                break;
            case "tournamentPrize":
                this.initTabs("tournamentPrize");
                break;
            case "tournamentProgress":
                this.initTabs("tournamentProgress");
                break;
            case "Join":
                this.joinBtnHandler();
                break;
            case "Quit":
                this.quitBtnHandler();
                break;
            case "TakeSeat":
                this.takeSeatBtnHandler();
                break;
            case "viewPlayerBtn":
                this.viewPlayerBtnHandler();
                break;
            case "instructions":
                this.instructionsBtnHandler();
                break;
        }
    },
    instructionsBtnHandler: function () {
        var reqObj = {};
        sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_INSTRUCTION_REQ, reqObj, this.trnmntRoom.id);
    },
    instructionsPopup: function (room, dataObj) {
        this.instructionPopup && this.instructionPopup.removeFromParent(true);
        this.instructionPopup = new TournamentInstructions(this, dataObj);
        applicationFacade._lobby.addChild(this.instructionPopup, GameConstants.tourPopupZorder, "instructionPopup");
    },
    viewPlayerBtnHandler: function () {
        var status = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.STATUS).value;
        var reqObj = {};
        if (status.toUpperCase() == TOURNAMENT_CONSTANTS.COMPLETED)
            sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.WINNER_INFO, reqObj, this.trnmntRoom.id);
        else
            sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.CMD_JOINED_PLAYER_INFO, reqObj, this.trnmntRoom.id);
    },
    handleJoinedPlayers: function () {
        this.joinedPlayerPopup && this.joinedPlayerPopup.removeFromParent(true);
        this.joinedPlayerPopup = new DetailsJoinedPlayerHandler(this.trnmntRoom, this);
        applicationFacade._lobby.addChild(this.joinedPlayerPopup, GameConstants.tourPopupZorder, "joinedPlayerPopup");

        var regPlsListStr = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.REG_PLS).value;
        if (regPlsListStr && regPlsListStr != "") {
            var regPlsListArr = regPlsListStr.split(",");
            if (regPlsListArr.length >= 1) {
                var plName;
                for (var i = 0; i < regPlsListArr.length; i++) {
                    plName = regPlsListArr[i];
                    this.joinedPlayerPopup.setRow(String(plName), i);
                }
            }
        }
    },
    handleWinnerPlayers: function () {
        this.winnerPlayerPopup && this.winnerPlayerPopup.removeFromParent(true);
        if (this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Round") {
            this.winnerPlayerPopup = new DetailsWinnerPlayerHandler(this.trnmntRoom, this);
            applicationFacade._lobby.addChild(this.winnerPlayerPopup, GameConstants.tourPopupZorder, "winnerPlayerPopup");

            var winnerListStr = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.WINNER_PLR_POS).value;
            if (winnerListStr && winnerListStr != "") {
                var winnerListArr = winnerListStr.split(";");
                winnerListArr.sort();
                if (winnerListArr.length >= 1) {
                    var prizeStr = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.PRIZE_STRUCT).value;
                    var prizeStrArr = prizeStr.split(";");
                    var plName;
                    var pos;
                    var prizeInfoArr;
                    var prize;
                    for (var i = 0; i < winnerListArr.length; i++) {
                        var posArr = winnerListArr[i].split(":");
                        pos = posArr[0];
                        plName = posArr[1];
                        for (var j = 0; j < prizeStrArr.length; j++) {
                            prizeInfoArr = String(prizeStrArr[j]).split(",");
                            if (pos == prizeInfoArr[0]) {
                                if (prizeInfoArr[1])
                                    prize = PrizeStringCalculator.calculatePrzStr(prizeInfoArr[1]);
                                this.winnerPlayerPopup.setRow(String(plName), prize, i);
                                break;
                            }
                        }
                    }
                }
            }
        } else {
            this.winnerPlayerPopup = new DetailsTimeWinnerPlayerHandler(this.trnmntRoom, this);
            applicationFacade._lobby.addChild(this.winnerPlayerPopup, GameConstants.tourPopupZorder, "winnerPlayerPopup");

            var winnerListTimeStr = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.WINNER_PLR_POS).value;
            if (winnerListTimeStr && winnerListTimeStr != "") {
                var winnerTimeListArr = winnerListTimeStr.split(";");
                if (winnerTimeListArr.length >= 1) {
                    var prizeT;
                    for (var k = 0; k < winnerTimeListArr.length; k++) {
                        var posTimeArr = winnerTimeListArr[k].split(",");
                        if (posTimeArr[3])
                            prizeT = PrizeStringCalculator.calculatePrzStr(posTimeArr[3]);
                        this.winnerPlayerPopup.setRow(posTimeArr[0], String(posTimeArr[1]), posTimeArr[2], prizeT, k);
                    }
                }
            }
        }
    },
    joinBtnHandler: function () {
        var roomZone = sfs.getRoomByName("TournamentLobby");
        var id = roomZone.id;
        if (!roomZone.isJoined) {
            sfs.send(new SFS2X.Requests.System.JoinRoomRequest(id, null, -1, true));
            return;
        }

        var status = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.STATUS).value;
        var tRoomId = this.trnmntRoom.id;
        if (status == TOURNAMENT_CONSTANTS.STATUS_REGISTERING || status == TOURNAMENT_CONSTANTS.STATUS_WAITING) {

            var reqObj = {};
            sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.PLAYER_ELIGIBLE_REQ, reqObj, tRoomId);
        }
    },
    quitBtnHandler: function () {
        var roomZone = sfs.getRoomByName("TournamentLobby");
        var id = roomZone.id;
        if (!roomZone.isJoined) {
            sfs.send(new SFS2X.Requests.System.JoinRoomRequest(id, null, -1, true));
            return;
        }

        this.quitConfirmationPopup && this.quitConfirmationPopup.removeFromParent(true);
        this._myTournament = 0;
        this.quitConfirmationPopup = new QuitConfirmationPopup(this.trnmntRoom, this, true, "quitConfrmPopup");
        applicationFacade._lobby.addChild(this.quitConfirmationPopup, GameConstants.tourPopupZorder, "quitConfirmationPopup");
    },
    takeSeatBtnHandler: function (context) {
        var roomZone = sfs.getRoomByName("TournamentLobby");
        var id = roomZone.id;
        if (!roomZone.isJoined) {
            sfs.send(new SFS2X.Requests.System.JoinRoomRequest(id, null, -1, true));
            return;
        }

        var status = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.STATUS).value;
        var tRoomId = this.trnmntRoom.id;
        var reqObj = {};
        if (tRoomId != -1)
            reqObj["feeType"] = "Chip";
        if (!applicationFacade.takeSeatFromLobby(tRoomId))
            sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_TAKESEAT_REQUEST, reqObj, tRoomId);
    },
    removeSelf: function () {
        this.removeFromParent(true);
    },
    statusSymToolTipListener: function (bool) {

    },
    getStatusSymBtn: function () {

    },
    onExit: function () {
        this._super();
        window.clearInterval(this.timeUpdaterInterval);
    }
});

/*
 expectedPrize
 viewPlayerBtn
 registrationStart
 registrationCloses
 estimatedDuration
 tournamentStartTime
 timer
 instructions
 */