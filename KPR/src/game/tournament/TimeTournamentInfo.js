/**
 * Created by stpl on 4/10/2017.
 */
var TimeTournamentInfo = TournamentInfo.extend({
    ctor: function (trnmntGameRoom, dataObj) {
        this._super(trnmntGameRoom, dataObj);

        this.setContentSize(cc.size(346 * scaleFactor, 140 * scaleFactor));
        this.setPosition(cc.p(this.width / 2 - 2 * scaleFactor, cc.winSize.height - this.height / 2 - 36 * scaleFactor));

        var whiteBackground = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png"), cc.rect(0, 0, 50 * scaleFactor, 50 * scaleFactor));
        whiteBackground.setCapInsets(cc.rect(16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor));
        whiteBackground.setAnchorPoint(0, 1);
        whiteBackground.setContentSize(cc.size(295 * scaleFactor, 130 * scaleFactor));
        whiteBackground.setPosition(cc.p(43 * scaleFactor, this.height - 5 * scaleFactor));
        this.addChild(whiteBackground);

        var entryfeeHeader = new cc.LabelTTF("Buy-In:", "RobotoBold", 13 * 2 * scaleFactor);
        entryfeeHeader.setScale(0.5);
        entryfeeHeader.setColor(cc.color(0, 0, 0));
        entryfeeHeader.setAnchorPoint(0, 0.5);
        entryfeeHeader.setPosition(cc.p(14 * scaleFactor, 118 * scaleFactor));
        whiteBackground.addChild(entryfeeHeader);

        this.entryFee = new cc.LabelTTF("", "RupeeFordian", 13 * 2 * scaleFactor);
        this.entryFee.setScale(0.5);
        this.entryFee.setColor(cc.color(0, 0, 0));
        this.entryFee.setAnchorPoint(0, 0.5);
        this.entryFee.setPosition(cc.p(entryfeeHeader.x + entryfeeHeader.width / 2 + 5 * scaleFactor, entryfeeHeader.y));
        whiteBackground.addChild(this.entryFee);

        var levelHeader = new cc.LabelTTF("Level:", "RobotoBold", 13 * 2 * scaleFactor);
        levelHeader.setScale(0.5);
        levelHeader.setColor(cc.color(0, 0, 0));
        levelHeader.setAnchorPoint(0, 0.5);
        levelHeader.setPosition(cc.p(14 * scaleFactor, 91 * scaleFactor));
        whiteBackground.addChild(levelHeader);

        var line = new cc.DrawNode();
        line.drawSegment(cc.p(44 * scaleFactor, 110 * scaleFactor), cc.p(336 * scaleFactor, 110 * scaleFactor), 0.5, cc.color(211, 211, 211));
        this.addChild(line, 1);

        line = new cc.DrawNode();
        line.drawSegment(cc.p(44 * scaleFactor, 87 * scaleFactor), cc.p(336 * scaleFactor, 87 * scaleFactor), 0.5, cc.color(211, 211, 211));
        this.addChild(line, 1);

        line = new cc.DrawNode();
        line.drawSegment(cc.p(44 * scaleFactor, 60 * scaleFactor), cc.p(336 * scaleFactor, 60 * scaleFactor), 0.5, cc.color(211, 211, 211));
        this.addChild(line, 1);

        line = new cc.DrawNode();
        line.drawSegment(cc.p(193 * scaleFactor, 132 * scaleFactor), cc.p(193 * scaleFactor, 60 * scaleFactor), 0.5, cc.color(211, 211, 211));
        this.addChild(line, 1);

        this.level = new cc.LabelTTF("", "RobotoRegular", 13 * 2 * scaleFactor);
        this.level.setScale(0.5);
        this.level.setColor(cc.color(0, 0, 0));
        this.level.setAnchorPoint(0, 0.5);
        this.level.setPosition(levelHeader.x + levelHeader.width / 2 + 5 * scaleFactor, levelHeader.y);
        whiteBackground.addChild(this.level);

        var pointValueHeader = new cc.LabelTTF("Point  Value:", "RobotoBold", 13 * 2 * scaleFactor);
        pointValueHeader.setScale(0.5);
        pointValueHeader.setColor(cc.color(0, 0, 0));
        pointValueHeader.setAnchorPoint(0, 0.5);
        pointValueHeader.setPosition(cc.p(14 * scaleFactor, 65 * scaleFactor));
        whiteBackground.addChild(pointValueHeader);

        this.pointValue = new cc.LabelTTF("", "RobotoRegular", 13 * 2 * scaleFactor);
        this.pointValue.setScale(0.5);
        this.pointValue.setColor(cc.color(0, 0, 0));
        this.pointValue.setAnchorPoint(0, 0.5);
        this.pointValue.setPosition(cc.p(pointValueHeader.x + pointValueHeader.width / 2 + 5 * scaleFactor, pointValueHeader.y));
        whiteBackground.addChild(this.pointValue);

        var prizesHeader = new cc.LabelTTF("Prizes:", "RobotoBold", 12 * 2 * scaleFactor);
        prizesHeader.setScale(0.5);
        prizesHeader.setColor(cc.color(0, 0, 0));
        prizesHeader.setAnchorPoint(0, 0.5);
        prizesHeader.setPosition(cc.p(14 * scaleFactor, 41 * scaleFactor));
        whiteBackground.addChild(prizesHeader);

        this.prizeValue = new cc.LabelTTF("As per prize table", "RobotoRegular", 12 * 2 * scaleFactor);
        this.prizeValue.setScale(0.5);
        this.prizeValue.setColor(cc.color(0, 0, 0));
        this.prizeValue.setAnchorPoint(0, 0.5);
        this.prizeValue.setPosition(cc.p(prizesHeader.x + prizesHeader.width / 2 + 5 * scaleFactor, prizesHeader.y));
        whiteBackground.addChild(this.prizeValue);

        var minChipHeader = new cc.LabelTTF("Min  chips  for  next  deal/level:", "RobotoBold", 12 * 2 * scaleFactor);
        minChipHeader.setScale(0.5);
        minChipHeader.setColor(cc.color(0, 0, 0));
        minChipHeader.setAnchorPoint(0, 0.5);
        minChipHeader.setPosition(cc.p(14 * scaleFactor, 20 * scaleFactor));
        whiteBackground.addChild(minChipHeader);

        this.minChip = new cc.LabelTTF("", "RobotoRegular", 12 * 2 * scaleFactor);
        this.minChip.setScale(0.5);
        this.minChip.setColor(cc.color(0, 0, 0));
        this.minChip.setAnchorPoint(0, 0.5);
        this.minChip.setPosition(cc.p(minChipHeader.x + minChipHeader.width / 2 + 5 * scaleFactor, minChipHeader.y));
        whiteBackground.addChild(this.minChip);

        var balanceChipsHeader = new cc.LabelTTF("Balance  Chips:", "RobotoBold", 12 * 2 * scaleFactor);
        balanceChipsHeader.setScale(0.5);
        balanceChipsHeader.setColor(cc.color(0, 0, 0));
        balanceChipsHeader.setAnchorPoint(0, 0.5);
        balanceChipsHeader.setPosition(cc.p(162 * scaleFactor, entryfeeHeader.y));
        whiteBackground.addChild(balanceChipsHeader);

        this.balanceChips = new cc.LabelTTF("", "RobotoRegular", 12 * 2 * scaleFactor);
        this.balanceChips.setScale(0.5);
        this.balanceChips.setColor(cc.color(0, 0, 0));
        this.balanceChips.setAnchorPoint(0, 0.5);
        this.balanceChips.setPosition(cc.p(balanceChipsHeader.x + balanceChipsHeader.width / 2 + 5 * scaleFactor, balanceChipsHeader.y));
        whiteBackground.addChild(this.balanceChips);

        var rankHeader = new cc.LabelTTF("Rank:", "RobotoBold", 12 * 2 * scaleFactor);
        rankHeader.setScale(0.5);
        rankHeader.setColor(cc.color(0, 0, 0));
        rankHeader.setAnchorPoint(0, 0.5);
        rankHeader.setPosition(cc.p(162 * scaleFactor, levelHeader.y));
        whiteBackground.addChild(rankHeader);

        this.rank = new cc.LabelTTF("", "Robotoregular", 12 * 2 * scaleFactor);
        this.rank.setScale(0.5);
        this.rank.setColor(cc.color(0, 0, 0));
        this.rank.setAnchorPoint(0, 0.5);
        this.rank.setPosition(cc.p(rankHeader.x + rankHeader.width / 2 + 5 * scaleFactor, rankHeader.y));
        whiteBackground.addChild(this.rank);

        var rebuyHeader = new cc.LabelTTF("Rebuy:", "RobotoBold", 12 * 2 * scaleFactor);
        rebuyHeader.setScale(0.5);
        rebuyHeader.setColor(cc.color(0, 0, 0));
        rebuyHeader.setAnchorPoint(0, 0.5);
        rebuyHeader.setPosition(cc.p(162 * scaleFactor, pointValueHeader.y));
        whiteBackground.addChild(rebuyHeader);

        this.rebuy = new cc.LabelTTF("", "RupeeFordian", 12 * 2 * scaleFactor);
        this.rebuy.setScale(0.5);
        this.rebuy.setColor(cc.color(0, 0, 0));
        this.rebuy.setAnchorPoint(0, 0.5);
        this.rebuy.setPosition(cc.p(rebuyHeader.x + rebuyHeader.width / 2 + 5 * scaleFactor, rebuyHeader.y));
        whiteBackground.addChild(this.rebuy);

        this.addCloseBtn();
        this.showTourInf(false);

        // this.setTimeParameter();
        return true;
    },
    showCurrentDeal: function () {

    },
    updateRank: function (rank, chip) {
        this._super(rank, chip);
        this.balanceChips.setString(chip);
    },
    setTimeParameter: function () {
        var tempFee;
        if (this.gameRoom.parentRoom.getVariable(TOURNAMENT_CONSTANTS.ENTRY_FEE))
            tempFee = this.gameRoom.parentRoom.getVariable(TOURNAMENT_CONSTANTS.ENTRY_FEE).value;
        var tempArr;
        if (tempFee) {
            tempArr = tempFee.split(",");
            if (tempArr.length == 1)
                this.entryFee.setString(tempFee);
            else {
                var formatArr = tempArr[0].split("|");
                var feeArr = tempArr[1].split("|");
                if (formatArr[0] == "Chip")
                    this.entryFee.setString("`" + feeArr[0]);
                if (formatArr[0] == "Ticket") {
                    if (String(feeArr[0]).split(":")[0] == 1)
                        this.entryFee.setString(formatArr[0]);
                    else
                        this.entryFee.setString(String(feeArr[0]).split(":")[0] + " " + formatArr[0]);
                }
            }
        }
        if (this._myChip)
            this.balanceChips.setString(this._myChip);
        else
            this.balanceChips.setString(this.gameRoom._myChip);

        if (this._myRank != -1)
            this.rank.setString(String(this._myRank + "/" + this._dataObj["maxRank"]));
        else
            this.rank.setString(String(this.gameRoom._myRank + "/" + this._dataObj["maxRank"]));

        var ptVal = this._dataObj["pointValue"];
        var maxLevel;
        if (this.gameRoom.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL))
            maxLevel = this.gameRoom.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL).value;

        var levelStr = String(this._dataObj["currLevel"] + "/" + maxLevel);
        this.level.setString(levelStr);
        this.pointValue.setString(String(ptVal));

        if (this._dataObj["rebuyChips"] && this._dataObj["rebuyChips"] != "") {

            var rebuy = String(this._dataObj["rebuyChips"]).split(":");
            var frmt = rebuy[0].split("|");
            if (frmt.length == 1)
                this.rebuy.setString("` " + String(rebuy[1]).split("|")[0]);
            else
                this.rebuy.setString("` " + rebuy[1].split("|")[0] + " OR " + rebuy[1].split("|")[1] + frmt[1]);
        }
        else
            this.rebuy.setString("N/A");

        var minChip = this._dataObj["nextLevelChips"];

        if (minChip && minChip != "0")
            this.minChip.setString(ptVal * 80 + "/" + minChip);
        else
            this.minChip.setString(String(ptVal * 80) + "/----");

    }
});