/**
 * Created by stpl on 9/16/2016.
 */
var PointGame = NormalBaseGame.extend({
    _mySeatIndex: null,
    _isBetPopUpEnable: null,
    _isNoBal: null,
    _waitingPlyrCount: null,

    ctor: function (room) {
        this._super(room);
        this.initRoom();

        this._mySeatIndex = -1;
        this._currentRound = 1;
        this._isBetPopUpEnable = false;
        this._isNoBal = false;
        this._waitingPlyrCount = 0;

        this.initHeader();

        this.GameDonotDealBtn = new GameDonotDealSprite(this);
        this.GameDonotDealBtn.setPosition(cc.p(720 * scaleFactor, 110 * scaleFactor));
        this.GameDonotDealBtn.hide(true);
        this.addChild(this.GameDonotDealBtn, 2);

        cc.eventManager.addListener(this.baseGameMouseListener.clone(), this.GameDonotDealBtn);
        cc.eventManager.addListener(this.baseGameTouchListener.clone(), this.GameDonotDealBtn);

        this.nextGameInfo = new cc.LabelTTF("Please wait, While next game start.", "RobotoRegular", 15 * 2 * scaleFactor, cc.TEXT_ALIGNMENT_CENTER);
        this.nextGameInfo.setPosition(cc.p(this.width / 2, this.height / 2 - 50 * scaleFactor - yMargin));
        this.nextGameInfo.setScale(0.75);
        this.addChild(this.nextGameInfo, 2, "nextGameInfo");
        this.nextGameInfo.setVisible(false);

        cc.log("Point game " + this.tableId + " is initialized");

        return true;
    },
    _joinRoom: function (seatPos) {
        this._mySeatIndex = seatPos;
        this.betCnfrmPopUp && this.betCnfrmPopUp.removeFromParent(true);
        this.betCnfrmPopUp = new BetConfirmation(this);
        this.addChild(this.betCnfrmPopUp, GameConstants.betConfirmationpopupZorder, "betCnfrmPopUp");
    },
    updatePoints: function (obj) {
        var profObj = this.getSeatObjByUser(obj["pId"]);
        if (profObj && profObj != -1) {
            if (obj["totPoints"])
                profObj.setScore(obj["totPoints"]);

            if (obj["rndPoints"])
                profObj.setRndPoints(obj["rndPoints"]);
        }
    },
    updateHeader: function () {
        var gameInfoTxt = "";
        var sessionId = "--";
        if(this._serverRoom.getVariable(SFSConstants._SESSION_ID))
            sessionId = this._serverRoom.getVariable(SFSConstants._SESSION_ID).value;
        if (this._serverRoom) {
            var gameType = this._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
            if (gameType.search("Best") != -1)
                gameType = gameType.split("-")[1];
            var turnType = this._serverRoom.getVariable(SFSConstants._TURNTYPE).value;
            gameInfoTxt = gameType + " - " + this._serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value + " Card |  Table ID: " + this._serverRoom.getVariable(SFSConstants._ROOMID).value + " |  Session ID: " + sessionId;
            this._gameInfoTF.setString(gameInfoTxt);
            this._isNoBal = false;
        }
        else {
            /* var gameType:String = _dummyObj[SFSConstants._GAMETYPE].toString();
             if (gameType.search("Best") != -1)
             gameType = gameType.split("-")[1];
             var turnType:String = _dummyObj[SFSConstants._TURNTYPE];
             gameInfoTxt.text = gameType + " - " + _dummyObj[SFSConstants.FLD_NUMOFCARD] + " Card |  Table ID: " + _dummyObj[SFSConstants._ROOMID].toString() + " |  Round: " + (this._currentRound == 0?"--":this._currentRound);
             */
        }
        this.updateHeaderPosition();
    },
    seatConfirmHandler: function (value) {
        this.waitingMC.setVisible(false);
        if (!this._leaveAllowed) {
            return;
        }
        var bet = parseFloat(value);
        var minBal = this._serverRoom.getVariable(SFSConstants._BET_AMOUNT).value;
        var maxBet = minBal * 10;
        var myBal = this.getMyAmount();
        if ((myBal >= bet) && (bet >= minBal) && (bet <= maxBet) && (this._mySeatIndex != -1)) {
            this.waitingMC.setString("Please Wait....");
            this.waitingMC.setVisible(true);

            var obj = {};
            obj[SFSConstants.FLD_SEATNO] = parseInt(this._mySeatIndex);
            obj[SFSConstants.FLD_AMOUNT] = bet + SFSConstants.doublePrecisionValue;
            obj[SFSConstants.FLD_IS_NO_BAL] = this._isNoBal;
            this.sendReqToServer(SFSConstants.CMD_CONFIRM, obj);
            this._isNoBal = false;
        }
        else {
            if (this.betCnfrmPopUp) {
                this.betCnfrmPopUp.removeToolTip();
                this.betCnfrmPopUp.errorMsg.setVisible(false);
            }
            if (myBal == 0 || myBal < bet) {
                if (this._serverRoom.getVariable(SFSConstants._CASH_OR_PROMO).value == "CASH") {
                    this.betCnfrmPopUp.createToolTip("CASH", bet);
                }
                else {
                    this.betCnfrmPopUp.createToolTip("PRACTICE", bet);
                }
            }
            else if (bet > maxBet) {
                this.betCnfrmPopUp.errorMsg.setString("Maximum amount for game is " + maxBet + " Chips.");
                this.betCnfrmPopUp.errorMsg.setVisible(true);
            }
            else if (bet < minBal) {
                this.betCnfrmPopUp.errorMsg.setString("Minimum amount for game is " + minBal + " Chips.");
                this.betCnfrmPopUp.errorMsg.setVisible(true);
            }
            else {
                this.betCnfrmPopUp.errorMsg.setString("Please place correct bet.");
                this.betCnfrmPopUp.errorMsg.setVisible(true);
            }
        }
    },
    instantPlayEnable: function (noOfPlyr) {
        this._super();
        this.watchingTxt.setVisible(false);
        if (this._myId != -1 || this._isWaiting)
            return;
        var noOfPl = noOfPlyr || this._serverRoom.getVariable(SFSConstants._SEATEDPLS).value.split(",").length;
        var enable = noOfPl == this._totalSeats;
        if (enable) {
            this._notfMsgStr = "You are currently watching this game. To play a game, please visit the lobby.\n";
            enable = this._isWatching;
        }
        else {
            this._notfMsgStr = "You are currently watching this game. To play a game, please join the seat.\n";
        }
        if (!enable) {
            this.insantPlayBtn && this.insantPlayBtn.removeFromParent(true);
            this.instantNotMC && this.instantNotMC.removeFromParent(true);
        }
        this.watchingTxt.setString(this._notfMsgStr);
        this.watchingTxt.setVisible(this._isWatching);

    },
    processFreeSeat: function (param) {
        if (!param)
            return;

        var name = param[SFSConstants.FLD_NAME];
        var obj = this.getSeatObjByUserName(name);

        if (obj) {

            window.clearInterval(this._userCardPanel.cardDistributeInterval);

            if (this._currentRoomState == SFSConstants.RS_MELD) {
                if (obj._playerId == this._currentTurnID) {
                    this._extraTimeToggle = false;
                }
            }
            if (this._currentRoomState == SFSConstants.RS_WATCH) {
                // this.removeFreeSeatPlayer(obj);
                /*if (obj._playerId == this._currentTurnID) {

                 }*/
            }

            if (obj.isWaiting)
                this._waitingPlyrCount -= 1;

            if (parseInt(obj._playerId) != this._myId)
                this.showNotification(obj.player_name + " left the table.");

            this.removeClosedCardForSpecificPlayer(obj);
            obj.leaveSeat();
            this.updatePlayerInfo(KHEL_CONSTANTS.USER_LEFT, obj, false, false, false, false, false);


            var leftPlStr;
            if (this._leftPlArr.indexOf(name) == -1)
                this._leftPlArr.push(name);

            leftPlStr = this._leftPlArr.join(",");
            if ((leftPlStr) && (leftPlStr != "") && (this._currentRoomState != SFSConstants.RS_RESULT)) {
                leftPlStr += " left the table!!";
                this.showNotification(leftPlStr);
            }
            this._notfMsgStr = "You are currently watching this game. To play a game, please join the seat.\n";
            this.watchingTxt.setString(this._notfMsgStr);
            this.insantPlayBtn && this.insantPlayBtn.removeFromParent(true);
            this.instantNotMC && this.instantNotMC.removeFromParent(true);
            // this.instantPlayEnable();
        }
        //todo enable join
    },
    cashBtnHandler: function (senderName) {

        this.waitingMC.setVisible(false);
        var reqStr = rummyData.path.paymentPage;

        if (this.noBalPopup && this.noBalPopup.isVisible()) {
            this.noBalPopup.removeFromParent(true);
            this.leaveRoomAndClearTab();
        }
        else if (senderName == "addCashBtn") {
            this.leaveRoomAndClearTab();
        }
        cc.sys.openURL(reqStr);

    },
    refillBtnHandler: function () {
        this.leaveRoomAndClearTab();
        var sfsObj = {
            "type": "promo"
        };
        sfsClient.sendReqFromLobby("getBal", sfsObj);
        //cmd:String="updatePlayerBalance",arg:String=""
    },
    leaveRoomAndClearTab: function () {
        this.leaveRoomByMyself();
        var sObj = {
            "cmd": "leaveRoom",
            "rId": this.roomId
        };
        applicationFacade.controlHandler(sObj);
    },
    noTableBal: function (dataObj) {
        this._isNoBal = true;
        this._mySeatIndex = parseInt(dataObj["seat"]);
        this._isBetPopUpEnable = true;
        this.betConfirmHandler();
    },
    getMyAmount: function () {
        var myBal;
        if (this._serverRoom.getVariable("cashOrPromo").value == "CASH")
            myBal = rummyData.cashBalance;
        else
            myBal = rummyData.promoBalance;
        return myBal;
    },
    processToss: function (data) {
        this._super(data);
        if (this._myId != -1) {
            this.disableAllChairs();
            //reArrangePopUpMC.visible = true;
        }
        else {
            this._isWatching = true;
            this._tabMc.setIcon(2);
        }
    },
    processCutTheDeckCmd: function (dataObj) {
        //todo this.waitingMC.setString("");
        this.initNewRound(dataObj);
        this.disableAllChairs();
        this._currentRound = 1;
        this.GameDonotDealBtn.setNext(true);

        if (this._myId == -1) {   // watching case
            this.GameDonotDealBtn.hide(true);
        } else {
            if (!this._isWaiting) {
                this.GameDonotDealBtn.hide(false);
            }
        }
        /*
         if (!this._isWaiting || this._myId != -1)
         this.GameDonotDealBtn.hide(false);*/

        if (this._currentRoomState == SFSConstants.RS_WATCH)
            if (this._myId != -1) {
                /*historyBtnMC.btn_mc.buttonMode = true;
                 handleBtnListener(settingMC.historyBtn, true, historyBtnHandler);*/
            }
            else {
                // historyBtnMC.btn_mc.buttonMode = false;
            }
    },
    handleResult: function (dataObj) {
        this._super(dataObj);
        this._userCardPanel.showConfirmPopup && this._userCardPanel.showConfirmPopup.removeFromParent(true);
        if (this._serverRoom.getVariable(SFSConstants._WILDCARD) && this._serverRoom.getVariable(SFSConstants._WILDCARD).value)
            this.result.setWildCard(this._serverRoom.getVariable(SFSConstants._WILDCARD).value);
        else
            this.result.setWildCard("");

        this.meldingPanel && this.meldingPanel.removeFromParent(true);
        this.leaveTablePopup && this.leaveTablePopup.removeFromParent(true);
        //todo this.wrongShowMC.text = "";
    },

    seatConfirmResponse: function (data, room) {
        this._super(data, room);
        var seatNo = parseInt(data[SFSConstants.FLD_SEATNO]);
        var userId = parseInt(data[SFSConstants._USERID]);
        var userScore = data[SFSConstants._NUM];
        var userName = data[SFSConstants.FLD_UNAME];
        var isConfirmed = data[SFSConstants.MSG].toLowerCase() == "confirmed";
        var isWaiting = false, noOfPlyr = null;

        if (data.hasOwnProperty(SFSConstants.FLD_IS_NEXT))
            isWaiting = data[SFSConstants.FLD_IS_NEXT];

        if (seatNo == this._mySeatIndex) {
            this.noBalPopup && this.noBalPopup.removeFromParent(true);
            this.clseBtnHandler();
        }

        if (this._currentRoomState == SFSConstants.RS_JOIN) {
            if (userName == rummyData.nickName) {
                this._myId = parseInt(userId);
                this._isWatching = false;
                this._isWaiting = isWaiting;
                this._isNext = isWaiting;
                applicationFacade._myUName = userName;
                applicationFacade._myId = this._myId;
                this.disableAllChairs();
                this.betCnfrmPopUp && this.betCnfrmPopUp.removeFromParent(true);
                this._mySeatIndex = -1;
                this.selfSeatPosition = seatNo;
                this._tabMc.setIcon(1);
                this.watchingTxt.setVisible(false);
                // this.showNotification();
            }
        } else {
            if (userName == rummyData.nickName) {
                this.disableAllChairs();
                this._isWatching = false;
                this._isWaiting = true;
                this._isNext = true;
                this.nextGameInfo.setString("Please wait, While next game start.");
                this.nextGameInfo.setVisible(true);

                applicationFacade._myUName = userName;
                this.showNotification();

                this.watchingTxt.setVisible(false);
                this._tabMc.setIcon(1);
                this.betCnfrmPopUp && this.betCnfrmPopUp.removeFromParent(true);
                this._mySeatIndex = -1;
                this.selfSeatPosition = seatNo;

                this.GameDonotDealBtn.hide(true);
                if (this._currentRoomState == SFSConstants.RS_WATCH) {
                    this.deckPanel.initPanel();
                    this.deckPanel.hideAllItems(false);
                }
            }
            else {
                if (this._myId != -1 && this._currentRoomState != SFSConstants.RS_GAMESTART) {
                    //seatObj.tsid = this.getShiftedSeatId(seatNo);
                }
            }
        }

        this._playerArray[(seatNo - 1) > -1 && seatNo - 1].updateInfo(seatNo, userId, userName, userScore, !1, !1, !1, !1, isWaiting);
        this._playerArray[(seatNo - 1) > -1 && seatNo - 1].takeSeat(userId, userName, userScore);

        // set score of player
        // this._playerArray[(seatNo - 1) > -1 && seatNo - 1]._isWaiting = isWaiting;
        this._playerArray[(seatNo - 1) > -1 && seatNo - 1].setScore(userScore);

        if (isWaiting == true) {
            this._playerArray[(seatNo - 1) > -1 && seatNo - 1].setIcon("Waiting", "WaitingIcon");
            this._waitingPlyrCount += 1;
        }

        if (this._isWaiting && this._currentRoomState == SFSConstants.RS_WATCH) {
            room.getVariable(SFSConstants._OD) && this.ODCards(room.getVariable(SFSConstants._OD).value);
            room.getVariable(SFSConstants._WILDCARD) && this.wildCard(room.getVariable(SFSConstants._WILDCARD).value);
        }


        if (this._isWatching) {
            if (isWaiting)
                noOfPlyr = room.getVariable(SFSConstants._SEATEDPLS).value.split(",").length + this._waitingPlyrCount;
            if (room.getVariable(SFSConstants._ROOMSTATE).value == SFSConstants.RS_GAMESTART)
                this.instantPlayEnable(noOfPlyr);
        }

        this.updatePlayerInfo(KHEL_CONSTANTS.USER_JOINED, this._playerArray[(seatNo - 1) > -1 && seatNo - 1], false, false, false, false, isWaiting);
    },
    clseBtnHandler: function () {
        this.betCnfrmPopUp && this.betCnfrmPopUp.removeFromParent(true);
        if (this._isNoBal) {
            this.sendReqToServer('noBalCancel', {});
        }
        this._mySeatIndex = -1;
    },
    noBalanceResp: function (msgStr, isCnfrm) {
        if (!this.noBalPopup) {
            this.noBalPopup = new NoBalPopup(msgStr, this);
            this.addChild(this.noBalPopup, GameConstants.noBalPop, "noBalPopup");
        }

        if (isCnfrm == "true") {
            this.noBalPopup.hide(this.noBalPopup.okButton, true);
            this.noBalPopup.hide(this.noBalPopup.yesButton, false);
            this.noBalPopup.hide(this.noBalPopup.noButton, false);
        }
        else {
            this.noBalPopup.hide(this.noBalPopup.okButton, false);
            this.noBalPopup.hide(this.noBalPopup.yesButton, true);
            this.noBalPopup.hide(this.noBalPopup.noButton, true);
        }
    },
    processRoomStatus: function (dataObj, room) {
        this._super(dataObj, room);
        // this.GameDonotDealBtn.hide(false);

        if (this._currentRoomState != SFSConstants.RS_JOIN) {
            if (this._myId != -1) {
                //set No deal btn
                /*todo NoDealChckBox.visible = true;
                 NoDealChckBox.check.gotoAndStop(String(dataObj.getBool("isNext")) == "true"?2:1);*/
            }
            else {
                if (this._isWatching) {
                    this._tabMc.setIcon(2);
                }
            }
        }
        if (this._currentRoomState == SFSConstants.RS_WATCH) {
            this.deckPanel.setWildCard();
            this.noDealEnable(dataObj['isNext']);
            if (this._myId != -1)
                this.GameDonotDealBtn.hide(false);
        }
        if (this._currentRoomState == SFSConstants.RS_RESULT) {
            this.noDealEnable(dataObj['isNext']);
            this.instantPlayEnable();
        }
        if (this._currentRoomState == SFSConstants.RS_GAMESTART) {
            if (this._myId != -1 && this._isWatching)
                this.pickWaitMC && this.pickWaitMC.setVisible(false);
        }
    },
    noDealEnable: function (isEnable) {
        if (this._myId == -1)
            isEnable = false;
        this.GameDonotDealBtn.setNext(isEnable);
    },
    removeFreeSeatPlayer: function (player) {
        var playerId = player._playerId;
        for (var i = 0; i < this._playerArray.length; i++) {
            if (playerId == this._playerArray[i]._playerId) {

                if (this._playerArray[i]._seatPosition) {
                    var closedCardObj = this.getClosedCardById(this._playerArray[i]._seatPosition);
                    closedCardObj && closedCardObj.removeFromParent(true);
                }
                // this.removeChild(this._playerArray[i]);
                // this._playerArray.splice(i, 1);
                return;
            }
        }
    },
    betConfirmHandler: function () {
        this.betCnfrmPopUp = new BetConfirmation(this);
        this.addChild(this.betCnfrmPopUp, GameConstants.betConfirmationpopupZorder, "betCnfrmPopUp");
    },
    processRoundOver: function (dataObj) {
        this.removeClosedCard();
        this._isWaiting = false;
        // this.stopAllTimers();
        this.result && this.result.removeFromParent(true);
        this.invalidDeclr && this.invalidDeclr.removeFromParent();
        this.GameDonotDealBtn && this.GameDonotDealBtn.hide(true);
        this._userCardPanel && this._userCardPanel.hideAllItems(true);

        for (var i = 0; i < this._playerArray.length; i++) {
            this._playerArray[i].hideTimer();
        }
        this._tabMc.setIcon(1);
        this._currentTurnSeat = null;
        this.confirmationPopup && this.confirmationPopup.removeFromParent(true);
        this.confirmationPopup = null;
        this.showValidity && this.showValidity.hide(true);
        this._userCardPanel.tempCardBuffer = null;
        this.updateSeatOnRoundOver(dataObj);
    },
    updateSeatOnRoundOver: function (dataObj) {

        this._myId = -1;
        this.nextGameInfo.setVisible(false);
        this.instantNotMC && this.instantNotMC.setVisible(false);
        this.watchingTxt.setVisible(false);
        this.betCnfrmPopUp && this.betCnfrmPopUp.removeFromParent(true);
        this._currentTurnSeat && this._currentTurnSeat.hideTimer();
        this.deckPanel && this.deckPanel.hideAllItems(true);

        /* for (var i = 0; i < this._playerArray.length; i++) {
         if (this._playerArray[i]._sittingStatus != 0 || this._playerArray[i]._isWaiting) {
         this._playerArray[i].removeIcon();
         this._playerArray[i].leaveSeat();
         }
         }
         this._playerArray = [];*/

        this.initRoom();

        var plsStr = dataObj["plsStr"];
        var plsArr = plsStr.split(",");
        var plSeatArr = [];

        if (plsArr) {
            for (var i = 0; i < plsArr.length; i++) {
                plSeatArr = plsArr[i].split(":");

                var plId = plSeatArr[1];
                var uName = plSeatArr[2];
                var drp = plSeatArr[4] == "true";
                var disConn = plSeatArr[5] == "true";
                var autoPl = plSeatArr[6] == "true";
                var wrngShw = plSeatArr[7] == "true";
                if (plSeatArr[8])
                    var isNext = plSeatArr[8] == "true";

                if (uName == applicationFacade._myUName) {
                    this._myId = parseInt(plId);
                    this._dropped = drp;
                    this.disableAllChairs();
                }
                if (isNext) {
                    this._myId = -1;
                    this.nextGameInfo.setString("Please wait, While next game start.");
                    this.nextGameInfo.setVisible(true);
                    this.showNotification();
                    this._tabMc.setIcon(1);
                    this.instantPlayEnable();
                }
                if (wrngShw) {
                    this._dropped = true;
                    /*todo wrongShowMC.text = "You have placed a wrong show";
                     wrongShowMC.visible = true;*/
                }

                this._playerArray[parseInt(plSeatArr[0] - 1)] && this._playerArray[parseInt(plSeatArr[0] - 1)].updateInfo(
                    plSeatArr[0],
                    plSeatArr[1],
                    plSeatArr[2],
                    plSeatArr[3],
                    plSeatArr.length > 4 && (plSeatArr[4] == "true"),
                    plSeatArr.length > 5 && (plSeatArr[5] == "true"),
                    plSeatArr.length > 6 && (plSeatArr[6] == "true"),
                    plSeatArr.length > 7 && (plSeatArr[7] == "true"),
                    plSeatArr.length > 8 && (plSeatArr[8] == "true"));
                this._playerArray[parseInt(plSeatArr[0] - 1)] && this._playerArray[parseInt(plSeatArr[0] - 1)].takeSeat(plSeatArr[1], plSeatArr[2], plSeatArr[3]);
                this._playerArray[parseInt(plSeatArr[0] - 1)] && this._playerArray[parseInt(plSeatArr[0] - 1)].setProfilePic(null);
                this._playerArray[parseInt(plSeatArr[0] - 1)] && this._playerArray[parseInt(plSeatArr[0] - 1)].setScore(plSeatArr[3]);

            }
        }
        this.showWaiting();

        if (this._myId != -1) {
            this._isWatching = false;
            this._isWaiting = false;
        }
    }
    //todo setDefaultBet
});
var GameDonotDealSprite = cc.Sprite.extend({
    _gameRoom: null,
    _hovering: null,
    ctor: function (context) {
        this._super(spriteFrameCache.getSpriteFrame("DonotDeal.png"));

        this._gameRoom = context;
        this.setName("GameDonotDealBtn");
        this._hovering = false;
        this._isEnabled = true;
        this._isChecked = false;
        this.setVisible(false);

        return true;
    },
    hover: function () {
    },
    hide: function (bool) {
        this.setVisible(!bool);
        this._isEnabled = !bool
    },
    initDonotDealSprite: function () {
        this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("DonotDeal.png"));
        this._isChecked = false;
    },
    setNext: function (boolean) {
        if (boolean) {
            this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("DonotDeal.png"));
            this._isChecked = false;
        } else {
            this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("DonotDealCheck.png"));
            this._isChecked = true;
        }
    },
    hideToggle: function () {
        // this._isEnabled = !this._isEnabled;
        this._isChecked = !this._isChecked;
    },
    GameDonotDealBtnHandler: function () {
        this.hideToggle();
        this._isChecked ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("DonotDealCheck.png")) : this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("DonotDeal.png"));

        var _isNext;
        _isNext = this._isChecked == true ? false : true;
        var obj = {};
        obj["isNext"] = _isNext;
        this._gameRoom.sendReqToServer(SFSConstants.CMD_PLAY_RESP, obj);
    },

});