/**
 * Created by stpl on 1/16/2017.
 */
var TimeAllPrize = cc.LayerColor.extend({
    ctor: function (context, room) {

        this._super(cc.color(255, 255, 255));
        this.setContentSize(cc.size(720 * scaleFactor, 390 * scaleFactor));
        this.setPosition(cc.p(220 * scaleFactor, 3 * scaleFactor));

        this.trnmntRoom = room;

        var label;
        label = new cc.LabelTTF("Cash Prizes", "RobotoBold", 25 * scaleFactor);
        label.setScale(0.7);
        label.setAnchorPoint(0, 1);
        label.setPosition(cc.p(1 * scaleFactor, this.height));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label);

        var state = String(this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.STATUS).value.toUpperCase());
        var title = "Leaderboard";
        if (state == TOURNAMENT_CONSTANTS.COMPLETED) {
            title = "Winners";
        }
        label = new cc.LabelTTF(title, "RobotoBold", 25 * scaleFactor);
        label.setAnchorPoint(0, 1);
        label.setScale(0.7);
        label.setPosition(cc.p(312 * scaleFactor, this.height));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label);

        this.initCashPrize();
        this.initLeatherBoard();

    },
    updatePrizeStructure: function () {
        if (this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.PRIZE_STRUCT)) {
            var przeStr = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.PRIZE_STRUCT).value;
            var przeArr = przeStr.split(";");
            var strip;
            this.cashPrizeListView.removeAllChildrenWithCleanup(true);

            for (var i = 0; i < przeArr.length; i++) {
                strip = this.getCashPrizeBar(przeArr[i], i);
                this.cashPrizeListView.pushBackCustomItem(strip);
            }
        }
    },
    initCashPrize: function () {

        var rect = new cc.DrawNode();
        rect.drawRect(cc.p(1 * scaleFactor, 332 * scaleFactor), cc.p(286 * scaleFactor, 360 * scaleFactor), cc.color(222, 222, 222), 1, cc.color(cc.color(222, 222, 222)));
        this.addChild(rect, 1);

        var rect2 = new cc.DrawNode();
        rect2.drawRect(cc.p(1 * scaleFactor, 60 * scaleFactor), cc.p(286 * scaleFactor, 331 * scaleFactor), cc.color(235, 235, 235, 0), 1, cc.color(cc.color(235, 235, 235)));
        this.addChild(rect2, 1);

        var label = new cc.LabelTTF("Rank", "RobotoBold", 18 * scaleFactor);
        label.setScale(0.7);
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(10 * scaleFactor, 345 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label, 2);

        label = new cc.LabelTTF("Prize", "RobotoBold", 18 * scaleFactor);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(110 * scaleFactor, 345 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label, 2);

        // on header strip
        var verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(96 * scaleFactor, 330 * scaleFactor), cc.p(96 * scaleFactor, 360 * scaleFactor), 0.5, cc.color(255, 255, 255));
        this.addChild(verticalLine, 2);

        // on ListView
        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(96 * scaleFactor, 60 * scaleFactor), cc.p(96 * scaleFactor, 360 * scaleFactor), 0.5, cc.color(220, 220, 220));
        this.addChild(verticalLine, 1);

        this.cashPrizeListView = new ccui.ListView();
        this.cashPrizeListView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        this.cashPrizeListView.setGravity(ccui.ListView.GRAVITY_CENTER_HORIZONTAL);
        this.cashPrizeListView.setTouchEnabled(true);
        this.cashPrizeListView.setItemsMargin(0);
        this.cashPrizeListView.setScrollBarAutoHideEnabled(true);
        // this.cashPrizeListView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        // this.cashPrizeListView.setBackGroundColor(cc.color(60, 25, 90));
        this.cashPrizeListView.setContentSize(cc.size(285 * scaleFactor, 270 * scaleFactor));
        this.cashPrizeListView.setPosition(cc.p(1 * scaleFactor, 60 * scaleFactor));
        this.addChild(this.cashPrizeListView);

        if (this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.PRIZE_STRUCT)) {
            var przeStr = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.PRIZE_STRUCT).value;
            var przeArr = przeStr.split(";");
            var strip;
            this.cashPrizeListView.removeAllChildrenWithCleanup(true);

            for (var i = 0; i < przeArr.length; i++) {
                strip = this.getCashPrizeBar(przeArr[i], i);
                this.cashPrizeListView.pushBackCustomItem(strip);
            }
        }
    },
    getCashPrizeBar: function (param, index) {

        var bar = ccui.Layout.extend({
            ctor: function (context, przeArr, index) {
                this._super();
                var width = context.cashPrizeListView.width - 2 * scaleFactor;
                this.setContentSize(cc.size(width, 30 * scaleFactor));
                this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);

                if (index % 2 == 0)
                    this.setBackGroundColor(cc.color(255, 255, 255));
                else
                    this.setBackGroundColor(cc.color(235, 235, 235));

                var rankPrizeStr;
                var prizeStr;
                var str;
                var tempArr;

                str = przeArr;
                tempArr = str.split(",");

                var label;
                label = new cc.LabelTTF(tempArr[0], "RobotoRegular", 16 * scaleFactor); //index + 1
                label.setScale(0.7);
                label.setAnchorPoint(0, 0.5);
                label.setPosition(cc.p(10 * scaleFactor, this.height / 2));
                label.setColor(cc.color(0, 0, 0));
                this.addChild(label, 1);

                rankPrizeStr = tempArr[1];
                if (rankPrizeStr)
                    prizeStr = PrizeStringCalculator.calculatePrzStr(rankPrizeStr);

                label = new cc.LabelTTF(prizeStr, "RupeeFordian", 16 * scaleFactor);
                label.setAnchorPoint(0, 0.5);
                label.setScale(0.7);
                label.setPosition(cc.p(110 * scaleFactor, this.height / 2));
                label.setColor(cc.color(0, 0, 0));
                this.addChild(label, 1);
            }
        });
        return new bar(this, param, index);
    },
    initLeaderBoardListView: function () {
        var state = String(this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.STATUS).value.toUpperCase());
        if (state == TOURNAMENT_CONSTANTS.COMPLETED) {
            var verticalLine = new cc.DrawNode();
            verticalLine.drawSegment(cc.p(365 * scaleFactor, 60 * scaleFactor), cc.p(365 * scaleFactor, 331 * scaleFactor), 0.5, cc.color(220, 220, 220));
            this.addChild(verticalLine, 1);

            verticalLine = new cc.DrawNode();
            verticalLine.drawSegment(cc.p(456 * scaleFactor, 60 * scaleFactor), cc.p(456 * scaleFactor, 331 * scaleFactor), 0.5, cc.color(220, 220, 220));
            this.addChild(verticalLine, 1);

            verticalLine = new cc.DrawNode();
            verticalLine.drawSegment(cc.p(526 * scaleFactor, 60 * scaleFactor), cc.p(526 * scaleFactor, 331 * scaleFactor), 0.5, cc.color(220, 220, 220));
            this.addChild(verticalLine, 1);
        } else {
            // on ListView
            var verticalLine = new cc.DrawNode();
            verticalLine.drawSegment(cc.p(397 * scaleFactor, 60 * scaleFactor), cc.p(397 * scaleFactor, 331 * scaleFactor), 0.5, cc.color(220, 220, 220));
            this.addChild(verticalLine, 1);

            verticalLine = new cc.DrawNode();
            verticalLine.drawSegment(cc.p(526 * scaleFactor, 60 * scaleFactor), cc.p(526 * scaleFactor, 331 * scaleFactor), 0.5, cc.color(220, 220, 220));
            this.addChild(verticalLine, 1);
        }

        this.leatherboardListView = new ccui.ListView();
        this.leatherboardListView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        this.leatherboardListView.setGravity(ccui.ListView.GRAVITY_CENTER_HORIZONTAL);
        this.leatherboardListView.setTouchEnabled(true);
        this.leatherboardListView.setItemsMargin(0);
        this.leatherboardListView.setScrollBarAutoHideEnabled(true);
        this.leatherboardListView.setContentSize(cc.size(409 * scaleFactor, 270 * scaleFactor));
        this.leatherboardListView.setPosition(cc.p(310 * scaleFactor, 60 * scaleFactor));
        // this.leatherboardListView.setBounceEnabled(true);
        // this.leatherboardListView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        // this.leatherboardListView.setBackGroundColor(cc.color(60, 25, 90));
        this.addChild(this.leatherboardListView);

    },
    getLeaderBoardHeader: function (state) {

        if (state == TOURNAMENT_CONSTANTS.COMPLETED) {
            var label = new cc.LabelTTF("Rank", "RobotoBold", 18 * scaleFactor);
            label.setScale(0.7);
            label.setAnchorPoint(0, 0.5);
            label.setPosition(cc.p(320 * scaleFactor, 345 * scaleFactor));
            label.setColor(cc.color(0, 0, 0));
            this.addChild(label, 2);

            label = new cc.LabelTTF("Player", "RobotoBold", 18 * scaleFactor);
            label.setAnchorPoint(0, 0.5);
            label.setScale(0.7);
            label.setPosition(cc.p(375 * scaleFactor, 345 * scaleFactor));
            label.setColor(cc.color(0, 0, 0));
            this.addChild(label, 2);

            label = new cc.LabelTTF("Chips", "RobotoBold", 18 * scaleFactor);
            label.setAnchorPoint(0, 0.5);
            label.setScale(0.7);
            label.setPosition(cc.p(465 * scaleFactor, 345 * scaleFactor));
            label.setColor(cc.color(0, 0, 0));
            this.addChild(label, 2);

            label = new cc.LabelTTF("Prize", "RobotoBold", 18 * scaleFactor);
            label.setAnchorPoint(0, 0.5);
            label.setScale(0.7);
            label.setPosition(cc.p(540 * scaleFactor, 345 * scaleFactor));
            label.setColor(cc.color(0, 0, 0));
            this.addChild(label, 2);

            // on header strip
            var verticalLine = new cc.DrawNode();
            verticalLine.drawSegment(cc.p(365 * scaleFactor, 330 * scaleFactor), cc.p(365 * scaleFactor, 360 * scaleFactor), 0.5, cc.color(255, 255, 255));
            this.addChild(verticalLine, 2);

            verticalLine = new cc.DrawNode();
            verticalLine.drawSegment(cc.p(456 * scaleFactor, 330 * scaleFactor), cc.p(456 * scaleFactor, 360 * scaleFactor), 0.5, cc.color(255, 255, 255));
            this.addChild(verticalLine, 2);

            verticalLine = new cc.DrawNode();
            verticalLine.drawSegment(cc.p(526 * scaleFactor, 330 * scaleFactor), cc.p(526 * scaleFactor, 360 * scaleFactor), 0.5, cc.color(255, 255, 255));
            this.addChild(verticalLine, 2);
        } else {
            var label = new cc.LabelTTF("Rank", "RobotoBold", 18 * scaleFactor);
            label.setScale(0.7);
            label.setAnchorPoint(0, 0.5);
            label.setPosition(cc.p(330 * scaleFactor, 345 * scaleFactor));
            label.setColor(cc.color(0, 0, 0));
            this.addChild(label, 2);

            label = new cc.LabelTTF("Player", "RobotoBold", 18 * scaleFactor);
            label.setAnchorPoint(0, 0.5);
            label.setScale(0.7);
            label.setPosition(cc.p(415 * scaleFactor, 345 * scaleFactor));
            label.setColor(cc.color(0, 0, 0));
            this.addChild(label, 2);

            label = new cc.LabelTTF("Chips", "RobotoBold", 18 * scaleFactor);
            label.setAnchorPoint(0, 0.5);
            label.setScale(0.7);
            label.setPosition(cc.p(557 * scaleFactor, 345 * scaleFactor));
            label.setColor(cc.color(0, 0, 0));
            this.addChild(label, 2);

            // on header strip
            var verticalLine = new cc.DrawNode();
            verticalLine.drawSegment(cc.p(397 * scaleFactor, 330 * scaleFactor), cc.p(397 * scaleFactor, 360 * scaleFactor), 0.5, cc.color(255, 255, 255));
            this.addChild(verticalLine, 2);

            verticalLine = new cc.DrawNode();
            verticalLine.drawSegment(cc.p(526 * scaleFactor, 330 * scaleFactor), cc.p(526 * scaleFactor, 360 * scaleFactor), 0.5, cc.color(255, 255, 255));
            this.addChild(verticalLine, 2);
        }
    },
    initLeatherBoard: function () {

        var rect = new cc.DrawNode();
        rect.drawRect(cc.p(310 * scaleFactor, 332 * scaleFactor), cc.p(this.width - 1 * scaleFactor, 360 * scaleFactor), cc.color(222, 222, 222), 1, cc.color(cc.color(235, 235, 235)));
        this.addChild(rect, 1);

        var rect2 = new cc.DrawNode();
        rect2.drawRect(cc.p(310 * scaleFactor, 60 * scaleFactor), cc.p(this.width - 1 * scaleFactor, 360 * scaleFactor),
            cc.color(235, 235, 235, 0), 1, cc.color(cc.color(235, 235, 235)));
        this.addChild(rect2, 1);


        var state = String(this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.STATUS).value.toUpperCase());

        this.getLeaderBoardHeader(state);

        var winnerPlPosStr = "";
        if (this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.WINNER_PLR_POS))
            winnerPlPosStr = String(this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.WINNER_PLR_POS).value);

        var winnerPlArr = winnerPlPosStr.split(";");
        var strip;
        var winnerStr;
        var winnerArr;
        var i = 0;

        if (state == TOURNAMENT_CONSTANTS.RUNNING) {
            this.initLeaderBoardListView();
            this.leatherboardListView.removeAllChildrenWithCleanup(true);

            for (i = 0; i < winnerPlArr.length; i++) {
                winnerStr = winnerPlArr[i];
                if (winnerStr) {
                    winnerArr = winnerStr.split(",");
                    strip = new LeaderPlayerStrip(this, winnerArr, i);
                    this.leatherboardListView.pushBackCustomItem(strip);
                }
            }
        }
        else if (state == TOURNAMENT_CONSTANTS.COMPLETED) {
            this.initLeaderBoardListView();
            this.leatherboardListView.removeAllChildrenWithCleanup(true);

            for (i = 0; i < winnerPlArr.length; i++) {
                winnerStr = winnerPlArr[i];
                if (winnerStr) {
                    winnerArr = winnerStr.split(",");
                    strip = new LeaderWinnerStrip(this, winnerArr, i);
                    this.leatherboardListView.pushBackCustomItem(strip);
                }
            }
        }
        else if (state.toUpperCase() == TOURNAMENT_CONSTANTS.CANCELLED) {
            // timeBoardHeader.gotoAndStop(1);
            // timeBoardHeader.msgMC.visible = false;
        } else {
            this.showLeaderBoardMsg();
        }

        /*playerScrPane.x = timeBoardHeader.refPt.x;
         playerScrPane.y = timeBoardHeader.refPt.y;
         playerScrPane.setSize(timeBoardHeader.refPt.width, 270);
         playerScrPane.source = timeBoardHeader.refPt;
         timeBoardHeader.addChild(playerScrPane);
         prizeBoardHeaderMC.boardReftPt.addChild(timeBoardHeader);*/


    },
    showLeaderBoardMsg: function () {
        this.timeBoardHeader = new ToolTip(cc.size(171 * scaleFactor, 46 * scaleFactor), cc.color(255, 238, 215));
        this.timeBoardHeader.setText("Please Wait while tournament Start");
        this.timeBoardHeader.setPosition(cc.p(515 * scaleFactor, 196 * scaleFactor));
        this.addChild(this.timeBoardHeader, 10, "timeBoardHeader");
    },
    getLeaderWinnerStrip: function () {

    },
    setWinnerPosition: function () {

    }
});

var LeaderboardBar = ccui.Layout.extend({
    _labelArr: null,
    ctor: function (context, winnerArr, index) {
        this._super();
        this._labelArr = [];

        var width = context.leatherboardListView.width - 2 * scaleFactor;
        this.setContentSize(cc.size(width, 30 * scaleFactor));
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);

    },
    setState: function (index) {
        var color;
        if (index == 1) {
            this.setBackGroundColor(cc.color(255, 255, 255));
            color = cc.color(0, 0, 0);
        }
        else if (index == 2) {
            this.setBackGroundColor(cc.color(235, 235, 235));
            color = cc.color(0, 0, 0);
        }
        else {
            this.setBackGroundColor(cc.color(124, 141, 7));
            color = cc.color(255, 255, 255);
        }
        for (var i = 0; i < this._labelArr.length; i++) {
            this._labelArr[i].setColor(color);
        }
    }
});
var LeaderPlayerStrip = LeaderboardBar.extend({
    ctor: function (context, winnerArr, index) {
        this._super(context, winnerArr, index);
        // rank
        var label;
        label = new cc.LabelTTF(String(winnerArr[0]), "RobotoRegular", 16 * scaleFactor);
        label.setScale(0.7);
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(20 * scaleFactor, this.height / 2));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label, 1);
        this._labelArr.push(label);

        // player
        label = new cc.LabelTTF(String(winnerArr[1]), "RobotoRegular", 16 * scaleFactor);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(105 * scaleFactor, this.height / 2));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label, 1);
        this._labelArr.push(label);

        // chip
        label = new cc.LabelTTF(String(winnerArr[2]), "RobotoRegular", 16 * scaleFactor);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(245 * scaleFactor, this.height / 2));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label, 1);
        this._labelArr.push(label);


        if (String(winnerArr[1]) == applicationFacade._myUName)
            this.setState(3);
        else
            this.setState(parseInt(index % 2) + 1);
    }
});
var LeaderWinnerStrip = LeaderboardBar.extend({
    ctor: function (context, winnerArr, index) {
        this._super(context, winnerArr, index);

        // rank
        var label;
        label = new cc.LabelTTF(String(winnerArr[0]), "RobotoRegular", 16 * scaleFactor);
        label.setScale(0.7);
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(10 * scaleFactor, this.height / 2));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label, 1);
        this._labelArr.push(label);

        // player
        var playerName = String(winnerArr[1]);
        if (playerName.length > 12) {
            playerName = playerName.substring(0, 12) + "...";
        }
        label = new cc.LabelTTF(playerName, "RobotoRegular", 16 * scaleFactor);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(70 * scaleFactor, this.height / 2));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label, 1);
        this._labelArr.push(label);

        // chips
        label = new cc.LabelTTF(String(winnerArr[2]), "RobotoRegular", 16 * scaleFactor);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(160 * scaleFactor, this.height / 2));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label, 1);
        this._labelArr.push(label);

        // prize
        var prize = PrizeStringCalculator.calculatePrzStr(winnerArr[3]);
        label = new cc.LabelTTF(prize, "RupeeFordian", 16 * scaleFactor);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(230 * scaleFactor, this.height / 2));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label, 1);
        this._labelArr.push(label);

        if (String(winnerArr[1]) == applicationFacade._myUName)
            this.setState(3);
        else
            this.setState(parseInt(index % 2) + 1);
    }
});