/**
 * Created by stpl on 4/5/2017.
 */
var CardRuletab = ccui.Layout.extend({
    _isEnabled: null,
    _hovering: null,
    ctor: function (buttonName, circleText, msg) {
        this._super();
        this.setName(buttonName);
        this._isEnabled = true;
        this._hovering = false;

        this.setBackGroundColorType(ccui.Layout.BG_COLOR_GRADIENT);
        this.setBackGroundColor(cc.color(253, 253, 253), cc.color(225, 226, 226));
        this.setContentSize(cc.size(228 * scaleFactor, 65 * scaleFactor));

        this.circle = new cc.Sprite(spriteFrameCache.getSpriteFrame("ValidToggleBtn.png"));
        this.circle.setColor(cc.color(255, 255, 255));
        this.circle.setPosition(cc.p(this.width / 2, this.height));
        this.circle.setCascadeColorEnabled(false);
        this.addChild(this.circle, 2);

        this.text = new cc.LabelTTF(circleText, "RobotoRegular", 11 * 2 * scaleFactor);
        this.text.setScale(0.5);
        this.text.setPosition(cc.p(this.circle.width / 2, this.circle.height / 2));
        this.circle.addChild(this.text, 1);

        this.drawpoly1 = new cc.DrawNode();
        var vertexArr1 = [];
        vertexArr1.push(cc.p(0, 0), cc.p(0, this.height), cc.p(this.width, this.height), cc.p(this.width, 0), cc.p(this.width / 2, -25 * scaleFactor));
        this.drawpoly1.drawPoly(vertexArr1, cc.color(196, 143, 2), 0.3, cc.color(196, 143, 2));
        this.drawpoly1.setVisible(false);
        this.addChild(this.drawpoly1, 1);

        this.messageTxt = new cc.LabelTTF(msg, "RobotoRegular", 14 * 2 * scaleFactor);
        this.messageTxt.setScale(0.5);
        this.messageTxt.setColor(cc.color(0, 0, 0));
        this.messageTxt.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.messageTxt._setBoundingWidth(2 * this.width - 10 * scaleFactor);
        this.messageTxt.setPosition(cc.p(this.width / 2, this.height / 2));
        this.messageTxt.setColor(cc.color(101, 87, 84));
        this.addChild(this.messageTxt, 2);

        return true;
    },
    hover: function (bool) {
        if (this._isEnabled) {
            if (bool) {
                this.circle.setColor(cc.color(49, 84, 104));
                this.messageTxt.setColor(cc.color(255, 255, 255));
            }
            else {
                this.circle.setColor(cc.color(255, 255, 255));
                this.messageTxt.setColor(cc.color(101, 87, 84));
            }
            this.drawpoly1.setVisible(bool);
        }
    },
    pauseListener: function (bool) {
        this._isEnabled = !bool;
    }
});


