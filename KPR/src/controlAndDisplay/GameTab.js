/**
 * Created by stpl on 10/14/2016.
 */
"use strict";
var GameTab = ccui.Layout.extend({
    _controlAndDisplay: null,
    _roomId: null,
    subMiniTable: null,
    timerTxt: null,
    totCard: null,
    defaultColor: null,
    _iconImage: null,
    iconExtraMC: null,

    ctor: function (rid, string, layoutColor, controlAndDisplay, totCard) {
        this._super();

        this._roomId = rid;
        this._controlAndDisplay = controlAndDisplay;
        this.totCard = totCard;
        this.defaultColor = layoutColor;

        var tabHeight = this._controlAndDisplay.buttomTabHeight,
            tabWidth = 88 * scaleFactor,
            tabMargin = 14 * scaleFactor,
            fontSize = 11 * scaleFactor,
            fontFamily = "RobotoRegular";

        this.label = new cc.LabelTTF(string, "RupeeFordian", fontSize * 2);
        this.label.setName(string.replace(/ /g, "") + "Label");
        this.label.setScale(0.41, 0.43);

        this.setName(string.replace(/ /g, ""));
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.setContentSize(cc.size(tabWidth + tabMargin, tabHeight + 2));
        this.setBackGroundColor(layoutColor);


        var lay = new ccui.Layout();
        lay.setContentSize(this.getContentSize());
        lay.setClippingEnabled(true);
        this.addChild(lay, 2);
        this.label.setPosition(cc.p(lay.width / 2, lay.height / 2 - yMargin));
        lay.addChild(this.label);

        this.subMiniTable = new SubMiniTable();
        this.subMiniTable.setAnchorPoint(0.5, 0);
        this.addChild(this.subMiniTable, 0);
        this.subMiniTable.setVisible(false);
        this.subMiniTable.setPosition(cc.p(this.width / 2, this.height + this.height / 2));

        this.closeBtn = new cc.Sprite(spriteFrameCache.getSpriteFrame("closeBlack.png"));
        this.addChild(this.closeBtn, 0, "closeBtn");
        this.closeBtn.setPosition(this.width, this.height);

        this.glow = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("tabGlow.png"));
        this.glow.setPosition(this.width / 2 + 0.5, this.height / 2);
        this.glow.setContentSize(cc.size(tabWidth + tabMargin + 25 * scaleFactor, tabHeight + 2 + 25 * scaleFactor));
        this.glow.setVisible(false);
        this.addChild(this.glow, -2, "glow");

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        return true;
    },
    setBgColor: function (color) {
        this.defaultColor = color;
        this.setBackGroundColor(color);
    },
    startGlow: function (tag) {
        // cc.log("Inside glow: " + tag);
        if (tag == 3) {
            //add glow
            this.glow.stopAllActions();
            this.glow.setVisible(true);
            if (this.totCard == 13) {
                this.setBackGroundColor(cc.color(105, 128, 20));
                this.glow.setColor(cc.color(199, 222, 47));
            } else {
                this.setBackGroundColor(cc.color(164, 99, 0));
                this.glow.setColor(cc.color(237, 199, 67));
            }
            this.glow.runAction(cc.sequence(cc.fadeIn(0.5), cc.fadeOut(0.5)).repeatForever());
        }
        else {
            //remove glow
            this.setBackGroundColor(this.defaultColor);
            this.glow.stopAllActions();
            this.glow.setVisible(false);
        }
    },
    setIcon: function (tag) {
        this.timerTxt && this.timerTxt.removeFromParent(true);
        this.iconExtraMC && this.iconExtraMC.removeFromParent(true);
        this._iconImage && this._iconImage.removeFromParent(true);
        this.iconExtraMC = null;
        var imgUrl = "";
        switch (tag) {
            case 2:
                this.getIcon("TabWatchIcon.png", false);
                break;
            case 3:
                this.getIcon("TabAutoPlayIcon.png", false);
                break;
            case 4:
                this.getIcon("TabTimer13.png", true);
                break;
            case 5:
                this.getIcon("TabTimer21.png", true);
                break;
        }
    },
    getIcon: function (spriteName, isText) {
        this._iconImage = new cc.Sprite(spriteFrameCache.getSpriteFrame(spriteName));
        this._iconImage.setPosition(cc.p(this.width / 2, this.height + 8 * scaleFactor));
        this.addChild(this._iconImage, 4);
        if (isText) {
            this.timerTxt = new cc.LabelTTF("1", "RobotoBold", 12 * 2 * scaleFactor, cc.TEXT_ALIGNMENT_CENTER);
            this.timerTxt.setName("timerTxt");
            this.timerTxt.setScale(0.5);
            this.timerTxt.setColor(cc.color(0, 0, 0));
            this.timerTxt.setPosition(cc.p(this._iconImage.width / 2, this._iconImage.height / 2 - yMargin));
            this._iconImage.addChild(this.timerTxt, 2);

            this.iconExtraMC = new cc.Sprite(spriteFrameCache.getSpriteFrame("TabPopUpExtraTime.png"));
            this.iconExtraMC.setPosition(cc.p(this._iconImage.width / 3 - 10 * scaleFactor, this._iconImage.height / 2));
            this.iconExtraMC.setVisible(false);
            this._iconImage.addChild(this.iconExtraMC, 2);
        }
    },
    initMouseListener: function () {
        this.tabMouseEventListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            swallowTouches: true,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    if (!this.subMiniTable.visible)
                        this.subMiniTable.setVisible(true);
                    return true;
                } else {
                    target.opacity = 255;
                    this.subMiniTable.setVisible(false);
                    return false;
                }
            }.bind(this)
        });
        this.tabMouseEventListener.retain();
        cc.eventManager.addListener(this.tabMouseEventListener, this);
        cc.eventManager.addListener(this.tabMouseEventListener.clone(), this.closeBtn);
    },
    initTouchListener: function () {
        this.tabTouchEventListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                target.setColor(cc.color(255, 255, 255));
                target.opacity = 255;
                applicationFacade._controlAndDisplay.closeAllTab();
                applicationFacade._lobby.closeAllTab();
                if (target.getName() == "closeBtn") {
                    this.leaveTable();
                } else {
                    this.setIcon(1);
                    this._controlAndDisplay.switchScreen(target._roomId);
                }
            }.bind(this),
        });
        cc.eventManager.addListener(this.tabTouchEventListener, this);
        cc.eventManager.addListener(this.tabTouchEventListener.clone(), this.closeBtn);
    },
    leaveTable: function () {
        applicationFacade.switchScreen(this._roomId);
        applicationFacade.handleLeaveTable(this);
    }
});