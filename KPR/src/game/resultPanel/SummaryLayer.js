/**
 * Created by stpl on 12/14/2016.
 */
var SummaryLayer = cc.Layer.extend({
    _result21: null,
    ctor: function (result21, params) {
        this._super();

        this._result21 = result21;

        this.setContentSize(cc.size(this._result21.width - 1 * scaleFactor, 470 * scaleFactor));

        var grey = new cc.DrawNode();
        grey.drawRect(cc.p(0, 0), cc.p(this.width - 1 * scaleFactor, 50 * scaleFactor), cc.color(102, 102, 102), 1, cc.color(102, 102, 102));
        grey.setPosition(cc.p(1, 470 - 50));
        this.addChild(grey, 0);

        var Player = new cc.LabelTTF("Player", "RobotoBold", 15 * 2 * scaleFactor);
        Player.setScale(0.5);
        Player.setPosition(cc.p(35 * scaleFactor, this.height - 24 * scaleFactor));
        this.addChild(Player, 3);

        var Result = new cc.LabelTTF("Result", "RobotoBold", 15 * 2 * scaleFactor);
        Result.setScale(0.5);
        Result.setPosition(cc.p(165 * scaleFactor, this.height - 24 * scaleFactor));
        this.addChild(Result, 3);

        var ValueCards = new cc.LabelTTF("Value Cards", "RobotoBold", 15 * 2 * scaleFactor);
        ValueCards.setScale(0.5);
        ValueCards.setPosition(cc.p(275 * scaleFactor, this.height - 24 * scaleFactor));
        this.addChild(ValueCards, 3);

        var ValueCardsScore = new cc.LabelTTF("Value Cards Score", "RobotoBold", 15 * 2 * scaleFactor);
        ValueCardsScore.setScale(0.5);
        ValueCardsScore._setBoundingWidth(115 * 2 * scaleFactor);
        ValueCardsScore.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        ValueCardsScore.setPosition(cc.p(550 * scaleFactor, this.height - 24 * scaleFactor));
        this.addChild(ValueCardsScore, 3);

        var PointsWon = new cc.LabelTTF("Points Won", "RobotoBold", 15 * 2 * scaleFactor);
        PointsWon.setScale(0.5);
        PointsWon.setPosition(cc.p(660 * scaleFactor, this.height - 24 * scaleFactor));
        this.addChild(PointsWon, 3);

        var totalScore = new cc.LabelTTF("Total Score", "RobotoBold", 15 * 2 * scaleFactor);
        totalScore.setScale(0.5);
        totalScore.setPosition(cc.p(780 * scaleFactor, this.height - 24 * scaleFactor));
        this.addChild(totalScore, 3);

        var amount = new cc.LabelTTF("Amount", "RobotoBold", 15 * 2 * scaleFactor);
        amount.setScale(0.5);
        amount.setPosition(cc.p(890 * scaleFactor, this.height - 24 * scaleFactor));
        this.addChild(amount, 3);

        this.addList(params);

    },
    addList: function (params) {

        for (var i = 0; i < parseInt(params["tot"]); i++) {

            var resultStrip = new Result_21_Summary_List(this._result21, params["pl" + (i + 1)], i);
            resultStrip.addValueCards(this._result21._valueCards[i]);

            var dy = (this.height - 50 * scaleFactor) - i * 70 * scaleFactor;
            resultStrip.setPosition(cc.p(1 * scaleFactor, dy - i));

            this.addChild(resultStrip, 5);
            cc.eventManager.addListener(this._result21.popupMouseListener.clone(), resultStrip);
        }
    }
});