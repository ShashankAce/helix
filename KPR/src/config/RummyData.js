/**
 * Created by stpl on 10/24/2016.
 */
var RummyData = cc.Class.extend({

    _className: "RummyData",
    levelDataFileName: "rummyDataFile",

    rummyPlaceHolders: null,
    tournament: null,
    flashVersion: null,
    DOB: null,
    removeLeaveTime: null,
    versioning: null,
    linkdinLink: null,
    postConnection: null,
    currency: null,
    umreadMail: null,
    lobbyHttpUrl: null,
    nickName: null,
    domainId: null,
    serverTime: null,
    path: null,
    userName: null,
    showPrivateTable: null,
    isLogin: null,
    apiVersion: null,
    marqueGroup: null,
    displaySubTab: null,
    regDate: null,
    promoBalance: null,
    preConnection: null,
    facebookLink: null,
    lobbyType: null,
    playerId: null,
    twitterLink: null,
    newsGroup: null,
    configPath: null,
    youTubeLink: null,
    displayTab: null,
    language: null,
    mixpanelToken: null,
    token: null,
    cashBalance: null,
    blogLink: null,
    mixpanelData: null,

    ctor: function (prop) {

        var name;
        for (name in prop) {
            this[name] = prop[name];
        }
    }
});

var ConnectionConfig = cc.Class.extend({
    webServerPath: null,
    ip: null,
    port: null,
    zone: null,
    debug: null,
    udpIp: null,
    udpPort: null,
    smartConnect: null,
    GameRoom: null,
    LobbyRoom: null,
    RummyTournament: null,
    RummyTournamentLobby: null,
    RummyTournamentGameRoom: null,

    ctor: function (prop) {
        var name;
        for (name in prop) {
            this[name] = prop[name];
        }
    }
});