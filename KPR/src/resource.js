var GAME_SIZE = {
    width: null,
    height: null
};
var res, loaderRes;
var scaleFactor = 1;
var g_resources = [];
var loader_resources = [];
(function () {

    var gameLib = {};

    //for landscape mode
    if (screen.width > screen.height) {
        gameLib.screenWidth = screen.width;
        gameLib.screenHeight = screen.height;
    }
    else {
        gameLib.screenHeight = screen.width;
        gameLib.screenWidth = screen.height;
    }

    var availGameSize = {
        /* 1: {
         width: 500,
         height: 350
         },*/
        1: {
            width: 1000,
            height: 650
        },
        4: {
            width: 4000,
            height: 2600
        }
    };

    var tempValue, gameIndex = 1, index;
    tempValue = Math.abs(gameLib.screenWidth - availGameSize[gameIndex].width) + Math.abs(gameLib.screenWidth - availGameSize[gameIndex].height);
    for (var key in availGameSize) {
        index = parseInt(key);
        var xratio = Math.abs(gameLib.screenWidth - availGameSize[index].width);
        var yratio = Math.abs(gameLib.screenHeight - availGameSize[index].height);
        if ((xratio + yratio) <= tempValue) {
            tempValue = xratio + yratio;
            gameIndex = index;
        }
    }
    scaleFactor = gameIndex;
    GAME_SIZE.width = availGameSize[gameIndex].width;
    GAME_SIZE.height = availGameSize[gameIndex].height;

    res = {
        font: [
            {
                type: "font",
                name: "RupeeFordian",
                srcs: ["res/fonts/ttf/Rupee_Foradian.ttf", "res/fonts/eot/Rupee_Foradian.eot"]
            },
            {
                type: "font",
                name: "FranklinDemi",
                srcs: ["res/fonts/ttf/FranklinGothicDemi.ttf", "res/fonts/eot/FranklinGothicDemi.eot"]
            },
            {
                type: "font",
                name: "RobotoBold",
                srcs: ["res/fonts/ttf/Roboto-Medium.ttf", "res/fonts/eot/Roboto-Medium.eot"]
            },
            {
                type: "font",
                name: "RobotoLight",
                srcs: ["res/fonts/ttf/Roboto-Light.ttf", "res/fonts/eot/Roboto-Light.eot"]
            },
            {
                type: "font",
                name: "RobotoRegular",
                srcs: ["res/fonts/ttf/Roboto-Regular.ttf", "res/fonts/eot/Roboto-Regular.eot"]
            },
            {
                type: "font",
                name: "RobotoMedium",
                srcs: ["res/fonts/ttf/Roboto-Medium.ttf", "res/fonts/eot/Roboto-Medium.eot"]
            }

        ],
        atlas: {
            lobbyPlist_png: "res/lobbyPlist.png",
            lobbyPlist_plist: "res/lobbyPlist.plist",
            gameRoomPlist_png: "res/gameRoomPlist.png",
            gameRoomPlist_plist: "res/gameRoomPlist.plist",
            SmileyPlist_png: "res/SmileyPlist.png",
            SmileyPlist_plist: "res/SmileyPlist.plist",
            ButtonPlist_png: "res/ButtonsPlist.png",
            ButtonPlist_plist: "res/ButtonsPlist.plist"
        },
        winExp: {
            winExp_plist: "res/winExp/particle_texture.plist",
            winExp_png: "res/winExp/particle_texture.png"
        },
        ccs: {
            smileyAndChat: "res/SmileyAndChat.json"
        },
        lobby: {
            logo: "res/logo.png",
            playBtn: "res/playBtn.png"
        },
        table: {
            background13card: "res/13cardbg.png",
            editbox: "res/editbox.png",
            sliderBar: "res/SliderBar.png"
        },
        winner: {
            winnerGlow: "res/winnerGlow.png",
            winnerLogo: "res/winnerLogo.png",
            winnerField: "res/winnerField.png"
        },
        gameFaq: {
            pureSeqImg: "res/setTip.png",
            jokerImage: "res/JokerTip.png",
            doublesImage: "res/DoubleTip.png",
            tunnelImage: "res/TunnelTip.png",
            TwentyOneCardsPureSequence: "res/pureSequenceTour.png",
            TwentyOneCardsJokerImage: "res/8Jokers.png",
            TwentyOneCardsDoublesImage: "res/8Doublees.png",
            TwentyOneTunnelImage: "res/3Tunnelas.png"
        },
        sound: {
            carddistributionflip: "res/Sounds/card_disribute.mp3",
            cardFlip: "res/Sounds/card_flip.mp3",
            cutthedeck: "res/Sounds/cutthedeck.mp3",
            dropsound: "res/Sounds/dropsound.mp3",
            extratimesound: "res/Sounds/extratimesound.mp3",
            finalwinningifme: "res/Sounds/finalwinningifme.mp3",
            jointable: "res/Sounds/jointable.mp3",
            meldingscreensound: "res/Sounds/meldingscreensound.mp3",
            myturnsound: "res/Sounds/myturnsound.mp3",
            picknddropsound: "res/Sounds/picknddropsound.mp3",
            validshow: "res/Sounds/validshow.mp3",
            wrongshow: "res/Sounds/wrongshow.mp3"
        },
        reportProblem: {
            minus: "res/minus_icon.png",
            plus: "res/plus_icon.png",
            success: "res/success-icon.png"
        }
    };
    loaderRes = {
        club: "res/loader/club.png",
        diamond: "res/loader/diamond.png",
        heart: "res/loader/heart.png",
        spade: "res/loader/spade.png",
        clubFilled: "res/loader/clubFilled.png",
        diamondFilled: "res/loader/diamondFilled.png",
        heartFilled: "res/loader/heartFilled.png",
        spadeFilled: "res/loader/spadeFilled.png",
        bg: "res/loader/loaderbg.png",
    };
    for (var key in loaderRes) {
        loader_resources.push(loaderRes[key]);
    }

    for (var i in res) {
        for (var j in res[i]) {
            g_resources.push(res[i][j]);
        }
    }

    res.video = {
        card_13: 'res/videos/13card.mp4',
        discard: 'res/videos/discard.mp4',
        group: 'res/videos/group.mp4',
        pick: 'res/videos/pick.mp4',
        sequence: 'res/videos/sequence.mp4',
        show: 'res/videos/show.mp4'
    }
})();