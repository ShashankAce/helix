/**
 * Created by Shashank on 6/7/2016.
 */
var GameInfo = function () {

    this.roomId = null;
    this.mTableId = null;
    this.mTableName = null;
    this.mGameType = null;
    this.mMaxPlayers = null;
    this.mRoomState = null;
    this.mEntryFee = null;
    this.mTotalPrize = null;
    this.mTurnType = null;
    this.mTotalPlayers = null;
    this.mSeatedPLayers = null;
    this.mCashOrPromo = null;
    this.mBetAmount = null;
    this.mPointValue = null;
    this.mCommonId = null;
    this.mGroupTotalPlayers = null;
    this.mNumOfCard = null;
    this.seatedPls = null;

};
// totalprize is extra field here
GameInfo.prototype.setGameInfo = function (tableId, roomId, tableName, gameType, maxPlayers, roomState,
                                           entryFee, totalPrize, turnType, totalPlayers, seatedPLayers,
                                           cashOrPromo, betAmt, pointValue, commonId, groupTotalPLayers, numOfCard) {
    this.mTableId = tableId;
    this.roomId = roomId;
    this.mTableName = tableName;
    this.mGameType = gameType;
    this.mMaxPlayers = maxPlayers;
    this.mRoomState = roomState;
    this.mEntryFee = parseInt(entryFee);
    this.mTotalPrize = totalPrize;
    this.mTurnType = turnType;
    this.mTotalPlayers = totalPlayers;
    this.mSeatedPLayers = seatedPLayers;
    this.mCashOrPromo = cashOrPromo;
    this.mBetAmount = betAmt;
    this.mPointValue = pointValue;
    this.mCommonId = commonId;
    this.mGroupTotalPlayers = groupTotalPLayers;
    this.mNumOfCard = numOfCard;
};

GameInfo.prototype.getmRoomId = function () {
    return this.roomId;
};

GameInfo.prototype.setmRoomId = function (roomId) {
    this.roomId = roomId;
};

GameInfo.prototype.getmTableId = function () {
    return this.mTableId;
};

GameInfo.prototype.setmTableId = function (tableId) {
    this.mTableId = tableId;
};

GameInfo.prototype.getmTableName = function () {
    return this.mTableName;
};

GameInfo.prototype.setmTableName = function (tableName) {
    this.mTableName = tableName;
};

GameInfo.prototype.getmGameType = function () {
    return this.mGameType;
};

GameInfo.prototype.setmGameType = function (gameType) {
    this.mGameType = gameType;
};

GameInfo.prototype.getmMaxPlayers = function () {
    return this.mMaxPlayers;
};

GameInfo.prototype.setmMaxPlayers = function (maxPlayers) {
    this.mMaxPlayers = maxPlayers;
};

GameInfo.prototype.getmRoomState = function () {
    return this.mRoomState;
};

GameInfo.prototype.setmRoomState = function (roomState) {
    this.mRoomState = roomState;
};

GameInfo.prototype.getmEntryFee = function () {
    return this.mEntryFee;
};

GameInfo.prototype.setmEntryFee = function (entryFee) {
    this.mEntryFee = entryFee;
};

GameInfo.prototype.getmtTotalPrize = function () {
    return this.mTotalPrize;
};

GameInfo.prototype.setmtTotalPrize = function (totalPrize) {
    this.mTotalPrize = totalPrize;
};

GameInfo.prototype.getmTurnType = function () {
    return this.mTurnType;
};

GameInfo.prototype.setmTurnType = function (turnType) {
    this.mTurnType = turnType;
};
GameInfo.prototype.getmTotalPlayers = function () {
    return this.mTotalPlayers;
};

GameInfo.prototype.setmTotalPlayers = function (totalPlayers) {
    this.mTotalPlayers = totalPlayers;
};

GameInfo.prototype.getSeatedPls = function () {
    return this.seatedPls;
};

GameInfo.prototype.setSeatedPls = function (seatedPls) {
    this.seatedPls = seatedPls;
};

GameInfo.prototype.getmSeatedPLayers = function () {
    return this.mSeatedPLayers;
};

GameInfo.prototype.setmSeatedPLayers = function (seatedPlayers) {
    this.mSeatedPLayers = seatedPlayers;
};

GameInfo.prototype.getmCashOrPromo = function () {
    return this.mCashOrPromo;
};

GameInfo.prototype.setmCashOrPromo = function (cashOrPromo) {
    this.mCashOrPromo = cashOrPromo;
};

GameInfo.prototype.getmBetAmount = function () {
    return this.mBetAmount;
};

GameInfo.prototype.setmBetAmount = function (betAmount) {
    this.mBetAmount = betAmount;
};

GameInfo.prototype.getmPointValue = function () {
    return this.mPointValue;
};

GameInfo.prototype.setmPointValue = function (pointValue) {
    this.mPointValue = pointValue;
};

GameInfo.prototype.getmCommonId = function () {
    return this.mCommonId;
};

GameInfo.prototype.setmCommonId = function (commonId) {
    this.mCommonId = commonId;
};

GameInfo.prototype.getmGroupTotalPlayers = function () {
    return this.mGroupTotalPlayers;
};

GameInfo.prototype.setmGroupTotalPlayers = function (groupTotalPlayers) {
    this.mGroupTotalPlayers = groupTotalPlayers;
};

GameInfo.prototype.getmNumOfCard = function () {
    return this.mNumOfCard;
};

GameInfo.prototype.setmNumOfCard = function (numOfCard) {
    this.mNumOfCard = numOfCard;
};
