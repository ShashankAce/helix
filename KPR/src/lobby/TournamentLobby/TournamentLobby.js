/**
 * Created by stpl on 9/19/2016.
 */
"use strict";
var TournamentLobby = cc.LayerColor.extend({
    _name: "TournamentLobby",
    _isEnabled: null,
    _lobby: null,
    // tournamentEventDispatcher: null,
    tourRoomListManager: null,
    tourGrpTypeArr: null,
    tourGameNameList: null,
    tourGameNameObj: null,
    tournamentFilteredList: null,
    myTourFilteredList: null,
    selectedcheckboxGrpTypeArr: null,
    checkboxArray: null,
    selectedTourGameType: null,
    tournamentDetail: null,

    ctor: function (lobby) {
        this._super(cc.color(0, 0, 0));
        this._lobby = lobby;
        this._isEnabled = true;
        this.tourGrpTypeArr = [];
        this.tourGameNameList = [];
        this.tourGameNameObj = [];
        this.tournamentFilteredList = [];
        this.myTourFilteredList = [];
        this.selectedcheckboxGrpTypeArr = [];
        this.checkboxArray = [];
        var filterParserLayerSize = cc.size(760 * scaleFactor, 435 * scaleFactor);
        this.setContentSize(filterParserLayerSize);

        this.filterStripSize = cc.size(760 * scaleFactor, 70 * scaleFactor);
        this.subGameTypeTabsHeight = 35 * scaleFactor;
        this.subGameTypeTabsWidth = 90 * scaleFactor;
        this.subGameTypeTabsMargin = 9 * scaleFactor;
        this.subGameTypeFontSize = 10 * scaleFactor;
        this.checkBoxSize = cc.size(760 * scaleFactor, 70 * scaleFactor);
        this.listHeaderSize = cc.size(760 * scaleFactor, 30 * scaleFactor);
        this.checkBoxHeight = 115 * scaleFactor;

        this.myTourHeaderSize = cc.size(760 * scaleFactor, 25 * scaleFactor);

        // this.tournamentEventDispatcher = new TournamentEventDispatcher(this);

        this.tabMargin = 10 * scaleFactor;
        this.tabSpace = 2 * scaleFactor;
        this.listViewHeaderFontSize = 11 * scaleFactor;
        this.fontSize = 12 * scaleFactor;

        this.rangeSlider = new ccui.RangedSlider();
        this.rangeSlider.loadBarTexture("SliderWhite.png", ccui.Widget.PLIST_TEXTURE);
        this.rangeSlider.loadSlidBallTextures("Slider_knob.png", "Slider_knob.png", "Slider_knob.png", ccui.Widget.PLIST_TEXTURE);
        this.rangeSlider.loadProgressBarTexture(res.table.sliderBar);
        this.rangeSlider.addEventListener(this.setEntryFeeValue.bind(this));

        this.startTimeText = new cc.LabelTTF("Start Time", "RobotoRegular", 12 * 2 * scaleFactor);
        this.startTimeText.setColor(cc.color(0, 0, 0));
        this.startTimeText.setScale(0.5);
        this.startTimeText.setPosition(cc.p(660 * scaleFactor, 345 * scaleFactor - yMargin));

        this.minTimeText = new cc.LabelTTF("0", "RobotoRegular", 12 * 2 * scaleFactor);
        this.minTimeText.setColor(cc.color(0, 0, 0));
        this.minTimeText.setScale(0.5);
        this.minTimeText.setPosition(cc.p(586 * scaleFactor, 385 * scaleFactor - yMargin));

        this.maxTimeText = new cc.LabelTTF("24", "RobotoRegular", 12 * 2 * scaleFactor);
        this.maxTimeText.setColor(cc.color(0, 0, 0));
        this.maxTimeText.setScale(0.5);
        this.maxTimeText.setPosition(cc.p(734 * scaleFactor, 385 * scaleFactor - yMargin));

        this.tourMiniRoom = new TournamentMiniRoom(this);
        this.tourMiniRoom.setPosition(cc.p(this.width + 1, 0));
        this.addChild(this.tourMiniRoom);

        this.tourRoomListManager = new TournamentRoomListManager(this);
        this.addChild(this.tourRoomListManager.listView, 0);

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    cc.log("Blank Listener in " + this._name);
                    return true;
                }
                return false;
            }.bind(this)
        }, this);

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();
        this.initCustomEventListener();
    },
    pauseListener: function (bool) {
        if (bool) {
            cc.eventManager.pauseTarget(this, true);
        } else {
            cc.eventManager.resumeTarget(this, true);
        }
        this._isEnabled = !bool;
        this.tourRoomListManager.pauseListener(bool);
    },
    createTournamentTab: function () {
        this._lobby.filterParser.roomListManager.listView.removeAllChildrenWithCleanup(true);
        this._lobby.filterParser._isEnabled = false;
        this.addSubGameTypeTab();

        var slctdGameType = this.tourGameTypeTargetArray[this.tourGameTypeTargetArray.length - 1];
        this.setSelectedGameType(slctdGameType);
        this.subGameTypeListener(slctdGameType);
    },
    addSubGameTypeTab: function () {
        var width = 10 * scaleFactor;

        if (this.tourGameNameList.length == 0)
            return;
        this.tourGameTypeTargetArray = [];
        for (var i = 0; i < this.tourGameNameList.length; i++) {
            var subGameTypeTab = this.getSubGameTypeTab(this.tourGameNameList[i]);
            subGameTypeTab.setAnchorPoint(0, 0);
            this.subGameTypeTabsHeight = subGameTypeTab.height + 2;
            subGameTypeTab.setPosition(cc.p(width, this.height - this.subGameTypeTabsHeight));
            width += subGameTypeTab.width + 10 * scaleFactor;
            this.addChild(subGameTypeTab);

            this.tourGameTypeTargetArray.push(subGameTypeTab);
            cc.eventManager.addListener(this.tournamentLobbyMouseListener.clone(), subGameTypeTab);
            cc.eventManager.addListener(this.tournamentLobbyTouchListener.clone(), subGameTypeTab);
        }

        for (var i = 0; i < this.tourRoomListManager.newTagGrpTypeArr.length; i++) {
            for (var j = 0; j < this.tourGameTypeTargetArray.length; j++) {
                var grpName = this.tourGameTypeTargetArray[j]._name.split("_");
                if (this.tourRoomListManager.newTagGrpTypeArr[i] == grpName[1]) {
                    this.tourGameTypeTargetArray[j].addNewTagSprite();
                }
            }
        }

        this.tourGameTypeTargetArray[this.tourGameTypeTargetArray.length - 1].hover(true);
    },
    initMouseListener: function () {
        this.tournamentLobbyMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            swallowTouches: true,
            onMouseMove: function (event) {
                if (this._isEnabled) {
                    var target = event.getCurrentTarget();
                    var locationInNode = target.convertToNodeSpace(event.getLocation());
                    var s = target.getContentSize();
                    var rect = cc.rect(0, 0, s.width, s.height);
                    if (cc.rectContainsPoint(rect, locationInNode)) {
                        !target._hovering && target.hover(true);
                        target._hovering = true;
                        return true;
                    } else {
                        if (this.selectedTourGameType != target && this._lobby.selectedGameType != target) {
                            target._hovering && target.hover(false);
                            target._hovering = false;
                        }
                        return false;
                    }
                }
            }.bind(this)
        });
        this.tournamentLobbyMouseListener.retain();
    },
    initTouchListener: function () {
        this.tournamentLobbyTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode) && this._isEnabled) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                var targetName = target._name.split("_")[0];
                applicationFacade._controlAndDisplay.closeAllTab();
                applicationFacade._lobby.closeAllTab();
                switch (targetName) {
                    case "MyTournamentHeader" :
                        this.myTournamentHeaderHandler();
                        break;
                    case "TournamentGameType" :
                        if (target != this._lobby.selectedGameType && target != this.selectedTourGameType) {
                            this.tourGameTypeHandler(target);
                        }
                        break;
                }
            }.bind(this)
        });
        this.tournamentLobbyTouchListener.retain();
    },
    tourGameTypeHandler: function (target) {
        this.subGameTypeListener(target);
    },
    subGameTypeListener: function (target) {
        this.rangeSlider.setBarPercentage({min: 0, max: 100});
        this.selectedTourGameType.hover(false);
        if (this.selectedTourGameType && this.selectedTourGameType != target) {
            this.selectedTourGameType.hover(false);
            target.hover(true);
        }
        this._lobby.gameInfoLabel.setString("Tournament Info");
        this.setSelectedGameType(target);
        this.selectedTourGameType.hover(true);
    },
    setSelectedGameType: function (target, isAddRoom) {
        var tourGameName;
        if (target._name == "TOURNAMENTS") {
            tourGameName = this.tourGameNameList[this.tourGameNameList.length - 1];
        } else {
            tourGameName = target._name.split("_")[1];
            if (this.selectedTourGameType != target) {
                this.selectedTourGameType = target;
                this.selectedcheckboxGrpTypeArr = [];
            }
        }
        this.ListFilteredByGameName(tourGameName);
        this.gettournamentGroupType(this.tournamentFilteredList);

        if (!isAddRoom) {
            this.initDynamicFilter(this.tourGrpTypeArr);
            this.showListViewHeader();
        }


        var minDivideValue = this.minTimeText.getString();
        var maxDivideValue = this.maxTimeText.getString();

        var allTourArr = this.tourRoomListManager.getFinalFilteredListByCheckbox(this.tournamentFilteredList);
        allTourArr = this.getSortedByEntryFee(allTourArr, minDivideValue, maxDivideValue);

        this.defaultSort(this.myTourFilteredList);
        this.defaultSort(allTourArr);

        this.tourRoomListManager.insertRoomList(this.widthArray, this.myTourFilteredList, allTourArr);
        // this.tourMiniRoom.setMiniRoom(this.tourRoomListManager.listView.getItem(1));

    },
    initDynamicFilter: function (tourGrpTypeArray) {
        var filterStripSize = cc.size(759.5 * scaleFactor, 70 * scaleFactor);
        this.filterDrawNode && this.filterDrawNode.removeFromParent(true);
        this.filterDrawNode = new cc.DrawNode();
        this.filterDrawNode.drawRect(cc.p(0, 330 * scaleFactor), cc.p(filterStripSize.width, (400 - 3) * scaleFactor), cc.color(255, 255, 255), 0, cc.color(255, 255, 255));
        this.addChild(this.filterDrawNode, 1);

        tourGrpTypeArray.sort();

        var initialMargin = 28 * scaleFactor,
            margin = 13 * scaleFactor,
            width = initialMargin,
            yPos = 365 * scaleFactor;

        this.checkboxArray = [];
        if (tourGrpTypeArray.length != 0) {
            for (var i = 0; i < tourGrpTypeArray.length; i++) {
                var checkboxWithLabel = new CheckBoxModule(tourGrpTypeArray[i], margin, this);
                checkboxWithLabel.setPosition(cc.p(width, yPos));
                this.checkboxArray.push(checkboxWithLabel);
                this.filterDrawNode.addChild(checkboxWithLabel, 10);
                width += checkboxWithLabel.width + 20 * scaleFactor;


            }
        }


        this.rangeSlider && this.rangeSlider.removeFromParent(true);
        this.startTimeText && this.startTimeText.removeFromParent(true);
        this.minTimeText && this.minTimeText.removeFromParent(true);
        this.maxTimeText && this.maxTimeText.removeFromParent(true);

        this.rangeSlider.setPosition(cc.p(665 * scaleFactor, yPos));
        this.filterDrawNode.addChild(this.rangeSlider, 10);
        this.filterDrawNode.addChild(this.startTimeText, 10);
        this.filterDrawNode.addChild(this.minTimeText, 10);
        this.filterDrawNode.addChild(this.maxTimeText, 10);
    },
    setEntryFeeValue: function (sender, type) {
        switch (type) {
            case ccui.RangedSlider.EVENT_PERCENT_CHANGED_END:

                var percent = this.rangeSlider.getPercent();
                var valueAtPercentage = Math.floor(100 / 24);
                var minDivideValue = parseInt(percent.min / valueAtPercentage);
                if (percent.max == 100)
                    var maxDivideValue = 24;
                else var maxDivideValue = parseInt(percent.max / valueAtPercentage);

                this.minTimeText.setString(minDivideValue);
                this.maxTimeText.setString(maxDivideValue);

                // var myTourArr = this.getSortedByEntryFee(this.myTourFilteredList, minDivideValue, maxDivideValue);
                var allTourArr = this.tourRoomListManager.getFinalFilteredListByCheckbox(this.tournamentFilteredList);
                allTourArr = this.getSortedByEntryFee(allTourArr, minDivideValue, maxDivideValue);
                this.tourRoomListManager.insertRoomList(this.widthArray, this.myTourFilteredList, allTourArr);
                break;

            case ccui.RangedSlider.EVENT_PERCENT_CHANGED:

                var percent = this.rangeSlider.getPercent();
                var valueAtPercentage = Math.floor(100 / 24);
                var minDivideValue = parseInt(percent.min / valueAtPercentage);
                if (percent.max == 100)
                    var maxDivideValue = 24;
                else var maxDivideValue = parseInt(percent.max / valueAtPercentage);

                this.minTimeText.setString(minDivideValue);
                this.maxTimeText.setString(maxDivideValue);

                break;
        }
    },
    getSortedByEntryFee: function (array, minStartTime, maxStartTime) {
        var filterArray = [], time;
        for (var i = 0; i < array.length; i++) {
            var startTime = moment(array[i].getVariable("startTime").value).format('HH,mm');
            time = parseInt(startTime.split(",")[0]);
            if (parseInt(time) >= minStartTime && parseInt(time) <= maxStartTime) {
                filterArray.push(array[i]);
            }
        }
        return filterArray;
    },
    resetLobbyTab: function () {
        // this._lobby.filterParser.removeFromParent(true);
        /*cc.eventManager.removeListener(this._lobby.filterParser.poolRummyTab);
         cc.eventManager.removeListener(this._lobby.filterParser.pointRummyTab);
         cc.eventManager.removeListener(this._lobby.filterParser.dealsRummyTab);
         cc.eventManager.removeListener(this._lobby.filterParser.pointRummy21cardsTab);
         this._lobby.filterParser.poolRummyTab.removeFromParent(true);
         this._lobby.filterParser.pointRummyTab.removeFromParent(true);
         this._lobby.filterParser.dealsRummyTab.removeFromParent(true);
         this._lobby.filterParser.pointRummy21cardsTab.removeFromParent(true);

         for (var i = 0; i < this._lobby.filterParser.checkboxArray.length; i++) {
         this._lobby.filterParser.checkboxArray[i].removeFromParent(true);
         }
         this._lobby.filterParser.checkboxArray = [];

         this._lobby.filterParser.findGameDrawNode && this._lobby.filterParser.findGameDrawNode.removeFromParent(true);*/

    },
    getTourListHeaderLayout: function (string, height, tabMargin, fontSize, fontFamily) {
        var label = new cc.LabelTTF(string, fontFamily, fontSize * 2);
        label.setName(string.replace(/ /g, "") + "Label");
        label.setScale(0.65);
        label.setAnchorPoint(0, 0.5);
        return label;
    },
    showListViewHeader: function () {
        this.tourlistHeaderNode && this.tourlistHeaderNode.removeFromParent(true);

        this.widthArray = [];
        this.tourlistHeaderNode = new ccui.Layout();
        this.tourlistHeaderNode.setContentSize(cc.size(this.listHeaderSize.width, this.listHeaderSize.height));
        this.tourlistHeaderNode.setBackGroundColorType(ccui.Layout.BG_COLOR_GRADIENT);
        this.tourlistHeaderNode.setBackGroundColor(cc.color(111, 111, 111), cc.color(88, 88, 88));
        // this.tourlistHeaderNode = new cc.DrawNode();
        // this.tourlistHeaderNode.drawRect(cc.p(0, this.listHeaderSize.height), cc.p(this.listHeaderSize.width, 0), cc.color(111, 111, 111), 0, cc.color(98, 98, 98));
        this.tourlistHeaderNode.setPosition(cc.p(0, 300 * scaleFactor));
        this.addChild(this.tourlistHeaderNode, 2);

        var widthPerTab = this.tourlistHeaderNode.width / 8;
        var width = 20 * scaleFactor;
        var listHeaderTextArr = ["Name", "Buy-In", "Prize", "Joined", "Start Time", "Status"];

        var nameLayout = this.getTourListHeaderLayout(listHeaderTextArr[0], this.listHeaderSize.height, this.tabMargin, this.listViewHeaderFontSize, "RobotoBold");
        nameLayout.setPosition(cc.p(20 * scaleFactor, this.tourlistHeaderNode.height / 2 - yMargin));
        width += widthPerTab + this.tabSpace;
        this.widthArray.push(nameLayout.getPositionX() /*+ nameLayout.width / 2*/);

        var entryFeeLayout = this.getTourListHeaderLayout(listHeaderTextArr[1], this.listHeaderSize.height, this.tabMargin, this.listViewHeaderFontSize, "RobotoBold");
        entryFeeLayout.setPosition(cc.p(160 * scaleFactor, this.tourlistHeaderNode.height / 2 - yMargin));
        width += widthPerTab + this.tabSpace;
        this.widthArray.push(entryFeeLayout.getPositionX() /*+ entryFeeLayout.width / 2*/);

        var prizeLayout = this.getTourListHeaderLayout(listHeaderTextArr[2], this.listHeaderSize.height, this.tabMargin, this.listViewHeaderFontSize, "RobotoBold");
        prizeLayout.setPosition(cc.p(230 * scaleFactor, this.tourlistHeaderNode.height / 2 - yMargin));
        width += widthPerTab + this.tabSpace;
        this.widthArray.push(prizeLayout.getPositionX() /*+ prizeLayout.width / 2*/);

        var joinedLayout = this.getTourListHeaderLayout(listHeaderTextArr[3], this.listHeaderSize.height, this.tabMargin, this.listViewHeaderFontSize, "RobotoBold");
        joinedLayout.setPosition(cc.p(400 * scaleFactor, this.tourlistHeaderNode.height / 2 - yMargin));
        width += widthPerTab + this.tabSpace;
        this.widthArray.push(joinedLayout.getPositionX() /*+ joinedLayout.width / 2*/);

        var startTimeLayout = this.getTourListHeaderLayout(listHeaderTextArr[4], this.listHeaderSize.height, this.tabMargin, this.listViewHeaderFontSize, "RobotoBold");
        startTimeLayout.setPosition(cc.p(475 * scaleFactor, this.tourlistHeaderNode.height / 2 - yMargin));
        width += widthPerTab + this.tabSpace;
        this.widthArray.push(startTimeLayout.getPositionX() /*+ startTimeLayout.width / 2*/);

        var statusLayout = this.getTourListHeaderLayout(listHeaderTextArr[5], this.listHeaderSize.height, this.tabMargin, this.listViewHeaderFontSize, "RobotoBold");
        statusLayout.setPosition(cc.p(602 * scaleFactor, this.tourlistHeaderNode.height / 2 - yMargin));
        width += widthPerTab + this.tabSpace;
        this.widthArray.push(statusLayout.getPositionX() /*+ statusLayout.width / 2*/);

        this.tourlistHeaderNode.addChild(nameLayout, 2);
        this.tourlistHeaderNode.addChild(entryFeeLayout, 2);
        this.tourlistHeaderNode.addChild(prizeLayout, 2);
        this.tourlistHeaderNode.addChild(joinedLayout, 2);
        this.tourlistHeaderNode.addChild(startTimeLayout, 2);
        this.tourlistHeaderNode.addChild(statusLayout, 2);

    },
    getSubGameTypeTab: function (subGameTypeName) {
        var tab = cc.Scale9Sprite.extend({
            _defaultImage: null,
            _hovering: null,
            ctor: function (subGameTypeName, subGameTypeTabsMargin, subGameTypeTabsHeight, subGameTypeFontSize) {
                this._super(spriteFrameCache.getSpriteFrame("Tournament.png"), cc.rect(0, 0, 159, 36));
                // this.setCapInsets(cc.rect(52, 11, 52, 11));
                this.setAnchorPoint(0, 0);
                this.setName("TournamentGameType_" + subGameTypeName);
                this._hovering = false;
                this.subGameTypeLabel = new cc.LabelTTF(subGameTypeName, "RobotoBold", 21);
                this.subGameTypeLabel.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
                this.subGameTypeLabel.setScale(0.65);
                this.addChild(this.subGameTypeLabel, 1);

                this.setContentSize(cc.size(this.subGameTypeLabel.width + 50 * scaleFactor, this.height));
                this.subGameTypeLabel.setPosition(cc.p(this.width / 2, this.height / 2 - yMargin));

                return true;
            },
            hover: function (bool) {
                if (bool) {
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("TournamentHover.png"));
                } else {
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("Tournament.png"));
                }
                this.setContentSize(cc.size(this.subGameTypeLabel.width + 50 * scaleFactor, this.height));
                this.setAnchorPoint(0, 0);
            },
            addNewTagSprite: function () {
                var newSprite = new cc.Sprite(spriteFrameCache.getSpriteFrame("newR.png"));
                newSprite.setColor(cc.color(255, 238, 215));
                newSprite.setAnchorPoint(0, 1);
                newSprite.setPosition(cc.p(0, this.height));
                this.addChild(newSprite, 2);
            }
        });
        return new tab(subGameTypeName, this.subGameTypeTabsMargin, this.subGameTypeTabsHeight, this.subGameTypeFontSize);
    },
    /*onRoomAdd : function (room) {
     var trmntGroupName = (room.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_NAME).value).split("-");
     var trmntGroupType = room.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value;
     if(this.tourGameNameList.indexOf(trmntGroupName[0]) < 0){
     this.tourGameNameList.push(trmntGroupName[0]);
     var myObj = {
     "gameName": trmntGroupName[0],
     "seqNo": trmntGroupName[1],
     };
     this.tourGameNameList.push(trmntGroupName[0]);
     this.tourGameNameObj.push(trmntGroupName[1]);

     this.addNewSubGameTypeTab();
     }else{
     if(this.tournamentFilteredList){
     if (this.selectedTourGameType && ((this.selectedTourGameType._name.indexOf("ALL") != -1) || (room.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_NAME.value)).split("-")[0] == this.selectedTourGameType._name.split("-")[0]))
     if(this.tournamentFilteredList){
     this.tournamentFilteredList.push(room);

     }
     }
     }
     },*/
    handleTournamentList: function (tournamentList, isAddRoom) {
        for (var i = 0; i < sfsClient._tournamentRoomArr.length; i++) {
            var tournamentGroupName = sfsClient._tournamentRoomArr[i].variables.tournamentGroupName.value;
            var status = sfsClient._tournamentRoomArr[i].variables.status.value;

            var startTime = sfsClient._tournamentRoomArr[i].variables.startTime.value;
            var groupName = tournamentGroupName.split("-");

            if (!(this.tourGameNameList.indexOf(groupName[0]) >= 0)) {
                var myObj = {
                    "gameName": groupName[0],
                    "seqNo": groupName[1],
                };
                this.tourGameNameList.push(groupName[0]);
                this.tourGameNameObj.push(myObj);
            }

            sfsClient._tournamentRoomArr[i].priority = this.setPriorityValue(status);
            sfsClient._tournamentRoomArr[i].startDateAndTime = moment(startTime).format('DD MMM,hh:mm a');
        }
        sfsClient._tournamentRoomArr = this.removeClosedTourmnt(sfsClient._tournamentRoomArr);
        this.getTournamentGameNameList(sfsClient._tournamentRoomArr);

        if (this.selectedTourGameType)
            this.setSelectedGameType(this.selectedTourGameType, isAddRoom);
        else this.ListFilteredByGameName(this.tourGameNameList[this.tourGameNameList.length - 1]);
    },
    removeClosedTourmnt: function (tournamentList) {
        for (var i = 0; i < tournamentList.length; i++) {
            if (tournamentList[i].priority == 0) {
                tournamentList.splice(i, 1);
                i--;
            }
        }
        return tournamentList;
    },
    gettournamentGroupType: function (filteredList) {
        this.tourGrpTypeArr = [];
        for (var i = 0; i < filteredList.length; i++) {
            var groupType = filteredList[i].getVariable("tournamentGroupType").value;

            if (!(this.tourGrpTypeArr.indexOf(groupType) >= 0)) {
                this.tourGrpTypeArr.push(groupType);
            }
        }
    },
    getTournamentGameNameList: function (tournamentList) {
        if (this.tourGameNameObj.length != 0) {
            this.tourGameNameObj.sort(function (a, b) {
                return a.seqNo - b.seqNo;
            });
            this.tourGameNameList = [];
            for (var key = 0; key < this.tourGameNameObj.length; key++) {
                this.tourGameNameList.push(this.tourGameNameObj[key].gameName);
            }
            this.tourGameNameList.push("ALL");
            // this.tourGameNameList.push("MY TOURNAMENT");
        }
    },
    setPriorityValue: function (status) {
        var priority;
        switch (status.toUpperCase()) {
            case TOURNAMENT_CONSTANTS.REGISTERING :
                priority = 1;
                break;
            case TOURNAMENT_CONSTANTS.OPEN :
                priority = 2;
                break;
            case TOURNAMENT_CONSTANTS.FREEZE :
                priority = 3;
                break;
            case TOURNAMENT_CONSTANTS.RUNNING :
                priority = 4;
                break;
            case TOURNAMENT_CONSTANTS.COMPLETED :
                priority = 5;
                break;
            case TOURNAMENT_CONSTANTS.CANCELLED :
                priority = 6;
                break;
            default :
                priority = 0;
        }
        return priority;
    },
    defaultSort: function (listArray) {
        if (!listArray) {
            listArray = this.tournamentFilteredList;
        }

        /*listArray.sort(function (a, b) {
         if (a.priority - b.priority == 0)  // 2nd
         return a.getVariable("startTime").value - b.getVariable("startTime").value;  // 2st
         return a.priority - b.priority;
         });*/

        listArray.sort(function (a, b) {
            return a.priority - b.priority || a.getVariable("startTime").value - b.getVariable("startTime").value;
        });

    },
    ListFilteredByGameName: function (gameName) {
        this.tournamentFilteredList = [];
        this.myTourFilteredList = [];
        if (gameName == null) {
            return;
        }
        sfsClient._tournamentRoomArr = this.removeClosedTourmnt(sfsClient._tournamentRoomArr);
        if (this.tourRoomListManager && this.tourRoomListManager._myTournamentsIds.length != 0) {
            for (var i = 0; i < this.tourRoomListManager._myTournamentsIds.length; i++) {
                for (var j = 0; j < sfsClient._tournamentRoomArr.length; j++) {
                    var index = (sfsClient._tournamentRoomArr[j].id == this.tourRoomListManager._myTournamentsIds[i]) ? j : -1;
                    if (index >= 0) {
                        this.myTourFilteredList.push(sfsClient._tournamentRoomArr[j]);
                        // this.tournamentFilteredList.splice(index, 1);
                        // j--;
                    }
                }
            }
        }
        for (var key = 0; key < sfsClient._tournamentRoomArr.length; key++) {
            if (gameName != "ALL") {
                var groupName = sfsClient._tournamentRoomArr[key].variables.tournamentGroupName.value.split("-");
                if (gameName != null) {
                    if (groupName[0] == gameName)
                        this.tournamentFilteredList.push(sfsClient._tournamentRoomArr[key]);
                }
            } else {
                this.tournamentFilteredList.push(sfsClient._tournamentRoomArr[key]);
            }
        }


        if (this.myTourFilteredList.length != 0) {
            for (var i = 0; i < this.myTourFilteredList.length; i++) {
                for (var j = 0; j < this.tournamentFilteredList.length; j++) {
                    var index = (this.tournamentFilteredList[j].id == this.myTourFilteredList[i].id) ? j : -1;
                    if (index >= 0) {
                        this.tournamentFilteredList.splice(index, 1);
                        j--;
                    }
                }
            }
        }


        /* for (var i = 0; i < this.tournamentFilteredList.length; i++) {
         if (this.tourRoomListManager && this.tourRoomListManager._myTournamentsIds.length != 0) {
         for (var j = 0; j < this.tourRoomListManager._myTournamentsIds.length; j++) {
         var index = (this.tournamentFilteredList[i].id == this.tourRoomListManager._myTournamentsIds[j]) ? i : -1;
         if (index >= 0) {
         this.myTourFilteredList.push(this.tournamentFilteredList[i]);
         this.tournamentFilteredList.splice(index, 1);
         i--;
         }
         }
         } else
         break;
         }*/
        /*this.myTourFilteredList = */
        // this.defaultSort(this.myTourFilteredList);
        /* this.tournamentFilteredList = */
        this.defaultSort(this.myTourFilteredList);
        this.defaultSort(this.tournamentFilteredList);
        /*   timeArr = [];
         for (var i = 0; i < this.tournamentFilteredList.length; i++) {
         timeArr.push(this.tournamentFilteredList[i].startDateAndTime + " " + this.tournamentFilteredList[i].priority);
         }
         this.tournamentFilteredList.sort(function (a, b) {
         if ((a.priority - b.priority) == 0)  // 2nd
         return a.getVariable("startTime").value - b.getVariable("startTime").value;  // 2st
         return a.priority - b.priority;
         });
         timeArr = [];
         for (var i = 0; i < this.tournamentFilteredList.length; i++) {
         timeArr.push(this.tournamentFilteredList[i].startDateAndTime + " " + this.tournamentFilteredList[i].priority);
         }*/
        for (var i = 0; i < this.tournamentFilteredList.length; i++) {
            var string = "";
            var startTime = this.tournamentFilteredList[i].getVariable("startTime").value;

            var startDateAndTime = moment(startTime).format('DD MMM,hh:mm a'); // "16 Oct 2011, 9:47 PM"
            var currentTimeInSec = (new Date).getTime();
            var currentDateAndTime = moment(currentTimeInSec).format('DD MMM,hh:mm a');

            var startTimeArr = startDateAndTime.split(",");
            var currentTimeArr = currentDateAndTime.split(",");

            if (startTimeArr[0] == currentTimeArr[0]) {
                var day = "Today";
                string += startTimeArr[1] + " " + day;
            } else {
                string = startTimeArr[0] + " " + startTimeArr[1];
            }
            this.tournamentFilteredList[i].StartTimeInUIFormat = string;
        }
    },

    showMiniRoom: function (sender, param1, param2) {
        if (this._lobby.selectedGameType._name == "TOURNAMENTS") {
            this._lobby.miniRoom && this._lobby.miniRoom.setVisible(false);
            // this.tourMiniRoom.setMiniRoom(sender);
            this.tourMiniRoom.setVisible(true);
        }
    },
    selectedCheckBoxStateEvent: function (sender, type) {
        var checkboxType = sender._name;
        var status;
        switch (type) {
            case  ccui.CheckBox.EVENT_UNSELECTED:
                status = "unselected";
                this.getfilteredListByCheckbox(checkboxType, status);
                break;
            case ccui.CheckBox.EVENT_SELECTED:
                status = "selected";
                this.getfilteredListByCheckbox(checkboxType, status);
                break;
            default:
                break;
        }
    },
    getfilteredListByCheckbox: function (checkboxType, status) {
        if (status == "selected") {
            this.selectedcheckboxGrpTypeArr.push(checkboxType);
        } else {
            var index = this.selectedcheckboxGrpTypeArr.indexOf(checkboxType);
            this.selectedcheckboxGrpTypeArr.splice(index, 1);
        }
        this.tourRoomListManager.filterSubGameTypeByCheckBoxSelected(checkboxType);
    },
    handleResp: function (cmd, room, params) {
        cc.log("tournament command2", cmd);
        // this.tournamentEventDispatcher.dispatchSFSEvents(cmd, room, params);
        switch (cmd) {
            case SFSConstants.CMD_TOURNAMENT_NEW:  //tournament
                var roomIds = params[TOURNAMENT_CONSTANTS.ROOMIDS];
                this.processNewTournament(roomIds);
                break;
            case SFSConstants.CMD_TOURNAMENT_ROOMSTATUS:  //tournament
                this.processTournamentRoomStatus(params);
                break;
            case SFSConstants.CMD_MYTOURNAMENT:  //tournament //
                this.processMyTournament(params);
                break;
            case SFSConstants.CMD_DISABLEREG_RESP:  //tournament
                cc.log("disable resp");
                break;
            case SFSConstants.CMD_TOURNAMENT_HELPTEXT:
                cc.log("tournament command3" + cmd);//tournament
                this.processHelpText(params);
                break;
            case SFSConstants.CMD_TOURNAMENT_PRIZESTRUCTURE:  //tournament
                this.processTournamentPrizeStr(params);
                break;
            case SFSConstants.CMD_PLAYER_ELIGIBLE:  //tournament
                this.processPlayerEligible(params);
                break;
            case SFSConstants.CMD_JOINED_PLAYER_INFO:  //tournament
                this.processJoinedPlayerInfo(room, params);
                break;
            case TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_REGNUM_RESP:  //tournament
                this.processRegNumResp(params, room);
                break;
            case TOURNAMENT_CONSTANTS.LEVEL_PRGRSS_INFO_RESP:  //tournament
                this.processLevelProgress(params);
                break;
            case TOURNAMENT_CONSTANTS.ROOM_PRGRSS_INFO_RESP:  //tournament
                this.processTableProgress(params);
                break;
            case TOURNAMENT_CONSTANTS.CMD_CONFIRM_RESP:  //tournament
                this.processJoinConfrmResp(room, params);
                break;
            case TOURNAMENT_CONSTANTS.CMD_QUIT_RESP: //tournament
                this.processQuitConfrmResp(room, params);
                break;
            case TOURNAMENT_CONSTANTS.CMD_WAITNUM_RESP: //tournament
                this.processWaitNumResp(params);
                break;
            case TOURNAMENT_CONSTANTS.DETAIL_INFO: //tournament
                this.handleTmntDetail(room, params);
                break;
            case TOURNAMENT_CONSTANTS.TOURNAMENT_CANCEL: //tournament
                this.handleTournamentCancelCmd(room, params);
                break;
            case TOURNAMENT_CONSTANTS.PRIZE_INFO:  //tournament
                this.processTournamentPrizeInfo(room, params);
                break;
            case TOURNAMENT_CONSTANTS.WINNER_INFO:  //tournament
                this.processTournamentWinnerInfo(room, params);
                break;
            case TOURNAMENT_CONSTANTS.TOURNAMENT_DATA:
                this.processTournamentData(params);
                break;
            case TOURNAMENT_CONSTANTS.ALREADY_TRNMNT_JOIN_STR:
                this.processAlreadyJoinedTournament(params);
                break;
            case TOURNAMENT_CONSTANTS.NO_ENTRY:
                this.processNoEntry(room, params);
                break;
            case TOURNAMENT_CONSTANTS.CMD_INSTRUCTIONS_RESP:
                this.processInstructions(room, params);
                break;
            case TOURNAMENT_CONSTANTS.UPDATE_PRIZE_STRUCT:
                this.updatePrizeStructure(room, params);
                break;
            case TOURNAMENT_CONSTANTS.STRUCTURE:
                this.setDetailsStructureVars(room, params);
                break;
            case TOURNAMENT_CONSTANTS.PROGRESS_INFO:
                this.setDetailsProgressVars(room, params);
                break;

        }
    },
    processHelpText: function (data) {
        this.tourRoomListManager.processHelpText(data);
    },
    processTournamentPrizeStr: function (data) {
        this.tourRoomListManager.processTournamentPrizeStr(data["str"]);
    },
    processTournamentPrizeInfo: function (room, data) {
        this.tourRoomListManager.processTournamentPrizeInfo(room, data);
    },
    processTournamentWinnerInfo: function (room, data) {
        this.tourRoomListManager.processTournamentWinnerInfo(room, data);
    },
    processTournamentData: function (dataObj) {
        this.tourRoomListManager.processTournamentData(dataObj);
    },
    processAlreadyJoinedTournament: function (dataObj) {
        this.tourRoomListManager.processAlreadyJoinedTournament(dataObj);
    },
    processNoEntry: function (room, dataObj) {
        this.tourRoomListManager.processNoEntry(room, dataObj);
    },
    processInstructions: function (room, dataObj) {
        this.tourRoomListManager.processInstructions(room, dataObj);
    },
    processPlayerEligible: function (data) {
        this.tourRoomListManager.processPlayerEligible(data);
    },
    processJoinedPlayerInfo: function (room, data) {
        this.tourRoomListManager.setJoinedPlayerInfo(room, data);
    },
    processRegNumResp: function (dataObj, room) {
        this.tourRoomListManager.processRegNumResp(dataObj, room);
        this.tournamentDetail && this.tournamentDetail.updateRegPls();
    },
    processLevelProgress: function (dataObj) {
        this.tournamentDetail.showLevelProgress(dataObj);
    },
    processTableProgress: function (dataObj) {
        this.tournamentDetail.showTableProgress(dataObj);
    },
    processWaitNumResp: function (dataObj) {
        this.tourRoomListManager.processWaitNumResp(dataObj);
    },
    processNewTournament: function (roomIds) {
        this.tourRoomListManager.processNewTournament(roomIds);
    },
    processMyTournament: function (data) {
        this.tourRoomListManager.processMyTournament(data);
    },
    processTournamentRoomStatus: function (dataObj) {
        this.tourRoomListManager.processTournamentRoomStatus(dataObj);
    },
    handleTmntDetail: function (room, dataObj) {

        this.tourRoomListManager.setDetailVars(room, dataObj);
        var roombar = this.tourRoomListManager.getTrmntBarByRoomId(room.id);
        if (roombar) {
            this.tournamentDetail = new TournamentDetail(this, room, roombar);
            this._lobby.addChild(this.tournamentDetail, GameConstants.tournamentDetailZorder, "tournamentDetail");
        }

    },
    handleTournamentCancelCmd: function (room, dataObj) {
        this.tourRoomListManager.handleTournamentCancelCmd(room, dataObj);
    },
    updatePrizeStructure: function (room, dataObj) {
        this.tourRoomListManager.updatePrizeStructure(room, dataObj);
    },
    setDetailsStructureVars: function (room, dataObj) {
        this.tourRoomListManager.setDetailsStructureVars(room, dataObj);
    },
    setDetailsProgressVars: function (room, dataObj) {
        this.tourRoomListManager.setDetailsProgressVars(room, dataObj);
    },
    processJoinConfrmResp: function (room, dataObj) {
        this.tourRoomListManager.processJoinConfrmResp(room, dataObj);
    },
    processQuitConfrmResp: function (room, dataObj) {
        this.tourRoomListManager.processQuitConfrmResp(room, dataObj);
        this.tournamentDetail && this.tournamentDetail.quitConfirmationPopup && this.tournamentDetail.quitConfirmationPopup.removeFromParent(true);
    },
    processquitReq: function (roomId, radioType) {
        this.tourRoomListManager.processquitReq(roomId, radioType);
    },
    handleRoomVarsUpdate: function (room, changedVars) {
        cc.log("room variable update");
    },
    initCustomEventListener: function () {
        var _listener1 = cc.EventListener.create({
            event: cc.EventListener.CUSTOM,
            eventName: "STATUS_SYMBOL_EVENT",
            callback: function (event) {
                this.createToolTipJoin(event.getUserData());
            }.bind(this)
        });
        cc.eventManager.addListener(_listener1, 1);

        var _listener2 = cc.EventListener.create({
            event: cc.EventListener.CUSTOM,
            eventName: "QUESTION_MARK_EVENT",
            callback: function (event) {
                this.createToolTip(event.getUserData());
            }.bind(this)
        });
        cc.eventManager.addListener(_listener2, 1);

    },
    createToolTipJoin: function (dataArray) {
        this.spriteHover && this.spriteHover.removeFromParent(true);
        this.spriteHover = null;
        this.spriteHover2 && this.spriteHover2.removeFromParent(true);
        this.spriteHover2 = null;

        if (!this.spriteHover2 && dataArray[0]) {
            this.spriteHover2 = new TrnamentToolTip(dataArray);
            this.spriteHover2.setAnchorPoint(cc.p(0.5, 0));
            this.addChild(this.spriteHover2, 10, "spriteHover");
        }

        // this.spriteHover2 && this.spriteHover2.setVisible(dataArray[0]);
        // this.spriteHover && this.spriteHover.setVisible(false);

        var target = dataArray[2];
        if (target && dataArray[0]) {
            var locationInNode = target.convertToWorldSpace(cc.p(0, 0));
            locationInNode.x += target.width / 2;
            locationInNode.y -= 30 * scaleFactor;
            this.spriteHover2.setPosition(locationInNode);
            this.spriteHover2.label.setString(dataArray[1]);
        }
    },
    createToolTip: function (dataArray) {
        this.spriteHover && this.spriteHover.removeFromParent(true);
        this.spriteHover = null;
        this.spriteHover2 && this.spriteHover2.removeFromParent(true);
        this.spriteHover2 = null;

        if (!this.spriteHover && dataArray[0]) {
            this.spriteHover = new TrnamentToolTip(dataArray);
            this.spriteHover.setAnchorPoint(cc.p(0.5, 0));
            this.addChild(this.spriteHover, 10, "spriteHover");
        }

        // this.spriteHover && this.spriteHover.setVisible(dataArray[0]);
        // this.spriteHover2 && this.spriteHover2.setVisible(false);

        var target = dataArray[2];
        if (target && dataArray[0]) {
            var locationInNode = target.convertToWorldSpace(cc.p(0, 0));
            locationInNode.x += target.width / 2 + 2;
            locationInNode.y -= 30 * scaleFactor;
            this.spriteHover.setPosition(locationInNode);
            this.spriteHover.label.setString(dataArray[1]);
        }
    }
});

var TrnamentToolTip = cc.Scale9Sprite.extend({
    ctor: function (dataArray) {
        this._super(spriteFrameCache.getSpriteFrame("rect.png"), cc.rect(0, 0, 30, 30));
        this.setColor(cc.color(255, 238, 215));
        this.setCapInsets(cc.rect(9, 9, 9, 9));
        this.setCascadeColorEnabled(false);
        this.setVisible(true);


        var expectedSize = dataArray[3];

        this.label = new cc.LabelTTF(dataArray[1], "RobotoRegular", 26);
        this.label.setScale(0.5);
        this.label.setAnchorPoint(cc.p(0, 0.5));
        this.label.setColor(cc.color(0, 0, 0));
        this.label._setBoundingWidth(expectedSize.width * 2 - 10 * scaleFactor);
        this.addChild(this.label, 3);

        var labelSize = this.label.getContentSize();
        this.setContentSize(cc.size(labelSize.width * 0.5 + 10, labelSize.height * 0.5 + 10));
        this.label.setPosition(10 * scaleFactor, this.height / 2 - yMargin);

        var arrow = new cc.Sprite(spriteFrameCache.getSpriteFrame("arrowBorder.png"));
        arrow.setPosition(cc.p(this.width / 2 - 2 * scaleFactor, -arrow.height / 2 + 6 * scaleFactor));
        arrow.setRotation(-90);
        this.addChild(arrow, 1);

        return true;
    }
});


var CheckBoxModule = ccui.Layout.extend({
    tournamentLobby: null,
    ctor: function (nameOfCheckbox, margin, context) {
        this._super();
        this.setAnchorPoint(0, 0.5);
        this.tournamentLobby = context;

        var label = new cc.LabelTTF(nameOfCheckbox, "RobotoRegular", 18);
        label.setColor(cc.color(0, 0, 0));
        label.setScale(0.7);

        var checkBox = new ccui.CheckBox();
        checkBox.setName(nameOfCheckbox);
        checkBox.setTouchEnabled(true);
        checkBox.loadTextures("checkBox.png",
            "checkBoxSelected.png", "checkBoxSelected.png", "checkBoxSelected.png",
            "checkBoxSelected.png", ccui.Widget.PLIST_TEXTURE);

        this.setContentSize(cc.size((label.width / 1.5) + checkBox.width + margin, checkBox.height));

        checkBox.setPosition(cc.p(checkBox.width / 2, this.height / 2));
        checkBox.addEventListener(this.tournamentLobby.selectedCheckBoxStateEvent, this.tournamentLobby);

        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(checkBox.x + margin, this.height / 2 - yMargin));

        this.addChild(checkBox, 1);
        this.addChild(label, 1);
    }
});
