/**
 * Created by stpl on 2/17/2017.
 */
var ToolTip = cc.Scale9Sprite.extend({
    _isEnabled: null,
    ctor: function (size, color) {
        this._super(spriteFrameCache.getSpriteFrame("rect.png"));

        this.setCapInsets(cc.rect(10 * scaleFactor, 10 * scaleFactor, 8 * scaleFactor, 8 * scaleFactor));
        this.setContentSize(size);
        this.setCascadeColorEnabled(false);
        this.setColor(color);

        this._isEnabled = true;

        this.label = new cc.LabelTTF("", "RobotoRegular", 22 * scaleFactor);
        this.label.setScale(0.6);
        this.label.setAnchorPoint(0, 0.5);
        this.label.setColor(cc.color(0, 0, 0));
        this.label.setPosition(cc.p(10 * scaleFactor, this.height / 2));
        this.label.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.label._setBoundingWidth(this.width + this.width * .55);
        this.addChild(this.label, 1);

        var shadowW = size.width + 10 * scaleFactor;
        var shadowH = size.height + 10 * scaleFactor;
        var shadow = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("shadow.png"));
        shadow.setContentSize(cc.size(shadowW, shadowH));
        shadow.setPosition(cc.p(this.width / 2, this.height / 2 - 3 * scaleFactor));
        this.addChild(shadow, -2);

    },
    setText: function (str) {
        this.label.setString(str);
    },
    setArrow: function (direction) {
        this.arrow = new cc.Sprite(spriteFrameCache.getSpriteFrame("arrowBorder.png"));
        this.arrow.setAnchorPoint(1, 0.5);
        this.addChild(this.arrow, 1);

        var pos, rotation;

        !direction && (direction = "LEFT");
        switch (direction) {
            case "LEFT"  :
                pos = cc.p(1.5 * scaleFactor, this.height / 2);
                rotation = 0;
                break;
            case "RIGHT"  :
                pos = cc.p(this.width - 1.5 * scaleFactor, this.height / 2);
                rotation = 180;
                break;
            case "BOTTOM"  :
                pos = cc.p(this.width / 2, 1.8 * scaleFactor);
                rotation = -90;
                break;
            case "UP"  :
                pos = cc.p(this.width / 2, this.height - 1.5 * scaleFactor);
                rotation = 90;
                break;
        }
        this.arrow.setPosition(pos);
        this.arrow.setRotation(rotation);

    }
});