/**
 * Created by stpl on 11/30/2016.
 */
"use strict";
var MeldingPanel = cc.Layer.extend({
    _cardZOrder: 0,
    ccTimeOutManager: null,
    SpaceBWCard: 32 * scaleFactor,
    MaxSlotsOneSide: 12,
    jkrSprite: null,
    remainingTime: 0,
    _selectedCardArray: [],
    _currentFrame: 0,
    _gameRoom: null,
    clipper: null,

    ctor: function (context) {
        this._super();
        this._gameRoom = context;
        this.ccTimeOutManager = new CCTimeOutManager();

        var layout = cc.LayerGradient.extend({
            ctor: function (context) {
                this._super(cc.color(0, 0, 0), cc.color(0, 0, 0));
                var size = cc.winSize;
                this.setContentSize(cc.size(700 * scaleFactor, 110 * scaleFactor));
                this.setPosition(cc.p((context.width - 700 * scaleFactor) / 2, 320 * scaleFactor));
                this.setColorStops([{p: 0, color: new cc.Color(0, 0, 0, 0)},
                    {p: .5, color: cc.color.BLACK},
                    {p: 1, color: new cc.Color(0, 0, 0, 0)}]);
                //this.setStartOpacity(255);
                //this.setEndOpacity(0);
                this.setVector(cc.p(-1, 0));
            }
        });
        this.baseLyr = new layout(this);
        this.addChild(this.baseLyr, 10);

        this.txt = new cc.LabelTTF("", "RobotoRegular", 14 * 2 * scaleFactor);
        this.txt.setScale(0.5);
        this.txt.setPosition(this.width / 2, this.height / 2 + 75 * scaleFactor);
        this.txt.setColor(new cc.Color(255, 255, 255));
        this.txt.setContentSize(cc.size(500 * scaleFactor, 50 * scaleFactor));
        this.txt._setBoundingWidth(500 * 2 * scaleFactor);
        this.txt.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.addChild(this.txt, 12);

        this.loaderIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("loader_grey.png"));
        this.loaderIcon.setColor(cc.color(110, 134, 3));
        this.loaderIcon.setPosition(cc.p(this.width / 2, this.height / 2 + 40 * scaleFactor));
        this.addChild(this.loaderIcon, 12);
        this.loaderIcon.runAction(cc.rotateBy(1, 360, 0).repeatForever());
        this.loaderIcon.setVisible(false);

        this.timerTxt = new cc.LabelTTF("", "RobotoRegular", 12 * 2 * scaleFactor);
        this.timerTxt.setScale(0.5);
        this.timerTxt.setPosition(this.width / 2, this.height / 2 + 40 * scaleFactor);
        this.timerTxt.setColor(new cc.Color(199, 222, 47));
        this.timerTxt.setContentSize(cc.size(500 * scaleFactor, 50 * scaleFactor));
        this.timerTxt._setBoundingWidth(500 * 2 * scaleFactor);
        this.timerTxt.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.addChild(this.timerTxt, 12);

        this.extraTxt = new cc.LabelTTF("MAXIMUM ESTIMATED TIME", "RobotoRegular", 10 * 2 * scaleFactor);
        this.extraTxt.setScale(0.5);
        this.extraTxt.globalAlpha = 0.5;
        this.extraTxt.setPosition(this.width / 2, this.height / 2 + 10 * scaleFactor);
        this.extraTxt.setColor(cc.color(110, 134, 3));
        this.extraTxt.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.addChild(this.extraTxt, 12);
        this.extraTxt.setVisible(false);

        this.declareBtn = this.createSprite("DeclareBtn", "DeclareBtnOver", "declareBtn", cc.p(this.width / 2, this.height / 2 + 30 * scaleFactor));
        this.addChild(this.declareBtn, 14);
        this.declareBtn.hide(false);

        this.glow = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("tabGlow.png"));
        this.glow.setPosition(cc.p(this.width / 2, this.height / 2 + 30 * scaleFactor));
        this.glow.setContentSize(cc.size(this.declareBtn.width + 23 * scaleFactor, this.declareBtn.height + 23 * scaleFactor));
        this.addChild(this.glow, 13, "glow");
        this.startGlow(true);

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

    },
    startGlow: function (bool) {
        if (bool) {
            this.glow.setVisible(true);
            this.glow.stopAllActions();
            this.glow.setColor(cc.color(199, 222, 47));
            this.glow.runAction(cc.sequence(cc.fadeIn(0.5), cc.fadeOut(0.5)).repeatForever());
        } else {
            this.glow.stopAllActions();
            this.glow.setVisible(false);
        }
    },
    initMouseListener: function (element) {
        this.userCardPanelMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            swallowTouches: true,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    target._isEnabled && !target._hovering && target.hover(true);
                    target._hovering = true;
                    return true;
                }
                target._isEnabled && target._hovering && target.hover(false);
                target._hovering = false;
                return false;
            }.bind(this)
        });
        this.userCardPanelMouseListener.retain();
        cc.eventManager.addListener(this.userCardPanelMouseListener, this.declareBtn);
    },
    initTouchListener: function () {
        this.userCardPanelTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                if (target._isEnabled) {
                    switch (target.getName()) {
                        case "declareBtn" :
                            this._gameRoom._userCardPanel.meldHandler();
                            break;
                    }
                }
            }.bind(this)
        });
        this.userCardPanelTouchListener.retain();
        cc.eventManager.addListener(this.userCardPanelTouchListener, this.declareBtn);
    },
    yesBtnHandler: function () {

        this.startGlow(false);
        this.declareBtn.hide(true);
        this.setMeldMsg(6);
        this.meldingPanelResp();

        //todo remove _gameRoom.meldConfirmPopUp.visible = false;
        /*todo if(jkrSprite)
         jkrSprite.visible = false;
         joker_txt.visible = false;
         lowerjkr.visible = false;*/
    },
    meldingPanelResp: function () {
        this._gameRoom.cardString = this._gameRoom._userCardPanel.getGroupCardsStr(true);
        this._gameRoom._userCardPanel.hideAllItems(true);
        this.sendMeldCards();
    },
    sendMeldCards: function () {
        var obj = {};
        obj.cards = this.getGroupCardsStr(true);
        this._gameRoom.sendReqToServer("meldCards", obj);
        this._gameRoom._userCardPanel.hideAllItems(true);
        if (this._gameRoom._extraTimeToggle) {
            this._gameRoom.scheduleWaitTime(SFSConstants.ETT_MELDEXTRATIMER);
        }
        this.declareBtn.hide(true);
        this.startGlow(false);
        this.removeJoker();

        var str = "";
        if (this._gameRoom._currentTurnID == this._gameRoom._myId) {
            str = "Please wait while your opponents are melding.";
        }
        else {
            str = this._gameRoom.getPlayerNameById(this._gameRoom._currentTurnID) + " has placed a show. Please wait while your opponents are melding.";
            this.setMeldMsg(3, str);
        }
        if (this._gameRoom._currentTurnSeat) {
            this._gameRoom._currentTurnSeat.hideTimer();
        }
        this._gameRoom.sendUpdateTab();
    },
    setMeldMsg: function (tag, str, timer) {
        this._currentFrame = tag;
        this.timerTxt.setString("");
        this.loaderIcon.setVisible(false);
        this.extraTxt.setVisible(false);
        switch (tag) {
            case 1:
            {
                this.txt.setString("Please group your cards into valid sequences/sets and click on the Declare button.");
                break;
            }
            case 2:
                this.txt.setString("Congratulations! You have declared valid sequences/sets.\nPlease wait while your opponents are melding.");
                this.txt.setPosition(this.width / 2, this.height / 2 + 55 * scaleFactor);
                this.startGlow(false);
                this.declareBtn.hide(true);
                break;
            case 3:
            {
                this.txt.setString(str);
                break;
            }
            case 4:
                this.txt.setString("Congratulations! You have declared valid sequences/sets.\nPlease wait for disconnected players.");
                this.txt.setPosition(this.width / 2, this.height / 2 + 55 * scaleFactor);
                this.startGlow(false);
                this.declareBtn.hide(true);
                break;
            case 5:
            {
                if (str != "")
                    this.txt.setString(str);
                this.timerTxt.setString(timer);
                this.loaderIcon.setVisible(true);
                this.extraTxt.setVisible(true);
                this.txt.setPosition(this.width / 2, this.height / 2 + 80 * scaleFactor);
                this.startGlow(false);
                this.declareBtn.hide(true);
                break;
            }
            case 6:
                this.txt.setString("Please wait...");
                this.txt.setPosition(this.width / 2, this.height / 2 + 55 * scaleFactor);
                this.startGlow(false);
                this.declareBtn.hide(true);
                break;

        }
    },
    updateStrTimer: function (timer) {
        this.loaderIcon.setVisible(true);
        this.timerTxt.setString(timer);
        this.extraTxt.setVisible(true);
    },
    init: function (cardStr) {
        this.declareBtn.hide(false);
        this._gameRoom.deckPanel.hideAllItems(true);
        this._gameRoom.showValidity && this._gameRoom.showValidity.hide(true);
        this.setWildCard();
    },
    setWildCard: function () {

        var gameTypeStr = this._gameRoom._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
        var jokerName = this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD) && this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD).value;

        if (gameTypeStr != "Point-NoJoker") {
            this.joker_txt = new cc.LabelTTF("", "RobotoRegular", 18 * scaleFactor);
            this.joker_txt.setPosition(this.x + 151 * scaleFactor, this.height / 2 - 97 * scaleFactor);
            this.addChild(this.joker_txt, 12);
            this.addJoker(jokerName);
        }
    },
    getGroupCardsStr: function (check) {
        if (check == undefined)
            check = false;
        var str = "";
        var card;
        var allGrpCardsArr = [];
        var allUnGrpCardsArr = [];
        var createdGrp = 0;
        for (var a = 0; a < this._gameRoom._userCardPanel.numOfGrps; a++) {
            if (this._gameRoom._userCardPanel.grpArr[a].length > 0) {
                if ((this._gameRoom._userCardPanel.totCards == 21) || (this._gameRoom._userCardPanel.grpArr[a].length > 2))
                    createdGrp = createdGrp + 1;
            }
            if ((this._gameRoom.totCards == 13) && (this._gameRoom._userCardPanel.grpArr[a].length < 3))
                allUnGrpCardsArr = allUnGrpCardsArr.concat(this._gameRoom._userCardPanel.grpArr[a]);
            else
                allGrpCardsArr = allGrpCardsArr.concat(this._gameRoom._userCardPanel.grpArr[a]);
        }
        for (var b = this._gameRoom._userCardPanel.numOfGrps; b < this._gameRoom._userCardPanel.grpArr.length; b++) {
            allUnGrpCardsArr = allUnGrpCardsArr.concat(this._gameRoom._userCardPanel.grpArr[b]);
        }

        for (var i = 0; i < allGrpCardsArr.length; ++i) {
            card = allGrpCardsArr[i];
            str += card._name;
            if (i < allGrpCardsArr.length - 1) {
                if (card.grNo == allGrpCardsArr[i + 1].grNo)
                    str += ",";
                else
                    str += ";";
            }
        }
        if (check && (allUnGrpCardsArr.length >= this._gameRoom._userCardPanel.minNumOfCardIngrp) && (allUnGrpCardsArr.length <= this._gameRoom._userCardPanel.maxNumOfCardIngrp) && createdGrp < this._gameRoom._userCardPanel.numOfGrps)   // to group last ungrouped cards
        {
            str += ";";
            for (i = 0; i < allUnGrpCardsArr.length; ++i) {
                card = allUnGrpCardsArr[i];
                str += card._name + ",";
            }
            str = str.substr(0, str.length - 1);
            str += "|";
        }
        else {
            str += "|";
            for (i = 0; i < allUnGrpCardsArr.length; ++i) {
                card = allUnGrpCardsArr[i];
                str += card._name;
                if (i < allUnGrpCardsArr.length - 1) {
                    if (card.grNo == allUnGrpCardsArr[i + 1].grNo)
                        str += ",";
                    else
                        str += ":";
                }
            }
        }
        cc.log("Meld getGroupCardsStr>>>>>>>>>> ", str);
        return str;
    },
    removeJoker: function () {
        this.removeChild(this.clipper, true);
    },
    addJoker: function (jokerName) {

        var numOfCard = this._gameRoom._serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;
        // var numOfCard = "21";
        if (parseInt(numOfCard) == 13) {

            this.clipper = new ccui.Layout();
            // this.clipper.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
            // this.clipper.setColor(cc.color(155, 155, 155));
            this.clipper.setClippingEnabled(true);
            this.addChild(this.clipper, 4);
            this.clipper.setPosition(cc.p(285 * scaleFactor, (320 + 110) * scaleFactor));

            var jokerSprite = new CardModel(jokerName || "S#A");
            var contentSize = jokerSprite.getContentSize();
            this.clipper.setContentSize(contentSize);

            jokerSprite.setScale(0.7);
            jokerSprite.setPosition(cc.p(this.clipper.width / 2, 10 * scaleFactor));
            this.clipper.addChild(jokerSprite, 1);

            var jokerLabel = new cc.LabelTTF("JOKER", "RobotoMedium", 11.5 * 2 * scaleFactor);
            jokerLabel.setScale(0.5);
            jokerLabel.setPosition(cc.p(this.clipper.width / 2, this.clipper.height / 2 + 5 * scaleFactor));
            this.clipper.addChild(jokerLabel, 2);
        } else {

            var jkr, dwnJkr, upJkr, aceJkr;
            var str = jokerName;
            if (str && str != "") {

                this.clipper = new ccui.Layout();
                this.clipper.setContentSize(cc.size(150 * scaleFactor, 70 * scaleFactor));
                this.clipper.setClippingEnabled(true);
                this.clipper.setPosition(cc.p(250 * scaleFactor, (320 + 110) * scaleFactor));
                this.addChild(this.clipper, 4);

                var down_joker_label = new cc.LabelTTF("DOWN", "RobotoBold", 10 * 2 * scaleFactor);
                down_joker_label.setScale(0.5);
                this.clipper.addChild(down_joker_label, 4);

                var joker_label = new cc.LabelTTF("JOKER", "RobotoBold", 10 * 2 * scaleFactor);
                joker_label.setScale(0.5);
                this.clipper.addChild(joker_label, 4);

                var up_joker_label = new cc.LabelTTF("UP", "RobotoBold", 10 * 2 * scaleFactor);
                up_joker_label.setScale(0.5);
                this.clipper.addChild(up_joker_label, 4);

                jkr = new CardModel(str);
                jkr.setScale(0.6);

                dwnJkr = new CardModel(jkr._dwnJkr);
                dwnJkr.setScale(0.5);

                upJkr = new CardModel(jkr._upJkr);
                upJkr.setScale(0.5);

                if (str == "Joker") {
                    this.clipper.setContentSize(cc.size(200 * scaleFactor, 70 * scaleFactor));

                    dwnJkr.setPosition(cc.p(25 * scaleFactor, 8 * scaleFactor));
                    jkr.setPosition(cc.p(75 * scaleFactor, 15 * scaleFactor));
                    upJkr.setPosition(cc.p(175 * scaleFactor, 8 * scaleFactor));

                    down_joker_label.setPosition(cc.p(25 * scaleFactor, 41 * scaleFactor));
                    joker_label.setPosition(cc.p(75 * scaleFactor, 54 * scaleFactor));
                    up_joker_label.setPosition(cc.p(175 * scaleFactor, 41 * scaleFactor));

                    var ace_joker_label = new cc.LabelTTF("ACE", "RobotoBold", 10 * 2 * scaleFactor);
                    ace_joker_label.setScale(0.5);
                    ace_joker_label.setPosition(cc.p(125 * scaleFactor, 54 * scaleFactor));
                    this.clipper.addChild(ace_joker_label, 4);

                    aceJkr = new CardModel("S#A");
                    aceJkr.setScale(0.6);
                    aceJkr.setPosition(cc.p(125 * scaleFactor, 15 * scaleFactor));
                    this.clipper.addChild(aceJkr, 1);

                } else {

                    down_joker_label.setPosition(cc.p(25 * scaleFactor, 41 * scaleFactor));
                    joker_label.setPosition(cc.p(75 * scaleFactor, 54 * scaleFactor));
                    up_joker_label.setPosition(cc.p(125 * scaleFactor, 41 * scaleFactor));

                    dwnJkr.setPosition(cc.p(25 * scaleFactor, 8 * scaleFactor));
                    jkr.setPosition(cc.p(75 * scaleFactor, 15 * scaleFactor));
                    upJkr.setPosition(cc.p(125 * scaleFactor, 8 * scaleFactor));
                }

                this.clipper.addChild(dwnJkr, 1);
                this.clipper.addChild(jkr, 1);
                this.clipper.addChild(upJkr, 1);

            }
        }
    },
    createSprite: function (spriteName, spriteOverName, buttonName, pos) {
        var Sprite = cc.Sprite.extend({
            spriteOverName: null,
            spriteName: null,
            _isEnabled: null,
            _hovering: null,
            ctor: function (spriteName, spriteOverName, buttonName, pos) {
                this._super(spriteFrameCache.getSpriteFrame(spriteName + ".png"));
                this.spriteName = spriteName;
                this.spriteOverName = spriteOverName;
                this._hovering = false;
                pos && this.setPosition(pos);
                this.setName(buttonName);

                return true;
            },
            hover: function (bool) {

                bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteOverName + ".png")) :
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteName + ".png"));
            },
            pauseListener: function (bool) {
                this._isEnabled = !bool;
            },
            hide: function (bool) {
                this.setVisible(!bool);
                this._isEnabled = !bool;
            }
        });
        return new Sprite(spriteName, spriteOverName, buttonName, pos);
    }
});
