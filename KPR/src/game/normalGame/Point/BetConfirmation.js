/**
 * Created by stpl on 12/1/2016.
 */
var BetConfirmation = Popup.extend({
/////////////////////////////////
    MinReq: null,
    MaxReq: null,
    maxAllowed: null,
    acBal: null,
/////////////////////////////////
    _gameRoom: null,
    betToolTip: null,
    accountBalance: null,
    minRequiredChips: null,
    maxRequiredChips: null,
    maxAllowedLabel: null,
    maxAllowedChips: null,
    maxRadioBtn: null,
    otherAmountLabel: null,
    otherRadioBtn: null,
    inputValue: null,
    errorMsg: null,
    okBtn: null,
    cancelBtn: null,

    ctor: function (context) {
        this._super(new cc.Size(530 * scaleFactor, 335 * scaleFactor), context);
        this.headerLabel.setString("Confirmation");

        var blue = new cc.DrawNode();
        blue.drawRect(cc.p(0, 112 * scaleFactor), cc.p(this.width - 1 * scaleFactor, 0), cc.color(202, 222, 223), 0, cc.color(202, 222, 223));
        blue.setPosition(cc.p(0, 187 * scaleFactor));
        this.addChild(blue);

        var accountBalHeader = new cc.LabelTTF("Your Account Balanace:", "RobotoBold", 19 * 2 * scaleFactor);
        accountBalHeader.setScale(0.5);
        accountBalHeader.setAnchorPoint(1, 0.5);
        accountBalHeader.setPosition(cc.p(258 * scaleFactor, 270 * scaleFactor));
        accountBalHeader.setColor(cc.color(51, 86, 105));
        this.addChild(accountBalHeader);

        this.accountBalance = new cc.LabelTTF(0 + " Chips", "RobotoBold", 19 * 2 * scaleFactor);
        this.accountBalance.setScale(0.5);
        this.accountBalance.setAnchorPoint(0, 0.5);
        this.accountBalance.setPosition(cc.p(accountBalHeader.x + 5 * scaleFactor, accountBalHeader.y));
        this.accountBalance.setColor(cc.color(51, 86, 105));
        this.addChild(this.accountBalance);

        var miniRequiredHeader = new cc.LabelTTF("Minimum Required:", "RobotoRegular", 15 * 2 * scaleFactor);
        miniRequiredHeader.setScale(0.5);
        miniRequiredHeader.setAnchorPoint(1, 0.5);
        miniRequiredHeader.setPosition(cc.p(258 * scaleFactor, 245 * scaleFactor));
        miniRequiredHeader.setColor(cc.color(0, 0, 0));
        this.addChild(miniRequiredHeader);

        this.minRequiredChips = new cc.LabelTTF(0 + " Chips", "RobotoRegular", 16 * 2 * scaleFactor);
        this.minRequiredChips.setScale(0.5);
        this.minRequiredChips.setColor(cc.color(50, 50, 50));
        this.minRequiredChips.setAnchorPoint(0, 0.5);
        this.minRequiredChips.setPosition(cc.p(miniRequiredHeader.x + 5 * scaleFactor, miniRequiredHeader.y));
        this.addChild(this.minRequiredChips);

        var maxRequired = new cc.LabelTTF("Maximum Required:", "RobotoRegular", 15 * 2 * scaleFactor);
        maxRequired.setScale(0.5);
        maxRequired.setAnchorPoint(1, 0.5);
        maxRequired.setPosition(cc.p(258 * scaleFactor, 220 * scaleFactor));
        maxRequired.setColor(cc.color(0, 0, 0));
        this.addChild(maxRequired);

        this.maxRequiredChips = new cc.LabelTTF(0 + " Chips", "RobotoRegular", 16 * 2 * scaleFactor);
        this.maxRequiredChips.setScale(0.5);
        this.maxRequiredChips.setColor(cc.color(50, 50, 50));
        this.maxRequiredChips.setAnchorPoint(0, 0.5);
        this.maxRequiredChips.setPosition(cc.p(maxRequired.x + 5 * scaleFactor, maxRequired.y));
        this.addChild(this.maxRequiredChips);

        var question = new cc.LabelTTF("How Much would you like to bring to the table?", "RoboToBold", 19 * 2 * scaleFactor);
        question.setScale(0.5);
        question.setAnchorPoint(0, 1);
        question.setPosition(cc.p(30 * scaleFactor, 170 * scaleFactor));
        question.setColor(cc.color(58, 91, 110));
        this.addChild(question);

        this.maxAllowedLabel = new cc.LabelTTF("Maximum Allowed:", "Robotoregular", 16 * 2 * scaleFactor);
        this.maxAllowedLabel._name = "MaxText";
        this.maxAllowedLabel.setScale(0.5);
        this.maxAllowedLabel.setAnchorPoint(0, 1);
        this.maxAllowedLabel.setPosition(cc.p(55 * scaleFactor, 135 * scaleFactor));
        this.maxAllowedLabel.setColor(cc.color(0, 0, 0));
        this.maxAllowedLabel._hovering = false;
        this.maxAllowedLabel.hover = function (bool) {
        };
        this.addChild(this.maxAllowedLabel);

        this.maxAllowedChips = new cc.LabelTTF(0 + " Chips", "RobotoRegular", 16 * 2 * scaleFactor);
        this.maxAllowedChips.setScale(0.5);
        this.maxAllowedChips.setColor(cc.color(50, 50, 50));
        this.maxAllowedChips.setAnchorPoint(0, 1);
        this.maxAllowedChips.setPosition(cc.p(this.maxAllowedLabel.x + this.maxAllowedLabel.width / 2 + 5 * scaleFactor, this.maxAllowedLabel.y));
        this.addChild(this.maxAllowedChips);

        this.maxRadioBtn = new ccui.CheckBox();
        this.maxRadioBtn.loadTextures("radio-down.png", "radio-selected.png", "radio-selected.png", "radio-selected.png",
            "radio-selected.png", ccui.Widget.PLIST_TEXTURE);
        this.maxRadioBtn._name = "MaxRadio";
        this.maxRadioBtn.selected = true;
        this.maxRadioBtn.setPosition(cc.p(this.maxAllowedLabel.x - 15 * scaleFactor, 125 * scaleFactor));
        this.maxRadioBtn.setAnchorPoint(0.5, 0.5);
        this.maxRadioBtn.addEventListener(this.selectedStateEvent, this);
        this.addChild(this.maxRadioBtn);

        this.otherAmountLabel = new cc.LabelTTF("Other Amount", "RobotoRegular", 16 * 2 * scaleFactor);
        this.otherAmountLabel.setScale(0.5);
        this.otherAmountLabel._name = "OtherText";
        this.otherAmountLabel.setAnchorPoint(0, 1);
        this.otherAmountLabel.setPosition(cc.p(55 * scaleFactor, 103 * scaleFactor));
        this.otherAmountLabel.setColor(cc.color(0, 0, 0));
        this.otherAmountLabel.hover = function (bool) {
        };
        this.addChild(this.otherAmountLabel, 1);

        this.otherRadioBtn = new ccui.CheckBox();
        this.otherRadioBtn.loadTextures("radio-down.png", "radio-selected.png", "radio-selected.png", "radio-selected.png",
            "radio-selected.png", ccui.Widget.PLIST_TEXTURE);
        this.otherRadioBtn._name = "other";
        this.otherRadioBtn.setPosition(cc.p(this.otherAmountLabel.x - 15 * scaleFactor, this.otherAmountLabel.y - 10 * scaleFactor /*this.maxAllowedLabel.y - this.maxAllowedLabel.height / 3 - 30*/));
        this.otherRadioBtn.setAnchorPoint(0.5, 0.5);
        this.otherRadioBtn.addEventListener(this.selectedStateEvent, this);
        this.addChild(this.otherRadioBtn);

        /*
         this.inputValue = new cc.EditBox(cc.size(75, 25), new cc.Scale9Sprite("whiteBoxPopUp.png"), new cc.Scale9Sprite("whiteBoxPopUp.png"));
         this.inputValue.setInputFlag(cc.EDITBOX_INPUT_MODE_SINGLELINE);
         this.inputValue.setAnchorPoint(0, 1);
         this.inputValue.setColor(cc.color(220, 220, 220));
         this.inputValue.setPosition(160, 110);
         this.inputValue.setFontColor(cc.color(0, 0, 0));
         this.inputValue.setFontName("RobotoRegular");
         this.inputValue.setFontSize(14);
         this.inputValue.setDelegate(this);
         this.inputValue.setMaxLength(13);
         this.inputValue.setInputFlag(cc.EDITBOX_INPUT_MODE_NUMERIC);
         this.addChild(this.inputValue);
         */

        var inputBg = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png"));
        inputBg.setCapInsets(cc.rect(16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor));
        inputBg.setContentSize(cc.size(75 * scaleFactor, 25 * scaleFactor));
        inputBg.setAnchorPoint(0, 1);
        inputBg.setPosition(cc.p(165 * scaleFactor, 105 * scaleFactor));
        inputBg.setColor(cc.color(245, 245, 245));
        inputBg.setName("InputBg");
        this.addChild(inputBg, 3);

        this.inputBgGlow = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("betConfirmGlow.png"));
        this.inputBgGlow.setCapInsets(cc.rect(24 * scaleFactor, 8 * scaleFactor, 51 * scaleFactor, 17 * scaleFactor));
        this.inputBgGlow.setContentSize(cc.size(88 * scaleFactor, 38 * scaleFactor));
        this.inputBgGlow.setAnchorPoint(0, 1);
        this.inputBgGlow.setPosition(cc.p(158.5 * scaleFactor, 110 * scaleFactor));
        this.addChild(this.inputBgGlow, 5);
        this.inputBgGlow.setVisible(false);

        this.inputValue = new cc.TextFieldTTF("", cc.size(73 * scaleFactor, 14 * scaleFactor), cc.TEXT_ALIGNMENT_LEFT, "RobotoRegular", 14 * scaleFactor);
        this.inputValue.setAnchorPoint(0, 1);
        this.inputValue.setPosition(cc.p(167 * scaleFactor, 100 * scaleFactor));
        this.inputValue.setFontFillColor(cc.color(0, 0, 0));
        this.inputValue.setDelegate(this);
        this.addChild(this.inputValue, 50, "inputValue");

        var chips = new cc.LabelTTF("Chips", "RobotoRegular", 16 * 2 * scaleFactor);
        chips.setScale(0.5);
        chips.setColor(cc.color(0, 0, 0));
        chips.setPosition(cc.p(265 * scaleFactor, 92 * scaleFactor));
        this.addChild(chips);

        var helpTxt = new cc.LabelTTF("(Please enter amount in the Box)", "RobotoRegular", 16 * 2 * scaleFactor);
        helpTxt.setScale(0.4);
        helpTxt.setColor(cc.color(0, 0, 0));
        helpTxt.setPosition(cc.p(405 * scaleFactor, 92 * scaleFactor));
        this.addChild(helpTxt);

        this.errorMsg = new cc.LabelTTF("Maximum amount for game is 80000", "RobotoRegular", 12 * 2 * scaleFactor);
        this.errorMsg.setScale(0.5);
        this.errorMsg.setAnchorPoint(0, 0.5);
        this.errorMsg.setColor(cc.color(255, 0, 0));
        this.errorMsg.setPosition(cc.p(33 * scaleFactor, 65 * scaleFactor));
        this.errorMsg.setVisible(false);
        this.addChild(this.errorMsg, 2);

        this.okBtn = new CreateBtn("OkBtn", "OkBtnOver", "OK");
        this.okBtn.setPosition(cc.p(this.width / 2 - 50 * scaleFactor, 40 * scaleFactor));
        this.addChild(this.okBtn, 1);

        this.cancelBtn = new CreateBtn("cancel", "cancelOver", "Cancel");
        this.cancelBtn.setPosition(cc.p(this.width / 2 + 50 * scaleFactor, 40 * scaleFactor));
        this.addChild(this.cancelBtn, 1);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), this.okBtn);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.cancelBtn);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.otherAmountLabel);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.maxAllowedLabel);

        cc.eventManager.addListener(this.popupTouchListener.clone(), this.inputValue);
        cc.eventManager.addListener(this.popupTouchListener.clone(), inputBg);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.okBtn);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.cancelBtn);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.otherAmountLabel);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.maxAllowedLabel);

        this.initConfirmation();
        return true;
    },
    selfBgTouchListener: function (touch, event) {
        cc.log("Blank listener in BetConfirmation");
        this.inputBgGlow.setVisible(false);
        this.removeToolTip();
    },
    onTextFieldAttachWithIME: function () {
        this.inputBgGlow.setVisible(true);
    },
    onTextFieldDetachWithIME: function () {
    },
    onTextFieldDeleteBackward: function () {
    },
    onTextFieldInsertText: function (sender, text, len) {
        if (text == "\n") {
            var value = 0;
            if (this.maxRadioBtn.selected == true) {
                value = this.getMaxAllowed();
            } else if (this.otherRadioBtn.selected == true) {
                value = this.inputValue.getString();
            } else if (this.maxRadioBtn.selected == false && this.otherRadioBtn.selected == false) {
                throw new Error("Both radio can not be off. error-code:B-C234");
            }
            this._gameRoom.seatConfirmHandler(value);
        }

        var str = sender._string;
        var dotIndex = str.indexOf(".");
        if (text == "." && dotIndex > -1)
            return true;

        if (!/^[0-9.]+$/.test(text)) {
            sender._string = sender._string.slice(0, sender._string.length - 1);
            return true;
        }

        var decimals = sender._string.split(".");
        if (decimals[1] && decimals[1].length >= 2)
            return true;

        if (parseFloat(sender._string) >= this.maxAllowed) {
            return true;
        }

    },
    onClickTrackNode: function (clicked) {
        var textField = this.inputValue;
        clicked && textField.attachWithIME();
        !clicked && textField.detachWithIME();
        textField.setPlaceHolder("|");
        this.maxRadioBtn.selected = false;
        this.otherRadioBtn.selected = true;
    },
    editBoxTextChanged: function (sender, value) {
        //on input
        var str = sender._edTxt.value;
        var lastChar = str.substr(str.length - 1, str.length);
        if (lastChar == "." && value == ".")
            return;

        if (!/^[0-9]+$/.test(value)) {
            str = value.slice(0, value.length - 1);
        }
        sender._edTxt.value = str;
    },
    editBoxReturn: function (sender, e) {
        //keyboard
        if (e.keyCode === cc.KEY.enter) {
            var value = 0;
            if (this.maxRadioBtn.selected == true) {
                value = this.getMaxAllowed();
            } else if (this.otherRadioBtn.selected == true) {
                value = this.inputValue.getString();
            } else if (this.maxRadioBtn.selected == false && this.otherRadioBtn.selected == false) {
                throw new Error("Both radio can not be off");
            }
            /*this.maxRadioBtn.selected = false;
             this.otherRadioBtn.selected = false;*/
            this._gameRoom.seatConfirmHandler(value);
        }
    },
    editBoxEditingDidBegin: function (sender) {
        //focus
        this.maxRadioBtn.selected = false;
        this.otherRadioBtn.selected = true;
        sender._edTxt.focus();
    },
    initConfirmation: function () {
        var minBet = this._gameRoom._serverRoom.getVariable(SFSConstants._BET_AMOUNT).value;
        var maxBet = minBet * 10;
        var myBal = this._gameRoom.getMyAmount();

        this.setAccountBal(myBal);
        this.setMinRequired(minBet);
        this.setMaxRequired(maxBet);
        this.setMaxAllowed(maxBet);
        this.inputValue.setString(minBet);
        this.otherRadioBtn.selected = false;
        this.maxRadioBtn.selected = false;

        if (myBal < maxBet) {
            // this.maxRadioBtn.selected = true;
            this.otherRadioBtn.selected = true;
        }
        else {
            this.maxRadioBtn.selected = true;
            // this.otherRadioBtn.selected = true;
        }
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        switch (target._name) {
            case "InputBg":
                this.onClickTrackNode(true);
                break;
            case "inputValue":
                this.onClickTrackNode(true);
                break;
            case "MaxText":
                this.otherRadioBtn.selected = false;
                this.maxRadioBtn.selected = true;
                this.inputBgGlow.setVisible(false);
                // this.inputValue._edTxt.blur();
                break;

            case "OtherText":
                this.maxRadioBtn.selected = false;
                this.otherRadioBtn.selected = true;
                // this.inputValue._edTxt.focus();
                this.onClickTrackNode(true);
                break;

            case "OK":
                var value = 0;
                if (this.maxRadioBtn.selected == true) {
                    value = this.getMaxAllowed();
                } else if (this.otherRadioBtn.selected == true) {
                    value = this.inputValue.getString();
                }
                this._gameRoom.seatConfirmHandler(value);
                this.inputBgGlow.setVisible(false);
                break;

            case "Cancel":
                this.removeFromParent(true);
                this.inputBgGlow.setVisible(false);
                break;
        }
    },
    setMinRequired: function (value) {
        this.MinReq = value;
        this.minRequiredChips.setString(value + " Chips");
    },
    getMinRequired: function () {
        return this.MinReq;
    },
    setMaxRequired: function (value) {
        this.MaxReq = value;
        this.maxRequiredChips.setString(value + " Chips")
    },
    getMaxRequired: function () {
        return this.MaxReq;
    },
    setMaxAllowed: function (value) {
        this.maxAllowed = value;
        this.maxAllowedChips.setString(value + " Chips");
    },
    getMaxAllowed: function () {
        return this.maxAllowed;
    },
    setAccountBal: function (value) {
        this.acBal = value;
        this.accountBalance.setString(value + " Chips");
    },
    getAccountBal: function () {
        return this.acBal;
    },
    selectedStateEvent: function (sender, type) {
        switch (type) {
            case ccui.CheckBox.EVENT_UNSELECTED:
                if (sender.selected == false)
                    sender.selected = true;
                break;

            case ccui.CheckBox.EVENT_SELECTED:
                if (sender._name == "MaxRadio") {
                    this.otherRadioBtn.selected = false;
                } else {
                    this.maxRadioBtn.selected = false;
                }
                break;
        }
    },
    createToolTip: function (gameType, amount) {
        this.toolTip = new BetToolTip(gameType, amount, this);
        this.toolTip.setPosition(cc.p(400 * scaleFactor, 230 * scaleFactor));
        this.addChild(this.toolTip, 10, "toolTip");
    },
    removeToolTip: function () {
        this.toolTip && this.removeChild(this.toolTip, true);
    },
    tooTipBtnClickHandler: function (gameType) {
        if (gameType == "CASH") {
            this._gameRoom.cashBtnHandler("addCashBtn");
        } else {
            this._gameRoom.refillBtnHandler();
        }
    }
});