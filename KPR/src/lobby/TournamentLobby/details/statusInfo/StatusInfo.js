/**
 * Created by stpl on 1/20/2017.
 */
var StatusInfo = cc.LayerColor.extend({
    _noOfLines: null,
    ctor: function () {
        this._super(cc.color(234, 234, 234));
        this.setContentSize(cc.size(200 * scaleFactor, 500 * scaleFactor));
        this.setPosition(cc.p(1 * scaleFactor, 1 * scaleFactor));
    }
});
StatusInfo.prototype._expectedPrize = function (header, value) {

    this.expectedPrizeHeader = new cc.LabelTTF(header, "RobotoRegular", 18 * scaleFactor);
    this.expectedPrizeHeader.setScale(0.7);
    this.expectedPrizeHeader.setPosition(cc.p(100 * scaleFactor, 478 * scaleFactor));
    this.expectedPrizeHeader.setColor(cc.color(70, 70, 70));
    this.addChild(this.expectedPrizeHeader, 3);

    this.expectedPrize = new cc.LabelTTF("`" + value, "RupeeFordian", 21 * scaleFactor);
    this.expectedPrize.setScale(0.7);
    this.expectedPrize.setPosition(cc.p(100 * scaleFactor, 453 * scaleFactor));
    this.expectedPrize.setColor(cc.color(0, 0, 0));
    this.expectedPrize.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
    this.expectedPrize.setDimensions({width: this.width + this.width * 0.3 - 5 * scaleFactor, height: 22 * scaleFactor});
    this.addChild(this.expectedPrize, 3);
};
//viewPlayerBtn
StatusInfo.prototype._viewPlayerBtn = function () {
    var ViewPlayerBtn = cc.LabelTTF.extend({
        ctor: function () {

            this.underline = new cc.LabelTTF("", "RobotoRegular", 20 * scaleFactor);
            this.underline.setAnchorPoint(0, 0);
            this.underline.setPosition(cc.p(-2 * scaleFactor, -2 * scaleFactor));

            this._super("View Joined Players", "RobotoRegular", 20 * scaleFactor);
            this.setScale(0.8);
            this.setPosition(cc.p(100 * scaleFactor, 412 * scaleFactor));
            this.setCascadeColorEnabled(true);

            this.addChild(this.underline, 3);
            return true;
        },
        setString: function (text) {
            text = String(text);
            if (this._originalText !== text) {
                this._originalText = text + "";
                this._updateString();
                // Force update
                this._setUpdateTextureDirty();
                this._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
            }
            var len = this.getString().length;
            this.underline.setString("_".repeat(len + 2));
        }
    });
    this.viewPlayerBtn = new ViewPlayerBtn();
    this.addChild(this.viewPlayerBtn, 3, "viewPlayerBtn");
};
StatusInfo.prototype._tournamentStatus = function () {
    var registrationStarted = new cc.LabelTTF("Registration\nStarted", "RobotoRegular", 23 * scaleFactor);
    registrationStarted.setScale(0.7);
    registrationStarted.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
    registrationStarted.setPosition(cc.p(100 * scaleFactor, 395 * scaleFactor));
    registrationStarted.setColor(cc.color(0, 0, 0));
    this.addChild(registrationStarted, 3);
};
StatusInfo.prototype._registrationStartStatus = function () {
    var registrationStartHeader = new cc.LabelTTF("Registration Start", "RobotoRegular", 18 * scaleFactor);
    registrationStartHeader.setScale(0.7);
    registrationStartHeader.setPosition(cc.p(100 * scaleFactor, 412 * scaleFactor));
    registrationStartHeader.setColor(cc.color(70, 70, 70));
    this.addChild(registrationStartHeader, 3);

    this.registrationStart = new cc.LabelTTF("03:40 pm Today", "RobotoRegular", 22 * scaleFactor);
    this.registrationStart.setScale(0.7);
    this.registrationStart.setPosition(cc.p(100 * scaleFactor, 388 * scaleFactor));
    this.registrationStart.setColor(cc.color(0, 0, 0));
    this.addChild(this.registrationStart, 3);
};
StatusInfo.prototype._registrationCloseStatus = function () {
    var registrationClosesHeader = new cc.LabelTTF("Registration Closes", "RobotoRegular", 18 * scaleFactor);
    registrationClosesHeader.setScale(0.7);
    registrationClosesHeader.setPosition(cc.p(100 * scaleFactor, 337 * scaleFactor));
    registrationClosesHeader.setColor(cc.color(70, 70, 70));
    this.addChild(registrationClosesHeader, 3);
    this.registrationCloses = new cc.LabelTTF("03:40 pm Today", "RobotoRegular", 22 * scaleFactor);
    this.registrationCloses.setScale(0.7);
    this.registrationCloses.setPosition(cc.p(100 * scaleFactor, 313 * scaleFactor));
    this.registrationCloses.setColor(cc.color(0, 0, 0));
    this.addChild(this.registrationCloses, 3);
};
StatusInfo.prototype._registrationClosedStatus = function () {
    var registrationStarted = new cc.LabelTTF("Registration\nClosed", "RobotoRegular", 23 * scaleFactor);
    registrationStarted.setScale(0.7);
    registrationStarted.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
    registrationStarted.setPosition(cc.p(100 * scaleFactor, 325 * scaleFactor));
    registrationStarted.setColor(cc.color(0, 0, 0));
    this.addChild(registrationStarted, 3);
};
StatusInfo.prototype._estimatedDuration = function () {
    var estimatedDurationHeader = new cc.LabelTTF("Estimated Duration", "RobotoRegular", 18 * scaleFactor);
    estimatedDurationHeader.setScale(0.7);
    estimatedDurationHeader.setPosition(cc.p(100 * scaleFactor, 268 * scaleFactor));
    estimatedDurationHeader.setColor(cc.color(70, 70, 70));
    this.addChild(estimatedDurationHeader, 3);
    this.estimatedDuration = new cc.LabelTTF("01 Hour =30 Minute", "RobotoRegular", 22 * scaleFactor);
    this.estimatedDuration.setScale(0.7);
    this.estimatedDuration.setPosition(cc.p(100 * scaleFactor, 245 * scaleFactor));
    this.estimatedDuration.setColor(cc.color(0, 0, 0));
    this.addChild(this.estimatedDuration, 3);
};
StatusInfo.prototype._tournamentStart = function () {
    var tournamentStartHeader = new cc.LabelTTF("Tournament Starts", "RobotoRegular", 18 * scaleFactor);
    tournamentStartHeader.setScale(0.7);
    tournamentStartHeader.setPosition(cc.p(100 * scaleFactor, 198 * scaleFactor));
    tournamentStartHeader.setColor(cc.color(70, 70, 70));
    this.addChild(tournamentStartHeader, 3);
    this.tournamentStartTime = new cc.LabelTTF("03:40 pm Today", "RobotoRegular", 22 * scaleFactor);
    this.tournamentStartTime.setScale(0.7);
    this.tournamentStartTime.setPosition(cc.p(100 * scaleFactor, 175 * scaleFactor));
    this.tournamentStartTime.setColor(cc.color(0, 0, 0));
    this.addChild(this.tournamentStartTime, 3);
};
StatusInfo.prototype._tournamentDuration = function () {
    var tournamentDurationHeader = new cc.LabelTTF("Tournament Duration", "RobotoRegular", 18 * scaleFactor);
    tournamentDurationHeader.setScale(0.7);
    tournamentDurationHeader.setPosition(cc.p(100 * scaleFactor, 337 * scaleFactor));
    tournamentDurationHeader.setColor(cc.color(70, 70, 70));
    this.addChild(tournamentDurationHeader, 3);
    this.tournamentDuration = new cc.LabelTTF("01 Hour =30 Minute", "RobotoRegular", 22 * scaleFactor);
    this.tournamentDuration.setScale(0.7);
    this.tournamentDuration.setPosition(cc.p(100 * scaleFactor, 313 * scaleFactor));
    this.tournamentDuration.setColor(cc.color(0, 0, 0));
    this.addChild(this.tournamentDuration, 3);
};
StatusInfo.prototype._timer = function () {
    this.countDowntimer = new CountDownTimer();
    this.countDowntimer.setPosition(cc.p(100 * scaleFactor, 130 * scaleFactor));
    this.addChild(this.countDowntimer, 2);
};
StatusInfo.prototype._instructions = function () {
    this.instructions = new CreateBtn("Instruction", "InstructionOver", "instructions", cc.p(100 * scaleFactor, 60 * scaleFactor));
    this.addChild(this.instructions, 3);
};
StatusInfo.prototype._drawLines = function (lines) {
    var line1 = new cc.DrawNode();
    line1.drawSegment(cc.p(1 * scaleFactor, 432 * scaleFactor), cc.p(199 * scaleFactor, 432 * scaleFactor), 0.5, cc.color(220, 220, 220));
    this.addChild(line1);
    if (lines == 1)return false;
    var line2 = new cc.DrawNode();
    line2.drawSegment(cc.p(1 * scaleFactor, 362 * scaleFactor), cc.p(199 * scaleFactor, 362 * scaleFactor), 0.5, cc.color(220, 220, 220));
    this.addChild(line2);
    if (lines == 2)return false;
    var line3 = new cc.DrawNode();
    line3.drawSegment(cc.p(1 * scaleFactor, 292 * scaleFactor), cc.p(199 * scaleFactor, 292 * scaleFactor), 0.5, cc.color(220, 220, 220));
    this.addChild(line3);
    if (lines == 3)return false;
    var line4 = new cc.DrawNode();
    line4.drawSegment(cc.p(1 * scaleFactor, 222 * scaleFactor), cc.p(199 * scaleFactor, 222 * scaleFactor), 0.5, cc.color(220, 220, 220));
    this.addChild(line4);
};
StatusInfo.prototype._playerStatus = function () {
    this.playerStatus = new cc.LabelTTF("Player Status", "RobotoRegular", 22 * scaleFactor);
    this.playerStatus.setScale(0.6);
    this.playerStatus.setPosition(cc.p(100 * scaleFactor, 410 * scaleFactor));
    this.playerStatus.setColor(cc.color(0, 0, 0));
    this.addChild(this.playerStatus, 3);
};
StatusInfo.prototype._viewJoinedPlayers = function () {

};
StatusInfo.prototype._viewWinners = function () {

};