/**
 * Created by stpl on 1/24/2017.
 */
var ButtonHandler = {
    handlerFun: function (target, context) {
        if (target._isEnabled) {
            switch (target._name) {
                case "Join":
                    this.joinBtnHandler(context);
                    break;
                case "Quit":
                    this.quitBtnHandler(context);
                    break;
                case "TakeSeat":
                    this.takeSeatBtnHandler(context);
                    break;
                case "Details":
                    this.detailsBtnHandler(context);
                    break;
                case "Waiting":
                    this.joinBtnHandler(context);
                    break;
                default:
                //this._joinRoom();
            }
        }
    },
    joinBtnHandler: function (context) {
        cc.log("Join Req");
        var roomZone = sfs.getRoomByName("TournamentLobby");
        var id = roomZone.id;
        if(!roomZone.isJoined){
            sfs.send(new SFS2X.Requests.System.JoinRoomRequest(id, null, -1, true));
            // return;
        }

        var status = context.status;
        var tRoomId = context.roomId;
        if (status == TOURNAMENT_CONSTANTS.STATUS_JOIN || status == TOURNAMENT_CONSTANTS.STATUS_WAITING) {
            var reqObj = {};
            /*if (tRoomId != -1)
             reqObj["feeType"] = "Chip";*/
            sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.PLAYER_ELIGIBLE_REQ, reqObj, tRoomId);
        }
    },
    quitBtnHandler: function (context) {
        var roomZone = sfs.getRoomByName("TournamentLobby");
        var id = roomZone.id;
        if(!roomZone.isJoined){
            sfs.send(new SFS2X.Requests.System.JoinRoomRequest(id, null, -1, true));
            return;
        }
        var room = sfsClient.getRoomById(context.roomId);
        this.quitConfirmationPopup && this.quitConfirmationPopup.removeFromParent(true);
        this.quitConfirmationPopup = new QuitConfirmationPopup(room, this, true, "quitConfrmPopup");
        applicationFacade._lobby.addChild(this.quitConfirmationPopup, GameConstants.popupZorder, "quitConfirmationPopup");

        // this.tournamentRoomListManager.processQuitPopup(this._parentClass.roomId);
    },
    takeSeatBtnHandler: function (context) {
        cc.log("TakeSeat Req");
        var roomZone = sfs.getRoomByName("TournamentLobby");
        var id = roomZone.id;
        if(!roomZone.isJoined){
            sfs.send(new SFS2X.Requests.System.JoinRoomRequest(id, null, -1, true));
            return;
        }
        var status = context.status;
        var tRoomId = context.roomId;
        var reqObj = {};
        /*if (tRoomId != -1)
            reqObj["feeType"] = "Chip";*/
        if (!applicationFacade.takeSeatFromLobby(tRoomId))
            sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_TAKESEAT_REQUEST, reqObj, tRoomId);
    },
    detailsBtnHandler: function (context) {
        cc.log("Details Req");
        var roomZone = sfs.getRoomByName("TournamentLobby");
        var id = roomZone.id;
        if(!roomZone.isJoined){
            sfs.send(new SFS2X.Requests.System.JoinRoomRequest(id, null, -1, true));
            return;
        }
        //closeDetailHandler(null);
        var tourName;
        var status = context.statusSym && context.statusSym.statusText && context.statusSym.statusText._originalText;
        if (context.room)
            tourName = context.room.getVariable("tournamentName").value;
        else
            tourName = context.tourNameValue._originalText;
        if (status == "Cancelled") {
            var message = "Due to lack of minimum participants (players), this tournament has been called off. Players entry fee would be refunded back to players account respectively.";
            var size = new cc.Size(600 * scaleFactor, 170 * scaleFactor);
            if (!context.tournamentRoomListManager) {
                context.tourCommonPopup && context.tourCommonPopup.removeFromParent(true);
                context.tourCommonPopup = new TourCommonPopup(message, tourName, size, this, true);
                applicationFacade._lobby.addChild(context.tourCommonPopup, GameConstants.popupZorder, "tourCancelPopup");
                return;
            } else {
                context.tournamentRoomListManager.tourCommonPopup && context.tournamentRoomListManager.tourCommonPopup.removeFromParent(true);
                context.tournamentRoomListManager.tourCommonPopup = new TourCommonPopup(message, tourName, size, this, true);
                applicationFacade._lobby.addChild(context.tournamentRoomListManager.tourCommonPopup, GameConstants.popupZorder, "tourCancelPopup");
                return;
            }
        }
            var tRoomId = context.roomId;
            var dataObj = {};
            dataObj["isPrizeStructure"] = true;
            sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.PRIZE_INFO, dataObj, tRoomId);
            var reqObj = {};
            if (tRoomId != -1)
                reqObj["roomId"] = tRoomId;
            sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.DETAIL_INFO, reqObj, tRoomId);
        }
    };
