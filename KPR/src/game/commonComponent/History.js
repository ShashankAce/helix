/**
 * Created by stpl on 11/18/2016.
 */
var History = Popup.extend({
    _name: "HistoryPopup",
    _historyCards: null,
    _wildJoker: null,
    ctor: function (_gameRoom, numOfCard) {

        var size = 130;
        if (parseInt(numOfCard) == 13)size = 130;
        else size = 190;

        this._super(new cc.Size(530 * scaleFactor, size * scaleFactor), _gameRoom, true);

        this.headerLabel.setString("First Hand History");

        this.closeBtn = new CloseButton;
        this.closeBtn.setPosition(cc.p(this.width, this.height));
        this.addChild(this.closeBtn, 50);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), this.closeBtn);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.closeBtn);

        return true;
    },
    initCards: function (cardStr, wildCard) {
        this._historyCards = cardStr;

        var cardModel;
        var crdArr = this._historyCards.split(",");
        cc.log(" crdArr:  ", crdArr);

        var yPos = (this.height - this.headerLeft.height) - 46 * scaleFactor;
        var space = 4 * scaleFactor;
        var ySpace = 8 * scaleFactor;
        var scale = 0.85;
        var dmC = cardModel = new DummyCard("S#A");

        var initial = (this.width - 12 * (dmC.width * scale + space )) / 2;
        for (var i = 0; i < 13; i++) {
            cardModel = new DummyCard(crdArr[i]);
            cardModel.x = initial + i * (cardModel.width * scale + space);
            cardModel.y = yPos;
            cardModel.setScale(scale);
            this.addChild(cardModel, 6);
        }
        if (crdArr.length > 13) {
            initial = (this.width - (crdArr.length - 13) * (dmC.width * scale + space )) / 2;
            for (i = 13; i < crdArr.length; i++) {
                cardModel = new DummyCard(crdArr[i]);
                cardModel.x = initial + (i - 13) * (cardModel.width * scale + space);
                cardModel.y = yPos - dmC.height - ySpace;
                cardModel.setScale(scale);
                this.addChild(cardModel, 6);
            }
        }
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        switch (target._name) {
            case "closeBtn":
                this.closePopUp(true);
                break;
        }
    },
    closePopUp: function () {
        this.setVisible(false);
        this.removeFromParent(true);
        this._gameRoom.settingPanel.setVisible(false);
        //removeFromParent(true);
        //leave table request
    }
}), CloseButton = cc.Sprite.extend({
    _hovering: null,
    ctor: function () {
        this._super(spriteFrameCache.getSpriteFrame("ResultClose.png"));
        this.setName("closeBtn");
        this.setPosition(cc.p(102 * scaleFactor, 30 * scaleFactor));
        this._hovering = false;
        return true;
    },
    hover: function (bool) {
        //bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("confirmationLeaveTable.png")) : this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("confirmationLeaveTable.png"));
    }
}), HistoryCards = ccui.Layout.extend({
    ctor: function () {
        this._super(_historyCards);
        cc.log("_historyCards: ", _historyCards);
        var cardModel;
        var crdArr = _historyCards.split(",");
        cc.log(" crdArr:  ", crdArr);

        for (var i = 0; i < 13; i++) {
            cardModel = new DummyCard(crdArr[i]);
            cardModel.x = i * (cardModel.width + 5 * scaleFactor);
            cardModel.y = 0;
            this.addChild(cardModel, 1);
        }
        if (crdArr.length > 13) {
            for (i = 13; i < crdArr.length; i++) {
                cardModel = new DummyCard(crdArr[i]);
                cardModel.x = (i - 13) * (cardModel.width + 5 * scaleFactor);
                cardModel.y = 0;
                this.addChild(cardModel, 2);
            }
        }
    }
});