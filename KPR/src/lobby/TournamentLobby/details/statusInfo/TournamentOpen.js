/**
 * Created by stpl on 1/20/2017.
 */
var TournamentOpen = StatusInfo.extend({
    ctor: function (context) {
        this._super();

        var CashToolTip = cc.Scale9Sprite.extend({
            _isEnabled: null,
            ctor: function () {
                this._super(spriteFrameCache.getSpriteFrame("rect.png"));
                this.setColor(cc.color(255, 238, 215));
                this.setCapInsets(cc.rect(10 * scaleFactor, 10 * scaleFactor, 8 * scaleFactor, 8 * scaleFactor));
                this.setContentSize(cc.size(220 * scaleFactor, 50 * scaleFactor));
                this.setCascadeColorEnabled(false);
                this.setVisible(false);
                this.setAnchorPoint(0, 0.5);
                this.setPosition(cc.p(175 * scaleFactor, 485 * scaleFactor));
                this._isEnabled = true;

                var arrow = new cc.Sprite(spriteFrameCache.getSpriteFrame("arrowBorder.png"));
                arrow.setAnchorPoint(1,0.5);
                arrow.setPosition(cc.p(1.5 * scaleFactor, this.height / 2));
                this.addChild(arrow, 1);

                var label = new cc.LabelTTF("Cash prize could be different from announced prize", "RobotoRegular", 22 * scaleFactor);
                label.setScale(0.6);
                label.setAnchorPoint(0,0.5);
                label.setColor(cc.color(0, 0, 0));
                label.setPosition(cc.p(10 * scaleFactor, this.height / 2));
                label._setBoundingWidth(this.width + this.width * .55);
                this.addChild(label, 1);

            }
        });

        this.prizeToolTip = new cc.Sprite(spriteFrameCache.getSpriteFrame("toolTipButton.png"));
        this.prizeToolTip.setPosition(cc.p(158 * scaleFactor, 485 * scaleFactor));
        this.prizeToolTip._isEnabled = true;
        this.prizeToolTip.cashToolTip = new CashToolTip();
        this.addChild(this.prizeToolTip.cashToolTip, 5);
        this.prizeToolTip.hover = function (bool) {
            this.cashToolTip.setVisible(bool);
        };
        this.addChild(this.prizeToolTip, 1);

        this._expectedPrize();
       // this._tournamentStatus();
        this._registrationStartStatus();
        this._registrationCloseStatus();
        this._estimatedDuration();
        this._tournamentStart();
        this._timer();
        this._instructions();
        this._drawLines(4);
    }
});