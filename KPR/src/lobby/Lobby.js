/**
 * Created by stpl on 9/16/2016.
 */
var Lobby = cc.LayerColor.extend({
    _name: "LobbyLayer",
    _isEnable: null,
    filterParser: null,
    // lobbyEventDispatcher: null,
    selectedGameType: null,
    _marquee: null,
    _roomId: null,
    _alreadyJoinRooms: null,
    newsArray: null,
    marqueeIndex: null,
    lobbyLoader: null,

    ctor: function () {
        this._super(cc.color(0, 0, 0));
        this._isEnable = true;
        this._roomId = 0;
        this.controlAndDisplayUpperTabHeight = 25 * scaleFactor;
        this.userInfoStrip = cc.size(480 * scaleFactor, 80 * scaleFactor);
        this.buttomTabHeight = 28 * scaleFactor;
        this.upperTabHeight = 25 * scaleFactor;
        this.profileStripHeight = 80 * scaleFactor;
        this.tabSpace = 0;
        this.newsArray = [];
        this.marqueeIndex = 0;

        var upperStrip = new cc.DrawNode();
        upperStrip.drawRect(cc.p(0, this.controlAndDisplayUpperTabHeight), cc.p(cc.winSize.width, 0), cc.color(0, 0, 0), 0, cc.color(0, 0, 0));
        upperStrip.setPosition(cc.p(0, cc.winSize.height - this.controlAndDisplayUpperTabHeight));
        this.addChild(upperStrip, 0);

        var bottomStrip = new cc.DrawNode();
        bottomStrip.drawRect(cc.p(0, this.buttomTabHeight), cc.p(cc.winSize.width + 1, 0), cc.color(0, 0, 0), 0, cc.color(0, 0, 0));
        bottomStrip.setPosition(cc.p(0, 0));
        this.addChild(bottomStrip, 1);

        this.mainTabfontSize = 16 * scaleFactor;
        this.profileFontSize = 14 * scaleFactor;
        this.fontFamily = "RobotoRegular";
        this.mainTabfontFamily = "RobotoBold";

        var profileStrip = new cc.DrawNode();
        profileStrip.drawRect(cc.p(0, this.profileStripHeight), cc.p(cc.winSize.width, 0), cc.color(20, 31, 37), 0, cc.color(0, 0, 0));
        profileStrip.setPosition(cc.p(0, this.height - this.upperTabHeight - this.profileStripHeight));
        this.addChild(profileStrip);

        this.lobbySettingButton = new CreateBtn("settingButton", "settingButton", "lobbySettingButton");
        this.lobbySettingButton.setPosition(cc.p(this.width - 15, this.height - 12));
        this.addChild(this.lobbySettingButton, 10);

        this.inboxTab = new MenuLayout("Inbox", 25, 0, 12, "RobotoBold");
        this.inboxTab.setPosition(130 * scaleFactor, cc.winSize.height - 25);
        this.addChild(this.inboxTab, 10);

        this.addCashTab = new CreateBtn("addCash", "addCashOver", "AddCash", cc.p(cc.winSize.width - 180, cc.winSize.height - 12));
        this.addChild(this.addCashTab, 10);

        this.myTableTab = new CreateBtn("myTables", "myTablesHover", "myTablesButton");
        this.myTableTab.setPosition(cc.p(this.width - this.myTableTab.width / 2 - this.lobbySettingButton.width - 3, this.height - this.myTableTab.height / 2));
        this.addChild(this.myTableTab, 10);

        this.lobbySettingPanel = new Settings("Lobby", this);
        this.addChild(this.lobbySettingPanel, GameConstants.settingZorder);
        this.lobbySettingPanel.setVisible(false);

        var khelLogo = new cc.Sprite(res.lobby.logo);
        khelLogo.setScale(0.5);
        khelLogo.setPosition(cc.p(80 * scaleFactor, this.height - this.userInfoStrip.height / 2 - this.controlAndDisplayUpperTabHeight));
        this.addChild(khelLogo, 10, "Logo");

        this.profileFrame = new Profile(cc.p(180 * scaleFactor, this.height - this.userInfoStrip.height / 2 - this.controlAndDisplayUpperTabHeight));
        this.addChild(this.profileFrame);

        this.userName = new cc.LabelTTF("", "RobotoRegular", this.profileFontSize * 2);
        this.userName.setScale(0.5);
        this.userName.setAnchorPoint(0, 0.5);
        this.userName._setBoundingWidth(210 * scaleFactor);
        this.userName.setPosition(cc.p(210 * scaleFactor, this.height - this.userInfoStrip.height / 2 - this.controlAndDisplayUpperTabHeight));
        this.addChild(this.userName, 15);

        var realChips = new cc.LabelTTF("Real Chips:", "RobotoRegular", this.profileFontSize * 2);
        realChips.setScale(0.5);
        realChips.setAnchorPoint(0, 0.5);
        realChips.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
        realChips.setPosition(cc.p(320, this.height - this.userInfoStrip.height / 2 - this.controlAndDisplayUpperTabHeight + 10));
        this.addChild(realChips, 15);

        this.realChipsvalue = new cc.LabelTTF("0", "RobotoRegular", this.profileFontSize * 2);
        this.realChipsvalue.setScale(0.5);
        this.realChipsvalue.setAnchorPoint(0, 0.5);
        this.realChipsvalue.setPosition(cc.p(325 + realChips.width / 2, this.height - this.userInfoStrip.height / 2 - this.controlAndDisplayUpperTabHeight + 10));
        this.addChild(this.realChipsvalue, 15);

        this.realRefreshButton = new RefreshChipButton("RealRefreshButton");
        this.realRefreshButton.setPosition(cc.p(this.realChipsvalue.x + this.realChipsvalue.width / 2 + 10, this.height - this.userInfoStrip.height / 2 - this.controlAndDisplayUpperTabHeight + 10));
        this.addChild(this.realRefreshButton, 15);

        var practiceChips = new cc.LabelTTF("Practice Chips:", "RobotoRegular", this.profileFontSize * 2);
        practiceChips.setScale(0.5);
        practiceChips.setAnchorPoint(0, 0.5);
        practiceChips.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
        practiceChips.setPosition(cc.p(320, this.height - this.userInfoStrip.height / 2 - this.controlAndDisplayUpperTabHeight - 10));
        this.addChild(practiceChips, 15);

        this.practiceChipsvalue = new cc.LabelTTF("0", "RobotoRegular", this.profileFontSize * 2);
        this.practiceChipsvalue.setScale(0.5);
        this.practiceChipsvalue.setAnchorPoint(0, 0.5);
        this.practiceChipsvalue.setPosition(cc.p(325 + practiceChips.width / 2, this.height - this.userInfoStrip.height / 2 - this.controlAndDisplayUpperTabHeight - 10));
        this.addChild(this.practiceChipsvalue, 15);

        this.practiceRefreshButton = new RefreshChipButton("PracticeRefreshButton");
        this.practiceRefreshButton.setPosition(cc.p(this.practiceChipsvalue.x + this.practiceChipsvalue.width / 2 + 10, this.height - this.userInfoStrip.height / 2 - this.controlAndDisplayUpperTabHeight - 10));
        this.addChild(this.practiceRefreshButton, 15);

        this.banner = new Banner(this);
        this.banner.setPosition(cc.p(this.width - this.banner.width, this.height - this.userInfoStrip.height - this.controlAndDisplayUpperTabHeight));
        this.addChild(this.banner, 20, 'lobbyHeaderBanner');

        this.cashGameTab = this.getGameTypeTab("CASH GAME", this.mainTabfontFamily, this.mainTabfontSize, "CASH");
        this.cashGameTab.setPosition(cc.p(0, this.height - this.upperTabHeight - this.profileStripHeight));
        this.addChild(this.cashGameTab, 15);

        this.tournamentTab = this.getGameTypeTab("TOURNAMENTS", this.mainTabfontFamily, this.mainTabfontSize);
        this.tournamentTab.setPosition(cc.p(this.cashGameTab.width + this.tabSpace, this.height - this.upperTabHeight - this.profileStripHeight));
        this.addChild(this.tournamentTab, 15);

        var newAnimation = cc.Sprite.extend({
            status: true,
            ctor: function (context) {
                this._super(spriteFrameCache.getSpriteFrame("newR.png"));

                var pos = cc.p(context.tournamentTab.getPosition());
                this.setPosition(cc.p(pos.x + this.width / 2, pos.y - this.height / 2));
                this.flare();
            },
            flare: function () {
                setInterval(function () {
                    if (this.status == true)
                        this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("newY.png"));
                    else {
                        this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("newR.png"));
                    }
                    this.status = !this.status;
                }.bind(this), 300);
            }
        });

        this.addChild(new newAnimation(this), 20);

        this.practiceTab = this.getGameTypeTab("PRACTICE GAME", this.mainTabfontFamily, this.mainTabfontSize, "PROMO");
        this.practiceTab.setPosition(cc.p(this.tournamentTab.width + this.cashGameTab.width + this.tabSpace * 2, this.height - this.upperTabHeight - this.profileStripHeight));
        this.addChild(this.practiceTab, 15);

        this.findGameTab = this.getGameTypeTab("FIND GAME", this.mainTabfontFamily, this.mainTabfontSize);
        this.findGameTab.setPosition(cc.p(this.practiceTab.width + this.tournamentTab.width + this.cashGameTab.width + this.tabSpace * 3, this.height - this.upperTabHeight - this.profileStripHeight));
        this.addChild(this.findGameTab, 15);

        this.gameInfoLabel = new cc.LabelTTF("", this.mainTabfontFamily, (this.mainTabfontSize + 2) * 1.4);
        this.gameInfoLabel.setScale(0.7);
        this.gameInfoLabel.setAnchorPoint(0, 0.5);

        this.gameInfo = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("gameTypeTabs.png"), ccui.Widget.PLIST_TEXTURE);
        this.gameInfo.setCascadeColorEnabled(false);
        this.gameInfo.setCapInsets(cc.rect(63, 16, 63, 16));
        this.gameInfo.setContentSize(cc.size(302 * scaleFactor, 40 * scaleFactor));
        this.gameInfo.setAnchorPoint(0, 1);
        this.gameInfo.addChild(this.gameInfoLabel, 10);
        this.gameInfo.setPosition(cc.p(this.findGameTab.width + this.practiceTab.width + this.tournamentTab.width + this.cashGameTab.width + this.tabSpace * 4, this.height - this.upperTabHeight - this.profileStripHeight));
        this.gameInfoLabel.setPosition(cc.p(78 * scaleFactor, this.gameInfo.height / 2));
        this.addChild(this.gameInfo, 20);

        this._alreadyJoinRooms = [];

        this.filterParser = new FilterParser(this);
        this.filterParser.setPosition(cc.p(0, this.buttomTabHeight * 2));
        this.addChild(this.filterParser, 1);


        this.tournamentLobby = new TournamentLobby(this);
        this.tournamentLobby.setPosition(cc.p(0, this.buttomTabHeight * 2));
        this.addChild(this.tournamentLobby, 1);

        this.initLobbyLoader(true);

        var box1 = new cc.DrawNode();
        box1.drawRect(cc.p(this.width + 1 - this.filterParser.miniRoom.width, 50), cc.p(this.width - 1, 0), cc.color(0, 0, 0), 0, cc.color(0, 0, 0));
        box1.setPosition(cc.p(1 * scaleFactor, 155 * scaleFactor));
        this.addChild(box1, 8);

        var box2 = new cc.DrawNode();
        box2.drawRect(cc.p(this.width + 1 - this.filterParser.miniRoom.width, 0), cc.p(this.width - 1, 50), cc.color(0, 0, 0), 0, cc.color(0, 0, 0));
        box2.setPosition(cc.p(1 * scaleFactor, 55 * scaleFactor));
        this.addChild(box2, 8);

        var separatorY = 195 * scaleFactor;
        var separator = new cc.DrawNode();//origin, destination, fillColor, lineWidth, lineColor
        separator.drawRect(cc.p(this.width + 1 - this.filterParser.miniRoom.width, separatorY), cc.p(this.width, separatorY), null, 1, cc.color(50, 50, 50));
        this.addChild(separator, 8);

        var newsNfeatures = new cc.LabelTTF("News & Features", "RobotoMedium", 25);
        newsNfeatures.setScale(0.8);
        newsNfeatures.setAnchorPoint(0, 0.5);
        newsNfeatures.setPosition(cc.p(20 + this.width + 1 - this.filterParser.miniRoom.width, 170 * scaleFactor));
        this.addChild(newsNfeatures, 20);

        this.initGameTypeTab(this.cashGameTab);

        // this.lobbyEventDispatcher = new LobbyEventDispatcher(this);

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                cc.log("Blank listener in " + this._name);
                applicationFacade._controlAndDisplay.closeAllTab();
                this.closeAllTab();
                return true;
            }.bind(this)
        }, this);

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();
        cc.eventManager.addListener(this.lobbyTouchListener.clone(), khelLogo);

        this.myTablePanel = new MyTables(this);
        this.addChild(this.myTablePanel, GameConstants.settingZorder, "myTablePanel");

        var marqueeStrip = new cc.DrawNode();
        marqueeStrip.drawRect(cc.p(0, this.buttomTabHeight), cc.p(cc.winSize.width + 2, 0), cc.color(18, 18, 18), 0, cc.color(18, 18, 18));
        marqueeStrip.setPosition(cc.p(-1 * scaleFactor, this.buttomTabHeight));
        this.addChild(marqueeStrip, 0, "marqueeStrip");

        return true;
    },
    closeAllTab: function () {
        this.lobbySettingPanel.setVisible(false);
        this.myTablePanel.setVisible(false);
        this.myTableTab.hover(false);
    },
    initLobby: function () {
        this.userName.setString(rummyData.userName);
        this.loadPlayerImage();
        this.initNewsAndFeatures();
        this.initMarquee();
        this.banner.initBanner();

        if (this.newsArray.length > 0)
            this.showNews();
        if (this.marqueeArray.length > 0)
            this.initNextMarquee();
    },
    initNewsAndFeatures: function () {
        this.newsArray = [];
        var news;
        for (var i = 0; i < rummyData.newsGroup.length; i++) {
            news = new News(rummyData.newsGroup[i], this);
            cc.eventManager.addListener(this.lobbyTouchListener.clone(), news);
            this.newsArray.push(news);
        }
    },
    showNews: function () {
        var index = 0;
        this.schedule(function () {
            if (index > 0)
                this.newsArray[index - 1].fly();

            if (index < this.newsArray.length)
                this.newsArray[index].appear(this);

            if (index == this.newsArray.length) {
                index = 0;
                this.newsArray[index].appear(this);
            }
            index++;
        }, 5, cc.REPEAT_FOREVER, 1, "news");
    },
    initMarquee: function () {
        this.marqueeArray = [];
        var marquee;
        this.marqueeNode = new cc.Node();
        this.addChild(this.marqueeNode, 20);
        var position = 0, separatorDist = 20;
        for (var i = 0; i < rummyData.marqueGroup.length; i++) {
            marquee = new Marquee2(rummyData.marqueGroup[i], this);
            marquee.setPositionX(position);
            position += separatorDist + marquee.width / 2;
            this.marqueeNode.addChild(marquee, 1);
            cc.eventManager.addListener(this.lobbyMouseListener.clone(), marquee);
            cc.eventManager.addListener(this.lobbyTouchListener.clone(), marquee);
            this.marqueeArray.push(marquee);
        }
        this.marqueeNode._marqueeWidth = position;
        this.marqueeNode.setPosition(cc.p(this.width, 42 - yMargin));
    },
    initNextMarquee: function () {
        this.marqueeNode.setPositionX(this.width);
        var speed = 100;
        var move2 = cc.moveTo(40, cc.p(-this.marqueeNode._marqueeWidth, this.buttomTabHeight + this.buttomTabHeight / 2));
        this.marqueeNode.runAction(cc.sequence(move2, cc.callFunc(function () {
            this.marqueeNode.setPositionX(this.width);
        }, this)).repeatForever());
    },
    pauseMarquee: function (bool) {
        /*if (bool) {
         for (var i = 0; i < this.marqueeArray.length; i++) {
         cc.director.getActionManager().pauseTarget(this.marqueeArray[i]);
         }
         } else {
         for (var j = 0; j < this.marqueeArray.length; j++) {
         cc.director.getActionManager().resumeTarget(this.marqueeArray[j]);
         }
         }*/

        if (bool) {
            cc.director.getActionManager().pauseTarget(this.marqueeNode);
        } else {
            cc.director.getActionManager().resumeTarget(this.marqueeNode);
        }
    },
    initLobbyLoader: function (bool) {
        if (bool) {
            this.lobbyLoader = new LobbyLoader();
            this.addChild(this.lobbyLoader, 50, "lobbyLoader");
        } else {
            this.lobbyLoader && this.lobbyLoader.removeFromParent(true);
        }
    },
    getGameTypeTab: function (tabName, fontFamily, fontSize, paramName) {

        var tab = cc.Sprite.extend({
            _hovering: null,
            ctor: function (tabName, fontFamily, fontSize) {
                this._super(spriteFrameCache.getSpriteFrame("gameTypeTabs.png"));
                this._hovering = false;
                if (paramName != undefined) {
                    this.setName(paramName);
                } else {
                    this.setName(tabName.replace(/ /g, ""));
                }

                this.setAnchorPoint(0, 1);

                var label = new cc.LabelTTF(tabName, fontFamily, fontSize * 2);
                label.setPosition(cc.p(this.width / 2, this.height / 2 - yMargin));
                label.setScale(0.5);
                this.addChild(label, 1);
            },
            hover: function (bool) {
                if (bool) {
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("gameTypeTabsSelected.png"));
                    // cc._canvas.style.cursor = "pointer";
                } else {
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("gameTypeTabs.png"));
                    // cc._canvas.style.cursor = "default";
                }
                this.setAnchorPoint(0, 1);
            }
        });

        return new tab(tabName, fontFamily, fontSize);
    },
    loadPlayerImage: function () {
        var imagePath = rummyData.path.avatarImage;
        if (imagePath && imagePath != "")
            this.profileFrame.setProfilePic(imagePath);
    },
    initMouseListener: function () {
        this.lobbyMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            swallowTouches: true,
            onMouseMove: function (event) {
                if (!this._isEnable)
                    return;
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    !target._hovering && target.hover(true);
                    target._hovering = true;
                    return true;
                } else {
                    if (target.getTag() == 222) {
                        target._hovering && target.hover(false);
                        target._hovering = false;
                        return false;
                    }
                    if (this.selectedGameType != target) {
                        target._hovering && target.hover(false);
                        target._hovering = false;
                        return false;
                    }
                    return false;
                }
            }.bind(this)
        });
        this.lobbyMouseListener.retain();
        cc.eventManager.addListener(this.lobbyMouseListener.clone(), this.cashGameTab);
        cc.eventManager.addListener(this.lobbyMouseListener.clone(), this.tournamentTab);
        cc.eventManager.addListener(this.lobbyMouseListener.clone(), this.practiceTab);
        cc.eventManager.addListener(this.lobbyMouseListener.clone(), this.findGameTab);
        cc.eventManager.addListener(this.lobbyMouseListener.clone(), this.realRefreshButton);
        cc.eventManager.addListener(this.lobbyMouseListener.clone(), this.practiceRefreshButton);
        cc.eventManager.addListener(this.lobbyMouseListener.clone(), this.lobbySettingButton);
        cc.eventManager.addListener(this.lobbyMouseListener.clone(), this.addCashTab);
    },
    initTouchListener: function () {
        this.lobbyTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode) && applicationFacade._activeTableId == 0) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                var targetName = target._name.split("_");
                applicationFacade._controlAndDisplay.closeAllTab();
                switch (targetName[0]) {
                    case "News":
                        target.clickHandler();
                        this.closeAllTab();
                        break;
                    case "Marquee":
                        this.closeAllTab();
                        target.clickHandler();
                        break;
                    case "RealRefreshButton":
                        this.closeAllTab();
                        this.realRefreshButtonListener();
                        break;
                    case "PracticeRefreshButton":
                        this.closeAllTab();
                        this.practiceRefreshButtonListener();
                        break;
                    case "lobbySettingButton":
                        this.lobbySettingBtnHandler();
                        break;
                    case "Inbox":
                        this.closeAllTab();
                        cc.sys.openURL(applicationFacade.baseUrl + "/inbox");
                        break;
                    case "myTablesButton":
                        this.myTablesBtnHandler();
                        break;
                    case "MyTable":
                        this.myTablesBtnHandler();
                        applicationFacade._controlAndDisplay.switchScreen(target.tableId);
                        break;
                    case "Logo":
                        this.closeAllTab();
                        cc.sys.openURL(applicationFacade.baseUrl);
                        break;
                    case "AddCash":
                        this.closeAllTab();
                        this.cashBtnHandler();
                        break;
                    case "lobbyHeaderBanner":
                        this.closeAllTab();
                        target.clickHandler();
                        break;
                    case "bannerImg":
                        this.closeAllTab();
                        this.banner.clickHandler();
                        break;
                    default :
                        this.closeAllTab();
                        this.initGameTypeTab(target);
                        return true;
                }
                return true;
            }.bind(this)
        });
        this.lobbyTouchListener.retain();
        cc.eventManager.addListener(this.lobbyTouchListener.clone(), this.cashGameTab);
        cc.eventManager.addListener(this.lobbyTouchListener.clone(), this.tournamentTab);
        cc.eventManager.addListener(this.lobbyTouchListener.clone(), this.inboxTab);
        cc.eventManager.addListener(this.lobbyTouchListener.clone(), this.practiceTab);
        cc.eventManager.addListener(this.lobbyTouchListener.clone(), this.findGameTab);
        cc.eventManager.addListener(this.lobbyTouchListener.clone(), this.realRefreshButton);
        cc.eventManager.addListener(this.lobbyTouchListener.clone(), this.practiceRefreshButton);
        cc.eventManager.addListener(this.lobbyTouchListener.clone(), this.lobbySettingButton);
        cc.eventManager.addListener(this.lobbyTouchListener.clone(), this.myTableTab);
        cc.eventManager.addListener(this.lobbyTouchListener.clone(), this.addCashTab);
        // cc.eventManager.addListener(this.lobbyTouchListener.clone(), this.gameInfo);
    },
    cashBtnHandler: function () {
        var reqStr;
        /*reqStr = rummyData.path.paymentPage;
         cc.sys.openURL(reqStr);*/
        applicationFacade.updateExternalLobbyBalance();
        window.parent.postMessage("openCashPopup", "*");

        // if(applicationFacade.isDeskTop){
        //     var source;
        //     source = "http://" + rummyData.domainName + "/portal/rummy-app-deposit?token=" + rummyData.token + "&responseType=downloadClient";
        // }
    },
    lobbySettingBtnHandler: function () {
        this.lobbySettingPanel.setVisible(!this.lobbySettingPanel.isVisible());
        this.myTableTab.hover(false);
        this.myTablePanel.setVisible(false);
    },
    myTablesBtnHandler: function () {
        this.lobbySettingPanel.setVisible(false);
        this.myTablePanel.setVisible(!this.myTablePanel.isVisible());
        this.myTableTab.hover(this.myTablePanel.isVisible());
        this.myTablePanel.isVisible() && this.myTablePanel.initMyTablePanel();
    },
    realRefreshButtonListener: function () {
        var obj = {};
        obj[SFSConstants.FLD_TYPE] = SFSConstants.GAMEVARIANT_CASH;
        sfsClient.sendReqFromLobby(SFSConstants.CMD_GET_BAL, obj);
    },
    practiceRefreshButtonListener: function () {
        var obj = {};
        obj[SFSConstants.FLD_TYPE] = SFSConstants.GAMEVARIANT_PROMO;
        sfsClient.sendReqFromLobby(SFSConstants.CMD_GET_BAL, obj);
    },
    initGameTypeTab: function (target, bool) {
        this.selectedGameType && this.selectedGameType.hover(false);
        if (this.selectedGameType && this.selectedGameType == target) {
            target.hover(true);
            return;
        }
        switch (target._name) {
            case "CASH":
                this.selectedGameType = this.cashGameTab;
                break;
            case "TOURNAMENTS":
                this.selectedGameType = this.tournamentTab;
                if (sfsClient._tournamentRoomArr.length == 0) {
                    sfs && sfsClient.sendTournamentSubscribeReq();
                }
                break;
            case "PROMO":
                this.selectedGameType = this.practiceTab;
                break;
            case "FINDGAME":
                this.selectedGameType = this.findGameTab;
                break;
        }
        this.selectedGameType.hover(true);
        this.initSubGameTypeTab(this.selectedGameType);

    },
    initSubGameTypeTab: function (selectedGameType) {

        this.filterParser.rangeSlider.setBarPercentage({min: 0, max: 100});
        if (selectedGameType._name == "TOURNAMENTS") {
            if (sfsClient._tournamentRoomArr.length != 0) {
                this.filterParser.findGameDrawNode && this.filterParser.findGameDrawNode.removeFromParent(true);

                this.tournamentLobby.pauseListener(false);
                this.filterParser.pauseListener(true);

                this.tournamentLobby.createTournamentTab();

                this.tournamentLobby.setLocalZOrder(3);
                this.filterParser.setLocalZOrder(1);
            }
        } else if (selectedGameType._name == "FINDGAME") {
            this.tournamentLobby.tourRoomListManager.listView.removeAllChildrenWithCleanup(true);

            this.tournamentLobby.pauseListener(true);
            this.filterParser.pauseListener(false);

            this.filterParser.rangeSlider.setBarPercentage({min: 0, max: 100});

            this.filterParser.createFindGameTab();
            this.filterParser.initFindGameTabProp();

            this.filterParser.subGameTypeListener(this.filterParser.selectedFindGameType);

            this.filterParser.setLocalZOrder(3);
            this.tournamentLobby.setLocalZOrder(1);

        } else {
            this.filterParser.rangeSlider.setBarPercentage({min: 0, max: 100});
            this.tournamentLobby.tourRoomListManager.listView.removeAllChildrenWithCleanup(true);

            this.tournamentLobby.pauseListener(true);
            this.filterParser.pauseListener(false);

            if (!this.filterParser.poolRummyTab) {
                this.filterParser.createPracticeAndCashSubGameTab();
                this.filterParser.initCashPromoTabProp();
            } else {
                this.filterParser.selectedSubGameType.hover(false);
                this.filterParser.selectedSubGameType = this.filterParser.poolRummyTab;
            }
            this.filterParser.subGameTypeListener(this.filterParser.selectedSubGameType);
            this.filterParser.setLocalZOrder(3);
            this.tournamentLobby.setLocalZOrder(1);
        }
    },
    handleResp: function (cmd, room, params) {
        // this.lobbyEventDispatcher.dispatchSFSEvents(cmd, room, params);
        var str;
        switch (cmd) {
            case SFSConstants.CMD_UPDATE_BAL:  // not used. lobby
                this.updateLobbyBal(params);
                break;
            case SFSConstants.CMD_LOGRESP:  // not used. lobby
                // startGame(params);
                break;
            case SFSConstants.CMD_ROOMINFO:
                this.processRoomInfo(params);
                break;
            case SFSConstants.CMD_ROOMPLSUPDATE :
                this.updateRoomSeated(params);
                break;
            case SFSConstants.CMD_ROOM_GROUP_STR :
                str = params[SFSConstants.STR];
                this.updateTable(str, params["tableId"]);
                break;
            case SFSConstants.CMD_GROUP_TOT_PLS :
                str = params[SFSConstants.STR];
                this.updateGroupTotalPlayers(str);
                break;
            case SFSConstants.CMD_NO_ROOM:
                applicationFacade.noRoomHandler(params);
                break;
            case SFSConstants.CMD_roomPrize :
                this.updateRoomPrize(params);
                break;
        }

    },
    updateLobbyBal: function (data) {

        // update weaver login balance
        rummyData.promoBalance = data[SFSConstants.GAMEIANT_PROMO];
        rummyData.cashBalance = data[SFSConstants.GAMEIANT_CASH];

        this.realChipsvalue.setString(rummyData.cashBalance);
        this.practiceChipsvalue.setString(rummyData.promoBalance);

        var refreshY = this.height - this.userInfoStrip.height / 2 - this.controlAndDisplayUpperTabHeight + 10;
        if (cc.sys.browserType == "firefox")
            refreshY = this.height - this.userInfoStrip.height / 2 - this.controlAndDisplayUpperTabHeight + 12;

        this.realRefreshButton.setPosition(cc.p(this.realChipsvalue.x + this.realChipsvalue.width / 2 + 10, refreshY));
        this.practiceRefreshButton.setPosition(cc.p(this.practiceChipsvalue.x + this.practiceChipsvalue.width / 2 + 10, refreshY - 20));

        // this.checkbalAmount(rummyData.promoBalance, rummyData.cashBalance);
    },
    checkbalAmount: function (promoBalance, cashBalance) {
        if (promoBalance >= 10000) {
            this.practiceRefreshButton.setVisible(false);
        } else {
            this.practiceRefreshButton.setVisible(true);
        }
        if (cashBalance >= 10000) {
            this.realRefreshButton.setVisible(false);
        } else {
            this.realRefreshButton.setVisible(true);
        }
    },
    refreshLobby: function () {
        if (this.selectedGameType == this.tournamentTab) {
            if (sfsClient._tournamentRoomArr.length != 0) {
                sfsClient.sendTournamentSubscribeReq();
            }
        }
    },
    processRoomInfo: function (params) {
        cc.log("call 1");
        var totalplsStr = params[SFSConstants.DATA_STR].split(SFSConstants.SEMICOLON_DELIMITER);
        cc.log("roomInfo waitinglist  " + totalplsStr);
        var totalPls = {};
        for (var s = 0; s < totalplsStr.length; s++) {
            var s1 = totalplsStr[s].split(SFSConstants.COLON_DELIMITER);
            var s2 = s1[1].split(SFSConstants.COMMA_DELIMITER);
            var info = new this.RoomInfoModel();
            info.setTableId(s2[0]);
            info.setNoOfPlayers(s2[1]);
            totalPls[s1[0]] = info;
        }
        this.initLobbyLoader(false);
        this.handleTotalPlayer(totalPls);

        cc.log("lobby alreadyjoined room: " + params["alreadyJoinRoomStr"]);

        if (params["alreadyJoinRoomStr"]) {
            var rmStr = params["alreadyJoinRoomStr"];
            if (rmStr != "-1" && rmStr != "undefined") {
                this._alreadyJoinRooms = rmStr.split(",");
                applicationFacade._tempJoinRooms = rmStr.split(",");
            }
            else {
                this._alreadyJoinRooms = [];
                applicationFacade._tempJoinRooms = [];
            }
        }
        this.refreshLobby();
        sfs.send(new SFS2X.Requests.System.SubscribeRoomGroupRequest("gameRoom"));
        //todo applicationFacade._controlAndDisplay.connectMsgInvisible();

        /*  if ((roomId.indexOf(SFSConstants.NO_ROOMJOINED) >= 0)) {
         if (cc.director.getRunningScene() == "gameTable") {
         if (this.mSeatedPLayers.length != 0) {
         this.mSeatedPLayers = [];
         }
         this.SFS_RoundNo = -1;
         this.SFS_RoomID = -1;
         cc.director.runScene(new LobbyScene());
         }
         } else {
         var tableIds = roomId.split(SFSConstants.COMMA_DELIMITER);
         //TODO add tables in my tables

         if (sfs.isConnected()) {
         var isJoined = false;
         for (var i = 0; i < tableIds.length; i++) {
         if (tableIds[i] == (this.SFS_RoomID + "")) {
         isJoined = true;
         this.joinRoom(this.SFS_RoomID);
         break;
         }
         }

         if (!isJoined && cc.director.getRunningScene() == "gameTable") {
         this.SFS_RoundNo = -1;
         this.SFS_RoomID = -1;
         cc.director.runScene(new LobbyScene());
         }
         }

         }*/
    },
    handleTotalPlayer: function (totalplslist) {
        this.filterParser.handleTotalPlayer(totalplslist);
    },
    RoomInfoModel: function () {
        var tableId;
        var noOfPlayers;
        this.getTableId = function () {
            return tableId;
        };
        this.setTableId = function (tableid) {
            tableId = tableid;
        };
        this.getNoOfPlayers = function () {
            return noOfPlayers;
        };
        this.setNoOfPlayers = function (noofplayers) {
            noOfPlayers = noofplayers;
        };
    },
    updateRoomSeated: function (params) {
        this.filterParser.updateRoomSeated(params);
    },
    updateTable: function (str, tableId) {
        this.filterParser.updateTable(str, tableId);
    },
    updateGroupTotalPlayers: function (str) {
        this.filterParser.updateGroupTotalPlayers(str);
    },
    updateRoomPrize: function (dataObj) {
        this.filterParser.updateRoomPrize(dataObj);
    },
    showMiniRoom: function (sender, param1, param2) {
        if (this.tournamentLobby && this.tournamentLobby.tourMiniRoom)
            this.tournamentLobby.tourMiniRoom.setVisible(false);
        this.filterParser.miniRoom.initMiniRoom(sender);
        this.filterParser.miniRoom.setVisible(true);
    },
    onRoomListUpdate: function (room) {
        this.filterParser.onRoomListUpdate(room);
    },
    pauseListener: function (bool) {
        if (bool) {
            cc.eventManager.pauseTarget(this);
        } else {
            cc.eventManager.resumeTarget(this);
        }
        this._isEnable = !bool;
        this.filterParser.pauseListener(bool);
        this.tournamentLobby.pauseListener(bool);
    },
    joinRoomHandler: function (r) {
        var uid = sfs.mySelf.id;
        var room = sfsClient.getRoomById(r.getInt(SFSConstants.SFS_ROOM_ID));
        var dataObj = {
            cId: parseInt(r.getInt(SFSConstants.COMMON_ID))
        };
        if (room) {
            this.joinRoom(room);
        }
        else {
            sfsClient.sendReqFromLobby(SFSConstants.CMD_ROOM_JOIN_REQUEST, dataObj);
        }
    },
    joinRoom: function (room) {
        var uid = sfs.mySelf.id;
        var u = room.getUserById(uid);
        var dataObj = {
            cmd: "RoomJoin",
            id: room.id
        };

        if (!u)
            applicationFacade.lobbyCmdHandler(dataObj);
        else
            applicationFacade._controlAndDisplay.joinRoomHandler(room.id);
    },
    sendJoinRoomRequestForNextRoom: function (rmId) {
        if (rmId == undefined)
            rmId = -1;

        cc.log("sendJoinRoomRequestForNextRoom _alreadyJoinRooms", rmId, this._alreadyJoinRooms);
        if (this._alreadyJoinRooms.length == 0)
            return;
        if (this._alreadyJoinRooms.length == 1) {
            applicationFacade.updateThumbnail();
        }
        var room;

        var index = this._alreadyJoinRooms.indexOf((rmId + ""));
        if (index == -1)
            room = sfsClient.getRoomById(parseInt(this._alreadyJoinRooms[0]));
        else {
            this._alreadyJoinRooms.splice(index, 1);
            if (this._alreadyJoinRooms.length > 0)
                room = sfsClient.getRoomById(parseInt(this._alreadyJoinRooms[0]));
        }
        var dataObj = {rId: parseInt(this._alreadyJoinRooms[0])};
        if (room) {
            this.joinRoom(room);
        }
        else {
            if (parseInt(this._alreadyJoinRooms[0]) > 0)
                sfsClient.sendReqFromLobby(SFSConstants.CMD_ROOM_JOIN_REQUEST, dataObj);
        }
    },
    // tournament
    handleTournamentList: function (tournamentList,isAddRoom) {
        this.tournamentLobby && this.tournamentLobby.handleTournamentList(tournamentList, isAddRoom);
        cc.log("Tournament room");
    }
}), RefreshChipButton = cc.Sprite.extend({
    _hovering: null,
    ctor: function (spriteName) {
        this._super(spriteFrameCache.getSpriteFrame("refresh.png"));
        this.setName(spriteName);
        this.opacity = 200;
        this._hovering = false;

        return true;
    },
    hover: function (bool) {
        bool ? this.opacity = 255 : this.opacity = 200;
    }
}), Banner = ccui.Layout.extend({
    _lobby: null,
    ctor: function (lobby) {
        this._super();
        this._lobby = lobby;
        this.setContentSize(cc.size(520 * scaleFactor, 79 * scaleFactor));
        this.setClippingEnabled(true);
        this.setName("lobbyHeaderBanner");
        return true;
    },
    initBanner: function () {
        if (rummyData['rummyPlaceHolders'] && rummyData['rummyPlaceHolders']['lobbyHeader'] && rummyData['rummyPlaceHolders']['lobbyHeader']['placeHolder']) {
            this.data = rummyData['rummyPlaceHolders']['lobbyHeader']['placeHolder'];
            if (this.data['path']) {
                cc.textureCache.addImageAsync(this.data['path'], function () {
                    var bannerSprite = new cc.Sprite(this.data['path']);
                    bannerSprite.setAnchorPoint(1, 1);
                    bannerSprite.setPosition(cc.p(this.width, this.height));
                    var sf = {
                        x: this.width / bannerSprite.width,
                        y: this.height / bannerSprite.height
                    };
                    bannerSprite.scaleX = sf.x;
                    bannerSprite.scaleY = sf.y;
                    this.addChild(bannerSprite, 2, 'bannerImg');
                    cc.eventManager.addListener(this._lobby.lobbyTouchListener.clone(), bannerSprite);
                }, this);
            }
        }
    },
    clickHandler: function () {
        this.data['url'] && cc.sys.openURL(this.data['url']);
    }
}), Profile = cc.Sprite.extend({
    ctor: function (pos) {
        this._super(spriteFrameCache.getSpriteFrame("profileFrameSmall.png"));
        this.setPosition(pos);
        this._initClippingNode();
    },
    _initClippingNode: function (spriteImage) {

        this.clippingNode = new ccui.Layout();
        this.clippingNode.setContentSize(cc.size(41 * scaleFactor, 41 * scaleFactor));
        this.clippingNode.setClippingEnabled(true);
        this.clippingNode.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.clippingNode.setBackGroundColor(cc.color(51, 55, 57));
        this.clippingNode.setPosition(cc.p(2 * scaleFactor, 2 * scaleFactor));
        this.addChild(this.clippingNode, -2, "profilePic");

    },
    setProfilePic: function (imageUrl) {
        if (imageUrl && imageUrl != "") {
            if ((imageUrl.indexOf("edit-thumbnai.jpg") >= 0)) {
                this.removeProfilePic();
            } else {
                var image = new Image();
                image.onload = function () {
                    cc.log("Image successfully loaded");
                    // cc.textureCache.addImageAsync(imageUrl, this.imageLoaded, this);
                    this.imageLoaded(imageUrl, image.width, image.height);

                }.bind(this);
                image.src = imageUrl;
                // image.crossOrigin = "anonymous";
                image.onerror = function () {
                    this.removeProfilePic();
                    cc.log("Image url is invalid");
                }.bind(this);
            }
        } else {
            this.removeProfilePic();
        }
    },
    processMask: function (_file) {

        var originalTexture = _file;
        var maskTexture = spriteFrameCache.getSpriteFrame("userPIC.png");
        MaskImage.processMask(originalTexture, maskTexture, function (imageData) {

            var texture2d = new cc.Texture2D();
            texture2d.initWithElement(imageData);
            texture2d.handleLoadedTexture();
            this.avatar.initWithTexture(texture2d);

        }.bind(this));

    },
    imageLoaded: function (imagePath, width, height) {
        var spriteAvatar;
        if (imagePath) {
            spriteAvatar = new cc.Sprite(imagePath);
            spriteAvatar.width = width;
            spriteAvatar.height = height;
        } else {
            spriteAvatar = new cc.Sprite(spriteFrameCache.getSpriteFrame("seatedButtonUserPic.png"));
        }
        var scaleFactor = this.getAvatarScaleFactor(spriteAvatar);
        spriteAvatar.setScale(scaleFactor.x, scaleFactor.y);
        spriteAvatar.setPosition(cc.p(this.clippingNode.width / 2, this.clippingNode.height / 2));
        this.clippingNode.removeAllChildren(true);
        this.clippingNode.addChild(spriteAvatar, 2);
    },
    setMask: function (originalImage, maskImage) {
        var clipngNode = new cc.ClippingNode();
        clipngNode.stencil = maskImage;
        originalImage.setName("originalImage");
        clipngNode.addChild(originalImage);
        return clipngNode;
    },
    getAvatarScaleFactor: function (avatar) {
        var tmpSize = avatar.getContentSize();
        var mask = spriteFrameCache.getSpriteFrame("userPIC.png").getRect();
        return {
            x: mask.width / tmpSize.width,
            y: mask.height / tmpSize.height
        };
    },
    removeProfilePic: function () {
        /*var spriteFrame = spriteFrameCache.getSpriteFrame("joker.png");
         this.processMask(spriteFrame);*/
        this.imageLoaded();
    }
});