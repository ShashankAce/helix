/**
 * Created by stpl on 1/4/2017.
 */
"use strict";
var MyTournamentRoomList = ccui.Layout.extend({
    _name: "MyTournamentRoomList",
    stripListener: null,
    _selectedItem: null,
    _isSelected: null,
    tournamentRoomListManager: null,
    myTourArrLength: null,
    ctor: function (widthArray, myTournamentRoomArr, myTourArrLength, context) {
        this._super();
        this.tournamentRoomListManager = context;
        this.myTourArrLength = myTourArrLength;

        this.setClippingEnabled(true);
        this.setContentSize(760 * scaleFactor, 25 * scaleFactor);
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.setBackGroundColor(cc.color(224, 235, 242));

        this.initTouchListener();
        this.setTag(0);
        this.setMyTournamentHeader();

        this.joinedCount = new labelText('', "RobotoRegular", 26 * scaleFactor, cc.p(27 * scaleFactor, this.height / 2));
        this.addChild(this.joinedCount, 2);

        this.pipe = new cc.LabelTTF("|", "RobotoRegular", 26 * scaleFactor);
        this.pipe.setPosition(cc.p(98 * scaleFactor, this.height / 2));
        this.pipe.setColor(cc.color(0, 0, 0));
        this.pipe.setScale(0.8);
        this.addChild(this.pipe, 2);

        this.runningCount = new labelText('', "RobotoRegular", 26 * scaleFactor, cc.p(117 * scaleFactor, this.height / 2));
        this.addChild(this.runningCount, 2);

        this.joinedCount.setVisible(false);
        this.pipe.setVisible(false);
        this.runningCount.setVisible(false);

    },
    setMyTournamentHeaderDetail: function (bool) {

        if (bool) {
            this.joinedCount.setString('Joined: ' + this.tournamentRoomListManager.myTourRoomBar.length.toString());
            this.runningCount.setString('Running: ' + this.tournamentRoomListManager.getRunningTourCount().toString());
            this.pipe.setPositionX(this.joinedCount.x + this.joinedCount.width / 2 + 10 * scaleFactor);
            this.runningCount.setPositionX(this.pipe.x + this.pipe.width + 4 * scaleFactor);
        }

        this.joinedCount.setVisible(bool);
        this.pipe.setVisible(bool);
        this.runningCount.setVisible(bool);
    },
    myTournamentHeaderHandler: function () {
        if (!this.arrowSprite._isClicked) {
            for (var i = 0; i < this.tournamentRoomListManager.myTourRoomBar.length; i++) {
                cc.log(this.tournamentRoomListManager.listView._items[i + 1]);
                this.tournamentRoomListManager.listView._items[i + 1] && this.tournamentRoomListManager.listView._items[i + 1].removeFromParent(false);
            }
            this.setMyTournamentHeaderDetail(true);
            this.arrowSprite.initSprite(true);
        } else {
            for (var i = 0; i < this.tournamentRoomListManager.myTourRoomBar.length; i++) {
                var roomBar = this.tournamentRoomListManager.myTourRoomBar[i];
                this.tournamentRoomListManager.listView.insertCustomItem(roomBar, i + 1)
            }
            this.setMyTournamentHeaderDetail(false);
            this.arrowSprite.initSprite(false);
        }
        this.tournamentRoomListManager.listView.forceDoLayout();
    },
    setMyTournamentHeader: function () {

        var label = new cc.LabelTTF("My Tournaments", "RobotoRegular", 26 * scaleFactor);
        label.setColor(cc.color(0, 0, 0));
        label.setPosition(cc.p(this.width / 2 - 5 * scaleFactor, this.height / 2));
        label.setScale(0.5);
        this.addChild(label, 2);

        this.arrowSprite = new MyTournamentArrowSprite();
        this.arrowSprite.setPosition(cc.p(this.width / 2 + 55 * scaleFactor, this.height / 2));
        this.addChild(this.arrowSprite, 2);

        cc.eventManager.addListener(this.MyTourTouchListener.clone(), this);
    },
    initTouchListener: function (element) {
        this.MyTourTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                this.myTournamentHeaderHandler();
            }.bind(this)
        });
        this.MyTourTouchListener.retain();
    }
});

var labelText = cc.LabelTTF.extend({
    isEnable: null,
    ctor: function (LabelName, fontFamily, fontSize, pos) {
        this._super(LabelName, fontFamily, fontSize, pos);
        this.setName(LabelName);
        this.setScale(0.5);
        this.setPosition(pos);
        this.setAnchorPoint(0, 0.5);
        this.setColor(cc.color(0, 0, 0));
    }
});
var MyTournamentArrowSprite = cc.Sprite.extend({
    _isClicked: null,
    _hovering: null,
    ctor: function () {
        this._super(spriteFrameCache.getSpriteFrame("myTableArrow.png"));
        this.setRotation(180);
        this.setColor(cc.color(0, 0, 0));
        this.setVisible(true);
        this._hovering = false;
        return true;
    },
    initSprite: function (bool) {
        bool ? this.setRotation(0) : this.setRotation(180);
        this._isClicked = bool;
    }
});