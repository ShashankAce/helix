/**
 * Created by stpl on 2/27/2017.
 */
var TwentyOneTips = Popup.extend({
    selectedGameType: null,
    _isEnabled: null,
    ctor: function (context) {
        this._super(new cc.Size(920 * scaleFactor, 578 * scaleFactor), context, true);
        this._controlAndDisplay = context;
        this.setName("Tips");

        this._isEnabled = true;
        this.headerLeft._setHeight(42 * scaleFactor);
        this.headerLabel.setString("21 Cards Rummy Tips");

        this.crossBtn = new CreateBtn("ResultClose", "ResultClose", "CrossX");
        this.crossBtn.setPosition(cc.p(920 * scaleFactor, 578 * scaleFactor));
        this.addChild(this.crossBtn, 5);

        this.ExitBtn = new CreateBtn("Exit", "ExitHover", "Exit", cc.p(870 * scaleFactor, 30 * scaleFactor));
        this.ExitBtn.setVisible(false);
        this.ExitBtn.pauseListener(true);
        this.addChild(this.ExitBtn, 1);

        this.nextBtn = new CreateBtn("Next", "NextHover", "NextBtn", cc.p(870 * scaleFactor, 30 * scaleFactor));
        this.addChild(this.nextBtn, 2);

        var BgLayer = new cc.DrawNode();
        BgLayer.drawRect(cc.p(1 * scaleFactor, this.height - 160 * scaleFactor), cc.p(this.width - 1 * scaleFactor, this.height - 95 * scaleFactor), cc.color(219, 219, 219), 2, cc.color(219, 219, 219));
        this.addChild(BgLayer);

        var statement = new cc.LabelTTF("Some 21 Cards Rummy Tips below", "RobotoRegular", 16 * 2 * scaleFactor);
        statement.setScale(0.5);
        statement.setColor(cc.color(0, 0, 0));
        statement.setAnchorPoint(0.5, 0.5);
        statement.setPosition(cc.p(this.width / 2, this.height - 60 * scaleFactor));
        this.addChild(statement);

        this.EightPureSequenceTab = new CardRuletab("One", 1, "Set / Sequence Tip");
        this.EightPureSequenceTab.setPosition(cc.p(1, this.height - 160 * scaleFactor));
        this.EightPureSequenceTab.hover(true);
        this.addChild(this.EightPureSequenceTab, 1);
        this.selectedGameType = this.EightPureSequenceTab;

        this.EightJokersTab = new CardRuletab("Two", 2, "Joker Tip");
        this.EightJokersTab.setPosition(cc.p(this.EightPureSequenceTab.x + this.EightPureSequenceTab.width + 2 * scaleFactor, this.height - 160 * scaleFactor));
        this.addChild(this.EightJokersTab, 1);

        this.EightDoublesTab = new CardRuletab("Three", 3, "Doublee tip");
        this.EightDoublesTab.setPosition(cc.p(this.EightJokersTab.x + this.EightJokersTab.width + 2 * scaleFactor, this.height - 160 * scaleFactor));
        this.addChild(this.EightDoublesTab, 1);

        this.threeTunnelsTab = new CardRuletab("Four", 4, "Tunnela Tip");
        this.threeTunnelsTab.setPosition(cc.p(this.EightDoublesTab.x + this.EightDoublesTab.width + 2 * scaleFactor, this.height - 160 * scaleFactor));
        this.addChild(this.threeTunnelsTab, 1);

        this.PureSequenceImage = new cc.Sprite(res.gameFaq.pureSeqImg);
        this.PureSequenceImage.setVisible(true);
        this.PureSequenceImage.setLocalZOrder(1);
        this.PureSequenceImage.setPosition(cc.p(this.width / 2, this.height / 2 - 100 * scaleFactor));
        this.addChild(this.PureSequenceImage);

        var imgText = new cc.LabelTTF("When opponet declares a show and if you have melds of one or more pure sequence/s,\nall cards other than the pure sequence will be counted to your point score.", "RobotoRegular", 15 * 2 * scaleFactor);
        imgText.setScale(0.5);
        imgText.setColor(cc.color(0, 0, 0));
        imgText._setBoundingWidth(1200 * scaleFactor);
        imgText.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        imgText.setAnchorPoint(0.5, 0.5);
        imgText.setPosition(cc.p(this.width / 2 - 60, 300 * scaleFactor));
        this.PureSequenceImage.addChild(imgText);

        this.JokersImage = new cc.Sprite(res.gameFaq.jokerImage);
        this.JokersImage.setAnchorPoint(0.5, 0.5);
        this.JokersImage.setLocalZOrder(0);
        this.JokersImage.setVisible(false);
        this.JokersImage.setPosition(cc.p(this.width / 2, this.height / 2 - 100 * scaleFactor));
        this.addChild(this.JokersImage);

        var JokersImageText = new cc.LabelTTF("when opponent decalres a show and if you have Seven (7) or more jokers,\nyou will earn only two (2) points.", "RobotoRegular", 15 * 2 * scaleFactor);
        JokersImageText.setScale(0.5);
        JokersImageText.setColor(cc.color(0, 0, 0));
        JokersImageText._setBoundingWidth(1200 * scaleFactor);
        JokersImageText.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        JokersImageText.setAnchorPoint(0.5, 0.5);
        JokersImageText.setPosition(cc.p(this.width / 2 - 60, 300 * scaleFactor));
        this.JokersImage.addChild(JokersImageText);

        this.doublesImage = new cc.Sprite(res.gameFaq.doublesImage);
        this.doublesImage.setAnchorPoint(0.5, 0.5);
        this.doublesImage.setVisible(false);
        this.doublesImage.setPosition(cc.p(this.width / 2, this.height / 2 - 100 * scaleFactor));
        this.addChild(this.doublesImage, 0);

        var doublesImageText = new cc.LabelTTF("When opponent declares a show and if you have six or more set of 'Doublees' (pair of same value, same suit cards),\nyou will earn only two (2) points.", "RobotoRegular", 15 * 2 * scaleFactor);
        doublesImageText.setScale(0.5);
        doublesImageText.setColor(cc.color(0, 0, 0));
        doublesImageText._setBoundingWidth(1200 * scaleFactor);
        doublesImageText.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        doublesImageText.setAnchorPoint(0.5, 0.5);
        doublesImageText.setPosition(cc.p(this.width / 2 - 60, 300 * scaleFactor));
        this.doublesImage.addChild(doublesImageText);

        this.TunnelImage = new cc.Sprite(res.gameFaq.tunnelImage);
        this.TunnelImage.setAnchorPoint(0.5, 0.5);
        this.TunnelImage.setVisible(false);
        this.TunnelImage.setPosition(cc.p(this.width / 2, this.height / 2 - 100 * scaleFactor));
        this.addChild(this.TunnelImage, 0);

        var tunnelImageText = new cc.LabelTTF("When opponent declares a show and if you have 3 sets of Tunnelas (trios of same value, same suit cards),\nyou will earn only two (2) points", "RobotoRegular", 15 * 2 * scaleFactor);
        tunnelImageText.setScale(0.5);
        tunnelImageText.setColor(cc.color(0, 0, 0));
        tunnelImageText._setBoundingWidth(1200 * scaleFactor);
        tunnelImageText.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        tunnelImageText.setAnchorPoint(0.5, 0.5);
        tunnelImageText.setPosition(cc.p(this.width / 2 - 60, 300 * scaleFactor));
        this.TunnelImage.addChild(tunnelImageText);

        var footerLabel = new cc.LabelTTF("For further Information Click on help > Rules in Menu", "RobotoRegular", 12 * 2 * scaleFactor);
        footerLabel.setScale(0.5);
        footerLabel.setColor(cc.color(0, 0, 0));
        footerLabel.setAnchorPoint(0.5, 0.5);
        footerLabel.setPosition(cc.p(474 * scaleFactor, 20 * scaleFactor));
        this.addChild(footerLabel, 4);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), this.nextBtn);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.ExitBtn);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.crossBtn);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.EightPureSequenceTab);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.EightJokersTab);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.EightDoublesTab);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.threeTunnelsTab);

        cc.eventManager.addListener(this.popupTouchListener.clone(), this.nextBtn);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.ExitBtn);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.crossBtn);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.EightPureSequenceTab);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.EightJokersTab);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.EightDoublesTab);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.threeTunnelsTab);

        return true;

    },
    makeOneVisible: function () {

        if (this.nextBtn.isVisible() == false)
            this.makeNextButonVisible();
        this.JokersImage.setVisible(false);
        this.doublesImage.setVisible(false);
        this.TunnelImage.setVisible(false);

        this.EightJokersTab.hover(false);
        this.EightDoublesTab.hover(false);
        this.threeTunnelsTab.hover(false);

        this.PureSequenceImage.setVisible(true);
        this.selectedGameType = this.EightPureSequenceTab;

        this.nextBtn.setLocalZOrder(2);
        this.ExitBtn.setLocalZOrder(1);
    },
    makeNextButonVisible: function () {
        this.ExitBtn.setVisible(false);
        this.ExitBtn.pauseListener(true);

        this.nextBtn.setVisible(true);
        this.nextBtn.pauseListener(false);
    },
    makeExitButtonvisible: function () {
        this.nextBtn.setVisible(false);
        this.nextBtn.pauseListener(true);

        this.ExitBtn.setVisible(true);
        this.ExitBtn.pauseListener(false);
    },
    makeTwoVisible: function () {
        if (this.nextBtn.isVisible() == false)
            this.makeNextButonVisible();

        this.PureSequenceImage.setVisible(false);
        this.TunnelImage.setVisible(false);
        this.doublesImage.setVisible(false);

        this.EightPureSequenceTab.hover(false);
        this.EightDoublesTab.hover(false);
        this.threeTunnelsTab.hover(false);

        this.JokersImage.setVisible(true);
        this.selectedGameType = this.EightJokersTab;

        this.nextBtn.setLocalZOrder(2);
        this.ExitBtn.setLocalZOrder(1);

    },
    makeThreeVisible: function () {
        if (this.nextBtn.isVisible() == false)
            this.makeNextButonVisible();
        this.PureSequenceImage.setVisible(false);
        this.JokersImage.setVisible(false);
        this.TunnelImage.setVisible(false);

        this.EightPureSequenceTab.hover(false);
        this.EightJokersTab.hover(false);
        this.threeTunnelsTab.hover(false);

        this.doublesImage.setVisible(true);
        this.selectedGameType = this.EightDoublesTab;

        this.nextBtn.setLocalZOrder(2);
        this.ExitBtn.setLocalZOrder(1);
    },
    makeFourVisible: function () {
        this.PureSequenceImage.setVisible(false);
        this.JokersImage.setVisible(false);
        this.doublesImage.setVisible(false);

        this.TunnelImage.setVisible(true);

        this.EightPureSequenceTab.hover(false);
        this.EightJokersTab.hover(false);
        this.EightDoublesTab.hover(false);

        this.makeExitButtonvisible();
        this.selectedGameType = this.threeTunnelsTab;

        this.ExitBtn.setLocalZOrder(2);
        this.nextBtn.setLocalZOrder(1);

    },
    nextBtnListener: function () {
        if (this.PureSequenceImage.isVisible() == true) {
            this.EightJokersTab.hover(true);
            this.makeTwoVisible();
        } else if (this.JokersImage.isVisible() == true) {
            this.EightDoublesTab.hover(true);
            this.makeThreeVisible();
        } else if (this.doublesImage.isVisible() == true) {
            this.threeTunnelsTab.hover(true);
            this.makeFourVisible();
        }
    },
    onMouseMoveCallBack: function (event) {
        var target = event.getCurrentTarget();
        var locationInNode = target.convertToNodeSpace(event.getLocation());
        var s = target.getContentSize();
        var rect = cc.rect(0, 0, s.width, s.height);
        if (cc.rectContainsPoint(rect, locationInNode)) {
            !target._hovering && target.hover(true, this);
            target._hovering = true;
            return true;
        } else {
            if (this.selectedGameType != target) {
                target._hovering && target.hover(false, this);
                target._hovering = false;
                return false;
            }
        }
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        switch (target._name) {
            case "CrossX":
                this.removeFromParent(true);
                break;
            case "One":
                this.makeOneVisible();
                break;
            case "Two":
                this.makeTwoVisible();
                break;
            case "Three":
                this.makeThreeVisible();
                break;
            case "Four":
                this.makeFourVisible();
                break;
            case "NextBtn":
                if (target._isEnabled)
                    this.nextBtnListener();
                break;
            case "Exit":
                if (target._isEnabled)
                    this.removeFromParent(true);
                break;
        }
    }
});