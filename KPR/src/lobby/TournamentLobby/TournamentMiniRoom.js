/**
 * Created by stpl on 9/16/2016.
 */

// request by tableid and roomid for display
var TournamentMiniRoom = cc.LayerColor.extend({

    _name: "TournamentMiniRoom",
    roomBar: null,
    tournamentLobby: null,

    prizeStrArr: null,

    ctor: function (lobby) {

        this._super(cc.color(0, 0, 0));
        this.tournamentLobby = lobby;

        var miniRoomSize = cc.size(245 * scaleFactor, 437 * scaleFactor);
        this.setContentSize(miniRoomSize);
        this.setAnchorPoint(0, 0);

        var label = cc.LabelTTF.extend({
            ctor: function (labelName, fontFamily, pos, color) {
                this._super(labelName, fontFamily, 12 * 2 * scaleFactor);
                this.setScale(0.5);
                this.setAnchorPoint(cc.p(0, 0.5));
                color ? this.setColor(color) : this.setColor(cc.color(255, 255, 255));
                this.setPosition(pos);
                this.setString(labelName);
                return true;
            }
        });

        var pos = cc.p(18 * scaleFactor, this.height - (8 * scaleFactor));
        var tourName = new label("Name:", "RobotoRegular", pos, cc.color(120, 120, 120));
        this.addChild(tourName, 2);
        pos = cc.p((tourName.x + tourName.width / 2 + 7) * scaleFactor, this.height - (8 * scaleFactor));
        this.tourNameValue = new label("", "RobotoRegular", pos);
        this.addChild(this.tourNameValue, 2);

        pos = cc.p(18 * scaleFactor, this.height - (32 * scaleFactor));
        var startTime = new label("Start Time:", "RobotoRegular", pos, cc.color(120, 120, 120));
        this.addChild(startTime, 2);
        pos = cc.p((startTime.x + startTime.width / 2 + 7) * scaleFactor, this.height - (32 * scaleFactor));
        this.startTimeValue = new label("", "RobotoRegular", pos);
        this.addChild(this.startTimeValue, 2);

        pos = cc.p(18 * scaleFactor, this.height - (56 * scaleFactor));
        var prize = new label("Prize:", "RobotoRegular", pos, cc.color(120, 120, 120));
        this.addChild(prize, 2);
        pos = cc.p((prize.x + prize.width / 2 + 7) * scaleFactor, this.height - (56 * scaleFactor));
        this.prizeValue = new label("", "RupeeFordian", pos);
        this.prizeValue.setDimensions(cc.size(this.width * 1.5, 24));
        this.addChild(this.prizeValue, 2);

        var line = new cc.DrawNode();
        line.drawSegment(cc.p(18 * scaleFactor, this.height - (70 * scaleFactor)), cc.p(218 * scaleFactor, this.height - (70 * scaleFactor)), 0.5, cc.color(38, 43, 46));
        this.addChild(line, 2);

        var layout = new ccui.Layout();
        layout.setBackGroundColorType(ccui.Layout.BG_COLOR_GRADIENT);
        layout.setBackGroundColor(cc.color(128, 128, 128), cc.color(0, 0, 0));
        layout.setContentSize(cc.size(200 * scaleFactor, 25 * scaleFactor));
        layout.setPosition(cc.p(18 * scaleFactor, this.height - (110 * scaleFactor)));
        this.addChild(layout, 2);

        var positionLabel = new cc.LabelTTF("Position", "RobotoRegular", 12 * 2);
        positionLabel.setScale(0.5);
        positionLabel.setColor(cc.color(255, 255, 255));
        positionLabel.setPosition(cc.p(35 * scaleFactor, layout.height / 2 + 2));
        layout.addChild(positionLabel, 2);

        var prizesLabel = new cc.LabelTTF("Prizes", "RobotoRegular", 12 * 2);
        prizesLabel.setScale(0.5);
        prizesLabel.setColor(cc.color(255, 255, 255));
        prizesLabel.setPosition(cc.p(90 * scaleFactor, layout.height / 2 + 2));
        layout.addChild(prizesLabel, 2);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        var height = layout.y + layout.height / 2;
        this.initDetailList(height);


        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    cc.log("Blank listener in TournamentMiniRoom");
                    applicationFacade._controlAndDisplay.closeAllTab();
                    applicationFacade._lobby.closeAllTab();
                    return true;
                }
                return false;
            }.bind(this)
        }, this);


        return true;
    },
    initMouseListener: function (element) {
        this.tourMiniRoomMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            swallowTouches: true,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    !target._hovering && target.hover(true);
                    target._hovering = true;
                    return true;
                }
                target._hovering && target.hover(false);
                target._hovering = false;
                return false;
            }.bind(this)
        });
        this.tourMiniRoomMouseListener.retain();
    },
    initTouchListener: function () {
        this.tourMiniRoomTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode))
                    return true;
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                applicationFacade._controlAndDisplay.closeAllTab();
                applicationFacade._lobby.closeAllTab();
                ButtonHandler.handlerFun(target, this);
            }.bind(this)
        });
        this.tourMiniRoomTouchListener.retain();
    },
    initDetailList: function (yPos) {
        this.detailListView = new ccui.ListView();
        this.detailListView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        this.detailListView.setGravity(ccui.ListView.GRAVITY_CENTER_HORIZONTAL);
        // this.detailListView.setScrollBarAutoHideEnabled(false);
        this.detailListView.setTouchEnabled(true);
        this.detailListView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.detailListView.setBackGroundColor(cc.color(41, 47, 50));
        this.addChild(this.detailListView);
        // cc.color(41, 47, 50)
        // this.initRoomListScrollListener();
    },
    insertDetailList: function () {

    },
    setDetailButtons: function (item) {
        var pos = cc.p(65 * scaleFactor, this.height - (273 * scaleFactor));
        if (this.detailBtn == null) {
            this.detailBtn = new CreateBtn("details", "detailsOver", "Details", pos);
            this.addChild(this.detailBtn, 2);
        }

        pos = cc.p(170 * scaleFactor, this.height - (273 * scaleFactor));
        if (this.statusSym == null) {
            this.statusSym = new StatusSymbol(this, pos);
            this.statusSym.setPosition(pos);
            this.addChild(this.statusSym, 12);

            if (this.statusSym.statusBtn) {
                cc.eventManager.addListener(this.tourMiniRoomMouseListener.clone(), this.statusSym.statusBtn);
                cc.eventManager.addListener(this.tourMiniRoomTouchListener.clone(), this.statusSym.statusBtn);
            }
        }

        cc.eventManager.addListener(this.tourMiniRoomMouseListener.clone(), this.detailBtn);
        cc.eventManager.addListener(this.tourMiniRoomTouchListener.clone(), this.detailBtn);


        var myTournament = item._myTournament || false;
        this.status = this.statusSym.status; // After changing register to join
        this.roomId = item.room.id;
        this.statusSym && this.statusSym.setState(item.room, myTournament, false);
        /* if(item.statusButton){

         }*/

    },
    tourPrizeReq: function (tId) {
        var dataObj = {};
        dataObj["isPrizeStructure"] = true;
        sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.PRIZE_INFO, dataObj, tId);
    },
    setMiniRoom: function (item) {
        // cc.log(item.room);
        var _room = item.room;
        var name = _room.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value;
        var time = _room.StartTimeInUIFormat;
        var prize = this.tournamentLobby.tourRoomListManager.getPrizeType(_room);

        this.tourNameValue.setString(name);
        this.startTimeValue.setString(time);
        this.prizeValue.setString("`" + prize);

        this.setDetailButtons(item);

        this.detailListView.removeAllChildrenWithCleanup(true);
        if (_room.getVariable(TOURNAMENT_CONSTANTS.PRIZE_STRUCT))
            var prizeStrArr = _room.getVariable(TOURNAMENT_CONSTANTS.PRIZE_STRUCT).value != undefined ? _room.getVariable(TOURNAMENT_CONSTANTS.PRIZE_STRUCT).value.split(";") : undefined;

        /*if (_room.hasOwnProperty(TOURNAMENT_CONSTANTS.PRIZE_STRUCT))
         var prizeStrArr = _room[TOURNAMENT_CONSTANTS.PRIZE_STRUCT] != undefined ? _room[TOURNAMENT_CONSTANTS.PRIZE_STRUCT].split(";") : undefined;*/

        if (!_room.getVariable(TOURNAMENT_CONSTANTS.PRIZE_STRUCT)) {
            this.tourPrizeReq(item.room.id);
            return;
        }
        if (prizeStrArr.length <= 4) {
            this.detailListView.setContentSize(cc.size(200, prizeStrArr.length * 25));
            this.detailListView.setPosition(cc.p(18, this.height - (109 + (prizeStrArr.length) * 25)));
        }
        else {
            this.detailListView.setContentSize(cc.size(200, 125));
            this.detailListView.setPosition(cc.p(18, this.height - (234 * scaleFactor)));
        }

        if (prizeStrArr.length == 1 && prizeStrArr[0] == "undefined") {
            var listItem = new DetailList(prizeStrArr[0], "", this);
            listItem.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
            listItem.setBackGroundColor(cc.color(32, 36, 38));
            this.detailListView.pushBackCustomItem(listItem);
        } else {
            for (var i = 0; i < prizeStrArr.length; i++) {
                var prizeInfoArr = (prizeStrArr[i]).split(",");
                var formatStr = prizeInfoArr[1].split(":")[0];
                var formatStrArr = formatStr.split("|");
                var przFrmtStr = prizeInfoArr[1].split(":")[1];
                var przFrmtStrArr = przFrmtStr.split("|");
                var j = 0;
                var prizeStr = "";

                for (j = 0; j < formatStrArr.length; j++) {
                    if (formatStrArr[j] == "C")
                        prizeStr += ("`" + przFrmtStrArr[j] + " + ").toString();
                    else
                        prizeStr += (przFrmtStrArr[j] + " + ").toString();
                }
                prizeStr = prizeStr.substring(0, prizeStr.length - 2);

                var index;
                if (_room.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Time") {
                    index = (prizeInfoArr[0]).split(" ")[0];
                }
                else {
                    index = prizeInfoArr[0];
                }

                var listItem = new DetailList(prizeStr, index, this);
                listItem.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
                listItem.setBackGroundColor(cc.color(32, 36, 38));
                this.detailListView.pushBackCustomItem(listItem);
            }
        }
        this.detailListView.setItemsMargin(1);
    }
});

var DetailList = ccui.Layout.extend({
    _tourMiniRoom: null,
    ctor: function (prizeStr, index, context) {
        this._super();
        this._tourMiniRoom = context;

        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.setContentSize(cc.size(200 * scaleFactor, 25 * scaleFactor));
        this.setBackGroundColor(cc.color(0, 0, 0));

        if (index != "") {
            var positionLabel = new cc.LabelTTF(index, "RobotoRegular", 12 * 2 * scaleFactor);
            positionLabel.setScale(0.5);
            positionLabel.setColor(cc.color(255, 255, 255));
            positionLabel.setAnchorPoint(cc.p(0, 0.5));
            positionLabel.setPosition(cc.p(14 * scaleFactor, this.height / 2 + yMargin));
            this.addChild(positionLabel, 2);
        }

        var prizesLabel = new cc.LabelTTF(prizeStr, "RupeeFordian", 12 * 2 * scaleFactor);
        prizesLabel.setScale(0.5);
        prizesLabel.setAnchorPoint(cc.p(0, 0.5));
        prizesLabel.setColor(cc.color(255, 255, 255));
        prizesLabel.setPosition(cc.p(75 * scaleFactor, this.height / 2 + yMargin));
        this.addChild(prizesLabel, 2);

        return true;
    }
});