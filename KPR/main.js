cc.game.pause = function() {
    cc.game._paused || (cc.game._paused = !0, cc.audioEngine && cc.audioEngine._pausePlaying(), cc.game._intervalId && window.cancelAnimationFrame(cc.game._intervalId), cc.game._intervalId = 0)
};
cc.game.resume = function() {
    cc.game._paused && (cc.game._paused = !1, cc.audioEngine && cc.audioEngine._resumePlaying(), cc.game._runMainLoop(), !cc.game._paused && navigator.onLine && sfs && !sfs.isConnected && sfsClient.connectSfs())
};
var cardEaseTime = 0.15,
    gameConfig = null,
    spriteFrameCache = null,
    applicationFacade = null,
    yMargin = 0;
cc.game.onStart = function() {
    cc.view.enableRetina(!0);
    cc.sys.isMobile && cc.sys.browserType !== cc.sys.BROWSER_TYPE_BAIDU && cc.sys.browserType !== cc.sys.BROWSER_TYPE_WECHAT && cc.view.enableAutoFullScreen(!1);
    cc.view.adjustViewPort(!0);
    cc.view._orientationChanging = !1;
    cc.view.setDesignResolutionSize(GAME_SIZE.width, GAME_SIZE.height, cc.ResolutionPolicy.SHOW_ALL);
    cc.view.resizeWithBrowserSize(!0);
    "firefox" == cc.sys.browserType && (yMargin = 3 * scaleFactor);
    cc.LabelTTF._lastWordRex = /([a-zA-Z0-9\u00c4\u00d6\u00dc\u00e4\u00f6\u00fc\u00df\u00e9\u00e8\u00e7\u00e0\u00f9\u00ea\u00e2\u00ee\u00f4\u00fb,]+|\S)$/;
    cc.LabelTTF._lastEnglish = /[a-zA-Z0-9\u00c4\u00d6\u00dc\u00e4\u00f6\u00fc\u00df\u00e9\u00e8\u00e7\u00e0\u00f9\u00ea\u00e2\u00ee\u00f4\u00fb,]+$/;
    cc.LabelTTF._firsrEnglish = /^[a-zA-Z0-9\u00c4\u00d6\u00dc\u00e4\u00f6\u00fc\u00df\u00e9\u00e8\u00e7\u00e0\u00f9\u00ea\u00e2\u00ee\u00f4\u00fb,]/;
    cc.loader.load(loader_resources, function() {
        !cc.sys.isNative && document.getElementById("cocosLoading") && document.body.removeChild(document.getElementById("cocosLoading"));
        var a = "Configuration/ConnectionConfig.json?v\x3d" +
            (new Date).getTime();
        cc.loader.loadJson(a, function(a, c) {
            a || (connectionConfig = new ConnectionConfig(c), new ResourcesSetUp, LoaderScene.preload(g_resources, function() {
                cc.loader.loadJson("Configuration/GameConfig.json", function(a, b) {
                    a || (spriteFrameCache = cc.spriteFrameCache, spriteFrameCache.addSpriteFrames(res.atlas.lobbyPlist_plist, res.atlas.lobbyPlist_png), spriteFrameCache.addSpriteFrames(res.atlas.gameRoomPlist_plist, res.atlas.gameRoomPlist_png), spriteFrameCache.addSpriteFrames(res.atlas.SmileyPlist_plist,
                            res.atlas.SmileyPlist_png), spriteFrameCache.addSpriteFrames(res.atlas.ButtonPlist_plist, res.atlas.ButtonPlist_png), spriteFrameCache.addSpriteFrames(res.atlas.loaderPlist_plist, res.atlas.loaderPlist_png), res.halfPixelImage = new Image, res.halfPixelImage.onload = function() {
                            res.bodyImage = new Image;
                            res.bodyImage.onload = function() {
                                gameConfig = new GameConfiguration(b);
                                applicationFacade = new ApplicationFacade;
                                cc.director.runScene(applicationFacade)
                            };
                            res.bodyImage.src = ccui.ScrollViewBar.BODY_IMAGE_1_PIXEL_HEIGHT
                        },
                        res.halfPixelImage.src = ccui.ScrollViewBar.HALF_CIRCLE_IMAGE)
                })
            }, this))
        }.bind(this))
    })
};
cc.game.run();
document.addEventListener("contextmenu", function(a) {
    a.preventDefault()
});
window.onkeyup = function(a) {
    if (123 === (a.keyCode ? a.keyCode : a.which)) return !1
};