/**
 * Created by stpl on 1/16/2017.
 */
var RoundAllPrize = cc.Layer.extend({
    ctor: function (context, room) {
        this._super();
        this.setContentSize(cc.size(720 * scaleFactor, 390 * scaleFactor));
        this.setPosition(cc.p(220 * scaleFactor, 3 * scaleFactor));

        this.trnmntRoom = room;

        var rect = new cc.DrawNode();
        rect.drawRect(cc.p(1 * scaleFactor, 0), cc.p(719 * scaleFactor, 30 * scaleFactor), cc.color(235, 235, 235), 1, cc.color(cc.color(235, 235, 235)));
        rect.setPosition(cc.p(0, this.height - 31 * scaleFactor));
        this.addChild(rect, 1);

        var rect2 = new cc.DrawNode();
        rect2.drawRect(cc.p(1 * scaleFactor, 60 * scaleFactor), cc.p(719 * scaleFactor, 389 * scaleFactor), cc.color(235, 235, 235, 0), 1, cc.color(cc.color(235, 235, 235)));
        this.addChild(rect2, 1);


        // on header strip
        var verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(85 * scaleFactor, 359 * scaleFactor), cc.p(85 * scaleFactor, 389 * scaleFactor), 0.5, cc.color(255, 255, 255));
        this.addChild(verticalLine, 2);

        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(388 * scaleFactor, 359 * scaleFactor), cc.p(388 * scaleFactor, 389 * scaleFactor), 0.5, cc.color(255, 255, 255));
        this.addChild(verticalLine, 2);

        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(570 * scaleFactor, 359 * scaleFactor), cc.p(570 * scaleFactor, 389 * scaleFactor), 0.5, cc.color(255, 255, 255));
        this.addChild(verticalLine, 2);

        // on list view
        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(85 * scaleFactor, 60 * scaleFactor), cc.p(85 * scaleFactor, 359 * scaleFactor), 0.5, cc.color(220, 220, 220));
        this.addChild(verticalLine, 1);

        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(388 * scaleFactor, 60 * scaleFactor), cc.p(388 * scaleFactor, 359 * scaleFactor), 0.5, cc.color(220, 220, 220));
        this.addChild(verticalLine, 1);

        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(570 * scaleFactor, 60 * scaleFactor), cc.p(570 * scaleFactor, 359 * scaleFactor), 0.5, cc.color(220, 220, 220));
        this.addChild(verticalLine, 1);

        var label;
        label = new cc.LabelTTF("Position", "RobotoBold", 18 * scaleFactor);
        label.setScale(0.7);
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(18 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        rect.addChild(label);

        label = new cc.LabelTTF("Prize", "RobotoBold", 18 * scaleFactor);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(108 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        rect.addChild(label);

        label = new cc.LabelTTF("Given in Round", "RobotoBold", 18 * scaleFactor);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(410 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        rect.addChild(label);

        label = new cc.LabelTTF("Players", "RobotoBold", 18 * scaleFactor);
        label.setScale(0.7);
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(590 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        rect.addChild(label);

        this.prizeListView = new ccui.ListView();
        this.prizeListView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        this.prizeListView.setGravity(ccui.ListView.GRAVITY_CENTER_HORIZONTAL);
        this.prizeListView.setTouchEnabled(true);
        this.prizeListView.setItemsMargin(0);
        this.prizeListView.setScrollBarAutoHideEnabled(true);
        this.prizeListView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.prizeListView.setBackGroundColor(cc.color(255, 255, 255));
        this.prizeListView.setContentSize(cc.size(this.width - 2 * scaleFactor, 300 * scaleFactor));
        this.prizeListView.setPosition(cc.p(1 * scaleFactor, 60 * scaleFactor));
        this.addChild(this.prizeListView);
    },
    updatePrizeStructure: function () {

        this.prizeListView.removeAllChildrenWithCleanup(true);

        var prizeStr = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.PRIZE_STRUCT).value;
        var prizeArr = prizeStr.split(";");

        var winnerStr = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.WINNER_PLR_POS) && this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.WINNER_PLR_POS).value;
        if (winnerStr == "undefined")
            winnerStr = ":;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:";
        var winnerStrArr = winnerStr.split(";");

        for (var index = 0; index < prizeArr.length; index++) {

            var prizeInfo = prizeArr[index].split(",");
            var plrName = "";
            for (var i = 0; i < winnerStrArr.length; i++) {
                var plrNameArr = winnerStrArr[i].split(":");
                if (plrNameArr[0] == prizeInfo[0]) {
                    plrName = plrNameArr[1];
                    winnerStrArr.splice(i, 1);
                    break;
                }
            }

            var bar = new RoundAllPrizeBar(this, prizeArr[index], plrName, index);
            this.prizeListView.pushBackCustomItem(bar);
        }
    },
    initRoomListScrollListener: function () {

        if ('mouse' in cc.sys.capabilities) {
            this.prizeListViewMouseListener = cc.EventListener.create({
                event: cc.EventListener.MOUSE,
                swallowTouches: true,
                onMouseScroll: function (event) {
                    var target = event.getCurrentTarget();
                    var locationInNode = target.convertToNodeSpace(event.getLocation());
                    var s = target.getContentSize();
                    var rect = cc.rect(0, 0, s.width, s.height);

                    if (cc.rectContainsPoint(rect, locationInNode)) {

                        var delta = cc.sys.isNative ? event.getScrollY() * 6 : -event.getScrollY();

                        var minY = this.prizeListView._contentSize.height - this.prizeListView._innerContainer.getContentSize().height;
                        var h = -minY;
                        var locContainer = this.prizeListView._innerContainer.getBottomBoundary();
                        var percentY = ((h + locContainer) / h) * 100;
                        var scrollSpeed = 15; // higher value is inversely proportional to speed

                        this._scrolledPercent = percentY;

                        if ((this._scrolledPercent + delta / scrollSpeed) < 0) {
                            this._scrolledPercent = 0;
                        } else if ((this._scrolledPercent + delta / scrollSpeed) > 100) {
                            this._scrolledPercent = 100;
                        } else {
                            this._scrolledPercent += delta / scrollSpeed;
                        }
                        if (this._scrolledPercent <= 100 && this._scrolledPercent >= 0) {
                            this.prizeListView.scrollToPercentVertical(this._scrolledPercent, 0.2, true);
                        }
                        return true;
                    }
                    return false;
                }.bind(this)
            });
            this.prizeListViewMouseListener.retain();
            cc.eventManager.addListener(this.prizeListViewMouseListener, this.prizeListView);
        }
    }
});
var RoundAllPrizeBar = ccui.Layout.extend({
    ctor: function (context, prizeStr, plrName, index) {
        this._super();
        this.setContentSize(cc.size(context.width, 30 * scaleFactor));
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);

        if (index % 2 == 0)
            this.setBackGroundColor(cc.color(255, 255, 255));
        else
            this.setBackGroundColor(cc.color(235, 235, 235));

        var prizeInfoArr = prizeStr.split(",");

        var label;
        label = new cc.LabelTTF(prizeInfoArr[0], "RobotoRegular", 16 * scaleFactor);
        label.setScale(0.7);
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(18 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label);

        var finalPrize = "";
        var prize = prizeInfoArr[1].split(":");
        var extraPrize = prize[1].split("|");
        if (extraPrize[1])
            finalPrize += extraPrize[0] + " + " + extraPrize[1];
        else
            finalPrize += extraPrize[0];
        label = new cc.LabelTTF('`' + finalPrize, "RupeeFordian", 16 * scaleFactor);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(107 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label);

        var rndStr = "";
        if (prizeInfoArr[3]) {
            if (prizeInfoArr[3] == "F")
                rndStr = "(Final)";
            else if (prizeInfoArr[3] == "S")
                rndStr = "(Semi Final)";
            else if (prizeInfoArr[3] == "Q")
                rndStr = "(Quarter Final)";
            else
                rndStr = "";
        }

        label = new cc.LabelTTF("Round " + prizeInfoArr[2] + rndStr, "RobotoRegular", 16 * scaleFactor);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(410 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label);

        label = new cc.LabelTTF(plrName, "RobotoRegular", 16 * scaleFactor);
        label.setScale(0.7);
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(590 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label);

    },
    setWinnerPosition: function () {

    }
});