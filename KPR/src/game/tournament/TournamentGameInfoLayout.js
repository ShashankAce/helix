/**
 * Created by stpl on 1/23/2017.
 */
var TournamentGameInfoLayout = cc.Sprite.extend({
    _name: "TournamentGameInfoLayout",
    serverRoom: null,
    blockSize: null,
    _isEnable: false,
    _isDone: true,
    parentRoom: null,
    _hovering: null,

    ctor: function (room, context) {
        this._super(spriteFrameCache.getSpriteFrame("GameInfoBg.png"));
        this.setAnchorPoint(0.5, 1);
        this.setCascadeOpacityEnabled(true);
        this.setCascadeColorEnabled(true);
        this.setOpacity(0);

        this._hovering = false;
        this.serverRoom = room;
        this.parentRoom = sfsClient.getRoomById(room.getVariable(SFSConstants.PARENT_EXT_ROOM_ID).value);
        //this._gameRoom = context;
        this.tabSpace = 4;

        this.initGameInfo();
        return true;
    },
    appear: function () {
        var dx = 0.3;
        var moveBy = cc.moveBy(dx, 0, 25).easing(cc.easeCubicActionOut());
        var fadeIn = cc.fadeIn(dx).easing(cc.easeCubicActionOut());
        var spawn = cc.spawn(moveBy, fadeIn);

        var spawnRev = spawn.reverse();
        if (!this._isEnable) {
            this.runAction(cc.sequence(spawnRev, cc.callFunc(function () {
                this.setVisible(false);
                this._isDone = true;
            }.bind(this), this)));
        }
        else {
            this.setVisible(true);
            this.runAction(cc.sequence(spawn, cc.callFunc(function () {
                // TODO
                this._isDone = true;
            }.bind(this), this)));
        }
    },
    initGameInfo: function () {
        var headerFontSize = 12.5 * scaleFactor,
            valueFontSize = 12.5 * scaleFactor,
            headerFontFamily = "RobotoBold",
            fontFamily = "RobotoRegular",
            leftColumnXCordinate = 15 * scaleFactor,
            rightColumnXCordinate = 200 * scaleFactor,
            margin = 14 * scaleFactor,
            startY = 7 * scaleFactor;

        var roomType = this.serverRoom.getVariable(SFSConstants._GAMETYPE).value;

        this.entryFee = this.getLabel("Buy-In", headerFontFamily, headerFontSize);
        this.entryFee.setPosition(cc.p(leftColumnXCordinate, this.height - margin - startY));

        this.entryFeeValue = this.getLabel("25", "RupeeFordian", valueFontSize);
        this.entryFeeValue.setPosition(cc.p(this.entryFee.x + this.entryFee.width / 2 + this.tabSpace, this.entryFee.y));

        this.addChild(this.entryFee);
        this.addChild(this.entryFeeValue);

        this.firstDrop = this.getLabel("First Drop", headerFontFamily, headerFontSize);
        this.firstDrop.setPosition(cc.p(rightColumnXCordinate, this.height - (margin * 7) - startY));

        this.interimDrop = this.getLabel("Interim Drop", headerFontFamily, headerFontSize);
        this.interimDrop.setPosition(cc.p(rightColumnXCordinate, this.height - (margin * 9) - startY));

        this.firstDropValue = this.getLabel("20 Points", fontFamily, valueFontSize);
        this.firstDropValue.setPosition(cc.p(this.firstDrop.x + this.firstDrop.width / 2 + this.tabSpace, this.firstDrop.y));

        this.interimDropValue = this.getLabel("40 Points", fontFamily, valueFontSize);
        this.interimDropValue.setPosition(cc.p(this.interimDrop.x + this.interimDrop.width / 2 + this.tabSpace, this.interimDrop.y));

        this.addChild(this.firstDrop);
        this.addChild(this.interimDrop);
        this.addChild(this.firstDropValue);
        this.addChild(this.interimDropValue);

        this.turnTime = this.getLabel("Turn Time", headerFontFamily, headerFontSize);
        this.turnTime.setPosition(cc.p(leftColumnXCordinate, this.height - (margin * 3) - startY));

        this.extraTimer = this.getLabel("Extra Timer", headerFontFamily, headerFontSize);
        this.extraTimer.setPosition(cc.p(leftColumnXCordinate, this.height - (margin * 5) - startY));

        this.noOfDecks = this.getLabel("Number of decks", headerFontFamily, headerFontSize);
        this.noOfDecks.setPosition(cc.p(leftColumnXCordinate, this.height - (margin * 7) - startY));

        this.printedJoker = this.getLabel("Printed Jokers", headerFontFamily, headerFontSize);
        this.printedJoker.setPosition(cc.p(leftColumnXCordinate, this.height - (margin * 9) - startY));

        this.jokers = this.getLabel("Jokers", headerFontFamily, headerFontSize);
        this.jokers.setPosition(cc.p(rightColumnXCordinate, this.height - margin - startY));

        this.fullCount = this.getLabel("Full Count", headerFontFamily, headerFontSize);
        this.fullCount.setPosition(cc.p(rightColumnXCordinate, this.height - (margin * 3) - startY));

        this.firstHandShow = this.getLabel("First hand Show", headerFontFamily, headerFontSize);
        this.firstHandShow.setPosition(cc.p(rightColumnXCordinate, this.height - (margin * 5) - startY));


        this.turnTimeValue = this.getLabel("40(Normal)", fontFamily, valueFontSize);
        this.turnTimeValue.setPosition(cc.p(this.turnTime.x + this.turnTime.width / 2 + this.tabSpace, this.turnTime.y));

        this.extraTimerValue = this.getLabel("60 sec", fontFamily, valueFontSize);
        this.extraTimerValue.setPosition(cc.p(this.extraTimer.x + this.extraTimer.width / 2 + this.tabSpace, this.extraTimer.y));

        this.decksValue = this.getLabel("2 decks", fontFamily, valueFontSize);
        this.decksValue.setPosition(cc.p(this.noOfDecks.x + this.noOfDecks.width / 2 + this.tabSpace, this.noOfDecks.y));

        this.printedJokersValue = this.getLabel("1 per deck", fontFamily, valueFontSize);
        this.printedJokersValue.setPosition(cc.p(this.printedJoker.x + this.printedJoker.width / 2 + this.tabSpace, this.printedJoker.y));

        this.jokersValue = this.getLabel("Printed & Wild Card", fontFamily, valueFontSize);
        this.jokersValue.setPosition(cc.p(this.jokers.x + this.jokers.width / 2 + this.tabSpace/*2.5*this.tabSpace*/, this.jokers.y));

        this.fullCountValue = this.getLabel("80 points", fontFamily, valueFontSize);
        this.fullCountValue.setPosition(cc.p(this.fullCount.x + this.fullCount.width / 2 + this.tabSpace, this.fullCount.y));

        this.firstHandShowValue = this.getLabel("40 Points", fontFamily, valueFontSize);
        this.firstHandShowValue.setPosition(cc.p(this.firstHandShow.x + this.firstHandShow.width / 2 + this.tabSpace, this.firstHandShow.y));

        this.addChild(this.turnTime, 1);
        this.addChild(this.extraTimer, 1);
        this.addChild(this.noOfDecks, 1);
        this.addChild(this.printedJoker, 1);
        this.addChild(this.jokers, 1);
        this.addChild(this.fullCount, 1);
        this.addChild(this.firstHandShow, 1);

        this.addChild(this.turnTimeValue, 1);
        this.addChild(this.extraTimerValue, 1);
        this.addChild(this.decksValue, 1);
        this.addChild(this.printedJokersValue, 1);
        this.addChild(this.fullCountValue, 1);
        this.addChild(this.firstHandShowValue, 1);
        this.addChild(this.jokersValue, 1);

        this.updateGameInfo();
    },
    getLabel: function (labelString, fontFamily, fontSize) {
        if (fontFamily == "RobotoBold")
            var label = new cc.LabelTTF(labelString + ":", fontFamily, fontSize * 2);
        else
            var label = new cc.LabelTTF(labelString, fontFamily, fontSize * 2);

        label.setScale(0.5);
        label.setColor(cc.color(0, 0, 0));
        label.setAnchorPoint(0, 1);

        return label;
    },
    setEntryFee: function () {
        if (this.parentRoom) {
            var tempFee = this.parentRoom.getVariable(SFSConstants._ENTRYFEE).value;
            var tempArr;
            if (tempFee) {
                tempArr = tempFee.split(",");
                if (tempArr.length == 1) {
                    this.entryFeeValue.setString(tempFee);
                }
                else {
                    var formatArr = tempArr[0].split("|");
                    var feeArr = tempArr[1].split("|");
                    if (formatArr[0] == "Chip")
                        this.entryFeeValue.setString("`" + feeArr[0]);
                    if (formatArr[0] == "Ticket") {
                        if (parseInt((feeArr[0].toString()).split(":")[0]) == 1)
                            this.entryFeeValue.setString(formatArr[0]);
                        else
                            this.entryFeeValue.setString((feeArr[0].toString()).split(":")[0] + " " + formatArr[0]);
                    }
                }
            }
        }
    },
    updateGameInfo: function () {
        if (this.serverRoom && this.serverRoom != null) {
            this.setEntryFee();
            this.setPrize();
            var roomType = this.serverRoom.getVariable(SFSConstants._GAMETYPE).value;
            if (roomType.split("-")[1] == "NoJoker")
                this.jokersValue.setString("Printed Joker");
            else if (this.serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value == 13)
                this.jokersValue.setString("Printed & Wild Card");
            else {
                this.jokersValue.setString("Up, Down, Printed & Wild Card");
                this.changeCoordinate();
            }
            this.extraTimerValue.setString("60 sec");
            this.printedJokersValue.setString("1 per Deck");
            if (this.serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value == 21) {
                this.decksValue.setString("3 Deck(s)");
                this.firstDropValue.setString("30 Points");
                this.interimDropValue.setString("70 Points");
                this.fullCountValue.setString("120 Points");
                this.firstHandShowValue.setString("70 Points");
            }
            else {
                this.firstHandShowValue.setString("40 Points");
                this.decksValue.setString("2 Deck(s)");
                this.fullCountValue.setString("80 Points");
                this.firstDropValue.setString("20 Points");
                this.interimDropValue.setString("30 Points");
            }
        }
    },
    changeCoordinate: function (shiftLeft) {

        var rightColumnXCordinate = 200 * scaleFactor,
            margin = 14 * scaleFactor,
            startY = 7 * scaleFactor;

        if (shiftLeft) {
            rightColumnXCordinate = 175 * scaleFactor;
        }
        this.firstDrop.setPosition(cc.p(rightColumnXCordinate, startY - (margin * 9)));
        this.interimDrop.setPosition(cc.p(rightColumnXCordinate, startY - (margin * 11)));
        this.jokers.setPosition(cc.p(rightColumnXCordinate, startY - (margin * 3)));
        this.fullCount.setPosition(cc.p(rightColumnXCordinate, startY - (margin * 5)));
        this.firstHandShow.setPosition(cc.p(rightColumnXCordinate, startY - (margin * 7)));
    },
    hover: function () {

    },
    setPrize: function () {
        //this.prizeValue.setString(this.serverRoom.getVariable(SFSConstants._WINNING).value);
    },
    setTurnTime: function (time) {
        this.turnTimeValue.setString(time + " ( " + this.serverRoom.getVariable(SFSConstants._TURNTYPE).value + " )");
    }
});