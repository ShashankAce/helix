/**
 * Created by stpl on 1/19/2017.
 */
var DateNTime = {};

DateNTime._serverTime = "";
DateNTime._zoneName = "Asia/Calcutta";

DateNTime.getTimeObject = function (time) {
    var seconds = Math.floor(time / 1000);
    var minutes = Math.floor(seconds / 60);
    var hours = Math.floor(minutes / 60);
    var days = Math.floor(hours / 24);
    seconds %= 60;
    minutes %= 60;
    hours %= 24;
    var d = "";
    var h = "";
    var m = "";
    var s = "";
    if (String(days).length == 1)
        d = "0" + String(days);
    else
        d = String(days);

    if (String(hours).length == 1)
        h = "0" + String(hours);
    else
        h = String(hours);

    if (String(minutes).length == 1)
        m = "0" + String(minutes);
    else
        m = String(minutes);

    if (String(seconds).length == 1)
        s = "0" + String(seconds);
    else
        s = String(seconds);
    //trace("days", d, "h", h, "m", m,"s",s);
    return {D: d, H: h, M: m, S: s};

};

DateNTime.getStartTime = function (timeInMS) {
    var startday = new Date(Number(timeInMS));
    var minutes = startday.getMinutes();
    var hours = startday.getHours();
    return (hours + minutes / 100);
};

DateNTime.getDateInfo = function (timeInMS) {
    var output;
    var startday = new Date(Number(timeInMS));
    //trace("startDate", startday);
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    //var today = new Date();
    var today = applicationFacade._controlAndDisplay._serverDate;
    var day;
    var c_time;
    var AM_PM;
    var is_zero_hour;
    var is_zero_min;
    var is_zero_dat;
    var is_zero_mon;
    var minutes = startday.getMinutes();
    var hours = startday.getHours();
    var dat = startday.getDate();
    var month = startday.getMonth();
    var year = startday.getFullYear();
    var dateStr;
    //trace("today",today.getMonth(),month, today.getFullYear(),year,today.getDate(),dat)
    if (today.getMonth() == month && today.getFullYear() == year) {
        //	trace("today ",today.getDate(),dat)
        if (dat == today.getDate()) {
            dateStr = "Today";
        }
        /*if (dat == (today.getDate()+1))
         {
         //trace("Tomorrow ",today.getDate())
         dateStr="Tomorrow";
         }*/
    }
    //trace("dateStr",dateStr)
    //var dayN = today.getDay();
    /*switch (dayN) {
     case 0 :day = "Sunday";
     break;
     case 1 :day = "Monday";
     break;
     case 2 :day = "Tuesday";
     break;
     case 3 :day = "Wednesday";
     break;
     case 4 :day = "Thursday";
     break;
     case 5 :day = "Friday";
     break;
     case 6 :day = "Saturday";
     break;
     }*/
    if (hours > 12) {
        c_time = (hours - 12);
        AM_PM = "pm";
    }
    if (hours == 12) {
        c_time = 12;
        AM_PM = "pm";
    }
    if (hours < 12) {
        c_time = hours;
        AM_PM = "am";
    }
    if (hours == 0) {
        c_time = 12;
        AM_PM = "am";
    }
    if (minutes < 10) {
        is_zero_min = "0";
    } else {
        is_zero_min = "";
    }
    if (dat < 10) {
        is_zero_dat = "0";
    } else {
        is_zero_dat = "";
    }
    if (month < 10) {
        is_zero_mon = "0";
    } else {
        is_zero_mon = "";
    }
    is_zero_hour = c_time >= 10 ? "" : "0";

    if (dateStr) {
        //trace("time ", c_time + ":" + is_zero_min + minutes + " " + AM_PM +" " + dateStr)
        output = is_zero_hour + c_time + ":" + is_zero_min + minutes + " " + AM_PM + " " + dateStr;
    }
    else {
        output = dat + " " + months[month] + " " + is_zero_hour + c_time + ":" + is_zero_min + minutes + " " + AM_PM;
        //trace("time ", month + "     " + dat + " " + months[month] + " " + c_time + ":" + is_zero_min + minutes + " " + AM_PM )
    }
    //output = c_time + ":" + is_zero_min + minutes + " " + AM_PM + " " + day + " " + is_zero_dat + dat + "." + is_zero_mon + month + "." + year;
    //trace("output", output);
    return output;
};