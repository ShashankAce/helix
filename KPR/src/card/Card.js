"use strict";
var Card = CardModel.extend({
    _userCardPanel: null,
    _isFlipped: null,
    _isFrontSide: null,
    //
    _frontSide: null,
    _backSide: null,
    //
    _touchListener: null,
    _repeatAnimation: null,
    _cardMoved: null,
    //
    _initialPosition: null,
    _leftCardPos: null,
    _rightCardPos: null,
    //
    _leftCardTag: null,
    _rightCardTag: null,
    _sortCardTag: null,
    grNo: null,
    //
    _stackIndex: null,
    _isEnabled: null,
    _isSelected: null,
    _touchDelta: null,
    //
    _touchEnded: null,
    ctor: function (name, flipped, parent) {
        this._super(name);
        this._userCardPanel = parent;

        this.setTag(-1);
        this._leftCardTag = -1;
        this._rightCardTag = -1;
        this._sortCardTag = 0;
        this.grNo = 0;
        this._stackIndex = 0;

        this._touchListener = null;
        this._repeatAnimation = false;
        this._cardMoved = false;
        this._initialPosition = {};
        this._leftCardPos = {};
        this._rightCardPos = {};
        this._isEnabled = true;
        this._isSelected = false;
        this._touchDelta = {x: 0, y: 0};
        this._touchEnded = false;

        if (flipped) {
            this.setScale(-1, 1);
            this._initBackSide();
        }

        this._isFrontSide = !flipped;
        this._isFlipped = !flipped;

        if (this._userCardPanel.totCards == 22)
            this.setScale(0.8);

        return true;
    },
    setSelected: function (bool) {
        this._cardMoved = bool;
        this._isSelected = bool;
    },
    _initBackSide: function () {
        this._backSide = new cc.Sprite(spriteFrameCache.getSpriteFrame("cardBack.png"));
        this._backSide.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(this._backSide, 5);
    },
    setStackIndex: function (params) {
        this._stackIndex = params;
    },
    _switchSide: function () {
        this._backSide.setVisible(!this._backSide.isVisible());
        this.setLocalZOrder(this._userCardPanel._cardZOrder);
        ++this._userCardPanel._cardZOrder;
    },
    _addListener: function () {
        this._isFrontSide = !this._isFrontSide;
        cc.eventManager.addListener(this._userCardPanel.card_touchListener.clone(), this);
    },
    flip: function (i) {
        this.stopAllActions();
        var scaleVal;
        if (this._userCardPanel.totCards == 22)
            scaleVal = 0.8;
        else
            scaleVal = 1;

        var delay = 0.3,
            cardSeq = cc.sequence(
                cc.scaleTo(delay, 0, scaleVal),
                cc.callFunc(function () {
                    this._switchSide();
                }, this),
                cc.scaleTo(delay, scaleVal, scaleVal),
                cc.callFunc(function () {
                    this._addListener();
                }, this),
                cc.callFunc(function () {
                    this.flipEnd(i);
                }, this)
            );
        // var rep = new cc.RepeatForever(cardSeq,2);
        if (!cc.game._paused) {
            this.runAction(cardSeq);
        } else {
            this._switchSide();
            this.setScale(scaleVal);
            this._addListener();
            this.flipEnd(i);
        }
    },
    flipEnd: function (i) {
        if (i == this._userCardPanel.cardStack.length - 1) {
            this._userCardPanel.flipEndListener();
        }
    },
    resetPosition: function (positionObj, i) {
        var _parent = this.parent;
        _parent.isCardAnimating = true;
        var _newPos = positionObj;
        var easing = cc.moveTo(0.2, _newPos.x, _newPos.y).easing(cc.easeExponentialOut());
        if (!cc.game._paused) {
            this.runAction(cc.sequence(easing, cc.callFunc(function () {
                if (i && i == _parent.cardStack.length - 1) {
                    _parent.isCardAnimating = false;
                    if (_parent._selectedCardArray.length > 0 && _parent.groupBtn.isVisible()) {
                        _parent.groupBtn.setPositionX(_parent._selectedCardArray[_parent._selectedCardArray.length - 1].x);
                    }
                }
                if (!i) {
                    _parent.isCardAnimating = false;
                }
            }, this)));
        } else {
            this.setPosition(_newPos);
            if (i && i == _parent.cardStack.length - 1) {
                _parent.isCardAnimating = false;
                if (_parent._selectedCardArray.length > 0 && _parent.groupBtn.isVisible()) {
                    _parent.groupBtn.setPositionX(_parent._selectedCardArray[_parent._selectedCardArray.length - 1].x);
                }
            }
            if (!i) {
                _parent.isCardAnimating = false;
            }
        }
    }
});

var CardBack = cc.Sprite.extend({
    _name: "CardBack",
    ctor: function () {
        this._super(spriteFrameCache.getSpriteFrame("cardBack.png"));
        return true;
    },
    tween: function (easing, cleanUp) {
        this.runAction(cc.sequence(easing, cc.callFunc(function () {
            cleanUp && this.removeFromParent(true);
        }, this)));
    }
});