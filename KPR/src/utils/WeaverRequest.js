/**
 * Created by shashank on 4/8/2016.
 */
var AjaxCommunication = {
    post: function (data, callback) {

        var xhr = cc.loader.getXMLHttpRequest();
        //xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.open("POST", "/khelplayrummy/rummyCurl/index.php", true);
        xhr.setRequestHeader("Device", gameConfig.deviceType);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.setRequestHeader("Device-Type", gameConfig.os);
        xhr.setRequestHeader("App-Version", "" + gameConfig.appVersion);
        xhr.setRequestHeader("Screen-Size", GAME_SIZE.width + "x" + GAME_SIZE.height + "y");
        xhr.onreadystatechange = function () {
            //xhr.status >= 200
            // if (xhr.readyState == 4 && ( xhr.status >= 200 && xhr.status <= 207 )) {
            if (xhr.readyState == 4 && xhr.status == 200) {
                try {
                    if (!xhr.responseText == "") {
                        var resp = JSON.parse(xhr.responseText);
                        if (!resp.errorCode) {
                            resp.errorCode = 0;
                        }
                        cc.log(resp);
                        callback(resp);
                    }
                    else {
                        cc.log(xhr.status);
                        callback({"errorCode": 404});
                    }
                }
                catch (exception) {
                    cc.log("Code Exception :" + exception + "\nResponce :" + xhr.responseText);
                }
            } else {
                cc.log(xhr.status);
            }
        };

        xhr.onerror = function () {
            cc.log("Connection Error :" + xhr.status);
            callback({"errorCode": xhr.status});
        };
        xhr.send(data);
    },
    requestUrl: function (url, type, callback) {

        var xhr = cc.loader.getXMLHttpRequest();
        xhr.open(type, url);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && ( xhr.status >= 200 && xhr.status <= 207 )) {
                try {
                    var resp = JSON.parse(xhr.responseText);
                    callback(resp);
                }
                catch (exception) {
                    cc.log("Code Exception :" + exception + "\nResponce :" + xhr.responseText);
                }
            } else {
                cc.log(xhr.status);
            }
        };
        xhr.onerror = function () {
            cc.log("Connection Error :" + xhr.status);
        };
        xhr.send();
    }
};
/*xhr.setRequestHeader('Access-Control-Allow-Headers','Content-Type, Content-Range, Content-Disposition, Content-Description');
 xhr.setRequestHeader('Access-Control-Allow-Origin', '*'); //Access-Control-Allow-Origin: *
 xhr.setRequestHeader('Access-Control-Allow-Methods', 'DELETE, HEAD, GET, OPTIONS, POST, PUT');*/