/**
 * Created by stpl on 08-Dec-16.
 */
var DiscardCard = cc.Scale9Sprite.extend({
    _name: "DiscardCard",
    _cardValue: null,
    _cardScore: null,
    _cardSuit: null,
    _pickedBy: "",
    _isPicked: null,
    _delegate: null,
    _hovering: null,
    ctor: function (name, delegate) {
        this._super(spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png"), cc.rect(0, 0, 50 * scaleFactor, 50 * scaleFactor));
        this.setCapInsets(cc.rect(20 * scaleFactor, 20 * scaleFactor, 20 * scaleFactor, 20 * scaleFactor));
        this.setContentSize(cc.size(58 * scaleFactor, 60 * scaleFactor));
        this.setScale(0.5);

        this._delegate = delegate;
        this._isPicked = false;
        this._pickedBy = "";
        this._hovering = false;
        var cardLabel;
        var nameArray = [], cardName = name, color = cc.color(0, 0, 0);
        if (name) {
            if (name == "Joker") {
                this._cardScore = 0;
                this._cardSuit = "JK";
                this._cardValue = "JK";
                cardName = "joker_small.png";

                cardLabel = new cc.LabelTTF(this._cardValue, "FranklinDemi", 15 * 2 * scaleFactor);
                cardLabel.setScale(0.75, 0.83);
                cardLabel.setPosition(cc.p(13.5 * scaleFactor, 37 * scaleFactor));
                cardLabel.setColor(color);
                // this.addChild(cardLabel, 1);

                var joker = new cc.Sprite(spriteFrameCache.getSpriteFrame(cardName));
                joker.setPosition(cc.p(this.width / 2, this.height / 2));
                this.addChild(joker, 1);
            }
            else {
                nameArray = name.split("#");
                if (nameArray.length == 2) {
                    switch (nameArray[0]) {
                        case "C":
                            cardName = "club.png";
                            color = cc.color(0, 0, 0);
                            break;
                        case "H":
                            cardName = "heart.png";
                            color = cc.color(204, 0, 0);
                            break;
                        case "S":
                            cardName = "spade.png";
                            color = cc.color(0, 0, 0);
                            break;
                        case "D":
                            cardName = "diamond.png";
                            color = cc.color(204, 0, 0);
                            break;
                    }
                    if (nameArray[1] == "A") {
                        this._cardScore = 1;
                    } else if (nameArray[1] == "J") {
                        this._cardScore = 11;
                    } else if (nameArray[1] == "Q") {
                        this._cardScore = 12;
                    } else if (nameArray[1] == "K") {
                        this._cardScore = 13;
                    } else {
                        this._cardScore = parseInt(nameArray[1]);
                    }
                    this._cardValue = nameArray[1];
                    this._cardSuit = nameArray[0];
                }
                cardLabel = new cc.LabelTTF(this._cardValue, "FranklinDemi", 25 * 2 * scaleFactor);
                cardLabel.setScale(0.45, 0.5);
                cardLabel.setPosition(cc.p(13 * scaleFactor, 41 * scaleFactor));
                cardLabel.setColor(color);
                this.addChild(cardLabel, 1);

                var smallSuit = new cc.Sprite(spriteFrameCache.getSpriteFrame(cardName));
                smallSuit.setPosition(cc.p(36 * scaleFactor, 23 * scaleFactor));
                smallSuit.setScale(0.70);
                this.addChild(smallSuit, 1);
            }
        } else {
            throw new Error("Card name can not be null");
        }
        return true;
    },
    picked: function (name) {
        this._pickedBy = name;
        this._isPicked = true;
        // coding for add star
    },
    hover: function (bool) {
        this._delegate.starHover(this, bool);
    }
});