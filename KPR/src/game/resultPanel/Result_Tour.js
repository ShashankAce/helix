/**
 * Created by stpl on 11/16/2016.
 */
var Result_Tour = Popup.extend({
    _gameType: null,
    _isPool: null,
    resultTime: null,
    _gameRoom: null,
    waitTxt: null,
    ccTimeOutManager: null,
    txtStr: null,
    tableId: null,
    _isBuyNoClicked: null,
    ctor: function (context, params, _showTime, _isPool) {
        this._super(new cc.Size(860 * scaleFactor, 580 * scaleFactor), context, true);
        this._gameRoom = context;
        this.gameType = context._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
        this.resultTime = params["resultTime"];

        this.ccTimeOutManager = new CCTimeOutManager();

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        this.txtStr = params["txtStr"];
        this.tableId = this._gameRoom._serverRoom.getVariable(SFSConstants.DATA_ROOMID).value;

        this.headerLeft.setContentSize(cc.size(this.width, 40 * scaleFactor));
        this.headerLeft.setAnchorPoint(0.5, 0);
        this.headerLeft.setPosition(cc.p(this.width / 2, this.height - this.headerLeft.height));

        this.headerLabel.setString("Result-Tournament ID:123456");

        this.cross = new cc.Sprite(spriteFrameCache.getSpriteFrame("ResultClose.png"));
        this.cross.setName("Cross");
        this.cross.setAnchorPoint(0.5, 0.5);
        this.cross.setPosition(this.width - 3 * scaleFactor, this.height - 3 * scaleFactor);
        this.addChild(this.cross, 1);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.cross);

        this.roundInfoLabel = new cc.LabelTTF("Round 1/3 | Deal:420", "RobotoBold", 34 * scaleFactor);
        this.roundInfoLabel.setScale(0.5);
        this.roundInfoLabel.setAnchorPoint(1, 0.5);
        this.roundInfoLabel.setPosition(cc.p(839 * scaleFactor, this.height - this.headerLeft.height / 2));
        this.addChild(this.roundInfoLabel, 1);

        if (this.txtStr)
            this.roundInfoLabel.setString(this.txtStr);
        else
            this.roundInfoLabel.setString("");

        this._isBuyNoClicked = false;

        /*if(params["rndId"])
         this.roundInfoLabel.setString(" Round : " +params["rndId"]+" | Round Time: "+params["rndTime"]+" s.");
         else
         this.roundInfoLabel.setString("");*/

        var topGreyStrip = new cc.DrawNode();
        topGreyStrip.drawRect(cc.p(0, 492 * scaleFactor), cc.p(this.width, (492 + 50) * scaleFactor), cc.color(102, 102, 102), 1, cc.color(102, 102, 102));
        this.addChild(topGreyStrip, 1);

        var labelFont = 30 * scaleFactor;
        var headerLabel = new cc.LabelTTF("Player", "RobotoBold", labelFont);
        headerLabel.setScale(0.5);
        headerLabel.setPosition(cc.p(50 * scaleFactor, (492 + 25) * scaleFactor));
        this.addChild(headerLabel, 1);

        headerLabel = new cc.LabelTTF("Result", "RobotoBold", labelFont);
        headerLabel.setScale(0.5);
        headerLabel.setPosition(cc.p(160 * scaleFactor, (492 + 25) * scaleFactor));
        this.addChild(headerLabel, 1);

        headerLabel = new cc.LabelTTF("Points Won", "RobotoBold", labelFont);
        headerLabel.setScale(0.5);
        headerLabel.setPosition(cc.p(615 * scaleFactor, (492 + 25) * scaleFactor));
        this.addChild(headerLabel, 1);

        headerLabel = new cc.LabelTTF("Chips", "RobotoBold", labelFont);
        headerLabel.setScale(0.5);
        headerLabel.setPosition(cc.p(710 * scaleFactor, (492 + 25) * scaleFactor));
        this.addChild(headerLabel, 1);

        headerLabel = new cc.LabelTTF("Total Chips", "RobotoBold", labelFont);
        headerLabel.setScale(0.5);
        headerLabel.setPosition(cc.p(805 * scaleFactor, (492 + 25) * scaleFactor));
        this.addChild(headerLabel, 1);

        var bottomBlueStrip = new cc.DrawNode();
        bottomBlueStrip.drawRect(cc.p(1 * scaleFactor, 1 * scaleFactor), cc.p(this.width - 1 * scaleFactor, 73 * scaleFactor), cc.color(230, 240, 245), 1, cc.color(230, 240, 245));
        this.addChild(bottomBlueStrip, 1);

        this.addList(params);

        /*this.namePopup = new UserNamePlaceHolder("blank");
         this.namePopup.setPosition(cc.p(132, 450));
         this.addChild(this.namePopup, 10);*/

        this.waitTxt = new cc.LabelTTF("", "RobotoRegular", 15 * 2 * scaleFactor);
        this.waitTxt.setScale(0.5);
        this.waitTxt.setColor(cc.color(0, 0, 0));
        this.waitTxt.setHorizontalAlignment(cc.TEXT_ALIGNMENT_RIGHT);
        this.waitTxt.setPosition(cc.p(this.width * (2 / 3), 35 * scaleFactor));
        this.addChild(this.waitTxt, 2);

        if (_isPool) {
            this._gameType = "Pool";
            this.setTime(_showTime, params["resultTime"]);
        }
        else {
            this._gameType = "Point";
            this.setTimePoint(params["resultTime"]);
        }

        return true;
    },
    leaveRoom: function () {
        this.stopResultTimer();
    },
    setTime: function (_showTime, time) {
        this.stopResultTimer();
        if (_showTime) {
            this.cross.setVisible(false);
            this.headerLabel.setString("Result - Table ID: " + this.tableId);
            this.setResultTime(time);
        }
        else {
            this.cross.setVisible(true);
            this.headerLabel.setString("Last Result - Table ID: " + this.tableId);
            this.waitTxt.setString("");
        }

    },
    setTimePoint: function (time) {

        this.stopResultTimer();
        this.cross.setVisible(false);
        this.headerLabel.setString("Result - Table ID: " + this.tableId);
        this.setResultTime(time);
    },
    updateRank: function (rnk) {
        if (this.txtStr) {
            this.txtStr = this.txtStr.replace("-1", rnk + "");
            this.roundInfoLabel.setString(this.txtStr);
        }
    },
    setWildCard: function (wildCrd) {
        var joker1;

        if (!wildCrd || wildCrd == "") {
            return;
        }
        var gameTypeStr = this._gameRoom._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
        if (gameTypeStr == "Point-NoJoker")
            return;

        var clipper = new ccui.Layout();
        clipper.setContentSize(cc.size(150 * scaleFactor, 70 * scaleFactor));
        clipper.setClippingEnabled(true);
        clipper.setPosition(cc.p(15 * scaleFactor, 1 * scaleFactor));
        this.addChild(clipper, 4);
        var joker_label = new cc.LabelTTF("JOKER", "RobotoBold", 12 * 2 * scaleFactor);
        joker_label.setScale(0.5);
        joker_label.setColor(cc.color(0, 0, 0));
        joker_label.setPosition(cc.p(90 * scaleFactor, 60 * scaleFactor));
        clipper.addChild(joker_label, 4);
        if (wildCrd == "Joker") {
            joker1 = new CardModel("S#A");
            joker1.setScale(0.6);
            joker1.setPosition(cc.p(40 * scaleFactor, 15 * scaleFactor));
            clipper.addChild(joker1, 1);
            joker_label.setPosition(cc.p(65 * scaleFactor, 60 * scaleFactor));
        }
        else
            joker_label.setPosition(cc.p(75 * scaleFactor, 60 * scaleFactor));
        var jkr = new CardModel(wildCrd);
        jkr.setScale(0.6);
        jkr.setPosition(cc.p(75 * scaleFactor, 15 * scaleFactor));
        clipper.addChild(jkr, 1);

    },
    setResultTime: function (time) {
        var resultTime = time;//data["resultTime"];
        var text = "Please wait while game starts in ";
        this.waitTxt && this.waitTxt.setString(text + resultTime + " secs");
        this.ccTimeOutManager.scheduleInterval(xyz.bind(this), 1000, 0, "resultTimer");
        function xyz() {

            if (resultTime > 1) {
                resultTime = resultTime - 1;
                this.waitTxt && this.waitTxt.setString(text + resultTime + " secs");
            }
            else {

                this.ccTimeOutManager.unScheduleInterval("resultTimer");
                this.waitTxt && this.waitTxt.setString("Please wait...");

            }
        }
    },
    stopResultTimer: function () {
        this.ccTimeOutManager.unScheduleInterval("resultTimer");
    },

    getResultPlayerInfo: function (params) {
        this.playerInfo = [];
        for (var i = 1; i <= parseInt(params.tot); i++) {
            var resultPlsInfo = new ResultPlInfo();
            var name = params["pl" + i]["name"];
            var meldCards = params["pl" + i]["meldCards"];
            var resultType = params["pl" + i]["resultType"];
            var roundScore = params["pl" + i]["rndSc"];
            var isSplit = params["pl" + i]["isSplit"];
            var totalScore = 0;
            if (params["pl" + i]["totSc"])
                totalScore = (params["pl" + i]["totSc"]);
            else if (params["pl" + i]["totalChip"]) {
                totalScore = (params["pl" + i]["totalChip"]);
            }
            var playerId = (params["pl" + i]["id"]);
            var amount = null;
            if (params["pl" + i][SFSConstants.FLD_AMOUNT])
                amount = params["pl" + i][SFSConstants.FLD_AMOUNT];
            var isRejoin = "" + params["pl" + i][SFSConstants.FLD_REJOIN];
            resultPlsInfo.setResultPlInfo(name, meldCards, resultType, roundScore, isSplit, totalScore, playerId, amount, isRejoin);
            this.playerInfo.push(resultPlsInfo);

            var player = this._gameRoom.getSeatObjByUserName(name);
            if (player) {
                player.player_score = totalScore;
                player.updateScore(totalScore);
            }
        }
    },
    addList: function (params) {
        this.getResultPlayerInfo(params);
        /*for (var k = 0; k < parseInt(params.tot); k++) {
         if (this.playerInfo[k].getPlayerId() == this._gameRoom._myId) {
         /!*if (this.playerInfo[k].getIsSplit() != "false") {
         if (this.checkForSplitButton(this.playerInfo[k].getIsSplit())) {
         this.SplitBtn.hide(false); // visible true
         this._gameRoom.isSplitVisible = true;
         this.splitData = this.playerInfo[k].getIsSplit();
         }
         }*!/

         /!*if (this.playerInfo[k].getIsRejoin() == "true" && !this._gameRoom._isWatching) {
         // visivle rejoin button
         this.reJoinBtn.hide(false);
         }*!/
         }
         }*/
        for (var i = 0; i < parseInt(params.tot); i++) {
            var resultStrip = new ResultTourListStrip(this, params["pl" + (i + 1)], i);
            var dy = 457 * scaleFactor - i * 70 * scaleFactor;
            resultStrip.setPosition(cc.p(this.width / 2, dy - i));
            this.addChild(resultStrip, 5);
            cc.eventManager.addListener(this.popupMouseListener.clone(), resultStrip);
            cc.eventManager.addListener(this.popupTouchListener.clone(), resultStrip);
        }
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        switch (target._name) {
            case "Cross":
                this._gameRoom.closeResult();
                break;

        }
    },
    initNamePopup: function (bool, name, posY) {
        if (bool) {
            this.namePopup.setVisible(true);
            this.namePopup.nameString.setString(name);
            this.namePopup.setPositionY(posY);
        } else {
            this.namePopup.setVisible(false);
        }
    },
    /*onMouseMoveCallBack: function (event) {
     var target = event.getCurrentTarget();
     var locationInNode = target.convertToNodeSpace(event.getLocation());
     var targetSize = target.getContentSize();
     var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
     if (cc.rectContainsPoint(targetRect, locationInNode)) {
     switch (target._name) {
     case "resultList":
     var name = target.playerName.name;
     this.initNamePopup(true, name, target.y);
     break;
     default:
     this.initNamePopup(!1);
     }
     return true;
     }
     return !1;
     }*/
});
var ResultTourListStrip = ccui.Layout.extend({
    _name: "resultList",
    _index: null,
    _result: null,
    ctor: function (context, Ginfo, index) {
        this._super();
        this._index = index;
        this._result = context;

        var Info = Ginfo;
        this.setContentSize(cc.size(858 * scaleFactor, 69 * scaleFactor));
        this.setAnchorPoint(0.5, 0.5);
        this.setLocalZOrder(0);
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);

        /*if (index % 2 == 0)
         this.setBackGroundColor(cc.color(227, 227, 227));
         else
         this.setBackGroundColor(cc.color(245, 245, 245));*/

        this.setBackGroundColor(cc.color(245, 245, 245));

        if (applicationFacade._myId == Info.id)
            this.setBackGroundColor(cc.color(227, 227, 227));

        var line = new cc.DrawNode();
        line.drawSegment(cc.p(115 * scaleFactor, this.height), cc.p(115 * scaleFactor, 0), 0.5, cc.color(220, 220, 220));
        this.addChild(line);

        var line2 = new cc.DrawNode();
        line2.drawSegment(cc.p(210 * scaleFactor, this.height), cc.p(210 * scaleFactor, 0), 0.5, cc.color(220, 220, 220));
        this.addChild(line2);

        var line3 = new cc.DrawNode();
        line3.drawSegment(cc.p(564 * scaleFactor, this.height), cc.p(564 * scaleFactor, 0), 0.5, cc.color(220, 220, 220));
        this.addChild(line3);

        var line4 = new cc.DrawNode();
        line4.drawSegment(cc.p(663 * scaleFactor, this.height), cc.p(663 * scaleFactor, 0), 0.5, cc.color(220, 220, 220));
        this.addChild(line4);

        var line5 = new cc.DrawNode();
        line5.drawSegment(cc.p(753 * scaleFactor, this.height), cc.p(753 * scaleFactor, 0), 0.5, cc.color(220, 220, 220));
        this.addChild(line5);

        var bottomLine = new cc.DrawNode();
        bottomLine.drawSegment(cc.p(1 * scaleFactor, 0), cc.p(this.width, 0), 0.5, cc.color(220, 220, 220));
        this.addChild(bottomLine);

        this.playerName = new PlayerName(Info.name, this);
        this.playerName.setPosition(cc.p(this.width / 16, this.height / 2));
        this.addChild(this.playerName, 2);

        var prizeIcon = "", prizeText = "";
        switch (Info.resultType) {
            case "drop":
                prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("DropTableIcon.png"));
                prizeText = "Dropped";
                break;
            case "Left":
                prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("LeftTableIcon.png"));
                prizeText = "Left";
                break;
            case "wrongShow" :
                prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("wrongShow.png"));
                prizeText = "WrongShow";
                break;
            case "validShow" :
                prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("ValidShowIcon.png"));
                prizeText = "ValidShow";
                break;
        }

        if (String(Info.resultType).toLowerCase() == "winner" || this._result.gameType.indexOf("Point") >= 0 && Info.resultType == "" && Info['rndSc'] == 0) {
            prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("prize.png"));
            prizeText = "Winner";
            this.setBackGroundColor(cc.color(231, 239, 188));
        }


        if (prizeIcon != "") {
            prizeIcon.setPosition(cc.p(this.width / 8 + this.width / 16, this.height / 2 + 10 * scaleFactor));
            this.addChild(prizeIcon);
        } else {
            cc.log("Prize information not available");
        }

        var prizeLabel = new cc.LabelTTF(prizeText, "RobotoRegular", 2 * 12 * scaleFactor);
        prizeLabel.setScale(0.5);
        prizeLabel.setAnchorPoint(0.5, 0.5);
        prizeLabel.setColor(cc.color(0, 0, 0));
        prizeLabel.setPosition(cc.p(this.width / 8 + this.width / 16, this.height / 3 - 10 * scaleFactor));
        this.addChild(prizeLabel);

        var gameScore = new cc.LabelTTF((Info.rndSc).toString(), "RobotoRegular", 14 * 2 * scaleFactor);
        gameScore.setScale(0.5);
        gameScore.setColor(cc.color(0, 0, 0));
        gameScore.setPosition(cc.p(615 * scaleFactor, this.height / 2));
        this.addChild(gameScore);

        var totalScore = new cc.LabelTTF("", "RobotoRegular", 14 * 2 * scaleFactor);
        totalScore.setScale(0.5);
        totalScore.setColor(cc.color(0, 0, 0));
        totalScore.setPosition(cc.p(709 * scaleFactor, this.height / 2));
        this.addChild(totalScore);

        var totalChip = new cc.LabelTTF((Info.totalChip).toString(), "RobotoRegular", 14 * 2 * scaleFactor);
        totalChip.setScale(0.5);
        totalChip.setColor(cc.color(0, 0, 0));
        totalChip.setPosition(cc.p(800 * scaleFactor, this.height / 2));
        this.addChild(totalChip);

        if (Info.hasOwnProperty("totSc")) {
            totalScore.setString((Info.totSc).toString());  // in case of pool
        } else {
            totalScore.setString((Info.amt).toString());  // in case of point
        }

        var meldCardString = Info.meldCards;
        if (meldCardString != "null") {
            this.addMeldCard(meldCardString);
        }
        return true;
    },

    addMeldCard: function (cardsString) {
        var groupArray,
            unGroupArray,
            tmpCards,
            cardsPipe;
        cc.log(cardsString);

        if (cardsString.indexOf("|") == -1) {
            unGroupArray = cardsString.split(":");
            groupArray = [];
        }
        else {
            cardsPipe = (cardsString).split("|");
            groupArray = cardsPipe[0].split(";");
            unGroupArray = cardsPipe[1].split(":");
        }
        var card;
        var xInt = 235 * scaleFactor, cardCount = 0;
        var spaceInGrp = 47;
        var spaceInCrd = 14;
        for (var i = 0; i < groupArray.length; i++) {
            if (groupArray[i] != "") {
                tmpCards = groupArray[i].split(",");
                for (var j = 0; j < tmpCards.length; j++) {
                    card = new DummyCard(tmpCards[j]);
                    card.setTag(i);
                    card.x = xInt;
                    card.y = this.height / 2;
                    xInt = xInt + 14 * scaleFactor;
                    ++cardCount;
                    this.addChild(card, cardCount);
                }
                xInt = card.x + spaceInGrp * scaleFactor;
            }
        }
        for (var i = 0; i < unGroupArray.length; i++) {
            if (unGroupArray[i] != "") {
                tmpCards = unGroupArray[i].split(",");
                for (var j = 0; j < tmpCards.length; j++) {
                    card = new DummyCard(tmpCards[j]);
                    card.setTag(i);
                    card.x = xInt;
                    card.y = this.height / 2;
                    xInt = xInt + spaceInCrd * scaleFactor;
                    ++cardCount;
                    this.addChild(card, cardCount);
                }
            }
        }
    }
});