/**
 * Created by stpl on 12/9/2016.
 */
var Drop = cc.Sprite.extend({
    _name: "DropBtn",
    _isEnabled: null,
    _userCardPanel: null,
    isCheckboxEnabled: null,
    isCheckboxSelected: null,
    currentState: null,
    _hovering: null,
    check: null,
    ctor: function (context) {
        this._super();
        this._userCardPanel = context;
        this._isEnabled = true;
        var self = this;
        this.check = {
            currentFrame: 2,
            gotoAndStop: function (e) {
                if (e == 1)
                    self.setState(3);
                else if (e == 2)
                    self.setState(2);
                this.currentFrame = e;
            }
        };
        this._hovering = false;
        this.setAnchorPoint(0, 0);
        this.setPosition(cc.p(585 * scaleFactor, 110 * scaleFactor));

        this.isCheckboxEnabled = false;
        this.isCheckboxSelected = true;
        this.currentState = 1;

        this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("DropButton.png"));

        return true;

    },
    /**
     * Set state of drop button
     * @param {Drop.DROP_BUTTON_NORMAL|1|Drop.DROP_BUTTON_CHECKBOX|2|Drop.DROP_BUTTON_CHECKBOX_SELECTED|3|Drop.DROP_BUTTON_CHECKBOX_DISABLED|4|Drop.DROP_BUTTON_NORMAL_DISABLED|5} value
     */
    setState: function (value) {
        this.currentState = value;
        switch (value) {
            case 1 :
                this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("DropButton.png"));
                this.isCheckboxEnabled = false;
                this.isCheckboxSelected = true;
                break;
            case 2 :
                this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("DropButtonCheck.png"));
                this.isCheckboxEnabled = true;
                this.isCheckboxSelected = false;
                break;
            case 3 :
                this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("DropButtonWithTick.png"));
                this.isCheckboxEnabled = true;
                this.isCheckboxSelected = true;
                break;
            case 4 :
                this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("DropButtonCheckOver.png"));
                this.isCheckboxEnabled = true;
                this.isCheckboxSelected = false;
                break;
            case 5 :
                this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("DropButtonOver.png"));
                this.isCheckboxEnabled = false;
                this.isCheckboxSelected = true;
                break;

        }
    },
    hide: function (bool) {
        this._isEnabled = !bool;
        this.setVisible(!bool);
        /*var stack = new Error().stack;
         cc.log("DROP VISIBLE - " + !bool + '\nCALL STACK ');
         cc.log(stack);*/
    },
    hover: function (bool) {
        // do not delete it | Its compulsory to have hover in every components
    }
});

Drop.DROP_BUTTON_NORMAL = 1;
Drop.DROP_BUTTON_CHECKBOX = 2;
Drop.DROP_BUTTON_CHECKBOX_SELECTED = 3;
Drop.DROP_BUTTON_CHECKBOX_DISABLED = 4;
Drop.DROP_BUTTON_NORMAL_DISABLED = 5;