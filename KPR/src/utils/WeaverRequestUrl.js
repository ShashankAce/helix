/**
 * Created by stpl on 7/13/2016.
 */
var WeaverRequestUrl = {};
WeaverRequestUrl.welcome = "https://dev_2x.khelplayrummy.com";
WeaverRequestUrl.rootUrl = KPR.GAME_BASE_URL + "Weaver/service/rest";
WeaverRequestUrl.playerLogin= WeaverRequestUrl.rootUrl+"/playerLogin";
WeaverRequestUrl.playerLogout= WeaverRequestUrl.rootUrl+"/playerLogout"
WeaverRequestUrl.forgotPassword= WeaverRequestUrl.rootUrl+"/forgotPassword";
WeaverRequestUrl.checkAvailability= WeaverRequestUrl.rootUrl+"/checkAvailability";
WeaverRequestUrl.playerRegistration= WeaverRequestUrl.rootUrl+"/playerRegistration";
WeaverRequestUrl.changePassword= WeaverRequestUrl.rootUrl+"/changePassword";
WeaverRequestUrl.playerProfile= WeaverRequestUrl.rootUrl+"/playerProfile";
WeaverRequestUrl.getCommonData= WeaverRequestUrl.rootUrl+"/getCommonData";
WeaverRequestUrl.updatePlayerProfile= WeaverRequestUrl.rootUrl+"/updatePlayerProfile";
WeaverRequestUrl.paymentOptions= WeaverRequestUrl.rootUrl+"/paymentOptions";
WeaverRequestUrl.transactionDetails= WeaverRequestUrl.rootUrl+"/transactionDetails";
WeaverRequestUrl.sendVerificationCode= WeaverRequestUrl.rootUrl+"/sendVerificationCode";
WeaverRequestUrl.verifyPlayer= WeaverRequestUrl.rootUrl+"/verifyPlayer";
WeaverRequestUrl.bonusDetails= WeaverRequestUrl.rootUrl+"/bonusDetails";
WeaverRequestUrl.playerInbox= WeaverRequestUrl.rootUrl+"/playerInbox";
WeaverRequestUrl.inboxActivity= WeaverRequestUrl.rootUrl+"/inboxActivity";
WeaverRequestUrl.withdrawalDetails= WeaverRequestUrl.rootUrl+"/withdrawalDetails";
WeaverRequestUrl.cancelWithdrawal= WeaverRequestUrl.rootUrl+"/cancelWithdrawal";
WeaverRequestUrl.promoCodeList= WeaverRequestUrl.rootUrl+"/promoCodeList";
WeaverRequestUrl.validatePromoCode= WeaverRequestUrl.rootUrl+"/validatePromoCode";
WeaverRequestUrl.inviteFriend= WeaverRequestUrl.rootUrl+"/inviteFriend";
WeaverRequestUrl.offlineDepositRequest= WeaverRequestUrl.rootUrl+"/offlineDepositRequest";
WeaverRequestUrl.withdrawalRequest= WeaverRequestUrl.rootUrl+"/withdrawalRequest";
WeaverRequestUrl.versionControl= WeaverRequestUrl.rootUrl+"/versionControl";
WeaverRequestUrl.isDepositProcess= WeaverRequestUrl.rootUrl+"/isDepositProcess";