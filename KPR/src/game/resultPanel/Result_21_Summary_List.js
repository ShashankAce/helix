/**
 * Created by stpl on 12/14/2016.
 */
"use strict";
var Result_21_Summary_List = ccui.Layout.extend({
    _name: "Result_21_Summary_List",
    _gameRoom: null,
    _result21: null,
    _index: null,
    playerName: null,
    ctor: function (result21, data, index) {
        this._super();

        this._result21 = result21;
        this._gameRoom = result21._gameRoom;
        this._index = index;

        this.setContentSize(cc.size(949 * scaleFactor, 69 * scaleFactor));
        this.setAnchorPoint(0, 1);
        this.setLocalZOrder(0);
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);

        /*if (index % 2 == 0)
         this.setBackGroundColor(cc.color(227, 227, 227));
         else
         this.setBackGroundColor(cc.color(245, 245, 245));*/

        this.setBackGroundColor(cc.color(245, 245, 245));

        if (applicationFacade._myId == data["id"])
            this.setBackGroundColor(cc.color(227, 227, 227));

        var line = new cc.DrawNode();
        line.drawSegment(cc.p(115 * scaleFactor, this.height - 1 * scaleFactor), cc.p(115 * scaleFactor, 0), 0.5, cc.color(200, 200, 200));
        this.addChild(line);

        var line2 = new cc.DrawNode();
        line2.drawSegment(cc.p(218 * scaleFactor, 0), cc.p(218 * scaleFactor, this.height - 1 * scaleFactor), 0.5, cc.color(200, 200, 200));
        this.addChild(line2);

        var line3 = new cc.DrawNode();
        line3.drawSegment(cc.p(488 * scaleFactor, this.height - 1 * scaleFactor), cc.p(488 * scaleFactor, 0), 0.5, cc.color(200, 200, 200));
        this.addChild(line3);

        var line4 = new cc.DrawNode();
        line4.drawSegment(cc.p(597 * scaleFactor, this.height - 1 * scaleFactor), cc.p(597 * scaleFactor, 0), 0.5, cc.color(200, 200, 200));
        this.addChild(line4);

        var line5 = new cc.DrawNode();
        line5.drawSegment(cc.p(716 * scaleFactor, this.height - 1 * scaleFactor), cc.p(716 * scaleFactor, 0), 0.5, cc.color(200, 200, 200));
        this.addChild(line5);

        var line6 = new cc.DrawNode();
        line6.drawSegment(cc.p(835 * scaleFactor, this.height - 1 * scaleFactor), cc.p(835 * scaleFactor, 0), 0.5, cc.color(200, 200, 200));
        this.addChild(line6);

        var bottomLine = new cc.DrawNode();
        bottomLine.drawSegment(cc.p(1 * scaleFactor, 0), cc.p(this.width - 1 * scaleFactor, 0), 0.5, cc.color(220, 220, 220));
        this.addChild(bottomLine);

        var nm = data["name"];
        nm = nm.substr(0, 12);
        this.playerName = new PlayerName(nm, this);
        this.playerName.setAnchorPoint(cc.p(0, 0.5));
        this.playerName.setPosition(cc.p(16 * scaleFactor, this.height / 2));
        this.addChild(this.playerName, 2);

        var prizeIcon, prizeText = "";
        if (data.resultType != "") {
            switch (data.resultType) {
                case "drop":
                    prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("DropTableIcon.png"));
                    prizeText = "Dropped";
                    break;
                case "Left":
                    prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("LeftTableIcon.png"));
                    prizeText = "Left";
                    break;
                case "wrongShow" :
                    prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("wrongShow.png"));
                    prizeText = "WrongShow";
                    break;
                case "validShow" :
                    prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("ValidShowIcon.png"));
                    prizeText = "ValidShow";
                    break;
            }

            if (String(data.resultType).toLowerCase() == "winner" || this._result21.gameType.indexOf("Point") >= 0 && data.resultType == "" && data['rndSc'] == 0) {
                prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("prize.png"));
                prizeText = "Winner";
                this.setBackGroundColor(cc.color(231, 239, 188));
            }

            if (prizeIcon) {
                prizeIcon.setPosition(cc.p(165 * scaleFactor, this.height / 2 + 10 * scaleFactor));
                this.addChild(prizeIcon);
            } else {
                cc.log("Prize information not available");
            }

            var prizeLabel = new cc.LabelTTF(prizeText, "RobotoRegular", 2 * 12 * scaleFactor);
            prizeLabel.setScale(0.5);
            prizeLabel.setAnchorPoint(0.5, 0.5);
            prizeLabel.setColor(cc.color(0, 0, 0));
            prizeLabel.setPosition(cc.p(165 * scaleFactor, this.height / 3 - 10 * scaleFactor));
            this.addChild(prizeLabel);
        }

        var valueCardScore = new cc.LabelTTF(data["valCrdSc"], "RobotoRegular", 14 * 2 * scaleFactor);
        valueCardScore.setScale(0.5);
        valueCardScore.setColor(cc.color(0, 0, 0));
        valueCardScore.setPosition(cc.p(548 * scaleFactor, this.height / 2));
        this.addChild(valueCardScore);

        var pointsWon = new cc.LabelTTF(data["ptWon"], "RobotoRegular", 14 * 2 * scaleFactor);
        pointsWon.setScale(0.5);
        pointsWon.setColor(cc.color(0, 0, 0));
        pointsWon.setPosition(cc.p(658 * scaleFactor, this.height / 2));
        this.addChild(pointsWon);

        var totalScore = new cc.LabelTTF(data["rndSc"], "RobotoRegular", 14 * 2 * scaleFactor);
        totalScore.setScale(0.5);
        totalScore.setColor(cc.color(0, 0, 0));
        totalScore.setPosition(cc.p(768 * scaleFactor, this.height / 2));
        this.addChild(totalScore);

        var amount = new cc.LabelTTF(data["amt"], "RobotoRegular", 14 * 2 * scaleFactor);
        amount.setScale(0.5);
        amount.setColor(cc.color(0, 0, 0));
        amount.setPosition(cc.p(890 * scaleFactor, this.height / 2));
        this.addChild(amount);

        if (data["name"] == rummyData.nickName) {
            this._result21._validityObj = {};
            this._result21._validityObj.cards = data["meldCards"];
            this._result21._validityObj.str = data["validity"];

            if (data.resultType == "wrongShow") {

                var gameType = this._gameRoom._serverRoom.getVariable(SFSConstants._CASH_OR_PROMO).value;
                if ((data["validity"] != null) && (data["validity"] != "null")) {/*gameType == "PROMO" && */
                    this._result21.showDeclareInfo();
                    this._result21.showInvalidDeclaration();
                }
                else {
                    this._result21.invalidDeclaration && this._result21.invalidDeclaration.removeFromParent(true);
                }
            }
        }
        return true;
    },
    addValueCards: function (valueCardsArray) {

        cc.log("Value card array : " + valueCardsArray);

        var _valueCards = valueCardsArray;
        var xInt = 260 * scaleFactor, cardCount = 1;
        var wildCardSuit = this._result21._wildCardStr.split("#")[0];

        for (var i = 0; i < _valueCards.length; i++) {

            if (_valueCards[i] == "Joker")
                continue;

            if (this._result21._wildCardStr != "Joker" && _valueCards[i].indexOf(wildCardSuit) == -1)
                continue;

            if (this._result21._wildCardStr == "Joker" && _valueCards[i].indexOf("A") != -1 && _valueCards[i].indexOf("S") == -1)
                continue;

            var card = new DummyCard(_valueCards[i]);
            card.setJoker(this._result21._wildCardStr, this._gameRoom._jokerSymVisble);
            card.setTag(i);
            card.x = xInt;
            card.y = this.height / 2;

            xInt = xInt + 14 * scaleFactor;
            ++cardCount;
            this.addChild(card, cardCount);
        }
    }
});

var PlayerName = cc.LabelTTF.extend({
    name: null,
    _resultList: null,
    ctor: function (playerName, context) {
        if (!playerName) {
            playerName = "default";
        }
        if (playerName.length > 9);
        var name = playerName.substr(0, 9) + "...";
        this.name = playerName;
        this._super(name, "RobotoRegular", 14 * 2 * scaleFactor);

        this._resultList = context;

        this.setScale(0.5);
        this.setColor(cc.color(0, 0, 0));
        return true;
    }
});