var News = cc.LabelTTF.extend({
    _lobby: null,
    _link: null,
    ctor: function (dataObj, lobby) {

        var newsStr = dataObj['msg'];
        this._link = dataObj['url'];
        this._super(newsStr, "RobotoRegular", 12 * 2);
        this._lobby = lobby;
        this.xOrigin = 20 + this._lobby.width + 1 - this._lobby.filterParser.miniRoom.width;
        this.setScale(0.5);
        this.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
        this.setAnchorPoint(0, 0.5);
        // this.setDimensions(cc.size(this._lobby.filterParser.miniRoom.width * 2 - 80 * scaleFactor, 100 * scaleFactor));
        this._setBoundingWidth(this._lobby.filterParser.miniRoom.width * 2 - 80 * scaleFactor);
        this.resetPosition();
        this.setName("News");
        return true;
    },
    fly: function () {
        var action = new cc.MoveBy(1, cc.p(0, 50));
        this.runAction(cc.sequence(action, cc.callFunc(function () {
            this.removeFromParent(false);
            this.resetPosition();
        }.bind(this), this)));
    },
    appear: function (parent) {
        var action = new cc.MoveBy(1, cc.p(0, 50));
        this.runAction(action);
        parent.addChild(this, 5);
    },
    resetPosition: function () {
        this.setPosition(cc.p(this.xOrigin, 70 * scaleFactor));
    },
    clickHandler: function () {
        this._link && cc.sys.openURL(this._link);
    }
});