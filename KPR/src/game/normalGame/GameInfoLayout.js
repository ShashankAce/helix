var GameInfoSprite = cc.Sprite.extend({
    _hovering: null,
    _isDummyRoom: null,
    ctor: function (spriteName, _name, room, gameRoom) {
        this._super(spriteFrameCache.getSpriteFrame("gameInfoBtn.png"));

        this.arrow = new cc.Sprite(spriteFrameCache.getSpriteFrame("arrow.png"));
        this.arrow.setPosition(cc.p(this.width + 5 * scaleFactor, this.height / 2 - 2 * scaleFactor));
        this.arrow.setScale(0.25);
        this.arrow.setRotation(270);
        this.arrow.setColor(cc.color(250, 165, 0));
        this.addChild(this.arrow, 1);

        this._isDummyRoom = false;
        if (room.getVariable("isDummyRoom") && room.getVariable("isDummyRoom").value) {
            this._isDummyRoom = true;
        }

        if (room.groupId == "gameRoom" || this._isDummyRoom)
            this.gameInfopopup = new GameInfoLayout(room);
        else
            this.gameInfopopup = new TournamentGameInfoLayout(room, gameRoom);

        this.gameInfopopup.setPosition(cc.p(this.width / 2, this.height - 50 * scaleFactor));
        this.gameInfopopup._isEnable = false;
        this.gameInfopopup.setVisible(false);
        this.addChild(this.gameInfopopup, 1);
    },
    updatePrize: function () {
        this.gameInfopopup.setPrize();
    },
    handleGameInfo: function () {
        if (this.gameInfopopup._isEnable) {
            this.arrow.setRotation(270);
            this.gameInfopopup._isEnable = false;
            this.gameInfopopup.appear(false);
        }
        else {
            if (this._isDummyRoom)
                this.gameInfopopup.updateDummyGameInfo();
            else this.gameInfopopup.updateGameInfo();

            this.arrow.setRotation(90);
            this.gameInfopopup._isEnable = true;
            this.gameInfopopup.appear(true);
        }
    },
    hidePopup: function () {
        if (this.gameInfopopup._isEnable) {
            this.arrow.setRotation(270);
            this.gameInfopopup._isEnable = false;
            this.gameInfopopup.appear(false);
        }
    }

});
var GameInfoLayout = cc.Sprite.extend({
    _name: "GameInfoLayout",
    serverRoom: null,
    blockSize: null,
    _isEnable: false,
    _hovering: null,

    ctor: function (room) {
        this._super(spriteFrameCache.getSpriteFrame("GameInfoBg.png"));
        this.setAnchorPoint(0.5, 1);
        this.setCascadeOpacityEnabled(true);
        this.setCascadeColorEnabled(true);
        this.setOpacity(0);
        this._hovering = false;
        this.serverRoom = room;
        this.tabSpace = 4;

        this.initGameInfo();
        return true;
    },
    appear: function (bool) {
        var dx = 0.3;
        var moveBy = cc.moveBy(dx, 0, 25).easing(cc.easeCubicActionOut());
        var fadeIn = cc.fadeIn(dx).easing(cc.easeCubicActionOut());
        var spawn = cc.spawn(moveBy, fadeIn);
        var spawnRev = spawn.reverse();

        if (!bool) {
            this.stopAllActions();
            this.runAction(cc.sequence(spawnRev, cc.callFunc(function () {
                this.setVisible(false);
            }.bind(this), this)));
        }
        else {
            this.stopAllActions();
            this.setVisible(true);
            this.runAction(cc.sequence(spawn, cc.callFunc(function () {
            }.bind(this), this)));
        }
    },
    changeCoordinate: function (shiftLeft) {
        var rightColumnXCordinate = 180 * scaleFactor;
        var startY = 158;
        var margin = 12 * scaleFactor;

        if (shiftLeft) {
            rightColumnXCordinate = 175 * scaleFactor;
        }
        this.firstDrop.setPosition(cc.p(rightColumnXCordinate, startY - (margin * 9)));
        this.interimDrop.setPosition(cc.p(rightColumnXCordinate, startY - (margin * 11)));
        this.printedJoker.setPosition(cc.p(rightColumnXCordinate, startY - margin));
        this.jokers.setPosition(cc.p(rightColumnXCordinate, startY - (margin * 3)));
        this.fullCount.setPosition(cc.p(rightColumnXCordinate, startY - (margin * 5)));
        this.firstHandShow.setPosition(cc.p(rightColumnXCordinate, startY - (margin * 7)));

        this.printedJokersValue.setPositionX(this.printedJoker.x + this.printedJoker.width / 2 + this.tabSpace);
        this.jokersValue.setPositionX(this.jokers.x + this.jokers.width / 2 + this.tabSpace);
        this.fullCountValue.setPositionX(this.fullCount.x + this.fullCount.width / 2 + this.tabSpace);
        this.firstHandShowValue.setPositionX(this.firstHandShow.x + this.firstHandShow.width / 2 + this.tabSpace);
        this.firstDropValue.setPositionX(this.firstDrop.x + this.firstDrop.width / 2 + this.tabSpace);
        this.interimDropValue.setPositionX(this.interimDrop.x + this.interimDrop.width / 2 + this.tabSpace);
    },
    initGameInfo: function () {
        var headerFontSize = 12.5 * scaleFactor,
            valueFontSize = 12.5 * scaleFactor,
            headerFontFamily = "RobotoBold",
            fontFamily = "RobotoRegular",
            leftColumnXCordinate = 15 * scaleFactor,
            rightColumnXCordinate = 180 * scaleFactor,
            margin = 12 * scaleFactor;
        var startY = 158;


        this.gameVariant = this.getLabel("Game Variant:", headerFontFamily, headerFontSize);
        this.gameVariant.setPosition(cc.p(leftColumnXCordinate, startY - margin));

        this.gameVariantValue = this.getLabel("", fontFamily, valueFontSize);
        this.gameVariantValue.setPosition(cc.p(100 * scaleFactor, this.gameVariant.y - 1 * scaleFactor));

        this.addChild(this.gameVariant, 1);
        this.addChild(this.gameVariantValue, 1);

        this.entryFeeOrPoint = this.getLabel("", headerFontFamily, headerFontSize);
        this.entryFeeOrPoint.setPosition(cc.p(leftColumnXCordinate, startY - (margin * 3)));

        this.entryFeeValueOrPointValue = this.getLabel("", "RupeeFordian", valueFontSize);
        this.entryFeeValueOrPointValue.setPosition(cc.p(70, this.entryFeeOrPoint.y - 1 * scaleFactor));

        this.prizeOrMinEntry = this.getLabel("", headerFontFamily, headerFontSize);
        this.prizeOrMinEntry.setPosition(cc.p(leftColumnXCordinate, startY - (margin * 5)));

        this.prizeValueOrMinEntryValue = this.getLabel("", fontFamily, valueFontSize);
        this.prizeValueOrMinEntryValue.setPosition(cc.p(50, this.prizeOrMinEntry.y - 1 * scaleFactor));

        this.addChild(this.entryFeeOrPoint, 1);
        this.addChild(this.entryFeeValueOrPointValue, 1);
        this.addChild(this.prizeOrMinEntry, 1);
        this.addChild(this.prizeValueOrMinEntryValue, 1);

        this.firstDrop = this.getLabel("", headerFontFamily, headerFontSize);
        this.firstDrop.setPosition(cc.p(rightColumnXCordinate, startY - (margin * 9)));

        this.interimDrop = this.getLabel("", headerFontFamily, headerFontSize);
        this.interimDrop.setPosition(cc.p(rightColumnXCordinate, startY - (margin * 11)));

        this.firstDropValue = this.getLabel("", fontFamily, valueFontSize);
        this.firstDropValue.setPosition(cc.p(245, this.firstDrop.y - 1 * scaleFactor));

        this.interimDropValue = this.getLabel("", fontFamily, valueFontSize);
        this.interimDropValue.setPosition(cc.p(260, this.interimDrop.y - 1 * scaleFactor));

        this.addChild(this.firstDrop, 1);
        this.addChild(this.interimDrop, 1);
        this.addChild(this.firstDropValue, 1);
        this.addChild(this.interimDropValue, 1);


        var roomType;
        if (this.serverRoom.getVariable(SFSConstants._GAMETYPE))
            roomType = this.serverRoom.getVariable(SFSConstants._GAMETYPE).value;

        var gameType = roomType.split("-")[0];
        if ((gameType == "POOL") || (gameType == "Pool")) {
            this.entryFeeOrPoint.setString("Buy-In:");
            this.entryFeeValueOrPointValue.setString("");

            this.prizeOrMinEntry.setString("Prize:");
            this.prizeValueOrMinEntryValue.setString("---");
        }
        else {
            this.entryFeeOrPoint.setString("Point Value:");
            this.entryFeeValueOrPointValue.setString("");

            this.prizeOrMinEntry.setString("Minimum Buy-In:");
            this.prizeValueOrMinEntryValue.setString("---");
        }

        this.entryFeeValueOrPointValue.setPositionX(this.entryFeeOrPoint.x + this.entryFeeOrPoint.width / 2 + this.tabSpace);
        this.prizeValueOrMinEntryValue.setPositionX(this.prizeOrMinEntry.x + this.prizeOrMinEntry.width / 2 + this.tabSpace);

        if ((roomType.split("-")[1] != "Best of 3") && (roomType.split("-")[1] != "Best of 2") && (roomType.split("-")[1] != "Best of 6")) {
            this.firstDrop.setString("First Drop:");
            this.firstDropValue.setString("20 Points");

            this.interimDrop.setString("Interim Drop:");
            this.interimDropValue.setString("40 Points");
        }

        this.turnTime = this.getLabel("Turn Time:", headerFontFamily, headerFontSize);
        this.turnTime.setPosition(cc.p(leftColumnXCordinate, startY - (margin * 7)));

        this.extraTimer = this.getLabel("Extra Timer:", headerFontFamily, headerFontSize);
        this.extraTimer.setPosition(cc.p(leftColumnXCordinate, startY - (margin * 9)));

        this.noOfDecks = this.getLabel("Number of Decks:", headerFontFamily, headerFontSize);
        this.noOfDecks.setPosition(cc.p(leftColumnXCordinate, startY - (margin * 11)));

        this.printedJoker = this.getLabel("Printed Jokers:", headerFontFamily, headerFontSize);
        this.printedJoker.setPosition(cc.p(rightColumnXCordinate, startY - margin));

        this.jokers = this.getLabel("Jokers:", headerFontFamily, headerFontSize);
        this.jokers.setPosition(cc.p(rightColumnXCordinate, startY - (margin * 3)));

        this.fullCount = this.getLabel("Full Count:", headerFontFamily, headerFontSize);
        this.fullCount.setPosition(cc.p(rightColumnXCordinate, startY - (margin * 5)));

        this.firstHandShow = this.getLabel("First hand Show:", headerFontFamily, headerFontSize);
        this.firstHandShow.setPosition(cc.p(rightColumnXCordinate, startY - (margin * 7)));

        this.turnTimeValue = this.getLabel("40(Normal)", fontFamily, valueFontSize);
        this.turnTimeValue.setPosition(cc.p(this.turnTime.x + this.turnTime.width / 2 + this.tabSpace, this.turnTime.y - 1 * scaleFactor));

        this.extraTimerValue = this.getLabel("60 sec", fontFamily, valueFontSize);
        this.extraTimerValue.setPosition(cc.p(this.extraTimer.x + this.extraTimer.width / 2 + this.tabSpace, this.extraTimer.y - 1 * scaleFactor));

        this.decksValue = this.getLabel("2 decks", fontFamily, valueFontSize);
        this.decksValue.setPosition(cc.p(this.noOfDecks.x + this.noOfDecks.width / 2 + this.tabSpace, this.noOfDecks.y - 1 * scaleFactor));

        this.printedJokersValue = this.getLabel("1 per deck", fontFamily, valueFontSize);
        this.printedJokersValue.setPosition(cc.p(this.printedJoker.x + this.printedJoker.width / 2 + this.tabSpace, this.printedJoker.y - 1 * scaleFactor));

        this.jokersValue = this.getLabel("Printed & Wild Card", fontFamily, valueFontSize);
        this.jokersValue.setPosition(cc.p(this.jokers.x + this.jokers.width / 2 + this.tabSpace/*2.5*this.tabSpace*/, this.jokers.y - 1 * scaleFactor));

        this.fullCountValue = this.getLabel("80 points", fontFamily, valueFontSize);
        this.fullCountValue.setPosition(cc.p(this.fullCount.x + this.fullCount.width / 2 + this.tabSpace, this.fullCount.y - 1 * scaleFactor));

        this.firstHandShowValue = this.getLabel("40 Points", fontFamily, valueFontSize);
        this.firstHandShowValue.setPosition(cc.p(this.firstHandShow.x + this.firstHandShow.width / 2 + this.tabSpace, this.firstHandShow.y - 1 * scaleFactor));

        this.addChild(this.turnTime, 1);
        this.addChild(this.extraTimer, 1);
        this.addChild(this.noOfDecks, 1);
        this.addChild(this.printedJoker, 1);
        this.addChild(this.jokers, 1);
        this.addChild(this.fullCount, 1);
        this.addChild(this.firstHandShow, 1);
        this.addChild(this.turnTimeValue, 1);
        this.addChild(this.extraTimerValue, 1);
        this.addChild(this.decksValue, 1);
        this.addChild(this.printedJokersValue, 1);
        this.addChild(this.fullCountValue, 1);
        this.addChild(this.firstHandShowValue, 1);
        this.addChild(this.jokersValue, 1);

        this.updateGameInfo();
    },
    getLabel: function (labelString, fontFamily, fontSize) {
        var label;
        if (fontFamily == "RobotoBold")
            label = new cc.LabelTTF(labelString, fontFamily, fontSize * 2);
        else
            label = new cc.LabelTTF(labelString, fontFamily, fontSize * 2);

        label.setScale(0.5);
        label.setColor(cc.color(0, 0, 0));
        label.setAnchorPoint(0, 1);

        return label;
    },
    updateGameInfo: function () {
        var roomType;
        if (this.serverRoom.getVariable(SFSConstants._GAMETYPE))
            roomType = this.serverRoom.getVariable(SFSConstants._GAMETYPE).value;


        if ((roomType.split("-")[0] == "POOL") || (roomType.split("-")[0] == "Pool")) {

            var fee;
            if (this.serverRoom.getVariable(SFSConstants._ENTRYFEE))
                fee = this.serverRoom.getVariable(SFSConstants._ENTRYFEE).value;
            this.entryFeeValueOrPointValue.setString(fee);

            var numOfCard;
            if (this.serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD))
                numOfCard = this.serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;

            if (numOfCard == 13)
                this.jokersValue.setString("Printed & Wild Card");
            else {
                this.jokersValue.setString("Up, Down, Printed & Wild Card");
                this.changeCoordinate(true);
            }
        }
        else {
            var betAmt = "";
            if (this.serverRoom.getVariable(SFSConstants._BET_AMOUNT))
                betAmt = this.serverRoom.getVariable(SFSConstants._BET_AMOUNT).value;

            var numC;
            if (this.serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD))
                numC = this.serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;

            this.prizeValueOrMinEntryValue.setString(betAmt);
            if (roomType.split("-")[1] == "NoJoker")
                this.printedJokersValue.setString("Printed Joker");
            else if (numC == 21) {
                this.jokersValue.setString("Up, Down, Printed & Wild Card");
                this.changeCoordinate(true);
            }
            else
                this.jokersValue.setString("Printed & Wild Card");
        }

        this.extraTimerValue.setString("60 sec");

        var cashOrP;
        if (this.serverRoom.getVariable("cashOrPromo"))
            cashOrP = this.serverRoom.getVariable("cashOrPromo").value;

        if (cashOrP == "CASH")
            this.gameVariantValue.setString("Cash");
        else
            this.gameVariantValue.setString("Practice");

        this.printedJokersValue.setString("1 per Deck");

        var numCrd;
        if (this.serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD))
            numCrd = this.serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;

        if (numCrd == 21) {
            this.decksValue.setString("3 Deck(s)");

            this.firstDropValue.setString("30 Points");
            this.interimDropValue.setString("70 Points");

            this.fullCountValue.setString("120 Points");
            this.firstHandShowValue.setString("70");
        }
        else {
            this.firstHandShowValue.setString("40 Points");
            this.decksValue.setString("2 Deck(s)");
            this.fullCountValue.setString("80 Points");
            if (roomType.split("-")[1] == "201") {
                this.firstDropValue.setString("25 Points");
                this.interimDropValue.setString("50 Points");
            }
            else if (roomType.split("-")[1] == "Best of 3" || roomType.split("-")[1] == "Best of 2") {
                this.firstDropValue.setVisible(false);
            }
            else if (roomType.split("-")[1] == "Best of 6") {
                this.firstDropValue.setVisible(false);
                this.firstDropValue.setString("Total Hand Value");
                this.interimDropValue.setVisible(false);
                this.interimDropValue.setString("(Max 80)");
            }
            else {
                this.firstDropValue.setString("20 Points");
                this.interimDropValue.setString("40 Points");
            }
        }
        var ptValue;
        if (this.serverRoom.getVariable(SFSConstants._POINTVALUE))
            ptValue = this.serverRoom.getVariable(SFSConstants._POINTVALUE).value;
        if (ptValue && ptValue != '' && ptValue != undefined) {
            this.entryFeeValueOrPointValue.setString(ptValue);
        }
    },
    updateDummyGameInfo: function () {
        var roomType;
        if (this.serverRoom.getVariable(SFSConstants._GAMETYPE))
            roomType = this.serverRoom.getVariable(SFSConstants._GAMETYPE).value;

        if ((roomType.split("-")[0] == "POOL") || (roomType.split("-")[0] == "Pool")) {
            var fee;
            if (this.serverRoom.getVariable(SFSConstants._ENTRYFEE))
                fee = this.serverRoom.getVariable(SFSConstants._ENTRYFEE).value;
            this.entryFeeValueOrPointValue.setString(fee);

            var numOfCard;
            if (this.serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD))
                numOfCard = this.serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;
            if (numOfCard == 13)
                this.jokersValue.setString("Printed & Wild Card");
            else {
                this.jokersValue.setString("Up, Down, Printed & Wild Card");
                this.changeCoordinate(true);
            }
        }
        else {
            var betAmt = "";
            if (this.serverRoom.getVariable(SFSConstants._BET_AMOUNT))
                betAmt = this.serverRoom.getVariable(SFSConstants._BET_AMOUNT).value;
            this.prizeValueOrMinEntryValue.setString(betAmt);

            var numC;
            if (this.serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD))
                numC = this.serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;

            if (roomType.split("-")[1] == "NoJoker")
                this.printedJokersValue.setString("Printed Joker");
            else if (numC == 21) {
                this.jokersValue.setString("Up, Down, Printed & Wild Card");
                this.changeCoordinate(true);
            }
            else
                this.jokersValue.setString("Printed & Wild Card");
        }

        this.extraTimerValue.setString("60 sec");

        var cashOrP;
        if (this.serverRoom.getVariable("cashOrPromo"))
            cashOrP = this.serverRoom.getVariable("cashOrPromo").value;
        if (cashOrP == "CASH")
            this.gameVariantValue.setString("Cash");
        else
            this.gameVariantValue.setString("Practice");

        this.printedJokersValue.setString("1 per Deck");
        var numCrd;
        if (this.serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD))
            numCrd = this.serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;

        if (numCrd == 21) {
            this.decksValue.setString("3 Deck(s)");

            this.firstDropValue.setString("30 Points");
            this.interimDropValue.setString("70 Points");

            this.fullCountValue.setString("120 Points");
            this.firstHandShowValue.setString("70");
        }
        else {

            this.firstHandShowValue.setString("40 Points");
            this.decksValue.setString("2 Deck(s)");
            this.fullCountValue.setString("80 Points");
            if (roomType.split("-")[1] == "201") {
                this.firstDropValue.setString("25 Points");
                this.interimDropValue.setString("50 Points");
            }

            else if (roomType.split("-")[1] == "Best of 3" || roomType.split("-")[1] == "Best of 2") {
                this.firstDropValue.setVisible(false);
            }
            else if (roomType.split("-")[1] == "Best of 6") {
                this.firstDropValue.setVisible(false);
                this.firstDropValue.setString("Total Hand Value");
                this.interimDropValue.setVisible(false);
                this.interimDropValue.setString("(Max 80)");
            }
            else {
                this.firstDropValue.setString("20 Points");
                this.interimDropValue.setString("40 Points");
            }
        }
        var ptValue;
        if (this.serverRoom.getVariable(SFSConstants._POINTVALUE))
            ptValue = this.serverRoom.getVariable(SFSConstants._POINTVALUE).value;
        if (ptValue && ptValue != '' && ptValue != undefined) {
            this.entryFeeValueOrPointValue.setString(ptValue);
        }
    },
    hover: function () {

    },
    setPrize: function () {
        var win;
        if (this.serverRoom.getVariable(SFSConstants._WINNING))
            win = this.serverRoom.getVariable(SFSConstants._WINNING).value;

        this.prizeValueOrMinEntryValue.setString(win);
    },
    setTurnTime: function (time) {
        var timeVar;
        if (this.serverRoom.getVariable(SFSConstants._TURNTYPE))
            timeVar = this.serverRoom.getVariable(SFSConstants._TURNTYPE).value;

        this.turnTimeValue.setString(time + " ( " + timeVar + " )");
    }
});