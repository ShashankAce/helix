/**
 * Created by stpl on 9/26/2016.
 */
var LobbyEventDispatcher = cc.Class.extend({
    _gameInterface: null,
    ctor: function (context) {
        this._gameInterface = context;
    },
    dispatchSFSEvents: function (cmd, room, params) {
        var str;
        switch (cmd) {
            case SFSConstants.CMD_UPDATE_BAL:  // not used. lobby
                this._gameInterface.updateLobbyBal(params);
                break;
            case SFSConstants.CMD_LOGRESP:  // not used. lobby
                // startGame(params);
                break;
            case SFSConstants.CMD_ROOMINFO:
                this._gameInterface.processRoomInfo(params);
                break;
            case SFSConstants.CMD_ROOMPLSUPDATE :
                this._gameInterface.updateRoomSeated(params);
                break;
            case SFSConstants.CMD_ROOM_GROUP_STR :
                str = params[SFSConstants.STR];
                this._gameInterface.updateTable(str, params["tableId"]);
                break;
            case SFSConstants.CMD_GROUP_TOT_PLS :
                str = params[SFSConstants.STR];
                this._gameInterface.updateGroupTotalPlayers(str);
                break;
            case SFSConstants.CMD_NO_ROOM:
                applicationFacade.noRoomHandler(params);
                break;
            case SFSConstants.CMD_roomPrize :
                this._gameInterface.updateRoomPrize(params);
                break;
        }
    }
});