/**
 * Created by stpl on 11/14/2016.
 */
var CardModel = cc.Sprite.extend({
    _name: null,
    _cardValue: null,
    _cardScore: null,
    _cardSuit: null,
    _isJoker: null,
    _cardName: null,
    cardVal: null,
    _cardNum: null,
    _dwnJkr: null,
    _upJkr: null,
    _isShadeEnable: null,
    _cardStr: null,

    ctor: function (name) {
        this._super(spriteFrameCache.getSpriteFrame("cardWhite.png"));
        var suitCnt;
        if (name) {

            this._name = name;
            this._cardStr = name;
            this._isJoker = false;
            this._cardName = null;
            this._cardNum = 0;
            this._dwnJkr = "";
            this._upJkr = "";
            this.cardVal = null;
            this._isShadeEnable = false;

            var nameArray = [], cardName = name, color = cc.color(0, 0, 0);
            if (name) {
                if (name.toLowerCase() == "joker") {
                    this._cardScore = 0;
                    this._cardSuit = "JK";
                    this._cardValue = "JOKER";
                    this._cardNum = 1;
                    cardName = "Joker";
                    this.cardVal = 0;
                    this._upJkr = "S" + "#" + 2;
                    this._dwnJkr = "S" + "#" + "K";
                    this._isJoker = true;

                    var cardLabel2 = new cc.Sprite(spriteFrameCache.getSpriteFrame("jokerText.png"));
                    cardLabel2.setPosition(cc.p(11 * scaleFactor, this.height / 2));
                    this.addChild(cardLabel2, 1);

                    var joker = new cc.Sprite(spriteFrameCache.getSpriteFrame(cardName.toLowerCase() + ".png"));
                    joker.setPosition(cc.p(this.width / 1.5, 57 * scaleFactor));
                    joker.setScale(0.40);
                    this.addChild(joker, 1);
                }
                else {
                    nameArray = name.split("#");
                    if (nameArray.length == 2) {
                        switch (nameArray[0]) {
                            case "C":
                                cardName = "club.png";
                                color = cc.color(0, 0, 0);
                                suitCnt = 2;
                                break;
                            case "H":
                                cardName = "heart.png";
                                color = cc.color(204, 0, 0);
                                suitCnt = 4;
                                break;
                            case "S":
                                cardName = "spade.png";
                                color = cc.color(0, 0, 0);
                                suitCnt = 1;
                                break;
                            case "D":
                                cardName = "diamond.png";
                                color = cc.color(204, 0, 0);
                                suitCnt = 3;
                                break;
                        }
                        if (nameArray[1] == "A") {
                            this._cardScore = 1;
                            this._cardNum = 1;
                            this._upJkr = "2";
                            this._dwnJkr = "K";
                        } else if (nameArray[1] == "J") {
                            this._cardScore = 11;
                            this._cardNum = 11;
                            this._upJkr = "Q";
                            this._dwnJkr = "10";
                        } else if (nameArray[1] == "Q") {
                            this._cardScore = 12;
                            this._cardNum = 12;
                            this._upJkr = "K";
                            this._dwnJkr = "J";
                        } else if (nameArray[1] == "K") {
                            this._cardScore = 13;
                            this._cardNum = 13;
                            this._upJkr = "A";
                            this._dwnJkr = "Q";
                        } else {
                            this._cardScore = parseInt(nameArray[1]);
                            this._cardNum = parseInt(nameArray[1]);
                            this._upJkr = (this._cardNum == 10 ? "J" : (this._cardNum + 1) + "");
                            this._dwnJkr = (this._cardNum == 2 ? "A" : (this._cardNum - 1) + "");
                        }
                        this._cardValue = nameArray[1];
                        this._cardSuit = nameArray[0];
                        this._upJkr = this._cardSuit + "#" + this._upJkr;
                        this._dwnJkr = this._cardSuit + "#" + this._dwnJkr;
                        this.cardVal = 13 * (suitCnt - 1) + this._cardNum;
                    }

                    var cardLabel = new cc.Sprite(spriteFrameCache.getSpriteFrame(this._cardValue + ".png"));
                    cardLabel.setColor(color);
                    cardLabel.setPosition(cc.p(15 * scaleFactor, 85 * scaleFactor));
                    this.addChild(cardLabel, 1);

                    var smallSuit = new cc.Sprite(spriteFrameCache.getSpriteFrame(cardName));
                    smallSuit.setPosition(cc.p(16 * scaleFactor, 57 * scaleFactor));
                    smallSuit.setScale(0.55);
                    this.addChild(smallSuit, 1);

                    var suit = new cc.Sprite(spriteFrameCache.getSpriteFrame(cardName));
                    suit.setScale(0.88);
                    suit.setPosition(cc.p(50 * scaleFactor, 30 * scaleFactor));
                    this.addChild(suit, 1);
                }
            } else {
                throw "Card name can not be null";
            }
        }
    },
    setDirtyCard: function (bool) {
        this._isShadeEnable = bool;
        bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("cardDirty.png")) : this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("cardWhite.png"));
    },
    setJoker: function (wildCrd, _jokerSymVisble) {

        var jokerSym;
        if (!wildCrd || this._cardStr == "Joker")
            return;

        if (wildCrd == "Joker") // K-A-2// up down normal
        {
            if (this._cardStr.indexOf("A") != -1) //joker  frame no 3
            {
                this._isJoker = true;
                jokerSym = new cc.Sprite(spriteFrameCache.getSpriteFrame("21Joker.png"));
                jokerSym.setPosition(cc.p(17 * scaleFactor, 19 * scaleFactor));
            }
            if (this._cardStr == "S#2") //upjoker  frame no 1
            {
                this._isJoker = true;
                jokerSym = new cc.Sprite(spriteFrameCache.getSpriteFrame("upArrowJoker.png"));
                jokerSym.setPosition(cc.p(17 * scaleFactor, 24 * scaleFactor));
            }
            if (this._cardStr == "S#K") //dwnjoker  frame no 2
            {
                this._isJoker = true;
                jokerSym = new cc.Sprite(spriteFrameCache.getSpriteFrame("downArrowJoker.png"));
                jokerSym.setPosition(cc.p(17 * scaleFactor, 24 * scaleFactor));
            }

            if (jokerSym && _jokerSymVisble) {
                this.addChild(jokerSym, 1);
                jokerSym.setScale(0.98);
            }

            return;
        }

        var crdArr = wildCrd.split("#");
        var suitStr = crdArr[0];
        var cardNo = crdArr[1];
        var upJ;
        var dwnJ;

        if (this._cardStr.indexOf(cardNo) != -1) {
            this._isJoker = true;
            jokerSym = new cc.Sprite(spriteFrameCache.getSpriteFrame("21Joker.png"));
            jokerSym.setPosition(cc.p(17 * scaleFactor, 19 * scaleFactor));
        }
        if (this._cardStr.indexOf(suitStr) != -1) {
            if (cardNo == "A") {
                upJ = suitStr + "#" + "2";
                dwnJ = suitStr + "#" + "K";
            }
            else if (cardNo == "J") {
                upJ = suitStr + "#" + "Q";
                dwnJ = suitStr + "#" + "10";
            }
            else if (cardNo == "Q") {
                upJ = suitStr + "#" + "K";
                dwnJ = suitStr + "#" + "J";

            }
            else if (cardNo == "K") {
                upJ = suitStr + "#" + "A";
                dwnJ = suitStr + "#" + "Q";
            }
            else {
                var no = parseInt(cardNo);
                upJ = suitStr + "#" + (no == 10 ? "J" : (no + 1) + "");
                dwnJ = suitStr + "#" + (no == 2 ? "A" : (no - 1) + "");
            }

            if (upJ == this._cardStr) // up joker
            {
                this._isJoker = true;
                jokerSym = new cc.Sprite(spriteFrameCache.getSpriteFrame("upArrowJoker.png"));
                jokerSym.setPosition(cc.p(17 * scaleFactor, 24 * scaleFactor));
            }
            if (dwnJ == this._cardStr) // dwn joker
            {
                this._isJoker = true;
                jokerSym = new cc.Sprite(spriteFrameCache.getSpriteFrame("downArrowJoker.png"));
                jokerSym.setPosition(cc.p(17 * scaleFactor, 24 * scaleFactor));
            }
        }

        if (jokerSym && _jokerSymVisble) {
            this.addChild(jokerSym, 1);
            jokerSym.setScale(0.98);
        }

    }
});