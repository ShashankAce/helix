/**
 * Created by stpl on 04-Apr-17.
 */
var DetailsJoinedPlayerHandler = Popup.extend({
    ctor: function (room, context) {
        this._super(new cc.Size(205 * scaleFactor, 330 * scaleFactor), context, true);

        this.headerLabel.setString("Joined Player");

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        this.closeBtn = new cc.Sprite(spriteFrameCache.getSpriteFrame("ResultClose.png"));
        this.closeBtn.setPosition(cc.p(this.width, this.height));
        this.addChild(this.closeBtn, 10, "closeBtn");
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.closeBtn);

        var node = new cc.DrawNode();
        node.drawRect(cc.p(0, 265 * scaleFactor), cc.p(this.width, 295 * scaleFactor), cc.color(222, 222, 222), 1, cc.color(222, 222, 222));
        this.addChild(node, 1);

        this.listViewHeader = new cc.LabelTTF("Player", "RobotoBold", 13 * 2);
        this.listViewHeader.setScale(0.5);
        this.listViewHeader.setPosition(cc.p(36 * scaleFactor, 280 * scaleFactor));
        this.listViewHeader.setColor(cc.color(0, 0, 0));
        this.addChild(this.listViewHeader, 2);

        this.listView = new ccui.ListView();
        this.listView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        this.listView.setGravity(ccui.ListView.GRAVITY_CENTER_HORIZONTAL);
        this.listView.setScrollBarAutoHideEnabled(false);
        this.listView.setTouchEnabled(true);
        //this.listView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        //this.listView.setBackGroundColor(cc.color(255, 255, 0));
        this.listView.setContentSize(cc.size(this.width, 265 * scaleFactor));
        this.listView.setPosition(cc.p(0, 0));
        this.addChild(this.listView, 2);

    },
    onTouchEndedCallBack: function (touch, event) {
        this._super(touch, event);
        var target = event.getCurrentTarget();
        target.getName() == "closeBtn" && this.removeFromParent(true);
    },
    getRow: function (str, index) {
        var row = new ccui.Layout();
        row.setContentSize(cc.size(this.width, 30 * scaleFactor));
        row.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        if (index % 2 == 1)
            row.setBackGroundColor(cc.color(222, 222, 222));
        else row.setBackGroundColor(cc.color(255, 255, 255));

        if (str[0].length > 15) {
            str[0] = str[0].substring(0, 15) + '..';
        }

        var text = new cc.LabelTTF(str, "RobotoRegular", 13 * 2);
        text.setScale(0.5);
        text.setAnchorPoint(0, 0.5);
        text.setPosition(cc.p(18 * scaleFactor, 15 * scaleFactor));
        text.setColor(cc.color(0, 0, 0));
        row.addChild(text, 2);

        return row;
    },
    setRow: function (str, index) {
        this.listView.pushBackCustomItem(this.getRow(str, index));
    }
});