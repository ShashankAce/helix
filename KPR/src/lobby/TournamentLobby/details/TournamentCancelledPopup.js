/**
 * Created by stpl on 1/13/2017.
 */
/**
 * Created by stpl on 1/12/2017.
 */
var TournamentCancelledPopup = Popup.extend({
    _name:"tournamentMessage",
    /**
     * @param {Object} arg
     * @param {class} context
     * @param {Boolean} overlay
     */
    ctor: function (arg, context, overlay) {
        this._super(new cc.Size(400 * scaleFactor, 180 * scaleFactor), context, overlay);
        this._gameRoom = context;

        var header = arg["header"];
        var message = arg["message"];

        this.headerLabel.setString(header);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        this.message = new cc.LabelTTF(message, "RobotoRegular", 15 * 2 * scaleFactor);
        this.message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        this.message.setScale(0.5);
        this.message.setColor(cc.color(0, 0, 0));
        this.message.setPosition(cc.p(this.width / 2, 95 * scaleFactor));
        this.addChild(this.message, 2);

        this.OkBtn = new CreateBtn("OkBtn", "OkBtnOver", "OkBtn");
        this.OkBtn.setName("LeaveYesButton");
        this.OkBtn.setPosition(this.width / 2, this.height / 2 - 40 * scaleFactor);
        this.addChild(this.OkBtn, 2);

        this.closeBtn = new cc.Sprite(spriteFrameCache.getSpriteFrame("ResultClose.png"));
        this.closeBtn.setName("closeBtn");
        this.closeBtn.setPosition(this.width - 3 * scaleFactor, this.height - 3 * scaleFactor);
        this.addChild(this.closeBtn, 3);

        cc.eventManager.addListener(this.popupMouseListener.clone(), this.OkBtn);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.OkBtn);

        return true;
    },
    setString: function (msg) {
        this.message.setString(msg);
    },
    setHeaderString: function (msg) {
        this.headerLabel.setString(msg);
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        switch (target._name) {
            case "OkBtn":
                this.removeSelf();
                break;
            case "closeBtn":
                this.removeSelf();
                break;
        }
    },
    removeSelf: function () {
        this.removeFromParent(true);
    }
});