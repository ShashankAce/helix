/**
 * Created by stpl on 11/30/2016.
 */
var ClosedDeck = cc.Node.extend({
    _name: "ClosedDeck",
    _deckPanel: null,
    _gameRoom: null,
    _isEnabled: null,
    glowCard: null,
    glowTag: 555,
    _hovering: null,
    ctor: function (context) {
        this._super();

        var deckCard = new cc.Sprite(spriteFrameCache.getSpriteFrame("deckBlue.png"));
        deckCard.setPosition(cc.p(0, -4));
        this.addChild(deckCard, 5);
        deckCard.setScale(0.67);

        this._deckPanel = context;
        this._gameRoom = this._deckPanel._gameRoom;
        this._isEnabled = false;
        this._hovering = false;

        var size = cc.winSize;
        this.blackShadow = new cc.Sprite(spriteFrameCache.getSpriteFrame("selected_state.png"));
        this.blackShadow.setScale(0.95, 0.95);
        this.blackShadow.setPosition(cc.p(2, -7));
        this.blackShadow.hover = function () {
        };
        this.addChild(this.blackShadow, 3, 100);

        var label = new cc.LabelTTF("Closed Deck", "RobotoRegular", 15 * scaleFactor);
        label.setScale(0.8);
        label.setPosition(cc.p(this.width / 2, -57 * scaleFactor));
        this.addChild(label, 1);

        this.glowCard = new cc.Sprite(spriteFrameCache.getSpriteFrame("CardGlow.png"));
        this.glowCard.setScale(1.05, 1);
        this.glowCard.setPosition(this.blackShadow.getPosition());
        this.glowCard.setVisible(false);
        this.addChild(this.glowCard, -1);

        return true;

    },
    glowEnabled: function (bool) {
        cc.log("ClosedDeckGlowActive->" + bool);
        if (bool) {
            this.glowCard.setVisible(true);
            var glowSequence = cc.sequence(cc.fadeIn(0.5), cc.fadeOut(0.5));
            glowSequence.tag = this.glowTag;
            this.glowCard.runAction(glowSequence.repeatForever());

        } else {
            this.glowCard.stopActionByTag(this.glowTag);
            this.glowCard.setVisible(false);
        }
    },
    pauseListener: function (bool) {
        this._isEnabled = !bool;
    },
    hide: function (bool) {
        this.setVisible(!bool);
        if (bool == true) {
            this._isEnabled = !bool;
        }
    },
    hover: function (bool) {
    }
});