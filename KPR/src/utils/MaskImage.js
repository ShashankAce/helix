/**
 * Created by stpl on 9/22/2016.
 */
var MaskImage = {
    _canvas: null,
    _canvasContext: null,
    processMask: function (originalpic, maskImage, callBackFunction) {

        if (originalpic && maskImage && callBackFunction && (typeof originalpic != "string") && (typeof maskImage != "string")) {
            if (!this._canvas) {
                this._canvas = document.createElement('canvas');
                this._canvasContext = this._canvas.getContext('2d');
            }
            var original = new Image();
            if (originalpic instanceof cc.Texture2D) {

                var contentSize = originalpic.getContentSize();
                this.maskOnLoad({
                    image: originalpic._htmlElementObj,
                    rect: cc.rect(0, 0, contentSize.width, contentSize.height)
                }, maskImage, callBackFunction);

            } else if ((originalpic._texture._htmlElementObj instanceof HTMLImageElement)) {

                this.maskOnLoad({
                    image: originalpic._texture._htmlElementObj,
                    rect: originalpic.getRect()
                }, maskImage, callBackFunction);

            } else if (originalpic._texture._htmlElementObj instanceof HTMLCanvasElement) {

                original.onload = function () {
                    this.maskOnLoad({
                        image: original,
                        rect: cc.rect(0, 0, original.width, original.height)
                    }, maskImage, callBackFunction);

                }.bind(this);
                original.src = originalpic._texture._htmlElementObj.toDataURL();
            }
        } else {
            if (!originalpic)throw Error("originalpic param can not be null or string");
            if (!maskImage)throw Error("maskImage param can not be null or string");
            if (!callBackFunction)throw Error("callBackFunction param can not be null");
        }

    },
    maskOnLoad: function (originalImageObj, maskImg, callBackFunction) {

        var mask = new Image();
        if (maskImg instanceof cc.Texture2D) {

            var contentSize = maskImg.getContentSize();
            this.maskOnLoad(originalImageObj, {
                image: maskImg._htmlElementObj,
                rect: cc.rect(0, 0, contentSize.width, contentSize.height)
            }, callBackFunction);

        } else if ((maskImg._texture._htmlElementObj instanceof HTMLImageElement)) {

            this.initMask(originalImageObj, {
                image: maskImg._texture._htmlElementObj,
                rect: maskImg.getRect()
            }, callBackFunction);

        } else if (maskImg._texture._htmlElementObj instanceof HTMLCanvasElement) {
            mask.onload = function () {

                this.initMask(originalImageObj, {
                    image: mask,
                    rect: cc.rect(0, 0, mask.width, mask.height)
                }, callBackFunction);

            }.bind(this);

            mask.src = maskImg._texture._htmlElementObj.toDataURL();
        }
    },
    initMask: function (originalImage, maskImage, callBackFunction) {

        var maskWidth = maskImage.rect.width;
        var maskHeight = maskImage.rect.height;

        this._canvas.width = maskWidth;
        this._canvas.height = maskHeight;
        this._canvasContext.clearRect(0, 0, maskWidth, maskHeight);

        this._canvasContext.drawImage(
            maskImage.image, // img
            maskImage.rect.x, //sx
            maskImage.rect.y, //sy
            maskWidth, // sw
            maskHeight, // sh
            0, // dx
            0, // dy
            maskWidth, //dw
            maskHeight //dh
        );

        this._canvasContext.globalCompositeOperation = 'source-atop';

        this._canvasContext.drawImage(
            originalImage.image,
            originalImage.rect.x, //sx
            originalImage.rect.y, //sy
            originalImage.rect.width,
            originalImage.rect.height,
            0,
            0,
            maskWidth,
            maskHeight
        );
        var baseData = this._canvas.toDataURL();
        var newImage = new Image();
        newImage.onload = function () {
            callBackFunction(newImage);
        };
        newImage.src = baseData;
    },
};
//img_elem, dx_or_sx, dy_or_sy, dw_or_sw, dh_or_sh, dx, dy, dw, dh
// image dx, dy, width, height
// image sx, sy ,swidth, sheight, dx, dy, dwidth, dheight