/**
 * Created by stpl on 11/27/2016.
 */
"use strict";
var NickNameLayer = cc.LayerColor.extend({
    _name: "NickNameLayer",
    _applicationMain: null,

    ctor: function (context) {
        this._super(cc.color(0, 0, 0, 180));
        this._applicationMain = context;

        var sprite = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png"), cc.rect(0, 0, 50 * scaleFactor, 50 * scaleFactor));
        sprite.setColor(cc.color(0, 0, 0));
        sprite.setContentSize(cc.size(535 * scaleFactor, 236 * scaleFactor));
        sprite.setCapInsets(cc.rect(16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor));
        sprite.setPosition(cc.p(518 * scaleFactor, 390 * scaleFactor));
        this.addChild(sprite);

        var roundSprite = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("squareBorder.png"), cc.rect(0, 0, 50 * scaleFactor, 50 * scaleFactor));
        roundSprite.setContentSize(cc.size(540 * scaleFactor, 241 * scaleFactor));
        roundSprite.setCapInsets(cc.rect(23 * scaleFactor, 23 * scaleFactor, 23 * scaleFactor, 23 * scaleFactor));
        roundSprite.setPosition(cc.p(518 * scaleFactor, 390 * scaleFactor));
        this.addChild(roundSprite);

        var text = new cc.LabelTTF("Choose Your Nickname", "RobotoRegular", 22 * 2 * scaleFactor);
        text.setScale(0.5);
        text.setPosition(cc.p(390 * scaleFactor, this._applicationMain.height - 195 * scaleFactor));
        text.setColor(cc.color(255, 255, 255));
        this.addChild(text);

        /*var TextFieldsprite = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png"), cc.rect(0, 0, 50, 50));
         TextFieldsprite.setContentSize(cc.size(330, 50));
         TextFieldsprite.setCapInsets(cc.rect(16, 16, 16, 16));
         TextFieldsprite.setPosition(cc.p(430, this._applicationMain.height - 250));
         this.addChild(TextFieldsprite, 2);*/

        this._textField = new cc.EditBox(cc.size(330 * scaleFactor, 50 * scaleFactor), new cc.Scale9Sprite("whiteBoxPopUp.png"),
            new cc.Scale9Sprite("whiteBoxPopUp.png"));

        this._textField.setDelegate(this);
        this._textField.setMaxLength(13);

        this._textField.setFont("RobotoRegular", 17);
        this._textField.setFontColor(cc.color(50, 50, 50)); // font color

        this._textField.setPlaceholderFont("RobotoRegular", 17 * scaleFactor);
        this._textField.setPlaceHolder("Enter Nickname");

        this._textField.setInputFlag(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        this._textField.setPosition(cc.p(430, this._applicationMain.height - 250 * scaleFactor));
        this._textField._edTxt.style.paddingLeft = "10px";
        this.addChild(this._textField, 4);

        this.submitBtn = this.getButton("Submit.png", "SubmitOver.png");
        this.submitBtn.setName("submitButton");
        this.submitBtn.setPosition(cc.p(430, this._applicationMain.height - 320 * scaleFactor));
        this.submitBtn.setVisible(true);
        this.addChild(this.submitBtn);

        var profile = new cc.Sprite(spriteFrameCache.getSpriteFrame("nickNamePic.png"));
        profile.setPosition(cc.p(680 * scaleFactor, 380 * scaleFactor));
        this.addChild(profile, 2);

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                cc.log("Blank listener in NickNameLayer ");
                return true;
            }.bind(this)
        }, this);

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        return true;
    },
    editBoxTextChanged: function (sender, value) {
        //on input
        /*if (!/^[0-9]+$/.test(value)) {
         sender._edTxt.value = value.slice(0, value.length - 1);
         }*/
    },
    editBoxReturn: function (sender, e) {
        //keyboard
        if (e.keyCode === cc.KEY.enter) {
            this.submitBtnHandler();
        }
    },
    editBoxEditingDidBegin: function (sender) {
        //focus
        sender._edTxt.focus();
        this.showInvalidMessage(false);
    },
    getButton: function (spriteNormal, spriteHover) {
        var Button = cc.Sprite.extend({
            _spriteNormal: null,
            _spriteHover: null,
            _hovering: null,
            ctor: function (spriteNormal, spriteHover) {
                this._super(spriteFrameCache.getSpriteFrame(spriteNormal));
                this._hovering = false;
                this._spriteNormal = spriteNormal;
                this._spriteHover = spriteHover;
                this._isEnabled = true;
                return true;
            },
            hover: function (bool) {
                bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this._spriteHover)) :
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this._spriteNormal));
            },
            hide: function () {
                this._isEnabled = false;
                this.setVisible(false);
            }
        });
        return new Button(spriteNormal, spriteHover);
    },
    initMouseListener: function () {
        this.nickNameMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            swallowTouches: true,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    target._isEnabled && !target._hovering && target.hover(true);
                    target._hovering = true;
                    return true;
                }
                target._isEnabled && target._hovering && target.hover(false);
                target._hovering = false;
                return false;
            }.bind(this)
        });
        this.nickNameMouseListener.retain();
        cc.eventManager.addListener(this.nickNameMouseListener.clone(), this.submitBtn);
    },
    initTouchListener: function () {
        this.nickNameTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                switch (target.getName()) {
                    case "submitButton" :
                        if (target._isEnabled)
                            this.submitBtnHandler();
                        break;
                }
            }.bind(this)
        });
        this.nickNameTouchListener.retain();
        cc.eventManager.addListener(this.nickNameTouchListener.clone(), this.submitBtn);
    },
    submitBtnHandler: function () {
        var _enteredNickName = this._textField._edTxt.value;
        _enteredNickName = _enteredNickName.replace(/^\s+|\s+$/g, '');

        if ((_enteredNickName != "") && (_enteredNickName != "Enter Nickname"))
            this.sendNickNameReq(_enteredNickName);
        else {
            this._textField.setString("");
        }
    },
    sendNickNameReq: function (nickName) {

        var _enteredNickName = nickName;
        if (this.validateNickname(_enteredNickName)) {
            var reqURLStr = connectionConfig.webServerPath + "saveNickName.action?playerId="
                + rummyData.playerId + "&nickName=" + _enteredNickName + "&token="
                + rummyData.token;

            var xhr = cc.loader.getXMLHttpRequest();
            xhr.open("POST", reqURLStr);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && ( xhr.status >= 200 && xhr.status <= 207 )) {
                    try {
                        var resp = xhr.responseText;
                        if (resp == "error") {
                            this.showInvalidMessage(true, "Nickname already\nExists");
                        }
                        else if (resp == "sessionInvalid") {
                            applicationFacade.logoutHandler();
                            this.removeFromParent(true);
                        }
                        else {
                            rummyData.nickName = resp;
                            this._applicationMain.initGame(rummyData);
                            this.removeFromParent(true);
                        }

                    }
                    catch (exception) {
                        cc.log("Code Exception :" + exception + "\nResponce :" + xhr.responseText);
                    }
                } else {
                    cc.log(xhr.status);
                }
            }.bind(this);

            xhr.onerror = function () {
                cc.log("Connection Error :" + xhr.status);
            };
            xhr.send();

        } else
            this.showInvalidMessage(true, "Invalid NickName");

    },
    showInvalidMessage: function (bool, msg) {
        if (bool) {
            this.errorMsg && this.errorMsg.removeFromParent(true);
            this.errorMsg = null;
            this.errorMsg = this.getInvalidMsg(msg);
            this.addChild(this.errorMsg, 10, "errorMsg");
        } else {
            this.errorMsg && this.errorMsg.removeFromParent(true);
            this.errorMsg = null;
        }
    },
    getInvalidMsg: function (msg) {
        var error = cc.Scale9Sprite.extend({
            ctor: function (msg) {
                this._super(spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png"), cc.rect(0, 0, 50 * scaleFactor, 50 * scaleFactor));

                this.setColor(cc.color(255, 20, 0));
                this.setCascadeColorEnabled(false);
                this.setCapInsets(cc.rect(16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor));
                this.setContentSize(cc.size(140 * scaleFactor, 41 * scaleFactor));
                this.setPosition(cc.p(185, cc.winSize.height - 250 * scaleFactor));

                var arrow = new cc.Sprite(spriteFrameCache.getSpriteFrame("arrow.png"));
                arrow.setAnchorPoint(0.5, 0.5);
                arrow.setPosition(cc.p(this.width + 2 * scaleFactor, this.height / 2));
                arrow.setScale(0.65);
                arrow.setRotation(180);
                arrow.setColor(cc.color(255, 20, 0));
                this.addChild(arrow, 10);

                var msg = new cc.LabelTTF(msg, "RobotoRegular", 20 * scaleFactor);
                msg.setScale(0.7);
                msg._setBoundingWidth(this.width * 1.5 - 35 * scaleFactor);
                msg.setAnchorPoint(0, 0.5);
                msg.setPosition(cc.p(10 * scaleFactor, this.height / 2));
                this.addChild(msg);

            }
        });
        return new error(msg);
    },
    validateNickname: function (str) {
        var pattern = /^[a-zA-Z][a-zA-Z0-9]+$/;
        cc.log("Nick Valid", pattern.test(str));
        if ((pattern.test(str)) && (str.length > 4))
            return true;
        else
            return false;
    },
    remove: function () {
        this.removeFromParent(true);
    }
});