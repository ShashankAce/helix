/**
 * Created by stpl on 9/16/2016.
 */
var MiniRoom = cc.LayerColor.extend({
    _name: "miniRoom",
    _playerArray: null,
    _chairPosition: null,
    roomBar: null,
    _lobby: null,
    _isEnabled: null,

    ctor: function (lobby) {

        this._super(cc.color(0, 0, 0));
        this._lobby = lobby;
        this._isEnabled = false;

        this._playerArray = [];

        var miniRoomSize = cc.size(241 * scaleFactor, 455 * scaleFactor);
        this.setContentSize(miniRoomSize);
        this.setAnchorPoint(0, 0);

        var tableTopMargin = 20 * scaleFactor;
        var miniTablePosition = cc.p(this.width / 2, this.height - (160 * scaleFactor));
        var instantPlayPosition = cc.p(this.width / 2, 180 * scaleFactor);

        this.miniTable = new cc.Sprite(spriteFrameCache.getSpriteFrame("MiniTable.png"));
        this.miniTable.texture.setAntiAliasTexParameters(false);
        this.miniTable.setPosition(miniTablePosition);
        this.addChild(this.miniTable);

        this.instantPlayButton = new InstantPlay(instantPlayPosition);
        this.addChild(this.instantPlayButton);

        this._chairPosition = {
            2: [cc.p(this.miniTable.x, this.miniTable.y + this.miniTable.height / 2 + 10 * scaleFactor),
                cc.p(this.miniTable.x, this.miniTable.y - this.miniTable.height / 2)],

            4: [cc.p(this.miniTable.x + this.miniTable.width / 2 - 10 * scaleFactor, this.miniTable.y),
                cc.p(this.miniTable.x, this.miniTable.y + this.miniTable.height / 2 + 10 * scaleFactor),
                cc.p(this.miniTable.x - this.miniTable.width / 2 + 10 * scaleFactor, this.miniTable.y),
                cc.p(this.miniTable.x, this.miniTable.y - this.miniTable.height / 2)],

            6: [cc.p(this.miniTable.x + 3 * this.miniTable.width / 8, this.miniTable.y - 3 * this.miniTable.height / 8),
                cc.p(this.miniTable.x + 3 * this.miniTable.width / 8, this.miniTable.y + 3 * this.miniTable.height / 8),
                cc.p(this.miniTable.x, this.miniTable.y + this.miniTable.height / 2 + 10 * scaleFactor),
                cc.p(this.miniTable.x - 3 * this.miniTable.width / 8, this.miniTable.y + 3 * this.miniTable.height / 8),
                cc.p(this.miniTable.x - 3 * this.miniTable.width / 8, this.miniTable.y - 3 * this.miniTable.height / 8),
                cc.p(this.miniTable.x, this.miniTable.y - this.miniTable.height / 2)],
        };

        var pos = cc.p(20 * scaleFactor, 425 * scaleFactor);
        this.gameInfoDetail = new cc.LabelTTF("", "RupeeFordian", 18);
        this.gameInfoDetail.setScale(0.62, 0.65);
        this.gameInfoDetail.setAnchorPoint(0, 0.5);
        this.gameInfoDetail.setPosition(pos.x, pos.y);
        this.addChild(this.gameInfoDetail);

        var gameInfoDivider = new cc.DrawNode();
        gameInfoDivider.drawSegment(cc.p(pos.x, pos.y - 15), cc.p(pos.x + this.width - pos.x * 2, pos.y - 15), 0.5, cc.color(100, 100, 100));
        this.addChild(gameInfoDivider);

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    applicationFacade._controlAndDisplay.closeAllTab();
                    applicationFacade._lobby.closeAllTab();
                    cc.log("Blank listener in MiniRoom");
                    return true;
                }
                return false;
            }.bind(this)
        }, this);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        /*cc.eventManager.addListener({
         event: cc.EventListener.KEYBOARD,
         onKeyPressed: function (keyCode, event) {
         alert("NO");
         },
         onKeyReleased: function (keyCode, event) {
         alert("NO");
         }
         }, this);*/

        return true;
    },
    initMouseListener: function (element) {
        this.miniRoomMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            swallowTouches: true,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode) && applicationFacade._activeTableId == 0) {
                    if ((target.getName() == "Player" && this._isEnabled) || target.getName() == "InstantPlay") {
                        !target._hovering && target.hover(true);
                        target._hovering = true;
                        return true;
                    }
                }
                target._hovering && target.hover(false);
                target._hovering = false;
                return false;
            }.bind(this)
        });
        this.miniRoomMouseListener.retain();
        cc.eventManager.addListener(this.miniRoomMouseListener, this.instantPlayButton);
    },
    initTouchListener: function () {
        this.miniRoomTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode) && applicationFacade._activeTableId == 0) {
                    if ((target.getName() == "Player" && this._isEnabled) || target.getName() == "InstantPlay") {
                        return true;
                    }
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                applicationFacade._controlAndDisplay.closeAllTab();
                applicationFacade._lobby.closeAllTab();
                switch (target.getName()) {
                    case "Player" :
                        !target._sittingStatus &&
                        this._joinRoom(target._seatPosition);
                        break;
                    case "InstantPlay" :
                        this.instantPlayHandler();
                        break;
                }
            }.bind(this)
        });
        this.miniRoomTouchListener.retain();
        cc.eventManager.addListener(this.miniRoomTouchListener.clone(), this.instantPlayButton);
    },
    initMiniRoom: function (roomBar) {
        this.roomBar = roomBar;
        this._isEnabled = true;
        var pos = this.miniTable.getPosition();
        var gameType = roomBar.gameType.split("-"), gameTypeStr, gameInfoStr;

        if (gameType[1].indexOf("Best") > -1)
            gameTypeStr = gameType[1];
        else
            gameTypeStr = gameType[0] + ' ' + gameType[1];

        if (roomBar.gameType.indexOf("Point") >= 0)
            gameInfoStr = gameTypeStr + " | " + "`" + roomBar.betAmount + " | " + roomBar.turnType + " Turn";
        else
            gameInfoStr = gameTypeStr + " | " + "`" + roomBar.entryFee + " | " + roomBar.turnType + " Turn";
        this.gameInfoDetail.setString(gameInfoStr);

        this.tableId && this.tableId.removeFromParent(true);
        this.tableId = new cc.LabelTTF("Table ID: " + roomBar.roomId, "RobotoRegular", 12 * 2);
        this.tableId.setScale(0.5);
        this.tableId.setColor(cc.color(255, 240, 0));
        this.tableId.setPosition(cc.p(pos.x, pos.y + this.tableId.height / 3));
        this.addChild(this.tableId);

        this.waitingPlayers && this.waitingPlayers.removeFromParent(true);
        this.waitingPlayers = new cc.LabelTTF(roomBar.totalPlayers + " player(s) waiting", "RobotoRegular", 11 * 2);
        this.waitingPlayers.setScale(0.5);
        this.waitingPlayers.setColor(cc.color(230, 255, 230));
        this.waitingPlayers.setPosition(cc.p(pos.x, pos.y - this.waitingPlayers.height / 4));
        this.addChild(this.waitingPlayers);
        this.arrangeSeats(this.roomBar.maxPlayers);
        this.allotSeat(roomBar);

    },
    allotSeat: function (roomBar) {

        if ((roomBar.gameType.indexOf("Point") >= 0 &&
            (roomBar.totalPlayers != 0 || roomBar.groupTotalPlayers != 0)) ||
            roomBar.totalPlayers != 0) {

            var seatedPlayerString = roomBar.seatedPls || roomBar.seatedPLayers; //"3:1424:meghanshi,6:1119:alphaA"
            if (!seatedPlayerString || seatedPlayerString == '')
                return;

            var playerArray = seatedPlayerString && seatedPlayerString.split(",");
            if (!Array.isArray(playerArray))
                return;

            for (var j = 0; j < this._playerArray.length; j++) {
                this._playerArray[j].leaveSeat();
            }
            var singPlayerDataArray;
            for (var i = 0; i < playerArray.length; i++) {
                singPlayerDataArray = playerArray[i].split(":");
                this._playerArray[parseInt(singPlayerDataArray[0]) > 0 && parseInt(singPlayerDataArray[0]) - 1].takeSeat(singPlayerDataArray[1], singPlayerDataArray[2]);
                if (singPlayerDataArray[4]) {
                    var isWaiting = singPlayerDataArray[4] == "true";
                    this._playerArray[parseInt(singPlayerDataArray[0]) > 0 && parseInt(singPlayerDataArray[0]) - 1].showWaiting(isWaiting);
                } else {
                    this._playerArray[parseInt(singPlayerDataArray[0]) > 0 && parseInt(singPlayerDataArray[0]) - 1].showWaiting(false);
                }
            }
            for (var j = 0; j < playerArray.length; j++) {
                singPlayerDataArray = playerArray[j].split(":");
                if (singPlayerDataArray[2] == rummyData.nickName) {
                    this.pauseListener(true);
                    break;
                }
            }
        } else {
            for (var j = 0; j < this._playerArray.length; j++) {
                this._playerArray[j].leaveSeat();
            }
        }
    },
    arrangeSeats: function (noOfPlayer) {

        if (noOfPlayer % 2 == 0) {
            this.getChair(noOfPlayer);

            for (var i = 0; i < this._playerArray.length; i++) {
                this._playerArray[i].setPosition(this._chairPosition[noOfPlayer][i]);
                this.addChild(this._playerArray[i]);
                cc.eventManager.addListener(this.miniRoomMouseListener.clone(), this._playerArray[i]);
                cc.eventManager.addListener(this.miniRoomTouchListener.clone(), this._playerArray[i]);
            }
        }
    },
    getChair: function (noOfPlayer) {

        for (var i = 0; i < this._playerArray.length; i++) {
            cc.eventManager.removeListener(this._playerArray[i], true);
            this._playerArray[i].removeFromParent(true);
        }
        this._playerArray = [];
        for (var index = 0; index < noOfPlayer; index++) {
            this._playerArray.push(new MiniPlayer(index));
        }

    },
    _joinRoom: function (seatId) {
        if (Object.keys(applicationFacade._gameMap).length == 3) {
            if (!applicationFacade.maxJoinTablePopup) {
                applicationFacade.maxJoinTablePopup = new MaxJoinTablePopup(this);
                applicationFacade.addChild(applicationFacade.maxJoinTablePopup, 10, "maxJoinTablePopup");
                return;
            }
        }
        cc.log("_joinRoom request sent:", seatId);
        //var uid = sfsClient.mySelf.id;
        //cc.log(uid);
        var room = sfsClient.getRoomById(this.roomBar.tableId);
        var sentObj;
        if (room) {
            applicationFacade.joinRoomFromMiniRoom(room, seatId);
        }
        else {
            sentObj = {
                cId: this.roomBar.cId,
                seatNo: seatId
            };
            sfsClient.sendReqFromLobby(SFSConstants.CMD_ROOM_JOIN_REQUEST, sentObj);
        }
    },
    instantPlayHandler: function () {
        if (Object.keys(applicationFacade._gameMap).length == 3) {
            if (!applicationFacade.maxJoinTablePopup) {
                applicationFacade.maxJoinTablePopup = new MaxJoinTablePopup(this);
                applicationFacade.addChild(applicationFacade.maxJoinTablePopup, 10, "maxJoinTablePopup");
                return;
            }
        }
        var sfsObj = {
            "type": this.roomBar.cashOrPromo,
            "gameType": this.roomBar.gameType,
            "numOfCard": this.roomBar.numOfCard + ""
        };
        applicationFacade.instantPlayBtnHandler(sfsObj);
    },
    pauseListener: function (bool) {
        this._isEnabled = !bool;
    }

});
var MiniPlayer = cc.Sprite.extend({
    _id: null,
    _seatPosition: null,
    _sittingStatus: !1,
    _hovering: null,
    ctor: function (index) {
        this._super(spriteFrameCache.getSpriteFrame("JoinMini.png"));
        this.texture.setAliasTexParameters();
        this.setName("Player");
        this._seatPosition = index + 1;
        this._hovering = false;

        return true;
    },
    hover: function (bool) {
        if (!this._sittingStatus) {
            bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("JoinMiniOver.png")) : this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("JoinMini.png"));
        }
    },
    takeSeat: function (playerId, playerName) {
        this.seatedPlayer = new cc.Sprite(spriteFrameCache.getSpriteFrame("seatedButtonBG.png"));
        this.seatedPlayer.setPosition(cc.p(this.width / 2, this.height / 2 - 8.5));
        this.addChild(this.seatedPlayer, 10);

        this.playerName = new cc.LabelTTF(playerName, "RobotoRegular", 9 * 2);
        this.playerName.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.playerName.setDimensions(cc.size(this.seatedPlayer.width * 2 - 10 * scaleFactor, this.playerName.height));
        this.playerName.setScale(0.5);
        this.playerName.setPosition(cc.p(21 * scaleFactor, 10 * scaleFactor));
        this.seatedPlayer.addChild(this.playerName, 1);

        this._sittingStatus = !0;
        cc.eventManager.pauseTarget(this);
    },
    showWaiting: function (bool) {
        bool && this.seatedPlayer.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("miniWaiting.png"));
        !bool && this.seatedPlayer.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("seatedButtonBG.png"));
    },
    leaveSeat: function () {
        if (this._sittingStatus == !0) {
            this._sittingStatus = !1;
            cc.eventManager.resumeTarget(this);
            this.seatedPlayer.removeFromParent(true);
        }
    }
});

var InstantPlay = cc.Sprite.extend({
    _hovering: null,
    ctor: function (instantPlayPosition) {
        this._super(spriteFrameCache.getSpriteFrame("instantPlayButton.png"));
        this.setName("InstantPlay");
        this.setPosition(instantPlayPosition);
        this._hovering = false;

    },
    hover: function (bool) {
        bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("instantPlayButtonMouseOver.png")) : this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("instantPlayButton.png"));
    }
});