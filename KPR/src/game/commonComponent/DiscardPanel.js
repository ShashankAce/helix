/**
 * Created by stpl on 11/26/2016.
 */
"use strict";
var DiscardPanel = cc.Scale9Sprite.extend({
    _player: null,
    _gameRoom: null,
    _plCardObjArr: null,
    _cardStr: null,
    curIndex: null,
    pointer: null,
    maxDscrdLen: null,
    DisCards: null,
    cardArray: null,
    userNameArray: null,

    ctor: function (context) {
        this._super(spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png"), cc.rect(0, 0, 50 * scaleFactor, 50 * scaleFactor), cc.rect(20 * scaleFactor, 20 * scaleFactor, 20 * scaleFactor, 20 * scaleFactor));

        this._gameRoom = context;

        this._cardStr = "";
        this.curIndex = 0;
        this.pointer = 0;
        this.maxDscrdLen = 0;
        this.DisCards = [];
        this.cardArray = [];
        this.userNameArray = [];


        this.setAnchorPoint(1, 1);
        this.setColor(cc.color(0, 0, 0));
        this.setCascadeColorEnabled(false);
        this.setCascadeOpacityEnabled(false);
        this.setOpacity(150);
        this.setLocalZOrder(2);

        this.discardToolTip = new ToolTip(cc.size(150 * scaleFactor, 46 * scaleFactor), cc.color(255, 238, 215));
        this.discardToolTip.setVisible(false);
        this.discardToolTip.setAnchorPoint(1, 1);
        this.discardToolTip.setPosition(cc.p(0, 0));
        this.addChild(this.discardToolTip, 10, "discardToolTip");

        this.upArrow = new cc.Sprite(spriteFrameCache.getSpriteFrame("discardUpArrow.png"));
        this.upArrow.setName("ArrowUp");
        this.upArrow.setPosition(cc.p(this.width - 10 * scaleFactor, 12 * scaleFactor));
        this.upArrow.hover = function () {
        };
        this.addChild(this.upArrow, 2);

        this.downArrow = new cc.Sprite(spriteFrameCache.getSpriteFrame("discardUpArrow.png"));
        this.downArrow.setName("ArrowDown");
        this.downArrow.setPosition(cc.p(this.width + 17 * scaleFactor, 12 * scaleFactor));
        this.downArrow.setRotation(180);
        this.downArrow.hover = function () {
        };
        this.addChild(this.downArrow, 2);

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        return true;
    },
    initDiscardPanel: function (string) {
        if (string) {
            this._cardStr = string;
            this.discardPanelResponseHandler(string);
            this.discardToolTip.setVisible(false);

            if (this.maxDscrdLen < 4) {
                this.downArrow.setVisible(false);
                this.upArrow.setVisible(false);
            }
            else {
                this.downArrow.setVisible(true);
                this.upArrow.setVisible(true);
            }
        }
    },
    /*
     * @param {string} str
     * */
    discardPanelResponseHandler: function (playerStr) {

        var playerArray = playerStr.split(";");

        var i = playerArray.length;
        this.setContentSize(cc.size(41 * i * scaleFactor, 152 * scaleFactor));

        this.initPanel(playerArray);
        this.addCardOnPanel();
    },
    initPanel: function (playerArray) {

        for (var b = 0; b < this.userNameArray.length; b++) {
            this.userNameArray[b].removeFromParent(true);
        }
        this.userNameArray = [];

        for (var a = 0; a < playerArray.length; a++) {
            var name = new cc.LabelTTF(playerArray[a].split(":")[0].substr(0, 5), "RobotoRegular", 13 * scaleFactor);
            name.setScale(0.8);
            name.setPosition(cc.p(((a + 0.7) * ((this.width / playerArray.length) - 5 * scaleFactor)), this.height - 15 * scaleFactor));
            name.setName("UserName");
            this.addChild(name);

            this.userNameArray.push(name);
        }
    },
    addCardOnPanel: function () {

        var playerArray = this._cardStr.split(";");
        var strtLen = 0;
        var k = 0;
        var checkIdx = 0;
        var infoArr = [];
        this.maxDscrdLen = 0;
        for (var i = 0; i < this.cardArray.length; i++) {
            this.cardArray[i].removeFromParent(true);
        }
        this.cardArray = [];

        for (var r = 0; r < playerArray.length; r++) {
            var cardStrR = playerArray[r].split(":")[1];
            var cardArrR = cardStrR.split(",");
            this.maxDscrdLen = Math.max(this.maxDscrdLen, cardArrR.length);
        }

        for (var a = 0; a < playerArray.length; a++) {
            var cardStr = playerArray[a].split(":")[1];
            var cardArr = cardStr.split(",");
            //this.maxDscrdLen = Math.max(this.maxDscrdLen, cardArr.length);
            this.curIndex = (this.maxDscrdLen - 1);
            if (this.maxDscrdLen > 2)
                strtLen = 2;
            else
                strtLen = (this.maxDscrdLen - 1);

            k = 0;
            for (var j = 0; j <= strtLen; j++) {
                if (this.maxDscrdLen > 2)
                    checkIdx = (this.maxDscrdLen - k - 1);
                else
                    checkIdx = j;
                if (cardArr[checkIdx]) {
                    infoArr = (cardArr[checkIdx]).split("-");
                    var cardMC = new DiscardCard(infoArr[0], this);
                    this.cardArray.push(cardMC);
                    for (var v = 0; v < this.userNameArray.length; v++) {
                        if (playerArray[a].split(":")[0].substr(0, 5) == this.userNameArray[v]._string) {
                            if (this.maxDscrdLen <= 2) {
                                if (j == 0)
                                    cardMC.setPosition(this.userNameArray[a].x, (2 * (cardMC.height - 26 * scaleFactor) + 40 * scaleFactor));
                                else
                                    cardMC.setPosition(this.userNameArray[a].x, ((cardMC.height - 26 * scaleFactor) + 40 * scaleFactor));
                            }
                            else
                                cardMC.setPosition(this.userNameArray[a].x, (j * (cardMC.height - 26 * scaleFactor) + 40 * scaleFactor));
                        }
                    }
                    this.addChild(cardMC);
                    cc.eventManager.addListener(this.discardPanelMouseListener.clone(), cardMC);
                    cc.eventManager.addListener(this.discardPanelTouchListener.clone(), cardMC);
                    if (infoArr[1]) {
                        var star = new Star();
                        star.setPosition(cardMC.width, cardMC.height);
                        cardMC.addChild(star);
                        cardMC.buttonMode = true;
                        cardMC._isPicked = true;
                        cardMC._pickedBy = infoArr[1];
                    }
                    else
                        cardMC._isPicked = false;
                }
                k++;
            }
        }
        this.DisCards.push(cardMC);
    },
    discardArrowListener: function () {
        if (this.curIndex >= 3) {
            this.curIndex--;
        }
        this.showPlayerCard();
    },
    disCardDownArrowListener: function () {

        if (this.curIndex <= (this.maxDscrdLen - 2)) {
            this.curIndex++;
        }
        this.showPlayerCard();

    },
    showPlayerCard: function () {
        cc.log(" showPlCard curIndex: ", this.curIndex);
        var str;
        var arr = [];
        var k = 0;
        var cardMC;
        var playerArray = this._cardStr.split(";");
        var infoArr = [];

        if (this.curIndex > 1) {
            for (var i = 0; i < this.cardArray.length; i++) {
                this.cardArray[i].removeFromParent(true);
            }
            this.cardArray = [];

            for (var a = 0; a < playerArray.length; a++) {
                var cardStr = playerArray[a].split(":")[1];
                arr = cardStr.split(",");
                k = 0;
                for (var j = 0; j <= 2; j++) {
                    if (arr[this.curIndex - k]) {
                        infoArr = (arr[this.curIndex - k]).split("-");
                        cardMC = new DiscardCard(infoArr[0], this);
                        this.cardArray.push(cardMC);

                        for (var v = 0; v < this.userNameArray.length; v++) {
                            if (playerArray[a].split(":")[0].substr(0, 5) == this.userNameArray[v]._string) {
                                cardMC.setPosition(this.userNameArray[a].x, (j * (cardMC.height - 26 * scaleFactor) + 40 * scaleFactor));
                            }
                        }
                        cc.eventManager.addListener(this.discardPanelMouseListener.clone(), cardMC);
                        cc.eventManager.addListener(this.discardPanelTouchListener.clone(), cardMC);
                        this.addChild(cardMC);

                        if (infoArr[1]) {
                            var star = new Star();
                            star.setPosition(cardMC.width, cardMC.height);
                            cardMC.addChild(star);
                            cardMC.buttonMode = true;
                            cardMC._isPicked = true;
                            cardMC._pickedBy = infoArr[1];
                        }
                        else
                            cardMC._isPicked = false;
                    }
                    k++;
                }
            }
        }
    },
    showPanel: function () {
        this._gameRoom.settingPanel.isVisible() && this._gameRoom.settingPanel.setVisible(false);
        if (this.isVisible()) {
            for (var i = 0; i < this.cardArray.length; i++) {
                this.cardArray[i].removeFromParent(true);
            }
            this.setVisible(false);
        }
        else {
            this.setVisible(true);

        }
    },
    hideDiscardPanel: function () {

        if (this.isVisible()) {
            for (var i = 0; i < this.cardArray.length; i++) {
                this.cardArray[i].removeFromParent(true);
            }
            this.setVisible(false);
        }
    },
    initMouseListener: function () {

        this.discardPanelMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode) && target._isPicked) {
                    switch (target._name) {
                        case "DiscardCard":
                            !target._hovering && target.hover(true);
                            target._hovering = true;
                            break;
                    }
                    return true;
                }
                target._hovering && target.hover(false);
                target._hovering = false;
                return false;
            }.bind(this)
        });
        this.discardPanelMouseListener.retain();
        cc.eventManager.addListener(this.discardPanelMouseListener.clone(), this.downArrow);
        cc.eventManager.addListener(this.discardPanelMouseListener.clone(), this.upArrow);

    },
    initTouchListener: function () {
        this.discardPanelTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                switch (target._name) {
                    case "ArrowUp" :
                        this.discardArrowListener();
                        /* context.;*/
                        break;
                    case"ArrowDown":
                        this.disCardDownArrowListener();
                        /*this.moveUp();*/
                        break;
                }
            }.bind(this)
        });
        this.discardPanelTouchListener.retain();
        cc.eventManager.addListener(this.discardPanelTouchListener.clone(), this.downArrow);
        cc.eventManager.addListener(this.discardPanelTouchListener.clone(), this.upArrow);
    },
    starHover: function (target, bool) {
        if (bool) {
            if (target._isPicked) {
                this.discardToolTip.setPosition(cc.p(target.x - 25 * scaleFactor, target.y + 25 * scaleFactor));
                this.discardToolTip.setText("This card is taken by " + target._pickedBy);
                this.discardToolTip.setArrow("RIGHT");
                this.discardToolTip.setVisible(true);
            }
        } else {
            this.discardToolTip.setVisible(false);
        }
    },
    addCard: function (card, i) {

        if (i == 0) {
            card.setPosition(cc.p(this.width / 4, this.height - 40 * scaleFactor));

        } else if (i == 1) {
            card.setPosition(cc.p(this.width / 4, this.height - 70 * scaleFactor));
        } else if (i == 2) {
            card.setPosition(cc.p(this.width / 4, this.height - 100 * scaleFactor));
        }
        this.addChild(card);
    }
});
var Star = cc.Sprite.extend({
    _hovering: null,
    ctor: function () {
        this._super(spriteFrameCache.getSpriteFrame("Star.png"));
        this.setName("Star");
        this.setScale(2);
        this.setAnchorPoint(0.5, 0.5);
        this._hovering = false;
        return true;
    },
    hover: function () {
    }
});