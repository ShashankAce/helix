/**
 * Created by stpl on 10/14/2016.
 */
var TournamentEventDispatcher = cc.Class.extend({
    _gameInterface: null,
    ctor: function (context) {
        this._gameInterface = context;
    },
    dispatchSFSEvents: function (cmd, room, params) {
        switch (cmd) {
            case TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_NEW:  //tournament
                var roomIds = params[TOURNAMENT_CONSTANTS.ROOMIDS];
                this._gameInterface.processNewTournament(roomIds);
                break;
            case TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_ROOMSTATUS:  //tournament
                this._gameInterface.processTournamentRoomStatus(params);
                break;
            case TOURNAMENT_CONSTANTS.CMD_MYTOURNAMENT:  //tournament //
                this._gameInterface.processMyTournament(params);
                break;
            case TOURNAMENT_CONSTANTS.CMD_DISABLEREG_RESP:  //tournament
                cc.log("disable resp");
                break;
            case TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_HELPTEXT:
                cc.log("tournament command3" + cmd);//tournament
                this._gameInterface.processHelpText(params);
                break;
            case TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_PRIZESTRUCTURE:  //tournament
                this._gameInterface.processTournamentPrizeStr(params);
                break;
            case TOURNAMENT_CONSTANTS.CMD_PLAYER_ELIGIBLE:  //tournament
                this._gameInterface.processPlayerEligible(params);
                break;
            case TOURNAMENT_CONSTANTS.CMD_JOINED_PLAYER_INFO:  //tournament
                this._gameInterface.processJoinedPlayerInfo(room, params);
                break;
            case TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_REGNUM_RESP:  //tournament
                this._gameInterface.processRegNumResp(params, room);
                break;
            case TOURNAMENT_CONSTANTS.LEVEL_PRGRSS_INFO_RESP:  //tournament
                this._gameInterface.processLevelProgress(params);
                break;
            case TOURNAMENT_CONSTANTS.ROOM_PRGRSS_INFO_RESP:  //tournament
                this._gameInterface.processTableProgress(params);
                break;
            case TOURNAMENT_CONSTANTS.CMD_CONFIRM_RESP:  //tournament
                this._gameInterface.processJoinConfrmResp(room, params);
                break;
            case TOURNAMENT_CONSTANTS.CMD_QUIT_RESP: //tournament
                this._gameInterface.processQuitConfrmResp(room, params);
                break;
            case TOURNAMENT_CONSTANTS.CMD_WAITNUM_RESP: //tournament
                this._gameInterface.processWaitNumResp(params);
                break;
            case TOURNAMENT_CONSTANTS.DETAIL_INFO: //tournament
                this._gameInterface.handleTmntDetail(room, params);
                break;
            case TOURNAMENT_CONSTANTS.TOURNAMENT_CANCEL: //tournament
                this._gameInterface.handleTournamentCancelCmd(room, params);
                break;
            case TOURNAMENT_CONSTANTS.PRIZE_INFO:  //tournament
                this._gameInterface.processTournamentPrizeInfo(room, params);
                break;
            case TOURNAMENT_CONSTANTS.WINNER_INFO:  //tournament
                this._gameInterface.processTournamentWinnerInfo(room, params);
                break;
            case TOURNAMENT_CONSTANTS.TOURNAMENT_DATA:
                this._gameInterface.processTournamentData(params);
                break;
            case TOURNAMENT_CONSTANTS.ALREADY_TRNMNT_JOIN_STR:
                this._gameInterface.processAlreadyJoinedTournament(params);
                break;
            case TOURNAMENT_CONSTANTS.NO_ENTRY:
                this._gameInterface.processNoEntry(room, params);
                break;
            case TOURNAMENT_CONSTANTS.CMD_INSTRUCTIONS_RESP:
                this._gameInterface.processInstructions(room, params);
                break;
            case TOURNAMENT_CONSTANTS.UPDATE_PRIZE_STRUCT:
                this._gameInterface.updatePrizeStructure(room, params);
                break;
            case TOURNAMENT_CONSTANTS.STRUCTURE:
                this._gameInterface.setDetailsStructureVars(room, params);
                break;
            case TOURNAMENT_CONSTANTS.PROGRESS_INFO:
                this._gameInterface.setDetailsProgressVars(room, params);
                break;

        }
    }
});