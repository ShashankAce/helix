/**
 * Created by stpl on 1/19/2017.
 */
var DetailTabs = cc.Scale9Sprite.extend({
    _isSelected: false,
    _isEnabled: null,
    _hovering: null,
    ctor: function (labelName, spriteObj, rect, originalRect) {
        var spriteFrame = spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png");
        spriteFrame.setRect(cc.rect(spriteFrame._rect.x + rect.x, spriteFrame._rect.y + rect.y, rect.width, rect.height));
        this._super(spriteFrame);
        spriteFrame.setRect(originalRect);

        this.setCapInsets(cc.rect(16, 16, 16, 16));
        this.setContentSize(cc.size(240 * scaleFactor, 70 * scaleFactor));
        this.setCascadeColorEnabled(false);

        this._isEnabled = true;
        this._hovering = false;

        var spriteNormal = spriteObj["normal"];
        var spriteHover = spriteObj["hover"];

        this.symbol = new cc.Sprite(spriteFrameCache.getSpriteFrame(spriteNormal));
        this.symbol.setPosition(cc.p(this.width / 2, 47 * scaleFactor));
        this.symbol.stateNormal = spriteNormal;
        this.symbol.stateHover = spriteHover;
        this.addChild(this.symbol, 2);

        this.tabLabel = new cc.LabelTTF(labelName, "RobotoRegular", 20 * scaleFactor);
        this.tabLabel.setScale(0.7);
        this.tabLabel.setPosition(cc.p(this.width / 2, 20 * scaleFactor));
        this.tabLabel.setColor(cc.color(50, 50, 50));
        this.addChild(this.tabLabel, 2);

        this.arrow = new cc.Sprite(spriteFrameCache.getSpriteFrame("arrow.png"));
        this.arrow.setRotation(-90);
        this.arrow.setPosition(cc.p(this.width / 2, 0));
        this.arrow.setColor(cc.color(58, 101, 124));
        this.arrow.setVisible(false);
        this.addChild(this.arrow, 2);
    },
    setSelected: function (bool) {
        this._isSelected = bool;
        if (bool) {
            this.tabLabel.setColor(cc.color(255, 255, 255));
            this.setColor(cc.color(58, 101, 124));
            this.symbol.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.symbol.stateHover));
        } else {
            this.tabLabel.setColor(cc.color(50, 50, 50));
            this.setColor(cc.color(255, 255, 255));
            this.symbol.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.symbol.stateNormal));
        }
        this.arrow.setVisible(bool);
    },
    hover: function (bool) {
        if (!this._isSelected) {
            if (bool) {
                this.symbol.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.symbol.stateHover));
                this.tabLabel.setColor(cc.color(255, 255, 255));
                this.setColor(cc.color(102, 136, 155));
            } else {
                this.symbol.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.symbol.stateNormal));
                this.tabLabel.setColor(cc.color(50, 50, 50));
                this.setColor(cc.color(255, 255, 255));
            }
        }
    },
    addWall: function () {
        var leftW = new cc.DrawNode();
        leftW.drawSegment(cc.p(0, 1 * scaleFactor), cc.p(0, this.height - 1 * scaleFactor), 0.4, cc.color(150, 150, 150));
        this.addChild(leftW);

        var rightW = new cc.DrawNode();
        rightW.drawSegment(cc.p(this.width, 1 * scaleFactor), cc.p(this.width, this.height - 1 * scaleFactor), 0.5, cc.color(120, 120, 120));
        this.addChild(rightW);
    }
});