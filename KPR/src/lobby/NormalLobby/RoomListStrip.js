/**
 * Created by stpl on 9/16/2016.
 */
"use strict";
var RoomListStrip = ccui.Layout.extend({

    _name: "RoomListStrip",
    _selectedColor: null,
    _defaultColor: null,
    stripListener: null,
    _isSelected: null,
    _isEnabled: null,
    roomListManager: null,

    roomId: null,
    tableId: null,
    tableName: null,
    gameType: null,
    maxPlayers: null,
    roomState: null,
    entryFee: null,
    turnType: null,
    totalPlayers: null,
    seatedPLayers: null,
    cashOrPromo: null,
    betAmount: null,
    pointValue: null,
    commonId: null,
    groupTotalPlayers: null,
    numOfCard: null,
    seatedPls: null,

    ctor: function (widthArray, roomInfo, index, RoomListManager) {
        this._super();

        this.roomId = roomInfo.roomId; // roomId is used to display in ui everywhere
        this.tableId = roomInfo.mTableId;
        this.tableName = roomInfo.mTableName;
        this.gameType = roomInfo.mGameType;
        this.maxPlayers = roomInfo.mMaxPlayers;
        this.roomState = roomInfo.mRoomState;
        this.entryFee = roomInfo.mEntryFee;
        this.turnType = roomInfo.mTurnType;
        this.totalPlayers = roomInfo.mTotalPlayers;
        this.seatedPLayers = roomInfo.mSeatedPLayers;
        this.cashOrPromo = roomInfo.mCashOrPromo;
        this.betAmount = roomInfo.mBetAmount;
        this.pointValue = roomInfo.mPointValue;
        this.commonId = roomInfo.mCommonId;
        this.groupTotalPlayers = roomInfo.mGroupTotalPlayers;
        this.totalPrize = roomInfo.mTotalPrize;
        this.seatedPls = roomInfo.seatedPls;
        this.numOfCard = roomInfo.mNumOfCard;


        this._selectedColor = cc.color(223, 230, 182);
        this._isSelected = !1;
        this._isEnabled = true;

        this.setTouchEnabled(true);
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.setContentSize(cc.size(755 * scaleFactor, 30 * scaleFactor));
        this.setTag(index);
        this._name += " " + index;

        if (index % 2 == 0) {
            this._defaultColor = cc.color(255, 255, 255);
        } else {
            this._defaultColor = cc.color(236, 236, 236);
        }

        this.setBackGroundColor(this._defaultColor);

        this.initBar(widthArray, roomInfo, index, RoomListManager);

    },
    pauseListener: function (bool) {
        this._isEnabled = !bool;
    },
    onExit: function () {
        this._super();
        this.release();
    }
});
RoomListStrip.prototype.initBar = function (widthArray, roomInfo, index, RoomListManager) {

    this.roomListManager = RoomListManager;
    var subGameType = RoomListManager._filterParser.selectedSubGameType;

    this._name += " " + index;

    var fontSize = 25;
    var gameTypeValue = new cc.LabelTTF(this.gameType.split("-")[1], "RobotoRegular", fontSize);
    gameTypeValue.setScale(0.5);
    gameTypeValue.setName("gameType");
    gameTypeValue.setPosition(cc.p(widthArray[0], this.height / 2));
    gameTypeValue.setColor(cc.color(0, 0, 0));
    this.addChild(gameTypeValue);

    if (subGameType._name == "POOL_13" || subGameType._name == "DEALS_13") {
        var entryFeeValue = new cc.LabelTTF(this.entryFee, "RobotoRegular", fontSize);
        entryFeeValue.setScale(0.5);
        entryFeeValue.setName("entryFee");
        entryFeeValue.setPosition(cc.p(widthArray[1], this.height / 2));
        entryFeeValue.setColor(cc.color(0, 0, 0));
        this.addChild(entryFeeValue);

        var prizeValue = new cc.LabelTTF(this.totalPrize, "RobotoRegular", fontSize);
        prizeValue.setScale(0.5);
        prizeValue.setName("prizeValue");
        prizeValue.setPosition(cc.p(widthArray[2], this.height / 2));
        prizeValue.setColor(cc.color(0, 0, 0));
        this.addChild(prizeValue);
    } else {
        var pointValue = new cc.LabelTTF(this.pointValue, "RobotoRegular", fontSize);  // roomInfo.mPointValue
        pointValue.setScale(0.5);
        pointValue.setName("pointValue");
        pointValue.setPosition(cc.p(widthArray[1], this.height / 2));
        pointValue.setColor(cc.color(0, 0, 0));
        this.addChild(pointValue);

        var betAmt = new cc.LabelTTF(this.betAmount, "RobotoRegular", fontSize); // roomInfo.mBetAmount
        betAmt.setScale(0.5);
        betAmt.setName("betAmount");
        betAmt.setPosition(cc.p(widthArray[2], this.height / 2));
        betAmt.setColor(cc.color(0, 0, 0));
        this.addChild(betAmt);
    }

    var turnTimeValue = new cc.LabelTTF(this.turnType, "RobotoRegular", fontSize); // roomInfo.mTurnType
    turnTimeValue.setScale(0.5);
    turnTimeValue.setName("turnType");
    turnTimeValue.setPosition(cc.p(widthArray[3], this.height / 2));
    turnTimeValue.setColor(cc.color(0, 0, 0));
    this.addChild(turnTimeValue);

    var playersValue = new cc.LabelTTF(this.maxPlayers, "RobotoRegular", fontSize);  // roomInfo.mMaxPlayers
    playersValue.setScale(0.5);
    playersValue.setName("players");
    playersValue.setPosition(cc.p(widthArray[4], this.height / 2));
    playersValue.setColor(cc.color(0, 0, 0));
    this.addChild(playersValue);

    var statusValue = new cc.LabelTTF(this.totalPlayers + " Waiting", "RobotoRegular", fontSize); // roomInfo.mTotalPlayers + " Waiting"
    statusValue.setScale(0.5);
    statusValue.setName("status");
    statusValue.setPosition(cc.p(widthArray[5], this.height / 2));
    statusValue.setColor(cc.color(0, 0, 0));
    this.addChild(statusValue);

    var joinSprite = new cc.Sprite(spriteFrameCache.getSpriteFrame("joinButton.png"));
    joinSprite._hovering = false;
    joinSprite.attr({
        x: widthArray[6],
        y: this.height / 2
    });
    joinSprite.hover = function (bool) {
        bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("joinButtonOver.png")) :
            this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("joinButton.png"));
    };
    this.addChild(joinSprite, 1, "joinBtn");

    cc.eventManager.addListener(this.roomListManager.roomListManagerMouseListener.clone(), joinSprite);
    cc.eventManager.addListener(this.roomListManager.roomListManagerTouchListener.clone(), joinSprite);

    var totalPlayers = (this.groupTotalPlayers == "0") ? "-" : this.groupTotalPlayers + " Players";
    var totalPlayerValue = new cc.LabelTTF(totalPlayers, "RobotoRegular", fontSize);
    totalPlayerValue.setScale(0.5);
    totalPlayerValue.setName("totalPlayers");
    totalPlayerValue.setPosition(cc.p((widthArray[7]), this.height / 2));
    totalPlayerValue.setColor(cc.color(0, 0, 0));
    this.addChild(totalPlayerValue);

    this.watchSprite = new WatchSprite("Watch", "RobotoRegular", fontSize, this.groupTotalPlayers);
    this.watchSprite.setPosition(cc.p(3 * (this.width - widthArray[7]) / 4 + widthArray[7] - 10 * scaleFactor, this.height / 2));
    this.addChild(this.watchSprite);

    cc.eventManager.addListener(this.roomListManager.roomListManagerMouseListener.clone(), this.watchSprite);
    cc.eventManager.addListener(this.roomListManager.roomListManagerTouchListener.clone(), this.watchSprite);

    /* if (parseInt(this.groupTotalPlayers) > 0)
     this.watchSprite.enableWatch();
     else
     this.watchSprite.disableWatch();*/
};
RoomListStrip.prototype.setSelected = function (bool) {
    if (bool) {
        this.roomListManager.selectedCommonId = this.commonId;
    }
    bool && (this.setBackGroundColor(this._selectedColor), this._isSelected = !0) || (this.setBackGroundColor(this._defaultColor), this._isSelected = !1);
};
RoomListStrip.prototype.watchButtonListener = function () {
    var room = sfsClient.getRoomById(this.tableId);
    var uid = sfs.mySelf.id;
    /* var user = room.getUserById(uid);
     if (!user) {*/
    applicationFacade.lobbyCmdHandler({
        cmd: "watch",
        id: room.id,
        commonId: this.commonId
    });
    /* } else {
     applicationFacade._controlAndDisplay.joinRoomHandler(room.id);
     }*/
    cc.log("RoomListStrip watchReq");
};
RoomListStrip.prototype._joinRoom = function () {
    var room = sfsClient.getRoomById(this.tableId);
    if (room) {
        this.joinRoomHandler(room);
    } else {
        var dataObj = {cId: this.commonId};
        sfsClient.sendReqFromLobby(SFSConstants.CMD_ROOM_JOIN_REQUEST, dataObj);
    }
    cc.log("RoomListStrip _joinRoom");
};
RoomListStrip.prototype.joinRoomHandler = function (room) {
    this.roomListManager.updateListViewItem(this);
    var uid = sfs.mySelf.id;
    var user = room.getUserById(uid);
    var dataObj = {
        cmd: "RoomJoin",
        id: room.id
    };
    !user ? applicationFacade.lobbyCmdHandler(dataObj) : applicationFacade._controlAndDisplay.joinRoomHandler(room.id);
};

var WatchSprite = cc.LabelTTF.extend({
    _hovering: null,
    isEnable: null,
    ctor: function (LabelName, fontFamily, fontSize, totalPlayer) {
        this._super(LabelName, fontFamily, fontSize);
        this.setName(LabelName);
        this.setScale(0.5);
        this.setColor(cc.color(120, 120, 120));
        this.setCascadeColorEnabled(true);
        this._hovering = false;

        this.line = new cc.Sprite(spriteFrameCache.getSpriteFrame("horizontalHair.png"));
        this.line.setColor(cc.color(255, 255, 255));
        this.addChild(this.line, 2);
        if (parseInt(totalPlayer) > 0)
            this.enableWatch();
        else
            this.disableWatch();
        return true;
    }
});
WatchSprite.prototype.enableWatch = function () {
    this.line.setPosition(cc.p(this.width / 2, this.height / 2 - 13));
    this.setColor(cc.color(191, 106, 1));
    this.isEnable = true;
};
WatchSprite.prototype.disableWatch = function () {
    this.line.setPosition(cc.p(this.width / 2, this.height / 2));
    this.setColor(cc.color(120, 120, 120));
    this.isEnable = !1;
};
WatchSprite.prototype.hover = function (bool) {
    if (this.isEnable) {
        if (bool) {
            this.setColor(cc.color(0, 0, 0));
        } else {
            this.setColor(cc.color(191, 106, 1));
        }
    }
};