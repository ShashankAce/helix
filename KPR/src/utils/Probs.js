/**
 * Created by stpl on 9/28/2016.
 */
function pickGameSize() {
    var availGameSize = {
        1: {
            x: 568, y: 320
        },
        2: {
            x: 1000, y: 650
        },
        3: {
            x: 1500, y: 975
        }
    }
    var sizePicked = {
        x: null,
        y: null
    }
    var temp = null, gameIndex = 0,
        tempValue = Math.abs(GAME_SIZE.width - availGameSize[1].x) + Math.abs(GAME_SIZE.width - availGameSize[1].y);

    for (var size in availGameSize) {

        var xRatio = Math.abs(GAME_SIZE.width - size[i].x);
        var yRatio = Math.abs(GAME_SIZE.height - size[i].y);
        if ((xRatio + yRatio) <= tempValue) {
            tempValue = (xRatio + yRatio);
            gameIndex = i;
        }
    }
    temp = gameIndex;

    if (sizePicked.x == null) {
        sizePicked.x = availGameSize[temp].x;
        sizePicked.y = availGameSize[temp].y;
    }
    return {width: sizePicked.x, height: sizePicked.y};
};

function labelModule(string, fontFamily, fontSize, scaleValue, color) {
    var label = new cc.LabelTTF(string, fontFamily, fontSize * 2);
    label.setScale(scaleValue);
    label.setFontFillColor(color);
    return label;
}