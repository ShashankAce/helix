/**
 * Created by stpl on 1/23/2017.
 */
var PrizeStringCalculator = {};
PrizeStringCalculator.calculatePrzStr = function (rankPrizeStr) {
    //trace("calculatePrizeString for : ",rankPrizeStr);
    if (!rankPrizeStr)
        return;
    var j = 0;
    var prizeStr = "";
    var formatStr = rankPrizeStr.split(":")[0];
    var przFrmtStr = rankPrizeStr.split(":")[1];
    var formatStrArr = formatStr.split("|");
    var przFrmtStrArr = przFrmtStr.split("|");

    for (j = 0; j < formatStrArr.length; j++) {
        if (formatStrArr[j] == "C")
            prizeStr += String("`" + przFrmtStrArr[j] + " + ");
        else
            prizeStr += String(przFrmtStrArr[j] + " + ");
        /*
         else if (formatStrArr[j] == "K" )
         prizeStr += String(przFrmtStrArr[j] + " "+ "KPP" + " + ");
         else if (formatStrArr[j] == "B")
         prizeStr += String(przFrmtStrArr[j] + " "+ "Bonus" + " + ");
         else if (formatStrArr[j] == "T")
         prizeStr += String(przFrmtStrArr[j] + " "+ "Ticket" + " + ");*/
    }
    //trace("Before: ",prizeStr);
    var prizeStr1 = prizeStr.substring(0, prizeStr.length - 2);
    //trace(" After return String: ",prizeStr1);
    return prizeStr1;
};
PrizeStringCalculator.calculateStructPrzStr = function (rankPrizeStr) {
    //trace("calculatePrizeString for : ",rankPrizeStr);
    var j = 0;
    var prizeStr = "";
    var formatStr = rankPrizeStr.split(":")[0];
    var przFrmtStr = rankPrizeStr.split(":")[1];
    var formatStrArr = formatStr.split("|");
    var przFrmtStrArr = przFrmtStr.split("|");

    for (j = 0; j < formatStrArr.length; j++) {
        if (formatStrArr[j] == "C")
            prizeStr += String("`" + przFrmtStrArr[j] + " + ");
        else
            prizeStr += String(przFrmtStrArr[j] + " + ");

        /*else if (formatStrArr[j] == "K" )
         prizeStr += String(przFrmtStrArr[j] + " "+ "KPP" + " + ");
         else if (formatStrArr[j] == "B")
         prizeStr += String(przFrmtStrArr[j] + " "+ "Bonus" + " + ");
         else if (formatStrArr[j] == "T")
         prizeStr += String(przFrmtStrArr[j] + " "+ "Ticket" + " + ");*/
    }
    //trace("Before: ",prizeStr);
    var prizeStr1 = prizeStr.substring(0, prizeStr.length - 3);
    //trace(" After return String: ",prizeStr1);
    return prizeStr1;
};