/**
 * Created by stpl on 10/14/2016.
 */
var SubMiniTable = cc.Sprite.extend({
    _chairArray: [],
    _seatArray: {},
    ctor: function () {
        this._super(spriteFrameCache.getSpriteFrame("tablePopUp.png"));

        this._seatArray = {

            2: [cc.p(this.width / 2, this.height / 2 + 10 * scaleFactor),
                cc.p(this.width / 2, this.height / 2 - 35 * scaleFactor)],
            4: [cc.p(this.width / 2 + 35, this.height / 2 - 12 * scaleFactor),
                cc.p(this.width / 2, this.height / 2 + 10 * scaleFactor),
                cc.p(this.width / 2 - 36, this.height / 2 - 12 * scaleFactor),
                cc.p(this.width / 2, this.height / 2 - 35 * scaleFactor)],
            6: [cc.p(this.width / 2 + 35, this.height / 2 - 12 * scaleFactor),
                cc.p(this.width / 2 + 22, this.height / 2 + 8 * scaleFactor),
                cc.p(this.width / 2, this.height / 2 + 10 * scaleFactor),
                cc.p(this.width / 2 - 22, this.height / 2 + 8 * scaleFactor),
                cc.p(this.width / 2 - 36, this.height / 2 - 12 * scaleFactor),
                cc.p(this.width / 2, this.height / 2 - 35 * scaleFactor)],
        };
        //this.setTurnIndicator(6, 2);

    },
    addMiniTableElements: function (noOfPlyrs, gameType, tableId) {
        cc.log("in addMiniTableElements");
        this._gameTF = new cc.LabelTTF(gameType, "RobotoRegular", 22 * scaleFactor, cc.TEXT_ALIGNMENT_CENTER);
        this._gameTF.x = this.width / 2;
        this._gameTF.y = this.height - 13 * scaleFactor;
        this._gameTF.setScale(0.5);
        this.addChild(this._gameTF);

        this._tableTF = new cc.LabelTTF(tableId, "RobotoRegular", 22 * scaleFactor, cc.TEXT_ALIGNMENT_CENTER);
        this._tableTF.x = this.width / 2;
        this._tableTF.y = this.height - 30 * scaleFactor;
        this._tableTF.setScale(0.5);
        this.addChild(this._tableTF);

        this._timeTF = new cc.LabelTTF("", "RobotoBold", 40 * scaleFactor, cc.TEXT_ALIGNMENT_CENTER);
        this._timeTF.x = this.width / 2;
        this._timeTF.y = 45 * scaleFactor;
        this._timeTF.setScale(0.5);
        this.addChild(this._timeTF, 1);

        this.extraMC = new cc.Sprite(spriteFrameCache.getSpriteFrame("TabPopUpExtraTime.png"));
        this.extraMC.setPosition(cc.p(this.width / 3, 45 * scaleFactor));
        this.extraMC.setVisible(false);
        this.addChild(this.extraMC, 2);

        this.addTableplayers(noOfPlyrs);

        this.turnIndicator = new cc.Sprite(spriteFrameCache.getSpriteFrame("tablePopUp_redDot.png"));
        this.addChild(this.turnIndicator, 1);
        this.turnIndicator.setVisible(false);
    },
    setTimer: function (val) {
        this._timeTF.setString(val);
    },
    setTableId: function (val) {
        this._tableTF.setString(val);
    },
    setGameTF: function (val) {
        this._gameTF.setString(val);
    },
    addTableplayers: function (noOfPlayer) {
        this.removeAllChairs();
        this.getChair(noOfPlayer);
        for (var i = 0; i < noOfPlayer; i++) {
            this._chairArray[i].setPosition(this._seatArray[noOfPlayer][i]);
            this.addChild(this._chairArray[i]);
        }
    },
    removeAllChairs: function () {
        for (var i = 0; i < this._chairArray.length; i++) {
            this._chairArray[i].removeFromParent();
        }
        this._chairArray = [];
    },
    getChair: function (noOfPlayer) {

        for (var i = 0; i < noOfPlayer; i++) {
            this._chairArray[i] = this.getPlayer();
        }
    },
    getPlayer: function () {
        return new cc.Sprite(spriteFrameCache.getSpriteFrame("tablePopUp_blackDot.png"));
    },
    setTurnIndicator: function (noOfPlayer, seatId) {
        var currentSeat = parseInt(seatId) - 1;
        this.turnIndicator.setVisible(true);
        this.turnIndicator.setPosition(this._seatArray[noOfPlayer][currentSeat]);
    }
});