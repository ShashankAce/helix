/**
 * Created by stpl on 02-Apr-17.
 */
var TournamentInstructions = Popup.extend({
    ctor: function (context, parmas) {
        this._super(new cc.Size(736 * scaleFactor, 414 * scaleFactor), context, true);

        this.headerLabel.setString("Tournament Instructions");

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        this.closeBtn = new cc.Sprite(spriteFrameCache.getSpriteFrame("ResultClose.png"));
        this.closeBtn.setPosition(cc.p(this.width, this.height));
        this.addChild(this.closeBtn, 10, "closeBtn");
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.closeBtn);

        var insStr = parmas["str"];
        if (insStr == null || insStr == "")
            insStr = "";

        var insTextArr = insStr.split("#");
        var previousTxt;
        for (var i = 0; i < insTextArr.length; i++) {
            var insText = this.addParagraph(insTextArr[i]);
            if (previousTxt)insText.setPosition(cc.p(25 * scaleFactor, previousTxt.y - previousTxt.height / 2 - 2));
            else insText.setPosition(cc.p(25 * scaleFactor, this.height - 55 * scaleFactor - (25 * scaleFactor * i)));
            this.addChild(insText, 5);
            previousTxt = insText;
        }
    },
    onTouchEndedCallBack: function (touch, event) {
        this._super(touch, event);
        var target = event.getCurrentTarget();
        target.getName() == "closeBtn" && this.removeFromParent(true);
    },
    addParagraph: function (insText) {
        var bullet = new cc.Sprite(spriteFrameCache.getSpriteFrame("whiteDot.png"));
        bullet.setColor(cc.color(126, 126, 126));

        var label = new cc.LabelTTF(insText, "RobotoRegular", 14 * 2 * scaleFactor);
        label.setScale(0.5);
        label.setColor(cc.color(60, 60, 60));
        label.addChild(bullet, 1);
        label._setBoundingWidth(this.width * 2 - 70 * scaleFactor);
        label.setAnchorPoint(0, 1);

        bullet.x = -20 * scaleFactor;
        bullet.y = label.height - 18 * scaleFactor;
        return label;
    }
});