/**
 * Created by stpl on 9/19/2016.
 */

"use strict";
var GameConfiguration = cc.Class.extend({

    _className: "GameConfiguration",
    os: null,
    _fileName: "groupRummyFile",

    appVersion: null,
    versionAppType: null,
    deviceType: null,
    appType: null,
    useSSL: null,

    ctor: function (data) {

        this.appVersion = data.appVersion;
        this.versionAppType = data.versionAppType;
        this.deviceType = data.deviceType;
        this.appType = data.appType;
        this.useSSL = data.useSSL;
        
        cc.sys.os == "windows" ? this.os = "ANDROID" : this.os = "ANDROID";

    },
    saveGameData: function (obj) {
        var dataObject = obj;

        if (cc.sys.isNative) {
            var path = jsb.fileUtils.getWritablePath();

            cc.log('Saving ' + path);
            if (jsb.fileUtils.writeToFile({data: JSON.stringify(dataObject)}, path + this._fileName)) {
                cc.log('Save SUCCESS');
            }
            else {
                cc.log('Save FAILED');
            }
        }
        else {
            cc.sys.localStorage.setItem(this._fileName, JSON.stringify(dataObject));
            cc.log('Save SUCCESS');
        }
    },
    loadGameData: function () {
        if (cc.sys.isNative) {
            cc.log('Loading game data');
            var path = jsb.fileUtils.getWritablePath();
            if (jsb.fileUtils.isFileExist(path + this._fileName)) {
                var temp = jsb.fileUtils.getValueMapFromFile(path + this._fileName);
                var dataObject = JSON.parse(temp.data);
                if (dataObject) {
                    cc.log('Game data found ' + dataObject.playerToken);
                    this.playerToken = dataObject.playerToken;
                }
            }
            else {
                cc.log('No Saved game data found');
            }
        }
        else {
            var data = cc.sys.localStorage.getItem(this._fileName);
            if (data != null) {
                var dataObject = JSON.parse(data);
                this.playerToken = dataObject.playerToken;
                cc.log('Game Data Found : ' + dataObject.playerToken);
            }
            else {
                cc.log('No Saved Game data found');
            }
        }
    },
});