/**
 * Created by stpl on 10/27/2016.
 */
var Player = PlayerInfo.extend({
    _name: "Player",
    _id: null,
    _seatPosition: null,
    _sittingStatus: null,
    gainedPoint: null,
    _playerName: null,
    displayTurnTime: null,
    _isEnabled: null,
    _playerId: null,
    _tossCard: null,
    _isMyTurn: null,
    _gameRoom: null,
    _totalTime: null,
    _totalExtraTime: null,
    _remExtraTime: null,
    _isExtraTimer: null,
    _points: null,
    player_rank: null,
    _toolTipEnabled: null,
    playerToolTip: null,
    _hovering: null,
    tsid: null,
    ssid: null,
    imgUrl: null,
    imgData: null,
    ctor: function (context, index) {
        this._super(spriteFrameCache.getSpriteFrame("Join_btn.png"));
        this._gameRoom = context;
        this.setName("Player");
        this.setTag(index);
        this._totalTime = 60;
        this._isMyTurn = false;
        this._isEnabled = true;
        this._sittingStatus = false;
        this._seatPosition = index + 1;
        this._playerName = "";
        // this._seatPosition = null;
        this._totalExtraTime = 45;
        this._isExtraTimer = false;
        this._points = 0;
        this.player_rank = 0;
        this._toolTipEnabled = false;
        this._hovering = false;

        this.imgUrl = null;
        this.imgData = null;

        if (this._gameRoom._serverRoom.getVariable(SFSConstants._TURNTIME)) {
            this._totalTime = parseInt(this._gameRoom._serverRoom.getVariable(SFSConstants._TURNTIME).value);
            this._totalExtraTime = parseInt(this._gameRoom._serverRoom.getVariable(SFSConstants._TOTALEXTRATIME).value);
        }
        this.createProgressTimer();
        return true;
    },
    hover: function (bool) {
        if (bool) {
            this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("Join_btn_selected.png"));
        } else {
            this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("Join_btn.png"));
        }
    },
    takeSeat: function (playerId, playerName, score) {

        var playerFullName = playerName;
        if (playerFullName.length > 8) {
            playerName = playerName.substr(0, 8);
            playerName += "...";

            this._toolTipEnabled = true;
        }

        this._sittingStatus = true;
        this._playerId = playerId;
        this._playerName = playerFullName;


        this.profileBase = new cc.Sprite(spriteFrameCache.getSpriteFrame("Profile_Base.png"));
        this.profileBase.setName("playerProfile");
        this.profileBase.setPosition(cc.p(this.width / 2, this.height / 2 - 15 * scaleFactor));
        this.addChild(this.profileBase, 4);
        this.profileBase._isEnabled = true;
        var self = this;
        this.profileBase.hover = function (bool) {
            if (self._toolTipEnabled && bool) {
                self._gameRoom.showPlayerNameToolTip(true, self._playerName, self.getPosition());
            } else {
                self._gameRoom.showPlayerNameToolTip(false);
            }
        };
        cc.eventManager.addListener(this.parent.baseGameMouseListener.clone(), this.profileBase);

        this.playerName = new cc.LabelTTF(playerName, "RobotoRegular", 12 * 2 * scaleFactor);
        this.playerName.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.playerName.setDimensions(cc.size(this.profileBase.width * 2 - 20 * scaleFactor, this.playerName.height));
        this.playerName.setScale(0.5);
        this.playerName.setPosition(cc.p(40 * scaleFactor, 27 * scaleFactor - yMargin));
        this.profileBase.addChild(this.playerName, 6);

        /* if(score){
         this.gainedPoint = new cc.LabelTTF(score.toString(), "RobotoRegular", 12 * 2);
         }else{
         this.gainedPoint = new cc.LabelTTF("", "RobotoRegular", 12 * 2);
         }*/
        this.gainedPoint = new cc.LabelTTF("", "RobotoRegular", 12 * 2 * scaleFactor);
        this.gainedPoint.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.gainedPoint.setDimensions(cc.size(this.profileBase.width * 2 - 20 * scaleFactor, this.gainedPoint.height));
        this.gainedPoint.setScale(0.5);
        this.gainedPoint.setPosition(cc.p(40 * scaleFactor, 12 * scaleFactor - yMargin));
        this.profileBase.addChild(this.gainedPoint, 6);

        /*this.profRank = new cc.LabelTTF("", "RobotoRegular", 12 * 2 * scaleFactor);
         this.profRank.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
         // this.profRank.setDimensions(cc.size(this.profileBase.width * 2 - 20 * scaleFactor, this.gainedPoint.height));
         this.profRank.setScale(0.5);
         this.profRank.setPosition(cc.p((60 + this.gainedPoint.width) * scaleFactor, 12 * scaleFactor));
         this.profileBase.addChild(this.profRank, 6);*/

        this.player_score = score;
        if (score > 0) {
            this.updateScore(score);
        }
        this._initClippingNode();

        /*this.avatar = new cc.Sprite(spriteFrameCache.getSpriteFrame("mask.png"));
         this.avatar.setPosition(cc.p(40, 68));
         this.profileBase.addChild(this.avatar, 2);*/

        /*auto labelGlow = cocos2d::Label::createWithTTF( "Long Long String Text With Glow", "Fonts/Arial.ttf", 50, cocos2d::Size( 400, 50 ), cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER );
         labelGlow->setTextColor( cocos2d::Color4B::BLUE );
         labelGlow->enableGlow( cocos2d::Color4B::RED );
         labelGlow->setOverflow( cocos2d::Label::Overflow::SHRINK );
         labelGlow->enableWrap( false );
         labelGlow->setPosition( cocos2d::Vec2( 250, 400 ) );
         addChild( labelGlow );*/

        // this.startTimer(50);
        if (playerId != this._gameRoom._myId) {
            this.pauseListener(true);
        }

        if (this.imgUrl) {
            this.setProfilePic(this.imgUrl);
        } else {
            this.imgUrl = null;
            this.imgData = null;
        }


    },
    allotSeat: function (data) {
        if (data['url'])
            this.imgUrl = data['url'];
        else {
            this.imgUrl = null;
            this.imgData = null;
        }

        this.takeSeat(data['plId'], data['uName'], data['points']);
        this.clearAllState();
    },
    setRndPoints: function (rndPts) {
        if (this.isDropped && this.dropPoints)
            this.dropPoints.setString(rndPts + " Pts.");
    },
    updateScore: function (score, rank) {
        rank = rank || this.player_rank;
        score = score || this.player_score;
        if (rank)
            score = score + "(" + rank + ")";
        this.gainedPoint && this.gainedPoint.setString(score);
        // this.profRank && this.profRank.setString("(" + (rank || this.player_rank) + ")");
    },
    pauseListener: function (bool) {
        this._isEnabled = !bool;
    },
    leaveSeat: function () {
        this._id = -1;
        this._name = "Player";
        this.gainedPoint = null;
        this.displayTurnTime = null;
        this._playerName = null;
        this._isEnabled = true;
        this._playerId = null;
        this._tossCard = null;
        this._isMyTurn = false;
        this._sittingStatus = false;
        this.imgUrl = null;
        this.imgData = null;
        this._seatPosition = null;
        this.seat_id = null;
        this.player_id = null;
        this.player_name = null;
        this.player_score = null;
        this.player_rank = null;
        this.player_extraTime = null;
        this.isDropped = null;
        this.isDisconnected = null;
        this.isAutoPlay = null;
        this.isWrongShow = null;
        this.isWaiting = null;
        this._isNext = null;
        this._toolTipEnabled = false;
        this.clippingNode && this.clippingNode.removeFromParent(true);
        this.clippingNode = null;

        this.profileBase && this.profileBase.removeFromParent(true);
        this.profileBase = null;

        this.removeIcon();
        this.removeDealerIcon();
        this.pauseListener(false);

    },
    createProgressTimer: function () {
        this.progressTimer = new cc.ProgressTimer(new cc.Sprite(spriteFrameCache.getSpriteFrame("timerBorder.png")));
        this.progressTimer.setType(cc.ProgressTimer.TYPE_RADIAL);
        this.progressTimer.setBarChangeRate(cc.p(0, 1));
        this.progressTimer.setMidpoint(cc.p(0.5, 0.5));
        this.progressTimer.setReverseDirection(true);
        this.progressTimer.setPosition(cc.p(this.width / 2, this.height / 2 - 15 * scaleFactor));
        this.progressTimer.setVisible(false);
        this.addChild(this.progressTimer, 8);

        this.timerSprite = new cc.Sprite(spriteFrameCache.getSpriteFrame("TimerCount.png"));
        this.timerSprite.setPosition(cc.p(-this.timerSprite.width / 2, 40 * scaleFactor));
        this.timerSprite.setVisible(false);
        this.addChild(this.timerSprite, 9);

        this.timerLabel = new cc.LabelTTF("", "RobotoBold", 12 * scaleFactor);
        this.timerLabel.setColor(cc.color(0, 0, 0));
        this.timerLabel.setPosition(cc.p(20 * scaleFactor, 40 * scaleFactor - yMargin));
        this.timerSprite.addChild(this.timerLabel, 10);

        this.extraTimerLabel = new cc.LabelTTF("", "RobotoRegular", 12 * scaleFactor);
        this.extraTimerLabel.setColor(cc.color(0, 0, 0));
        this.extraTimerLabel.setPosition(cc.p(25 * scaleFactor, 16 * scaleFactor - yMargin));
        this.timerSprite.addChild(this.extraTimerLabel, 10);

    },
    setTime: function (time) {
        if (time < 0)
            return;
        var percentage;
        if (this._isExtraTimer == false) {
            var color = this.getProgBarColor(this._totalTime, time);
            this.progressTimer.setColor(cc.color(color));
            this.timerLabel.setString(time + "");
            this.extraTimerLabel.setString(this._remExtraTime + "");

            percentage = parseInt(100 / this._totalTime * time);
            this.progressTimer.setPercentage(percentage);
        }
        else {
            this._remExtraTime = time;
            this.progressTimer.setColor(cc.color(255, 0, 0));
            this.timerLabel.setString("0");
            this.extraTimerLabel.setString(this._remExtraTime + "");

            percentage = parseInt(100 / this._totalExtraTime * time);
            this.progressTimer.setPercentage(percentage);
        }

    },
    startTimer: function (isExtraTimerBool) {

        this._isExtraTimer = isExtraTimerBool;

        this.progressTimer.setVisible(true);
        this.timerSprite.setVisible(true);

        /*this._isMyTurn = true;
         if (this._totalTime != timeInteger) {
         var maxTime = this._totalTime;
         } else {
         var maxTime = timeInteger;
         }
         var timer = timeInteger;
         this.progressTimer = new cc.ProgressTimer(new cc.Sprite(spriteFrameCache.getSpriteFrame("timerBorder.png")));
         this.progressTimer.setType(cc.ProgressTimer.TYPE_RADIAL);
         this.progressTimer.setBarChangeRate(cc.p(0, 1));
         this.progressTimer.setMidpoint(cc.p(0.5, 0.5));

         this.progressTimer.setReverseDirection(true);

         if (timerType == KHEL_CONSTANTS.TIMER_SHOW_TIME) {
         var color = this.getProgBarColor(maxTime, timer);
         this.progressTimer.setColor(cc.color(color));
         this.timerLabel.setString(timeInteger + "");
         }
         else {
         this.progressTimer.setColor(cc.color(255, 0, 0));
         this.timerLabel.setString("0");
         this.extraTimerLabel.setString(timeInteger + "");
         }
         this.progressTimer.setPosition(cc.p(this.width / 2, this.height / 2 - 15));

         var percentage = parseInt(100 / maxTime * timer);
         this.progressTimer.setPercentage(percentage);
         // this.progressTimer.setPercentage(100);
         this.addChild(this.progressTimer, 20);

         /!*var to1 = cc.progressTo(5, 100);
         healthBar.runAction(to1);*!/

         var timerCount = 0;
         this.progressTimerInterval = setInterval(function () {
         timerCount += 1;

         if (timerType == KHEL_CONSTANTS.TIMER_SHOW_TIME) {
         var color = this.getProgBarColor(maxTime, (timer - timerCount));
         this.progressTimer.setColor(cc.color(color));
         this.timerLabel.setString((timer - timerCount) + "");
         }
         else {
         this.progressTimer.setColor(cc.color(255, 0, 0));
         this.extraTimerLabel.setString((timer - timerCount) + "");
         }
         var percentage = parseInt(100 / maxTime * (timer - timerCount));
         this.progressTimer.setPercentage(percentage);

         if (this._gameRoom._currentTurnID == rummyData.playerId) {
         if ((timer - timerCount) <= 2) {
         /!*for (var i = 0; i < sfsClient.mSeatedPLayers.length; i++) {
         if (this.turn_id == sfsClient.mSeatedPLayers[i].getPlayer_id() && sfsClient.mSeatedPLayers[i].getPlayer_extraTime() <= 0) {
         // disable click
         //mCardLayout.setBtnClicksEnabled(false);
         }
         }*!/
         }
         }
         if ((timer - timerCount) == 3 || (timer - timerCount) == 1) {
         // send group cards in parameter
         var meldCardStr = this._gameRoom._userCardPanel.getmeldCardStr();
         this._gameRoom.updateGroupCards(meldCardStr);
         }
         if (/!*timer >= (maxTime - timer) || *!/(timer - timerCount) == 0) {
         timerCount = 0;
         clearInterval(this.progressTimerInterval);
         }
         }.bind(this), 1000);*/
    },
    setRemainingExtraTime: function (extraTime) {
        this._remExtraTime = extraTime;
        if (this._remExtraTime < 0)
            this._remExtraTime = 0;
        this.extraTimerLabel.setString(this._remExtraTime.toString());
    },
    hideTimer: function () {
        this.progressTimer && this.progressTimer.setVisible(false);
        this.timerSprite && this.timerSprite.setVisible(false);
    },
    getProgBarColor: function (maxVal, progress) {
        var halfVal = maxVal / 2;
        var rValue;
        var gValue;
        if (progress > halfVal) {
            rValue = (255 * (maxVal - progress)) / halfVal;
            gValue = 255;
        } else if (progress < halfVal) {
            rValue = 255;
            gValue = (255 * progress) / halfVal;
        } else {
            rValue = 255;
            gValue = 255;
        }
        return cc.color(rValue, gValue, 0);
    },
    setSmileyIcon: function (smiley) {
        if (this.smileySprite)
            this.smileySprite.removeFromParent(true);

        this.smileySprite = new cc.Sprite(spriteFrameCache.getSpriteFrame(smiley + ".png"));
        this.smileySprite.setScale(0.5);
        this.smileySprite.setPosition(cc.p(this.profileBase.x + this.profileBase.width / 4 + 5 * scaleFactor, this.profileBase.y - 10 * scaleFactor/*+ this.profileBase.height / 4 - 10*/));
        this.addChild(this.smileySprite, 12, "smileySprite");
    },
    removeSmileyIcon: function () {
        this.smileySprite && this.smileySprite.removeFromParent(true);
        this.smileySprite = null;
    },
    setDealerIcon: function (bool) {
        if (bool) {
            this.dealerSprite = new cc.Sprite(spriteFrameCache.getSpriteFrame("dealerIcon.png"));
            this.dealerSprite.setPosition(cc.p(this.width - 5 * scaleFactor, this.height - 5 * scaleFactor));
            this.addChild(this.dealerSprite, 20, "dealerSprite");
        }
        else
            this.removeDealerIcon();

    },
    removeDealerIcon: function () {
        this.dealerSprite && this.removeChild(this.dealerSprite, true);
        this.dealerSprite = null;
    },
    updateProfileIcon: function () {
        this.removeIcon();
        if (this.isDropped || this.isautoPlay || this.isWrongShow || this.isDisconnected) {
            if (this.isDisconnected) {
                this.setIcon("Disconnected", "WrongShowIcon");
            }
            if (this.isautoPlay) {
                this.setIcon("Auto Mode", "autoPlay");
            }
            if (this.isDropped) {
                this.setIcon("Dropped", "DropIcon");
            }
            if (this.isWrongShow) {
                this.setIcon("Wrong Show", "wrongShow");
            }
        }
    },
    updateRank: function (rank, chip) {
        this.setScore(chip, rank);
    },
    setIcon: function (text, iconSpriteName) {
        if (!text || !iconSpriteName) {
            return;
        }
        this.removeIcon();

        this.transparentSprite = new cc.Sprite(spriteFrameCache.getSpriteFrame("cardDirty.png"));
        this.transparentSprite.setColor(cc.color(0, 0, 0));
        this.transparentSprite.setOpacity(160);
        this.transparentSprite.setScale(1.08, 1.01);
        this.transparentSprite.setPosition(cc.p(this.width / 2, this.height / 2 - 15 * scaleFactor));
        this.addChild(this.transparentSprite, 6, "transparentSprite");

        this.iconText = new cc.LabelTTF(text, "RobotoBold", 12 * 2 * scaleFactor);
        this.iconText.setScale(0.5);
        this.iconText.setPosition(cc.p(this.profileBase.x, this.height / 2 + 12 * scaleFactor - yMargin));
        this.addChild(this.iconText, 7, "iconText");

        if (text == "Dropped") {
            this.dropPoints = new cc.LabelTTF("", "RobotoBold", 12 * 2 * scaleFactor);
            this.dropPoints.setScale(0.5);
            this.dropPoints.setPosition(cc.p(this.profileBase.x, this.height / 2 + 5 * scaleFactor - yMargin));

            this.iconText.setPosition(cc.p(this.profileBase.x, this.height / 2 + 19 * scaleFactor - yMargin));
            this.addChild(this.dropPoints, 7, "dropPoints");
        }

        this.iconSprite = new cc.Sprite(spriteFrameCache.getSpriteFrame(iconSpriteName + ".png"));
        this.iconSprite.setPosition(cc.p(this.profileBase.x, this.height / 4));
        this.addChild(this.iconSprite, 7, "iconSprite");
    },
    removeIcon: function () {
        this.transparentSprite && this.transparentSprite.removeFromParent(true);
        this.iconText && this.iconText.removeFromParent(true);
        this.iconSprite && this.iconSprite.removeFromParent(true);
        this.dropPoints && this.dropPoints.removeFromParent(true);
    },
    setProfilePic: function (imageUrl) {
        // imageUrl = 'https://test-weaver.s3-ap-southeast-1.amazonaws.com/commonContent/dev.khelplayrummy.com/playerImages/246_image.png';
        // imageUrl = 'https://khelplay-weaver.s3.amazonaws.com/commonContent/www.khelplayrummy.com/playerImages/avatar1.jpg';
        var self = this;
        if (imageUrl && imageUrl.indexOf("null") == -1 && imageUrl != "") {
            if ((imageUrl.indexOf("edit-thumbnai.jpg") >= 0)) {
                this.removeProfilePic();
            } else {
                var image = new Image();
                image.onload = function () {
                    cc.log("Image successfully loaded");
                    // cc.textureCache.addImageAsync(imageUrl, this.imageLoaded, this);
                    self.imgUrl = imageUrl;
                    self.imgData = image;
                    self.imageLoaded(image, image.width, image.height);
                };
                image.src = imageUrl;
                image.onerror = function () {
                    self.removeProfilePic();
                    cc.log("Image url is invalid");
                };
            }
        } else {
            this.removeProfilePic();
        }
    },
    imageLoaded: function (imagePath, width, height) {
        var spriteAvatar;
        if (imagePath) {
            spriteAvatar = new cc.Sprite(imagePath);
            spriteAvatar.width = width;
            spriteAvatar.height = height;
        } else {
            spriteAvatar = new cc.Sprite(spriteFrameCache.getSpriteFrame("ProfileImage.png"));
        }
        var scaleFactor = this.getAvatarScaleFactor(spriteAvatar);
        spriteAvatar.setScale(scaleFactor.x, scaleFactor.y);
        spriteAvatar.setPosition(cc.p(this.clippingNode.width / 2, this.clippingNode.height / 2));
        this.clippingNode.removeAllChildren(true);
        this.clippingNode.addChild(spriteAvatar, 2);
    },
    _initClippingNode: function () {
        this.clippingNode = new ccui.Layout();
        this.clippingNode.setContentSize(cc.size(75 * scaleFactor, 70 * scaleFactor));
        this.clippingNode.setClippingEnabled(true);
        this.clippingNode.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.clippingNode.setBackGroundColor(cc.color(51, 55, 57));
        this.clippingNode.setPosition(cc.p(3 * scaleFactor, 3 * scaleFactor));
        this.addChild(this.clippingNode, 2, "profilePic");
        this.removeProfilePic();
    },
    getAvatarScaleFactor: function (avatar) {
        var tmpSize = avatar.getContentSize();
        var mask = spriteFrameCache.getSpriteFrame("Mask.png").getRect();
        return {
            x: mask.width / tmpSize.width,
            y: mask.height / tmpSize.height
        };
    },
    removeProfilePic: function () {
        var spriteAvatar = new cc.Sprite(spriteFrameCache.getSpriteFrame("ProfileImage.png"));
        var scaleFactor = this.getAvatarScaleFactor(spriteAvatar);
        spriteAvatar.setScale(scaleFactor.x, scaleFactor.y);
        spriteAvatar.setPosition(cc.p(this.clippingNode.width / 2, this.clippingNode.height / 2));
        this.clippingNode.removeAllChildren(true);
        this.clippingNode.addChild(spriteAvatar, 2);
    }
}), PlayerToolTip = cc.Scale9Sprite.extend({
    ctor: function () {
        this._super(spriteFrameCache.getSpriteFrame("blackBoxPopUp.png"), cc.rect(0, 0, 50 * scaleFactor, 50 * scaleFactor));
        this.setCapInsets(cc.rect(16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor));
        this.setContentSize(cc.size(130 * scaleFactor, 30 * scaleFactor));

        var arrow = new cc.Sprite(spriteFrameCache.getSpriteFrame("arrow.png"));
        arrow.setRotation(90);
        arrow.setColor(cc.color(0, 0, 0));
        arrow.setPosition(cc.p(this.width / 2, this.height));
        this.addChild(arrow, 2);

        this.playerName = new cc.LabelTTF("", "RobotoRegular", 2 * 13 * scaleFactor); //Full player name to be displayed in placeholder
        this.playerName.setScale(0.5);
        this.playerName.setColor(cc.color(255, 255, 255));
        this.playerName.setPosition(cc.p(this.width / 2, this.height / 2 - yMargin));
        this.addChild(this.playerName, 3);

        return true;
    },
    hide: function (bool) {
        this.setVisible(!bool);
    },
    setPlayerName: function (playerName) {
        this.playerName.setString(playerName);
    }
});
