/**
 * Created by stpl on 1/20/2017.
 */
var RoundProgress = cc.Layer.extend({
    _tournamentDetail: null,
    ctor: function (context, room) {
        this._tournamentDetail = context;

        this._super();
        this.trnmntRoom = room;
        this.rId = room.id;
        this.setContentSize(cc.size(720 * scaleFactor, 390 * scaleFactor));
        this.setPosition(cc.p(220 * scaleFactor, 3 * scaleFactor));

        this.initHeader();
        this.initListView();

        /*this.trnmntRoom = room;

         var roundBtn;
         var totRound = this._tournamentDetail.TOT_ROUND;
         var curRound = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value;*/

    },
    initHeader: function () {

        var headerStrip = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("pentagon.png"));
        headerStrip.setCapInsets(cc.rect(5 * scaleFactor, 7 * scaleFactor, 5 * scaleFactor, 7 * scaleFactor));
        headerStrip.setContentSize(cc.size(719 * scaleFactor, 45 * scaleFactor));
        headerStrip.setFlippedX(true);
        headerStrip.setAnchorPoint(0, 0.5);
        headerStrip.setColor(cc.color(200, 200, 200));
        headerStrip.setCascadeColorEnabled(false);
        headerStrip.setPosition(cc.p(720 * scaleFactor, 362 * scaleFactor));
        this.addChild(headerStrip, 5);

        var roundSprite = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("pentagon.png"));
        roundSprite.setCapInsets(cc.rect(5 * scaleFactor, 7 * scaleFactor, 5 * scaleFactor, 7 * scaleFactor));
        roundSprite.setContentSize(cc.size(123 * scaleFactor, 45 * scaleFactor));
        roundSprite.setAnchorPoint(0, 0.5);
        roundSprite.setPosition(cc.p(1 * scaleFactor, 362 * scaleFactor));
        this.addChild(roundSprite, 10);

        var divider = new cc.Sprite(spriteFrameCache.getSpriteFrame("traingleLine.png"));
        divider.setPosition(cc.p(115 * scaleFactor, 362 * scaleFactor));
        this.addChild(divider, 10);

        var tableSprite = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("pentagon.png"));
        tableSprite.setCapInsets(cc.rect(5 * scaleFactor, 7 * scaleFactor, 5 * scaleFactor, 7 * scaleFactor));
        tableSprite.setContentSize(cc.size(170 * scaleFactor, 45 * scaleFactor));
        tableSprite.setAnchorPoint(0, 0.5);
        tableSprite.setPosition(cc.p(110 * scaleFactor, 362 * scaleFactor));
        this.addChild(tableSprite, 8);

        var label;
        label = new cc.LabelTTF("Round", "RobotoBold", 20 * scaleFactor);
        label.setScale(0.7);
        label.setPosition(cc.p(45 * scaleFactor, 362 * scaleFactor));
        this.addChild(label, 12);

        label = new cc.LabelTTF("Table", "RobotoBold", 20 * scaleFactor);
        label.setScale(0.7);
        label.setPosition(cc.p(156 * scaleFactor, 362 * scaleFactor));
        this.addChild(label, 12);

        label = new cc.LabelTTF("Deal", "RobotoBold", 20 * scaleFactor);
        label.setScale(0.7);
        label.setPosition(cc.p(223 * scaleFactor, 362 * scaleFactor));
        this.addChild(label, 12);

        label = new cc.LabelTTF("Players", "RobotoBold", 20 * scaleFactor);
        label.setScale(0.7);
        label.setPosition(cc.p(324 * scaleFactor, 362 * scaleFactor));
        this.addChild(label, 12);

        label = new cc.LabelTTF("Chips", "RobotoBold", 20 * scaleFactor);
        label.setScale(0.7);
        label.setPosition(cc.p(398 * scaleFactor, 362 * scaleFactor));
        this.addChild(label, 12);

        label = new cc.LabelTTF("Status", "RobotoBold", 20 * scaleFactor);
        label.setScale(0.7);
        label.setPosition(cc.p(462 * scaleFactor, 362 * scaleFactor));
        this.addChild(label, 12);

    },
    initListView: function () {

        var rect = new cc.DrawNode();
        rect.drawRect(cc.p(1 * scaleFactor, 46 * scaleFactor), cc.p(719 * scaleFactor, (339 + 46) * scaleFactor), cc.color(162, 162, 162), 1, cc.color(162, 162, 162));
        this.addChild(rect, 0);

        this.roundListView = new ccui.ListView();
        this.roundListView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        this.roundListView.setGravity(ccui.ListView.GRAVITY_CENTER_HORIZONTAL);
        this.roundListView.setScrollBarAutoHideEnabled(true);
        this.roundListView.setTouchEnabled(true);
        this.roundListView.setItemsMargin(0);
        this.roundListView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.roundListView.setBackGroundColor(cc.color(255, 255, 255));
        this.roundListView.setContentSize(cc.size(110 * scaleFactor, 293 * scaleFactor));
        this.roundListView.setPosition(cc.p(1 * scaleFactor, 46 * scaleFactor));
        this.roundListView.addEventListener(this.roundListViewListener.bind(this));
        this.addChild(this.roundListView, 1);

        this.tableListView = new ccui.ListView();
        this.tableListView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        this.tableListView.setGravity(ccui.ListView.GRAVITY_CENTER_HORIZONTAL);
        this.tableListView.setScrollBarAutoHideEnabled(true);
        this.tableListView.setTouchEnabled(true);
        this.tableListView.setItemsMargin(0);
        this.tableListView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.tableListView.setBackGroundColor(cc.color(255, 255, 255));
        this.tableListView.setContentSize(cc.size(157 * scaleFactor, 293 * scaleFactor));
        this.tableListView.setPosition(cc.p(112 * scaleFactor, 46 * scaleFactor));
        this.tableListView.addEventListener(this.tableListViewListener.bind(this));
        this.addChild(this.tableListView, 1);

        this.playerInfoListView = new ccui.ListView();
        this.playerInfoListView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        this.playerInfoListView.setGravity(ccui.ListView.GRAVITY_CENTER_HORIZONTAL);
        this.playerInfoListView.setScrollBarAutoHideEnabled(true);
        this.playerInfoListView.setTouchEnabled(true);
        this.playerInfoListView.setItemsMargin(0);
        this.playerInfoListView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.playerInfoListView.setBackGroundColor(cc.color(255, 255, 255));
        this.playerInfoListView.setContentSize(cc.size((439 + 10) * scaleFactor, 293 * scaleFactor));
        this.playerInfoListView.setPosition(cc.p(270 * scaleFactor, 46 * scaleFactor));
        this.addChild(this.playerInfoListView, 1);

        var structStr = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.STRUCTURE).value;
        if (structStr && (structStr == "" || structStr == " "))
            return;
        var roundStrArr = structStr.split(";");
        var tempCurRnd;

        /* for (var i:int = 0; i < totRound ; i++) {

         rndDisplayArr = roundStrArr[i].split("#");
         //roundInfoArr = rndDisplayArr[1].split(",");
         roundBtnMC = new TournamentAssets.RoundBtnMC();
         roundBtnMC.gotoAndStop(int(i % 2) + 1);
         roundBtnMC.rndTxt.text = String("Round "+(i+1));
         roundBtnMC.y = (totRound - (i + 1)) * (roundBtnMC.height);
         tempCurRnd = String(rndDisplayArr[0]);  //String(rndDisplayArr[1]).split(",")[0];
         roundBtnMC.name = String(tempCurRnd + ":" + "Round " + (i + 1)+":"+rndDisplayArr[0]);
         roundBtnMC.buttonMode = true;
         if (curRound==int(tempCurRnd))
         roundBtnMC.curRndMC.visible = true;
         else
         roundBtnMC.curRndMC.visible = false;
         roundBtnMC.addEventListener(MouseEvent.CLICK, roundBtnHandler);
         //prgrssHeader.rndRefPt.addChild(roundBtnMC);
         rndContainer.addChild(roundBtnMC);
         }*/

        var _isShowRoundIcon = false;
        var curRound = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value;
        this.roundListView.removeAllChildrenWithCleanup(true);


        var index = 0;
        for (var i = roundStrArr.length, j; i > 0; i--) {
            _isShowRoundIcon = false;
            var rndDisplayArr = roundStrArr[i-1].split("#");
            tempCurRnd = (rndDisplayArr[0]).toString();
            if (curRound == parseInt(tempCurRnd))
                _isShowRoundIcon = true;
            j = new RoundTime({}, this, i, rndDisplayArr[0], _isShowRoundIcon);

            this.roundListView.pushBackCustomItem(j);
            index++;
        }
        /*

        for (var i = 0, j; i < roundStrArr.length; i++) {
            _isShowRoundIcon = false;
            var rndDisplayArr = roundStrArr[i].split("#");
            tempCurRnd = (rndDisplayArr[0]).toString();
            if (curRound == parseInt(tempCurRnd))
                _isShowRoundIcon = true;
            j = new RoundTime({}, this, i, rndDisplayArr[0], _isShowRoundIcon);

            this.roundListView.pushBackCustomItem(j);
        }*/
        this.roundToolTip = new ToolTip(cc.size(140 * scaleFactor, 30 * scaleFactor), cc.color(255, 238, 215));
        this.roundToolTip.setAnchorPoint(1, 1);
        this.addChild(this.roundToolTip, 10, 555);
        this.roundToolTip.setPosition(cc.p(this.tableListView.x + 150 * scaleFactor, this.tableListView.y + 200 * scaleFactor));
        this.roundToolTip.setText("Please select round");
        this.roundToolTip.setVisible(true);
    },
    roundListViewListener: function (sender, type) {
        switch (type) {
            case ccui.ListView.ON_SELECTED_ITEM_END :
                var listViewEx = sender;
                var item = listViewEx.getItem(listViewEx.getCurSelectedIndex());
                this.selectedRound && this.selectedRound.changeState(false);
                this.selectedRound = item;
                item.onClickHandler();
                break;
            default:
                break;
        }
    },
    tableListViewListener: function (sender, type) {
        switch (type) {
            case ccui.ListView.ON_SELECTED_ITEM_END :
                var listViewEx = sender;
                var item = listViewEx.getItem(listViewEx.getCurSelectedIndex());
                this.selectedTable && this.selectedTable.changeState(false);
                this.selectedTable = item;
                item.onClickHandler();
                break;
            default:
                break;
        }
    },
    showCurrentRound: function () {
        var roundList = this.roundListView.getChildren();
        for (var i = 0; i < roundList.length; i++) {
            roundList[i].showRound(false);
        }
        var curRound = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value;
        roundList = this.roundListView.getChildByTag(curRound);
        roundList.showRound(true);
    },
    cleanUpLevelProgress: function () {
        this.tableListView.removeAllChildrenWithCleanup(true);
        this.playerInfoListView.removeAllChildrenWithCleanup(true);
        this.selectedRound && this.selectedRound.changeState(false);
        this.selectedTable && this.selectedTable.changeState(false);
        this.removeChildByTag(555, true);
        this.roundToolTip = new ToolTip(cc.size(140 * scaleFactor, 30 * scaleFactor), cc.color(255, 238, 215));
        this.roundToolTip.setAnchorPoint(1, 1);
        this.addChild(this.roundToolTip, 10, 555);
        this.roundToolTip.setPosition(cc.p(this.tableListView.x + 150 * scaleFactor, this.tableListView.y + 200 * scaleFactor));
        this.roundToolTip.setText("Please select round");
        this.roundToolTip.setVisible(true);
    },
    showLevelProgress: function (dataObj) {
        this.tableListView.removeAllChildrenWithCleanup(true);
        this.playerInfoListView.removeAllChildrenWithCleanup(true);
        this.removeChildByTag(555, true);
        if (dataObj["msgStr"] == "-1") {
            var tableInfoStr = dataObj["levelInfo"];
            var tableInfoArr = tableInfoStr.split(",");

            for (var i = 0, j; i < tableInfoArr.length; i++) {
                j = new TableDeal(tableInfoArr[i], this, i);
                this.tableListView.pushBackCustomItem(j);
            }
        }
        this.roundToolTip = new ToolTip(cc.size(170 * scaleFactor, 46 * scaleFactor), cc.color(255, 238, 215));
        this.roundToolTip.setAnchorPoint(1, 1);
        this.addChild(this.roundToolTip, 10, 555);
        this.roundToolTip.setPosition(cc.p(this.playerInfoListView.x + 300 * scaleFactor, this.playerInfoListView.y + 200 * scaleFactor));
        this.roundToolTip.setText("Please select table");
        this.roundToolTip.setVisible(true);
    },
    showTableProgress: function (dataObj) {
        this.playerInfoListView.removeAllChildrenWithCleanup(true);
        this.removeChildByTag(555, true);
        var playerStr = dataObj["roomInfo"];
        var playerStrArr = playerStr.split(";");

        for (var i = 0, j; i < playerStrArr.length; i++) {
            j = new StatusBar(playerStrArr[i], this, i);
            this.playerInfoListView.pushBackCustomItem(j);
        }
    }
});

var RoundTime = ccui.Layout.extend({
    _parentClass: null,
    _roundNo: null,
    ctor: function (params, context, index, roundNo, _isShowRoundIcon) {
        this._super();
        this._parentClass = context;
        this._roundNo = index;
        this.setTag(roundNo - 1);

        this.setContentSize(cc.size(109 * scaleFactor, 30 * scaleFactor));
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_GRADIENT);
        this.setCascadeColorEnabled(false);
        this.setTouchEnabled(true);

        if (index % 2 == 0) {
            this.defaultColor = cc.color(235, 235, 235);
        } else this.defaultColor = cc.color(255, 255, 255);

        this.setBackGroundColor(this.defaultColor, this.defaultColor);

        this.round = new cc.LabelTTF("Round " + (index), "RobotoRegular", 20 * scaleFactor);
        this.round.setScale(0.6);
        this.round.setColor(cc.color(0, 0, 0));
        this.round.setPosition(cc.p(44 * scaleFactor, this.height / 2));
        this.addChild(this.round, 1, parseInt(roundNo));

        this.arrow = new cc.Sprite(spriteFrameCache.getSpriteFrame('arrow.png'));
        this.arrow.setFlippedX(true);
        this.arrow.setScale(0.3);
        this.arrow.setPosition(cc.p(this.width - 15 * scaleFactor, this.height / 2));
        this.arrow.setVisible(false);
        this.addChild(this.arrow, 1);

        if (_isShowRoundIcon)
            this.showRound(true);

    },
    showRound: function (bool) {
        if (bool) {
            this.currentRoundIcon && this.currentRoundIcon.removeFromParent(true);
            this.currentRoundIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame('CurrentRoundIcon.png'));
            this.currentRoundIcon.setPosition(cc.p(this.currentRoundIcon.width / 2, this.height / 2));
            this.addChild(this.currentRoundIcon, 5, 'currentRoundIcon');
        } else {
            this.currentRoundIcon && this.currentRoundIcon.removeFromParent(true);
        }
    },
    onClickHandler: function () {
        this.changeState(true);
        var levelObj = {levelNo: this.round.getTag()};
        sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.LEVEL_PRGRSS_INFO, levelObj, this._parentClass.rId);
    },
    changeState: function (pressed) {
        if (pressed) {
            this.setBackGroundColor(cc.color(133, 151, 9), cc.color(102, 116, 1));
            this.arrow.setVisible(true);
            this.round.setColor(cc.color(255, 255, 255));

        } else {
            this.setBackGroundColor(this.defaultColor, this.defaultColor);
            this.arrow.setVisible(false);
            this.round.setColor(cc.color(0, 0, 0));
        }
    }
});

var TableDeal = ccui.Layout.extend({
    _parentClass: null,
    ctor: function (params, context, index) {
        this._super();
        this._parentClass = context;

        this.setContentSize(cc.size(157 * scaleFactor, 30 * scaleFactor));
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_GRADIENT);
        this.setCascadeColorEnabled(false);
        this.setTouchEnabled(true);

        if (index % 2 == 0) {
            this.defaultColor = cc.color(235, 235, 235);
        } else this.defaultColor = cc.color(255, 255, 255);

        this.setBackGroundColor(this.defaultColor, this.defaultColor);

        var tableArr = (params).split(":");

        this.table = new cc.LabelTTF("Table " + tableArr[0], "RobotoRegular", 20 * scaleFactor);
        this.table.setScale(0.6);
        this.table.setColor(cc.color(0, 0, 0));
        this.table.setPosition(cc.p(45 * scaleFactor, this.height / 2));
        this.addChild(this.table, 1, parseInt(tableArr[2]));

        this.deal = new cc.LabelTTF(tableArr[1], "RobotoRegular", 20 * scaleFactor);
        this.deal.setScale(0.6);
        this.deal.setColor(cc.color(0, 0, 0));
        this.deal.setPosition(cc.p((45 + 67) * scaleFactor, this.height / 2));
        this.addChild(this.deal, 1);

        this.arrow = new cc.Sprite(spriteFrameCache.getSpriteFrame('arrow.png'));
        this.arrow.setFlippedX(true);
        this.arrow.setScale(0.3);
        this.arrow.setPosition(cc.p(this.width - 15 * scaleFactor, this.height / 2));
        this.arrow.setVisible(false);
        this.addChild(this.arrow, 1);

    },
    onClickHandler: function () {
        this.changeState(true);
        var levelObj = {roomId: this.table.getTag()};
        sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.ROOM_PRGRSS_INFO, levelObj, this._parentClass.rId);
    },
    changeState: function (pressed) {
        if (pressed) {
            this.setBackGroundColor(cc.color(133, 151, 9), cc.color(102, 116, 1));
            this.arrow.setVisible(true);
            this.table.setColor(cc.color(255, 255, 255));
            this.deal.setColor(cc.color(255, 255, 255));
        } else {
            this.setBackGroundColor(this.defaultColor, this.defaultColor);
            this.arrow.setVisible(false);
            this.table.setColor(cc.color(0, 0, 0));
            this.deal.setColor(cc.color(0, 0, 0));
        }
    }
});

var StatusBar = ccui.Layout.extend({
    _parentClass: null,
    ctor: function (params, context, index) {
        this._super();
        this._parentClass = context;

        this.setContentSize(cc.size((440 + 8) * scaleFactor, 30 * scaleFactor));
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.setCascadeColorEnabled(false);

        if (index % 2 == 0) {
            this.defaultColor = cc.color(235, 235, 235);
        } else this.defaultColor = cc.color(255, 255, 255);
        this.setBackGroundColor(this.defaultColor);

        var playerArr = (params).split(",");

        if (playerArr[0].length > 10) {
            playerArr[0] = playerArr[0].substring(0, 10) + '..';
        }
        var label = new cc.LabelTTF(playerArr[0], "RobotoRegular", 18 * scaleFactor); // player
        label.setColor(cc.color(0, 0, 0));
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(31 * scaleFactor, this.height / 2));
        this.addChild(label, 5);

        label = new cc.LabelTTF(playerArr[1], "RobotoRegular", 18 * scaleFactor); // chip
        label.setColor(cc.color(0, 0, 0));
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(110 * scaleFactor, this.height / 2));
        this.addChild(label, 5);

        var labelStr = playerArr[2];

        if (playerArr[3]) {
            labelStr += PrizeStringCalculator.calculateStructPrzStr(playerArr[3]);
        }
        labelStr += " " + playerArr[4];
        label = new cc.LabelTTF(labelStr, "RupeeFordian", 15 * scaleFactor); // status
        label.setColor(cc.color(0, 0, 0));
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7, 0.8);
        label.setPosition(cc.p(171 * scaleFactor, this.height / 2));
        this.addChild(label, 5);

    },
    changeState: function (state) {
        if (state == 1) this.setColor(this.defaultColor);
        else this.setBackGroundColor(cc.color(167, 181, 69));
    }
});