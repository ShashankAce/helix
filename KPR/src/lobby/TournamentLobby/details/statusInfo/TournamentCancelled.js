/**
 * Created by stpl on 1/20/2017.
 */
var TournamentCancelled = StatusInfo.extend({
    ctor: function (context) {
        this._super();

        this._super(cc.color(234, 234, 234));
        this.setContentSize(cc.size(200 * scaleFactor, 500 * scaleFactor));
        this.setPosition(cc.p(1 * scaleFactor, 1 * scaleFactor));

        this._drawLines(4);
    }
});