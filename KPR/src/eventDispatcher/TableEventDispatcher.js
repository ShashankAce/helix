/**
 * Created by stpl on 9/27/2016.
 */
"use strict";
var TableEventDispatcher = cc.Class.extend({
    _gameInterface: null,
    ctor: function (context) {
        this._gameInterface = context;
    },
    dispatchSFSEvents: function (cmd, room, params) {
        switch (cmd) {
            case SFSConstants.CMD_NO_BAL:
                this._gameInterface.noBalanceResp(params.msg, params.isConfirm);
                break;
            case SFSConstants.CMD_ROOMSTATUS:
                // connected back code
                this._gameInterface.processRoomStatus(params, room);
                break;
            case SFSConstants.CMD_AVATAR:
                this._gameInterface.processAvatar(params);
                break;
            case SFSConstants.CMD_SEATCONFRESP:
                this._gameInterface.seatConfirmResponse(params, room);
                break;
            case SFSConstants.CMD_WAITTIMER :
                this._gameInterface.processWaitTimer(params, room);
                break;
            case SFSConstants.CMD_STOPTIME :
                this._gameInterface.processStopTimer(params);
                break;
            case SFSConstants.CMD_DISABLELEAVE :
                this._gameInterface.processDisableLeave();
                break;
            case SFSConstants.CMD_FREESEAT :
                this._gameInterface.processFreeSeat(params);
                break;
            case SFSConstants.CMD_TOSS :
                this._gameInterface.processToss(params);
                break;
            case SFSConstants.CMD_CUTTHEDECK :
                this._gameInterface.processCutTheDeckCmd(params);
                break;
            case SFSConstants.CMD_CUTTHEDECKRESP :
                this._gameInterface.processCutTheDeckResp(params);
                break;
            case SFSConstants.CMD_UCARD :
                this._gameInterface.processUserCards(params, parseInt(params[SFSConstants._SROOM]));
                break;
            case SFSConstants.CMD_EXTRATIMER :
                this._gameInterface.processExtraTimer(params);
                break;
            case SFSConstants.CMD_MELDSTATUS :
                // in case of disconnection
                this._gameInterface.setMeldStatus(params);
                break;
            case SFSConstants.CMD_MELDRESP :
                this._gameInterface.handleMeldResp(params);
                break;
            case SFSConstants.CMD_PLREXTRATIMER :
                this._gameInterface.processPlrExtraTimer(params);
                break;
            case SFSConstants.CMD_INACTIVEPLAYER :
                this._gameInterface.processInActivePlayer(params);
                break;
            case SFSConstants.CMD_ACTIVEPLAYER :
                this._gameInterface.processActivePlayer(params);
                break;
            case SFSConstants.CMD_PLAYERDISCONNECTED :
                this._gameInterface.processPlayerDisconnected(params);
                break;
            case SFSConstants.CMD_DROPRESP :
                this._gameInterface.processDropResp(params);
                break;
            case SFSConstants.CMD_PICKRESP :// for open deck
                this._gameInterface.handleOpenDeckResp(params);
                break;
            case SFSConstants.CMD_CLOSEDCARD :// for closed deck
                this._gameInterface.handleClosedDeckResp(params);
                break;
            case SFSConstants.CMD_RESULT :
                this._gameInterface.handleResult(params);
                break;
            case SFSConstants.CMD_LASTRESULT_RESP:
                this._gameInterface.showLastResult(params);
                break;
            case SFSConstants.CMD_GAMEOVER :
                this._gameInterface.processGameOver(params);
                break;
            case SFSConstants.CMD_RNDOVER :
                this._gameInterface.processRoundOver(params);
                break;
            case SFSConstants.CMD_EXTRATIMEUSERLIST :
                this._gameInterface.processExtraTimeUserList(params);
                break;
            case SFSConstants.CMD_REJOIN_RESP:
                this._gameInterface.handleRejoinResp(params);
                break;
            case SFSConstants.CMD_SPLIT_RESP:
                this._gameInterface.handleSplitResp(params);
                break;
            case SFSConstants.CMD_AUTO_SPLIT:
                this._gameInterface.processAutoSplitResponse(params);
                break;
            case SFSConstants.CMD_NO_TABLE_BAL:
                this._gameInterface.noTableBal(params);
                break;
            case SFSConstants.CMD_RESHUFFLE:
                this._gameInterface.processClosedDeckReshuffle();
                break;
            case SFSConstants.CMD_ZONEPLS:
                var count = parseInt(params[SFSConstants._NUM]);
                // this._gameInterface.updateOnlinePlsInfo(count);
                break;
            case SFSConstants.CMD_HISTORYCARD_RESP:
                this._gameInterface.historyRespHandler(params[SFSConstants.DATA_STR]);
                break;
            case SFSConstants.CMD_SMILERESP:
                this._gameInterface.smileyResp(params);
                cc.log("smiley ");
                break;
            case SFSConstants.CMD_SCORECARD_PLAYERSTR:
                cc.log("score card player");
                this._gameInterface.processPlayerStrResp(params);
                break;
            case SFSConstants.CMD_SCORECARD_RESP:
                this._gameInterface.processScoreBoard(params);
                cc.log("score card ");
                break;
            case SFSConstants.CMD_BONUSTOCASH:
                cc.log("bonus to cash");
                 this._gameInterface.processBonusToCash(params[SFSConstants.FLD_AMOUNT]);
                break;
            case SFSConstants.CMD_POINTS_UPDATE:
                this._gameInterface.updatePoints(params);
                break;
            case SFSConstants.CMD_PLAYERCONNECT:
                this._gameInterface.playerConnect(params);
                break;
            case SFSConstants.CMD_PLAYER_OVER:
                this._gameInterface.handlePlayerOver(params);
                break;
            case SFSConstants.TIE_BREAKER_ROUND:
                this._gameInterface.handleTieBreaker(params);
                break;
            case SFSConstants.WAIT_FOR_TIE_BREAKER_ROUND:
                this._gameInterface.handleWaitForTieBreaker(params);
                break;
            case SFSConstants.REMOVE_RESULT:
                this._gameInterface.removeResult(params);
                break;
            case SFSConstants.LEVEL_END_WARNING:
                this._gameInterface.setLevelEndWarning(params);
                break;
            case SFSConstants.IS_REJOIN:
                this._gameInterface.handleIsRejoin(params);
                break;
            case SFSConstants.TOURNAMENT_RE_JOIN_RESP:
                this._gameInterface.handleTourRejoinResp(params);
                break;
            case SFSConstants.DROP_WAITING:
                this._gameInterface.handleDropWaiting(params);
                break;
            case SFSConstants.TEA_TIME:
                this._gameInterface.handleTeaTime(params);
                break;
            case SFSConstants.ADD_REBUY_CASH_RESP:
                this._gameInterface.addRebuyCash(params);
                break;
            case SFSConstants.UPDATE_DEAL:
                this._gameInterface.updateDeal(params);
                break;
            case SFSConstants.LEVEL_EXTRA_TIME_NOTIFICATION:
                this._gameInterface.handleLevelExtraTime(params);
                break;
            case SFSConstants.TOURNAMENT_INFO:
                this._gameInterface.handleTourInfo(params);
                break;
            case SFSConstants.LEVEL_CMPLT_STATUS:
                this._gameInterface.showLevelStatusMsg(params);
                break;
            case SFSConstants.UPDATE_PLAYER_RANK:
                this._gameInterface.updatePlayerRank(params);
                break;

        }
    }
});