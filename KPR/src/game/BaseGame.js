/**
 * Created by stpl on 9/16/2016.
 */
var BaseGame = cc.Layer.extend({
    _name: "BaseGame",
    _chairPosition: null,
    _tossCardPosition: null,
    roomId: null,
    tableId: null,
    // tableEventDispatcher: null,
    cutDeckPanel: null,
    _userCardPanel: null,
    noOfPlayer: null,
    ccTimeOutManager: null,
    notification: null,
    jokerToolTip: null,
    smileyAndChatDisableToolTip: null,
    totCards: null,
    scoreBoardPanel: null,
    historyPanel: null,
    _tabMc: null,
    ungroupedCardCount: null,
    _playerArray: null,
    _closedCards: null,
    _canLeaveRoom: null,
    _dropped: null,
    _currentRound: null,
    _myId: null,
    _myUName: null,
    _actualTurnTime: null,
    _currentRoomState: null,

    ctor: function (room) {
        if (!room) {
            throw "You must pass room as argument";
        }
        this._super();
        // this.tableEventDispatcher = new TableEventDispatcher(this);
        this.ccTimeOutManager = new CCTimeOutManager();

        this._serverRoom = room;
        this.tableId = this._serverRoom.id;

        this.roomId = this._serverRoom.getVariable(SFSConstants._ROOMID).value; // tableId to display on gameInfo header
        this._roomId = room.id;  // sfsRoomId its not displayed any communication is done through this id

        this._tabMc = applicationFacade._controlAndDisplay.addRoomTab("Room" + room.name, this.tableId, room);
        this.ungroupedCardCount = 0;
        this._playerArray = [];
        this._closedCards = [];
        this._canLeaveRoom = false;
        this._leaveAllowed = true;
        this._dropped = false;
        this._currentRound = 0;
        this._myId = -1;
        this._myUName = "";
        this.lastPickCard = "";
        this._actualTurnTime = "";
        this._currentRoomState = "";

        var khelplayLogo = new cc.Sprite(res.lobby.logo);
        khelplayLogo.setScale(0.6);
        khelplayLogo.setColor(cc.color(32, 80, 50));
        khelplayLogo.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(khelplayLogo, 1);

        var upperStrip = new cc.DrawNode();
        upperStrip.drawRect(cc.p(0, 25 * scaleFactor), cc.p(cc.winSize.width, 0), cc.color(0, 0, 0), 0, cc.color(0, 0, 0));
        upperStrip.setPosition(cc.p(0, cc.winSize.height - 25 * scaleFactor));
        this.addChild(upperStrip, 1);

        this.buttomTabHeight = 28 * scaleFactor;
        var bottomStrip = new cc.DrawNode();
        bottomStrip.drawRect(cc.p(0, this.buttomTabHeight), cc.p(cc.winSize.width, 0), cc.color(0, 0, 0), 0, cc.color(0, 0, 0));
        bottomStrip.setPosition(cc.p(0, 0));
        this.addChild(bottomStrip, 1);

        this.createMenuButtons();

        this.pickWaitMC = new cc.LabelTTF("Please Wait...", "RobotoRegular", 16 * 2 * scaleFactor);
        this.pickWaitMC.setScale(0.5);
        this.pickWaitMC.setPosition(cc.p((this.width / 2) + 90 * scaleFactor, this.height / 2 + 17 * scaleFactor - yMargin));
        this.pickWaitMC.setColor(cc.color(145, 172, 155));
        this.pickWaitMC.setVisible(false);
        this.addChild(this.pickWaitMC, 2, "pickWaitMC");

        this.settingPanel = new Settings("gameRoom", this);
        this.addChild(this.settingPanel, GameConstants.settingZorder, "settingPanel");
        this.settingPanel.setVisible(false);

        this.discardPanel = new DiscardPanel(this);
        this.discardPanel.setPosition(this.width - 2 * scaleFactor, this.discardButton.y - 14 * scaleFactor);
        this.discardPanel.setVisible(false);
        this.addChild(this.discardPanel, GameConstants.discardPanelZorder);

        this.playerToolTip = new PlayerToolTip();
        this.addChild(this.playerToolTip, GameConstants.deckPanelZorder + 10);
        this.playerToolTip.hide(true);

        this.watchingTxt = new cc.LabelTTF("", "RupeeFordian", 30, cc.TEXT_ALIGNMENT_CENTER);
        this.watchingTxt.setPosition(cc.p(this.width / 2, this.height / 2 - 60 * scaleFactor - yMargin));
        this.watchingTxt.setScale(0.5);
        this.watchingTxt.setColor(cc.color(177, 194, 183));
        this.addChild(this.watchingTxt, 2);
        /*
         this._popUp = new RebuyTktPopup(this, "200", "`34", "Ticket");
         this.addChild(this._popUp, GameConstants.popupZorder, "_popUp");*/

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                cc.log("Blank listener in Base Game " + this.tableId);
                this.smileyAndChatDisableToolTip && this.smileyAndChatDisableToolTip.removeFromParent();
                applicationFacade._controlAndDisplay.closeAllTab();
                this.discardPanel.hideDiscardPanel();
                this.discardButton._isEnabled && this.discardButton.isPressed(false);
                this.settingPanel.setVisible(false);
                this.gameInfoSprite.hidePopup();
                return true;
            }.bind(this)
        }, this);

        return true;

    },
    onClickTrackNode: function (clicked) {
        var textField = this.chatEditBox;
        clicked && textField.attachWithIME();
        !clicked && textField.detachWithIME();
    },
    createSnow: function () {

        this._emitter = new cc.ParticleSnow();
        this.addChild(this._emitter, 10);

        this._emitter.life = 5;
        this._emitter.lifeVar = 1;

        // gravity
        this._emitter.gravity = cc.p(0, -10);

        // speed of particles
        this._emitter.speed = 10;
        this._emitter.speedVar = 1;

        var startColor = this._emitter.startColor;
        startColor.r = 230;
        startColor.g = 230;
        startColor.b = 230;
        this._emitter.startColor = startColor;

        var startColorVar = this._emitter.startColorVar;
        startColorVar.b = 26;
        this._emitter.startColorVar = startColorVar;

        // this._emitter.emissionRate = this._emitter.totalParticles / this._emitter.life;
        this._emitter.emissionRate = 10;

        this._emitter.texture = cc.textureCache.addImage(spriteFrameCache.getSpriteFrame("snow.png"));
        this._emitter.shapeType = cc.ParticleSystem.STAR_SHAPE;

        var sourcePos = this._emitter.getSourcePosition();
        if (sourcePos.x === 0 && sourcePos.y === 0)
            this._emitter.x = 300;
        this._emitter.y = this.height;

    },
    freeAllSeat: function () {
        this.initRoom();
    },
    handleResp: function (cmd, room, params) {
        // this.tableEventDispatcher.dispatchSFSEvents(cmd, room, params);

        switch (cmd) {
            case SFSConstants.CMD_NO_BAL:
                this.noBalanceResp(params.msg, params.isConfirm);
                break;
            case SFSConstants.CMD_ROOMSTATUS:
                // connected back code
                this.processRoomStatus(params, room);
                break;
            case SFSConstants.CMD_AVATAR:
                this.processAvatar(params);
                break;
            case SFSConstants.CMD_SEATCONFRESP:
                this.seatConfirmResponse(params, room);
                break;
            case SFSConstants.CMD_WAITTIMER :
                this.processWaitTimer(params, room);
                break;
            case SFSConstants.CMD_STOPTIME :
                this.processStopTimer(params);
                break;
            case SFSConstants.CMD_DISABLELEAVE :
                this.processDisableLeave();
                break;
            case SFSConstants.CMD_FREESEAT :
                this.processFreeSeat(params);
                break;
            case SFSConstants.CMD_TOSS :
                this.processToss(params);
                break;
            case SFSConstants.CMD_CUTTHEDECK :
                this.processCutTheDeckCmd(params);
                break;
            case SFSConstants.CMD_CUTTHEDECKRESP :
                this.processCutTheDeckResp(params);
                break;
            case SFSConstants.CMD_UCARD :
                this.processUserCards(params, parseInt(params[SFSConstants._SROOM]));
                break;
            case SFSConstants.CMD_EXTRATIMER :
                this.processExtraTimer(params);
                break;
            case SFSConstants.CMD_MELDSTATUS :
                // in case of disconnection
                this.setMeldStatus(params);
                break;
            case SFSConstants.CMD_MELDRESP :
                this.handleMeldResp(params);
                break;
            case SFSConstants.CMD_PLREXTRATIMER :
                this.processPlrExtraTimer(params);
                break;
            case SFSConstants.CMD_INACTIVEPLAYER :
                this.processInActivePlayer(params);
                break;
            case SFSConstants.CMD_ACTIVEPLAYER :
                this.processActivePlayer(params);
                break;
            case SFSConstants.CMD_PLAYERDISCONNECTED :
                this.processPlayerDisconnected(params);
                break;
            case SFSConstants.CMD_DROPRESP :
                this.processDropResp(params);
                break;
            case SFSConstants.CMD_PICKRESP :// for open deck
                this.handleOpenDeckResp(params);
                break;
            case SFSConstants.CMD_CLOSEDCARD :// for closed deck
                this.handleClosedDeckResp(params);
                break;
            case SFSConstants.CMD_RESULT :
                this.handleResult(params);
                break;
            case SFSConstants.CMD_LASTRESULT_RESP:
                this.showLastResult(params);
                break;
            case SFSConstants.CMD_GAMEOVER :
                this.processGameOver(params);
                break;
            case SFSConstants.CMD_RNDOVER :
                this.processRoundOver(params);
                break;
            case SFSConstants.CMD_EXTRATIMEUSERLIST :
                this.processExtraTimeUserList(params);
                break;
            case SFSConstants.CMD_REJOIN_RESP:
                this.handleRejoinResp(params);
                break;
            case SFSConstants.CMD_SPLIT_RESP:
                this.handleSplitResp(params);
                break;
            case SFSConstants.CMD_AUTO_SPLIT:
                this.processAutoSplitResponse(params);
                break;
            case SFSConstants.CMD_NO_TABLE_BAL:
                this.noTableBal(params);
                break;
            case SFSConstants.CMD_RESHUFFLE:
                this.processClosedDeckReshuffle();
                break;
            case SFSConstants.CMD_ZONEPLS:
                var count = parseInt(params[SFSConstants._NUM]);
                // this.updateOnlinePlsInfo(count);
                break;
            case SFSConstants.CMD_HISTORYCARD_RESP:
                this.historyRespHandler(params[SFSConstants.DATA_STR]);
                break;
            case SFSConstants.CMD_SMILERESP:
                this.smileyResp(params);
                cc.log("smiley ");
                break;
            case SFSConstants.CMD_SCORECARD_PLAYERSTR:
                cc.log("score card player");
                this.processPlayerStrResp(params);
                break;
            case SFSConstants.CMD_SCORECARD_RESP:
                this.processScoreBoard(params);
                cc.log("score card ");
                break;
            case SFSConstants.CMD_BONUSTOCASH:
                cc.log("bonus to cash");
                this.processBonusToCash(params[SFSConstants.FLD_AMOUNT]);
                break;
            case SFSConstants.CMD_POINTS_UPDATE:
                this.updatePoints(params);
                break;
            case SFSConstants.CMD_PLAYERCONNECT:
                this.playerConnect(params);
                break;
            case SFSConstants.CMD_PLAYER_OVER:
                this.handlePlayerOver(params);
                break;
            case SFSConstants.TIE_BREAKER_ROUND:
                this.handleTieBreaker(params);
                break;
            case SFSConstants.WAIT_FOR_TIE_BREAKER_ROUND:
                this.handleWaitForTieBreaker(params);
                break;
            case SFSConstants.REMOVE_RESULT:
                this.removeResult(params);
                break;
            case SFSConstants.LEVEL_END_WARNING:
                this.setLevelEndWarning(params);
                break;
            case SFSConstants.IS_REJOIN:
                this.handleIsRejoin(params);
                break;
            case SFSConstants.TOURNAMENT_RE_JOIN_RESP:
                this.handleTourRejoinResp(params);
                break;
            case SFSConstants.DROP_WAITING:
                this.handleDropWaiting(params);
                break;
            case SFSConstants.TEA_TIME:
                this.handleTeaTime(params);
                break;
            case SFSConstants.ADD_REBUY_CASH_RESP:
                this.addRebuyCash(params);
                break;
            case SFSConstants.UPDATE_DEAL:
                this.updateDeal(params);
                break;
            case SFSConstants.LEVEL_EXTRA_TIME_NOTIFICATION:
                this.handleLevelExtraTime(params);
                break;
            case SFSConstants.TOURNAMENT_INFO:
                this.handleTourInfo(params);
                break;
            case SFSConstants.LEVEL_CMPLT_STATUS:
                this.showLevelStatusMsg(params);
                break;
            case SFSConstants.UPDATE_PLAYER_RANK:
                this.updatePlayerRank(params);
                break;

        }

    },
    handleChat: function (senderName, msg) {
        this.smileyAndChatIcon.handleChat(senderName, msg);
    },
    initCutDeckPanel: function (time, player, dealer, turnBool) {
        this.cutDeckPanel = new CutDeckPanel(time, this, player, dealer, turnBool);
        this.addChild(this.cutDeckPanel, GameConstants.cutDeckPanelZorder);
    },
    createMenuButtons: function () {
        this.settingButton = new CreateBtn("settingButton", "settingButton", "settingButton");
        this.settingButton.setPosition(cc.p(this.width - 15 * scaleFactor, this.height - 12 * scaleFactor));
        this.addChild(this.settingButton, GameConstants.headerZorder);

        this.leaveTableButton = new CreateBtn("leaveTable", "leaveTable", "leaveTableButton");
        this.leaveTableButton.setPosition(cc.p(this.settingButton.x - this.leaveTableButton.width - 2 * scaleFactor, this.height - 12 * scaleFactor));
        this.addChild(this.leaveTableButton, GameConstants.headerZorder);

        this.discardButton = new DiscardButton();
        this.discardButton.setPosition(cc.p(900 * scaleFactor, this.height - 12 * scaleFactor));
        this.addChild(this.discardButton, GameConstants.headerZorder);
    },
    initMouseListener: function (element) {
        this.baseGameMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            swallowTouches: true,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    target._isEnabled && !target._hovering && target.hover(true);
                    target._hovering = true;
                    return true;
                }
                target._isEnabled && target._hovering && target.hover(false);
                target._hovering = false;
                return false;
            }.bind(this)
        });
        this.baseGameMouseListener.retain();
        cc.eventManager.addListener(this.baseGameMouseListener.clone(), this.settingButton);
        cc.eventManager.addListener(this.baseGameMouseListener.clone(), this.leaveTableButton);
        cc.eventManager.addListener(this.baseGameMouseListener.clone(), this.discardButton);
    },
    initTouchListener: function () {
        this.baseGameTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                applicationFacade._controlAndDisplay.closeAllTab();
                switch (target.getName()) {
                    case "Player" :
                        if (target._isEnabled)
                            this._joinRoom(target._seatPosition);
                        break;
                    case "gameInfoSprite":
                        target.handleGameInfo();
                        break;
                    case "settingButton":
                        this.settingBtnHandler();
                        break;
                    case "leaveTableButton":
                        this.leaveBtnHandler();
                        break;
                    case "DiscardButton":
                        this.discardBtnHandler();
                        break;
                    case "AutoModeBackButton":
                        this.autoModeBackBtnReq();
                        break;
                    case "insantPlayBtn":
                        this.instantPlayBtnHandler();
                        break;
                    case "GameDonotDealBtn":
                        this.GameDonotDealBtn.GameDonotDealBtnHandler();
                        break;
                    case "playForRealButton":
                        this.playForCashHandler();
                        break;
                    case "tournamentInfo":
                        this.handleInfoClicked();
                        break;
                    case "chatEditBox":
                        this.onClickTrackNode(true);
                        break;
                    case "bonusCloseBtn":
                        this.bonusToCashPopup.removeFromParent(true);
                        break;
                }
            }.bind(this)
        });
        this.baseGameTouchListener.retain();
        cc.eventManager.addListener(this.baseGameTouchListener.clone(), this.settingButton);
        cc.eventManager.addListener(this.baseGameTouchListener.clone(), this.leaveTableButton);
        cc.eventManager.addListener(this.baseGameTouchListener.clone(), this.discardButton);
    },
    processInActivePlayer: function (dataObj) {
        if ((dataObj["playerId"]) == this._myId)
            this.enableAutoPlay();
        else {
            var plrInactive = this.getPlayerNameById(dataObj["playerId"]);
            if (plrInactive) {
                plrInactive += " auto play mode is Active!!";
                this.showNotification(plrInactive);
            }
        }
        var profObj = this.getSeatObjByUser(dataObj["playerId"]);
        if (profObj && profObj._seatPosition != -1) {
            profObj.isautoPlay = true;
            profObj.setIcon("Auto Mode", "autoPlay");
        }
    },
    processActivePlayer: function (dataObj) {

        this._userCardPanel.cardBeingTouched = false;
        this._userCardPanel.isCardAnimating = false;

        if (dataObj["playerId"] == this._myId) {
            if (this._myId == this._currentTurnID)
                this.deckPanel.enable();
            this.autoPlayPopup && this.autoPlayPopup.removeAutoPlayLayer();
            this.autoPlayPopup = null;
            if (this._tabMc)
                this._tabMc.setIcon(1);
            this._userCardPanel.dropVisible();
            this._userCardPanel.handleReActive();
        }
        else {
            for (var i = 0; i < this._playerArray.length; i++) {
                if (this._playerArray[i].getPlayer_id() == dataObj["playerId"]) {
                    this.showNotification(this._playerArray[i].player_name + " is in active mode.");
                }
            }
            /*if (this._myId != -1)
             this.showNotification("");*/
        }
        var profObj = this.getSeatObjByUser(dataObj["playerId"]);
        if (profObj && profObj._seatPosition != -1) {
            profObj.isautoPlay = false;
            profObj.removeIcon();
            // profObj.updateProfileIcon();
        }
    },
    processPlayerDisconnected: function (dataObj) {
        var id = dataObj["playerDisconnect"];
        var profObj = this.getSeatObjByUser(id);
        if (id == this._myId && applicationFacade._controlAndDisplay._onlineStatus) {
            applicationFacade.sendConnectionOk();
            return;
        }
        if (profObj && profObj._seatPosition != -1) {
            profObj.isDisconnected = true;
            profObj.setIcon("Disconnected", "WrongShowIcon");
            var msg = profObj.player_name + " is currently disconnected !";
            this.showNotification(msg);
        }
    },
    // TODO work on all comments
    /*playerDisconnected: function (playerId, activeFlag) {
     // after disconnection user can not discard the cards
     //  this.pauseDiscardBtn(true);
     if (parseInt(playerId) == this._myId) {
     if (activeFlag == SFSConstants.INACTIVE) {
     // case of player inactive
     // visible reconnect button, invisible drop button, invisible dropcheckbox
     //   this.pauseReconnectButton(false);
     //   this.dropCheckbox.setVisible(false);
     // this.pauseDropButton(true);
     //   this.pauseShowButton(true);
     if (this._cardManager.cardStack.length > 13) {
     var index = this._cardManager.cardStack.indexOf(this.currentPickCard);
     //this._cardManager.cardStack.splice(index, 1);
     // discard card whick are picked in last
     this.discardCard(this.currentPickCard);
     // remove that picking card from cardstack also
     // this.pauseDiscardBtn(true);
     }

     } else {
     // case of disconection show toast
     /!* this._toastManager.makeToast("Notification", "Ooopss!! You are disconnected.",this);
     this._toastManager.toastHolder.setPosition(GAME_SIZE.width / 2, GAME_SIZE.height / 2 - 150);
     *!/
     // this.
     }
     }

     for (var i = 0; i < this._playerArray.length; i++) {
     if (this._playerArray[i].getPlayer_id() == playerId) {
     if (activeFlag == SFSConstants.DISCONNECT) {
     if (parseInt(playerId) != this._myId) {
     this.showNotification(this._playerArray[i].player_name + " is currently disconnected.");
     // show toast for opponent is in diconnect mode
     /!*this._toastManager.makeToast("alert", sfsClient.mSeatedPLayers[i].getPlayer_name() + "is currently disconnected.",this);
     this._toastManager.toastHolder.setPosition(GAME_SIZE.width/2 ,GAME_SIZE.height/2 - 150);
     *!/
     }
     this._playerArray[i].setDisconnected(true);
     this.updatePlayerInfo(KHEL_CONSTANTS.USER_ICON_UPDATE, this._playerArray[i].getSeat_id(), this._playerArray[i].getPlayer_id(),
     this._playerArray[i].getPlayer_name(), this._playerArray[i].getPlayer_score(), false, true, false, false, false);
     } else {
     if (parseInt(playerId) != this.getPlayerId()) {
     this.showNotification(sfsClient.mSeatedPLayers[i].player_name + " auto play mode is active");
     // show toast for opponent is in auto mode
     /!*this._toastManager.makeToast("Alert", sfsClient.mSeatedPLayers[i].getPlayer_name() + "auto play mode is active",this);
     this._toastManager.toastHolder.setPosition(GAME_SIZE.width/2 ,GAME_SIZE.height/2 - 150);
     *!/
     }
     this._playerArray[i].setAutoPlay(true);
     this.updatePlayerInfo(KHEL_CONSTANTS.USER_ICON_UPDATE, this._playerArray[i].getSeat_id(),
     this._playerArray[i].getPlayer_id(), this._playerArray[i].getPlayer_name(),
     this._playerArray[i].getPlayer_score(), false, false, true, false, false);
     }
     }
     }
     },*/
    discardCard: function (discardCardName) {
        /* if (!(sfsClient.roomUpdates.variables[SFSConstants._GAMETYPE].value.indexOf(SFSConstants.GT_BEST3) >= 0)) {
         this.pauseDropButton(false);
         } else {
         this.pauseDropButton(true);
         }*/
        var discardedCard;
        if (discardCardName != null) {
            discardedCard = discardCardName;
        } else {
            discardedCard = this.cardStand[0];
        }
        this.deckPanel.openDeck.addCardAfterDiscard(discardedCard.getName());

        // remove from group list and cardStand
        // var groupNo = discardedCard.getTag();

        // Flush cardStand
        this.cardStand = [];
        // Remove discarded card from stack
        this._userCardPanel.cardStack.splice(discardedCard._stackIndex, 1);

        /*  //removing from group list
         if (this.groupList[groupNo]) {
         var index = this.groupList[groupNo].indexOf(discardedCard);
         this.groupList[groupNo].splice(index, 1);

         // Removing group with length == 1 and placing card to last index of cardStack

         if (this.groupList[groupNo].length == 1) {

         // setting tag to last remaining card
         var lastCard = this.groupList[groupNo][0];
         lastCard.setTag(-1);

         // repositioning card in cardStack
         var cpos = this._cardManager.cardStack.indexOf(lastCard);
         this._cardManager.cardStack.splice(cpos, 1);
         this._cardManager.cardStack.splice(this._cardManager.cardStack.length, 0, lastCard);

         // removing group
         this.groupList.splice(groupNo, 1);
         }
         }

         //resetting tag of cards of group list
         for (var i = 0; i < this.groupList.length; i++) {
         for (var j = 0; j < this.groupList[i].length; j++) {
         this.groupList[i][j].setTag(i);
         }
         }*/

        this.isDiscardActionEnabled = false;
        this.discardBtn.hide(true);
        //this._cardManager.resetCardsPosition(this.groupList, this.sortList);
        //this.showCard.setVisible(false);

        var meldCardStr = this.getmeldCardStr();
        sfsClient.updateGroupCards(meldCardStr);
        this.discardReq(discardedCard.getName());
    },
    discardReq: function (mDiscardCardName) {
        var obj = {};
        obj[SFSConstants.FLD_ID] = this._myId;
        obj[SFSConstants.FLD_ACTION] = SFSConstants.ACTION_DISCARD;
        obj[SFSConstants.FLD_DC] = mDiscardCardName;
        this.sendReqToServer(SFSConstants.CMD_DROPREQ, obj);
    },
    updateGroupCards: function (grpCardStr) {
        //groupCards
        var obj = {};
        obj[SFSConstants.FLD_MELD_CARDS] = grpCardStr;
        obj[SFSConstants.FLD_GRP_RND] = this._currentRound;
        this.sendReqToServer(SFSConstants.CMD_GROUPCARDS, obj);

        //isConnect
        obj = {};
        obj[SFSConstants.FLD_VAL] = true;
        this.sendReqToServer(SFSConstants.CMD_ISCONNECT, obj);
    },
    seatedPlayerCount: function () {
        var count = 0;
        for (var i = 0; i < this._playerArray.length; i++) {
            if (this._playerArray[i]._sittingStatus != 0)
                count++;
        }
        return count;
    },
    handleMeldResp: function (dataObj) {
        this.pickWaitMC.setVisible(false);
        this.waitingMC && this.waitingMC.setString("");
        /*todo toolTip.visible = false;
         toolTip.txt.text = "";*/
        if (dataObj.valid) {
            //in case of right melded cards we have to give chance to rest players to meld their card
            if (this._currentTurnID == this._myId) {
                SoundManager.playSound(SoundManager.validshow, false);
                this._userCardPanel.clearCards();
                this._userCardPanel.hideAllItems(true);
                this.meldingPanel.removeJoker();
                this.meldingPanel.setMeldMsg(2, "");
            }
        }
        else {
            this._extraTimeToggle = false;

            this._userCardPanel.meldConfirmPopUp && this._userCardPanel.meldConfirmPopUp.removeFromParent(true);
            this.meldingPanel && this.meldingPanel.removeFromParent(true);
            SoundManager.playSound(SoundManager.wrongshow, false);
            this.handlePlayerFold("wrongShow", dataObj);
            // this.updatePlayerInfo(KHEL_CONSTANTS.USER_ICON_UPDATE, obj, false, false, false, false, false);  // TODO icon update for wrong show
            this.messagePopUp && this.messagePopUp.removeFromParent(true);

            if (this._currentTurnID == this._myId) {
                this._userCardPanel.clearCards();
                this._userCardPanel.hideAllItems(true);

                var player = this.getSeatObjByUser(this._currentTurnID);
                if (!player) {
                    cc.log(this._playerArray);
                }
                player.isWrongShow = true;
                var totalSeatedPlr = this.seatedPlayerCount();
                if (totalSeatedPlr > 2)
                    this.showWrongShow();
            }
            else {
                if ((this._myId != -1) && (!this._dropped)) {
                    this._userCardPanel.activatePanel();
                    this._userCardPanel.hideAllItems(false);
                    this._userCardPanel.dropVisible();
                    if ((this._serverRoom.groupId != "tournamentGameRoom") && (this._serverRoom.getVariable("cashOrPromo").value != "CASH"))        //(totCards==21)&&
                    {
                        if (this.showValidity) {
                            this.showValidity.hide(false);
                            if (this.totCards == 21)
                                this.showValidity.checkValidity();
                            else
                                this.showValidity.checkValidity();
                        }
                    }
                }
            }
            this.deckPanel.hideAllItems(false);
            this.deckPanel.disable();
        }
    },
    processExtraTimeUserList: function (dataObj) {
        //wrongShowMC.visible = false;
        if (this._currentRoomState == SFSConstants.RS_MELD) {
            if (!this._dropped && !this._isWatching && (this._myId != -1)) {
                this.messagePopUp && this.messagePopUp.removeFromParent(true);

                var userList = dataObj["extraTimeUserList"].split(",");
                this.meldingPanel.setVisible(true);
                var str;
                var timerStr;
                if (userList.length == 1)
                    str = "Please wait " + dataObj["extraTimeUserList"] + " is using extra timer.";
                else
                    str = "Please wait " + dataObj["extraTimeUserList"] + " are using extra timer.";
                if (dataObj["maxExtraTime"] > 0)
                    timerStr = dataObj["maxExtraTime"];
                else
                    timerStr = "0";
                this.meldingPanel.setMeldMsg(5, str, timerStr);
                this.scheduleWaitTime(SFSConstants.ETT_MELDEXTRATIMER, parseInt(dataObj["maxExtraTime"]));
                if (this._userCardPanel.isVisible()) {
                    var groupCards = this._userCardPanel.getGroupCardsStr();
                    this._userCardPanel.sendGroupCards(groupCards, true);
                    this._userCardPanel.hideAllItems(true);
                }
                var seatObj = this.getSeatObjByUser(this._myId);
                if (seatObj)
                    seatObj.hideTimer();
                this.meldingPanel.removeJoker();
            }
            else {
                this.initMessagePop();
                this.messagePopUp.setMsg("Melding in progress!!!\nPlease wait while players are melding... ");
            }
        }
        else
            this.messagePopUp && this.messagePopUp.removeFromParent(true);
    },
    pauseListener: function (bool) {
        bool ? cc.eventManager.pauseTarget(this) : cc.eventManager.resumeTarget(this);
    },
    initRoom: function () {
        var maxPlayer = this._serverRoom.variables.maxPlayers.value;
        // var noOfPlayers = this._serverRoom.variables.noOfPlayers.value;
        this.arrangeSeats(maxPlayer, false);

    },
    removeAllPlayers: function () {
        var k = this._playerArray.length;
        while (k--) {
            this.removeChild(this._playerArray[k]);
        }
        this._playerArray = [];
    },
    arrangeSeats: function (noOfPlayer, isRearrangeSeat) {
        this.noOfPlayer = noOfPlayer;
        var seatPos;
        if (isRearrangeSeat) {
            for (var i = 0; i < this._playerArray.length; i++) {
                // if (this._playerArray[i]._sittingStatus) {
                seatPos = this._playerArray[i]._seatPosition - 1;
                this._playerArray[i].setPosition(this._chairPosition[this.noOfPlayer][seatPos]);
                // }
            }
        } else {
            if (noOfPlayer % 2 == 0) {
                this.getChair(noOfPlayer);
                for (var i = 0; i < this._playerArray.length; i++) {
                    this._playerArray[i].setPosition(this._chairPosition[noOfPlayer][i]);
                    this.addChild(this._playerArray[i], GameConstants.playerZorder);
                    cc.eventManager.addListener(this.baseGameMouseListener.clone(), this._playerArray[i]);
                    cc.eventManager.addListener(this.baseGameTouchListener.clone(), this._playerArray[i]);
                }
            }
        }
    },
    getChair: function (noOfPlayer) {
        for (var i = 0; i < this._playerArray.length; i++) {
            cc.eventManager.removeListener(this._playerArray[i], true);
            this._playerArray[i].removeFromParent(true);
        }
        this._playerArray = [];
        for (var index = 0; index < noOfPlayer; index++) {
            this._playerArray.push(new Player(this, index));
        }

    },
    handleUserLeave: function () {
        if (this._requestToSever) {
            // Main.callExternalInterface();
            //todo weaver call
            this.sendReqToServer(SFSConstants.CMD_CLICK_LEAVE_TABLE, {});
            this.leaveRoomByMyself();
        }
        this.removeChild(this.leaveTablePopup, true);
        this.waitingMC.setString("Please Wait...");
        this.waitingMC.setVisible(true);
    },
    discardBtnHandler: function () {

        if (this._serverRoom.getVariable(SFSConstants._DISCARD)) {
            var discrdStr = this._serverRoom.getVariable(SFSConstants._DISCARD).value;
            var cardStrValid = false;
            if (discrdStr) {
                var playerArray = discrdStr.split(";");
                for (var a = 0; a < playerArray.length; a++) {
                    var cardStr = playerArray[a].split(":")[1];
                    if (cardStr != "") {
                        cardStrValid = true;
                        break;
                    }
                }
                if (cardStrValid) {
                    this.discardPanel && this.discardPanel.initDiscardPanel(discrdStr);
                    this.discardPanel && this.discardPanel.showPanel();
                    this.discardButton.isPressed(!this.discardButton._isPressed);
                }
            }
        }

    },
    settingBtnHandler: function () {
        this.discardButton.isPressed(false);
        this.discardPanel.isVisible() && this.discardPanel.setVisible(false);
        if (this.settingPanel.isVisible()) {
            this.settingPanel.setVisible(false);
        } else {
            this.settingPanel.setVisible(true);
        }
    },
    lastResBtnHandler: function () {
        if (this._currentRoomState == SFSConstants.RS_RESULT)
            return;
        this.settingPanel.setVisible(false);
        this.scoreBoardPanel && this.scoreBoardPanel.setVisible(false);

        if (this.result && this.result.isVisible())
            this.result.setVisible(false);
        else
            this.sendReqToServer(SFSConstants.CMD_LASTRESULT, {});
    },
    scoreBoardBtnHandler: function () {
        if (this._currentRoomState == SFSConstants.RS_JOIN)
            return;
        if (this._currentRoomState != SFSConstants.RS_RESULT)
            this.result && this.result.setVisible(false);

        this.settingPanel.setVisible(false);
        if (this.scoreBoardPanel && this.scoreBoardPanel.isVisible())
            this.scoreBoardPanel.setVisible(false);
        else {
            var obj = {};
            this.sendReqToServer(SFSConstants.CMD_SCORECARD, obj);
        }
    },
    historyBtnHandler: function () {
        this.settingPanel.setVisible(false);
        if ((this._currentRoomState != SFSConstants.RS_WATCH) || (!this._hasDistributionComp))
            return;

        if (!this.historyPanel) {
            var numOfCard = this._serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;
            this.historyPanel = new History(this, numOfCard);
            this.addChild(this.historyPanel, GameConstants.historyPanelZorder, "historyPanel");
            this.historyPanel.setPosition(cc.p(this.width / 2, this.height / 2 + 80 * scaleFactor));
            this.historyPanel.setVisible(false);
            this.historyPanel._historyCards = "";
        }
        if (this.historyPanel._historyCards == "") {
            this.sendReqToServer(SFSConstants.CMD_HISTORY_CARD, {});
        }
        else {
            this.historyPanel.setVisible(!this.historyPanel.visible);
        }
    },
    historyRespHandler: function (cardStr) {
        var wldCard = this._serverRoom.getVariable(SFSConstants._WILDCARD).value;
        this.historyPanel.initCards(cardStr, wldCard);
        this.historyPanel.setVisible(true);
    },
    showNotification: function () {
        if (cc.game._paused)
            return;

        if (this.notification) {
            this.notification.stopAllActions();
            this.notification.removeFromParent(true);
        }
    },
    showSmileyAndChatDisableToolTip: function () {

        var SmileyAndChatDisableToolTip = cc.Scale9Sprite.extend({
            timeOut: null,
            ctor: function () {
                this._super(spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png"), cc.rect(0, 0, 50 * scaleFactor, 50 * scaleFactor));
                this.setColor(cc.color(255, 238, 215));
                this.setCapInsets(cc.rect(16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor));
                this.setContentSize(cc.size(190 * scaleFactor, 50 * scaleFactor));
                this.setCascadeColorEnabled(false);
                this.setVisible(true);
                this.setPosition(cc.p(150 * scaleFactor, 100 * scaleFactor));
                // this.setPosition(cc.p(549, 500));

                var arrow = new cc.Sprite(spriteFrameCache.getSpriteFrame("arrow.png"));
                arrow.setColor(cc.color(255, 238, 215));
                arrow.setPosition(cc.p(-3 * scaleFactor, this.height / 2));
                arrow.setRotation(360);
                this.addChild(arrow);

                var msg = new cc.LabelTTF("This function is disabled \n because you are watching game.", "RobotoBold", 12 * 2 * scaleFactor);
                msg.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
                msg.setScale(0.5);
                msg.setPosition(cc.p(this.width / 2, this.height / 2 - yMargin));
                msg.setColor(cc.color(0, 0, 0));
                this.addChild(msg, 1);

            },
            onExit: function () {
                this._super();
                this.timeOut && window.clearTimeout(this.timeOut);
            }
        });
        this.smileyAndChatDisableToolTip = new SmileyAndChatDisableToolTip();
        this.addChild(this.smileyAndChatDisableToolTip, GameConstants.jokerToolTipZorder, "smileyAndChatDisableToolTip");
    },
    initMessagePop: function () {
        if (!this.messagePopUp) {
            this.messagePopUp = new MessagePopUp(this, true);
            this.addChild(this.messagePopUp, GameConstants.popupZorder, "messagePopUp");
        }
    },
    showJokerToolTip: function () {
        if (this.jokerToolTip) {
            this.jokerToolTip.removeFromParent(true);
            this.jokerToolTip = null;
        }
        var JokerToolTip = cc.Scale9Sprite.extend({
            timeOut: null,
            ctor: function () {
                this._super(spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png"), cc.rect(0, 0, 50 * scaleFactor, 50 * scaleFactor));
                this.setColor(cc.color(255, 238, 215));
                this.setCapInsets(cc.rect(16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor));
                this.setContentSize(cc.size(120 * scaleFactor, 40 * scaleFactor));
                this.setCascadeColorEnabled(false);
                this.setVisible(true);
                this.setPosition(cc.p(549 * scaleFactor, 500 * scaleFactor));

                var arrow = new cc.Sprite(spriteFrameCache.getSpriteFrame("arrow.png"));
                arrow.setColor(cc.color(255, 238, 215));
                arrow.setPosition(cc.p(this.width / 2 - 2 * scaleFactor, 0));
                arrow.setRotation(-90);
                this.addChild(arrow);

                var msg = new cc.LabelTTF("Picking of Joker is not allowed.", "RobotoBold", 12 * 2 * scaleFactor);
                msg._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
                msg.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
                msg.setScale(0.5);
                msg.setPosition(cc.p(this.width / 2, this.height / 2 - yMargin));
                msg.setColor(cc.color(0, 0, 0));
                this.addChild(msg, 1);

                this.tween();
            },
            tween: function () {
                var self = this;
                this.timeOut = setTimeout(function () {
                    self.removeFromParent(true);
                }, 1000);

                /* var fade = cc.fadeIn(0.5);
                 this.runAction(cc.sequence(fade, cc.callFunc(function () {
                 this.timeOut = setTimeout(function () {
                 this.runAction(cc.sequence(fade.reverse(), cc.callFunc(function () {
                 this.removeFromParent(true);
                 }, this)));
                 }.bind(this), 4.5);
                 }, this)));*/
            },
            onExit: function () {
                this._super();
                this.timeOut && window.clearTimeout(this.timeOut);
            }
        });
        this.jokerToolTip = new JokerToolTip();
        this.addChild(this.jokerToolTip, GameConstants.jokerToolTipZorder);
    },
    getPlayerNameById: function (id) {
        var obj = this.getSeatObjByUser(id);
        if (obj)
            return obj._playerName;
        else
            return null;
    },
    handleOpenDeckResp: function (dataObj) {
        this.pickWaitMC.setVisible(false);
        if (dataObj.pc == "OD")
            this.deckPanel.handlePickResp("OPEN_DECK", dataObj);
        this.deckPanel.glowEnabled(false);
        var self = this;
        setTimeout(function () {
            var str = self._serverRoom.getVariable(SFSConstants._DISCARD).value;
            if (str) {
                self.discardPanel && self.discardPanel.initDiscardPanel(str);
            }
        }, 1000);
    },
    handleClosedDeckResp: function (dataObj) {
        this.deckPanel.handlePickResp("CLOSED_DECK", dataObj);
        this.deckPanel.glowEnabled(false);
    },
    getSeatObjByUser: function (id) {
        if (id < 0)
            return;

        for (var i = 0; i < this._playerArray.length; i++) {
            if (this._playerArray[i]._playerId == id)
                return this._playerArray[i];
        }
    },
    getSeatObjByUserName: function (name) {
        for (var i = 0; i < this._playerArray.length; i++) {
            if (this._playerArray[i]._playerName == name) {
                return this._playerArray[i];
            }
        }
    },
    getSeatObjBySeatId: function (id) {
        for (var i = 0; i < this._playerArray.length; i++) {
            if (this._playerArray[i]._seatPosition == id)
                return this._playerArray[i];
        }
    },
    sendPickResp: function (pickObj) {
        this.pickWaitMC.setVisible(true);
        this.pickWaitMC.setString("Please Wait...");
        this._userCardPanel._discardClicked = true;
        this._userCardPanel.DropBtn.hide(true);
        this.sendReqToServer("pickResp", pickObj);
    },
    handleUserResp: function (dataObj) {
        if (dataObj.cmd == "drop") {
            if (this._currentTurnID == this._myId) {
                this.sendTurnResp(dataObj);
                this.deckPanel.disable();
            }
        }
        else if (dataObj.cmd == "show") {
            this.pickWaitMC.setVisible(true);
            this.pickWaitMC.setString("Please Wait...");
            this.sendTurnResp(dataObj);
        }
        else {
            this.pickWaitMC.setVisible(true);
            this.pickWaitMC.setString("Please Wait...");
            this.sendTurnResp(dataObj);
        }
    },
    sendTurnResp: function (dropObj) {
        if (dropObj)
            this.sendReqToServer("dropResp", dropObj);
    },
    setPrize: function (data) {
        this.gameInfoSprite && this.gameInfoSprite.updatePrize();
    },
    setNoRoomPopUp: function () {

        this._dummyObj = null;
        this._roomId = -1 * this._roomId;

        // fetching_popUp.visible = true;
        // fetching_popUp.setState(2);
        // fetching_popUp.leaveBtn.addEventListener(MouseEvent.CLICK, onDummyLeave);
    },
    smileyResp: function (data) {
        var player = this.getSeatObjByUser(data.id);
        var smiley = data["smile"];
        this.setSmileyIntervalTimer(player);
        player.setSmileyIcon(smiley);
    },
    processClosedDeckReshuffle: function () {
        this.showNotification("The Closed Deck has been reshuffled!");
    },
    setSmileyIntervalTimer: function (player) {
        var time = 10;
        this.ccTimeOutManager.unScheduleInterval("smileyTimer");
        this.ccTimeOutManager.scheduleInterval(xyz.bind(this), 1000, 0, "smileyTimer");
        function xyz() {
            if (time > 1) {
                time = time - 1;
            } else {
                this.ccTimeOutManager.unScheduleInterval("smileyTimer");
                player.removeSmileyIcon();
                this.smileyAndChatIcon.currentSmileyNo = null;

            }
        }
    },
    stopAllTimers: function () {
        this.ccTimeOutManager.stopAllTimers();
    },
    initClosedCards: function () {
        var ClosedCard = cc.Sprite.extend({
            seatId: null,
            playerName: null,
            ctor: function () {
                this._super(spriteFrameCache.getSpriteFrame("CloseCard.png"));
            },
            showAllCard: function () {
                var card = new cc.Sprite(spriteFrameCache.getSpriteFrame("CloseCard.png"));
                card.setAnchorPoint(0.5, 0);
                card.setPosition(cc.p(this.width / 2 - 2 * scaleFactor, 0));
                card.setRotation(-5);
                this.addChild(card, 1);
                this.setRotation(5);
                this.x += 2 * scaleFactor;
            }
        });

        var maxPlayer = this._serverRoom.variables.maxPlayers.value;
        if (maxPlayer % 2 == 0) {
            for (var i = 0; i < this._closedCards.length; i++) {
                this._closedCards[i].removeFromParent(true);
            }
            this._closedCards = [];
            for (var i = 0; i < this._playerArray.length; i++) {
                if (this._playerArray[i]._playerId != this._myId && this._playerArray[i]._sittingStatus == !0
                    && !this._playerArray[i].isWaiting && !this._playerArray[i].isWrongShow) {
                    var seatPosition = this._playerArray[i]._seatPosition;
                    var closedCard = new ClosedCard();
                    closedCard.seatId = seatPosition;
                    closedCard.playerName = this._playerArray[i]._playerName;
                    closedCard.setPosition(GameConstants.closedCardPos_13[maxPlayer][seatPosition - 1]);
                    this.addChild(closedCard, GameConstants.playerZorder - 1);
                    this._closedCards.push(closedCard);
                }
            }
        }
    },
    arrangeClosedCard: function () {
        for (var i = 0; i < this._closedCards.length; i++) {
            this._closedCards[i].showAllCard(true);
        }
    },
    removeClosedCard: function () {
        for (var i = 0; i < this._closedCards.length; i++) {
            this._closedCards[i].removeFromParent(true);
        }
    },
    removeClosedCardForSpecificPlayer: function (obj) {
        for (var i = 0; i < this._closedCards.length; i++) {
            if (obj && this._closedCards[i].seatId == obj._seatPosition) {
                this._closedCards[i].removeFromParent(true);
                return;
            }
        }
    },
    getClosedCardById: function (seatId) {
        for (var i = 0; i < this._closedCards.length; i++) {
            if (this._closedCards[i].seatId == (seatId)) {
                return this._closedCards[i];
            }
        }
    },
    showPlayerNameToolTip: function (bool, playerName, pos) {
        if (bool) {
            this.playerToolTip.hide(false);
            this.playerToolTip.setPlayerName(playerName);
            this.playerToolTip.setPosition(cc.p(pos.x, pos.y - 80 * scaleFactor));
        } else {
            this.playerToolTip.hide(true);
        }
    },
    initPopup: function () {
        // this.result.splitPopup.setVisible(false);
        // this.result && this.result.waitTxt.setVisible(false);

        this._isWatching = true;
        this.waitingMC.setVisible(false);
        this.watchingTxt.setVisible(false);

        this.smileyAndChatIcon.resetChat();
        this._userCardPanel.killDistribution();

        this.instantNotMC && this.instantNotMC.removeFromParent(true);
        this.instantNotMC = null;
        this.insantPlayBtn && this.insantPlayBtn.removeFromParent(true);
        this.insantPlayBtn = null;

        this.invalidDeclr && this.invalidDeclr.removeFromParent(true);

        this.deckPanel && this.deckPanel.hideAllItems(true);
        this._userCardPanel && this._userCardPanel.hideAllItems(true);

        this.confirmationPopup && this.confirmationPopup.removeFromParent(true);
        this.confirmationPopup = null;
        this.meldingPanel && this.meldingPanel.removeFromParent(true);
        this.scoreBoardPanel && this.scoreBoardPanel.removeFromParent(true);
        this.leaveTablePopup && this.leaveTablePopup.removeFromParent(true);
        this.noBalPopup && this.noBalPopup.removeFromParent(true);
        this.cutDeckPanel && this.cutDeckPanel.removeFromParent(true);
        this.meldConfirmPopUp && this.meldConfirmPopUp.removeFromParent(true);
        this.showValidity && this.showValidity.hide(true);
        this.autoPlayPopup && this.autoPlayPopup.removeFromParent(true);
    },
    handleGameFinishEvent: function (_meWon, _winning, params) {

        if (_meWon) {
            if (applicationFacade._mixpanelToggle) {
                // GameMixPanel.getMixPanel().trackGameWonCount(_serverRoom.getVariable(Command.CASH_OR_PROMO).getValue());
                // GameMixPanel.getMixPanel().trackGameWonAmt(Number(_winning), _serverRoom.getVariable(Command.CASH_OR_PROMO).getValue());
            }
        }
        else {
            //_wager = String(obj.amt);
            if ((applicationFacade._mixpanelToggle) && (this._myId != -1)) {
                // GameMixPane.l.getMixPanel().trackGameLostCount(_serverRoom.getVariable(Command.CASH_OR_PROMO).getValue());
            }
        }

        if (this._myId != -1) {
            var rejoinAmt = "";
            var _count = "";
            var plyArr;
            var rejoinArr = params["rejoinStr"].split("|");
            for (i = 0; i < rejoinArr.length; i++) {
                plyArr = rejoinArr[i].split(":");
                if (plyArr[0] == this._myId) {
                    rejoinAmt = String(plyArr[2]);
                    _count = String(plyArr[1]);
                    if (_count > 0) {
                        this._joinType = "Reconfirm";
                    }
                }
            }
            this.sendGameFinishEvent(rejoinAmt, _count, _winning, String(params["gameDuration"]));
        }
    },
    onExit: function () {
        this._super();
        this.stopAllTimers();
    }
});
BaseGame.prototype._chairPosition = {
    2: [cc.p(500 * scaleFactor, 560 * scaleFactor),
        cc.p(500 * scaleFactor, 110 * scaleFactor)],

    4: [cc.p(925 * scaleFactor, 415 * scaleFactor),
        cc.p(500 * scaleFactor, 560 * scaleFactor),
        cc.p(75 * scaleFactor, 415 * scaleFactor),
        cc.p(500 * scaleFactor, 110 * scaleFactor)],

    6: [cc.p(925 * scaleFactor, 415 * scaleFactor),
        cc.p(735 * scaleFactor, 550 * scaleFactor),
        cc.p(500 * scaleFactor, 560 * scaleFactor),
        cc.p(265 * scaleFactor, 550 * scaleFactor),
        cc.p(75 * scaleFactor, 415 * scaleFactor),
        cc.p(500 * scaleFactor, 110 * scaleFactor)]
};
BaseGame.prototype._tossCardPosition = {
    2: [cc.p(500 * scaleFactor, 430 * scaleFactor),
        cc.p(500 * scaleFactor, 200 * scaleFactor)],

    4: [cc.p(925 * scaleFactor, 300 * scaleFactor),
        cc.p(500 * scaleFactor, 430 * scaleFactor),
        cc.p(75 * scaleFactor, 300 * scaleFactor),
        cc.p(500 * scaleFactor, 200 * scaleFactor)],

    6: [cc.p(925 * scaleFactor, 300 * scaleFactor),
        cc.p(735 * scaleFactor, 430 * scaleFactor),
        cc.p(500 * scaleFactor, 430 * scaleFactor),
        cc.p(265 * scaleFactor, 430 * scaleFactor),
        cc.p(75 * scaleFactor, 300 * scaleFactor),
        cc.p(500 * scaleFactor, 200 * scaleFactor)]
};
var DiscardButton = cc.Sprite.extend({
    _hovering: null,
    _isEnabled: null,
    _isPressed: null,
    ctor: function () {
        this._super(spriteFrameCache.getSpriteFrame("DiscardBtn.png"));
        this.texture.setAliasTexParameters();
        this.setName("DiscardButton");
        this._hovering = false;
        this._isPressed = false;
    },
    pauseListener: function (bool) {
        this._isEnabled = !bool;
    },
    hide: function (bool) {
        this.setVisible(!bool);
        this._isEnabled = !bool;
    },
    hover: function (bool) {
    },
    isPressed: function (bool) {
        this._isPressed = bool;
        this._isEnabled = bool;
        bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("DiscardBtnOver.png")) : this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("DiscardBtn.png"));
    }
});