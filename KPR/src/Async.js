/**
 * Created by stpl on 4/10/2017.
 */
cc.async.map(["a", "B"], function (item, index, cb) {
    console.log(this.name);//TempObj
    cb(null, index + ":" + item);//Instead of async function here
}, function (err, results) {
    if (err) return console.error(err);
    console.log(results);//[ '0:a', '1:B' ]
});

//http://www.cocos2d-x.org/wiki/Asynchronised_Process_by_ccasync

cc.async.parallel([
    function(next){
        setTimeout(function(){
            console.log(0);
            next();
        }, 2000);
    }, function(next){
        console.log(1);
        next();
    }
], function(){
    console.log("end");
});


cc.async.parallel([
    function(next){
        first();
        next();
    }, function(next){
        second()
        next();
    }
], function(){
    cc.log("end");
});