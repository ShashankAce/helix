/**
 * Created by stpl on 1/13/2017.
 */
var RoundStructure = cc.Layer.extend({

    _tournamentDetail: null,

    ctor: function (context, room) {

        this._super();

        this._tournamentDetail = context;
        this.trnmntRoom = room;

        this.setContentSize(cc.size(720 * scaleFactor, 390 * scaleFactor));
        this.setPosition(cc.p(220 * scaleFactor, 3 * scaleFactor));

        var headerStrip = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("HeaderStrip.png"));
        headerStrip.setCapInsets(cc.rect(19 * scaleFactor, 13 * scaleFactor, 30 * scaleFactor, 28 * scaleFactor));
        headerStrip.setContentSize(cc.size(720 * scaleFactor, 41 * scaleFactor));
        headerStrip.setAnchorPoint(0, 0.5);
        headerStrip.setPosition(cc.p(0, 372 * scaleFactor));
        this.addChild(headerStrip, 1);

        var d1;
        d1 = new cc.Sprite(spriteFrameCache.getSpriteFrame("HeaderDivider.png"));
        d1.setPosition(cc.p(142 * scaleFactor, headerStrip.height / 2));
        d1.setScaleX(0.8);
        headerStrip.addChild(d1);

        d1 = new cc.Sprite(spriteFrameCache.getSpriteFrame("HeaderDivider.png"));
        d1.setPosition(cc.p(352 * scaleFactor, headerStrip.height / 2));
        d1.setScaleX(0.8);
        headerStrip.addChild(d1);

        d1 = new cc.Sprite(spriteFrameCache.getSpriteFrame("HeaderDivider.png"));
        d1.setPosition(cc.p(530 * scaleFactor, headerStrip.height / 2));
        d1.setScaleX(0.8);
        headerStrip.addChild(d1);

        var label;
        label = new cc.LabelTTF("Round", "RobotoBold", 20 * scaleFactor);
        label.setScale(0.7);
        label.setPosition(cc.p(70 * scaleFactor, headerStrip.height / 2 - yMargin));
        headerStrip.addChild(label);

        label = new cc.LabelTTF("Total no. of players on tables", "RobotoBold", 20 * scaleFactor);
        label.setScale(0.7);
        label.setPosition(cc.p(245 * scaleFactor, headerStrip.height / 2 - yMargin));
        headerStrip.addChild(label);

        label = new cc.LabelTTF("Players qualifying/table", "RobotoBold", 20 * scaleFactor);
        label.setScale(0.7);
        label.setPosition(cc.p(440 * scaleFactor, headerStrip.height / 2 - yMargin));
        headerStrip.addChild(label);

        label = new cc.LabelTTF("BreakTime", "RobotoBold", 20 * scaleFactor);
        label.setScale(0.7);
        label.setPosition(cc.p(625 * scaleFactor, headerStrip.height / 2 - yMargin));
        headerStrip.addChild(label);

        this.structureListView = new ccui.ListView();
        this.structureListView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        this.structureListView.setGravity(ccui.ListView.GRAVITY_CENTER_HORIZONTAL);
        this.structureListView.setScrollBarAutoHideEnabled(true);
        this.structureListView.setTouchEnabled(true);
        this.structureListView.setItemsMargin(10 * scaleFactor);
        /*this.structureListView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
         this.structureListView.setBackGroundColor(cc.color(255, 255, 255));*/
        this.structureListView.setContentSize(cc.size(this.width - 2, 340 * scaleFactor));
        this.structureListView.setPosition(cc.p(1 * scaleFactor, 1 * scaleFactor));
        this.structureListView.addEventListener(this.listViewListener.bind(this));

        this.addChild(this.structureListView);

        this.initRoom(room);
    },
    initRoom: function (room) {

        this.trnmntRoom = room;

        var structStr = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.STRUCTURE).value;
        var curRound = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value;
        if (structStr && (structStr == "" || structStr == " "))
            return;

        var tempStrArr = structStr.split(";");
        var roundStrArr = tempStrArr.reverse();
        var totRound = roundStrArr.length;
        var strip;
        var roundNo;
        var roundInfoStr;
        var roundInfoArr;
        var rndDisplayArr;

        this._tournamentDetail.TOT_ROUND = totRound;

        this.structureListView.removeAllChildrenWithCleanup(true);

        for (var i = 0; i < totRound; i++) {
            rndDisplayArr = roundStrArr[i].split("#");
            roundInfoArr = rndDisplayArr[1].split(",");

            strip = new RoundStructureBar(this, rndDisplayArr, roundInfoArr);
            strip.name = rndDisplayArr[0];

            if (curRound == strip.name)
                strip.changeState(2);
            else
                strip.changeState(1);


            // strip.noTF.text = roundInfoArr[0];

            this.structureListView.pushBackCustomItem(strip);

        }


    },
    handleRoomVarDetailUpdate: function () {

    },
    listViewListener: function (sender, type) {
        switch (type) {
            case ccui.ListView.ON_SELECTED_ITEM_END :
                var listViewEx = sender;
                var item = listViewEx.getItem(listViewEx.getCurSelectedIndex());
                this.updateListViewItem(item);
                break;
            default:
                break;
        }
    },
    updateListViewItem: function (item) {

    },
    initRoomListScrollListener: function () {

        if ('mouse' in cc.sys.capabilities) {
            this.structureListViewMouseListener = cc.EventListener.create({
                event: cc.EventListener.MOUSE,
                swallowTouches: true,
                onMouseScroll: function (event) {
                    var target = event.getCurrentTarget();
                    var locationInNode = target.convertToNodeSpace(event.getLocation());
                    var s = target.getContentSize();
                    var rect = cc.rect(0, 0, s.width, s.height);

                    if (cc.rectContainsPoint(rect, locationInNode)) {

                        var delta = cc.sys.isNative ? event.getScrollY() * 6 : -event.getScrollY();

                        var minY = this.structureListView._contentSize.height - this.structureListView._innerContainer.getContentSize().height;
                        var h = -minY;
                        var locContainer = this.structureListView._innerContainer.getBottomBoundary();
                        var percentY = ((h + locContainer) / h) * 100;
                        var scrollSpeed = 15; // higher value is inversely proportional to speed

                        this._scrolledPercent = percentY;

                        if ((this._scrolledPercent + delta / scrollSpeed) < 0) {
                            this._scrolledPercent = 0;
                        } else if ((this._scrolledPercent + delta / scrollSpeed) > 100) {
                            this._scrolledPercent = 100;
                        } else {
                            this._scrolledPercent += delta / scrollSpeed;
                        }
                        if (this._scrolledPercent <= 100 && this._scrolledPercent >= 0) {
                            this.structureListView.scrollToPercentVertical(this._scrolledPercent, 0.2, true);
                        }
                        return true;
                    }
                    return false;
                }.bind(this)
            });
            this.structureListViewMouseListener.retain();
            cc.eventManager.addListener(this.structureListViewMouseListener, this.structureListView);
        }
    }
});

var RoundStructureBar = ccui.Layout.extend({

    ctor: function (context, rndDisplayArr, roundInfoArr) {
        this._super();
        this.setContentSize(cc.size(context.width - 4 * scaleFactor, 50 * scaleFactor));
        this._name = rndDisplayArr[0];

        var rndStr = "";
        if (roundInfoArr[1]) {
            if (roundInfoArr[1] == "F")
                rndStr = "(Final)";
            else if (roundInfoArr[1] == "S")
                rndStr = "(Semi Final)";
            else if (roundInfoArr[1] == "Q")
                rndStr = "(Quarter Final)";
            else
                rndStr = "";
        }

        this.layout0 = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("whiteBox.png"));
        this.layout0.setCapInsets(cc.rect(16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor));
        this.layout0.setContentSize(cc.size(250 * scaleFactor, 50 * scaleFactor));
        this.layout0.setAnchorPoint(1, 0.5);
        this.layout0.setPosition(cc.p(this.width, this.height / 2));
        this.layout0.setColor(cc.color(245, 245, 245));
        this.addChild(this.layout0, 1);

        this.layout1 = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("RoundStrip.png"));
        this.layout1.setCapInsets(cc.rect(10 * scaleFactor, 16 * scaleFactor, 20 * scaleFactor, 25 * scaleFactor));
        this.layout1.setContentSize(cc.size(150 * scaleFactor, 50 * scaleFactor));
        this.layout1.setAnchorPoint(0, 0.5);
        this.layout1.setPosition(cc.p(0, this.height / 2));
        this.layout1.setColor(cc.color(211, 211, 211));
        this.addChild(this.layout1, 3);

        this.layout2 = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("RoundStrip.png"));
        this.layout2.setCapInsets(cc.rect(10 * scaleFactor, 16 * scaleFactor, 20 * scaleFactor, 25 * scaleFactor));
        this.layout2.setContentSize(cc.size(220 * scaleFactor, 50 * scaleFactor));
        this.layout2.setAnchorPoint(0, 0.5);
        this.layout2.setPosition(cc.p(135 * scaleFactor, this.height / 2));
        this.layout2.setColor(cc.color(225, 225, 225));
        this.addChild(this.layout2, 2);

        this.layout3 = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("RoundStrip.png"));
        this.layout3.setCapInsets(cc.rect(10 * scaleFactor, 16 * scaleFactor, 20 * scaleFactor, 25 * scaleFactor));
        this.layout3.setContentSize(cc.size(190 * scaleFactor, 50 * scaleFactor));
        this.layout3.setAnchorPoint(0, 0.5);
        this.layout3.setPosition(cc.p(340 * scaleFactor, this.height / 2));
        this.layout3.setColor(cc.color(238, 238, 238));
        this.addChild(this.layout3, 1);

        var circle = new cc.Sprite(spriteFrameCache.getSpriteFrame("whiteRound.png"));
        circle.setScale(0.9);
        circle.setColor(cc.color(255, 255, 255));
        circle.setPosition(cc.p(23 * scaleFactor, this.height / 2));
        this.addChild(circle, 5);

        this.levelArray = []; // plz do not abuse for this array
        var label;
        label = new cc.LabelTTF(roundInfoArr[0], "RobotoBold", 18 * scaleFactor);
        label.setColor(cc.color(0, 0, 0));
        label.setScale(0.7);
        label.setPosition(cc.p(23 * scaleFactor, this.height / 2 - yMargin));
        this.addChild(label, 5);

        var lavelStr = "";
        if (rndStr == "")
            lavelStr = "Round " + roundInfoArr[0];
        else
            lavelStr = "Round " + roundInfoArr[0] + "\n" + rndStr;

        label = new cc.LabelTTF(lavelStr, "RobotoRegular", 18 * scaleFactor);
        label.setScale(0.7);
        label.setColor(cc.color(0, 0, 0));
        label.setAnchorPoint(0, 0.5);
        label.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        label.setPosition(cc.p(65 * scaleFactor, this.height / 2 - yMargin));
        this.addChild(label, 5);
        this.levelArray.push(label);

        label = new cc.LabelTTF(roundInfoArr[2] + " Players on " + roundInfoArr[3] + " Table(s)", "RobotoRegular", 18 * scaleFactor);
        label.setScale(0.7);
        label.setColor(cc.color(0, 0, 0));
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(165 * scaleFactor, this.height / 2 - yMargin));
        this.addChild(label, 5);
        this.levelArray.push(label);

        var lavelStr2 = "";
        if (String(roundInfoArr[4]) != "N/A")
            lavelStr2 = String(roundInfoArr[4]) + " Players on the table";
        else
            lavelStr2 = String(roundInfoArr[4]);

        label = new cc.LabelTTF(lavelStr2, "RobotoRegular", 18 * scaleFactor);
        label.setScale(0.7);
        label.setColor(cc.color(0, 0, 0));
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(375 * scaleFactor, this.height / 2 - yMargin));
        this.addChild(label, 5);
        this.levelArray.push(label);

        var lavelStr3 = "";
        if (roundInfoArr[5])
            lavelStr3 = String(roundInfoArr[5]);
        else
            lavelStr3 = "No break time after round";

        label = new cc.LabelTTF(lavelStr3, "RobotoRegular", 18 * scaleFactor);
        label.setScale(0.7);
        label.setColor(cc.color(0, 0, 0));
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(555 * scaleFactor, this.height / 2 - yMargin));
        this.addChild(label, 5);
        this.levelArray.push(label);

    },
    changeState: function (state) {
        if (state == 1) {
            this.layout0.setColor(cc.color(245, 245, 245));
            this.layout1.setColor(cc.color(211, 211, 211));
            this.layout2.setColor(cc.color(225, 225, 225));
            this.layout3.setColor(cc.color(238, 238, 238));
            for (var i = 0; i < this.levelArray.length; i++) {
                this.levelArray[i].setColor(cc.color(0, 0, 0));
            }
        }
        else {
            this.layout0.setColor(cc.color(168 + 40, 182 + 40, 59 + 40));
            this.layout1.setColor(cc.color(126 + 40, 143 + 40, 15 + 40));
            this.layout2.setColor(cc.color(136 + 40, 152 + 40, 20 + 40));
            this.layout3.setColor(cc.color(156 + 40, 171 + 40, 52 + 40));
            for (var i = 0; i < this.levelArray.length; i++) {
                this.levelArray[i].setColor(cc.color(255, 255, 255));
            }
        }
    }
});