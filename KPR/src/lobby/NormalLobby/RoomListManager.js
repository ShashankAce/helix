/**
 * Created by stpl on 9/16/2016.
 */
"use strict";
var RoomListManager = cc.Class.extend({

    _name: "RoomListManager",
    _isEnabled: null,
    _scrolledPercent: 0,
    _filterParser: null,
    _selectedItem: null,
    selectedCommonId: null,

    ctor: function (filterParser) {

        this._filterParser = filterParser;
        this._isEnabled = true;

        this.listView = new ccui.ListView();
        this.listView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        this.listView.setGravity(ccui.ListView.GRAVITY_CENTER_HORIZONTAL);
        this.listView.setScrollBarAutoHideEnabled(false);
        this.listView.setTouchEnabled(true);
        this.listView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.listView.setBackGroundColor(cc.color(255, 255, 255));
        this.listView.setContentSize(cc.size(760 * scaleFactor, 300 * scaleFactor));
        this.listView.setPosition(cc.p(0, 0));

        this.listView.addEventListener(this.roomListSelectedItemEvent.bind(this));
        this.initRoomListScrollListener();

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        return true;
    },
    reqAlreadyJoinedRoom: function () {
        //request for already joined rooms

    },
    roomListSelectedItemEvent: function (sender, type) {
        switch (type) {
            //case ccui.ListView.EVENT_SELECTED_ITEM:
            case ccui.ListView.ON_SELECTED_ITEM_END :
                this.listView._isClicked = true;
                var listViewEx = sender;
                var item = listViewEx.getItem(listViewEx.getCurSelectedIndex());
                this.updateListViewItem(item);
                applicationFacade._controlAndDisplay.closeAllTab();
                applicationFacade._lobby.closeAllTab();
                break;
            default:
                break;
        }
    },
    updateListViewItem: function (item) {
        if (item) {
            if (this._selectedItem && this._selectedItem != item) {
                this._selectedItem.setSelected(!1);
            }
            if (this._selectedItem != item) {
                this._selectedItem = item;
                item.setSelected(!0);
                this._filterParser._lobby.showMiniRoom(this._selectedItem);
            }
            /*else{
             var newItem = this.listView.getItem(0);
             this._selectedItem = newItem;
             newItem.setSelected(!0);
             this._filterParser._lobby.showMiniRoom(this._selectedItem);
             }*/
        }
    },
    updateRoomPrize: function (grpId) {
        /*var rmBar;
         if(!this.roomListArray)
         return;

         for (var j = 0; j < _roomListArr.length;j++ )
         {
         rmBar = _roomListArr[j];
         //trace("rmBar", rmBar.room.getId(), room.getId())
         if (int(rmBar.room.getVariable(Command.COMMON_ID).getValue()) == grpId)
         {
         rmBar.updateRoomBarPrize();
         break;
         }
         }
         defaultSort();*/
    },
    updateNoOfPl: function () {

        /*var rmBar;
         for (var j = 0; j < _roomListArr.length;j++ )
         {
         rmBar = _roomListArr[j];
         //trace("rmBar", rmBar.room.getId(), room.getId())
         if(rmBar.room.getVariable(Command.COMMON_ID)){
         //trace(rmBar.room.getVariable(Command.COMMON_ID).getValue());
         if (int(rmBar.room.getVariable(Command.COMMON_ID).getValue()) == grpId)
         {
         rmBar.updateRoomBar();
         break;
         }
         }
         }
         if (rmBar && _selectedRoomBar && (rmBar == _selectedRoomBar))
         {
         _filterParser._lobby.showMiniRoom(_selectedRoomBar);
         }
         defaultSort();*/
    },
    updateListData: function (updatedListArray) {
        for (var i = 0; i < this.listView._items.length; i++) {
            var item = this.listView.getItem(i);
            if (updatedListArray[i]) {

                item.roomId = updatedListArray[i].roomId;
                item.tableId = updatedListArray[i].mTableId;
                item.tableName = updatedListArray[i].mTableName;
                item.gameType = updatedListArray[i].mGameType;
                item.maxPlayers = updatedListArray[i].mMaxPlayers;
                item.roomState = updatedListArray[i].mRoomState;
                item.entryFee = updatedListArray[i].mEntryFee;
                item.turnType = updatedListArray[i].mTurnType;
                item.totalPlayers = updatedListArray[i].mTotalPlayers;
                item.seatedPLayers = updatedListArray[i].mSeatedPLayers;
                item.cashOrPromo = updatedListArray[i].mCashOrPromo;
                item.betAmount = updatedListArray[i].mBetAmount;
                item.pointValue = updatedListArray[i].mPointValue;
                item.commonId = updatedListArray[i].mCommonId;
                item.groupTotalPlayers = updatedListArray[i].mGroupTotalPlayers;
                item.totalPrize = updatedListArray[i].mTotalPrize;
                item.seatedPls = updatedListArray[i].seatedPls;

                item.setSelected(false);

                if (this.selectedCommonId == item.commonId) {
                    item.setSelected(true);
                    this._selectedItem = item;
                    this._filterParser._lobby.showMiniRoom(this._selectedItem);
                }

                item.getChildByName("gameType").setString(item.gameType.split("-")[1]);
                if (item.gameType.indexOf(SFSConstants.TAG_POOL) >= 0) {
                    item.getChildByName("entryFee").setString(item.entryFee);
                    item.getChildByName("prizeValue").setString(item.totalPrize);
                } else {
                    item.getChildByName("pointValue").setString(item.pointValue);
                    item.getChildByName("betAmount").setString(item.betAmount);
                }

                item.getChildByName("turnType").setString(item.turnType);
                item.getChildByName("players").setString(item.maxPlayers);

                var totalPlayers = (item.groupTotalPlayers == "0") ? "-" : item.groupTotalPlayers + " Players";
                item.getChildByName("status").setString(item.totalPlayers + " Waiting");
                item.getChildByName("totalPlayers").setString(totalPlayers);

                var watchSprite = item.getChildByName("Watch");
                if (parseInt(totalPlayers) > 0)
                    watchSprite.enableWatch();
                else
                    watchSprite.disableWatch();

            }
        }
        if (!this.listView._isClicked) {
            var item = this.listView.getItem(0);
            this.updateListViewItem(item);
        }
    },
    
    insertRoomList: function (widthArray, displayRoomListData, tabType) {
        if (!this._filterParser._isEnabled)
            return;

        this.listView.removeAllChildrenWithCleanup(true);
        // displayRoomListData = (displayRoomListData === undefined) ? this._filterParser.cash13PoolArr : displayRoomListData;
        for (var index = 0; index < displayRoomListData.length; index++) {
            var roomBar = new RoomListStrip(widthArray, displayRoomListData[index], index, this);
            this.listView.pushBackCustomItem(roomBar);
        }

        var item = this.listView.getItem(0);
        this.listView._isClicked = false;
        if (tabType == "FINDGAME") {
            this.listView.setContentSize(cc.size(760 * scaleFactor, 260 * scaleFactor));
        } else {
            this.listView.setContentSize(cc.size(760 * scaleFactor, 300 * scaleFactor));
        }
        this.updateListViewItem(item);
    },
    initRoomListScrollListener: function () {

        if ('mouse' in cc.sys.capabilities) {
            this.listViewMouseListener = cc.EventListener.create({
                event: cc.EventListener.MOUSE,
                swallowTouches: true,
                onMouseScroll: function (event) {
                    if (this._isEnabled) {
                        var target = event.getCurrentTarget();
                        var locationInNode = target.convertToNodeSpace(event.getLocation());
                        var s = target.getContentSize();
                        var rect = cc.rect(0, 0, s.width, s.height);

                        if (cc.rectContainsPoint(rect, locationInNode)) {

                            var delta = cc.sys.isNative ? event.getScrollY() * 6 : -event.getScrollY();

                            var minY = this.listView._contentSize.height - this.listView._innerContainer.getContentSize().height;
                            var h = -minY;
                            var locContainer = this.listView._innerContainer.getBottomBoundary();
                            var percentY = ((h + locContainer) / h) * 100;
                            var scrollSpeed = 15; // higher value is inversely proportional to speed

                            this._scrolledPercent = percentY;

                            if ((this._scrolledPercent + delta / scrollSpeed) < 0) {
                                this._scrolledPercent = 0;
                            } else if ((this._scrolledPercent + delta / scrollSpeed) > 100) {
                                this._scrolledPercent = 100;
                            } else {
                                this._scrolledPercent += delta / scrollSpeed;
                            }
                            if (this._scrolledPercent <= 100 && this._scrolledPercent >= 0) {
                                this.listView.scrollToPercentVertical(this._scrolledPercent, 0.2, true);
                            }
                            return true;
                        }
                        return false;
                    }
                }.bind(this)
            });
            this.listViewMouseListener.retain();
            cc.eventManager.addListener(this.listViewMouseListener, this.listView);
        }
    },
    filterSubGameTypeByCheckBoxSelected: function (checkboxType) {
        var listArray = this.getSelectedList();

        var minEntryFee = this._filterParser.minValue.getString();
        var maxEntryFee = this._filterParser.maxValue.getString();
        listArray = this._filterParser.getSortedByEntryFee(listArray, minEntryFee, maxEntryFee);

        var filterArray = this.getFinalFilteredListByCheckbox(listArray);
        if (this._filterParser._lobby.selectedGameType._name == "FINDGAME") {
            this.insertRoomList(this._filterParser.widthArray, filterArray, "FINDGAME");
        } else {
            this.insertRoomList(this._filterParser.widthArray, filterArray, "CASH");
        }
    },
    getSelectedListByTab: function (selectedGameTypeName, selectedSubGameTypeName) {
        var filterArray = [];

        if (selectedGameTypeName == "PROMO") {
            switch (selectedSubGameTypeName) {
                case "POOL_13":
                    filterArray = this._filterParser.promo13PoolArr;
                    break;
                case "POINT_13":
                    filterArray = this._filterParser.promo13PointArr;
                    break;
                case "DEALS_13":
                    filterArray = this._filterParser.promo13DealsArr;
                    break;
                case "POINT_21":
                    filterArray = this._filterParser.promo21PointArr;
                    break;
            }
        } else if (selectedGameTypeName == "CASH") {
            switch (selectedSubGameTypeName) {
                case "POOL_13":
                    filterArray = this._filterParser.cash13PoolArr;
                    break;
                case "POINT_13":
                    filterArray = this._filterParser.cash13PointArr;
                    break;
                case "DEALS_13":
                    filterArray = this._filterParser.cash13DealsArr;
                    break;
                case "POINT_21":
                    filterArray = this._filterParser.cash21PointArr;
                    break;
            }
        }
        return filterArray;
    },

    getSelectedList: function () {
        var filterArray = [];
        var selectedGameTypeName;
        var selectedSubGameTypeName;
        if (this._filterParser._lobby.selectedGameType._name == "PROMO") {
            selectedGameTypeName = this._filterParser._lobby.selectedGameType._name;
            selectedSubGameTypeName = this._filterParser.selectedSubGameType._name;
            filterArray = this.getSelectedListByTab(selectedGameTypeName, selectedSubGameTypeName);
        } else if (this._filterParser._lobby.selectedGameType._name == "CASH") {
            selectedGameTypeName = this._filterParser._lobby.selectedGameType._name;
            selectedSubGameTypeName = this._filterParser.selectedSubGameType._name;
            filterArray = this.getSelectedListByTab(selectedGameTypeName, selectedSubGameTypeName);
        } else if (this._filterParser._lobby.selectedGameType._name == "TOURNAMENTS") {

        } else if (this._filterParser._lobby.selectedGameType._name == "FINDGAME") {
            selectedGameTypeName = this._filterParser.selectedFindGameType._name;
            selectedSubGameTypeName = this._filterParser.selectedSubGameType._name;
            filterArray = this.getSelectedListByTab(selectedGameTypeName, selectedSubGameTypeName);
        }
        return filterArray;
    },
    getFinalFilteredListByCheckbox: function (listArray) {
        var filterArray = [];
        for (var key in listArray) {
            if (this._filterParser.selectedcheckboxGameTypeArr.length == 0 && this._filterParser.selectedcheckboxPlayersArr.length == 0
                && this._filterParser.selectedcheckboxTurnTypeArr == 0) {
                filterArray = listArray;
            } else {
                if (this._filterParser.selectedcheckboxGameTypeArr.length != 0) {
                    if (this._filterParser.selectedcheckboxGameTypeArr.indexOf(listArray[key].mGameType.split("-")[1]) >= 0) {
                        if (this._filterParser.selectedcheckboxPlayersArr.length != 0) {
                            if (this._filterParser.selectedcheckboxPlayersArr.indexOf(listArray[key].mMaxPlayers.toString()) >= 0) {
                                if (this._filterParser.selectedcheckboxTurnTypeArr.length != 0) {
                                    if (this._filterParser.selectedcheckboxTurnTypeArr.indexOf(listArray[key].mTurnType) >= 0) {
                                        filterArray.push(listArray[key]);
                                    }
                                } else {
                                    filterArray.push(listArray[key]);
                                }
                            }
                        } else {
                            if (this._filterParser.selectedcheckboxTurnTypeArr.length != 0) {
                                if (this._filterParser.selectedcheckboxTurnTypeArr.indexOf(listArray[key].mTurnType) >= 0) {
                                    filterArray.push(listArray[key]);
                                }
                            } else {
                                filterArray.push(listArray[key]);
                            }
                        }
                    }
                } else {
                    if (this._filterParser.selectedcheckboxPlayersArr.length != 0) {
                        if (this._filterParser.selectedcheckboxPlayersArr.indexOf(listArray[key].mMaxPlayers.toString()) >= 0) {
                            if (this._filterParser.selectedcheckboxTurnTypeArr.length != 0) {
                                if (this._filterParser.selectedcheckboxTurnTypeArr.indexOf(listArray[key].mTurnType) >= 0) {
                                    filterArray.push(listArray[key]);
                                }
                            } else {
                                filterArray.push(listArray[key]);
                            }
                        }
                    } else {
                        if (this._filterParser.selectedcheckboxTurnTypeArr.length != 0) {
                            if (this._filterParser.selectedcheckboxTurnTypeArr.indexOf(listArray[key].mTurnType) >= 0) {
                                filterArray.push(listArray[key]);
                            }
                        } else {
                            filterArray.push(listArray[key]);
                        }
                    }
                }
            }
        }
        return filterArray;
    },
    pauseListener: function (bool) {
        this._isEnabled = !bool;
    }
});

RoomListManager.prototype.initTouchListener = function (element) {
    this.roomListManagerTouchListener = cc.EventListener.create({
        event: cc.EventListener.TOUCH_ONE_BY_ONE,
        swallowTouches: true,
        onTouchBegan: function (touch, event) {
            var target = event.getCurrentTarget();
            var locationInNode = target.convertToNodeSpace(touch.getLocation());
            var targetSize = target.getContentSize();
            var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);

            var targetRectToWorld = target.getBoundingBoxToWorld();
            var insideListViewBool = cc.rectContainsRect(this.listView.getBoundingBoxToWorld(), targetRectToWorld);

            if (cc.rectContainsPoint(targetRect, locationInNode) && this._isEnabled && insideListViewBool) {
                return true;
            }
            return false;
        }.bind(this),
        onTouchEnded: function (touch, event) {
            var target = event.getCurrentTarget();
            var parent = target.getParent();
            applicationFacade._controlAndDisplay.closeAllTab();
            applicationFacade._lobby.closeAllTab();
            switch (target._name) {
                case "Watch":
                    parent.watchButtonListener();
                    break;
                case "joinBtn":
                    parent._joinRoom();
                    break;
            }
        }.bind(this)
    });
    this.roomListManagerTouchListener.retain();
};
RoomListManager.prototype.initMouseListener = function () {
    this.roomListManagerMouseListener = cc.EventListener.create({
        event: cc.EventListener.MOUSE,
        onMouseMove: function (event) {
            if (this._isEnabled) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);

                var targetRect = target.getBoundingBoxToWorld();
                var insideListViewBool = cc.rectContainsRect(this.listView.getBoundingBoxToWorld(), targetRect);
                
                if (cc.rectContainsPoint(rect, locationInNode) && applicationFacade._activeTableId == 0 && insideListViewBool) {
                    !target._hovering && target.hover(true);
                    target._hovering = true;
                    return true;
                }
                target._hovering && target.hover(false);
                target._hovering = false;
                return false;
            }
        }.bind(this)
    });
    this.roomListManagerMouseListener.retain();
};


/*
 * this.listView._scrollDistance += delta;
 // this.listView.scrollToPercentVertical();

 var target = event.getCurrentTarget();
 var locationInNode = target.convertToNodeSpace(event.getLocation());
 var s = target.getContentSize();
 var rect = cc.rect(0, 0, s.width, s.height);
 if (cc.rectContainsPoint(rect, locationInNode)) {
 target.setOpacity(220);
 // cc.$("#gameCanvas").style.cursor = "pointer";
 return true;
 } else {
 target.setOpacity(255);
 return false;
 }


 return true;
 *
 * */