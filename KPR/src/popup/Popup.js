/**
 * Created by stpl on 11/8/2016.
 */
var Popup = cc.Scale9Sprite.extend({
    _gameRoom: null,
    popupMouseListener: null,
    popupTouchListener: null,
    whiteSpace: null,
    ctor: function (size, context, overlay) {
        this._super(spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png"), cc.rect(0, 0, 50 * scaleFactor, 50 * scaleFactor));
        this.setCapInsets(cc.rect(16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor));
        this.setPosition(cc.p(cc.winSize.width / 2, cc.winSize.height / 2));

        if (!context) {
            var get = context['generateError'];
            return;
        }
        this._gameRoom = context;

        if (size instanceof cc.Size) {
            this.setContentSize(size);
        } else {
            throw "size should be of type cc.Size";
        }
        var self = this;
        if (overlay) {
            var node = new cc.DrawNode();
            node.drawRect(cc.p(0, 0), cc.p(cc.winSize.width, cc.winSize.height), cc.color(0, 0, 0, 130), 1, cc.color(0, 0, 0, 130));
            node.setPosition(cc.p(-cc.winSize.width / 2 + this.width / 2, -cc.winSize.height / 2 + this.height / 2));
            cc.eventManager.addListener({
                event: cc.EventListener.TOUCH_ONE_BY_ONE,
                swallowTouches: true,
                onTouchBegan: function (touch, event) {
                    cc.log("Blank listener in node overlay");
                    return true;
                }
            }, node);
            this.addChild(node, -1);
        } else {
            cc.eventManager.addListener({
                event: cc.EventListener.TOUCH_ONE_BY_ONE,
                swallowTouches: true,
                onTouchBegan: function (touch, event) {
                    var target = event.getCurrentTarget();
                    var locationInNode = target.convertToNodeSpace(touch.getLocation());
                    var targetSize = target.getContentSize();
                    var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                    if (cc.rectContainsPoint(targetRect, locationInNode)) {
                        self.selfBgTouchListener(touch, event);
                        return true;
                    }
                    return false;
                }
            }, this);
        }

        this.headerLeft = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("PopUpHeader.png"), cc.rect(0, 0, 36 * scaleFactor, 35 * scaleFactor));
        this.headerLeft.setContentSize(cc.size(this.width * scaleFactor, 37 * scaleFactor));
        this.headerLeft.setCapInsets(cc.rect(16 * scaleFactor, 12 * scaleFactor, 16 * scaleFactor, 12 * scaleFactor));
        this.headerLeft.setPosition(cc.p(this.width / 2 * scaleFactor, this.height - this.headerLeft.height / 2 + 1 * scaleFactor));
        this.addChild(this.headerLeft, 1);

        this.headerLabel = new cc.LabelTTF("", "RobotoBold", 13 * 2 * scaleFactor);
        this.headerLabel.setScale(0.60);
        this.headerLabel.setAnchorPoint(0, 0.5);
        this.headerLabel.setPosition(cc.p(20 * scaleFactor, this.height - this.headerLeft.height / 2));
        this.addChild(this.headerLabel, 2);

        this.whiteSpace = this.height - this.headerLeft.height;

        var shadowW = size.width + 15 * scaleFactor;
        var shadowH = size.height + 15 * scaleFactor;
        var shadow = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("shadow.png"));
        shadow.setContentSize(cc.size(shadowW, shadowH));
        shadow.setPosition(cc.p(this.width / 2, this.height / 2 - 3 * scaleFactor));
        this.addChild(shadow, -2);

        return true;

    },
    selfBgTouchListener: function (touch, event) {
        cc.log("Blank listener in Popup");
    },
    initMouseListener: function () {
        this.popupMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            swallowTouches: true,
            onMouseMove: this.onMouseMoveCallBack.bind(this)
        });
        this.popupMouseListener.retain();
    },
    initTouchListener: function () {
        this.popupTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: this.onTouchBeganCallBack.bind(this),
            onTouchEnded: this.onTouchEndedCallBack.bind(this)
        });
        this.popupTouchListener.retain();
    },
    /**
     * @override
     */
    onMouseMoveCallBack: function (event) {
        var target = event.getCurrentTarget();
        var locationInNode = target.convertToNodeSpace(event.getLocation());
        var s = target.getContentSize();
        var rect = cc.rect(0, 0, s.width, s.height);
        if (cc.rectContainsPoint(rect, locationInNode) && target._isEnabled) {
            !target._hovering && target.hover(true);
            target._hovering = true;
            return true;
        }
        target._hovering && target.hover(false);
        target._hovering = false;
        return false;
    },
    /**
     * @override
     */
    onTouchBeganCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        var locationInNode = target.convertToNodeSpace(touch.getLocation());
        var targetSize = target.getContentSize();
        var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
        if (cc.rectContainsPoint(targetRect, locationInNode)) {
            return true;
        }
    },
    /**
     * @override
     */
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
    },
    onExit: function () {
        this._super();
        this.removeAllChildren(true);
        cc.eventManager.removeListener(this.popupMouseListener, true);
        cc.eventManager.removeListener(this.popupTouchListener, true);
        this.release();
    }
});

var LobbyLoadBanner = cc.Scale9Sprite.extend({
    popupTouchListener: null,
    link: null,
    ctor: function (data) {
        this._super(spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png"), cc.rect(0, 0, 50 * scaleFactor, 50 * scaleFactor));
        this.setCapInsets(cc.rect(16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor));
        this.setPosition(cc.p(cc.winSize.width / 2, cc.winSize.height / 2));
        this.setColor(cc.color(146, 146, 146));
        this.setCascadeColorEnabled(false);
        this.setContentSize(cc.size(655 * scaleFactor, 405 * scaleFactor));

        this.initTouchListener();

        var lay = new ccui.Layout();
        lay.setContentSize(cc.size(650 * scaleFactor, 400 * scaleFactor));
        lay.setClippingEnabled(true);
        lay.setPosition(cc.p(2 * scaleFactor, 2 * scaleFactor));
        this.addChild(lay, 12);

        if (data.path) {
            var image = new cc.Sprite(data.path);
            image.setPosition(cc.p(lay.width / 2, lay.height / 2));
            lay.addChild(image, 1, "bannerImage");
            this.link = data.url;
            cc.eventManager.addListener(this.popupTouchListener.clone(), image);
        }

        var closeBtn = new cc.Sprite(spriteFrameCache.getSpriteFrame("ResultClose.png"));
        closeBtn.setPosition(cc.p(this.width, this.height));
        this.addChild(closeBtn, 20, "closeBtn");
        cc.eventManager.addListener(this.popupTouchListener.clone(), closeBtn);

        var node = new cc.DrawNode();
        node.drawRect(cc.p(0, 0), cc.p(cc.winSize.width, cc.winSize.height), cc.color(0, 0, 0, 180), 1, cc.color(0, 0, 0, 180));
        node.setPosition(cc.p(-cc.winSize.width / 2 + this.width / 2, -cc.winSize.height / 2 + this.height / 2));
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                cc.log("Blank listener in banner");
                return true;
            }.bind(this)
        }, node);
        this.addChild(node, -2);
        return true;

    },
    initTouchListener: function () {
        this.popupTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                if (target._name == 'closeBtn') {
                    this.removeFromParent(true);
                } else if (target.getName() == "bannerImage") {
                    this.link && cc.sys.openURL(this.link);
                }
            }.bind(this)
        });
        this.popupTouchListener.retain();
    }
});

var MaxJoinTablePopup = Popup.extend({
    ctor: function (context) {
        this._super(new cc.Size(460 * scaleFactor, 180 * scaleFactor), context, true);

        this.ccTimeOutManager = new CCTimeOutManager();
        this.headerLabel.setString("Message from KhelPlayRummy");

        var font = 21 * scaleFactor;
        var richText1 = new ccui.RichText();
        var text1 = new ccui.RichElementText(1, cc.color.BLACK, 255, "If you are playing ", "RobotoRegular", font);
        var text2 = new ccui.RichElementText(1, cc.color.BLACK, 255, "Pool/Point ", "RobotoBold", font);
        var text3 = new ccui.RichElementText(1, cc.color.BLACK, 255, "you can join maximum", "RobotoRegular", font);
        richText1.setScale(0.7);
        richText1.pushBackElement(text1);
        richText1.pushBackElement(text2);
        richText1.pushBackElement(text3);
        richText1.setPosition(cc.p(this.width / 2, this.height - 60 * scaleFactor));
        this.addChild(richText1);
        var text4 = new cc.LabelTTF("number of table is 3", "RobotoRegular", font);
        text4.setPosition(cc.p(this.width / 2, richText1.y - text4.height / 1.2));
        text4.setColor(cc.color.BLACK);
        text4.setScale(0.7);
        this.addChild(text4, 1);

        var orMsg = new cc.LabelTTF("OR", "RobotoBold", 18 * 2 * scaleFactor);
        orMsg.setScale(0.5);
        orMsg.setColor(cc.color.BLACK);
        orMsg.setPosition(cc.p(this.width / 2, text4.y - orMsg.height / 1.7));
        this.addChild(orMsg, 1);

        var richText2 = new ccui.RichText();
        text1 = new ccui.RichElementText(1, cc.color.BLACK, 255, "If you are playing ", "RobotoRegular", font);
        text2 = new ccui.RichElementText(1, cc.color.BLACK, 255, "Pool/Point/Tournaments ", "RobotoBold", font);
        text3 = new ccui.RichElementText(1, cc.color.BLACK, 255, "you can join", "RobotoRegular", font);
        richText2.setScale(0.7);
        richText2.pushBackElement(text1);
        richText2.pushBackElement(text2);
        richText2.pushBackElement(text3);
        richText2.setPosition(cc.p(this.width / 2, orMsg.y - orMsg.height / 1.6));
        this.addChild(richText2);
        text4 = new cc.LabelTTF("maximum number of table is 5", "RobotoRegular", font);
        text4.setPosition(cc.p(this.width / 2, richText2.y - text4.height / 1.2));
        text4.setColor(cc.color.BLACK);
        text4.setScale(0.7);
        this.addChild(text4, 1);

        var cross = new CreateBtn("ResultClose", "ResultClose", "close");
        cross.setPosition(this.width - 3 * scaleFactor, this.height - 3 * scaleFactor);
        this.addChild(cross, 50);

        this.initTouchListener();
        cc.eventManager.addListener(this.popupTouchListener.clone(), cross);

    },
    onTouchEndedCallBack: function (touch, event) {
        this.removeFromParent(true);
    }
});
var Confirmation = Popup.extend({
    _name: "GameStartConfirmationPopup",
    ctor: function (context) {
        this._super(new cc.Size(500 * scaleFactor, 170 * scaleFactor), context);

        this.ccTimeOutManager = new CCTimeOutManager();

        this.headerLabel.setString("Confirmation");

        this.message = new cc.LabelTTF("Game starts in 5 Secs", "RobotoBold", 13 * 2 * scaleFactor, cc.TEXT_ALIGNMENT_CENTER);
        this.message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.message.setScale(0.6);
        this.message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        this.message.setColor(cc.color(25, 25, 25));
        this.message.setPosition(cc.p(this.width / 2, this.height / 2 + 5));
        this.addChild(this.message);

        this.leaveTable = new CreateBtn('confirmationLeaveTable', 'confirmationLeaveTableHover');
        this.leaveTable.setPosition(cc.p(this.width / 2, this.height / 2 - 40 * scaleFactor));
        this.addChild(this.leaveTable, 2, 'leaveTable');

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), this.leaveTable);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.leaveTable);

        return true;
    },
    setMessage: function (msg) {
        this.message.setString(msg);
    },
    onTouchEndedCallBack: function (touch, event) {
        this._leaveTableReq();
    },
    _leaveTableReq: function () {
        sfs.send(new SFS2X.Requests.System.LeaveRoomRequest(sfs.getRoomById(this._gameRoom._serverRoom.id)));
        cc.log("leave request");
        // this._gameRoom._playerArray = null;
        this._leaveTable();
    },
    _leaveTable: function () {
        this.ccTimeOutManager.unScheduleInterval("leaveTableTime");
        this.removeFromParent(true);
    },
    setTime: function (timeOut) {
        var remainingTime = timeOut + " Sec.";
        this.setMessage("Game starts in " + remainingTime);
        if (timeOut == 1) {
            var self = this;
            setTimeout(function () {
                self.setMessage("Please wait while game starts...")
            }, 1000);
        }
    }
});

var LeaveTable = Popup.extend({
    _name: "leaveTablePopup",
    _gameRoom: null,
    /**
     * @param {Int} tableId
     * @param {class} context
     * @param {Boolean} overlay
     */
    ctor: function (tableId, context, overlay) {
        this._super(new cc.Size(500 * scaleFactor, 180 * scaleFactor), context, overlay);
        this._gameRoom = context;
        /*if (applicationFacade._tourRoomLeaveAllowed)
         applicationFacade._tourRoomLeaveAllowed = true;*/

        this.headerLabel.setString("Confirmation");

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        this.message = new cc.LabelTTF("", "RobotoRegular", 15 * 2 * scaleFactor);
        this.message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        this.message.setScale(0.5);
        this.message.setColor(cc.color(0, 0, 0));
        this.message.setPosition(cc.p(this.width / 2, 95 * scaleFactor));
        this.addChild(this.message, 2);

        if (!this._gameRoom._leaveAllowed) {

            this.message.setString("You can not leave the table during toss!");
            this.leaveOkButton = new CreateBtn("OkBtn", "OkBtnOver", "LeaveOkButton");
            this.leaveOkButton.setName("LeaveOkButton");
            this.leaveOkButton.setPosition(this.width / 2, this.height / 2 - 40 * scaleFactor);
            this.addChild(this.leaveOkButton, 1);
            cc.eventManager.addListener(this.popupMouseListener.clone(), this.leaveOkButton);
            cc.eventManager.addListener(this.popupTouchListener.clone(), this.leaveOkButton);

        } else {
            this.message.setString("Are you sure you want to leave the table " + tableId);

            this.leaveYesButton = new CreateBtn("yesBtn", "yesBtnOver", "LeaveYesButton");
            this.leaveYesButton.setName("LeaveYesButton");
            this.leaveYesButton.setPosition(this.width / 2 - GameConstants.popButtonMargin, this.height / 2 - 40 * scaleFactor);
            this.addChild(this.leaveYesButton, 1);

            this.leaveNoButton = new CreateBtn("noBtn", "noBtnOver", "LeaveNoButton");
            this.leaveNoButton.setName("LeaveNoButton");
            this.leaveNoButton.setPosition(this.width / 2 + GameConstants.popButtonMargin, this.height / 2 - 40 * scaleFactor);
            this.addChild(this.leaveNoButton, 1);

            cc.eventManager.addListener(this.popupMouseListener.clone(), this.leaveYesButton);
            cc.eventManager.addListener(this.popupMouseListener.clone(), this.leaveNoButton);

            cc.eventManager.addListener(this.popupTouchListener.clone(), this.leaveYesButton);
            cc.eventManager.addListener(this.popupTouchListener.clone(), this.leaveNoButton);

        }
        return true;
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        switch (target._name) {
            case "LeaveYesButton":
                this._leaveTable(true);
                break;
            case "LeaveNoButton":
                this._leaveTable(false);
                break;
            case "LeaveOkButton":
                this._leaveTable(false);
                break;
        }
    },
    _leaveTable: function (bool) {
        if (bool) {
            this._gameRoom.yesLeaveBtnHandler();
        } else {
            this.removeFromParent(true);
            // this._gameRoom.noLeaveBtnHandler();
        }
    }

});

var Logout = cc.LayerColor.extend({
    _name: "logout",
    ctor: function () {
        this._super(cc.color(0, 0, 0, 120 * scaleFactor));

        this.blackBox = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("blackBoxPopUp.png"), cc.rect(0, 0, 50 * scaleFactor, 50 * scaleFactor));
        this.blackBox.setCapInsets(cc.rect(16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor));
        this.blackBox.setContentSize(cc.size(360 * scaleFactor, 100 * scaleFactor));
        this.blackBox.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(this.blackBox);

        this.message = new cc.LabelTTF("Your session is no longer valid.\n Please login again.", "RobotoRegular", 22 * scaleFactor);
        this.message.setScale(0.8);
        this.message.setColor(cc.color(255, 255, 255));
        this.message.setPosition(cc.p(this.width / 2 + 25 * scaleFactor, this.height / 2));
        this.message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.addChild(this.message);

        this.anchorIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("serverMaintenanceInfo.png"));
        this.anchorIcon.setScale(1.4);
        this.anchorIcon.setPosition(cc.p(374 * scaleFactor, this.height / 2));
        this.addChild(this.anchorIcon);

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                cc.log("Blank listener in Logout Popup");
                return true;
            }.bind(this)
        }, this);

    }
});
var serverRestartPopup = Logout.extend({
    _name: "serverRestart",
    ctor: function () {
        this._super();

        this.ccTimeOutManager = new CCTimeOutManager();
        this.blackBox.setContentSize(cc.size(740 * scaleFactor, 180 * scaleFactor));
        this.message.setFontSize(20 * scaleFactor);
        this.message.setString("Sorry for the interruption! Your game will resume in    ");
        this.message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
        this.anchorIcon.setPosition(cc.p(200 * scaleFactor, this.height / 2));

        this.message.setPosition(cc.p(490 * scaleFactor, this.height / 2 + 20));
        this.time = new cc.LabelTTF("", "RobotoBold", 24 * scaleFactor);
        this.time.setString("05:00");
        this.time.setScale(0.8);
        this.time.setColor(cc.color(224, 159, 62));
        this.time.setPosition(cc.p(this.message.x + this.message.width / 2 - 30, this.height / 2 + 20));
        this.time.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
        this.addChild(this.time);

        var minLabel = new cc.LabelTTF("mins.", "RobotoRegular", 20 * scaleFactor);
        minLabel.setScale(0.8);
        minLabel.setColor(cc.color(255, 255, 255));
        minLabel.setPosition(cc.p(this.time.x + this.time.width - 5, this.height / 2 + 20));
        minLabel.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
        this.addChild(minLabel);

        this.serverRestartLabel = new cc.LabelTTF("5 mins,", "RobotoRegular", 20 * scaleFactor);
        this.serverRestartLabel.setScale(0.8);
        this.serverRestartLabel.setColor(cc.color(255, 255, 255));
        this.serverRestartLabel.setPosition(cc.p(this.time.x + this.time.width + 22, this.height / 2 - 1));
        this.serverRestartLabel.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
        this.addChild(this.serverRestartLabel);

        var msgLabel = new cc.LabelTTF("Please DO NOT close game lobby or log out. If game doesn't starts in \nit will be cancelled and your Buy-In Amount will be refunded to you within 24 hours.", "RobotoBold", 20 * scaleFactor);
        msgLabel.setScale(0.8);
        msgLabel.setColor(cc.color(255, 255, 255));
        msgLabel.setPosition(cc.p(550 * scaleFactor, this.height / 2 - 10));
        msgLabel.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
        this.addChild(msgLabel);

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                cc.log("Blank listener in serverRestart Popup");
                return true;
            }.bind(this)
        }, this);
    },
    setTime: function (time) {
        var timeInSec = time;
        var timeInMin = moment.utc(timeInSec * 1000).format('mm:ss');
        this.time.setString(timeInMin);
        this.serverRestartLabel.setString(timeInSec/60 + " mins,");
        this.ccTimeOutManager.scheduleInterval(xyz.bind(this), 1000, "serverRestartTimer");
        function xyz() {
            if (timeInSec >= 1) {
                timeInSec = timeInSec - 1;
                timeInMin = moment.utc(timeInSec * 1000).format('mm:ss');
                this.time.setString(timeInMin);
            } else {
                this.ccTimeOutManager.unScheduleInterval("serverRestartTimer");
                // this.removeFromParent(true);
            }
        }
    }
});

var GameNotification = cc.Scale9Sprite.extend({
    timeOut: null,
    ctor: function (msgStr) {

        this._super(spriteFrameCache.getSpriteFrame("smileyBorder.png"), cc.rect(0, 0, 41 * scaleFactor, 41 * scaleFactor));
        this.setCascadeColorEnabled(true);
        this.setCascadeOpacityEnabled(true);
        this.setCapInsets(cc.rect(13 * scaleFactor, 13 * scaleFactor, 13 * scaleFactor, 13 * scaleFactor));
        this.setAnchorPoint(0, 0.5);
        this.setContentSize(cc.size(200 * scaleFactor, 51 * scaleFactor));
        this.setPosition(cc.p(10 * scaleFactor, 580 * scaleFactor));

        this.timeOut = 0;

        var msg = new cc.LabelTTF(msgStr, "RobotoRegular", 13 * 2 * scaleFactor);
        msg.setScale(0.5);
        // msg.setAnchorPoint(0, 0.5);
        msg._setBoundingWidth(this.width * 2 - 20 * scaleFactor);
        msg.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        // msg.setPosition(cc.p(10 * scaleFactor, this.height - msg.height / 2));
        msg.setPosition(cc.p(this.width / 2, this.height / 2));
        msg.setColor(cc.color(100, 100, 100));
        this.addChild(msg, 2);
    },
    tween: function () {
        var fade = cc.fadeIn(1.5);
        var that = this;
        this.runAction(cc.sequence(fade, cc.callFunc(function () {
            this.runAction(cc.sequence(fade.reverse(), cc.callFunc(function () {
                this.removeFromParent(true);
            }, this)));
        }, this)));
    }
});

var BonusToCash = cc.Scale9Sprite.extend({
    _name: "bonusToCash",
    ctor: function (amount, context) {
        this._super(spriteFrameCache.getSpriteFrame("smileyBorder.png"));
        this.setContentSize(cc.size(390 * scaleFactor, 60 * scaleFactor));
        this.setAnchorPoint(1, 0);
        this.setPosition(cc.p(cc.winSize.width - 10 * scaleFactor, 40 * scaleFactor));

        var tickBtn = new cc.Sprite(spriteFrameCache.getSpriteFrame("bonusTick.png"));
        tickBtn.setPosition(cc.p(25 * scaleFactor, this.height / 2));
        this.addChild(tickBtn, 2);

        this.bonusCloseBtn = new cc.Sprite(spriteFrameCache.getSpriteFrame("closeBlack.png"));
        this.bonusCloseBtn.setPosition(cc.p(this.width, this.height));
        this.addChild(this.bonusCloseBtn, 1, "bonusCloseBtn");

        var congrats = new cc.LabelTTF("Congratulations!", "RobotoBold", 26 * scaleFactor);
        congrats.setScale(0.5);
        congrats.setColor(cc.color(173, 208, 0));
        congrats.setAnchorPoint(0, 0.5);
        congrats.setPosition(cc.p(48 * scaleFactor, 38 * scaleFactor));
        this.addChild(congrats, 2);

        var str = "Rs." + amount + " has been disbursed from your";

        this.bonusTxt = new cc.LabelTTF(str, "RobotoRegular", 26 * scaleFactor);
        this.bonusTxt.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
        this.bonusTxt.setScale(0.5);
        this.bonusTxt.setAnchorPoint(0, 0.5);
        this.bonusTxt.setPosition(cc.p(150 * scaleFactor, 38 * scaleFactor));
        this.addChild(this.bonusTxt, 2);

        var disbursed = new cc.LabelTTF("bonus account and credited in your cash.!", "RobotoRegular", 26 * scaleFactor);
        disbursed.setScale(0.5);
        disbursed.setAnchorPoint(0, 0.5);
        disbursed.setPosition(cc.p(48 * scaleFactor, 20 * scaleFactor));
        this.addChild(disbursed, 2);
        var self = this;
        setTimeout(function () {
            self.removeFromParent(true);
        }, 10000)
    }
});


var DisconnectionPopup = cc.LayerColor.extend({
    ctor: function (context) {
        this._super(cc.color(0, 0, 0, 160 * scaleFactor));

        var loaderIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("discnLoader.png"));
        loaderIcon.setPosition(cc.p(this.width / 2, this.height / 2 + 30 * scaleFactor));
        this.addChild(loaderIcon);
        loaderIcon.runAction(cc.rotateBy(1, 360, 0).repeatForever());

        var message = new cc.LabelTTF("You seem to have a problem with your internet connectivity.\nWe are waiting for you to join back soon.", "RobotoRegular", 26 * scaleFactor);
        message.setScale(0.6);
        message.setColor(cc.color(255, 255, 255));
        message.setPosition(cc.p(this.width / 2, this.height / 2 - 30 * scaleFactor));
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.addChild(message);

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                cc.log("Blank listener in Disconnection Popup");
                return true;
            }.bind(this)
        }, this);
    }
});
var NoRoomPop = Popup.extend({
    _name: "NoBalPopup",
    _gameRoom: null,
    ctor: function (string, context, overlay) {
        this._super(new cc.Size(500 * scaleFactor, 180 * scaleFactor), context, overlay);
        this._gameRoom = context;

        this.headerLabel.setString("Message from KhelPlayRummy");

        var message = new cc.LabelTTF(string, "RobotoRegular", 15 * 2 * scaleFactor);
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        message.setScale(0.5);
        message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        message.setPosition(cc.p(this.width / 2, 95 * scaleFactor));
        message.setColor(cc.color(0, 0, 0));
        this.addChild(message);

        this.yesButton = new CreateBtn("yesBtn", "yesBtnOver", "YesButton");
        this.yesButton.setPosition(this.width / 2 - GameConstants.popButtonMargin, this.height / 2 - 40 * scaleFactor);
        this.yesButton.setName("YesButton");
        this.addChild(this.yesButton);

        this.noButton = new CreateBtn("noBtn", "noBtnOver", "NoButton");
        this.noButton.setPosition(this.width / 2 + GameConstants.popButtonMargin, this.height / 2 - 40 * scaleFactor);
        this.noButton.setName("NoButton");
        this.addChild(this.noButton);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), this.yesButton);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.noButton);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.yesButton);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.noButton);

        return true;
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        switch (target._name) {
            case "YesButton":
                if (target._isEnabled)
                    this.yesBtnHandler();
                break;
            case "NoButton":
                if (target._isEnabled)
                    this.noBtnHandler();
                break;
        }
    },
    yesBtnHandler: function () {
        applicationFacade.updateExternalLobbyBalance();
        window.parent.postMessage("openCashPopup", "*");
        this.removeFromParent(true);
    },
    noBtnHandler: function () {
        this.removeFromParent(true);
    }
});


var NoBalPopup = Popup.extend({
    _name: "NoBalPopup",
    _gameRoom: null,
    ctor: function (string, context) {
        this._super(new cc.Size(500 * scaleFactor, 180 * scaleFactor), context);
        this._gameRoom = context;

        this.headerLabel.setString("Message from KhelPlayRummy");

        var message = new cc.LabelTTF(string, "RobotoRegular", 15 * 2 * scaleFactor);
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        message.setScale(0.5);
        message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        message.setPosition(cc.p(this.width / 2, 95 * scaleFactor));
        message.setColor(cc.color(0, 0, 0));
        this.addChild(message);

        this.yesButton = new CreateBtn("yesBtn", "yesBtnOver", "YesButton");
        this.yesButton.setPosition(this.width / 2 - GameConstants.popButtonMargin, this.height / 2 - 40 * scaleFactor);
        this.yesButton.setName("YesButton");
        this.addChild(this.yesButton);

        this.noButton = new CreateBtn("noBtn", "noBtnOver", "NoButton");
        this.noButton.setPosition(this.width / 2 + GameConstants.popButtonMargin, this.height / 2 - 40 * scaleFactor);
        this.noButton.setName("NoButton");
        this.addChild(this.noButton);

        this.okButton = new CreateBtn("OkBtn", "OkBtnOver", "OkButton");
        this.okButton.setPosition(this.width / 2, this.height / 2 - 40 * scaleFactor);
        this.okButton.setName("OkButton");
        this.addChild(this.okButton);

        this.hide(this.yesButton, true);
        this.hide(this.noButton, true);
        this.hide(this.okButton, true);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), this.yesButton);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.noButton);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.okButton);


        cc.eventManager.addListener(this.popupTouchListener.clone(), this.yesButton);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.noButton);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.okButton);

        return true;
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        switch (target._name) {
            case "YesButton":
                if (target._isEnabled)
                    this.yesBtnHandler();
                break;
            case "NoButton":
                if (target._isEnabled)
                    this.noBtnHandler();
                break;
            case "OkButton":
                if (target._isEnabled)
                    this.okBtnHandler();
                break;
        }
    },
    yesBtnHandler: function () {
        applicationFacade.updateExternalLobbyBalance();
        sfs.send(new SFS2X.Requests.System.LeaveRoomRequest(sfs.getRoomById(this._gameRoom._serverRoom.id)));
        this._gameRoom.waitingMC.setVisible(false);
        this.removeFromParent(true);
        cc.sys.openURL("https://www.khelplayrummy.com/select-amount");
    },
    noBtnHandler: function () {
        sfs.send(new SFS2X.Requests.System.LeaveRoomRequest(sfs.getRoomById(this._gameRoom._serverRoom.id)));
        this.removeFromParent(true);
    },
    okBtnHandler: function () {
        sfs.send(new SFS2X.Requests.System.LeaveRoomRequest(sfs.getRoomById(this._gameRoom._serverRoom.id)));
        this._gameRoom.waitingMC.setVisible(false);
        this.removeFromParent(true);

    },
    hide: function (target, bool) {
        target.setVisible(!bool);
        target._isEnabled = !bool;
    }
});

var DropConfirmation = Popup.extend({
    _userCardPanel: null,
    ctor: function (context) {

        this._super(new cc.Size(500 * scaleFactor, 180 * scaleFactor), context);
        this._userCardPanel = context;

        this.headerLabel.setString("Drop");

        this.message = new cc.LabelTTF("Are you sure you want to drop?", "RobotoRegular", 15 * 2 * scaleFactor);
        this.message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        this.message.setScale(0.5);
        this.message.setPosition(cc.p(this.width / 2, 95 * scaleFactor));
        this.message.setColor(cc.color(0, 0, 0));
        this.addChild(this.message, 1);

        if (this._userCardPanel.DropBtn.currentState == 3) {
            this.message.setString("Are you sure you want to drop on your turn?");
        }

        this.dropYes = this.createSprite("yesBtn", "yesBtnOver", "dropYes");
        this.dropYes.setPosition(cc.p(this.width / 2 - GameConstants.popButtonMargin, this.height / 2 - 40 * scaleFactor));
        this.addChild(this.dropYes, 5);

        this.dropNo = this.createSprite("noBtn", "noBtnOver", "dropNo");
        this.dropNo.setPosition(this.width / 2 + GameConstants.popButtonMargin, this.height / 2 - 40 * scaleFactor);
        this.addChild(this.dropNo, 5);

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), this.dropYes);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.dropNo);

        cc.eventManager.addListener(this.popupTouchListener.clone(), this.dropYes);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.dropNo);

        this.setPosition(cc.p(cc.winSize.width / 2, cc.winSize.height / 2 + 65 * scaleFactor));

        return true;
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        switch (target._name) {
            case "dropYes":
                this._userCardPanel.dropYesBtnHandler();
                break;
            case "dropNo":
                this._userCardPanel.dropNoBtnHandler();
                break;
        }
    },
    createSprite: function (spriteName, spriteOverName, buttonName, pos) {
        var Sprite = cc.Sprite.extend({
            spriteOverName: null,
            spriteName: null,
            _isEnabled: null,
            _hovering: null,
            ctor: function (spriteName, spriteOverName, buttonName, pos) {
                this._super(spriteFrameCache.getSpriteFrame(spriteName + ".png"));
                this.spriteName = spriteName;
                this.spriteOverName = spriteOverName;
                this._hovering = false;
                pos && this.setPosition(pos);
                this.setName(buttonName);

                return true;
            },
            hover: function (bool) {

                bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteOverName + ".png")) :
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteName + ".png"));
            },
            pauseListener: function (bool) {
                this._isEnabled = !bool;
            },
            hide: function (bool) {
                this.setVisible(!bool);
                this._isEnabled = !bool;
            }
        });
        return new Sprite(spriteName, spriteOverName, buttonName, pos);
    }

});
var MessagePopUp = Popup.extend({
    _name: "MessagePopUp",
    _gameRoom: null,
    ctor: function (context, overlay) {
        this._super(new cc.Size(450 * scaleFactor, 160 * scaleFactor), context, overlay);
        this._gameRoom = context;

        this.headerLabel.setString("Message from KhelPlayRummy");

        this.message = new cc.LabelTTF("", "RobotoRegular", 14 * 2 * scaleFactor);
        this.message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        this.message.setScale(0.5);
        this.message.setColor(cc.color(25, 25, 25));
        this.message.setPosition(cc.p(this.width / 2, this.height / 2));
        this.message.multiline = true;
        this.addChild(this.message, 2);

        return true;
    },
    setMsg: function (txt) {
        this.message.setString(txt);
    },
    hide: function (bool) {
        this.setVisible(!bool);
    }

});

var ShowConfirmation = Popup.extend({
    _userCardPanel: null,
    ctor: function (context) {

        this._super(new cc.Size(500 * scaleFactor, 180 * scaleFactor), context, true);
        this._userCardPanel = context;

        this.headerLabel.setString("Show");

        var message = new cc.LabelTTF("Kindly re-confirm whether you want to place a SHOW?", "RobotoRegular", 15 * 2 * scaleFactor);
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        message.setScale(0.5);
        message.setPosition(cc.p(this.width / 2, 95 * scaleFactor));
        message.setColor(cc.color(0, 0, 0));
        this.addChild(message);

        this.showYes = new CreateBtn("yesBtn", "yesBtnOver", "showYes");
        this.showYes.setPosition(cc.p(this.width / 2 - GameConstants.popButtonMargin, this.height / 2 - 40 * scaleFactor));
        this.addChild(this.showYes, 5);

        this.showNo = new CreateBtn("noBtn", "noBtnOver", "showNo");
        this.showNo.setPosition(this.width / 2 + GameConstants.popButtonMargin, this.height / 2 - 40 * scaleFactor);
        this.addChild(this.showNo, 5);

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), this.showYes);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.showNo);

        cc.eventManager.addListener(this.popupTouchListener.clone(), this.showYes);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.showNo);

        return true;
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        var targetName = target.getName();
        switch (target._name) {
            case "showYes":
                this._userCardPanel.showCardHandler(this._userCardPanel._cardToShow);
                break;
            case "showNo":
                this._userCardPanel.showNoBtnHandler();
                break;
        }
    }
});

var MeldConfirm = Popup.extend({
    _meldingPanel: null,
    ctor: function (context, str) {

        this._super(new cc.Size(500 * scaleFactor, 160 * scaleFactor), context);
        this._meldingPanel = context;

        this.headerLabel.setString("Confirmation");
        this.setPosition(cc.p(cc.winSize.width / 2, cc.winSize.height / 2 + 55));

        var message = new cc.LabelTTF(str, "RobotoRegular", 15 * 2 * scaleFactor);
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        message.setScale(0.5);
        message.setPosition(cc.p(this.width / 2, this.height / 2 + 15));
        message.setColor(cc.color(0, 0, 0));
        this.addChild(message);

        this.showYes = this.createSprite("yesBtn", "yesBtnOver", "showYes");
        this.showYes.setPosition(cc.p(this.width / 2 - GameConstants.popButtonMargin, this.height / 2 - 40 * scaleFactor));
        this.addChild(this.showYes, 5);

        this.showNo = this.createSprite("noBtn", "noBtnOver", "showNo");
        this.showNo.setPosition(this.width / 2 + GameConstants.popButtonMargin, this.height / 2 - 40 * scaleFactor);
        this.addChild(this.showNo, 5);

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), this.showYes);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.showNo);

        cc.eventManager.addListener(this.popupTouchListener.clone(), this.showYes);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.showNo);

        return true;
    },
    onTouchBeganCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        var locationInNode = target.convertToNodeSpace(touch.getLocation());
        var targetSize = target.getContentSize();
        var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
        if (cc.rectContainsPoint(targetRect, locationInNode)) {
            if (target.isVisible()) {
                return true;
            }
        }
        return false;
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        switch (target._name) {
            case "showYes":
            {
                this._meldingPanel.yesBtnHandler();
                this.removeFromParent(true);
                break;
            }
            case "showNo":
                this.removeFromParent(true);
                break;
        }
    },
    createSprite: function (spriteName, spriteOverName, buttonName, pos) {
        var Sprite = cc.Sprite.extend({
            spriteOverName: null,
            spriteName: null,
            _isEnabled: null,
            _hovering: null,
            ctor: function (spriteName, spriteOverName, buttonName, pos) {
                this._super(spriteFrameCache.getSpriteFrame(spriteName + ".png"));
                this.spriteName = spriteName;
                this.spriteOverName = spriteOverName;
                this._hovering = false;
                pos && this.setPosition(pos);
                this.setName(buttonName);

                return true;
            },
            hover: function (bool) {
                bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteOverName + ".png")) :
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteName + ".png"));
            },
            pauseListener: function (bool) {
                this._isEnabled = !bool;
            },
            hide: function (bool) {
                this.setVisible(!bool);
                this._isEnabled = !bool;
            }
        });
        return new Sprite(spriteName, spriteOverName, buttonName, pos);
    }
});

var CreateBtn = cc.Sprite.extend({
    spriteOverName: null,
    spriteName: null,
    _isEnabled: null,
    _hovering: null,
    toolTip: null,
    ctor: function (spriteName, spriteOverName, buttonName, pos) {
        this._super(spriteFrameCache.getSpriteFrame(spriteName + ".png"));
        this.spriteName = spriteName;
        this.spriteOverName = spriteOverName;
        this._isEnabled = true;
        this._hovering = false;

        pos && this.setPosition(pos);
        buttonName && this.setName(buttonName);
        return true;
    },
    hover: function (bool) {
        if (this._isEnabled) {
            bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteOverName + ".png")) :
                this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteName + ".png"));
        }
    },
    showToolTip: function () {
        //toolTip
    },
    pauseListener: function (bool) {
        this._isEnabled = !bool;
    },
    hide: function (bool) {
        this.setVisible(!bool);
        this._isEnabled = !bool;
    }
});