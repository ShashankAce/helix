"use strict";
/*
 *
 * var __nativeST__ = window.setTimeout,
 __nativeSI__ = window.setInterval;

 window.setTimeout = function (vCallback, timeOut, argumentToPass1, argumentToPass2) {
 var oThis = this,
 aArgs = Array.prototype.slice.call(arguments, 2);
 return __nativeST__(vCallback instanceof Function ? function () {
 vCallback.apply(oThis, aArgs);
 } : vCallback, timeOut);
 };

 window.setInterval = function (vCallback, timeOut, argumentToPass1, argumentToPass2) {
 var oThis = this,
 aArgs = Array.prototype.slice.call(arguments, 2);
 return __nativeSI__(vCallback instanceof Function ? function () {
 vCallback.apply(oThis, aArgs);
 } : vCallback, timeOut);
 };
 var updateTimeId = setInterval(this.updateTime.bind(this), 1000);
 *
 * @namespace
 * @name TimeOutManager
 * */

var CCTimeOutManager = function () {

    var __nativeST__ = window.setTimeout,
        __nativeSI__ = window.setInterval;

    window.setTimeout = function (vCallback, timeOut, argumentToPass1, argumentToPass2) {
        var oThis = this,
            aArgs = Array.prototype.slice.call(arguments, 2);
        return __nativeST__(vCallback instanceof Function ? function () {
            vCallback.apply(oThis, aArgs);
        } : vCallback, timeOut);
    };

    window.setInterval = function (vCallback, timeOut, argumentToPass1, argumentToPass2) {
        var oThis = this,
            aArgs = Array.prototype.slice.call(arguments, 2);
        return __nativeSI__(vCallback instanceof Function ? function () {
            vCallback.apply(oThis, aArgs);
        } : vCallback, timeOut);
    };
    this.timeoutIds = {};
    this.intervalIds = {};

    this.scheduleTimeOut = function (vCallback, timeOut, delay, schedulerId, argumentToPass1, argumentToPass2) {

        if (delay === "undefined") {
            throw new Error(" You must argument 3");
        }
        if (typeof delay === "string") {
            schedulerId = delay;
            delay = "undefined";
        }
        if (delay != "undefined") {
            if (delay == 0) {
                vCallback();
                var id = __nativeST__(vCallback, timeOut, argumentToPass1, argumentToPass2);
                this.timeoutIds[schedulerId] = id;
            }
            if (delay > 0) {
                __nativeST__(function () {
                    var id = __nativeST__(vCallback, timeOut, argumentToPass1, argumentToPass2);
                    this.timeoutIds[schedulerId] = id;
                }.bind(this), delay);
            }
        } else {
            var id = __nativeST__(vCallback, timeOut, argumentToPass1, argumentToPass2);
            this.timeoutIds[schedulerId] = id;
        }
    };
    this.scheduleInterval = function (vCallback, timeOut, delay, schedulerId, argumentToPass1, argumentToPass2) {

        if (delay === "undefined") {
            throw new Error(" You must argument 3");
        }
        if (typeof delay === "string") {
            schedulerId = delay;
            delay = "undefined";
        }
        if (delay != "undefined") {
            if (delay == 0) {
                vCallback();
                var id2 = __nativeSI__(vCallback, timeOut, argumentToPass1, argumentToPass2);
                this.intervalIds[schedulerId] = id2;
            }
            if (delay > 0) {
                __nativeST__(function () {
                    var id2 = __nativeST__(vCallback, timeOut, argumentToPass1, argumentToPass2);
                    this.intervalIds[schedulerId] = id2;
                }.bind(this), delay);
            }
        }
        else {
            var id2 = __nativeSI__(vCallback, timeOut, argumentToPass1, argumentToPass2);
            this.intervalIds[schedulerId] = id2;
        }
    };
    this.unScheduleTimeOut = function (schedulerId) {
        if (this.timeoutIds[schedulerId]) {
            window.clearTimeout(this.timeoutIds[schedulerId]);
            delete this.timeoutIds[schedulerId];
        }
    };
    this.unScheduleInterval = function (schedulerId) {
        if (this.intervalIds[schedulerId]) {
            window.clearInterval(this.intervalIds[schedulerId]);
            delete this.intervalIds[schedulerId];
        }
    };
    this.stopAllTimers = function () {
        for (var key in this.timeoutIds) {
            window.clearTimeout(this.timeoutIds[key]);
        }
        for (var key2 in this.intervalIds) {
            window.clearInterval(this.intervalIds[key2]);
        }
    }
};