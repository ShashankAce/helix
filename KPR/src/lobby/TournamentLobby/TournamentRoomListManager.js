/**
 * Created by stpl on 9/16/2016.
 */
"use strict";
var TournamentRoomListManager = cc.Class.extend({

    _name: "TournamentRoomListManager",
    _isEnabled: null,
    _scrolledPercent: 0,
    tournamentLobby: null,
    _selectedItem: null,

    selectedMyTourRoomId: null,
    selectedAllTourRoomId: null,

    _myTournamentsIds: null,
    myTourRoomBar: null,
    allTourRoomBar: null,

    _prizeStructure: null,

    widthArray: null,
    myTourListArr: null,
    allTourListArr: null,

    newTagGrpTypeArr: null,
    waitNum: null,

    ctor: function (tournamentLobby) {
        this.tournamentLobby = tournamentLobby;
        this._isEnabled = true;
        this._myTournamentsIds = [];
        this.myTourRoomBar = [];
        this.allTourRoomBar = [];
        this._prizeStructure = null;
        this.widthArray = [];
        this.myTourListArr = [];
        this.allTourListArr = [];
        this.newTagGrpTypeArr = [];
        this.waitNum = null;

        this.listView = new ccui.ListView();
        this.listView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        this.listView.setGravity(ccui.ListView.GRAVITY_CENTER_HORIZONTAL);
        this.listView.setScrollBarAutoHideEnabled(false);
        this.listView.setTouchEnabled(true);
        this.listView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.listView.setBackGroundColor(cc.color(255, 255, 255));
        this.listView.setContentSize(cc.size(760 * scaleFactor, 300 * scaleFactor));
        this.listView.setPosition(cc.p(0, 0));

        this.listView.addEventListener(this.tourListSelectedItemEvent.bind(this));
        this.initRoomListScrollListener();

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

    },
    pauseListener: function (bool) {
        this._isEnabled = !bool;
    },
    reqAlreadyJoinedRoom: function () {
        //request for already joined rooms
    },
    tourListSelectedItemEvent: function (sender, type) {
        switch (type) {
            case ccui.ListView.ON_SELECTED_ITEM_END :
                var listViewEx = sender;
                var item = listViewEx.getItem(listViewEx.getCurSelectedIndex());
                this.updateTourListViewItem(item);
                applicationFacade._controlAndDisplay.closeAllTab();
                applicationFacade._lobby.closeAllTab();
                break;
            default:
                break;
        }
    },
    updateTourListViewItem: function (item) {
        if (item) {
            if (this._selectedItem && this._selectedItem != item) {
                this._selectedItem.setSelected(!1);
            }
            if (this._selectedItem != item) {
                this._selectedItem = item;
                item.setSelected(!0);
                // this.tourPrizeReq(this._selectedItem);
                this.tournamentLobby.tourMiniRoom.setMiniRoom(this._selectedItem);

                console.log(this._selectedItem.room);

                this.tournamentLobby.tourMiniRoom.isVisible() && this.tournamentLobby.showMiniRoom();
            }
        }
    },
    getEntryFeeType: function (tournamentList) {

        var entryFee = tournamentList.getVariable("entryFee").value;
        var feeType, feeArray = [];

        // "FREE","Chip,10","Ticket,1:GR Cash Card","Free","Chip|Ticket,100|1:GR Cash Card"

        // Chip|Ticket,10|1:KPR Cash Card 50"
        if (entryFee.indexOf(TOURNAMENT_CONSTANTS.ENTRYFEE_CHIP) >= 0) {
            entryFee = entryFee.split(",");
            if (entryFee[1].indexOf("|") >= 0) {
                entryFee = entryFee[1].split("|")[0];
                feeType = "ChipAndTicket";
            } else {
                entryFee = entryFee[1];
                feeType = "Chip";
            }
        } else if (entryFee.indexOf(TOURNAMENT_CONSTANTS.ENTRYFEE_TICKET) >= 0 && !entryFee.indexOf(TOURNAMENT_CONSTANTS.ENTRYFEE_CHIP) >= 0) {
            entryFee = TOURNAMENT_CONSTANTS.ENTRYFEE_TICKET;
            feeType = "Ticket";
        } else {
            feeType = "Free";
        }
        feeArray.push(entryFee, feeType);
        return feeArray;
    },

    getPrizeType: function (tournamentList) {
        if (!tournamentList["actualPrize"])
            tournamentList["actualPrize"] = tournamentList.getVariable("totalPrize").value;
        // var prize = tournamentList.getVariable("totalPrize").value;
        var prize = tournamentList["actualPrize"];
        // "C:301""C:160""T:1 GR Cash Card""C:100""C:18""C:400""C|T:1000|1 GR Cash Card"
        if (prize.indexOf(TOURNAMENT_CONSTANTS.PRIZE_CHIP) >= 0) {
            prize = prize.split(":");
            if (prize[1].indexOf("|") >= 0) {
                prize = prize[1].split("|")[0] + " + " + prize[1].split("|")[1];
            } else {
                prize = prize[1];
            }
        } else if (prize.indexOf(TOURNAMENT_CONSTANTS.PRIZE_TICKET) >= 0 && !prize.indexOf(TOURNAMENT_CONSTANTS.PRIZE_CHIP) >= 0) {
            prize = prize.split(":")[1];
        }
        return prize;
    },

    insertRoomList: function (widthArray, myTourListArr, allTourListArr) {
        if (!this.tournamentLobby._isEnabled)
            return;

        this.listView.removeAllChildrenWithCleanup(true);

        this.tournamentLobby.spriteHover && this.tournamentLobby.spriteHover.setVisible(false);
        this.tournamentLobby.spriteHover2 && this.tournamentLobby.spriteHover2.setVisible(false);

        this.myTourRoomBar = [];
        this.allTourRoomBar = [];
        this.widthArray = widthArray;
        this.myTourListArr = myTourListArr;

        var myTourArrLength = this.tournamentLobby.myTourFilteredList.length;
        this.myTournamentList = new MyTournamentRoomList(widthArray, allTourListArr, myTourArrLength, this);
        this.listView.pushBackCustomItem(this.myTournamentList);
        var roomBar;
        for (var index = 0; index < myTourListArr.length; index++) {
            roomBar = new AllTournamentRoomListStrip(widthArray, myTourListArr[index], index, this, 1);
            this.myTourRoomBar.push(roomBar);
            this.listView.pushBackCustomItem(roomBar);
        }

        // cc.log("listview: " + this.listView.getChildren());

        this.allTournamentList = new AllTournamentRoomList(widthArray, allTourListArr, this);
        this.listView.pushBackCustomItem(this.allTournamentList);

        for (var index2 = 0; index2 < allTourListArr.length; index2++) {
            roomBar = new AllTournamentRoomListStrip(widthArray, allTourListArr[index2], index2, this);
            this.allTourRoomBar.push(roomBar);
            this.listView.pushBackCustomItem(roomBar);
        }

        var length = 1 + this.myTourRoomBar.length + 1;

        var item = this.listView.getItem(length);
        this.updateTourListViewItem(item);
        this.tournamentLobby.showMiniRoom();
        this.listView.setContentSize(cc.size(760 * scaleFactor, 300 * scaleFactor));
    },
    initRoomListScrollListener: function () {
        if ('mouse' in cc.sys.capabilities) {
            this.listViewMouseListener = cc.EventListener.create({
                event: cc.EventListener.MOUSE,
                swallowTouches: true,
                onMouseScroll: function (event) {
                    var target = event.getCurrentTarget();
                    var locationInNode = target.convertToNodeSpace(event.getLocation());
                    var s = target.getContentSize();
                    var rect = cc.rect(0, 0, s.width, s.height);

                    if (cc.rectContainsPoint(rect, locationInNode)) {
                        this.tournamentLobby.spriteHover && this.tournamentLobby.spriteHover.removeFromParent(true);
                        this.tournamentLobby.spriteHover2 && this.tournamentLobby.spriteHover2.setVisible(false);

                        var delta = cc.sys.isNative ? event.getScrollY() * 6 : -event.getScrollY();

                        var minY = this.listView._contentSize.height - this.listView._innerContainer.getContentSize().height;
                        var h = -minY;
                        var locContainer = this.listView._innerContainer.getBottomBoundary();
                        var percentY = ((h + locContainer) / h) * 100;
                        var scrollSpeed = 15; // higher value is inversely proportional to speed

                        this._scrolledPercent = percentY;

                        if ((this._scrolledPercent + delta / scrollSpeed) < 0) {
                            this._scrolledPercent = 0;
                        } else if ((this._scrolledPercent + delta / scrollSpeed) > 100) {
                            this._scrolledPercent = 100;
                        } else {
                            this._scrolledPercent += delta / scrollSpeed;
                        }
                        if (this._scrolledPercent <= 100 && this._scrolledPercent >= 0) {
                            this.listView.scrollToPercentVertical(this._scrolledPercent, 0.2, true);
                        }
                        return true;
                    }
                    return false;
                }.bind(this)
            });
            this.listViewMouseListener.retain();
            cc.eventManager.addListener(this.listViewMouseListener, this.listView);
        }
    },
    filterSubGameTypeByCheckBoxSelected: function (checkboxType) {
        var minDivideValue = this.tournamentLobby.minTimeText.getString();
        var maxDivideValue = this.tournamentLobby.maxTimeText.getString();

        var allTourArr = this.getFinalFilteredListByCheckbox(this.tournamentLobby.tournamentFilteredList);
        allTourArr = this.tournamentLobby.getSortedByEntryFee(allTourArr, minDivideValue, maxDivideValue);

        this.tournamentLobby.defaultSort(this.myTourFilteredList);
        this.tournamentLobby.defaultSort(allTourArr);

        this.insertRoomList(this.tournamentLobby.widthArray, this.tournamentLobby.myTourFilteredList, allTourArr);
    },
    getFinalFilteredListByCheckbox: function (listArray) {
        var filterArray = [];
        for (var i = 0; i < listArray.length; i++) {
            if (this.tournamentLobby.selectedcheckboxGrpTypeArr.length == 0) {
                filterArray = listArray;
            } else {
                if (this.tournamentLobby.selectedcheckboxGrpTypeArr.length != 0) {
                    if (this.tournamentLobby.selectedcheckboxGrpTypeArr.indexOf(listArray[i].getVariable("tournamentGroupType").value) >= 0) {
                        filterArray.push(listArray[i]);
                    }
                }
            }

        }
        return filterArray;
    }, checkForMaxTables: function (room) {
        //if(Main.getInstance()._gameRoomsMap.length==Main.MaxRooms)
        //    _trmtDymcTab._tmtPopUp.getMaxTablePopup(room);
    },
    updateRoomVar: function (rId, rmVar, val) {
        var room = sfsClient.getRoomById(rId);
        cc.log("updateRoomVar", rId, rmVar, val, room);
        if (room){
            room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(rmVar, val));
            room.priority = this.tournamentLobby.setPriorityValue(val);
        }

    },
    getRunningTourCount: function () {
        var count = 0;
        for (var i = 0; i < this.myTourRoomBar.length; i++) {
            if ((this.myTourRoomBar[i].room.getVariable("status").value).toLowerCase() == "running")
                count++;
        }
        return count;
    },
    getTrmntBarByRoomId: function (tRmId) {
        var roomBar;
        if (this.myTourRoomBar) {
            for (var k = 0; k < this.myTourRoomBar.length; k++) {
                if (this.myTourRoomBar[k].roomId == tRmId)
                    return this.myTourRoomBar[k];
            }
        }
        if (this.allTourRoomBar) {
            var elementPos = this.allTourRoomBar.map(function (x) {
                return x.roomId;
            }).indexOf(tRmId);
            if (elementPos != -1)
                return this.allTourRoomBar[elementPos];
        }
    },
    getTournamentRoom: function (tRoomId) {
        if (this.tournamentLobby.myTourFilteredList != null)
            for (var i = 0; i < this.tournamentLobby.myTourFilteredList.length; i++) {
                if (this.tournamentLobby.myTourFilteredList[i].id == tRoomId)
                    return this.tournamentLobby.myTourFilteredList[i];
            }

        /*for (var j = 0; j < this.tournamentLobby.tournamentFilteredList.length; j++) {
            if (this.tournamentLobby.tournamentFilteredList[j].id == tRoomId)
                return this.tournamentLobby.tournamentFilteredList[j];
        }*/

        var tRoom = this.tournamentLobby.tournamentFilteredList.filter(function (room) {
            return room.id == tRoomId;
        });
        return tRoom[0];

    },
    removeRoomFromList: function (tRoomId) {
        if (this.tournamentLobby.myTourFilteredList != null)
            for (var i = 0; i < this.tournamentLobby.myTourFilteredList.length; i++) {
                if (this.tournamentLobby.myTourFilteredList[i].id == tRoomId) {
                    this.tournamentLobby.myTourFilteredList.splice(i, 1);
                    return;
                }
            }
        for (var j = 0; j < this.tournamentLobby.tournamentFilteredList.length; j++) {
            if (this.tournamentLobby.tournamentFilteredList[j].id == tRoomId) {
                this.tournamentLobby.tournamentFilteredList.splice(j, 1);
                return;
            }
        }
    },
    onRoomDeleted: function (room) {

        if (this.tournamentLobby.tournamentDetail && this.tournamentLobby.tournamentDetail.isVisible() &&
            this.tournamentLobby.tournamentDetail.trnmntRoom.id == room.getVariable("tournamentId").value) {
            this.tournamentLobby.tournamentDetail.removeFromParent(true);
        }
        var indx = sfsClient._tournamentRoomArr.lastIndexOf(room);
        if (indx != -1)
            sfsClient._tournamentRoomArr.splice(indx, 1);
        if (!this.tournamentLobby.tournamentFilteredList && !this.tournamentLobby.myTourFilteredList)
            return;

        indx = this.tournamentLobby.tournamentFilteredList.lastIndexOf(room);
        if (indx != -1)
            this.tournamentLobby.tournamentFilteredList.splice(indx, 1);
        else {
            indx = this.tournamentLobby.myTourFilteredList.lastIndexOf(room);
            if (indx != -1)
                this.tournamentLobby.myTourFilteredList.splice(indx, 1);
        }
       /* if (indx != -1)
            this.removeAllPopup();*/

        //trace("onRoomDeleted before",  _trnmntListArr.length , _myTournamentArr.length);
        /* if ((tmntDetail && tmntDetail.trnmntRoom.id == room.id) && _trmtDymcTab._lobby.contains(tmntDetail))
         {
         _trmtDymcTab._lobby.removeChild(tmntDetail);
         tmntDetail = null;
         }*/

        var roomBar = this.getTrmntBarByRoomId(room.id);
        if (!roomBar)
            return;

        var myIndex = this.myTourRoomBar.indexOf(roomBar);
        if (myIndex >= 0) {
            this.myTourRoomBar.splice(myIndex, 1);
            this.listView.removeItem(1 + myIndex);
            roomBar.removeFromParent(true);
        } else {
            var index = this.allTourRoomBar.indexOf(roomBar);
            if (index >= 0) {
                this.allTourRoomBar.splice(index, 1);
                var removeAtIndex = 1 + this.myTourRoomBar.length + index;
                this.listView.removeItem(removeAtIndex);
                roomBar.removeFromParent(true);
            }
        }
        this.myTournamentList.myTournamentHeaderHandler();
        //trace("onRoomDeleted", i, _trnmntListArr.length , _myTournamentArr.length);

        // this.tournamentLobby.myTourFilteredList = this.tournamentLobby.defaultSort(this.tournamentLobby.myTourFilteredList);
        // this.tournamentLobby.tournamentFilteredList = this.tournamentLobby.defaultSort(this.tournamentLobby.tournamentFilteredList);
    },
    updateTournamentList: function (tRoomId, status) {
        for (var j = 0; j < sfsClient._tournamentRoomArr.length; j++) {
            if (sfsClient._tournamentRoomArr[j].id == tRoomId) {
                sfsClient._tournamentRoomArr[j].priority = this.tournamentLobby.setPriorityValue(status);
                break;
            }
        }
    },
    showCancelTourPopup: function (message, tourName) {
        var size = new cc.Size(570 * scaleFactor, 170 * scaleFactor);
        this.removeAllPopup();
        this.tourCommonPopup = new TourCommonPopup(message, tourName, size, this, true);
        this.tournamentLobby._lobby.addChild(this.tourCommonPopup, GameConstants.popupZorder, "tourCommonPopup");
    },
    processTournamentRoomStatus: function (dataObj) {
        var room = this.getTournamentRoom(dataObj["tId"]);
        this.updateRoomVar(dataObj["tId"], TOURNAMENT_CONSTANTS.STATUS, dataObj["status"]);
      //  this.updateTournamentList(dataObj["tId"], dataObj["status"]);
        if (!room)
            return;
        room.priority = this.tournamentLobby.setPriorityValue(dataObj["status"]);
        var roomBar = this.getTrmntBarByRoomId(dataObj["tId"]);
        if (!roomBar)
            return;

        if (dataObj["status"].toUpperCase() == TOURNAMENT_CONSTANTS.FREEZE) {
            this.joinConfirmationPopup && this.joinConfirmationPopup.removeFromParent(true);
            this.quitConfirmationPopup && this.quitConfirmationPopup.removeFromParent(true);

            if (this._myTournamentsIds.indexOf(dataObj["tId"] + "") != -1)
                AllTournamentRoomListStrip.ALREADY_JOIN_ROOM.push(dataObj["tId"] + "");
            this.checkForMaxTables(roomBar._room);
        }
        roomBar.updateJoinBtn();
        if (this.tournamentLobby.tournamentDetail && this.tournamentLobby.tournamentDetail.trnmntRoom.id == roomBar.roomId) {
            this.tournamentLobby.tournamentDetail.updateJoinBtn();
        }
        // this.tournamentLobby.myTourFilteredList = this.tournamentLobby.defaultSort(this.tournamentLobby.myTourFilteredList);
        // this.tournamentLobby.tournamentFilteredList = this.tournamentLobby.defaultSort(this.tournamentLobby.tournamentFilteredList);
        if (dataObj["status"].toUpperCase() == TOURNAMENT_CONSTANTS.CLOSED) {
            var newRoom = sfsClient.getRoomById(dataObj["tId"]);
            this.onRoomDeleted(newRoom);
            return;
        }

        if (this.tournamentLobby.tournamentDetail && this.tournamentLobby.tournamentDetail.trnmntRoom.id == roomBar.roomId) {
            if (dataObj["status"].toUpperCase() == TOURNAMENT_CONSTANTS.CANCELLED) {
                this.handleTournamentCancel(room, dataObj);
                this.tournamentLobby.tournamentDetail.removeFromParent(true);
                this.tournamentLobby.tournamentDetail = null;
            }
            else this.tournamentLobby.tournamentDetail.setTournamentStatus(roomBar);
        }

        var minDivideValue = this.tournamentLobby.minTimeText.getString();
        var maxDivideValue = this.tournamentLobby.maxTimeText.getString();

        var allTourArr = this.getFinalFilteredListByCheckbox(this.tournamentLobby.tournamentFilteredList);
        allTourArr = this.tournamentLobby.getSortedByEntryFee(allTourArr, minDivideValue, maxDivideValue);

        this.tournamentLobby.defaultSort(this.myTourFilteredList);
        this.tournamentLobby.defaultSort(allTourArr);

        this.insertRoomList(this.widthArray, this.tournamentLobby.myTourFilteredList, allTourArr);
    },
    processHelpText: function (dataObj) {
        //i stands for Ineligibility Info
        //e stands for entry Fee Info
        //p stands for Prize Info

//        str = "2:i-you need to own a Ferrari,
//               3:e-you need 10 rupees to enter,
//               1:i-You should have verified your email and mobile and be a non depositor to be eligible for this tournament,
//               11:e-you need 10 rupees to enter;p-400%;i-You should have verified your email and mobile and be a non depositor to be eligible for this tournament,
//               12:e-Entry Fee;p-;i-,
//               13:e-gdfgsd;p-dfgds;i-,
//               14:e-;p-;i-,
//               15:e-Entry Fee Help Text;p-Prize Help Text;i-Ineligible Help Text,
//               16:e-Entry Fee;p-Prize help;i-Ineligible Text";


        // "e-you need 10 rupees to enter"

        var helpTxt = (dataObj["str"]).split(",");

        for (var i = 0; i < helpTxt.length; i++) {
            for (var key = 0; key < sfsClient._tournamentRoomArr.length; key++) {
                var commonId = sfsClient._tournamentRoomArr[key].getVariable(TOURNAMENT_CONSTANTS.COMMON_ID).value;
                var tmpCommonId = parseInt(helpTxt[i].split(":")[0]);
                var helpText = helpTxt[i].split(":")[1];
                if (tmpCommonId == commonId) {
                    var helpTypeText = [];
                    if (helpText.indexOf(";") >= 0) {
                        helpTypeText = helpText.split(";");
                    } else {
                        helpTypeText.push(helpText);
                    }
                    for (var j = 0; j < helpTypeText.length; j++) {
                        var str = helpTypeText[j].split("-");
                        if (str[0] == TOURNAMENT_CONSTANTS.HELPTEXT_ENTRYFEE) {
                            sfsClient._tournamentRoomArr[key]["EntryFeeText"] = str[1];
                        } else if (str[0] == TOURNAMENT_CONSTANTS.HELPTEXT_PRIZE) {
                            sfsClient._tournamentRoomArr[key]["PrizeText"] = str[1];
                        } else if (str[0] == TOURNAMENT_CONSTANTS.HELPTEXT_INELIGIBLE) {
                            sfsClient._tournamentRoomArr[key]["IneligibleText"] = str[1];
                        }
                    }
                }
            }
        }
    },
    processMyTournament: function (dataObj) {
        if (dataObj.ids != -1)
            this._myTournamentsIds = dataObj.ids.split(",");

    },
    handleTournamentQuitResp: function (room, dataObj) {
        var tId;
        if (!dataObj) {
            tId = room.id;
        } else {
            tId = dataObj["tId"];
        }
        var roomBar = this.getTrmntBarByRoomId(tId);
        if (!roomBar)
            return;

        var indexId = this._myTournamentsIds.indexOf((roomBar.roomId).toString());
        if (indexId >= 0)
            this._myTournamentsIds.splice(indexId, 1);

        var newRoom = this.getTournamentRoom(tId);
        this.removeRoomFromList(tId);

        var status = room.getVariable(TOURNAMENT_CONSTANTS.STATUS).value;
        room.priority = this.tournamentLobby.setPriorityValue(status);

        this.tournamentLobby.tournamentFilteredList.push(newRoom);

        var index = this.listView.getIndex(roomBar);
        this.listView.removeItem(index);
        roomBar.removeFromParent(true);

        var minDivideValue = this.tournamentLobby.minTimeText.getString();
        var maxDivideValue = this.tournamentLobby.maxTimeText.getString();

        var allTourArr = this.getFinalFilteredListByCheckbox(this.tournamentLobby.tournamentFilteredList);
        allTourArr = this.tournamentLobby.getSortedByEntryFee(allTourArr, minDivideValue, maxDivideValue);

        this.tournamentLobby.defaultSort(this.myTourFilteredList);
        this.tournamentLobby.defaultSort(allTourArr);

        this.insertRoomList(this.widthArray, this.tournamentLobby.myTourFilteredList, allTourArr);
        roomBar._myTournament = 0;
        roomBar.updateJoinBtn();
    },
    handleTournamentJoinResp: function (room, dataObj, waitNum) {
        var roomBar = this.getTrmntBarByRoomId(dataObj["tId"]);
        if (!roomBar)
            return;
        this._myTournamentsIds.push((roomBar.roomId).toString());
        var status = room.getVariable(TOURNAMENT_CONSTANTS.STATUS).value;
        room.priority = this.tournamentLobby.setPriorityValue(status);

        var newRoom = this.getTournamentRoom(dataObj["tId"]);
        this.removeRoomFromList(dataObj["tId"]);
        this.tournamentLobby.myTourFilteredList.push(newRoom);

        var index = this.listView.getIndex(roomBar);
        this.listView.removeItem(index);
        roomBar.removeFromParent(true);

        var minDivideValue = this.tournamentLobby.minTimeText.getString();
        var maxDivideValue = this.tournamentLobby.maxTimeText.getString();

        var allTourArr = this.getFinalFilteredListByCheckbox(this.tournamentLobby.tournamentFilteredList);
        allTourArr = this.tournamentLobby.getSortedByEntryFee(allTourArr, minDivideValue, maxDivideValue);

        this.tournamentLobby.defaultSort(this.myTourFilteredList);
        this.tournamentLobby.defaultSort(allTourArr);

        this.insertRoomList(this.widthArray, this.tournamentLobby.myTourFilteredList, allTourArr);

        // if (waitNum <= 0)
        roomBar._myTournament = 1;
        roomBar.updateJoinBtn();

        if (this.tournamentLobby.tournamentDetail && this.tournamentLobby.tournamentDetail.trnmntRoom.id == roomBar.roomId) {
            this.tournamentLobby.tournamentDetail._myTournament = 1;
            this.tournamentLobby.tournamentDetail.updateJoinBtn();
        }

        // this.updateMyTourList();
        // this.updateAllTourList();

        /*var addAtIndex = this.myTourRoomBar.length; // (1 + this.myTourRoomBar.length - 1)
         this.listView.insertCustomItem(roomBar, addAtIndex);*/
        // todo ADD DATA IN myTourList
        /* var indexInListview = this.listView._items.indexOf(roomBar);
         if (indexInListview >= 0)
         this.listView._items.remove(indexInListview, 1);*/
        // roomBar.joinBtnClickedResp();
    },
    processInstructions: function (room, dataObj) {
        var roomBar = this.getTrmntBarByRoomId(room.id);
        if (this.tournamentLobby.tournamentDetail && this.tournamentLobby.tournamentDetail.trnmntRoom.id == roomBar.roomId)
            this.tournamentLobby.tournamentDetail.instructionsPopup(room, dataObj);
    },
    setJoinedPlayerInfo: function (room, dataObj) {
        room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.REG_PLS, dataObj["regPls"]));
        var roomBar = this.getTrmntBarByRoomId(room.id);
        if (this.tournamentLobby.tournamentDetail && this.tournamentLobby.tournamentDetail.trnmntRoom.id == roomBar.roomId)
            this.tournamentLobby.tournamentDetail.handleJoinedPlayers();
    },
    removeAllPopup: function () {
        this.joinConfirmationPopup && this.joinConfirmationPopup.removeFromParent(true);
        this.quitConfirmationPopup && this.quitConfirmationPopup.removeFromParent(true);
        this.tourCommonPopup && this.tourCommonPopup.removeFromParent(true);
    },
    getPlayerNonFlashPopUp: function (room) {
        this.removeAllPopup();
        var flag = true; // flag true for doing extra work on ok button in some tournaments.
        var text = "You can enter this Tournament only on our new Mobile App. Click OK to download/upgrade the App.";

        var tourName = room.getVariable("tournamentName").value;
        var size = new cc.Size(500 * scaleFactor, 170 * scaleFactor);

        this.tourCommonPopup = new TourCommonPopup(text, tourName, size, this, true, flag);
        this.tournamentLobby._lobby.addChild(this.tourCommonPopup, GameConstants.popupZorder, "tourCommonPopup");
    },
    processPlayerEligible: function (dataObj) {
        var room = sfsClient.getRoomById(dataObj["sfsId"]);
        if (dataObj["isEligible"] == true) {
            this.joinConfirmationPopup && this.joinConfirmationPopup.removeFromParent(true);
            this.joinConfirmationPopup = new JoinConfirmationPopup(room, this, true, "joinConfrmPopup");
            this.tournamentLobby._lobby.addChild(this.joinConfirmationPopup, GameConstants.popupZorder, "joinConfirmationPopup");
        }
        else this.getPlayerIneliblePopUp(room, dataObj);
    },
    processQuitPopup: function (roomId) {
        var room = sfsClient.getRoomById(roomId);
        this.quitConfirmationPopup && this.quitConfirmationPopup.removeFromParent(true);
        this.quitConfirmationPopup = new QuitConfirmationPopup(room, this, true, "quitConfrmPopup");
        this.tournamentLobby._lobby.addChild(this.quitConfirmationPopup, GameConstants.popupZorder, "quitConfirmationPopup");
    },
    getPlayerIneliblePopUp: function (room, dataObj) {
        var msg;
        if (dataObj["eligibleText"]) {
            msg = dataObj["eligibleText"];
        } else {
            msg = room["IneligibleText"];
        }
        var tourName = room.getVariable("tournamentName").value;
        tourName = "Tournament Eligibility";
        var size = new cc.Size(500 * scaleFactor, 170 * scaleFactor);
        this.tourCommonPopup && this.tourCommonPopup.removeFromParent(true);

        var isAddCash;
        if (dataObj["showACBtn"])
            isAddCash = true;
        else
            isAddCash = false;

        this.tourCommonPopup = new TourCommonPopup(msg, tourName, size, this, true, false, isAddCash);
        this.tournamentLobby._lobby.addChild(this.tourCommonPopup, GameConstants.popupZorder, "tourCommonPopup");
    },
    processRegNumResp: function (dataObj, _room) {
        var status = _room.getVariable(TOURNAMENT_CONSTANTS.STATUS).value;
        var room = sfsClient.getRoomById(dataObj["tId"]);
        var roomBar = this.getTrmntBarByRoomId(dataObj["tId"]);
        if (!roomBar)
            return;
        var regNum = dataObj["regNum"];
        if (regNum == -1)
            regNum = _room.getVariable(TOURNAMENT_CONSTANTS.REG_NUM).value;
        var joinedStr = regNum + "/" + room.getVariable("maxRegNum").value;
        roomBar.joinedValue.setString(joinedStr);

        if (status.toUpperCase() != TOURNAMENT_CONSTANTS.REGISTERING)
            return;

        var _isMeInRoom = roomBar.isMeInTournament();
        if (_isMeInRoom) {
            roomBar.statusSym.statusBtn.setSprite("QuitBar", "Quit_over");
            roomBar.statusSym.statusBtn.setName("Quit");
            return;
        }
        if (regNum >= room.getVariable("maxRegNum").value) {
            roomBar.statusSym.statusBtn.setSprite("wait", "waitHover");
            roomBar.statusSym.statusBtn.setName("Waiting");
        } else {
            roomBar.statusSym.statusBtn.setSprite("joinButton", "joinButtonOver");
            roomBar.statusSym.statusBtn.setName("Join");
        }
    },
    processWaitNumResp: function (dataObj) {
        /*forRmId  528
         tId 570
         waitNum : 1*/
        this.waitNum = dataObj["waitNum"];
        cc.log("RegNum Resp");
        this.updateRoomVar(dataObj["tId"], TOURNAMENT_CONSTANTS.WAIT_NUM, dataObj["waitNum"]);
        var roomBar = this.getTrmntBarByRoomId(dataObj["tId"]);
        if (!roomBar)
            return;
        roomBar.updateWaitPls(TOURNAMENT_CONSTANTS.WAIT_NUM, dataObj["waitNum"]);
        /*if (tmntDetail && tmntDetail.detailBarStrip == roomBar)
         tmntDetail.updateWaitPls(TournamentConstant.WAIT_NUM,dataObj.getInt("waitNum"));*/
    },
    processTournamentPrizeStr: function (str) {

        cc.log("prize structure");
        if (sfsClient._tournamentRoomArr == null) {
            this._prizeStructure = str;
            return;
        }

        var strctArr = [];
        var commonRoomArr = [];
        var prizeStrArr = str.split("#");
        var prizeTxt = {};
        for (var i = 0; i < prizeStrArr.length; i++) {
            strctArr = (prizeStrArr[i]).split("=");
            commonRoomArr = (strctArr[1]).split("^");
            for (var j = 0; j < commonRoomArr.length; j++)
                prizeTxt[parseInt(commonRoomArr[j])] = (strctArr[0]).toString();
        }

        var r;
        var cId;

        for (var i = 0; i < sfsClient._tournamentRoomArr.length; i++) {
            r = sfsClient._tournamentRoomArr[i];
            if (r) {
                cId = r.id;
                this.setPrizeStructure(prizeTxt[cId], r);
            }
        }

        if (this.tournamentLobby.tournamentFilteredList != null) {
            for (var i = 0; i < this.tournamentLobby.tournamentFilteredList.length; i++) {
                r = this.tournamentLobby.tournamentFilteredList[i];
                if (r) {
                    cId = r.id;
                    this.setPrizeStructure(prizeTxt[cId], r);
                }
            }
        }

        for (var j = 0; j < this.tournamentLobby.myTourFilteredList.length; j++) {
            r = this.tournamentLobby.myTourFilteredList[j];
            if (r) {
                cId = r.id;
                this.setPrizeStructure(prizeTxt[cId], r);
            }
        }

    },
    setPrizeStructure: function (str, room) {
        if (!str)
            return;
        room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.PRIZE_STRUCT, str));
    },
    setDetailsPrizevars: function (room, dataObj) {
        if (!dataObj)
            return;
        if (dataObj["prizeStructure"] && dataObj["prizeStructure"] != "") {
            room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.PRIZE_STRUCT, dataObj["prizeStructure"]));
        } else {
            room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.PRIZE_STRUCT, "undefined"));
        }
    },
    processTournamentPrizeInfo: function (room, data) {

        var roomBar = this.getTrmntBarByRoomId(data["forRmId"]);
        this.setDetailsPrizevars(room, data);
        if (!data)
            return;
        if (data["winners"] && data["winners"] != "") {
            room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.WINNER_PLR_POS, data["winners"]));
        } else {
            room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.WINNER_PLR_POS, "undefined"));
        }
        this.tournamentLobby.tourMiniRoom.setMiniRoom(this._selectedItem);
        this.tournamentLobby.tourMiniRoom.isVisible() && this.tournamentLobby.showMiniRoom();
        if (this.tournamentLobby.tournamentDetail) {
            this.tournamentLobby.tournamentDetail.prize.updatePrizeStructure();
        }
        // this.tournamentLobby.showMiniRoom(roomBar);

    },
    updatePrizeStructure: function (room, dataObj) {
        this.setPrizeStructure(dataObj["prizeStr"], room);
        if (this._selectedItem && this._selectedItem.room.id == dataObj["roomId"]) {
            this.tournamentLobby.tourMiniRoom.setMiniRoom(this._selectedItem);
            this.tournamentLobby.tourMiniRoom.isVisible() && this.tournamentLobby.showMiniRoom();
        }
    },
    setDetailsStructureVars: function (room, dataObj) {
        if (!dataObj)
            return;
        room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL, dataObj["currLevel"]));
        if (this.tournamentLobby.tournamentDetail)
            this.tournamentLobby.tournamentDetail.handleProgress();
    },
    setDetailsProgressVars: function (room, dataObj) {
        if (!dataObj)
            return;
        room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.STRUCTURE, dataObj["str"]));
        room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL, dataObj["currLevel"]));
        if (this.tournamentLobby.tournamentDetail)
            this.tournamentLobby.tournamentDetail.handleStructure();
    },
    processTournamentWinnerInfo: function (room, dataObj) {
        if (!dataObj)
            return;
        if (dataObj["winners"] && dataObj["winners"] != "") {
            room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.WINNER_PLR_POS, dataObj["winners"]));
        } else {
            room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.WINNER_PLR_POS, {}));
        }
        var roomBar = this.getTrmntBarByRoomId(room.id);
        // TODO remove Tournament Detail Popup
        if (roomBar && this.tournamentLobby.tournamentDetail && this.tournamentLobby.tournamentDetail.trnmntRoom.id == roomBar.roomId)
            this.tournamentLobby.tournamentDetail.handleWinnerPlayers();
    },
    processNewTournament: function (roomIds) {
        var roomIdArr = roomIds.split(",");
        var groupName;
        for (var i = 0; i < roomIdArr.length; i++) {
            for (var j = 0; j < sfsClient._tournamentRoomArr.length; j++) {
                if (parseInt(roomIdArr[i]) == sfsClient._tournamentRoomArr[j].id) {
                    sfsClient._tournamentRoomArr[j]._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.NEW, true));
                    groupName = sfsClient._tournamentRoomArr[j].variables.tournamentGroupName.value.split("-");

                    if (this.newTagGrpTypeArr.indexOf(groupName[0]) < 0)
                        this.newTagGrpTypeArr.push(groupName[0]);
                }
            }
        }

        if (this.newTagGrpTypeArr.length != 0)
            this.newTagGrpTypeArr.push("ALL");
    },
    processJoinConfrmResp: function (room, dataObj) {
        var tourName = room.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value;
        var isSuccess = dataObj[TOURNAMENT_CONSTANTS.SUCCESS];
        var message;

        if (isSuccess) {
            var waitNum = dataObj[TOURNAMENT_CONSTANTS.NUM];

            this.joinConfirmationPopup && this.joinConfirmationPopup.removeFromParent(true);
            this.tourCommonPopup && this.tourCommonPopup.removeFromParent(true);
            if (waitNum > 0) {
                this.tourCommonPopup = new WaitConfirmationPopup(room, waitNum, this, true);
            } else {
                message = "Congratulation! Your seat is confirmed for the tournament. Click on the \n Take Seat button when the tournament starts.";
                var size = new cc.Size(570 * scaleFactor, 170 * scaleFactor);
                this.tourCommonPopup = new TourCommonPopup(message, tourName, size, this, true);
            }
            this.tournamentLobby._lobby.addChild(this.tourCommonPopup, GameConstants.popupZorder, "tourCommonPopup");
            this.handleTournamentJoinResp(room, dataObj, waitNum);
        } else {
            this.getJoinRespPopUp(dataObj, room);
        }
    },
    getJoinRespPopUp: function (dataObj, room) {
        var txt = null;
        var tourName = room.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value;
        var name;
        if (this.joinConfirmationPopup._selectedRadioBtn)
            name = this.joinConfirmationPopup._selectedRadioBtn._name;
        var entryFeeType;
        if (name == "RadioOne")
            entryFeeType = "Chip";
        else if (name == "RadioTwo")
            entryFeeType = "Ticket";
        // else entryFeeType = "Free";

        if (dataObj["msg"] == "RF") // Registration full
            txt = "Sorry, your seat could not be confirmed for this tournament\nbecause all registration and waitlist places have been filled.";
        if (dataObj["msg"] == "RC")	// Registration Closed
            txt = "Sorry, your seat could not be confirmed for this tournament\nbecause registration has been closed.";
        if (dataObj["msg"] == "NB")	// no bal
        {
            if (entryFeeType == "Ticket")
                this.getInsufficientTicketPopUp();
            else
                this.getAddCashPopUp();
        }
        if (txt) {
            var size = new cc.Size(570 * scaleFactor, 170 * scaleFactor);
            this.joinConfirmationPopup && this.joinConfirmationPopup.removeFromParent(true);
            this.joinConfirmationPopup = null;
            this.tourCommonPopup && this.tourCommonPopup.removeFromParent(true);
            this.tourCommonPopup = null;
            this.tourCommonPopup = new TourCommonPopup(txt, tourName, size, this, true);
            this.tournamentLobby._lobby.addChild(this.tourCommonPopup, GameConstants.popupZorder, "tourCommonPopup");
        }
    },
    getInsufficientTicketPopUp: function (str) {
        this.joinConfirmationPopup && this.joinConfirmationPopup.removeFromParent(true);
        this.joinConfirmationPopup = null;
        this.tourCommonPopup && this.tourCommonPopup.removeFromParent(true);
        this.tourCommonPopup = null;
        this.tourCommonPopup = new InsufficientTicketPopUp(this, str);
        this.tournamentLobby._lobby.addChild(this.tourCommonPopup, GameConstants.popupZorder, "tourCommonPopup");
    },
    getAddCashPopUp: function (str) {
        this.joinConfirmationPopup && this.joinConfirmationPopup.removeFromParent(true);
        this.joinConfirmationPopup = null;
        this.tourCommonPopup && this.tourCommonPopup.removeFromParent(true);
        this.tourCommonPopup = null;
        this.tourCommonPopup = new AddCashReBuyPopUp(this);
        this.tournamentLobby._lobby.addChild(this.tourCommonPopup, GameConstants.popupZorder, "tourCommonPopup");
    },
    processQuitConfrmResp: function (room, dataObj) {
        ButtonHandler.quitConfirmationPopup && ButtonHandler.quitConfirmationPopup.removeFromParent(true);
        this.tourCommonPopup && this.tourCommonPopup.removeFromParent(true);
        this.handleTournamentQuitResp(room, dataObj);
    },
    processquitReq: function (roomId, radioType) {
        var name = radioType && radioType._name;
        var entryFeeType;
        if (name == "RadioOne")
            entryFeeType = "Chip";
        else if (name == "RadioTwo")
            entryFeeType = "Ticket";
        else entryFeeType = "Free";

        // TODO check by mycashBalance
        var dataObj = {};
        dataObj["feeType"] = entryFeeType;
        sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_QUIT_REQUEST, dataObj, roomId);
    },
    handleTmntDetail: function (room, dataObj) {
        this.setDetailVars(room, dataObj);
        var roombar = this.getTrmntBarByRoomId(room.id);
        if (!roombar)
            return;
        /* if (tmntDetail && _trmtDymcTab._lobby.contains(tmntDetail))
         {
         _trmtDymcTab._lobby.removeChild(tmntDetail);
         tmntDetail = null;
         }*/
    },
    getCancelTournamentMsg: function (room, status) {
        var message = "";
        if (status == 0) {
            if (room.getVariable(TOURNAMENT_CONSTANTS.ENTRY_FEE).value.toLowerCase() == "free")
                message = "Due to lack of minimum participants (players), this tournament has been called off.";
            else
                message = "Due to lack of minimum participants (players), this tournament has been called off. Players Buy-In would be refunded back to players account respectively.";
        } else if (status == 1) {
            if (room.getVariable(TOURNAMENT_CONSTANTS.ENTRY_FEE).value.toLowerCase() == "free")
                message = "Due to technical reason this tournament has been cancelled, inconvenience regretted.";
            else
                message = "Due to technical reason this tournament is cancelled, players entry fee will be refunded and credited to their respective account.";
        }
        return message;
    },
    handleTournamentCancel : function (room, dataObj) {
        var status = dataObj["status"].toString();
        var message = this.getCancelTournamentMsg(room, 0);
        var tourName = room.getVariable("tournamentName").value;
        room.priority = this.tournamentLobby.setPriorityValue(status);

        this.showCancelTourPopup(message, tourName);
    },
    handleTournamentCancelCmd: function (room, dataObj) {
        var status = dataObj["status"].toString();
        var message = this.getCancelTournamentMsg(room, status);
        var tourName = room.getVariable("tournamentName").value;
        room.priority = this.tournamentLobby.setPriorityValue("Cancelled");

        if (this._myTournamentsIds.indexOf(room.id.toString()) >= 0)
            this.showCancelTourPopup(message, tourName);

    },
    setDetailVars: function (room, dataObj) {
        if (!dataObj)
            return;

        //console.log("level before data set: " + this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value);

        cc.log("level2 :" + dataObj["currLevel"]);
        room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.STRUCTURE, dataObj["tournamentStr"]));
        room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL, dataObj["currLevel"]));
        room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.REG_END_TIME, dataObj["regEndTime"]));
        room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.REG_NUM, dataObj["totJoinedPlr"]));
        room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.ESTIMATED_TIME, dataObj["tournamentDuration"]));
        room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.TOTAL_PRIZE, dataObj["expectedPrize"]));

    },
    processTournamentData: function (dataObj) {
        var myTournamentObj = dataObj[TOURNAMENT_CONSTANTS.CMD_MYTOURNAMENT];
        this._myTournamentsIds = myTournamentObj["ids"].split(",");

        var alreadyJoinTournamentObj = dataObj[TOURNAMENT_CONSTANTS.ALREADY_TRNMNT_JOIN_STR];
        var rmStr = alreadyJoinTournamentObj["str"];
        if (rmStr != "-1" || rmStr != "undefined")
            AllTournamentRoomListStrip.ALREADY_JOIN_ROOM = rmStr.split(",");
        this.handleAlreadyJoined();

        /* for(var i = 0; i < applicationFacade._tourOpenRooms.length; i++){
         if(applicationFacade._tourOpenRooms.indexOf() >= 0)
         }*/

        var isTournamentNewObj = dataObj[TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_NEW];
        var roomIds = isTournamentNewObj[TOURNAMENT_CONSTANTS.ROOMIDS];
        this.processNewTournament(roomIds);

        var helpTextObj = dataObj[TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_HELPTEXT];
        this.processHelpText(helpTextObj);

        var tournamentPrizeStructureObj = dataObj[TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_PRIZESTRUCTURE];
        var str = tournamentPrizeStructureObj["str"];
        this.processTournamentPrizeStr(str);
        applicationFacade._lobby.initSubGameTypeTab(applicationFacade._lobby.selectedGameType);

    },
    processAlreadyJoinedTournament: function (dataObj) {
        var rmStr = dataObj["str"];
        if (rmStr != "-1" || rmStr != "undefined")
            AllTournamentRoomListStrip.ALREADY_JOIN_ROOM = rmStr.split(",");
        this.handleAlreadyJoined();
    },
    processNoEntry: function (room, dataObj) {
        this.tourCommonPopup && this.tourCommonPopup.removeFromParent(true);

        var message = "Sorry, your waitlisted seat could not be confirmed for this tournament.\nPlease note that the Buy-In (if any) will be credited back to your account.";
        var tourName = room.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value;
        var size = new cc.Size(500 * scaleFactor, 170 * scaleFactor);

        this.tourCommonPopup = new TourCommonPopup(message, tourName, size, this, true);
        this.tournamentLobby.addChild(this.tourCommonPopup, GameConstants.popupZorder);
    },
    handleAlreadyJoined: function () {
        var roomBar;
        for (var i = 0; i < this.myTourRoomBar.length; i++) {
            roomBar = this.myTourRoomBar[i];
            roomBar.updateJoinBtn();
            if (this.tournamentLobby.tourMiniRoom && this._selectedItem == roomBar)
                this.tournamentLobby.tourMiniRoom.setMiniRoom(roomBar);
            if (this.tournamentLobby.tournamentDetail && this.tournamentLobby.tournamentDetail.trnmntRoom.id == roomBar.roomId)
                this.tournamentLobby.tournamentDetail.updateJoinBtn();
        }
    },
    startTimeInUIFormat: function (room) {
        var string = "";
        var startTime = room.getVariable("startTime").value;

        var startDateAndTime = moment(startTime).format('DD MMM,hh:mm a'); // "16 Oct 2011, 9:47 PM"
        var currentTimeInSec = (new Date).getTime();
        var currentDateAndTime = moment(currentTimeInSec).format('DD MMM,hh:mm a');

        var startTimeArr = startDateAndTime.split(",");
        var currentTimeArr = currentDateAndTime.split(",");

        if (startTimeArr[0] == currentTimeArr[0]) {
            var day = "Today";
            string += startTimeArr[1] + " " + day;
        } else {
            string = startTimeArr[0] + " " + startTimeArr[1];
        }
        room.StartTimeInUIFormat = string;
    }
});

TournamentRoomListManager.prototype.initTouchListener = function (element) {
    this.trnmtRoomListManagerTouchListener = cc.EventListener.create({
        event: cc.EventListener.TOUCH_ONE_BY_ONE,
        swallowTouches: true,
        onTouchBegan: function (touch, event) {
            var target = event.getCurrentTarget();
            var locationInNode = target.convertToNodeSpace(touch.getLocation());
            var targetSize = target.getContentSize();
            var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);

            var targetRectToWorld = target.getBoundingBoxToWorld();
            var insideListViewBool = cc.rectContainsRect(this.listView.getBoundingBoxToWorld(), targetRectToWorld);

            if (cc.rectContainsPoint(targetRect, locationInNode) && this._isEnabled && insideListViewBool) {
                return true;
            }
            return false;
        }.bind(this),
        onTouchEnded: function (touch, event) {
            var target = event.getCurrentTarget();
            var parent = target.grandParent;
            applicationFacade._controlAndDisplay.closeAllTab();
            applicationFacade._lobby.closeAllTab();
            ButtonHandler.handlerFun(target, parent);
        }.bind(this)
    });
    this.trnmtRoomListManagerTouchListener.retain();
};

TournamentRoomListManager.prototype.initMouseListener = function () {
    this.trnmtRoomListManagerMouseListener = cc.EventListener.create({
        event: cc.EventListener.MOUSE,
        onMouseMove: function (event) {
            if (this._isEnabled) {
                var target = event.getCurrentTarget();
                if (!target.isVisible()) {
                    return;
                }
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);

                var targetRect = target.getBoundingBoxToWorld();
                var insideListViewBool = cc.rectContainsRect(this.listView.getBoundingBoxToWorld(), targetRect);

                this.tmpBtnHolder = null;
                var parent, size;
                if (target._symbolName && target._symbolName == "Symbol_Btn")
                    parent = target.grandParent;
                else if (target._name == "QuesMark")
                    parent = target.parent;

                if (cc.rectContainsPoint(rect, locationInNode) && insideListViewBool) {
                    var joinHelpText;
                    if (!target._isEnabled) {
                        if (target._symbolName && target._symbolName == "Symbol_Btn" && !target._hovering) {
                            if (target._name == "Join")
                                joinHelpText = "Registration for this tournament will start at " + target._regStartTime;
                            else if (target._name == "TakeSeat")
                                joinHelpText = "This button will automatically enabled when tournament starts.";
                            target._hovering = true;
                            parent.tmpBtnHolder = target;
                            size = cc.size(250 * scaleFactor, 45 * scaleFactor);

                            parent.showToolTipJoin(true, joinHelpText, target, size);
                        } else if (target._name == "QuesMark" && !target._hovering) {
                            target._hovering = true;
                            parent.tmpBtnHolder = target;
                            size = cc.size(180 * scaleFactor, 30 * scaleFactor);
                            joinHelpText = target.toolTipMsg;
                            parent.showToolTip(true, joinHelpText, target, size);
                        }
                    } else if (target._symbolName && target._symbolName == "Symbol_Btn" && !target._hovering) {
                        parent.tmpBtnHolder = target;
                        size = cc.size(250 * scaleFactor, 45 * scaleFactor);
                        !target._hovering && target.hover(true);
                        target._hovering = true;
                        if (target._name == "Waiting") {
                            joinHelpText = "There are " + parent.room.getVariable(TOURNAMENT_CONSTANTS.WAIT_NUM).value + " player(s) on waitlist for this tournament.";
                            parent.showToolTipJoin(true, joinHelpText, target, size);
                        }
                    } else {
                        !target._hovering && target.hover(true);
                        target._hovering = true;
                        !target._isEnabled && parent.showToolTip(false);
                    }
                    return true;
                }
                if (parent && parent.tmpBtnHolder && parent.tmpBtnHolder == target) {
                    if (target._name == "Waiting") {
                        parent.showToolTipJoin(false);
                    }
                    !target._isEnabled && parent.showToolTip(false);
                    !target._isEnabled && parent.showToolTipJoin(false);
                    parent.tmpBtnHolder = null;
                }
                target._hovering && target.hover(false);
                target._hovering = false;
                return false;
            }
        }.bind(this)
    });
    this.trnmtRoomListManagerMouseListener.retain();
};