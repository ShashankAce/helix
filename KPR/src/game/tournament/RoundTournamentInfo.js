/**
 * Created by stpl on  2/1/2017.
 */
var RoundTournamentInfo = TournamentInfo.extend({
    ctor: function (trnmntGameRoom, dataObj) {
        this._super(trnmntGameRoom, dataObj);

        this.setContentSize(cc.size(440 * scaleFactor, 225 * scaleFactor));
        this.setPosition(cc.p(this.width / 2 - 2 * scaleFactor, cc.winSize.height - this.height / 2 - 36 * scaleFactor));

        this.hideCross = new cc.Sprite(spriteFrameCache.getSpriteFrame("closeBlack.png"));
        this.hideCross.setName("Hide");
        this.hideCross.setPosition(this.width, this.height);
        this.addChild(this.hideCross, 1);

        var whiteBackground = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png"), cc.rect(0, 0, 50 * scaleFactor, 50 * scaleFactor));
        whiteBackground.setCapInsets(cc.rect(16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor));
        whiteBackground.setAnchorPoint(0, 0);
        whiteBackground.setContentSize(385 * scaleFactor, 212 * scaleFactor);
        whiteBackground.setPosition(48 * scaleFactor, 6 * scaleFactor);
        this.addChild(whiteBackground);

        var bottomLine = new cc.DrawNode();
        bottomLine.drawSegment(cc.p(49 * scaleFactor, 188 * scaleFactor), cc.p(432 * scaleFactor, 188 * scaleFactor), 0.5, cc.color(215, 215, 215));
        this.addChild(bottomLine);

        var entryfeeHeader = new cc.LabelTTF("Buy-In:", "RobotoBold", 12 * 2 * scaleFactor);
        entryfeeHeader.setScale(0.5);
        entryfeeHeader.setColor(cc.color(0, 0, 0));
        entryfeeHeader.setAnchorPoint(0, 0.5);
        entryfeeHeader.setPosition(16 * scaleFactor, whiteBackground.height - 15 * scaleFactor);
        whiteBackground.addChild(entryfeeHeader);

        this.entryFeeValue = new cc.LabelTTF("Value", "RupeeFordian", 12 * 2 * scaleFactor);
        this.entryFeeValue.setScale(0.5);
        this.entryFeeValue.setColor(cc.color(0, 0, 0));
        this.entryFeeValue.setAnchorPoint(0, 0.5);
        this.entryFeeValue.setPosition(entryfeeHeader.x + entryfeeHeader.width - 35 * scaleFactor, whiteBackground.height - 15 * scaleFactor);
        whiteBackground.addChild(this.entryFeeValue);

        var pipe1 = new cc.LabelTTF("|", "RobotoBold", 17 * scaleFactor);
        pipe1.setColor(cc.color(0, 0, 0));
        pipe1.setAnchorPoint(0, 0.5);
        pipe1.setPosition(131 * scaleFactor, whiteBackground.height - 15 * scaleFactor);
        whiteBackground.addChild(pipe1);

        var roundHeader = new cc.LabelTTF("Round:", "RobotoBold", 12 * 2 * scaleFactor);
        roundHeader.setScale(0.5);
        roundHeader.setColor(cc.color(0, 0, 0));
        roundHeader.setAnchorPoint(0, 0.5);
        roundHeader.setPosition(140 * scaleFactor, whiteBackground.height - 15 * scaleFactor);
        whiteBackground.addChild(roundHeader);

        this.roundValue = new cc.LabelTTF("1/3:", "RobotoRegular", 12 * 2 * scaleFactor);
        this.roundValue.setScale(0.5);
        this.roundValue.setColor(cc.color(0, 0, 0));
        this.roundValue.setAnchorPoint(0, 0.5);
        this.roundValue.setPosition(roundHeader.x + roundHeader.width / 2 + 2 * scaleFactor, whiteBackground.height - 15 * scaleFactor);
        whiteBackground.addChild(this.roundValue);

        var pipe2 = new cc.LabelTTF("|", "RobotoBold", 17 * scaleFactor);
        pipe2.setColor(cc.color(0, 0, 0));
        pipe2.setAnchorPoint(0, 0.5);
        pipe2.setPosition(212 * scaleFactor, whiteBackground.height - 15 * scaleFactor);
        whiteBackground.addChild(pipe2);

        var dealHeader = new cc.LabelTTF("Deal:", "RobotoBold", 12 * 2 * scaleFactor);
        dealHeader.setScale(0.5);
        dealHeader.setColor(cc.color(0, 0, 0));
        dealHeader.setAnchorPoint(0, 0.5);
        dealHeader.setPosition(222 * scaleFactor, whiteBackground.height - 15 * scaleFactor);
        whiteBackground.addChild(dealHeader);

        this.dealValue = new cc.LabelTTF("gy5tr", "RobotoRegular", 12 * 2 * scaleFactor);
        this.dealValue.setScale(0.5);
        this.dealValue.setColor(cc.color(0, 0, 0));
        this.dealValue.setAnchorPoint(0, 0.5);
        this.dealValue.setPosition(dealHeader.x + dealHeader.width / 2 + 2 * scaleFactor, whiteBackground.height - 15 * scaleFactor);
        whiteBackground.addChild(this.dealValue);

        this.btnRefPt = new cc.Node();
        this.btnRefPt.setPosition(cc.p(48 * scaleFactor, 140 * scaleFactor));
        this.addChild(this.btnRefPt, 2);

        var tablePopup = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png"), cc.rect(0, 0, 50 * scaleFactor, 50 * scaleFactor));
        tablePopup.setCapInsets(cc.rect(16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor));
        tablePopup.setCascadeColorEnabled(false);
        tablePopup.setColor(cc.color(138, 138, 138));
        tablePopup.setContentSize(277 * scaleFactor, 166 * scaleFactor);
        tablePopup.setAnchorPoint(0, 0);
        tablePopup.setPosition(cc.p(99 * scaleFactor, 8 * scaleFactor));
        whiteBackground.addChild(tablePopup);

        var tableRankheader = new cc.LabelTTF("Table Rank", "RobotoBold", 13 * 2 * scaleFactor);
        tableRankheader.setScale(0.5);
        tableRankheader.setColor(cc.color(255, 255, 255));
        tableRankheader.setAnchorPoint(0, 0.5);
        tableRankheader.setPosition(cc.p(10 * scaleFactor, 153 * scaleFactor));
        tablePopup.addChild(tableRankheader);

        var statusHeader = new cc.LabelTTF("Status", "RobotoBold", 13 * 2 * scaleFactor);
        statusHeader.setScale(0.5);
        statusHeader.setColor(cc.color(255, 255, 255));
        statusHeader.setAnchorPoint(0, 0.5);
        statusHeader.setPosition(cc.p(97 * scaleFactor, 153 * scaleFactor));
        tablePopup.addChild(statusHeader);

        this.tabelListView = new ccui.ListView();
        this.tabelListView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.tabelListView.setBackGroundColor(cc.color(229, 229, 229));
        this.tabelListView.setVisible(true);
        this.tabelListView.setContentSize(cc.size(tablePopup.width, 140 * scaleFactor));
        this.tabelListView.setPosition(cc.p(0, 0));
        tablePopup.addChild(this.tabelListView);

        this.initTouchListener();
        this.addCloseBtn();
        this.showTourInf(false);

        // this.roundInit();

        return true;

    },
    roundInit: function () {

        var curRnd = this._dataObj["currLevel"]; // gameRoom.parentRoom.getVariable(TournamentConstant.CUR_LEVEL);
        var rndStructStr = this._dataObj["prizeStr"]; // trnmntRoom.getVariable(TournamentConstant.GAME_PRZE_STRUCT);
        cc.log(" GamePrizeStructure Str: ", rndStructStr);

        var rndStructArr = rndStructStr.split(";");
        this.totRnd = rndStructArr.length;
        cc.log(" totRnd: ", this.totRnd);

        var rndBtn;
        var rndStrArr;
        var formatArr;
        var prizeStr;
        for (var i = 0; i < this.totRnd; i++) {
            rndBtn = new RoundContainer();
            rndStrArr = rndStructArr[i].split("#");
            rndBtn.posTF.setString("Round " + rndStrArr[1]);
            rndBtn.name = rndStrArr[0] + "#" + rndStrArr[1];
            rndBtn.setTag(1111);
            rndBtn.x = 0;
            rndBtn.y = -(this.totRnd - i) * rndBtn.height;
            cc.eventManager.addListener(this.tournamentInfoListener.clone(), rndBtn);
            this.btnRefPt.addChild(rndBtn, 1);
        }

        var tempFee = this.gameRoom.parentRoom.getVariable(TOURNAMENT_CONSTANTS.ENTRY_FEE).value;
        var tempArr;
        if (tempFee) {
            tempArr = tempFee.split(",");
            if (tempArr.length == 1) {
                this.entryFeeValue.setString(tempFee);
            }
            else {
                formatArr = tempArr[0].split("|");
                var feeArr = tempArr[1].split("|");

                if (formatArr[0] == "Chip")
                    this.entryFeeValue.setString("`" + feeArr[0]);

                if (formatArr[0] == "Ticket") {
                    if (Number(feeArr[0].split(":")[0]) == 1)
                        this.entryFeeValue.setString(formatArr[0]);
                    else
                        this.entryFeeValue.setString(feeArr[0].split(":")[0] + " " + formatArr[0]);
                }
            }
        }

        var curDeal = "";
        var maxDeal = "";
        var maxLevel = "";

        if (this.gameRoom.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_DEAL))
            curDeal = this.gameRoom.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_DEAL).value;

        if (this.gameRoom.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_DEAL))
            maxDeal = this.gameRoom.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_DEAL).value;

        if (this.gameRoom.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL))
            maxLevel = this.gameRoom.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL).value;

        this.roundValue.setString(curRnd + "/" + maxLevel);
        this.dealValue.setString(curDeal + "/" + maxDeal);

        this.showCurrentRndPrize();
    },
    roundBtnHandler: function (target) {
        cc.log("Round btn Clicked: " + target.name);

        var i = 0;
        var curBtn = target;

        if (this._currentRnd == curBtn)
            return;

        var curRound = this._dataObj["currLevel"]; // gameRoom.parentRoom.getVariable(TournamentConstant.CUR_LEVEL);
        var idx = this._currentRnd.name.split("#")[1];
        var curBtnName = (curBtn.name);
        var selectIndex = (curBtnName).split("#")[1];

        if (idx == curRound)
            this._currentRnd._gotoAndStop(3);
        else
            this._currentRnd._gotoAndStop(1);
        this._currentRnd.posTF.setString("Round " + idx);

        this._currentRnd = curBtn;
        if (selectIndex == curRound)
            this._currentRnd._gotoAndStop(4);
        else
            this._currentRnd._gotoAndStop(2);
        this._currentRnd.posTF.setString("Round " + selectIndex);

        this.showRndPrize(selectIndex);
    },
    showCurrentDeal: function () {
        var curDeal = "";
        var maxDeal = "";

        if (this.gameRoom.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_DEAL))
            curDeal = this.gameRoom.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_DEAL).value;

        if (this.gameRoom.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_DEAL))
            maxDeal = this.gameRoom.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_DEAL).value;

        this.dealValue.setString(curDeal + "/" + maxDeal);
    },
    showCurrentRndPrize: function () {
        var i = 0;
        // gameRoom.parentRoom.getVariable(TournamentConstant.CUR_LEVEL);

        var curRound = this._dataObj["currLevel"];
        cc.log("showCurrentRndPrize curRound :    ", curRound);
        var tempCurRound;
        var curRndForDisplay;
        var btnMC;

        var btnRefPtChildren = this.btnRefPt.getChildren();

        while (i < btnRefPtChildren.length) {
            btnMC = btnRefPtChildren[i];
            tempCurRound = btnMC.name.split("#")[1];

            if (tempCurRound == curRound) {
                this._currentRnd = btnMC;
                curRndForDisplay = Number(btnMC.name.split("#")[1]);
                btnMC._gotoAndStop(4);
                cc.log(" In game room for display : ", curRndForDisplay);
                this.showRndPrize(curRndForDisplay);
            }
            else
                btnMC._gotoAndStop(1);

            btnMC.posTF.setString("Round " + btnMC.name.split("#")[1]);
            i++;
        }

        this.roundValue.setString(curRndForDisplay + "/" + this.totRnd);
    },
    showRndPrize: function (rndDisplay) {

        this.tabelListView.removeAllChildrenWithCleanup(true);

        var rndStructStr = this._dataObj["prizeStr"];   //trnmntRoom.getVariable(TournamentConstant.GAME_PRZE_STRUCT);
        var rndStructArr = rndStructStr.split(";");
        var rndStrArr = [];
        var tempArr = [];
        for (var i = 0; i < rndStructArr.length; i++) {
            tempArr = rndStructArr[i].split("#");
            if (tempArr[1] == rndDisplay) {
                rndStrArr = tempArr; //String(rndStructArr[rnd - 1]).split("#");
                break;
            }
        }

        var rndInfoStrip;
        var posInfoArr = [];
        var prizeStr;

        for (var j = 2; j < rndStrArr.length; j++) {
            rndInfoStrip = new RndInfoPosStrip();
            posInfoArr = rndStrArr[j].split(",");
            rndInfoStrip.rndTxt.setString(posInfoArr[0]);   //Need to ask flash peeps

            if ((posInfoArr[1].split(":")[0] == "str"))
                rndInfoStrip.infoTF.setString(posInfoArr[1].split(":")[1]);  //Need to ask flash peeps
            else
                posInfoArr[1] && rndInfoStrip.infoTF.setString(PrizeStringCalculator.calculatePrzStr(posInfoArr[1]));

            this.tabelListView.pushBackCustomItem(rndInfoStrip);  //need to ask flash peeps
        }
    }
});

var RndInfoPosStrip = ccui.Layout.extend({
    ctor: function () {

        this._super();
        this.setContentSize(cc.size(277 * scaleFactor, 20 * scaleFactor));
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.setBackGroundColor(cc.color(229, 229, 229));

        this.rndTxt = new cc.LabelTTF("", "RobotoRegular", 12 * 2 * scaleFactor);
        this.rndTxt.setScale(0.5);
        this.rndTxt.setColor(cc.color(0, 0, 0));
        this.rndTxt.setAnchorPoint(0, 0.5);
        this.rndTxt.setPosition(38 * scaleFactor, this.height / 2);
        this.addChild(this.rndTxt);

        this.infoTF = new cc.LabelTTF("", "RupeeFordian", 12 * 2 * scaleFactor);
        this.infoTF.setScale(0.5);
        this.infoTF.setColor(cc.color(0, 0, 0));
        this.infoTF.setAnchorPoint(0, 0.5);
        this.infoTF.setPosition(95 * scaleFactor, this.height / 2);
        this.addChild(this.infoTF);

        var bottomLine = new cc.DrawNode();
        bottomLine.drawSegment(cc.p(0, 0), cc.p(this.width, 0), 1, cc.color(215, 215, 215));
        this.addChild(bottomLine);

        return true;
    }
});
var RoundContainer = cc.Sprite.extend({
    ctor: function () {
        this._super(spriteFrameCache.getSpriteFrame("roundContainer.png"));
        this.setAnchorPoint(0, 0.5);
        this.setCascadeOpacityEnabled(false);

        this.posTF = new cc.LabelTTF("Round  1", "RobotoBold", 12 * 2 * scaleFactor);
        this.posTF.setScale(0.5);
        this.posTF.setColor(cc.color(0, 0, 0));
        this.posTF.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(this.posTF, 3);
    },
    _gotoAndStop: function (val) {

        switch (val) {
            case 1:
                this.setOpacity(0);
                this.posTF.setColor(cc.color(0, 0, 0));
                break;
            case 2:
                this.setOpacity(255);
                this.posTF.setColor(cc.color(0, 0, 0));
                break;
            case 3:
                this.setOpacity(0);
                this.posTF.setColor(cc.color(217, 113, 2));
                break;
            case 4:
                this.setOpacity(255);
                this.posTF.setColor(cc.color(217, 113, 2));
                break;
        }

    }
});