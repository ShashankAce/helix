/**
 * Created by stpl on 3/10/2017.
 */
var SoundManager = {
    _audioPool: {},
    _musicPool: {},
    /**
     * @param {
     * SoundManager.cardDistribute|
     * SoundManager.cardFlip|
     * SoundManager.extratimesound|
     * SoundManager.cutTheDeck|
     * SoundManager.drop |
     * SoundManager.jointable |
     * SoundManager.meldingscreensound |
     * SoundManager.picknddropsound |
     * SoundManager.myturnsound |
     * SoundManager.validshow|
     * SoundManager.winner |
     * SoundManager.wrongShow} effectType
     * @param {Boolean} loop
     * */
    playSound: function (effectType, loop) {
        !loop && (loop = false);
        this._audioPool[effectType] = cc.audioEngine.playEffect(res.sound[effectType], loop);
    },
    removeSound: function (effectType) {
        this._audioPool[effectType] && cc.audioEngine.stopEffect(this._audioPool[effectType]);
    },
    pauseAllSound: function (bool) {
        cc.audioEngine.pauseAllEffects();
    },
    resumeAllSound: function (bool) {
        cc.audioEngine.resumeAllEffects();
    },
    stopAllSound: function () {
        cc.audioEngine.stopAllEffects();
    },
    /**
     * @param {
     * SoundManager.cardDistribute|
     * SoundManager.cardFlip|
     * SoundManager.extratimesound|
     * SoundManager.cutTheDeck|
     * SoundManager.drop |
     * SoundManager.jointable |
     * SoundManager.meldingscreensound |
     * SoundManager.picknddropsound |
     * SoundManager.myturnsound |
     * SoundManager.validshow|
     * SoundManager.winner |
     * SoundManager.wrongShow} musicType
     * @param {Boolean} loop
     * */
    playMusic: function (musicType, loop) {
        !loop && (loop = false);
        this._musicPool[musicType] = cc.audioEngine.playMusic(res.sound[musicType], loop);
    },
    removeMusic: function (musicType) {
        this._musicPool[musicType] && cc.audioEngine.stopMusic(this._musicPool[musicType]);
    },
    pauseAllMusic: function () {
        cc.audioEngine._pausePlaying();
    },
    resumeAllMusic: function () {
        cc.audioEngine._resumePlaying();
    },
    muteAllSound: function (bool) {
        if (bool) {
            cc.audioEngine.setMusicVolume(0.0);
            cc.audioEngine.setEffectsVolume(0.0);
        } else {
            cc.audioEngine.setMusicVolume(1.0);
            cc.audioEngine.setEffectsVolume(1.0);
        }
    }
};
SoundManager.cardDistribute = "carddistributionflip";
SoundManager.cardFlip = "cardFlip";
SoundManager.cutTheDeck = "cutthedeck";
SoundManager.drop = "dropsound";
SoundManager.extratimesound = "extratimesound";
SoundManager.finalwinningifme = "finalwinningifme";
SoundManager.jointable = "jointable";
SoundManager.meldingscreensound = "meldingscreensound";
SoundManager.myturnsound = "myturnsound";
SoundManager.picknddropsound = "picknddropsound";
SoundManager.validshow = "validshow";
SoundManager.wrongshow = "wrongshow";