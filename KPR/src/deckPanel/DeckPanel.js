/**
 * Created by stpl on 11/5/2016.
 */
var DeckPanel = cc.Layer.extend({
    _gameRoom: null,
    _wildCardName: null,
    _isMove: false,
    cardWidth: 0,
    cardHeight: 0,
    _animComp: null,
    ctor: function (context) {
        this._super();
        this._gameRoom = context;
        this._animComp = false;

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        this.wildcard = new WildCard(this, this._gameRoom);
        this.wildcard.rotation = -90;
        this.wildcard.setPosition(cc.p(385 * scaleFactor, 420 * scaleFactor));
        this.addChild(this.wildcard, 1);

        this.closedDeck = new ClosedDeck(this);
        this.closedDeck.setPosition(cc.p(440 * scaleFactor, 420 * scaleFactor));
        this.addChild(this.closedDeck, 2);

        this.openDeck = new OpenDeck(this);
        this.openDeck.setPosition(cc.p(549 * scaleFactor, 419 * scaleFactor));
        this.addChild(this.openDeck, 1);

        cc.eventManager.addListener(this.deckPanelMouseListener, this.openDeck);
        cc.eventManager.addListener(this.deckPanelMouseListener.clone(), this.closedDeck.blackShadow);
        cc.eventManager.addListener(this.deckPanelTouchListener, this.openDeck);
        cc.eventManager.addListener(this.deckPanelTouchListener.clone(), this.closedDeck.blackShadow);

        this._isMove = false;

        if (this._gameRoom.totCards == 21) {
            this.cardWidth = GameConstants.cardWidth21;
            this.cardHeight = GameConstants.cardHeight21;
        }
        else {
            this.cardWidth = GameConstants.cardWidth;
            this.cardHeight = GameConstants.cardHeight;
        }

        this.hideAllItems(true);
        return true;
    },
    glowEnabled: function (bool) {
        this.closedDeck.glowEnabled(bool);
        this.openDeck.glowEnabled(bool);
    },
    initPanel: function () {
        this.wildcard.removeJoker();
        this.openDeck.removeAllCard();
        this._animComp = true;
    },
    enable: function () {
        // this.wildcard.pauseListener(false);
        this.closedDeck.pauseListener(false);
        this.openDeck.pauseListener(false);
        // this.glowEnabled(true);
    },
    disable: function () {
        // this.wildcard.pauseListener(true);
        this.closedDeck.pauseListener(true);
        this.openDeck.pauseListener(true);
        // this.glowEnabled(false);
    },
    hideAllItems: function (bool) {
        this.wildcard.hide(bool);
        this.closedDeck.hide(bool);
        this.openDeck.hide(bool);
    },
    setWildCard: function () {
        if (this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD)) {
            var wildcardName = this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD).value;
            this._gameRoom._wildCardstr = wildcardName;
            this.wildcard.initJoker(wildcardName);
        }
    },
    initMouseListener: function () {
        this.deckPanelMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    !target._hovering && target.hover(true);
                    target._hovering = true;
                    return true;
                }
                target._hovering && target.hover(false);
                target._hovering = false;
                return false;
            }.bind(this)
        });
        this.deckPanelMouseListener.retain();
    },
    initTouchListener: function () {
        this.deckPanelTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                if (target.getTag() == 100)
                    target = target.parent;
                if (target._isEnabled && this._animComp) {
                    switch (target._name) {
                        case "OpenDeck":
                            this.pickOpenDeckCard();
                            break;
                        case "ClosedDeck":
                            this.pickClosedDeckCard();
                            break;
                    }
                }
                return true;
            }.bind(this)
        });
        this.deckPanelTouchListener.retain();
    },
    pickOpenDeckCard: function () {
        if (this._gameRoom._dropped) {
            return;
        }
        //"sk1:D#10;rummy_2:D#2"
        var str = this._gameRoom._serverRoom.getVariable(SFSConstants._DISCARD).value;
        if (str) {
            var plArr = str.split(';');
            var isDiscarded = false;
            for (var i = 0; i < plArr.length; i++) {
                if (plArr[i].split(":")[1]) {
                    isDiscarded = true;
                    break;
                }
            }
            if (str != "" && isDiscarded) {
                if (this.openDeck._openCrd._isJoker) {
                    this._gameRoom.showJokerToolTip();
                    return;
                }
                if (this.wildcard._joker && (this.wildcard._joker._cardNum == 0) && (this.openDeck._openCrd._cardNum == 1))	//wild card is joker
                {
                    this._gameRoom.showJokerToolTip();
                    return;
                }
                if ((this.openDeck._openCrd._cardNum == 0) || (this.wildcard._joker && this.openDeck._openCrd._cardNum == this.wildcard._joker._cardNum))	//Joker nd wild card
                {
                    this._gameRoom.showJokerToolTip();
                    return;
                }
            }
        }
        var pickObj = {};
        pickObj[SFSConstants.FLD_PC] = "OD";
        pickObj["card"] = this.openDeck._openCrd._name;
        this.disable();
        this._gameRoom.sendPickResp(pickObj);
    },
    pickClosedDeckCard: function () {
        var pickObj = {};
        pickObj[SFSConstants.FLD_PC] = "CD";
        this.disable();
        this._gameRoom.sendPickResp(pickObj);
        cc.log("closed deck pick");
    },
    handlePickResp: function (deckType, dataObj) {
        this._animComp = false;
        var seatObj;
        var closedCardObj;
        var closedCard;
        var fromPosition;
        var cardName;
        var cardEaseTime = 0.20;

        SoundManager.playSound(SoundManager.picknddropsound, false);
        if (deckType == "CLOSED_DECK") {

            if (this._gameRoom._currentTurnID == this._gameRoom._myId) {
                //animate card to self
                cardName = dataObj.CD || "S#A";
                fromPosition = this.closedDeck.getPosition();
                this._gameRoom._userCardPanel.DropBtn.hide(true);
                this._gameRoom._userCardPanel.setPickCard(cardName, fromPosition);

            } else {
                //animate card to other
                var card = new CardBack();
                card.setPosition(this.closedDeck.getPosition());
                this.addChild(card, GameConstants.deckPanelZorder + 5);
                seatObj = this._gameRoom.getSeatObjByUser(this._gameRoom._currentTurnID);

                closedCardObj = this._gameRoom.getClosedCardById(seatObj._seatPosition);
                closedCard = closedCardObj.getPosition();

                cc.log("OPEN_DECK position:" + closedCard.x + '--' + closedCard.y);

                if (!cc.game._paused) {
                    card.tween(cc.spawn(
                        cc.moveTo(cardEaseTime, cc.p(closedCard.x, closedCard.y)),
                        cc.scaleBy(cardEaseTime, 0.2, 0.2)
                    ), true);

                } else {
                    card.removeFromParent(true);
                }
            }
        }
        else {
            //OPEN_DECK
            //animate card to self
            if (this._gameRoom._currentTurnID == this._gameRoom._myId) {
                cardName = dataObj.card;
                fromPosition = this.openDeck.getPosition();
                this._gameRoom._userCardPanel.DropBtn.hide(true);
                // removing upper picked card from open deck
                this.openDeck.removeSingleCard(cardName);

                //animate card to self
                this._gameRoom._userCardPanel.setPickCard(cardName, fromPosition);

            } else {
                //animate card to other
                seatObj = this._gameRoom.getSeatObjByUser(this._gameRoom._currentTurnID);

                closedCardObj = this._gameRoom.getClosedCardById(seatObj._seatPosition);
                closedCard = closedCardObj.getPosition();

                cc.log("OPEN_DECK position:" + closedCard.x + '--' + closedCard.y);

                /*var openDeckPos = this.openDeck.getPosition();
                 var finalPos = cc.p(-(openDeckPos.x - closedCard.x), -(openDeckPos.y - closedCard.y));*/
                if (!cc.game._paused) {
                    this.openDeck._openCrd.runAction(cc.sequence(
                        cc.spawn(
                            cc.moveTo(cardEaseTime, closedCard.x, closedCard.y),
                            cc.scaleBy(cardEaseTime, 0.2, 0.2)
                        ),
                        cc.callFunc(function () {
                            this.openDeck._openCrd.removeFromParent(true);
                        }, this))
                    );

                } else {
                    this.openDeck._openCrd.removeFromParent(true);
                }
            }
        }
    }
});