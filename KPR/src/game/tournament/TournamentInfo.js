/**
 * Created by stpl on 4/10/2017.
 */
var TournamentInfo = cc.Scale9Sprite.extend({
    totRnd: null,
    trnmntRoom: null,
    gameRoom: null,
    _myRank: null,
    _trnmntInfo: null,
    _myChip: null,
    _dataObj: null,
    _currentRnd: null,
    _isEnabled: null,
    tournamentInfoListener: null,

    ctor: function (trnmntGameRoom, dataObj) {
        this._super(spriteFrameCache.getSpriteFrame("smileyBorder.png"), cc.rect(0, 0, 41 * scaleFactor, 41 * scaleFactor));
        this.setCapInsets(cc.rect(16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor));
        this.setCascadeColorEnabled(false);
        this.setCascadeOpacityEnabled(false);
        this.initTouchListener();

        this.trnmntRoom = trnmntGameRoom._serverRoom;
        this.gameRoom = trnmntGameRoom;
        this._myRank = -1;
        this._dataObj = dataObj;
        this._isEnabled = false;
    },
    addCloseBtn: function () {
        var closeBtn = new cc.Sprite(spriteFrameCache.getSpriteFrame("closeBlack.png"));
        closeBtn.setPosition(cc.p(this.width, this.height));
        this.addChild(closeBtn, 1, "closeBtn");
        cc.eventManager.addListener(this.tournamentInfoListener.clone(), closeBtn);
    },
    updateRoom: function (room) {
        this.trnmntRoom = room;
    },
    updateRank: function (rank, chip) {
        this._myChip = chip;
        this._myRank = rank;
    },
    showTournamentInfo: function (dataObj) {
        this._dataObj = dataObj;
        this.infoVisibleHandler();
        this.showTourInf(true);
    },
    infoVisibleHandler: function () {
        var grpType = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value; // Time Round
        if (grpType == "Round")
            this.roundInit();
        else
            this.setTimeParameter();
    },
    showTourInf: function (bool) {
        this._isEnabled = bool;
        this.setVisible(bool);
    },
    initTouchListener: function () {
        this.tournamentInfoListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode) && this._isEnabled) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                if (target.getName() == "closeBtn")
                    this.showTourInf(false);
                else if (target.tag == 1111)
                    this.roundBtnHandler(target);
            }.bind(this)
        });
        this.tournamentInfoListener.retain();
    }
});