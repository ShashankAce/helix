/**
 * Created by stpl on 12/28/2016.
 */
"use strict";
var ConnectionChecker = cc.Class.extend({
    _urlToCheck: null,
    _pingTime: null,
    _timeOut: null,
    _timeoutCount: null,
    _disconnectCount: null,
    _contentToCheck: null,
    checkTimer: null,
    _xml: null,
    _preCounter: null,
    _postCounter: null,
    _localTest: null,
    _pingCounter: null,
    _disconnectCounter: null,
    _roomListCounter: null,
    _roundCounter: null,
    _pingType: null,
    _discType: null,
    _discTime: null,
    tempCount: null,
    ccTimeOutManager: null,

    ctor: function () {
        this._postCounter = 1;
        this._preCounter = 1;
        this._disconnectCounter = 0;
        this._roomListCounter = 0;
        this._pingCounter = 0;
        this._localTest = 0;
        this._roundCounter = 0;
        this._discTime = -2;
        this._discType = "";
        this.tempCount = 1;
        this._urlToCheck = "http://192.168.124.27:9080/RummyGameEngine";
        this._pingTime = 5000;
        this._timeOut = 5000;
        this._timeoutCount = 5;
        this._disconnectCount = 0;
        this._contentToCheck = "This is my JSP page.";
        this.checkTimer = null;
        this._xml = "";
        this._pingType = SFS2XClient._pingType;
        this.ccTimeOutManager = new CCTimeOutManager();
    },
    setParameter: function (data) {
        this._discTime = -2;
        this._urlToCheck = data.url;
        this._localTest = parseInt(data.localTest);
        this._pingTime = parseInt(data.pingTime);
        this._timeoutCount = parseInt(data.timeoutCount);
        this._disconnectCount = parseInt(data.disconnectCount);
        this.stopTimer();
        this.startTimer();
    },
    startTimer: function () {
        this.stopTimer();
        this.connectionHandler();
        this.ccTimeOutManager.scheduleInterval(this.connectionHandler.bind(this), 1000, 0, "connectionTimer");
    },
    stopTimer: function () {
        this.ccTimeOutManager.unScheduleInterval("connectionTimer");
    },
    connectionHandler: function () {
        if (this._discTime >= 0)
            this._discTime++;
        this.tempCount--;
        if (this.tempCount > 0)
            return;

        this.tempCount = this._pingTime;
        this._discType = "";

        if (sfs.isConnected()) {
            if (sfs.getRoomList().length > 0) {
                sfsClient.pingZone();
                this._disconnectCounter++;
                if (this._disconnectCounter == this._disconnectCount)
                    sfsClient.disconnectUser();

                if (!applicationFacade._controlAndDisplay._onlineStatus) {
                    // Request Data From Server
                    //applicationFacade.onSuccesResponse("success");
                    return;
                }
                if (this._pingType == "ping")
                    this._pingCounter++;
                else if (this._pingType == "round")
                    this._roundCounter++;
                else {
                    this._pingCounter++;
                    this._roundCounter++;
                }
                if (this._pingCounter == this._timeoutCount)
                    this._discType = "ping";

                if (this._roundCounter == this._timeoutCount)
                    this._discType = "round";

                if (this._pingCounter == this._timeoutCount && this._roundCounter == this._timeoutCount)
                    this._discType = "both";

                if (this._discType != "") {
                    this._discTime = 0;
                    //trace("_pingCounter, _disconnectCounter,_roomListCounter: ",_pingCounter,_disconnectCounter,_roomListCounter);
                    applicationFacade.onErrorResponse("error");
                }
            }
            else {
                this._roomListCounter++;
                if (this._roomListCounter == this._disconnectCount)
                    sfsClient.disconnectUser();
            }
        }
        else {
            // Load Config
            this._discTime = -2;
            applicationFacade._controlAndDisplay.setLoadTime();
            cc.log("reconnect to sfs");
            sfsClient.connectSfs();
            // sfsClient.loadConnectionConfigDiscon(rummyData.isLogin);
        }
    },
    pingResp: function (respType) {
        if ((!applicationFacade._controlAndDisplay._onlineStatus) && (this._pingCounter == this._timeoutCount || this._roundCounter == this._timeoutCount))
            applicationFacade.onSuccesResponse("success");

        if (this._pingType == "ping")
            this._pingCounter = 0;
        else if (this._pingType == "round")
            this._roundCounter = 0;
        else {
            this._pingCounter = 0;
            this._roundCounter = 0;
        }
        this._discType = "";
        this._discTime = -2;
        this._disconnectCounter = 0;
        this._roomListCounter = 0;
    },
    checkOnline: function (isOnline) {
        if (isOnline)
            this.dispatchSuccessEvent();
        else
            this.dispatchErrorEvent();
    },
    dispatchSuccessEvent: function () {
        this._preCounter = 1;
        if (applicationFacade._controlAndDisplay._onlineStatus) {
            this._postCounter = 1;
            applicationFacade.onSuccesResponse("success");
            return;
        }
        if (this._postCounter == this._timeoutCount) {
            applicationFacade.onSuccesResponse("success");
        }
        else
            this._postCounter++;
    },
    dispatchErrorEvent: function () {
        if (this._preCounter == this._timeoutCount)
            applicationFacade.onErrorResponse("error");
        else
            this._preCounter++;
    }

});
