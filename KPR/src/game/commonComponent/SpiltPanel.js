/**
 * Created by stpl on 12/6/2016.
 */
'use strict';
var SplitPanel = Popup.extend({
    _gameRoom: null,
    _resultPanel: null,
    splitDataStr: null,
    splitInfoObj: null,
    splitList: null,
    _Position: null,
    _hovering: null,

    ctor: function (context, splitData) {
        this._super(new cc.Size(450 * scaleFactor, 285 * scaleFactor), context);
        this._resultPanel = context;
        this._gameRoom = context._gameRoom;

        this._hovering = false;
        this.splitDataStr = [];
        this.splitInfoObj = {};
        this.splitList = [];
      //  this.removeAllChildren(true);
        this.headerLabel.setString("Split Confirmation");


        this.msg = new cc.LabelTTF("Please Wait...", "RobotoRegular", 15 * scaleFactor);
        this.msg.setColor(cc.color(0, 0, 0));
        this.msg.setPosition(cc.p(this.width / 2, 70 * scaleFactor));
        this.msg.setVisible(false);
        // this.msg.setPosition(cc.p(this.width / 2 - 50, this.height / 2 - 50));
        this.addChild(this.msg);

        this.yesButton = this.createSprite("yesBtn", "yesBtnOver", "YesButton", cc.p(179 * scaleFactor, 40 * scaleFactor));
        this.addChild(this.yesButton, 2);
        this.yesButton.hide(true);

        this.noButton = this.createSprite("noBtn", "noBtnOver", "NoButton", cc.p(277 * scaleFactor, 40 * scaleFactor));
        this.addChild(this.noButton, 2);
        this.noButton.hide(true);

        // var splitData = "1000:112.45,1200:234.56,7867:123.56";
        this.initSplitRow(splitData);

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();


        cc.eventManager.addListener(this.popupMouseListener.clone(), this.yesButton);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.noButton);

        cc.eventManager.addListener(this.popupTouchListener.clone(), this.yesButton);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.noButton);


    },
    initSplitRow: function (splitData) {

        this.splitDataStr = splitData;
        this.splitDataArr = this.splitDataStr.split(",");
        var plName, plId, score, height = 0;
        for (var i = 0; i < this.splitDataArr.length; i++) {
            plId = this.splitDataArr[i].split(":")[0];
            plName = this._gameRoom.getPlayerNameById(plId);
            score = this.splitDataArr[i].split(":")[1];

            var rect = new cc.DrawNode();
            // rect.drawRect(cc.p(240, 193 - height), cc.p(360, 213 - height), cc.color(255, 0, 0), 0.5);
            rect.drawRect(cc.p(0, 0), cc.p(120 * scaleFactor, 26 * scaleFactor), cc.color(228, 238, 244), 0, cc.color(228, 238, 244));
            rect.setPosition(cc.p(240 * scaleFactor, 200 * scaleFactor - height));
            this.addChild(rect);

            var list = new SplitList(plId, plName, score, height);
            list.setPosition(cc.p(108 * scaleFactor, 212 * scaleFactor - height));

            this.splitList.push(list);
            this.addChild(list, 1);

            var line1 = new cc.DrawNode();
            line1.drawSegment(cc.p(0, 0), cc.p(330 * scaleFactor, 0.3 * scaleFactor), 0, cc.color(230, 230, 230));
            line1.setPosition(cc.p(60 * scaleFactor, 187 * scaleFactor - height));
            this.addChild(line1);

            height += 50 * scaleFactor;
        }
        this.yesButton.hide(false);
        this.noButton.hide(false);
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        switch (target._name) {
            case "YesButton":
                if (target._isEnabled)
                    this.splitCnfrmHandler();
                break;
            case "NoButton":
                if (target._isEnabled)
                    this.noBtnHandler();
                break;
        }
    },
    splitCnfrmHandler: function () {
        this.yesButton.hide(true);
        this.noButton.hide(true);
        this.msg.setString("Please Wait...");
        this.msg.setVisible(true);

        var sentObj = {};
        sentObj["isClick"] = "true";
        this._gameRoom.sendReqToServer("splitResp", sentObj);
    },
    noBtnHandler: function () {
        this.yesButton.hide(true);
        this.noButton.hide(true);
        this.msg.setString("Split is declined, Game will continue...");
        this.msg.setVisible(true);

        var sentObj = {};
        sentObj["isClick"] = "false";
        this._gameRoom.sendReqToServer("splitResp", sentObj);
    },
    handleSplitResp: function (dataObj) {
        if (this._gameRoom.isSplitVisible) {
            var isClick = dataObj["isClick"] != null ? dataObj["isClick"] : "null";
            var id = dataObj["id"];
            this._resultPanel.SplitAndRejoinBtn.hide(true);
            if (id != null || id != undefined) {
                var index = this.getList(id);
                if (isClick == "true") {
                    this.splitList[index].addYesMark(true, name);
                } else {
                    this.msg.setString("Split is declined,,Game will continue...");
                    this.msg.setVisible(true);
                    this.yesButton && this.yesButton.hide(true);
                    this.noButton && this.noButton.hide(true);
                    this.splitList[index].addNoMark(true, name);
                }
            }
        }

    },
    getList: function (plId) {
        for (var i = 0; i < this.splitList.length; i++) {
            if (this.splitList[i].playerId == parseInt(plId))
                return i;
        }
    },
    pauseListener: function (bool) {
        this._isEnabled = !bool;
    },
    hide: function (bool) {
        this.setVisible(!bool);
        this._isEnabled = !bool;
    },
    hover: function (bool) {

    },
    createSprite: function (spriteName, spriteOverName, buttonName, pos) {
        var Sprite = cc.Sprite.extend({
            spriteOverName: null,
            spriteName: null,
            _isEnabled: null,
            _hovering: null,
            ctor: function (spriteName, spriteOverName, buttonName, pos) {
                this._super(spriteFrameCache.getSpriteFrame(spriteName + ".png"));
                this.spriteName = spriteName;
                this.spriteOverName = spriteOverName;

                this._hovering = false;
                pos && this.setPosition(pos);
                this.setName(buttonName);

                return true;
            },
            hover: function (bool) {

                bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteOverName + ".png")) :
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteName + ".png"));
            },
            pauseListener: function (bool) {
                this._isEnabled = !bool;
            },
            hide: function (bool) {
                this.setVisible(!bool);
                this._isEnabled = !bool;
            }
        });
        return new Sprite(spriteName, spriteOverName, buttonName, pos);
    }
});

var SplitList = cc.Node.extend({
    playerId: null,
    statusMark: null,
    ctor: function (playerId, name, score, height) {
        this._super();
        this.setName(name);
        this.playerId = parseInt(playerId);

        var playerName = new cc.LabelTTF(name);
        playerName.setFontSize(30);
        playerName.setScale(0.5);
        playerName.setAnchorPoint(0, 0.5);
        playerName.setPosition(cc.p(0, 0));
        playerName.setColor(cc.color(0, 0, 0));
        this.addChild(playerName, 2);

        var winningAmount = new cc.LabelTTF(score);
        winningAmount.setFontSize(30 * scaleFactor);
        winningAmount.setScale(0.5);
        winningAmount._setBoundingWidth(200 * scaleFactor);
        winningAmount.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        winningAmount.setColor(cc.color(54, 82, 98));
        winningAmount.setAnchorPoint(0, 0.5);
        winningAmount.setPosition(cc.p(140 * scaleFactor, 0));
        winningAmount.setColor(cc.color(0, 0, 0));
        this.addChild(winningAmount, 2);

        // mark sprite
        this.statusMark = new cc.Sprite();
        this.statusMark.setPosition(cc.p(271, 0));
        this.addChild(this.statusMark, 20, "statusMark");

        return true;

    },
    addYesMark: function (bool) {
        if (bool) {
            this.statusMark.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("Tick.png"));
        } else {
            this.removeChild(this.statusMark);
        }
    },
    addNoMark: function (bool, name) {
        if (bool) {
            this.statusMark.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("Cross.png"));
        } else {
            this.removeChild(this.statusMark);
        }
    }
});
