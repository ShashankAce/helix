/**
 * Created by stpl on 9/16/2016.
 */
"use strict";
var PoolGame = NormalBaseGame.extend({
    rejoinPlArr: null,
    isSplitVisible: null,
    ctor: function (room) {
        this._super(room);

        this.rejoinPlArr = [];
        this.isSplitVisible = false;

        this.initHeader();
        this.initRoom();

        cc.log("Pool game " + this.tableId + " is initialized");
        return true;
    },
    _joinRoom: function (seatPos) {
        /*var avatarPic = new SFS2X.Entities.Variables.SFSUserVariable("switch", true);
         sfs.send(new SFS2X.Requests.System.SetUserVariablesRequest([avatarPic]));*/

        var obj = {};
        obj[SFSConstants.FLD_SEATNO] = parseInt(seatPos);
        this.sendReqToServer(SFSConstants.CMD_CONFIRM, obj);
    },
    instantPlayEnable: function () {
        this._super();
        if (this._myId == -1 || this._isWatching) {
            this.insantPlayBtn._isEnable = true;
            this.instantNotMC.setVisible(true);
            this.watchingTxt.setVisible(true);
            this.watchingTxt.setString("You are currently watching this game. To play game, visit the lobby.");
        }
    },
    initNewRound: function (dataObj) {
        this._super(dataObj);
        this._currentRound = dataObj["rndNo"];

        if (this._currentRound == 1) {
            for (var i = 0; i < this._playerArray.length; i++) {
                this._playerArray[i].updateScore();
            }
        }
        this.removeExtraPlayers();
        if(this.result && this.result.splitPopup){
           // this.result.splitPopup.splitList =  [];
            this.result.splitPopup.removeFromParent(true);
            this.result.splitPopup = null;
        }
        this.updateHeader();
    },
    processRoomStatus: function (dataObj, room) {
        this._super(dataObj, room);

        var playerString = dataObj.plsStr;
        if (playerString != "")
            this.currentSeatedPlayers = playerString.split(",");

        this._currentRound = dataObj.rndNo;


        if (this._isWatching && (this._currentRoomState == SFSConstants.RS_WATCH || this._currentRoomState == SFSConstants.RS_CUTTHEDECK)) {
            this.removeExtraPlayers();
        }

        if (this._currentRoomState == SFSConstants.RS_WATCH) {
            this.deckPanel.setWildCard();
        }
        if (this._currentRoomState == SFSConstants.RS_RESULT) {
            this._currentRound -= 1;
            this.instantPlayEnable();
        }
        if (this._currentRoomState != SFSConstants.RS_JOIN) {
            this.disableAllChairs();
            this.updateHeader();
            if (this._currentRound > 1) {
                // todo make setting last result and scoreBtn active
            }
            if (this._myId == -1) {
                this._isWatching = true;
                this._tabMc.setIcon(2);
            }
            this.gameInfoSprite && this.gameInfoSprite.updatePrize();
        }
        else {
            // todo make setting last result and scoreBtn inactive
        }
        if (!dataObj["currentRndPointStr"])
            return;

        var rndPntArr = (dataObj["currentRndPointStr"].split(","));
        var pts;
        for (var i = 0; i < rndPntArr.length - 1; i++) {
            if (rndPntArr[i]) {
                pts = rndPntArr[i].split(":")[1];
                if (pts == 0)
                    continue;
                var profObj = this.getSeatObjByUser(rndPntArr[i].split(":")[0]);
                if (profObj && profObj != -1)
                    profObj.setRndPoints(pts);
            }
        }

        this.removeExtraPlayers();
    },

    updateHeader: function () {
        var gameInfoTxt = "";
        var sessionId = "--";
        if(this._serverRoom.getVariable(SFSConstants._SESSION_ID))
            sessionId = this._serverRoom.getVariable(SFSConstants._SESSION_ID).value;
        if (this._serverRoom) {
            var gameType = this._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
            if (gameType.search("Best") != -1)
                gameType = gameType.split("-")[1];
            var turnType = this._serverRoom.getVariable(SFSConstants._TURNTYPE).value;
            gameInfoTxt = gameType + " - " + this._serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value + " Card |  Table ID: " + this._serverRoom.getVariable(SFSConstants._ROOMID).value + " |  Session ID: " + sessionId +" |  Round: " + (this._currentRound == 0 ? "--" : this._currentRound);
            this._gameInfoTF.setString(gameInfoTxt);

        }
        else {
            /* var gameType:String = _dummyObj[SFSConstants._GAMETYPE].toString();
             if (gameType.search("Best") != -1)
             gameType = gameType.split("-")[1];
             var turnType:String = _dummyObj[SFSConstants._TURNTYPE];
             gameInfoTxt.text = gameType + " - " + _dummyObj[SFSConstants.FLD_NUMOFCARD] + " Card |  Table ID: " + _dummyObj[SFSConstants._ROOMID].toString() + " |  Round: " + (this._currentRound == 0?"--":this._currentRound);
             */
        }
        this.updateHeaderPosition();
    },
    noBalanceResp: function (msgStr, isCnfrm) {
        if (!this.noBalPopup) {
            this.noBalPopup = new NoBalPopup(msgStr, this);
            this.addChild(this.noBalPopup, GameConstants.noBalPop, "noBalPopup");
        }

        if (isCnfrm == "true") {
            this.noBalPopup.hide(this.noBalPopup.okButton, true);
            this.noBalPopup.hide(this.noBalPopup.yesButton, false);
            this.noBalPopup.hide(this.noBalPopup.noButton, false);
        }
        else {
            this.noBalPopup.hide(this.noBalPopup.okButton, false);
            this.noBalPopup.hide(this.noBalPopup.yesButton, true);
            this.noBalPopup.hide(this.noBalPopup.noButton, true);
        }
    },
    seatConfirmResponse: function (data, room) {
        this._super(data, room);

        var seatNo = parseInt(data[SFSConstants.FLD_SEATNO]);
        var userId = parseInt(data[SFSConstants._USERID]);
        var userScore = data[SFSConstants._NUM];
        var userName = data[SFSConstants.FLD_UNAME];
        var isConfirmed = data[SFSConstants.MSG].toLowerCase() == "confirmed";
        var isWaiting = false;

        if (data.hasOwnProperty(SFSConstants.FLD_IS_NEXT))
            isWaiting = data[SFSConstants.FLD_IS_NEXT];

        if (userName == rummyData.nickName) {
            this._myId = parseInt(userId);
            applicationFacade._myUName = userName;
            applicationFacade._myId = this._myId;
            this._isWatching = false;
            this.selfSeatPosition = seatNo;
            this.disableAllChairs();
            // this.showNotification();
        }

        this._playerArray[(seatNo - 1) > -1 && seatNo - 1].updateInfo(seatNo, userId, userName, userScore, !1, !1, !1, !1, isWaiting);
        this._playerArray[(seatNo - 1) > -1 && seatNo - 1].takeSeat(userId, userName, userScore);

        this.updatePlayerInfo(KHEL_CONSTANTS.USER_JOINED, this._playerArray[(seatNo - 1) > -1 && seatNo - 1], false, false, false, false, isWaiting);

    },
    processToss: function (data) {
        this._super(data);
        this.removeExtraPlayers();
        this.disableAllChairs();
        if (this._myId != -1) {
            //reArrangePopUpMC.visible = true;
        }
        else {
            this._isWatching = true;
            this._tabMc.setIcon(2);
        }
    },
    processCutTheDeckCmd: function (dataObj) {
        if(this.result && this.result.splitPopup){
           // this.result.splitPopup.splitList =  [];
            this.result.splitPopup.removeFromParent(true);
            this.result.splitPopup = null;
        }
        this.initNewRound(dataObj);
        var rejoinPlStr;
        rejoinPlStr = this.rejoinPlArr.join(",");
        if ((rejoinPlStr) && (rejoinPlStr != "undefined") && (rejoinPlStr != "")) {
            rejoinPlStr += " rejoined the table!!";
            this.showNotification(rejoinPlStr);
        }
        this.rejoinPlArr = [];
    },
    handleResult: function (dataObj) {
        this._super(dataObj);
        this._userCardPanel.showConfirmPopup && this._userCardPanel.showConfirmPopup.removeFromParent(true);
        if (!dataObj.rndId)
            dataObj.rndId = this._currentRound;

        if (this._serverRoom.getVariable(SFSConstants._WILDCARD) && this._serverRoom.getVariable(SFSConstants._WILDCARD).value)
            this.result.setWildCard(this._serverRoom.getVariable(SFSConstants._WILDCARD).value);
        else
            this.result.setWildCard("");
    },
    processFreeSeat: function (param) {
        if (!param[SFSConstants.FLD_NAME])
            return;

        var name = param[SFSConstants.FLD_NAME];
        var obj = this.getSeatObjByUserName(name);
        if (obj) {

            window.clearInterval(this._userCardPanel.cardDistributeInterval);

            if (this._currentRoomState == SFSConstants.RS_MELD) {
                if (obj._playerId == this._currentTurnID) {
                    this._extraTimeToggle = false;
                }
            }

            if (this._currentRoomState == SFSConstants.RS_WATCH) {
                this.removeFreeSeatPlayer(obj);
                /*if (obj._playerId == this._currentTurnID) {
                 }*/
            }

            this.removeClosedCardForSpecificPlayer(obj);
            obj.leaveSeat();
            this.updatePlayerInfo(KHEL_CONSTANTS.USER_LEFT, obj, false, false, false, false, false);

            if (parseInt(obj._playerId) == this._myId) {
                this._myId = -1;
            }

            var leftPlStr;
            if (this._leftPlArr.indexOf(name) == -1)
                this._leftPlArr.push(name);

            leftPlStr = this._leftPlArr.join(",");
            if ((leftPlStr) && (leftPlStr != "") && (this._currentRoomState != SFSConstants.RS_RESULT))
                this.showNotification(leftPlStr + " left the table!!");
        }
    },
    removeFreeSeatPlayer: function (player) {
        var playerId = player._playerId;
        for (var i = 0; i < this._playerArray.length; i++) {
            if (playerId == this._playerArray[i]._playerId) {
                this.removeChild(this._playerArray[i]);
                this._playerArray.splice(i, 1);
                return;
            }
        }
    },
    processScoreBoard: function (params) {
        if (params.totRnd > 0) {
            if (this.scoreBoardPanel) {
                this.scoreBoardPanel.removeFromParent(true);
                return;
            }
            this.scoreBoardPanel = new ScoreBoard(this, params, this.ScoreBoardPlayers);
            this.addChild(this.scoreBoardPanel, GameConstants.discardPanelZorder + 1, "scoreBoardPanel");
        }
    },
    updatePoints: function (obj) {
        var profObj = this.getSeatObjByUser(obj["pId"]);
        if (profObj && profObj != -1) {
            if (obj["totPoints"])
                profObj.setScore(obj["totPoints"]);

            if (obj["rndPoints"])
                profObj.setRndPoints(obj["rndPoints"]);
        }
    },
    processAutoSplitResponse: function (params) {
        // SoundManager.stopEffect("extratimesound");
        this.handleSplitWinning(params);
    },
    handleSplitWinning: function (params) {
        if(this.result && this.result.splitPopup){
           // this.result.splitPopup.splitList =  [];
            this.result.splitPopup.removeFromParent(true); 
            this.result.splitPopup = null;
        }
        this.updateHeader();
        this.stopWaitTimer();
        this.initPopup();

        this._currentRound = 0;
        this._isWatching = true;

        this.winner = new Winner(params, this);
        this.addChild(this.winner, GameConstants.winnerScreenZorder, "winner");

        for (var i = 0; i < this._playerArray.length; i++) {
            cc.eventManager.removeListener(this._playerArray[i], true);
            this._playerArray[i].removeFromParent(true);
        }
        this._playerArray = [];
        this.initRoom();
    },
    handleSplitResp: function (params) {
        /*roomId: 3057 (Num)
         isClick: false (Str)
         id: 1585 (Num)*/
        /*todo if(resultPanel._isStillIn)
         resultPanel.splitMC.handleSplitResp(params);*/

        /*  var RmId = data["forRmId"];
         var isClick = data["isClick"] != null ? data["isClick"] : "null";
         var id = data["id"];*/

        SoundManager.removeSound(SoundManager.extratimesound);
        !this.result.splitPopup && this.result.splitBtnHandler();
        this.result.splitPopup.handleSplitResp(params);

    },
    handleRejoinResp: function (params) {
        var playerId = params[SFSConstants.FLD_ID];
        var points = params[SFSConstants.FLD_PTS];
        this.rejoinPlArr.push(this.getPlayerNameById(playerId));
        var seatObj = this.getSeatObjByUser(playerId);
        if ((seatObj) && (seatObj._seatPosition != -1)) {
            seatObj.updateScore(points);
            // seatObj._points = parseFloat(points);
            //todo seatObj.setPoints();
        }
    }
});


/*
 * var vertices = [
 {x: x + radius, y: y},
 {x: r - radius, y: y},
 {cpx: r, cpy: y, x: r, y: y + radius},
 {x: r, y: b - radius},
 {cpx: r, cpy: b, x: r - radius, y: b},
 {x: x + radius, y: b},
 {cpx: x, cpy: b, x: x, y: b - radius},
 {x: x, y: y + radius},
 {cpx: x, cpy: y, x: x + radius, y: y}
 ];
 this.roundRect = new cc.DrawNode();
 this.roundRect.drawArc(vertices, 1, radius, cc.color(250, 255, 0), cc.color(250, 0, 0), false);
 this.addChild(this.roundRect, 20);
 *
 * */