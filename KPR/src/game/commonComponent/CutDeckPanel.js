/**
 * Created by stpl on 11/15/2016.
 */
var CutDeckPanel = cc.LayerColor.extend({
    _name: "CutDeckPanel",
    _gameRoom: null,
    gameType: null,
    selectedCard: null,
    turnBool: null,
    checkGameInt: null,
    ctor: function (timeLeft, context, player, dealer, turnBool) {
        this._super(cc.color(0, 0, 0, 150 * scaleFactor));
        this.timeOutManager = new CCTimeOutManager();
        this._gameRoom = context;
        this.turnBool = false;
        this.gameType = this._gameRoom._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
        this.selectedCard = 0;
        this.noOfCard = 13;
        this.cardDistance = 2 * scaleFactor;
        this._deckInitialPosition = cc.p(this.width / 2, 315 * scaleFactor);
        this._cardArray = [];
        this.checkGameInt = 0;

        this.createDeck();


        var userName = player['_playerName'];
        var opponentName = dealer['_playerName'];

        var playerImg;
        var dealerImg;

        if (player['imgData'])
            playerImg = player['imgData'];

        if (dealer['imgData'])
            dealerImg = dealer['imgData'];

        this.turnBool = turnBool;
        if (turnBool) {
            this.initHandArrow();
            if ('mouse' in cc.sys.capabilities) {
                this.initMouseListener();
            }
            this.initTouchListener();
            this.addListener();
        }

        cc.log("CutDeckTurnBool is:" + turnBool);

        this.cutDeckMessage = new cc.LabelTTF("Offering Cut The Deck!", "RobotoRegular", 22 * 2 * scaleFactor);
        this.cutDeckMessage.setScale(0.5);
        this.cutDeckMessage.setPosition(cc.p(this.width / 2, 410 * scaleFactor));
        this.addChild(this.cutDeckMessage, 2);

        var greenBg = new cc.Sprite(spriteFrameCache.getSpriteFrame("Sortbtn.png"));
        greenBg.setPosition(this.width / 2, 445 * scaleFactor);
        greenBg.setScale(0.7);
        this.addChild(greenBg, 1, "green");

        this.cutTimer = new cc.LabelTTF("1", "RobotoBold", 15 * 2 * scaleFactor);
        this.cutTimer.setScale(0.5);
        this.cutTimer.setColor(cc.color(0, 0, 0));
        this.cutTimer.setPosition(cc.p(this.width / 2, 445 * scaleFactor - yMargin));
        this.addChild(this.cutTimer, 5);

        var hintMsg;
        if (!turnBool)
            hintMsg = opponentName + " is cutting the deck. Please wait...";
        else
            hintMsg = "Click on above deck to divide the cards.";

        var hint = new cc.LabelTTF(hintMsg, "RobotoRegular", 15 * 2 * scaleFactor);
        hint.setColor(cc.color(100, 100, 100));
        hint.setScale(0.5);
        hint.setPosition(cc.p(this.width / 2, 200 * scaleFactor));
        this.addChild(hint, 2);

        this.player1 = new CircularProfile(cc.p(250 * scaleFactor, this._deckInitialPosition.y), userName);
        this.addChild(this.player1, 2);
        this.player1.setProfilePic(playerImg);

        this.player2 = new CircularProfile(cc.p(this.width - 250 * scaleFactor, this._deckInitialPosition.y), opponentName);
        this.addChild(this.player2, 2);
        this.player2.setProfilePic(dealerImg);

        timeLeft < 0 ? timeLeft = 0 : timeLeft;
        this.initTimer(timeLeft || 10);
        this.createOfferDots();

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                cc.log("Blank listener in CutDeckPanel");
                return true;
            }.bind(this)
        }, this);
    },
    createDeck: function () {
        var index = 0, card;
        while (index < this.noOfCard) {
            card = new cc.Sprite(spriteFrameCache.getSpriteFrame("isometricCard.png"));
            card.setScale(0.7);
            card.setTag(index);
            card.cardNo = index + 1;
            card.isHoverd = !1;
            card.hover = function (bool) {
                return true;
            };
            this.addChild(card);
            this._cardArray.push(card);
            index++;
        }
        this.packDeck();
    },
    packDeck: function () {
        //reverse loop 52-0 so that card with higher zIndex is on top
        //card placement is from top to down
        this.selectedCard = 0;
        var index = this._cardArray.length - 1, y = this._deckInitialPosition.y, maxZOrder = this.noOfCard * 4;
        while (index > -1) {
            this._cardArray[index].setPosition(cc.p(this._deckInitialPosition.x, y -= this.cardDistance));
            this._cardArray[index].setLocalZOrder(maxZOrder--);
            index--;
        }
    },
    createOfferDots: function () {

        var DotsNode = cc.Node.extend({
            _stopAnimation: null,
            ctor: function () {
                this._super();
                this._stopAnimation = !1;
                this.setContentSize(cc.size(60 * scaleFactor, 15 * scaleFactor));
                this._dotArray = [];

                var dot = cc.Sprite.extend({
                    ctor: function (index) {
                        this._super(spriteFrameCache.getSpriteFrame("whiteDot.png"));
                        this.setColor(cc.color(136, 154, 13));
                        this.setPosition(cc.p(index * 10 * scaleFactor, 0));
                        this.setScale(0.5);
                        this.setOpacity(0);
                    },
                    appear: function (bool) {
                        bool ? this.runAction(cc.spawn(cc.scaleTo(0.3, 0.5, 0.5), cc.fadeIn(0.3))) :
                            this.runAction(cc.spawn(cc.scaleTo(0.3, 0.4, 0.4), cc.fadeOut(0.3)));
                    }
                });

                var index = 0;
                while (index < 5) {
                    var tmpDot = new dot(index);
                    this.addChild(tmpDot, 2);
                    this._dotArray.push(tmpDot);
                    index++;
                }
                this.initOfferAnimation();

                return true;
            },
            initOfferAnimation: function () {
                var index = 0;
                var direction = true;
                var time = 100;
                this.timeOutInstance = 0;
                var animation = function () {
                    this.timeOutInstance = setTimeout(function () {
                        var item = this._dotArray[index];
                        if (direction) {
                            item.appear(true);
                        } else {
                            item.appear(false);
                        }
                        index++;
                        if (index >= this._dotArray.length) {
                            direction = !direction;
                            index = 0;
                            time = 300
                        } else {
                            time = 100
                        }
                        if (!this._stopAnimation) {
                            animation();
                        }
                    }.bind(this), time);
                }.bind(this);
                animation();
            }
        });
        //_stopAnimation
        this.offerDot1 = new DotsNode();
        this.offerDot1.setPosition(cc.p(290 * scaleFactor, this._deckInitialPosition.y));
        this.addChild(this.offerDot1, 2);
        var self = this;
        setTimeout(function () {
            self.offerDot2 = new DotsNode();
            self.offerDot2.setPosition(cc.p(self.width - 350 * scaleFactor, self._deckInitialPosition.y));
            self.addChild(self.offerDot2, 2);
        }, 600);
    },
    initHandArrow: function () {

        var hand = new cc.Sprite(spriteFrameCache.getSpriteFrame("HandArrow.png"));
        hand.setPosition(cc.p(382 * scaleFactor, 320 * scaleFactor));
        this.addChild(hand, 2);
        var downward = cc.moveBy(0.4, 0, -27);
        var upward = cc.moveBy(0.4, 0, 27);
        hand.runAction(cc.sequence(downward, upward).repeatForever());
    },
    initMouseListener: function () {
        var cardListenerMargin = 80 * scaleFactor;//original height of isometric card is 91
        this.cutDeckMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            swallowTouches: true,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var targetSize = target.getContentSize();
                var rect = cc.rect(0, 0, targetSize.width, targetSize.height - cardListenerMargin);
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    this.rearrangeCards(target.getTag());
                    return true;
                }
                return false;
            }.bind(this)
        });
        this.cutDeckMouseListener.retain();
    },
    initTouchListener: function () {
        var cardListenerMargin = 80 * scaleFactor;
        this.cutDeckTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height); // - cardListenerMargin
                if (cc.rectContainsPoint(targetRect, locationInNode) && target.cardNo > 0 && target.cardNo < 13) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                this.sendCutReq(this.selectedCard);
                // this.shuffleDeck(this.selectedCard * 4);
            }.bind(this)
        });
        this.cutDeckTouchListener.retain();
    },
    addListener: function () {
        for (var i = 0; i < this._cardArray.length; i++) {
            cc.eventManager.addListener(this.cutDeckMouseListener.clone(), this._cardArray[i]);
            cc.eventManager.addListener(this.cutDeckTouchListener.clone(), this._cardArray[i]);
        }
    },
    pauseListener: function (bool) {
        this.timeOutManager.unScheduleInterval("cutDeckTimer");
        for (var i = 0; i < this._cardArray.length; i++) {
            if (bool) {
                cc.eventManager.pauseTarget(this._cardArray[i], true);
            } else {
                cc.eventManager.resumeTarget(this._cardArray[i], true);
            }
        }
    },
    removeListener: function () {
        cc.eventManager.removeListener(this.cutDeckMouseListener, true);
        cc.eventManager.removeListener(this.cutDeckTouchListener, true);
    },
    rearrangeCards: function (tag) {
        var extraSpaceY = 0, y = this._deckInitialPosition.y;
        for (var i = this._cardArray.length - 1; i >= 0; i--) {
            if ((tag != this._cardArray.length - 1 ) && i == tag) {
                this.selectedCard = tag;
                y -= 10;
            }
            this._cardArray[i].setPositionY(y -= this.cardDistance);
        }
    },
    checkGamePaused: function () {
        this.afterDeckCut();
    },
    shuffleDeck: function (tag) {
        var self = this;
        if (!cc.game._paused) {
            if (tag > 52) {
                this.pauseListener(!0);
                setTimeout(function () {
                    self.afterDeckCut();
                }, 1200);
                return;
            }
            this.checkGameInt = setTimeout(function () {
                self.checkGamePaused();
            }, 1300);
            tag = parseInt(tag / 4);
            if (tag < this._cardArray.length) {
                this.pauseListener(!0);
                var isComplete = !1, dx = 0.4, cardMarginY = 10;

                if (!this.turnBool) {
                    cardMarginY = 0
                }
                var pos = ((this.noOfCard - tag) * this.cardDistance) + (tag * this.cardDistance) + cardMarginY;

                for (var i = this._cardArray.length - 1; i > tag; i--) {
                    this._cardArray[i].runAction(cc.sequence(
                        cc.moveBy(dx, 60, -50).easing(cc.easeCubicActionOut()),
                        cc.moveBy(dx, 0, -pos).easing(cc.easeCubicActionOut()),
                        cc.callFunc(function () {
                            var lowestCardZ = 15;
                            for (var i = tag + 1; i < this._cardArray.length; i++) {
                                this._cardArray[i].setLocalZOrder(lowestCardZ++);
                            }
                        }, this),
                        cc.moveBy(dx, -60, 50.5).easing(cc.easeCubicActionOut()),
                        cc.callFunc(function () {
                            if (!isComplete) {
                                isComplete = !0;
                                this.afterDeckCut();
                            }
                        }, this)));
                }
            }
        } else {
            setTimeout(function () {
                self.afterDeckCut();
            }, 1200);
        }
    },
    afterDeckCut: function () {
        window.clearTimeout(this.checkGameInt);
        if (this.gameType.indexOf("Point") >= 0 && this._gameRoom._myId == -1 && !this._gameRoom._isWaiting)
            this._gameRoom.enableAllChairs();
        this._gameRoom.handleCutDeckAnimCompl();
        this.removeFromParent(true);
    },
    sendCutReq: function (cutNum) {
        this.pauseListener(!0);
        var obj = {};
        obj[SFSConstants.FLD_CUTNUM] = cutNum * 4;
        this._gameRoom.sendReqToServer(SFSConstants.CMD_CUTTHEDECK_REQ, obj);
    },
    initTimer: function (timeOut) {
        var remainingTime = "";
        remainingTime = timeOut;
        this.cutTimer.setString(remainingTime);
        var self = this;
        this.timeOutManager.scheduleInterval(function () {
            --timeOut;
            if (timeOut > 0) {
                remainingTime = timeOut;
                self.cutTimer.setString(remainingTime);
            } else if (timeOut == 1) {
                self.pauseListener(!0);
            }
        }, 1000, "cutDeckTimer");
    },
    stopAllTimer: function () {
        this.timeOutManager.unScheduleInterval("cutDeckTimer");
        this.offerDot1._stopAnimation = !0;
        this.offerDot2._stopAnimation = !0;
    },
    onExit: function () {
        this.stopAllTimer();
        this.removeListener();
        this._super();
        this.release();
    }
}), CircularProfile = cc.Sprite.extend({
    _name: null,
    ctor: function (pos, name) {
        this._super(spriteFrameCache.getSpriteFrame("CircleOuter.png"));
        this._name = name;
        this.setPosition(pos);

        var stencil = new cc.DrawNode();
        stencil.drawCircle(cc.p(0, 0), 25, 0, 50, false, 2, cc.color(0, 0, 0));

        var circle = new cc.Sprite(spriteFrameCache.getSpriteFrame('CircleMask.png'));
        circle.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(circle, 2);

        this.clipper = new cc.ClippingNode();
        this.clipper.setPosition(cc.p(this.width / 2, this.height / 2));
        this.clipper.setStencil(stencil);
        this.addChild(this.clipper, 5);

        this.name = new cc.LabelTTF(name, "RobotoRegular", 12 * 2 * scaleFactor);
        this.name.setScale(0.5);
        this.name.setPosition(cc.p(this.width / 2, -12 * scaleFactor));
        this.addChild(this.name, 1);
    },
    getAvatarScaleFactor: function (avatar) {
        var tmpSize = avatar.getContentSize();
        var mask = spriteFrameCache.getSpriteFrame("CircleMask.png").getRect();
        return {
            x: (mask.width + 3) / tmpSize.width,
            y: (mask.height + 3) / tmpSize.height
        };
    },
    setProfilePic: function (imageData) {
        if (imageData) {
            this.processMask(imageData);
        } else {
            this.removeProfilePic();
        }
    },
    processMask: function (imageData) {
        var spriteAvatar = new cc.Sprite(imageData);
        spriteAvatar.width = imageData.width;
        spriteAvatar.height = imageData.height;
        var scaleFactor = this.getAvatarScaleFactor(spriteAvatar);
        spriteAvatar.setScale(scaleFactor.x, scaleFactor.y);
        this.clipper.addChild(spriteAvatar);
    },
    removeProfilePic: function () {
        var spriteAvatar = new cc.Sprite(spriteFrameCache.getSpriteFrame("ProfileImage.png"));
        var scaleFactor = this.getAvatarScaleFactor(spriteAvatar);
        spriteAvatar.setScale(scaleFactor.x, scaleFactor.y);
        this.clipper.addChild(spriteAvatar);
    }
});