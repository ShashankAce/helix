"use strict";
var AllTournamentRoomList = ccui.Layout.extend({
    _name: "AllTournamentRoomList",
    ctor: function (widthArray, allTournamentRoomArr, context) {
        this._super();
        this._selectedColor = cc.color(223, 230, 182);
        this.setClippingEnabled(true);
        this.setContentSize(760 * scaleFactor, 30 * scaleFactor);
        this.setTag(1);

        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.setBackGroundColor(cc.color(243, 232, 217));

        var label = new cc.LabelTTF("All Tournaments", "RobotoBold", 23 * scaleFactor);
        label.setScale(0.65);
        label.setColor(cc.color(0, 0, 0));
        label.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(label, 2);
    }
});