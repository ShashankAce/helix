/**
 * Created by stpl on 3/30/2017.
 */
"use strict";
var MiddleBaseGame = BaseGame.extend({
    isSeatRotated: null,
    _notfMsgStr: null,
    isCutDeckRespHandled: null,
    shiftedPositionByLength: null,
    ctor: function (room) {
        this._super(room);
        this.isSeatRotated = false;
        this._notfMsgStr = "";
        this.isCutDeckRespHandled = false;
        this.shiftedPositionByLength = 0;
        //this class is used as middleware between tournament and normal game
        return true;
    },
    initHeader: function () {

        this.gameInfoSprite = new GameInfoSprite("gameInfoBtn.png", "gameInfoSprite", this._serverRoom, this);
        this.gameInfoSprite.setAnchorPoint(0, 0.5);

        this._gameInfoTF = new cc.LabelTTF("", "RobotoBold", 22 * scaleFactor, cc.TEXT_ALIGNMENT_CENTER);
        this._gameInfoTF.setScale(0.5);
        this._gameInfoTF.setAnchorPoint(0, 0.5);

        var middle = 500;
        this._gameInfoTF.setPosition(cc.p(middle, this.height - 12.5 * scaleFactor));
        this.addChild(this._gameInfoTF, GameConstants.headerZorder);

        this.gameInfoSprite.setPosition(cc.p(middle, cc.winSize.height - this.gameInfoSprite.height / 2));
        this.addChild(this.gameInfoSprite, GameConstants.headerZorder, "gameInfoSprite");

        this.watchingTxt.setString(this._notfMsgStr);
        this.watchingTxt.setVisible(false);

        cc.eventManager.addListener(this.baseGameMouseListener.clone(), this.gameInfoSprite);
        cc.eventManager.addListener(this.baseGameTouchListener.clone(), this.gameInfoSprite);

        this.updateHeader();
    },
    updateHeaderPosition: function () {
        var middle = (cc.winSize.width - this._gameInfoTF.width / 2 - this.gameInfoSprite.width) / 2;
        this._gameInfoTF.setPosition(cc.p(middle, this.height - 12.5 * scaleFactor - yMargin));
        this.gameInfoSprite.setPosition(cc.p(middle + this._gameInfoTF.width / 2 + 5 * scaleFactor, cc.winSize.height - this.gameInfoSprite.height / 2 - 1 * scaleFactor));
    },
    playerConnect: function (dataObj) {
        if (!this._serverRoom)
            return;
        var cardsStr;
        if (this._serverRoom.getVariable(SFSConstants.OPEN_DECK_CARD))
            cardsStr = this._serverRoom.getVariable(SFSConstants.OPEN_DECK_CARD).value;
        this._userCardPanel._isShowReqSent = false;
        this._currentRoomState = this._serverRoom.getVariable(SFSConstants._ROOMSTATE).value;
        if (this._myId == parseInt(dataObj["playerConnect"])) {
            this._tabMc.subMiniTable.extraMC.setVisible(false);
            this._userCardPanel.meldConfirmPopUp && this._userCardPanel.meldConfirmPopUp.removeFromParent(true);

            if (this._currentRoomState == SFSConstants.RS_RESULT)
                this._isPlayerConnect = true;
            else
                this._isPlayerConnect = false;

            if (dataObj["extraTimeType"] == SFSConstants.ETT_MELDEXTRATIMER)
                this._extraTimeToggle = true;

            if (this._currentRoomState == SFSConstants.RS_WATCH) {
                this.initClosedCards();
                this.arrangeClosedCard();
                if (this._hasDistributionComp)
                    this.deckPanel.hideAllItems(false);
                if (this._connectCount == 1 || this._connectCount == 0)
                    this.scheduleWaitTime(dataObj["extraTimeType"], parseInt(dataObj["timer"]));
                if (this._connectCount == 2)							// timer  comes from player connect
                    this.handleCurrentTurn(parseInt(dataObj["timer"]), dataObj["extraTimeType"]);
            }
            else {
                if (this._currentRoomState != SFSConstants.RS_JOIN)
                    this.scheduleWaitTime(dataObj["extraTimeType"], parseInt(dataObj["timer"]));
            }
            this._connectCount = 0;
            if (dataObj["od"] && dataObj["od"] != cardsStr) {
                var roomVars = [];
                roomVars.push(new SFS2X.Entities.Variables.SFSRoomVariable(SFSConstants.OPEN_DECK_CARD, dataObj["od"]));
                sfs.send(new SFS2X.Requests.System.SetRoomVariablesRequest(roomVars));
            }
            if (this._currentTurnID != this._myId) {
                this.deckPanel.openDeck.setOpenDeckCards();
            }
            if (dataObj["extraTimeType"] && (this._currentRoomState == SFSConstants.RS_WATCH || this._currentRoomState == SFSConstants.RS_RESULT)) {
                this._isExtraTurnTime = true;
                this._tabMc.subMiniTable.extraMC.setVisible(this._isExtraTurnTime);
                if (this._tabMc.iconExtraMC)
                    this._tabMc.iconExtraMC.setVisible(this._isExtraTurnTime);
            }
        }
        else {
            this.handlePlyrConnect(dataObj["playerConnect"]);
        }
    },

    rotateSeats: function () {
        this.rearrangeSeatsBySelf();
    },
    handlePlyrConnect: function (id) {
        var seatObj = this.getSeatObjByUser(id);
        if (seatObj && seatObj._seatPosition != -1) {
            seatObj.isDisconnected = false;
            seatObj.updateProfileIcon();
            seatObj.removeIcon();
            if (seatObj._playerName != rummyData.nickName)
                this.showNotification(seatObj._playerName + " is now connected !");
        }
    },
    initNewRound: function (dataObj) {
        this.isCutDeckRespHandled = false;
        this._dropped = false;
        this._currentTurnID = -2;
        this._isShowDistribution = true;
        this._stopAllTimer = false;
        this._isPlayerConnect = false;
        this._extraTimeToggle = false;
        this._leftPlArr = [];
        this.openCard = "";
        this._14Card = "";
        this._hasDistributionComp = false;
        this.deckPanel.wildcard.removeToolTip();
        this.WrongShowMsg && this.WrongShowMsg.removeWrongShowLayer();
        this.result && this.result.removeFromParent(true);
        this.autoPlayPopup && this.autoPlayPopup.removeAutoPlayLayer();
        this.waitingMC && this.waitingMC.setString("");
        this.waitingMC && this.waitingMC.setVisible(false);
        this.pickWaitMC.setVisible(false);
        this.messagePopUp && this.messagePopUp.removeFromParent(true);
        this.settingPanel.setVisible(false);
        this.meldingPanel && this.meldingPanel.removeFromParent(true);
        this.showValidity && this.showValidity._init();
        this.result && this.result.removeFromParent(true);

        this.processCutTheDeck(dataObj);

        // if (this._myId != -1 && !this._isWaiting && !this._isWatching)
        this._userCardPanel.initPanel();
        this._userCardPanel.hideAllItems(true);

        if (this._isWaiting && this._currentRoomState == SFSConstants.RS_WATCH) {
            this.deckPanel.initPanel();
            this.deckPanel.hideAllItems(false);
        } else {
            this.deckPanel.initPanel();
            this.deckPanel.hideAllItems(true);
        }

    },
    freeSeatsInDummyRoom: function () {
        for (var i = 0; i < this._playerArray.length; i++)
            this.removeChild(this._playerArray[i]);
        this._playerArray = [];
    },
    disableAllChairs: function () {
        for (var i = 0; i < this._playerArray.length; i++) {
            this._playerArray[i].hover(false);
            this._playerArray[i].pauseListener(true);
        }
    },
    enableAllChairs: function () {
        for (var i = 0; i < this._playerArray.length; i++) {
            if (this._playerArray[i]._sittingStatus == false)
                this._playerArray[i].pauseListener(false);
        }
    },
    processStopTimer: function () {
        this._leaveAllowed = true;
        this.stopWaitTimer();
    },
    enableAutoPlay: function () {
        this.deckPanel.disable();
        if (!this.autoPlayPopup) {
            if (this._userCardPanel.cardStack.length > this.totCards) {
                this._userCardPanel.dropVisible();
                this._userCardPanel._cardToShow = null;
                this._userCardPanel.settleDownSelectedCard(false);
                this._userCardPanel.showBtn.hide(true);
                this._userCardPanel.showCard.hide(true);
            }
            this.showAutoPlayMessage();
            this._userCardPanel.discardBtn.hide(true);
            this._userCardPanel.DropBtn.hide(true);
            this._userCardPanel._discardClicked = false;

            if (applicationFacade._currScr == this) {
                this._tabMc.setIcon(1);
                this._tabMc.startGlow(1);
            }
            else {
                this._tabMc.setIcon(3);
                this._tabMc.startGlow(3);
            }
        }
    },
    pauseListenerOnPlayer: function (seatedPls) {
        for (var k = 0; k < this._playerArray.length; k++) {
            if (this._playerArray[k]._seatPosition == parseInt(seatedPls)) {
                this._playerArray[k].pauseListener(true);
                break;
            }
        }
    },
    setRoundNo: function (num) {
        this._currentRound = num;
    },
    processDisableLeave: function () {
        cc.log("processDisableLeave", this._myId);
        this.disableLeave();
        var obj = {};
        if (this._myId != -1)
            this.sendReqToServer(SFSConstants.CMD_READY, obj);
    },
    disableLeave: function () {
        this._leaveAllowed = false;
        this.leaveTablePopup && this.leaveTablePopup.removeFromParent(true);
    },
    processAvatar: function (params) {
        if (params[SFSConstants.DATA_STR]) {
            var avatarArr = params[SFSConstants.DATA_STR].split(",");
            for (var a = 0; a < avatarArr.length; a++) {
                var info = avatarArr[a].split("#");
                var plId = info[0];
                var url = info[1];
                var seatProf = this.getSeatObjByUser(plId);
                if (seatProf && seatProf._sittingStatus) {
                    seatProf.setProfilePic(url);
                }
            }
        }
    },
    thumbnailBlink: function () {
        if (this != applicationFacade._currScr && !this.autoPlayPopup) {
            this._tabMc.setIcon(this.totCards == 13 ? 4 : 5);
            this._tabMc.iconExtraMC && this._tabMc.iconExtraMC.setVisible(this._isExtraTurnTime || this._extraTimeToggle);
            if (applicationFacade._currScr == this)
                this._tabMc.startGlow(1);
            else
                this._tabMc.startGlow(3);

        }
        if (this._tabMc._iconImage && this._tabMc.timerTxt && this._userCardPanel._isVisible && this._waitTimer) {
            var remainingTime = this._waitTimer.repeatCount - this._waitTimer.currentCount;
            this._tabMc.timerTxt.setString(remainingTime);
        }
    },
    wildCard: function (wildCardName) {
        this._wildCardStr = wildCardName;
        this.deckPanel.wildcard.initJoker(wildCardName, this);
    },
    ODCards: function (odcards) {
        var cardEaseTime = 0.20;
        var str = this._serverRoom.getVariable(SFSConstants._DISCARD).value;
        if (str) {
            this.discardPanel && this.discardPanel.initDiscardPanel(str);
            if (this._currentTurnID != this._myId) {
                var seatObj = this.getSeatObjByUser(this._currentTurnID);
                if (seatObj) {
                    var closedCardObj = this.getClosedCardById(seatObj._seatPosition);
                    var closedCardPos = closedCardObj.getPosition();
                    var openDeckPos = this.deckPanel.openDeck.getPosition();
                    var cardsArr = odcards.split(",");
                    if (cardsArr[0] && cardsArr[0] != "null" && cardsArr[0] != "" && typeof cardsArr[0] == "string") {
                        var cardSpr = new CardModel(cardsArr[0]);
                        cardSpr.setPosition(closedCardPos);
                        cardSpr.setScale(0.4);
                        this.deckPanel.addChild(cardSpr, 10);
                        if (cc.game._paused) {
                            this.deckPanel.openDeck.setOpenDeckCards(odcards);
                            cardSpr.removeFromParent(true);
                            this.deckPanel._animComp = true;
                        } else {
                            // setting card to open deck
                            // setting discarded card by other players
                            cardSpr.runAction(cc.sequence(
                                cc.spawn(
                                    cc.moveTo(cardEaseTime, openDeckPos),
                                    cc.scaleTo(cardEaseTime, GameConstants.openDeckCardScaleValue, GameConstants.openDeckCardScaleValue)
                                ),
                                cc.callFunc(function () {
                                    this.deckPanel.openDeck.setOpenDeckCards(odcards);
                                    cardSpr.removeFromParent(true);
                                    this.deckPanel._animComp = true;
                                }, this))
                            );
                        }
                    }
                } else this.deckPanel.openDeck.setOpenDeckCards(odcards);
            } else this.deckPanel.openDeck.setOpenDeckCards(odcards);
        } else
            this.deckPanel.openDeck.setOpenDeckCards(odcards);
        var that = this;
        setTimeout(function () {
            that.deckPanel._animComp = true;
        }, 200)
    },
    updateDiscardHistory: function () {
        if (!this.isDiscardHistoryAvailable) {
            if (this.checkIfDiscardHistoryAvailable()) {
                this.setDiscardHistoryAvailable(true);
            }
        }
    },
    checkIfDiscardHistoryAvailable: function () {
        var discard = this._serverRoom.getVariable[SFSConstants._DISCARD];
        if (discard != null) {
            var discardStr = discard.value;
            if (discardStr != null) {
                var usersDiscardCards = discardStr.split(";");
                for (var i = 0; i < usersDiscardCards.length; i++) {
                    var userCardDetails = usersDiscardCards[i].split(":");
                    if (userCardDetails.length > 1 && userCardDetails[1] != null) {
                        return true;
                    }
                }
            }
        }
        return false;
    },
    setDiscardHistoryAvailable: function (isAvailable) {
        this.isDiscardHistoryAvailable = isAvailable;
    },
    scheduleWaitTime: function (str, wtimer) {
        this.stopWaitTimer();
        var waitTime;
        if (wtimer == -2 || wtimer == undefined)
            waitTime = this._serverRoom.getVariable(SFSConstants._WAITTIME).value;
        else
            waitTime = wtimer;
        waitTime = parseInt(waitTime);
        this._currentRoomState = this._serverRoom.getVariable(SFSConstants._ROOMSTATE).value;
        if (str == SFSConstants.ETT_MELDEXTRATIMER) {
            cc.log("In setting meld extra timer");
            this._extraTimeToggle = true;
        }
        if (str == "" || str == undefined)
            str = null;
        if (!str && this._currentRoomState == SFSConstants.RS_WATCH && (waitTime > this._serverRoom.getVariable(SFSConstants._TURNTIME).value))
            waitTime = this._serverRoom.getVariable(SFSConstants._TURNTIME).value;

        if (waitTime > 1) {
            if (this._currentTurnSeat)
                this._currentTurnSeat.startTimer(str != null);
            //todo this._currentTurnSeat bug for any senario

            this._waitTime = waitTime;
            this._waitTimer = {};
            this._waitTimer.repeatCount = waitTime;
            this._waitTimer.currentCount = 0;

            this.ccTimeOutManager.scheduleInterval(this.waitTimerhandler.bind(this), 1000, 0, "waitTimer");
            this.ccTimeOutManager.scheduleTimeOut(this.stopWaitTimer.bind(this), waitTime * 1000, "stopWaitTimer");
        }
    },
    waitTimerhandler: function (waitTime) {
        if (this._waitTimer) {
            var remainTime = this._waitTimer.repeatCount - this._waitTimer.currentCount;
            this._waitTimer.currentCount += 1;
            this.showWaitTimer(remainTime);
            if (remainTime == 0) {
                this.stopWaitTimer();
            }
        }
    },
    stopWaitTimer: function () {
        this.ccTimeOutManager.unScheduleInterval("waitTimer");
        this.ccTimeOutManager.unScheduleTimeOut("stopWaitTimer");
        if (this._tabMc.timerTxt)
            this._tabMc.timerTxt.setString("");
    },
    setTabTimer: function (time) {
        if (this._tabMc) {
            if (this._tabMc._iconImage && this._tabMc.timerTxt && this._userCardPanel._isVisible)
                this._tabMc.timerTxt.setString(time.toString());
            if (this._tabMc.subMiniTable && this._hasDistributionComp)
                this._tabMc.subMiniTable.setTimer(time.toString());
        }
    },
    setMeldStatus: function (dataObj) {
        this.showValidity && this.showValidity.hide(true);
        var str = dataObj["str"];
        var msgStr = "";

        var seatObj = this.getSeatObjByUser(this._myId);

        if (!this._dropped && !this._isWatching && (this._myId != -1) && (seatObj && seatObj._seatPosition != -1 && !seatObj.isWrongShow)) {
            if (str == "0") {
                this._showMeld = true;
                this._userCardPanel.setVisible(true);
                this.meldingPanel && this.meldingPanel.setVisible(true);
                if (this._currentTurnID != this._myId) {
                    msgStr = this.getPlayerNameById(this._currentTurnID) + " has placed a show. Please group your cards into valid sequences/sets and click on the Declare button.";
                    this.meldingPanel.setMeldMsg(3, msgStr);
                }
                else {
                    this.meldingPanel.setMeldMsg(1, "");
                }
                if (dataObj["meldTimer"])
                    this.scheduleWaitTime(SFSConstants.ETT_MELDEXTRATIMER, dataObj["meldTimer"]);
            }
            else {
                this._showMeld = false;
                this.meldingPanel.setVisible(true);
                this._userCardPanel.hideAllItems(true);
                this._userCardPanel.clearCards();
                this.meldingPanel.declareBtn.hide(true);
                this.meldingPanel.removeJoker();
                if (str == "1") {
                    msgStr = this.getPlayerNameById(this._currentTurnID) + " has placed a show. Please wait while your opponents are melding.";
                    this.meldingPanel.setMeldMsg(6, "");
                    this.meldingPanel.setMeldMsg(3, msgStr);
                    this.meldingPanel.declareBtn.hide(true);
                    this.meldingPanel.startGlow(false);
                }
                else if (str == "2") {
                    if (this._extraTimeToggle)
                        this.meldingPanel.setMeldMsg(4, msgStr);
                    else
                        this.meldingPanel.setMeldMsg(2, msgStr);
                }
                if (this._currentTurnSeat) {
                    this._tabMc.subMiniTable.turnIndicator.setVisible(false);
                    this._tabMc.subMiniTable.extraMC.setVisible(false);
                    this._tabMc.subMiniTable.setTimer("");
                    this._currentTurnSeat.hideTimer();
                    this._currentTurnSeat = null;
                }
            }
        }
        else {
            this.meldingPanel && this.meldingPanel.removeFromParent(true);
            this._userCardPanel.hideAllItems(true);
            this._userCardPanel.clearCards();
            this.initMessagePop();
            this.messagePopUp.setMsg("Melding in progress!!!\nPlease wait while players are melding... ");
        }
    },
    getSelfPlayer: function () {
        for (var i = 0; i < this._playerArray.length; i++) {
            if (this._playerArray[i]._playerId == this._myId) {
                return this._playerArray[i];
            }
        }
    },
    parseSeat: function (dataObj, room) {

        var playerString = dataObj["plsStr"];
        this._currentRound = parseInt(dataObj[SFSConstants.DATA_ROUNDNO]);

        if (playerString != '') {
            var playerDetailArray = playerString.split(",");
            this._totalSeats = room.getVariable(SFSConstants._MAXPLAYERS).value;

            for (var i = 0; i < playerDetailArray.length; i++) {

                var details = playerDetailArray[i].split(":");
                this.pauseListenerOnPlayer(details[0]);

                // seatId, playerId, playerName, playerScre, isDropped, isDisconnected, isAutoPlay, isWrongShow, isWaiting
                this._playerArray[parseInt(details[0] - 1)].setName(details[2]);
                this._playerArray[parseInt(details[0] - 1)].updateInfo(
                    details[0],
                    details[1],
                    details[2],
                    details[3],
                    details.length > 4 && (details[4] == "true"),
                    details.length > 5 && (details[5] == "true"),
                    details.length > 6 && (details[6] == "true"),
                    details.length > 7 && (details[7] == "true"),
                    details.length > 8 && (details[8] == "true"));

                this._playerArray[parseInt(details[0] - 1)].takeSeat(details[1], details[2], details[3]);

                if (room.getVariable(SFSConstants._ROOMSTATE).value != SFSConstants.RS_JOIN) {
                    this._playerArray[parseInt(details[0] - 1)].updateScore(details[3].toString());
                }

                var dropped = (details[4] == "true");
                var disconnected = (details[5] == "true");
                var autoPlay = (details[6] == "true");
                var wrngShw = (details[7] == "true");
                var _isNext = (details[8] == "true");

                var isMyautoPl = false;

                if (details[2] == rummyData.nickName) {

                    this._myId = parseInt(details[1]);
                    applicationFacade._myId = this._myId;
                    isMyautoPl = (details[6] == "true");
                    this._dropped = (details[4] == "true");
                    this._isWatching = false;
                    this.selfSeatPosition = parseInt(details[0]);
                    this.shiftedPositionByLength = this.noOfPlayer - this.selfSeatPosition;
                    this.disableAllChairs();
                    if (wrngShw) {
                        this._dropped = true;
                        this.showNotification("You have placed a wrong show");
                        this.showWrongShow();
                    }
                    if (this._isNext) {
                        this._myId = -1;
                        this.waitingMC.setString("Please wait, While next game start.");
                        this.waitingMC.setVisible(true);
                        this.showNotification();
                        this._tabMc.setIcon(1);
                        this.instantPlayEnable();
                    }
                }
                this.updatePlayerInfo(KHEL_CONSTANTS.USER_ICON_UPDATE, this._playerArray[parseInt(details[0] - 1)], dropped, disconnected, autoPlay, wrngShw, _isNext);
            }
            return isMyautoPl;
        }
    },
    instantPlayEnable: function () {
        this.instantNotMC && this.instantNotMC.removeFromParent(true);
        this.instantNotMC = new cc.Node();
        this.addChild(this.instantNotMC, 3);

        if ((this._myId != -1 && !this._isWatching) || this._isWaiting)
            return;

        var instantNotTxt = new cc.LabelTTF("Click 'Instant Play' to directly join a table.", "RupeeFordian", 30 * scaleFactor, cc.TEXT_ALIGNMENT_CENTER);
        instantNotTxt.setPosition(cc.p(this.width / 2, this.height / 2 - 85 * scaleFactor - yMargin));
        instantNotTxt.setScale(0.5);
        instantNotTxt.setColor(cc.color(177, 194, 183));
        this.instantNotMC.addChild(instantNotTxt, 2);

        this.insantPlayBtn = new CreateBtn("InstantPlayGame", "InstantPlayGameOver", "insantPlayBtn", cc.p(this.width / 2, this.height / 2 - 125 * scaleFactor));
        this.instantNotMC.addChild(this.insantPlayBtn, 3, "insantPlayBtn");

        cc.eventManager.addListener(this.baseGameMouseListener.clone(), this.insantPlayBtn);
        cc.eventManager.addListener(this.baseGameTouchListener.clone(), this.insantPlayBtn);

    },
    processToss: function (data) {
        this.isCutDeckRespHandled = false;
        this._isShowDistribution = true;
        var tossWinnerPlayerId;
        var tossCards = data[SFSConstants.DATA_TOSSSTR].split(SFSConstants.SEMICOLON_DELIMITER);
        var playerInfo = {};

        var _info = tossCards[0].split(SFSConstants.COLON_DELIMITER);
        tossWinnerPlayerId = _info[0];
        for (var i = 0; i < tossCards.length; i++) {
            var info = tossCards[i].split(SFSConstants.COLON_DELIMITER);
            playerInfo[info[0]] = {
                "tossCard": info[1].split(SFSConstants.COMMA_DELIMITER)[0],
                "seatPosition": info[1].split(SFSConstants.COMMA_DELIMITER)[1]
            };
            if (info[0] == ("" + this._myId))
                this.selfSeatPosition = parseInt(info[1].split(SFSConstants.COMMA_DELIMITER)[1]);
        }

        var isWaitingBool;
        if (((this._serverRoom.getVariable(SFSConstants._GAMETYPE).value).indexOf("Point")) >= 0) {
            isWaitingBool = this._isWaiting || (this._myId == -1);
        } else {
            isWaitingBool = (this._myId == -1) ? true : "";
        }

        this.rearrangeSeatsBytoss(playerInfo);
        this.showTossCard(playerInfo, tossWinnerPlayerId, isWaitingBool);
        if (this._myId == -1)
            this._leaveAllowed = true;
        else
            this._leaveAllowed = false;

    },
    showTossCard: function (playerInfo, tossWinnerPlayerId, isWaitingBool) {
        this.tossLayer = new TossLayer(this, playerInfo, tossWinnerPlayerId, isWaitingBool);
        this.addChild(this.tossLayer, GameConstants.tossZorder);
    },
    processCutTheDeck: function (data) {

        this.setRoundNo(parseInt(data[SFSConstants.DATA_ROUNDNO]));
        var gameType = this._serverRoom.getVariable(SFSConstants._GAMETYPE).value;

        if (gameType.indexOf("Point") >= 0) {
            if (!this._isWaiting)
                this.rearrangeSeatsBySelf();

        } else {
            if (this._currentRound == 1)
                this.rearrangeSeatsBySelf();
        }


        var userName, opponentName, turnBool = false;
        var playerImg, dealerImg;
        var playerId = data[SFSConstants.DATA_STR].split(SFSConstants.HASH_DELIMITER);
        this.tossLayer && this.tossLayer.removeTossLayer(true);

        var time = 5;
        if (this._serverRoom != null) {
            var timeKey = this._serverRoom.variables[SFSConstants._WAITTIME];
            if (timeKey != null) {
                time = timeKey.value;
            }
        }

        for (var i = 0; i < this._playerArray.length; i++) {
            if (parseInt(this._playerArray[i]._playerId) == playerId[0]) {
                userName = this._playerArray[i];
            } else if (parseInt(this._playerArray[i]._playerId) == playerId[1]) {
                opponentName = this._playerArray[i];
            }
        }

        if (parseInt(playerId[1]) == this._myId && this._myId != -1) {
            turnBool = true;
        }

        if (!userName) {
            throw new Error('User can not be undefined');
        }

        if (!opponentName) {
            throw new Error('Opponent can not be undefined');
        }


        this.initCutDeckPanel(time, userName, opponentName, turnBool);
    },
    processCutTheDeckResp: function (data) {
        this._canLeaveRoom = true;
        this._leaveAllowed = true;
        var cutNo = parseInt(data[SFSConstants.FLD_CUTNUM]);
        SoundManager.playSound(SoundManager.cutTheDeck, false);
        this.cutDeckPanel && this.cutDeckPanel.shuffleDeck(cutNo);
    },
    handleCutDeckAnimCompl: function () {
        cc.log("HandleCutDeckAnimCompl");
        this.isCutDeckRespHandled = true;
        this._userCardPanel.startDistribution();
    },
    removeExtraPlayers: function () {
        var k = this._playerArray.length;
        while (k--) {
            if (!this._playerArray[k]._sittingStatus) {
                this.removeChild(this._playerArray[k]);
                this._playerArray.splice(k, 1);
            }
        }
    },
    seatConfirmResponse: function (data, room) {
        SoundManager.playSound(SoundManager.jointable, false);
        this.showPlayerNameToolTip(false);
        if (this.waitingMC) {
            this.waitingMC.setString("");
        }
        if (data["isSeatRotate"] && (data["isSeatRotate"] == "Y")) {
            this.isSeatRotated = false;
        }
    },
    getShiftedSeatId: function (sid) {
        // console.log("seat offset: " + this.shiftedPositionByLength);
        var newSid = sid + this.shiftedPositionByLength;
        if (newSid > this.noOfPlayer)
            newSid = newSid - this.noOfPlayer;
        return newSid;
    },
    rearrangeSeatsBySelf: function () {

        if (this._myId == -1) {
            return;
        }

        if (this.selfSeatPosition == 6) {
            return;
        }

        this.shiftedPositionByLength = this.noOfPlayer - this.selfSeatPosition;
        if (this.isSeatRotated == false && this.selfSeatPosition != 0) {
            this.isSeatRotated = true;
            if (this.shiftedPositionByLength != 0) {
                this.showPlayerNameToolTip(false);
                var tmpShiftedPositionByLength;
                for (var j = 0; j < this._playerArray.length; j++) {
                    var newSeatPosition = this._playerArray[j]._seatPosition + this.shiftedPositionByLength;
                    if (newSeatPosition > this.noOfPlayer) {
                        tmpShiftedPositionByLength = newSeatPosition - this.noOfPlayer;
                        this._playerArray[j]._seatPosition = tmpShiftedPositionByLength;
                    } else {
                        this._playerArray[j]._seatPosition += this.shiftedPositionByLength;
                    }
                }
                this.arrangeSeats(this.noOfPlayer, true);
            }
        }
        this.selfSeatPosition = this.noOfPlayer;
    },
    rearrangeSeatsBytoss: function (playerInfo, bool) {
        cc.log("SeatRotated- TOSS");
        this.showPlayerNameToolTip(false);
        var tmpPlayerArray = [];
        tmpPlayerArray = this._playerArray;


        this._playerArray = [];

        for (var i = 0; i < tmpPlayerArray.length; i++) {
            cc.eventManager.removeListener(tmpPlayerArray[i], true);
            tmpPlayerArray[i].removeFromParent(true);
            for (var key in playerInfo) {

                if (key == tmpPlayerArray[i]._playerId) {

                    var seatId = parseInt(playerInfo[key]["seatPosition"]);
                    tmpPlayerArray[i]._tossCard = playerInfo[key]["tossCard"];
                    tmpPlayerArray[i]._seatPosition = seatId;
                    tmpPlayerArray[i].seat_id = seatId;

                    console.log("player_name :" + tmpPlayerArray[i].player_name + " seat id: " + tmpPlayerArray[i].seat_id);

                    this._playerArray[seatId - 1] = tmpPlayerArray[i];
                }
            }
        }

        for (var i = 0; i < this.noOfPlayer; i++) {
            var obj = this._playerArray[i];
            if (obj == undefined || !obj) {
                this._playerArray[i] = new Player(this, i);
            } else {
                if (this._myId != -1 && this._myId == this._playerArray[i].player_id) {
                    this.selfSeatPosition = this._playerArray[i]._seatPosition;
                    this.shiftedPositionByLength = this.noOfPlayer - this.selfSeatPosition;
                }
            }

            this._playerArray[i].setPosition(this._chairPosition[this.noOfPlayer][i]);
            this.addChild(this._playerArray[i], GameConstants.playerZorder);
            cc.eventManager.addListener(this.baseGameMouseListener.clone(), this._playerArray[i]);
            cc.eventManager.addListener(this.baseGameTouchListener.clone(), this._playerArray[i]);

        }
    },
    processUserCards: function (data, sourceRoom) {
        var cardStr = data[SFSConstants._CARDSTR];
        this.cardString = cardStr;

        if (this._currentRoomState == SFSConstants.RS_MELD) {
            this.handleDistributionCompl();
            this._userCardPanel.initPanel();
            this._userCardPanel.packCards(cardStr, this._14Card);
            this.meldingPanel = new MeldingPanel(this);
            this.addChild(this.meldingPanel, GameConstants.meldConfirmZorder);
            this.meldingPanel.init(cardStr);
            this.startMelding();
            if (this._showMeld)
                this.meldingPanel.setVisible(true);
            else
                this.meldingPanel.setVisible(false);
            return;
        }
        this._userCardPanel.packCards(cardStr);

        var numOfCard = this._serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;
        if (this._currentTurnID == this._myId) {
            if (parseInt(numOfCard) == 13) {
                if (this._userCardPanel.cardStack.length != 0 && this._userCardPanel.cardStack.length < 14) {
                    this.deckPanel.enable();
                    this.deckPanel.glowEnabled(true);
                }
            } else {
                if (this._userCardPanel.cardStack.length != 0 && this._userCardPanel.cardStack.length < 22) {
                    this.deckPanel.enable();
                    this.deckPanel.glowEnabled(true);
                }
            }
        }
    },
    processExtraTimer: function (dataObj) {

        this._currentRoomState = this._serverRoom.getVariable(SFSConstants._ROOMSTATE).value;
        this._tabMc.subMiniTable.extraMC.setVisible(false);
        if (this._currentRoomState == SFSConstants.RS_WATCH) {
            var seatObj = this.getSeatObjByUser(this._currentTurnID);
            SoundManager.playSound(SoundManager.extratimesound, true);
            this.scheduleWaitTime(SFSConstants.CMD_EXTRATIMER);
            this._isExtraTurnTime = true;
            if (this._myId == this._currentTurnID) {
                if (seatObj && seatObj._seatPosition != -1 && seatObj.isDisconnected)
                    this.sendConnectionOk();
                this.thumbnailBlink();
            }
        }
        else if (this._currentRoomState == SFSConstants.RS_MELD) {
            this._isExtraTurnTime = false;
            SoundManager.playSound(SoundManager.extratimesound, true);
            if (dataObj["extraTimer"])
                this.scheduleWaitTime(SFSConstants.ETT_MELDEXTRATIMER, parseInt(dataObj["extraTimer"]));
            else
                this.scheduleWaitTime(SFSConstants.ETT_MELDEXTRATIMER);
            this.thumbnailBlink();
        }
        this._tabMc.subMiniTable.extraMC.setVisible(this._isExtraTurnTime);

    },
    updateDealer: function (dealerId) {
        for (var i = 0; i < this._playerArray.length; i++) {
            this._playerArray[i].setDealerIcon(false);
        }

        for (var i = 0; i < this._playerArray.length; i++) {
            if (this._playerArray[i]._playerId == dealerId) {
                this._playerArray[i].setDealerIcon(true);
                break;
            }
        }
    },
    showWrongShow: function () {
        if (this.WrongShowMsg) {
            this.WrongShowMsg.removeFromParent(true);
        }
        var WrongShowMsg = cc.LayerGradient.extend({
            ctor: function () {
                this._super(cc.color(0, 0, 0), cc.color(0, 0, 0));
                var size = cc.winSize;
                this.setPosition(cc.p(90 * scaleFactor, 290 * scaleFactor));
                this.setContentSize(cc.size(800 * scaleFactor, 50 * scaleFactor));
                this.setColorStops([{p: 0, color: new cc.Color(0, 0, 0, 0)},
                    {p: .5, color: cc.color.BLACK},
                    {p: 1, color: new cc.Color(0, 0, 0, 0)}]);
                //this.setStartOpacity(255);
                //this.setEndOpacity(0);
                this.setVector(cc.p(-1, 0));

                var msgString = "Wrong Show";
                var autoModeMessage = new cc.LabelTTF(msgString, "RobotoRegular", 16 * scaleFactor);
                autoModeMessage.setColor(cc.color(255, 255, 255));
                autoModeMessage.setPosition(cc.p(this.width / 2, this.height / 2 - yMargin));
                this.addChild(autoModeMessage, GameConstants.tossZorder + 2);

            },
            onExit: function () {
                this._super();
            },
            removeWrongShowLayer: function () {
                this.removeFromParent(true);
            }
        });

        this.WrongShowMsg = new WrongShowMsg();
        this.addChild(this.WrongShowMsg, GameConstants.autoPlayZorder);
    },
    showAutoPlayMessage: function () {
        this.autoPlayPopup && this.autoPlayPopup.removeAutoPlayLayer();

        this.autoPlayPopup = new AutoPlayPopup();
        this.addChild(this.autoPlayPopup, GameConstants.autoPlayZorder, "autoPlayPopup");

        cc.eventManager.addListener(this.baseGameMouseListener.clone(), this.autoPlayPopup.autoModeBackBtn);
        cc.eventManager.addListener(this.baseGameTouchListener.clone(), this.autoPlayPopup.autoModeBackBtn);

    },
    autoModeBackBtnReq: function () {
        var obj = {};
        obj[SFSConstants.FLD_ID] = this._myId;
        this.sendReqToServer(SFSConstants.CMD_BACKTOACTIVE, obj);
    },
    closeResult: function () {
        this.result && this.removeChild(this.result, true);
        this.result = null;
        this.settingPanel.setVisible(false);
    },
    setDealerpos: function (bool) {
        for (var i = 0; i < this._playerArray.length; i++) {
            this._playerArray[i].setDealerIcon(bool);
        }
    },
    processDropResp: function (data) {
        if (data[SFSConstants.FLD_ACTION] == SFSConstants.ACTION_SHOW) {
            if ((this._myId == data.id) && (!this._userCardPanel._cardToShow)) {
                this._userCardPanel.setShowCard(data.card);
            }
            this.handlePlayerShow();
        } else if (data[SFSConstants.FLD_ACTION] == SFSConstants.ACTION_DROP) {
            this.handlePlayerFold("drop", data);
        } else if (data[SFSConstants.FLD_ACTION] == SFSConstants.ACTION_DISCARD) {

            if (this._myId != -1 && !this._userCardPanel._cardToShow) {
                this._userCardPanel.setShowCard(data.card);
            }

            if (this._currentTurnID == this._myId && !this.autoPlayPopup && this._userCardPanel._cardToShow) {
                this._userCardPanel.tweenDiscardCard();
            }
        }
    },
    handlePlayerShow: function () {
        this._userCardPanel.showConfirmPopup && this._userCardPanel.showConfirmPopup.removeFromParent();
        this._selectedCardArray = [];
        this._cardToShow = null;
        SoundManager.removeSound(SoundManager.extratimesound);
        this._isExtraTurnTime = false;
        var groupCards;
        var profObj = this.getSeatObjByUser(this._currentTurnID);
        if (this._currentTurnID != this._myId)
            this.showNotification(profObj.player_name + " has placed a show.");
        if (profObj && profObj._seatPosition != -1) {
            // this.deckPanel.hideAllItems(true);
            if (this._currentTurnID == this._myId) {
                this.pickWaitMC.setVisible(false);
                this._userCardPanel.settleDownSelectedCard();
                this._userCardPanel.checkForExtraCard();
                groupCards = this._userCardPanel.getGroupCardsStr();
                this.meldingPanel = new MeldingPanel(this);
                this.addChild(this.meldingPanel, GameConstants.meldConfirmZorder, "meldingPanel");
                this.meldingPanel.init(groupCards);
                this.meldingPanel.setMeldMsg(1);
                this._userCardPanel.sendGroupCards(groupCards, false);
                this._userCardPanel.showCard.hide(true);
                this.startMelding();
            }
            else {
                if (this._myId != -1 && !this._dropped) {
                    this._userCardPanel.checkForExtraCard();
                    SoundManager.playSound(SoundManager.meldingscreensound, false);
                    groupCards = this._userCardPanel.getGroupCardsStr();
                    this.meldingPanel = new MeldingPanel(this);
                    this.addChild(this.meldingPanel, GameConstants.meldConfirmZorder, "meldingPanel");
                    this.meldingPanel.init(groupCards);
                    var msg = this.getPlayerNameById(this._currentTurnID) + " has placed a show. Please group your cards into valid sequences/sets and click on the Declare button.";
                    this.meldingPanel.setMeldMsg(3, msg);

                    this._userCardPanel.sendGroupCards(groupCards, false);
                    this.startMelding();
                }
                else {
                    if (this._currentTurnSeat) {
                        this._tabMc.subMiniTable.turnIndicator.setVisible(false);
                        this._tabMc.subMiniTable.extraMC.setVisible(false);
                        this._tabMc.subMiniTable.setTimer("");

                        this._currentTurnSeat.hideTimer();
                        this._currentTurnSeat = null;
                    }
                    this.deckPanel.hideAllItems(true);
                    this.initMessagePop();
                    this.messagePopUp.setMsg("Melding in progress!!!\nPlease wait while players are melding... ");
                }
            }
        }
    },
    startMelding: function () {
        if (this._currentTurnSeat) {
            this._tabMc.subMiniTable.turnIndicator.setVisible(false);
            this._tabMc.subMiniTable.extraMC.setVisible(false);
            this._tabMc.subMiniTable.setTimer("");
            this._currentTurnSeat.hideTimer();
            this._currentTurnSeat = null;
        }
        if (!this._dropped && this._myId != -1) {
            SoundManager.playSound(SoundManager.meldingscreensound);
            this._currentTurnSeat = this.getSeatObjByUser(this._myId);
            this._currentTurnSeat.startTimer(this._extraTimeToggle);
            this.thumbnailBlink();
            this._userCardPanel.DropBtn.hide(true);
            this.deckPanel.hideAllItems(true);
        }
        else {
            this.deckPanel.hideAllItems(true);
            this.initMessagePop();
            this.messagePopUp.setMsg("Melding in progress!!!\nPlease wait while players are melding... ");
        }
    },
    sendUpdateTab: function (cmd) {
        if (cmd == undefined)
            cmd = "noTurn";
        if (this._serverRoom && (this._serverRoom.getVariable(SFSConstants._ROOMSTATE).value == SFSConstants.RS_RESULT || this._serverRoom.getVariable(SFSConstants._ROOMSTATE).value == SFSConstants.RS_JOIN))
            cmd = "noTurn";
        if (cmd == "noTurn") {
            if (this._tabMc) {
                this._tabMc.timerTxt && this._tabMc.timerTxt.setString("");
                this._tabMc.subMiniTable.turnIndicator.setVisible(false);
                this._tabMc.subMiniTable.extraMC.setVisible(false);
                if (this._myId == -1 && this._isWatching && (this._serverRoom.getVariable(SFSConstants._ROOMSTATE).value != SFSConstants.RS_JOIN) && !this._isWaiting) {
                    this._tabMc.setIcon(2);
                }
                else
                    this._tabMc.setIcon(1);
                this._tabMc.startGlow(1);
            }
            if (this._currentTurnSeat) {
                this._currentTurnSeat.hideTimer();
                this._currentTurnSeat = null;
            }
            return;
        }
        if (this._currentTurnID == -2)
            return;
        this._tabMc.subMiniTable.extraMC.setVisible(this._isExtraTurnTime);
        if (this._currentTurnSeat)
            this.showThambnailTimeIndicator();
        if (this._myId != -1) {
            if (this._myId == this._currentTurnID) {
                if (this != applicationFacade._currScr && !this.autoPlayPopup) {
                    this._tabMc.setIcon(this.totCards == 13 ? 4 : 5);
                    this._tabMc.subMiniTable.extraMC.setVisible(this._isExtraTurnTime);
                    if (applicationFacade._currScr == this)
                        this._tabMc.startGlow(1);
                    else
                        this._tabMc.startGlow(3);

                }
                else if (this != applicationFacade._currScr && this.autoPlayPopup) {
                    this._tabMc.setIcon(3);
                    this._tabMc.startGlow(3);
                }
                else {
                    this._tabMc.setIcon(1);
                    this._tabMc.startGlow(1);
                }
            }
            else if (this != applicationFacade._currScr && this.autoPlayPopup) {
                this._tabMc.setIcon(3);
                this._tabMc.startGlow(3);
            }
            else {
                this._tabMc.setIcon(1);
                this._tabMc.startGlow(1);
            }
        }
        else {
            if (this._isWatching)
                this._tabMc.setIcon(2);
            else
                this._tabMc.setIcon(1);
            this._tabMc.startGlow(1);
        }
    },
    showThambnailTimeIndicator: function () {
        if (this._currentRoomState == SFSConstants.RS_WATCH) {
            this._tabMc && this._tabMc.subMiniTable.setTurnIndicator(this._serverRoom.getVariable(SFSConstants._MAXPLAYERS).value, this._currentTurnSeat._seatPosition);
        }
    },
    handlePlayerFold: function (type, data) {
        this.messagePopUp && this.messagePopUp.removeFromParent(true);
        // var seatObj = this.getSeatObjByUser(this._currentTurnID);
        var seatObj = this.getSeatObjByUser(data['id']);
        if (seatObj && seatObj._seatPosition != -1) {
            if (type == "drop") {
                SoundManager.playSound(SoundManager.drop, false);
                seatObj.isDropped = true;
                seatObj.setIcon("Dropped", "DropIcon");
            } else if (type == "wrongShow") {
                seatObj.isWrongShow = true;
                seatObj.setIcon("Wrong Show", "wrongShow");
            }

            if (this._currentTurnID == this._myId) {
                this._dropped = true;
                this._userCardPanel.initPanel();
                this._userCardPanel.hideAllItems(true);
                this.deckPanel.disable();
                // pinky
                this.showValidity && this.showValidity.hide(true);
            }
            this.getClosedCardById(seatObj._seatPosition) && this.getClosedCardById(seatObj._seatPosition).setVisible(false);

        }
    },
    processPlrExtraTimer: function (data) {
        var playerId = data[SFSConstants.FLD_PLAYERID];
        var plrtimer = parseInt(data[SFSConstants.CMD_EXTRATIMER]);
        var player = this.getPlayerById(playerId);
        player && player.setRemainingExtraTime(plrtimer);
    },
    getPlayerById: function (plId) {
        function findPlayer(player) {
            return player._playerId == plId;
        }

        return this._playerArray.find(findPlayer);
    },
    sendConnectionOk: function (bool) {
        if (!this._serverRoom)
            return;
        var gameType = this._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
        this._isShowDistribution = bool;
        if (this._connectCount != 2)
            this._connectCount = 1;
        if (this._myId == -1)
            this._connectCount = 0;
        if (!this._isWatching && (gameType.indexOf("Point") >= 0))
            this._connectCount = 0;

        var sentObj = {};
        sentObj["discData"] = applicationFacade._disconnectTime + "";
        this.sendReqToServer(SFSConstants.CMD_CONNECTAGAIN, sentObj);
    },
    leaveBtnHandler: function () {
        if (this._currentRoomState == SFSConstants.RS_JOIN && this.confirmationPopup && this._leaveAllowed) {
            if (this._requestToSever) {
                // Main.callExternalInterface(); weaver update balance call
                // todo
                this.sendReqToServer(SFSConstants.CMD_CLICK_LEAVE_TABLE, {});
                // sfs.send(new SFS2X.Requests.System.LeaveRoomRequest(sfs.getRoomById(this._roomId)));
                this.leaveRoomByMyself();
            }
            return;
        }

        if (!this.leaveTablePopup) {
            this.leaveTablePopup = new LeaveTable(this.tableId, this, true);
            this.addChild(this.leaveTablePopup, GameConstants.leaveZorder, "leaveTablePopup");
        }

        if (!this._leaveAllowed) {
            return;
        }

        this._mySeatIndex = -1;
        var isWaiting = false;
        for (var i = 0; i < this._playerArray.length; ++i) {
            var obj = this._playerArray[i];
            if (obj && obj._seatPosition != -1 && obj.isWaiting && obj._playerName == applicationFacade._myUName) {
                isWaiting = true;
            }
            // It was written in tournament base game by Meghanshi
            /*if (obj && obj._seatPosition != -1 && obj._isNext && obj._playerName == applicationFacade._myUName) {
             isWaiting = true;
             }*/
        }
        var gameType = this._serverRoom.getVariable(SFSConstants._GAMETYPE).value.toString();
        if (!this._isWatching && !isWaiting &&
            (this._currentRoomState == SFSConstants.RS_GAMESTART ||
            this._currentRoomState == SFSConstants.RS_WATCH ||
            this._currentRoomState == SFSConstants.RS_CUTTHEDECK ||
            this._currentRoomState == SFSConstants.RS_MELD ||
            (this._currentRoomState == SFSConstants.RS_RESULT && gameType.search("Point") == -1))) {
            this.leaveTablePopup.message.setString("You will lose your money if you leave the game in between,\nAre you sure you want to leave the table ");
        }
        else {
            this.leaveTablePopup.message.setString("Are you sure you want to leave the table ");
        }
        var string = this.leaveTablePopup.message.getString();
        this.leaveTablePopup.message.setString(string + this._serverRoom.getVariable(SFSConstants._ROOMID).value.toString() + "?");

    },
    yesLeaveBtnHandler: function () {
        //todo call weaver Main.callExternalInterface();
        // SoundManager.removeSound(SoundManager.extratimesound);
        SoundManager.stopAllSound();
        this.sendReqToServer(SFSConstants.CMD_CLICK_LEAVE_TABLE, {});
        this.leaveRoomByMyself();

        this.leaveTablePopup && this.leaveTablePopup.removeFromParent(true);
        if (this.waitingMC) {
            this.waitingMC.setString("Please Wait...");
            this.waitingMC.setVisible(true);
        }
        this._requestToSever = applicationFacade._controlAndDisplay._onlineStatus;
    },
    noLeaveBtnHandler: function () {
        this.leaveTablePopup && this.leaveTablePopup.removeFromParent(true);
        this.leaveTablePopup = null;
    },
    setCurrentScreen: function () {
        if (this._currentRoomState == SFSConstants.RS_JOIN) {
            this._tabMc.setIcon(1);
            this._tabMc.startGlow(1);
            return;
        }
        var iconFrame = 1;
        var tabBgFrame = 1;
        if (this._myId != -1) {
            if (this._currentRoomState == SFSConstants.RS_WATCH) {
                if (applicationFacade._currScr == this) {
                    iconFrame = 1;
                    tabBgFrame = 1;
                }
                else if (this.autoPlayPopup) {
                    iconFrame = 3;
                    tabBgFrame = 3;
                }
                else if (this._currentTurnID == this._myId && this._hasDistributionComp && this._userCardPanel._isVisible) {
                    iconFrame = (this.totCards == 13 ? 4 : 5);
                    tabBgFrame = 3;
                }
            }
            if (this._currentRoomState == SFSConstants.RS_MELD) {
                if (applicationFacade._currScr == this) {
                    iconFrame = 1;
                    tabBgFrame = 1;
                }
                else if (this.meldingPanel && this._userCardPanel._isVisible) {
                    iconFrame = (this.totCards == 13 ? 4 : 5);
                    tabBgFrame = 3;
                }
            }
        }
        else {
            tabBgFrame = 1;
            iconFrame = ((this._isWatching && !this._isWaiting) ? 2 : 1);
        }
        this._tabMc.setIcon(iconFrame);
        this._tabMc.startGlow(tabBgFrame);
        if (this._tabMc._iconImage && this._tabMc.timerTxt) {
            var profMC = this.getSeatObjByUser(this._myId);//this.getSeatObjBySeatId(this._totalSeats);
            if (profMC && profMC.timerLabel) {
                if (this._isExtraTurnTime || this._extraTimeToggle)
                    this._tabMc.timerTxt.setString(profMC.extraTimerLabel.getString());
                else
                    this._tabMc.timerTxt.setString(profMC.timerLabel.getString());
            }
        }
        if (this._tabMc._iconImage && this._tabMc.iconExtraMC)
            this._tabMc.iconExtraMC.setVisible(this._isExtraTurnTime || this._extraTimeToggle);
    }
});