/**
 * Created by stpl on 5/31/2017.
 */
var GameDemo = Popup.extend({
    ctor: function () {
        this._super(new cc.Size(650 * scaleFactor, 435 * scaleFactor), this, true);
        this.initTouchListener();

        var closeBtn = new cc.Sprite(spriteFrameCache.getSpriteFrame("ResultClose.png"));
        closeBtn.setPosition(cc.p(this.width - 5 * scaleFactor, this.height - 5 * scaleFactor));
        this.addChild(closeBtn, 5, 555);
        cc.eventManager.addListener(this.popupTouchListener.clone(), closeBtn);
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        if (target.getTag() == 555)
            this.removeFromParent(true);
        else if (target.getTag() == 112 && target._isEnabled) {
            this.playButton.click();
        }
    },
    onExit: function () {
        this._super();
        this.video.pause();
        // window.clearTimeout(this.videoTimeId);
    }
});

var CardGameDemo_13 = GameDemo.extend({
    demoTouchListener: null,
    ctor: function () {
        this._super();
        this.initTouchListener();

        var videoNode = new cc.DrawNode();
        var isVideoLoaded = false;

        this.headerLabel.setString('13 Card Point Rummy');

        this.headerLabel.setString("13 Card Point Rummy");
        this.headerLabel.x -= 5;
        this.headerLabel.y += 5;
       /* var header = new cc.LabelTTF("13 Card Point Rummy", "RobotoRegular", 11 * 2 * scaleFactor);
        header.setColor(new cc.Color(255, 255, 255));
        header.setScale(0.5);
        header.setPosition(cc.p(10, 10));
        this.addChild(header, 2);*/

        this.video = videoNode.playVideo(res.video.card_13, cc.p(0, 400 * scaleFactor), cc.size(844 * scaleFactor, 500 * scaleFactor), cc.size(650 * scaleFactor, 400 * scaleFactor));
        this.video.id = "demoVideo";
        // this.video.loop = true;
        this.video.load();
        this.video.onloadeddata = function () {
            isVideoLoaded = true;
        };

        var overlay = new cc.DrawNode();
        overlay.drawRect(cc.p(0, 0), cc.p(650, 400), cc.color(0, 0, 0, 120), 1, cc.color(0, 0, 0, 120));
        overlay.setPosition(cc.p(0, 0));
        this.addChild(overlay, 10);

        var playButtonSprite = new cc.Sprite(res.lobby.playBtn);
        playButtonSprite.setTag(112);
        playButtonSprite.setEnable = function (bool) {
            this._isEnabled = bool;
            this.setVisible(bool);
            overlay.setVisible(bool);
        };
        playButtonSprite.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(playButtonSprite, 50);
        cc.eventManager.addListener(this.popupTouchListener.clone(), playButtonSprite);
        playButtonSprite.setEnable(true);

        this.playButton = document.createElement('button');
        this.playButton.id = 'playVideo';

        var that = this;

        function startPlayback() {
            if (isVideoLoaded)
                return that.video.play();
            else return false;
        }

        this.video.addEventListener('ended', myHandler, false);
        function myHandler(e) {
            playButtonSprite.setEnable(true);
        }

        that.playButton.addEventListener('click', function () {
            var promise = startPlayback();
            if (promise.then && promise.then(function () {
                    playButtonSprite.setEnable(false);
                }).catch(function (error) {
                    playButtonSprite.setEnable(true);
                })) {
                playButtonSprite.setEnable(false);
            } else playButtonSprite.setEnable(true);
        });

        this.addChild(videoNode, 5);
    }
});

var PickDemo = GameDemo.extend({
    ctor: function () {
        this._super();
        var videoNode = new cc.DrawNode();
        var isVideoLoaded = false;

        this.headerLabel.setString('How to Pick a Card?');

        this.headerLabel.setString("How to Pick a Card?");
        this.headerLabel.x -= 5;
        this.headerLabel.y += 5;

        this.video = videoNode.playVideo(res.video.pick, cc.p(0, 400 * scaleFactor), cc.size(844 * scaleFactor, 500 * scaleFactor), cc.size(650 * scaleFactor, 400 * scaleFactor));
        this.video.id = "demoVideo";
        // this.video.loop = true;
        this.video.load();
        this.video.onloadeddata = function () {
            isVideoLoaded = true;
        };


        var overlay = new cc.DrawNode();
        overlay.drawRect(cc.p(0, 0), cc.p(650, 400), cc.color(0, 0, 0, 120), 1, cc.color(0, 0, 0, 120));
        overlay.setPosition(cc.p(0, 0));
        this.addChild(overlay, 10);

        var playButtonSprite = new cc.Sprite(res.lobby.playBtn);
        playButtonSprite.setTag(112);
        playButtonSprite.setEnable = function (bool) {
            this._isEnabled = bool;
            this.setVisible(bool);
            overlay.setVisible(bool);
        };
        playButtonSprite.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(playButtonSprite, 50);
        cc.eventManager.addListener(this.popupTouchListener.clone(), playButtonSprite);
        playButtonSprite.setEnable(true);

        this.playButton = document.createElement('button');
        this.playButton.id = 'playVideo';

        var that = this;

        function startPlayback() {
            if (isVideoLoaded)
                return that.video.play();
            else return false;
        }

        this.video.addEventListener('ended', myHandler, false);
        function myHandler(e) {
            playButtonSprite.setEnable(true);
        }

        that.playButton.addEventListener('click', function () {
            var promise = startPlayback();
            if (promise.then && promise.then(function () {
                    playButtonSprite.setEnable(false);
                }).catch(function (error) {
                    playButtonSprite.setEnable(true);
                })) {
                playButtonSprite.setEnable(false);
            } else playButtonSprite.setEnable(true);
        });
        this.addChild(videoNode, 5);
    }
});

var DiscardDemo = GameDemo.extend({
    ctor: function () {
        this._super();
        var videoNode = new cc.DrawNode();
        var isVideoLoaded = false;

        this.headerLabel.setString('How to Discard?');

        this.headerLabel.setString("How to Discard?");
        this.headerLabel.x -= 5;
        this.headerLabel.y += 5;

        this.video = videoNode.playVideo(res.video.discard, cc.p(0, 400 * scaleFactor), cc.size(844 * scaleFactor, 500 * scaleFactor), cc.size(650 * scaleFactor, 400 * scaleFactor));
        this.video.id = "demoVideo";
        // this.video.loop = true;
        this.video.load();
        this.video.onloadeddata = function () {
            isVideoLoaded = true;
        };

        var overlay = new cc.DrawNode();
        overlay.drawRect(cc.p(0, 0), cc.p(650, 400), cc.color(0, 0, 0, 120), 1, cc.color(0, 0, 0, 120));
        overlay.setPosition(cc.p(0, 0));
        this.addChild(overlay, 10);

        var playButtonSprite = new cc.Sprite(res.lobby.playBtn);
        playButtonSprite.setTag(112);
        playButtonSprite.setEnable = function (bool) {
            this._isEnabled = bool;
            this.setVisible(bool);
            overlay.setVisible(bool);
        };
        playButtonSprite.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(playButtonSprite, 50);
        cc.eventManager.addListener(this.popupTouchListener.clone(), playButtonSprite);
        playButtonSprite.setEnable(true);

        this.playButton = document.createElement('button');
        this.playButton.id = 'playVideo';

        var that = this;

        function startPlayback() {
            if (isVideoLoaded)
                return that.video.play();
            else return false;
        }

        this.video.addEventListener('ended', myHandler, false);
        function myHandler(e) {
            playButtonSprite.setEnable(true);
        }

        that.playButton.addEventListener('click', function () {
            var promise = startPlayback();
            if (promise.then && promise.then(function () {
                    playButtonSprite.setEnable(false);
                }).catch(function (error) {
                    playButtonSprite.setEnable(true);
                })) {
                playButtonSprite.setEnable(false);
            } else playButtonSprite.setEnable(true);
        });
        this.addChild(videoNode, 5);
    }
});

var GroupCardDemo = GameDemo.extend({
    ctor: function () {
        this._super();
        var videoNode = new cc.DrawNode();
        var isVideoLoaded = false;

        this.headerLabel.setString('How to Group Cards?');

        this.headerLabel.setString("How to Group Cards?");
        this.headerLabel.x -= 5;
        this.headerLabel.y += 5;

        this.video = videoNode.playVideo(res.video.group, cc.p(0, 400 * scaleFactor), cc.size(844 * scaleFactor, 500 * scaleFactor), cc.size(650 * scaleFactor, 400 * scaleFactor));
        this.video.id = "demoVideo";
        // this.video.loop = true;
        this.video.load();

        this.video.onloadeddata = function () {
            isVideoLoaded = true;
        };

        var overlay = new cc.DrawNode();
        overlay.drawRect(cc.p(0, 0), cc.p(650, 400), cc.color(0, 0, 0, 120), 1, cc.color(0, 0, 0, 120));
        overlay.setPosition(cc.p(0, 0));
        this.addChild(overlay, 10);

        var playButtonSprite = new cc.Sprite(res.lobby.playBtn);
        playButtonSprite.setTag(112);
        playButtonSprite.setEnable = function (bool) {
            this._isEnabled = bool;
            this.setVisible(bool);
            overlay.setVisible(bool);
        };
        playButtonSprite.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(playButtonSprite, 50);
        cc.eventManager.addListener(this.popupTouchListener.clone(), playButtonSprite);
        playButtonSprite.setEnable(true);

        this.playButton = document.createElement('button');
        this.playButton.id = 'playVideo';

        var that = this;

        function startPlayback() {
            if (isVideoLoaded)
                return that.video.play();
            else return false;
        }

        this.video.addEventListener('ended', myHandler, false);
        function myHandler(e) {
            playButtonSprite.setEnable(true);
        }

        that.playButton.addEventListener('click', function () {
            var promise = startPlayback();
            if (promise.then && promise.then(function () {
                    playButtonSprite.setEnable(false);
                }).catch(function (error) {
                    playButtonSprite.setEnable(true);
                })) {
                playButtonSprite.setEnable(false);
            } else playButtonSprite.setEnable(true);
        });
        this.addChild(videoNode, 5);
    }
});

var ShowDemo = GameDemo.extend({
    ctor: function () {
        this._super();
        var videoNode = new cc.DrawNode();
        var isVideoLoaded = false;

        this.headerLabel.setString('How to Show?');

        this.headerLabel.setString("How to Show?");
        this.headerLabel.x -= 5;
        this.headerLabel.y += 5;

        this.video = videoNode.playVideo(res.video.show, cc.p(0, 400 * scaleFactor), cc.size(844 * scaleFactor, 500 * scaleFactor), cc.size(650 * scaleFactor, 400 * scaleFactor));
        this.video.id = "demoVideo";
        // this.video.loop = true;
        this.video.load();
        this.video.onloadeddata = function () {
            isVideoLoaded = true;
        };

        var overlay = new cc.DrawNode();
        overlay.drawRect(cc.p(0, 0), cc.p(650, 400), cc.color(0, 0, 0, 120), 1, cc.color(0, 0, 0, 120));
        overlay.setPosition(cc.p(0, 0));
        this.addChild(overlay, 10);

        var playButtonSprite = new cc.Sprite(res.lobby.playBtn);
        playButtonSprite.setTag(112);
        playButtonSprite.setEnable = function (bool) {
            this._isEnabled = bool;
            this.setVisible(bool);
            overlay.setVisible(bool);
        };
        playButtonSprite.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(playButtonSprite, 50);
        cc.eventManager.addListener(this.popupTouchListener.clone(), playButtonSprite);
        playButtonSprite.setEnable(true);

        this.playButton = document.createElement('button');
        this.playButton.id = 'playVideo';

        var that = this;

        function startPlayback() {
            if (isVideoLoaded)
                return that.video.play();
            else return false;
        }

        this.video.addEventListener('ended', myHandler, false);
        function myHandler(e) {
            playButtonSprite.setEnable(true);
        }

        that.playButton.addEventListener('click', function () {
            var promise = startPlayback();
            if (promise.then && promise.then(function () {
                    playButtonSprite.setEnable(false);
                }).catch(function (error) {
                    playButtonSprite.setEnable(true);
                })) {
                playButtonSprite.setEnable(false);
            } else playButtonSprite.setEnable(true);
        });

        /*this.videoTimeId = setTimeout(function () {
         cc.log('Attempting to play automatically');
         if (that.video.canPlayType("video/mp4") != "") {
         startPlayback().then(function () {
         cc.log('The play() Promise fulfilled!');
         playButtonSprite.setEnable(false);
         }).catch(function (error) {
         cc.log('The play() Promise rejected!');
         cc.log('Use the Play button instead.');
         cc.log(error);
         that.playButton.addEventListener('click', startPlayback);
         setTimeout(function () {
         // that.playButton.click();
         }, 100)
         });
         }
         }, 500);*/

        this.addChild(videoNode, 5);
    }
});