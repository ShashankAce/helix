/**
 * Created by stpl on 11/9/2016.
 */
"use strict";
var UserCardPanel = cc.Layer.extend({
    _gameRoom: null,
    isClickLocked: null,
    SortBtn: null,
    DropBtn: null,
    showBtn: null,
    groupBtn: null,
    discardBtn: null,
    showCard: null,
    dummyCard: null,
    ccTimeOutManager: null,
    showCardArea: null,
    _cardsToMove: null,
    _initX: null,
    _initY: null,
    _cardToDiscard: null,
    _cardToShow: null,
    _lastCardX: null,
    userCardsGroupMC: null,
    totCards: null,
    numOfGrps: null,
    numOfUngrp: null,
    minNumOfCardIngrp: null,
    maxNumOfCardIngrp: null,
    cardHeight: null,
    cardWidth: null,
    _groupSprite: null,
    _groupY: null,
    _isSortClicked: null,
    isCardDistributed: null,

    _isShowReqSent: null,
    _numOfTunnela: null,
    _numOfDoublee: null,
    _numOfPureSeq: null,
    _numOfSeq: null,
    _numOfImpSeq: null,
    _numOfJokers: null,
    _numOfInvalid: null,

    cardString: null,
    cardStack: null,
    isCardAnimating: null,
    cardBeingTouched: null,
    tempCardBuffer: null,
    _cardZOrder: null,

    FlipTime: null,
    SpaceBWCard: null,
    MaxSlotsOneSide: null,
    _selectedCardArray: null,
    grpArr: null,
    cardsGroupArr: null,
    _isDragging: null,
    _cardDone: null,
    _discardClicked: null,
    _startPos: null,
    _gameType: null,
    _dropObj: null,
    _changedGrps: null,
    _cardSetStr: null,
    _killDistribution: null,
    _initialCardStr: null,
    _isVisible: null,
    startP: null,
    endP: null,

    ctor: function (gameRoomContext) {
        this._super();
        this._gameRoom = gameRoomContext;
        this.ccTimeOutManager = new CCTimeOutManager();

        this.isCardDistributed = false;
        this._isShowReqSent = false;
        this._numOfTunnela = 0;
        this._numOfDoublee = 0;
        this._numOfPureSeq = 0;
        this._numOfSeq = 0;
        this._numOfImpSeq = 0;
        this._numOfJokers = 0;
        this._numOfInvalid = 0;

        this.cardString = "";
        this.cardStack = [];
        this.isCardAnimating = false;
        this.cardBeingTouched = false;
        this.tempCardBuffer = [];
        this._cardZOrder = 0;

        this.FlipTime = 0.6;
        this.SpaceBWCard = 37;
        this.MaxSlotsOneSide = 12;
        this._selectedCardArray = [];
        this.grpArr = [];
        this.cardsGroupArr = [];
        this._isDragging = false;
        this._cardDone = false;
        this._discardClicked = false;
        this._startPos = cc.p(0, 0);
        this._gameType = "";
        this._dropObj = {};
        this._changedGrps = [];
        this._cardSetStr = "";
        this.startP = cc.p(90 * scaleFactor, 414.95 * scaleFactor);
        this.endP = cc.p(900 * scaleFactor, 414.95 * scaleFactor);
        this._killDistribution = false;
        this._initialCardStr = "";
        this._isVisible = false;
        this._isSortClicked = false;

        this.SortBtn = new CreateBtn("SortButton", "SortButtonOver", "SortBtn", cc.p(this.width - 90 * scaleFactor, this.height - 400 * scaleFactor));
        this.SortBtn.hide(true);
        this.addChild(this.SortBtn, 1);

        this.DropBtn = new Drop(this);
        this.DropBtn.hide(true);
        this.addChild(this.DropBtn, 1);

        var numOfCard = this._gameRoom._serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;
        var gameType = '';
        if (parseInt(numOfCard) != 13) {
            gameType = 21;
        }

        this.groupBtn = new NormalButton("group", "groupBtn", gameType);
        this.groupBtn.setPosition(cc.p(100 * scaleFactor, 320 * scaleFactor));
        this.groupBtn.hide(true);
        this.addChild(this.groupBtn, 1);

        this.discardBtn = new NormalButton("discard", "discardBtn", gameType);
        this.discardBtn.setScale(0.80);
        this.discardBtn.setPosition(cc.p(400 * scaleFactor, 320 * scaleFactor));
        this.discardBtn.hide(true);
        this.addChild(this.discardBtn, 1);

        this.showBtn = new NormalButton("show", "showBtn", gameType);
        this.showBtn.setScale(0.80);
        this.showBtn.setPosition(cc.p(500 * scaleFactor, 330 * scaleFactor));
        this.showBtn.hide(true);
        this.addChild(this.showBtn, 1);

        this.showCard = this.createSprite("ShowCardHolder", "ShowCardHolder", "showCard", cc.p(644 * scaleFactor, 420 * scaleFactor));
        this.addChild(this.showCard, 1);
        var pos = this.showCard.getPosition();
        this.showCardArea = cc.rect(pos.x - this.showCard.width / 2, pos.y - this.showCard.height / 2, this.showCard.width, this.showCard.height);

        this.ungroupBtn = this.createSprite("refreshBTN", "refreshBTN", "ungroupBtn", cc.p(500 * scaleFactor, 320 * scaleFactor));
        this.addChild(this.ungroupBtn, 1);

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }

        this.initCardListener();
        this.initTouchListener();
        this.hideAllItems(true);

    },
    getPickedCardPos: function () {
        var pickPos = cc.p(0, GameConstants.cardsDefaultPosY);
        this.cardStack = this.sortOnXPos(this.cardStack);
        if (this.cardStack.length == this.totCards - 1)
            return this.cardStack[this.cardStack.length - 1].getPosition();
        else {
            var lastCrd = this.cardStack[this.cardStack.length - 1];
            if (this.grpArr[this.numOfGrps + this.numOfUngrp].length > 0) {
                if (this.totCards == 22)
                    pickPos.x = lastCrd.x + this.SpaceBWCard + 22 * scaleFactor;
                else
                    pickPos.x = lastCrd.x + this.SpaceBWCard + 37 * scaleFactor;
            }
            else
                pickPos.x = lastCrd.x + this.SpaceBWCard;
            return pickPos;
        }
    },
    initCardListener: function () {
        var _self = this;
        this.card_touchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode) && target.isVisible()) {
                    if (!_self.cardBeingTouched && !_self.isCardAnimating) {

                        target.tempX = target.x;
                        target._touchDelta.x = 0;
                        target._touchDelta.y = 0;
                        target._cardMoved = false;

                        _self.isClickLocked = true;
                        _self.cardBeingTouched = true;

                        // hide buttons over card
                        _self.groupBtn.hide(true);
                        _self.discardBtn.hide(true);
                        _self.showBtn.hide(true);
                        _self.ungroupBtn.hide(true);

                        target.opacity = 150;
                        target.oldZOrder = target.getLocalZOrder();
                        return true;
                    }
                }
                return false;
            },
            onTouchMoved: function (touch, event) {
                var target = event.getCurrentTarget();
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(target.x - targetSize.width / 2, target.y - targetSize.height / 2, targetSize.width, targetSize.height);
                if (cc.rectContainsRect(cc.rect(0, 0, cc.winSize.width, cc.winSize.height), targetRect)) {
                    if (!target.isVisible())return;
                    var delta = touch.getDelta();

                    target.x += delta.x;
                    target.y += delta.y;

                    target._touchDelta.x += delta.x;
                    target._touchDelta.y += delta.y;

                    target.setLocalZOrder(100);
                    if (_self.cardStack.length == 14 || _self.cardStack.length == 22) {
                        _self.showCard.setVisible(true);
                    }
                    return true;
                }
                return false;
            },
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                target._touchEnded = true;
                if (!target.isVisible())return;
                var targetRect = cc.rect(target.x - target.width / 2, target.y - target.height / 2, target.width, target.height);
                var deltaAllowed = 4;

                if (Math.abs(target._touchDelta.x) > deltaAllowed || Math.abs(target._touchDelta.y) > deltaAllowed) {
                    target._cardMoved = true;
                } else {
                    target.setLocalZOrder(target.oldZOrder);
                }
                if (_self.cardBeingTouched) {
                    _self.cardBeingTouched = false;
                    if (!target._cardMoved) { // card is clicked
                        _self.cardClickHandler(target);
                        target.x = target.tempX;
                        target.tempX = null;
                    } else { // card is moved
                        _self.cardMoveHandler(target, targetRect);
                    }
                    target.opacity = 255;
                    target._cardMoved = false;
                }
            }
        });
        this.card_touchListener.retain();
    },
    initMouseListener: function (element) {
        this.userCardPanelMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            swallowTouches: true,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    target._isEnabled && !target._hovering && target.hover(true);
                    target._hovering = true;
                    return true;
                }
                target._isEnabled && target._hovering && target.hover(false);
                target._hovering = false;
                return false;

            }.bind(this)
        });
        this.userCardPanelMouseListener.retain();
        cc.eventManager.addListener(this.userCardPanelMouseListener, this.SortBtn);
        cc.eventManager.addListener(this.userCardPanelMouseListener.clone(), this.DropBtn);
        cc.eventManager.addListener(this.userCardPanelMouseListener.clone(), this.ungroupBtn);
    },
    initTouchListener: function () {
        this.userCardPanelTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode) && target._isEnabled) {
                    switch (target.getName()) {
                        case "SortBtn" :
                            !this.isCardAnimating && !this._isSortClicked && this.sortHandler();
                            break;
                        case "DropBtn" :
                            this.onActivity("drop");
                            break;
                        case "groupBtn":
                            this.groupBtnHandler();
                            break;
                        case "discardBtn":
                            this.onActivity("discard");
                            break;
                        case "showBtn":
                            this.onActivity("show");
                            break;
                        case "ungroupBtn":
                            this.unGroupBtnHandler();
                            break;
                    }
                }
            }.bind(this)
        });
        this.userCardPanelTouchListener.retain();
        cc.eventManager.addListener(this.userCardPanelTouchListener, this.SortBtn);
        cc.eventManager.addListener(this.userCardPanelTouchListener.clone(), this.DropBtn);
        cc.eventManager.addListener(this.userCardPanelTouchListener.clone(), this.groupBtn);
        cc.eventManager.addListener(this.userCardPanelTouchListener.clone(), this.discardBtn);
        cc.eventManager.addListener(this.userCardPanelTouchListener.clone(), this.showBtn);
        cc.eventManager.addListener(this.userCardPanelTouchListener.clone(), this.ungroupBtn);
    },
    hideAllItems: function (bool) {
        if (typeof bool != 'boolean') {
            throw new Error("Boolean can not be undefined or other than boolean");
        }
        this.SortBtn.hide(bool);
        this.DropBtn.hide(bool);
        this.hideButtons(bool);
        if (this._groupSprite && bool) {
            this._groupSprite.setVisible(!bool);
            this._groupSprite.removeAllChildrenWithCleanup();
        }

        for (var i = 0; i < (this.numOfGrps + this.numOfUngrp); i++) {
            for (var j = 0; j < this.grpArr[i].length; j++) {
                this.grpArr[i][j]._isEnabled = !bool;
                this.grpArr[i][j].setVisible(!bool);
            }
        }
        this._isVisible = !bool;
    },
    hideButtons: function (bool) {
        if (typeof bool != 'boolean') {
            throw new Error("Boolean can not be undefined or other than boolean");
        }
        if (!bool) {
            if (this._selectedCardArray.length > 1) {
                this.groupBtn.hide(false);
                this.ungroupBtn.hide(false);
            }
            this.activatePanel(bool);
        }
        else {
            this.groupBtn.hide(bool);
            this.discardBtn.hide(bool);
            this.ungroupBtn.hide(bool);
            this.showBtn.hide(bool);
            this.showCard.hide(bool);
        }
    },
    resetFilter: function () {
        for (var i = 0; i < this.cardStack.length; i++) {
            this.cardStack[i].setDirtyCard(false);
        }
    },
    dropButtonAction: function () {
        cc.log("Drop button" + this._gameRoom.tableId);
    },
    createSprite: function (spriteName, spriteOverName, buttonName, pos) {
        var Sprite = cc.Sprite.extend({
            spriteOverName: null,
            spriteName: null,
            _isEnabled: null,
            _hovering: null,
            ctor: function (spriteName, spriteOverName, buttonName, pos) {
                this._super(spriteFrameCache.getSpriteFrame(spriteName + ".png"));
                this.spriteName = spriteName;
                this.spriteOverName = spriteOverName;
                this._hovering = false;
                pos && this.setPosition(pos);
                this.setName(buttonName);

                return true;
            },
            hover: function (bool) {

                bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteOverName + ".png")) :
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteName + ".png"));
            },
            pauseListener: function (bool) {
                this._isEnabled = !bool;
            },
            hide: function (bool) {
                this.setVisible(!bool);
                this._isEnabled = !bool;
            }
        });
        return new Sprite(spriteName, spriteOverName, buttonName, pos);
    },
    setVars: function (cards) {
        cards = parseInt(cards);
        if (cards == 21) {
            this.totCards = 22;// 14;
            this.numOfGrps = 8;  // 4;
            this.numOfUngrp = 5;
            this.minNumOfCardIngrp = 2;
            this.maxNumOfCardIngrp = 8;// 5;
            this.SpaceBWCard = 21 * scaleFactor;
            this._groupY = 44 * scaleFactor; //48;	//58;
            this.startP = cc.p(123 * scaleFactor, 414.95 * scaleFactor);
            this.SortBtn.hide(true);
            //sortBtnMC.x = 970.9;
            this.cardHeight = 85.8 * scaleFactor;
            this.cardWidth = 61 * scaleFactor;
            if (this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD))
                this._gameRoom._wildcardStr = this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD).value;

        }
        else if (cards == 13) {
            this.totCards = 14;
            this.numOfGrps = 5;
            this.numOfUngrp = 5;
            this.minNumOfCardIngrp = 2;
            this.maxNumOfCardIngrp = 7;
            this.SpaceBWCard = 27 * scaleFactor;// 25;
            this._groupY = 58 * scaleFactor;
            this.startP = cc.p(120 * scaleFactor, 135 * scaleFactor);
            this.endP = cc.p(920 * scaleFactor, 135 * scaleFactor);
            //this.sortBtnMC.x = 918.9;
            this.cardHeight = 104 * scaleFactor;
            this.cardWidth = 74 * scaleFactor;
            this._gameRoom.showValidity && this._gameRoom.showValidity.hide(true);
            //this.drop_btn.y = 525.30;
        }
    },
    getCard: function (name, flipped) {
        return new Card(name, flipped, this);
    },
    packCards: function (cardString) {
        this._initialCardStr = cardString;
        var cards = [];
        this.tempCardBuffer = null;
        if (!(cardString.indexOf("|") >= 0) && !(cardString.indexOf(":") >= 0) && !(cardString.indexOf(";") >= 0)) {
            cards = cardString.split(SFSConstants.COMMA_DELIMITER);
            this.tempCardBuffer = cards;
        } else {
            this.showDistributedCards(cardString, "");
        }

        /*if (this._gameRoom.isCutDeckRespHandled == true) {
         this.startDistribution();
         }*/
    },
    getmeldCardStr: function () {
        var meldCardStr = "|";
        for (var i = 0; i < this.tempCardBuffer.length; i++) {
            meldCardStr += this.tempCardBuffer[i] + ",";
        }
        // str.replace(/,(\s+)?$/, '');
        meldCardStr.replace(/, $/, ";");
        return meldCardStr;

    },
    startDistribution: function () {
        cc.log("StartDistribution", this.tempCardBuffer);
        // this.showDistributedCards(this._initialCardStr, "");
        if (this.tempCardBuffer && this.tempCardBuffer.length > 0) {
            this.distributeCards(this.tempCardBuffer);
        } else {
            if (this._gameRoom._isWaiting || this._gameRoom._myId == -1 || this._gameRoom._isWatching) {
                this._gameRoom._userCardPanel.distributeCardsInWaiting();
            }
        }
    },
    distributeCardsInWaiting: function () {
        this._gameRoom.initClosedCards();
        this._gameRoom.arrangeClosedCard();
        // this.isCardDistributed = true;
        var self = this;
        GameConstants.cardsPositionY = GameConstants.cardsDefaultPosY;
        var xConstant = (GAME_SIZE.width - (((13 - 1) * GameConstants.cardSpace) + GameConstants.cardHalf)) / 2;
        var closedDeckPos = this._gameRoom.deckPanel.closedDeck.getPosition();

        this.dummyCard = new CardBack();
        this.dummyCard.setScale(0.9);
        this.addChild(this.dummyCard, 1, "dummyCard");
        this.dummyCard.setPosition(closedDeckPos);

        var player = [];
        var selfId = this._gameRoom._playerArray[this._gameRoom._playerArray.length - 1]._playerId;

        for (var a = 0; a < this._gameRoom._playerArray.length; a++) {
            if (this._gameRoom._playerArray[a]._sittingStatus == !0 && !this._gameRoom._playerArray[a].isWaiting) {
                player.push(this._gameRoom._playerArray[a]); // must be rearranged player
            }
        }

        var numOfCard = this._gameRoom._serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;
        var cardIndex = 0, playerIndex = 0, card;
        var cardDistributeInterval = 0;

        SoundManager.playSound(SoundManager.cardDistribute, true);
        var drawCard = function () {
            playerIndex = 0;
            card = null;

            cardDistributeInterval = setInterval(function () {
                var currentPlayer = player[playerIndex];
                card = new CardBack();
                card.setPosition(closedDeckPos);
                self.addChild(card, (GameConstants.cardMinZorder + 13) - cardIndex);
                // var seatPos = player[playerIndex]._seatPosition;
                var closedCard = self._gameRoom.getClosedCardById(currentPlayer._seatPosition);
                var _newClosedCardPos = closedCard.getPosition();
                if (!cc.game._paused) {
                    card.tween(cc.spawn(cc.moveTo(0.2, _newClosedCardPos.x, _newClosedCardPos.y),
                        cc.scaleBy(0.2, 0.1, 0.1)), true);
                } else {
                    card.removeFromParent(true);
                }
                ++playerIndex;
                if (playerIndex > player.length - 1) {
                    window.clearInterval(cardDistributeInterval);
                    if (cardIndex != numOfCard - 1) {
                        drawCard();
                        ++cardIndex;
                    } else if (cardIndex >= numOfCard - 1) {
                        SoundManager.removeSound(SoundManager.cardDistribute);
                        self.dummyCard.removeFromParent(true);
                        self._gameRoom.handleDistributionCompl();
                    }
                }
            }, 50);
        };

        var timeout = player.length * numOfCard * 60;
        setTimeout(function () {
            if (cardIndex != numOfCard - 1) {
                self.killDistribution();
                window.clearInterval(cardDistributeInterval);
                self.dummyCard && self.dummyCard.removeFromParent(true);
                self.dummyCard = null;
                self._gameRoom.handleDistributionCompl();
            }
        }, timeout);

        drawCard();
    },
    distributeCards: function (cardArray) {
        cc.log("Distribution started", cardArray);
        var self = this;
        this._gameRoom.initClosedCards();
        this._gameRoom.arrangeClosedCard();

        if (!cardArray || cardArray == "" || this._gameRoom._dropped) {
            this.SortBtn.hide(true);
            this.DropBtn.hide(true);
            return;
        }
        var cardSpace, cardScaleFactor;
        if (this.totCards == 22) {
            cardSpace = 22;
            cardScaleFactor = 0.8;
            this._gameRoom._wildcardStr = this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD).value;
        }
        else {
            cardScaleFactor = 1;
            cardSpace = 25;
        }

        GameConstants.cardsPositionY = GameConstants.cardsDefaultPosY;
        var xConstant = (GAME_SIZE.width - (((cardArray.length - 1) * cardSpace) + GameConstants.cardHalf * cardScaleFactor)) / 2;
        var closedDeckPos = this._gameRoom.deckPanel.closedDeck.getPosition();

        this.dummyCard = new CardBack();
        this.dummyCard.setScale(0.9);
        this.addChild(this.dummyCard, 1);
        this.dummyCard.setPosition(closedDeckPos);

        this.cardStack = [];
        var player = [];
        for (var a = 0; a < this._gameRoom._playerArray.length; a++) {
            if (this._gameRoom._playerArray[a]._sittingStatus == !0 && !this._gameRoom._playerArray[a].isWaiting) {
                player.push(this._gameRoom._playerArray[a]);// must be rearranged player
            }
        }

        var tmpCard = [];
        for (var k = 0; k < cardArray.length; k++) {
            tmpCard.push(new Card(cardArray[k], true, this));
        }

        var cardIndex = 0, playerIndex = 0, card;
        var cardDistributeInterval = 0;
        var crdFlipInt = 1;


        SoundManager.playSound(SoundManager.cardDistribute, true);
        var drawCard = function () {
            playerIndex = 0;
            card = null;
            cardDistributeInterval = setInterval(function () {
                var currentPlayer = player[playerIndex];
                if (currentPlayer._playerId == self._gameRoom._myId) {

                    // card = new Card(tmpCard[cardIndex], true, self);
                    card = tmpCard[cardIndex];
                    card.setPosition(closedDeckPos);
                    card._initialPosition = cc.p(xConstant + cardSpace * cardIndex, (GameConstants.cardsDefaultPosY));
                    card._stackIndex = cardIndex;
                    card.setJoker(self._gameRoom._wildcardStr, self._gameRoom._jokerSymVisble);
                    self.addChild(card, (GameConstants.cardMinZorder + cardArray.length) - cardIndex);
                    card.resetPosition(card._initialPosition);
                    card._cardName = "card" + (cardIndex);
                    card.grNo = self.numOfGrps + self.numOfUngrp - 1;
                    self.cardStack.push(card);
                    self.grpArr[self.numOfGrps + self.numOfUngrp - 1].push(card);

                } else {

                    card = new CardBack();
                    card.setPosition(closedDeckPos);
                    self.addChild(card, (GameConstants.cardMinZorder + cardArray.length) - cardIndex);
                    // var seatPos = player[playerIndex]._seatPosition;
                    var closedCard = self._gameRoom.getClosedCardById(currentPlayer._seatPosition);
                    if (!closedCard) {
                        window.clearInterval(cardDistributeInterval);
                        /*cc.log(self._gameRoom._closedCards);
                         throw new Error("Closed card not received for player " + currentPlayer._seatPosition);*/
                    }
                    var _newClosedCardPos = closedCard.getPosition();
                    if (!cc.game._paused) {
                        card.tween(cc.spawn(cc.moveTo(0.2, _newClosedCardPos.x, _newClosedCardPos.y),
                            cc.scaleBy(0.2, 0.1, 0.1)), true);
                    } else {
                        card.removeFromParent(true);
                    }

                }
                ++playerIndex;
                if (playerIndex > player.length - 1) {
                    window.clearInterval(cardDistributeInterval);
                    if (cardIndex != cardArray.length - 1) {
                        drawCard();
                        ++cardIndex;
                    }
                    else {
                        SoundManager.removeSound(SoundManager.cardDistribute);
                        self.isCardDistributed = true;
                        self.dummyCard.removeFromParent(true);
                        crdFlipInt = setTimeout(function () {
                            self.flipAllCards();
                        }, 500);
                    }
                }
            }, 50);
        };


        var timeout = player.length * cardArray.length * 60;
        setTimeout(function () {
            if (cardIndex != cardArray.length - 1) {
                window.clearInterval(cardDistributeInterval);
                window.clearInterval(crdFlipInt);
                self.killDistribution();
                self.showDistributedCards(self._initialCardStr);
                self.dummyCard && self.dummyCard.removeFromParent(true);
                self.dummyCard = null;
                self._gameRoom.handleDistributionCompl();
            }
        }, timeout);

        drawCard();
    },
    flipAllCards: function () {
        SoundManager.removeSound(SoundManager.cardDistribute);

        if (this.cardStack.length <= 0)
            return;

        this._cardZOrder = this.cardStack[0]._localZOrder;
        for (var i = 0; i < this.cardStack.length; i++) {
            this.cardStack[i].flip(i);
        }
    },
    flipEndListener: function () {
        this._isVisible = true;
        this._gameRoom.handleDistributionCompl();
        this.reArrangeCards();
        this.dropVisible();
    },
    clearCards: function () {

        this.dummyCard && this.dummyCard.removeFromParent(true);

        var crd;
        for (var j = 0; j < this.grpArr.length; j++) {
            for (var k = 0; k < this.grpArr[j].length; k++) {
                crd = this.grpArr[j][k];
                crd.removeFromParent(true);
            }
        }

        if (this.cardStack.length > 0) {
            for (var i = 0; i < this.cardStack.length; i++) {
                this.removeChild(this.cardStack[i], true);
            }
        }
        this.cardStack = [];
        this.grpArr = [];
        for (var i = 0; i < (this.numOfGrps + this.numOfUngrp); i++) {
            this.grpArr[i] = [];
        }
        this._selectedCardArray = [];

        this._cardToShow = null;

    },
    initPanel: function () {
        this.clearCards();
        this.hideAllItems(true);
        this.DropBtn.hide(true);
        this.isCardDistributed = false;
        this._discardClicked = false;
        this._isSortClicked = false;

        this._gameType = this._gameRoom._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
        this.setVars(this._gameRoom._serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value);
        this.dropVisible();
        this._dropObj = null;

        this._startPos = cc.p(145 * scaleFactor, 0);

        this.grpArr = [];
        for (var i = 0; i < (this.numOfGrps + this.numOfUngrp); i++) {
            this.grpArr[i] = [];
        }

        this._selectedCardArray = [];
        this.cardsGroupArr = [];

        this.DropBtn.setState(Drop.DROP_BUTTON_NORMAL);
        this.DropBtn.check.gotoAndStop(2);
        if (this._gameRoom._dropped) {
            this.SortBtn.hide(true);
            this.DropBtn.hide(true);
        }

        this._isShowReqSent = false;
        this._numOfTunnela = 0;
        this._numOfDoublee = 0;
        this._numOfPureSeq = 0;
        this._numOfSeq = 0;
        this._numOfImpSeq = 0;
        this._numOfJokers = 0;
        this._numOfInvalid = 0;

        if ((this._gameRoom._myId != -1) && (this._gameRoom._serverRoom.groupId != "tournamentGameRoom") && (this._gameRoom._serverRoom.getVariable("cashOrPromo").value != "CASH"))       //((totCards==21)&&(_serverRoom.getVariable("cashOrPromo") != "CASH"))
        {
            if (!this._isWaiting) {
                this._gameRoom.showValidity && this._gameRoom.showValidity.hide(false);

                if (this._gameRoom.showValidity) {
                    this._gameRoom.showValidity._dropDownSelection = "Pure Sequence";
                    if (this.totCards == 22)
                        this._gameRoom.showValidity.checkValidity();
                    else
                        this._gameRoom.showValidity.checkValidity13();
                }
            }
        }
        if (this.totCards == 22) {
            this.SortBtn.setPosition(cc.p(this.width - 40 * scaleFactor, this.height - 400 * scaleFactor));
        }
    },
    showDistributedCards: function (cardStr) {
        SoundManager.removeSound(SoundManager.cardDistribute);
        this._isVisible = true;
        GameConstants.cardsPositionY = GameConstants.cardsDefaultPosY;
        this.hideButtons(true);
        var isCardRemoved = false;
        this._discardClicked = false;
        this._isDragging = false;
        if (!cardStr || cardStr == "" || this._gameRoom._dropped) {
            this.SortBtn.hide(true);
            this.DropBtn.hide(true);
            return;
        }

        this.clearCards();
        if (this.totCards == 21)
            this._gameRoom._wildcardStr = this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD).value;
        var userCardGroupArr = [];

        var cardCount = 1;
        var groupCount = 0;
        var unGroupCount = this.numOfGrps;
        var k;
        var groupStr = "";
        var unGroupStr = "";
        var groupArray = [];
        var unGroupArray = [];
        var cardArr = [];
        var card;

        if (cardStr.indexOf("|") == -1) {
            unGroupArray = cardStr.split(":");
            groupArray = [];
            groupStr = "";
            unGroupStr = cardStr;
            unGroupCount = this.numOfGrps + this.numOfUngrp - 1;
            this.SortBtn.hide(false);
        }
        if (cardStr.indexOf("|") == 0) {
            cardStr = cardStr.slice(1);
            unGroupArray = cardStr.split(":");
            groupStr = "";
            unGroupStr = cardStr;
            //trace("unGroupArray::  ", unGroupArray, unGroupArray.length);
            if (unGroupArray.length == 1) {
                this.unGroupCount = this.numOfGrps + this.numOfUngrp - 1;
                this.SortBtn.hide(false);
            }
            groupArray = [];
        }
        else if (cardStr.indexOf("|") == cardStr.length - 1) {
            cardStr = cardStr.slice(0, cardStr.length - 1);
            groupArray = cardStr.split(";");
            groupStr = cardStr;
            unGroupStr = "";
        }
        else {
            userCardGroupArr = cardStr.split("|");
            if (userCardGroupArr.length == 1) {
                unGroupStr = userCardGroupArr[0];
                unGroupArray = unGroupStr.split(":");
                groupStr = "";
                groupArray = [];
                unGroupCount = this.numOfGrps + this.numOfUngrp - 1;
                this.SortBtn.hide(false);
            }
            else {
                groupStr = userCardGroupArr[0];
                unGroupStr = userCardGroupArr[1];
                if (userCardGroupArr[0] != "") {
                    groupArray = groupStr.split(";");
                }
                if (userCardGroupArr[1] != "") {
                    unGroupArray = unGroupStr.split(":");
                }
            }
        }
        var tempCard = cardStr.split("|").join(",").split(";").join(",").split(":").join(",");
        var arr;
        if ((this._gameRoom.openCard != "") && ((tempCard.split(",").length) >= this.totCards)) {
            if (unGroupStr.indexOf(this._gameRoom.openCard) != -1) {
                for (var a = 0; a < unGroupArray.length; a++) {
                    arr = unGroupArray[a].split(",");
                    if (arr.indexOf(this._gameRoom.openCard) != -1) {
                        arr.splice(arr.indexOf(this._gameRoom.openCard), 1);
                        unGroupArray[a] = arr.join(",");
                        this.isCardRemoved = true;
                        break;
                    }
                }
            }
            else if (groupStr.indexOf(this._gameRoom.openCard) != -1) {
                for (var b = 0; b < groupArray.length; i++) {
                    arr = groupArray[b].split(",");
                    if (arr.indexOf(this._gameRoom.openCard) != -1) {
                        arr.splice(arr.indexOf(this._gameRoom.openCard), 1);
                        groupArray[b] = arr.join(",");
                        this.isCardRemoved = true;
                        break;
                    }
                }
            }
            this._gameRoom.openCard = "";
        }

        for (var i = 0; i < groupArray.length; i++) {
            if (groupArray[i] != "") {
                cardArr = groupArray[i].split(",");

                for (k = 0; k < cardArr.length; k++) {
                    card = this.getCard(cardArr[k], false);
                    card.setJoker(this._gameRoom._wildcardStr, this._gameRoom._jokerSymVisble);
                    card._cardName = "card" + (cardCount);
                    card.grNo = (groupCount > (this.numOfGrps + this.numOfUngrp - 1)) ? (this.numOfGrps + this.numOfUngrp - 1) : groupCount;
                    card.setPosition(this.startP.x + ((cardCount - 1) * this.SpaceBWCard), GameConstants.cardsPositionY);
                    card._initialPosition = card.getPosition();
                    card._addListener();

                    this.addChild(card, GameConstants.cardMinZorder + (cardCount - 1));
                    this.cardStack.push(card);
                    this.grpArr[(groupCount > (this.numOfGrps + this.numOfUngrp - 1)) ? (this.numOfGrps + this.numOfUngrp - 1) : groupCount].push(card);// = arrToPush;
                    cardCount++;
                }
            }
            groupCount++;
        }

        for (var j = 0; j < unGroupArray.length; j++) {

            if (unGroupArray[j] != "") {
                cardArr = unGroupArray[j].split(",");
                //trace(cardArr.length);
                for (k = 0; k < cardArr.length; k++) {
                    card = this.getCard(cardArr[k], false);
                    card.setJoker(this._gameRoom._wildcardStr, this._gameRoom._jokerSymVisble);
                    card._cardName = "card" + (cardCount);
                    card.grNo = (unGroupCount > (this.numOfGrps + this.numOfUngrp - 1)) ? (this.numOfGrps + this.numOfUngrp - 1) : unGroupCount;
                    card.setPosition(this.startP.x + ((cardCount - 1) * this.SpaceBWCard), GameConstants.cardsPositionY);//+3; add this for 21 card only
                    card._initialPosition = card.getPosition();
                    card._addListener();

                    this.addChild(card, GameConstants.cardMinZorder + (cardCount - 1));
                    this.grpArr[(unGroupCount > (this.numOfGrps + this.numOfUngrp - 1)) ? (this.numOfGrps + this.numOfUngrp - 1) : unGroupCount].push(card);// = arrToPush;

                    cardCount++;
                }
                unGroupCount++;
            }
        }

        if (cardCount == this.totCards + 1) {
            card = this.getLastCard();
            card && card.setVisible(true);
            this.SortBtn._isEnabled = false;
            this.DropBtn.hide(true);
            this._gameRoom.deckPanel.disable();
        }
        this.reArrangeCards();
        if (isCardRemoved) {
            var groupCards = this.getGroupCardsStr();
            this.sendGroupCards(groupCards, false);
        }
        //sortBtnVisible();
    },
    handleReActive: function () {
        if (this.DropBtn.isCheckboxEnabled) {
            this.DropBtn.check.gotoAndStop(2);
        }
        this.dropVisible();
    },
    dropVisible: function () {
        if ((this._gameType == "Pool-Best of 3") || (this._gameType == "Pool-Best of 6") || (this._gameType == "Pool-Best of 2") || this._gameRoom._dropped)
            this.DropBtn.hide(true);
        else {
            this.DropBtn.hide(false);
            this.inactivePanel();
        }
        /*if (this.getLastCard())
         this.DropBtn.hide(true);*/
    },
    inactivePanel: function () {
        this.discardBtn.hide(true);
        this.showBtn.hide(true);
        this.showCard.hide(true);
    },

    activatePanel: function () {
        if (this.getLastCard() && (this._selectedCardArray && this._selectedCardArray.length == 1)) {
            this.discardBtn.hide(false);
            this.showBtn.hide(false);
            this.showCard.hide(false);
        }
    },
    getLastCard: function () {
        for (var i = 0; i < this.cardStack.length; i++) {
            if (this.cardStack[i]._cardName == ("card" + this.totCards))
                return this.cardStack[i];
        }
        return null;
    },
    settleDownSelectedCard: function (bool, discard) {
        cc.log("settleDownSelected_Card>>>>>>> ", this.getGroupCardsStr());
        if (bool == undefined)
            bool = true;
        if (this.getLastCard()) {
            var card;
            if (!bool) {
                card = this.getLastCard();
            }
            else {
                if (this._cardToShow != null) {
                    card = this._cardToShow;
                    if (this._selectedCardArray.length == 1) {
                        var c = this._selectedCardArray.pop();
                        c.y = GameConstants.cardsPositionY;
                        //c.setDirtyCard(false);
                    }
                }
                else if (this._selectedCardArray.length == 1)
                    card = this._selectedCardArray.pop();
                else
                    card = this.getLastCard();
            }
            //card.setDirtyCard(false);
            card.y = GameConstants.cardsPositionY;

            this.grpArr[card.grNo].splice(this.grpArr[card.grNo].indexOf(card), 1);

            if (card != this.getLastCard()) {
                if (this.grpArr[card.grNo].indexOf(card) != -1)
                    this.grpArr[card.grNo].splice(this.grpArr[card.grNo].indexOf(card), 1);

                if (this._selectedCardArray.indexOf(card) != -1) {
                    this._selectedCardArray.splice(this._selectedCardArray.indexOf(card), 1);
                }
                this.removeChild(card);
                this.cardStack.splice(this.cardStack.indexOf(card), 1);
                //this.getLastCard()._name = card._name;
                this.getLastCard()._cardName = card._cardName;

            }
            else {
                if (this.grpArr[this.getLastCard().grNo].indexOf(this.getLastCard()) != -1)
                    this.grpArr[this.getLastCard().grNo].splice(this.grpArr[this.getLastCard().grNo].indexOf(this.getLastCard()), 1);

                if (this._selectedCardArray.indexOf(this.getLastCard()) != -1) {
                    this._selectedCardArray.splice(this._selectedCardArray.indexOf(this.getLastCard()), 1);
                }
                this.removeChild(this.getLastCard());
                this.cardStack.splice(this.getLastCard(), 1);
            }

            if (this._selectedCardArray.length < 3) {
                this.groupBtn.hide(true);
                this.ungroupBtn.hide(true);
            }
            cc.log("settleDownSelectedCard>>>>>>> ", this.getGroupCardsStr());
            this.reArrangeCards();
        }
    },
    cardClickHandler: function (card) {
        if (card._isSelected) {
            card._isSelected = false;
            card.y = GameConstants.cardsPositionY;
            this._selectedCardArray.splice(this._selectedCardArray.indexOf(card), 1);

        } else {
            card.y = GameConstants.cardsPositionY + 15 * scaleFactor;
            card._isSelected = true;
            this._selectedCardArray.push(card);
        }
        this.showCardAction();
    },
    cardMoveHandler: function (card, targetRect) {
        if (this.showCard && this.showCard.isVisible() && cc.rectOverlapsRect(this.showCardArea, targetRect)) {
            // If card is dropped at showCard Area
            if (this.cardStack.length == this.totCards) {
                card.setLocalZOrder(card.oldZOrder);
                this._cardToShow = card;
                if (!this.showConfirmPopup) {
                    this.showConfirmPopup = new ShowConfirmation(this);
                    this.addChild(this.showConfirmPopup, GameConstants.popupZorder, "showConfirmPopup");
                }
            }
            else {
                this.reArrangeCards();
            }
        }
        else {
            // card is not dropped at show area
            /*if (card._isShadeEnable) {
             card.y = GameConstants.cardsPositionY;// + 15;
             }
             else {
             card.y = GameConstants.cardsPositionY;
             }*/
            this.showCard.hide(true);
            this.arrangeCardInBtw(card);
            this.showCardAction();
        }
        if (card.y == GameConstants.cardsPositionY + 15 * scaleFactor) {
            this.discardBtn.x = this.showBtn.x = this.groupBtn.x = card.x;
            this.ungroupBtn.x = card.x + 52 * scaleFactor;
        }
    },
    arrangeCardInBtw: function (card) {
        this.cardsGroupArr = [];
        for (var i = 0; i < this.grpArr.length; i++) {
            this.cardsGroupArr = this.cardsGroupArr.concat(this.grpArr[i]);
        }
        this.cardsGroupArr = this.sortOnXPos(this.cardsGroupArr);
        this.cardStack = this.sortOnXPos(this.cardStack);

        var newIndx = this.cardsGroupArr.indexOf(card);
        if (newIndx != -1) {
            var newGroupArrInx;
            var oldGroupArrInx;

            if (newIndx == 0) {
                newGroupArrInx = this.cardsGroupArr[1].grNo;
                oldGroupArrInx = card.grNo;
                if (this.cardsGroupArr[1].grNo < this.numOfGrps) {
                    if (this.grpArr[newGroupArrInx].length < this.maxNumOfCardIngrp) {
                        this.grpArr[oldGroupArrInx].splice(this.grpArr[oldGroupArrInx].indexOf(card), 1);
                        card.grNo = this.cardsGroupArr[1].grNo;
                        this.grpArr[newGroupArrInx].unshift(card);
                    }
                }
                else {
                    this.grpArr[oldGroupArrInx].splice(this.grpArr[oldGroupArrInx].indexOf(card), 1);
                    card.grNo = this.cardsGroupArr[1].grNo;
                    this.grpArr[newGroupArrInx].unshift(card);
                }
            }
            else if (newIndx == (this.cardsGroupArr.length - 1)) {
                newGroupArrInx = this.cardsGroupArr[this.cardsGroupArr.length - 2].grNo;
                oldGroupArrInx = card.grNo;
                var diffFrmPrev = card.x - this.cardsGroupArr[this.cardsGroupArr.length - 2].x;

                if ((this.cardsGroupArr[this.cardsGroupArr.length - 2].grNo < this.numOfGrps) && (diffFrmPrev > 100)) {
                    newGroupArrInx = this.numOfGrps + this.numOfUngrp - 1;  //8
                    this.grpArr[oldGroupArrInx].splice(this.grpArr[oldGroupArrInx].indexOf(card), 1);
                    card.grNo = this.numOfGrps + this.numOfUngrp - 1;  //8
                    this.grpArr[newGroupArrInx].push(card);
                }
                else if (this.cardsGroupArr[this.cardsGroupArr.length - 2].grNo >= this.numOfGrps) {
                    this.grpArr[oldGroupArrInx].splice(this.grpArr[oldGroupArrInx].indexOf(card), 1);
                    card.grNo = this.cardsGroupArr[this.cardsGroupArr.length - 2].grNo;
                    this.grpArr[newGroupArrInx].push(card);
                }
                else {
                    if (this.grpArr[newGroupArrInx].length < this.maxNumOfCardIngrp) {
                        this.grpArr[oldGroupArrInx].splice(this.grpArr[oldGroupArrInx].indexOf(card), 1);
                        card.grNo = this.cardsGroupArr[this.cardsGroupArr.length - 2].grNo;
                        this.grpArr[newGroupArrInx].push(card);
                    }
                }
            }
            else {
                if (this.cardsGroupArr[newIndx - 1].grNo == this.cardsGroupArr[newIndx + 1].grNo) {
                    newGroupArrInx = this.cardsGroupArr[newIndx - 1].grNo;
                    oldGroupArrInx = card.grNo;
                    if (this.cardsGroupArr[newIndx - 1].grNo < this.numOfGrps) {
                        if (this.grpArr[newGroupArrInx].length < this.maxNumOfCardIngrp) {
                            this.grpArr[oldGroupArrInx].splice(this.grpArr[oldGroupArrInx].indexOf(card), 1);
                            card.grNo = this.cardsGroupArr[newIndx - 1].grNo;
                            this.grpArr[newGroupArrInx].push(card);
                        }
                    }
                    else {
                        this.grpArr[oldGroupArrInx].splice(this.grpArr[oldGroupArrInx].indexOf(card), 1);
                        card.grNo = this.cardsGroupArr[newIndx - 1].grNo;
                        this.grpArr[newGroupArrInx].push(card);
                    }
                }
                else {
                    diffFrmPrev = card.x - this.cardsGroupArr[newIndx - 1].x;
                    var diffFrmNext = this.cardsGroupArr[newIndx + 1].x - card.x;
                    if (diffFrmNext < diffFrmPrev) {
                        newGroupArrInx = this.cardsGroupArr[newIndx + 1].grNo;
                        oldGroupArrInx = card.grNo;
                        if (this.cardsGroupArr[newIndx + 1].grNo < this.numOfGrps) {
                            if (this.grpArr[newGroupArrInx].length < this.maxNumOfCardIngrp) {
                                this.grpArr[oldGroupArrInx].splice(this.grpArr[oldGroupArrInx].indexOf(card), 1);
                                card.grNo = this.cardsGroupArr[newIndx + 1].grNo;
                                this.grpArr[newGroupArrInx].push(card);
                            }
                        }
                        else {
                            this.grpArr[oldGroupArrInx].splice(this.grpArr[oldGroupArrInx].indexOf(card), 1);
                            card.grNo = this.cardsGroupArr[newIndx + 1].grNo;
                            this.grpArr[newGroupArrInx].push(card);
                        }
                    }
                    else {
                        newGroupArrInx = this.cardsGroupArr[newIndx - 1].grNo;
                        oldGroupArrInx = card.grNo;
                        if (this.cardsGroupArr[newIndx - 1].grNo < this.numOfGrps) {
                            if (this.grpArr[newGroupArrInx].length < this.maxNumOfCardIngrp) {
                                this.grpArr[oldGroupArrInx].splice(this.grpArr[oldGroupArrInx].indexOf(card), 1);
                                card.grNo = this.cardsGroupArr[newIndx - 1].grNo;
                                this.grpArr[newGroupArrInx].push(card);
                            }
                        }
                        else {
                            this.grpArr[oldGroupArrInx].splice(this.grpArr[oldGroupArrInx].indexOf(card), 1);
                            card.grNo = this.cardsGroupArr[newIndx - 1].grNo;
                            this.grpArr[newGroupArrInx].push(card);
                        }
                    }
                }
            }
            this.grpArr[newGroupArrInx] = this.sortOnXPos(this.grpArr[newGroupArrInx]);

            //todo validity check
            //this.checkGroupValidity();

            this.grpArr[this.numOfGrps + this.numOfUngrp - 1] = this.sortOnXPos(this.grpArr[this.numOfGrps + this.numOfUngrp - 1]);

            this.grpArr[newGroupArrInx] = this.removeDuplicate(this.grpArr[newGroupArrInx]);
            this.grpArr[oldGroupArrInx] = this.removeDuplicate(this.grpArr[oldGroupArrInx]);
        }
        this.reArrangeCards();
    },
    showCardAction: function () {
        var lastCardMC;
        if (this._selectedCardArray.length == 0) {
            this.groupBtn.hide(true);
            this.ungroupBtn.hide(true);
            this.discardBtn.hide(true);
            this.showBtn.hide(true);
            this.showCard.hide(true);
        }
        else if (this._selectedCardArray.length == 1) {
            this.groupBtn.hide(true);
            this.ungroupBtn.hide(true);
            this.discardBtn.hide(false);
            if ((this.cardStack.length == this.totCards) && (this._gameRoom._currentTurnID == this._gameRoom._myId)) {
                this.discardBtn.hide(false);
                this.showBtn.hide(false);

                lastCardMC = this._selectedCardArray[0];
                /*lastCardMC.showCardAction(1);*/

                this.discardBtn.x = lastCardMC.x;
                this.showBtn.x = lastCardMC.x;
                if (this._gameRoom.totCards == 21) {
                    this.discardBtn.y = lastCardMC.y + 58 * scaleFactor;
                }
                this.showBtn.y = this.discardBtn.y + 25 * scaleFactor;
                this.showCard.hide(false);
            }
            else {
                this.discardBtn.hide(true);
                this.showBtn.hide(true);
                this.showCard.hide(true);
            }
        }
        else if ((this._selectedCardArray.length == (this.minNumOfCardIngrp - 1)) || (this._selectedCardArray.length > this.maxNumOfCardIngrp)) {
            this.discardBtn.hide(true);
            this.showBtn.hide(true);
            this.showCard.hide(true);
            this.groupBtn.hide(true);
            this.ungroupBtn.hide(false);
            lastCardMC = this._selectedCardArray[this._selectedCardArray.length - 1];
            this.groupBtn.x = lastCardMC.x;
            if (this._gameRoom.totCards == 21) {
                this.ungroupBtn.y = lastCardMC.y + 58 * scaleFactor;
                this.ungroupBtn.x = lastCardMC.x;
            }
        }
        else {
            this.discardBtn.hide(true);
            this.showBtn.hide(true);
            this.showCard.hide(true);
            this.groupBtn.hide(false);
            this.ungroupBtn.hide(false);

            lastCardMC = this._selectedCardArray[this._selectedCardArray.length - 1];
            this.groupBtn.x = lastCardMC.x;
            this.ungroupBtn.x = lastCardMC.x + 52 * scaleFactor;
            if (this._gameRoom.totCards == 21) {
                this.groupBtn.y = lastCardMC.y + 58 * scaleFactor;
                this.ungroupBtn.y = lastCardMC.y + 58 * scaleFactor;
                this.ungroupBtn.x = lastCardMC.x + 44 * scaleFactor;
            }
        }
    },
    sendGroupCards: function (groupCards, _is) {
        var sentObj = {};
        if (applicationFacade._controlAndDisplay._onlineStatus && (groupCards != "")) {
            sentObj.cards = groupCards;
            sentObj.rnd = this._gameRoom._currentRound;
            this._gameRoom.sendReqToServer("groupCards", sentObj);
            if (_is) {
                var obj = {};
                obj.val = _is;
                this._gameRoom.sendReqToServer("isConnect", obj);
            }
        }
    },
    sendIsConnect: function (_is) {
        if (applicationFacade._controlAndDisplay._onlineStatus) {
            if (_is) {
                var obj = {};
                obj.val = _is;
                this._gameRoom.sendReqToServer("isConnect", obj);
            }
        }
    },
    unGroupBtnHandler: function () {
        this.groupBtn.hide(true);
        this.ungroupBtn.hide(true);

        for (var i = 0; i < this._selectedCardArray.length; i++) {
            this._selectedCardArray[i].y = GameConstants.cardsPositionY;
            this._selectedCardArray[i].setSelected(false);
        }
        this._selectedCardArray = [];
        this.reArrangeCards();
    },
    checkForUngroupedCards: function () {
        var numOfGrp = 0;
        var i;
        var j;

        for (i = 0; i < this.numOfGrps; i++) {
            if ((this.grpArr[i] != null) && (this.grpArr[i].length >= this.minNumOfCardIngrp)) {
                numOfGrp = numOfGrp + 1;
            }
        }
        if (numOfGrp >= this.numOfGrps) {
            for (i = this.numOfGrps; i < this.numOfGrps + this.numOfUngrp - 1; i++) {
                if ((this.grpArr[i] != null) && (this.grpArr[i].length > 0)) {
                    for (j = 0; j < this.grpArr[i].length; j++) {
                        this.grpArr[i][j].grNo = this.numOfGrps + this.numOfUngrp - 1;
                        this.grpArr[this.numOfGrps + this.numOfUngrp - 1].push(this.grpArr[i][j]);
                    }
                    this.grpArr[i].splice(0, this.grpArr[i].length);
                    this.grpArr[i] = [];
                }
            }
        }
    },
    checkForExtraCard: function () {
        //cc.log("checkForExtraCard>>>>> ", this.getGroupCardsStr());
        for (var i = 0; i < this.grpArr.length; i++) {
            this.cardsGroupArr = this.cardsGroupArr.concat(this.grpArr[i]);
        }
        if (this.cardsGroupArr.length >= this.totCards) {
            for (i = 0; i < this.grpArr.length; i++) {
                if (this.grpArr[i].indexOf(this._cardToShow) != -1)
                    this.grpArr[i].splice(this.grpArr[i].indexOf(this._cardToShow), 1);
            }
        }
        //cc.log("checkForExtraCard>>>>> ", this.getGroupCardsStr());
    },
    setShowCard: function (str) {
        for (var b = this.numOfGrps; b < this.grpArr.length; b++) {
            for (var x = 0; x < this.grpArr[b].length; x++) {
                if (this.grpArr[b][x]._name == str) {
                    this._cardToShow = this.grpArr[b][x];
                    return;
                }
            }
        }
        for (var a = 0; a < this.numOfGrps; a++) {
            for (var y = 0; y < this.grpArr[a].length; y++) {
                if (this.grpArr[a][y]._name == str) {
                    this._cardToShow = this.grpArr[a][y];
                    return;
                }
            }
        }
    },
    removeDuplicate: function (arr) {
        var i;
        var j;
        for (i = 0; i < arr.length - 1; i++) {
            for (j = i + 1; j < arr.length; j++) {
                if (arr[i] === arr[j]) {
                    arr.splice(j, 1);
                }
            }
        }
        return arr;
    },
    tweenDiscardCard: function () {
        SoundManager.playSound(SoundManager.picknddropsound, false);
        var moveTo = this._gameRoom.deckPanel.openDeck.getPosition();
        var cardEaseTime = 0.20;
        var moveAnimation = cc.spawn(cc.scaleTo(cardEaseTime, 0.87, 0.87), cc.moveTo(cardEaseTime, moveTo));
        if (this._cardToDiscard && !this._discardClicked) {
            // this._cardToDiscard.setPosition(moveTo);
            this._cardToDiscard.runAction(cc.sequence(moveAnimation, cc.callFunc(function () {
                this.onCompleteDiscard();
            }, this)));

        }
        else if (this._cardToShow) {
            // this._cardToShow.setPosition(moveTo);
            this._cardToShow.runAction(cc.sequence(moveAnimation, cc.callFunc(function () {
                this.onCompleteDiscard();
            }, this)));

        }
        else
            this._gameRoom.deckPanel.setOpenDeckCards(false);
    },
    onCompleteDiscard: function () {
        if (this._cardToDiscard || this._cardToShow) {
            this.dropVisible();
            this.settleDownSelectedCard(true, false);
            var groupCards = this.getGroupCardsStr();
            //  this.sendGroupCards(groupCards, true);
            this.sendIsConnect(true);
        }
        else
            this.dropVisible();
        //this._gameRoom.pickWaitMC.setVisible(false);

        this._discardClicked = false;
    },
    sortHandler: function () {

        var ungrpArr = [];
        var card;
        for (var a = this.numOfGrps; a < this.numOfGrps + this.numOfUngrp; a++) {
            if (this.grpArr[a]) {
                for (var b = 0; b < this.grpArr[a].length; b++) {
                    ungrpArr.push(this.grpArr[a][b]);
                }
            }
            this.grpArr[a] = [];
        }

        if (ungrpArr.length == 0)
            return;

        var grp = this.numOfGrps;
        var tempArr = [];

        for (var i = 0; i < ungrpArr.length; i++) {
            card = ungrpArr[i];
            if (!card)
                continue;
            if (card.cardVal != 0) {
                if (card.cardVal >= 1 && card.cardVal <= 13) {
                    card.grNo = grp;
                    this.grpArr[grp].push(card);
                }
                else if (card.cardVal >= 14 && card.cardVal <= 26) {
                    card.grNo = grp + 1;
                    this.grpArr[grp + 1].push(card);
                }
                else if (card.cardVal >= 27 && card.cardVal <= 39) {
                    card.grNo = grp + 2;
                    this.grpArr[grp + 2].push(card);
                }
                else if (card.cardVal >= 40 && card.cardVal <= 52) {
                    card.grNo = grp + 3;
                    this.grpArr[grp + 3].push(card);
                }
            }
            else {
                tempArr.push(card);
                card.grNo = this.numOfGrps + this.numOfUngrp - 1;
            }
        }
        //cc.log(this.grpArr[grp], this.grpArr[grp + 1], this.grpArr[grp + 2], this.grpArr[grp + 3]);
        this.grpArr[this.numOfGrps + this.numOfUngrp - 1] = tempArr;
        this.grpArr[grp] = this.sortOnCardVal(this.grpArr[grp]);
        this.grpArr[grp + 1] = this.sortOnCardVal(this.grpArr[grp + 1]);
        this.grpArr[grp + 2] = this.sortOnCardVal(this.grpArr[grp + 2]);
        this.grpArr[grp + 3] = this.sortOnCardVal(this.grpArr[grp + 3]);
        this.reArrangeCards();
        var groupCards = this.getGroupCardsStr();
        this.sendGroupCards(groupCards, false);
        this._isSortClicked = false;
    },
    sortOnCardVal: function (arr) {
        arr.sort(function (a, b) {
            var value = a.cardVal - b.cardVal;
            return (value);
        });
        return arr;
    },
    sortOnXPos: function (arr) {
        var value;
        arr.sort(function (a, b) {
            value = a.x - b.x;
            return value;
        });
        return arr;
    },
    groupBtnHandler: function () {
        this.ungroupBtn.hide(true);
        this._changedGrps = [];

        /*this._selectedCardArray.forEach(function (card) {
         this.grpArr[card.grNo].splice(this.grpArr[card.grNo].indexOf(card), 1);
         this._changedGrps.push(card.grNo);
         }.bind(this));*/

        var card;
        for (var i = 0; i < this._selectedCardArray.length; i++) {
            this.grpArr[this._selectedCardArray[i].grNo].splice(this.grpArr[this._selectedCardArray[i].grNo].indexOf(this._selectedCardArray[i]), 1);
            this._changedGrps.push(this._selectedCardArray[i].grNo);
        }

        this._changedGrps = this.removeDuplicate(this._changedGrps);

        for (var i = 0; i < this._changedGrps.length; i++) {
            if (this._changedGrps[i] < this.numOfGrps) {
                if (this.grpArr[this._changedGrps[i]].length < this.minNumOfCardIngrp) {
                    for (var j = 0; j < this.grpArr[this._changedGrps[i]].length; j++) {
                        this.grpArr[this._changedGrps[i]][j].grNo = this.numOfGrps + this.numOfUngrp - 1;
                        this.grpArr[this.numOfGrps + this.numOfUngrp - 1].push(this.grpArr[this._changedGrps[i]][j]);
                    }
                    this.grpArr[this._changedGrps[i]] = [];
                }
            }
        }
        var availGrpObj = this.shiftGroups1();
        var arr = this.grpArr[availGrpObj.no];
        var newcard;
        for (j = 0; j < this._selectedCardArray.length; j++) {
            newcard = this._selectedCardArray[j];
            newcard._isSelected = false;
            this.grpArr[availGrpObj.no].push(newcard);
            newcard.setDirtyCard(false);
            newcard.y = 0;
            newcard.grNo = availGrpObj.no;
        }
        this._selectedCardArray.splice(0, this._selectedCardArray.length);
        this._selectedCardArray = [];
        this.groupBtn.hide(true);
        this.ungroupBtn.hide(true);
        this.reArrangeCards();
    },
    shiftGroups1: function () {
        var card;
        for (var i = this.numOfGrps - 1; i > 0; i--) {
            for (var j = i; j > 0; j--) {
                if (this.grpArr[j].length == 0) {
                    if (this.grpArr[j - 1].length != 0) {
                        this.grpArr[j] = this.grpArr[j - 1];
                        this.grpArr[j].forEach(function (card) {
                            card.grNo = j;
                        });
                        this.grpArr[j - 1] = [];
                    }
                }
            }
        }

        var availGrpObj = {};
        if (this.grpArr[0].length == 0)
            return {arr: this.grpArr[0], no: 0};
        else
            return {arr: this.grpArr[this.numOfGrps + this.numOfUngrp - 1], no: (this.numOfGrps + this.numOfUngrp - 1)};
    },
    reArrangeCards: function () {

        this.checkGroupValidity();
        this.cardStack = [];

        if (this.totCards == 22)
            this.checkForUngroupedCards();

        this._groupSprite && this._groupSprite.removeFromParent(true);
        this._groupSprite = new cc.Node();
        this.addChild(this._groupSprite, GameConstants.cardMinZorder + 5);

        var lastX = this.startP.x;
        var card;
        var showGrp;
        var i;
        var j;
        this._numOfJokers = 0;
        var cardCount = 0;

        for (i = 0; i < this.numOfGrps + this.numOfUngrp; i++) {
            if (this.grpArr[i] && this.grpArr[i].length > 0) {
                this.grpArr[i] = this.removeDuplicate(this.grpArr[i]);
                for (j = 0; j < this.grpArr[i].length; j++) {
                    card = this.grpArr[i][j];
                    this.cardStack.push(card);
                    if (card._isJoker)
                        this._numOfJokers += 1;
                    if (card.grNo == i) {
                        card.x = lastX;
                        if (card._isSelected)
                            card.y = GameConstants.cardsPositionY + 15 * scaleFactor;
                        else
                            card.y = GameConstants.cardsPositionY;
                        card._initialPosition = card.getPosition();
                        lastX += this.SpaceBWCard;
                        card.setLocalZOrder(GameConstants.cardMinZorder + cardCount);
                        cardCount += 1;
                    }
                    else {
                        this.grpArr[i].splice(this.grpArr[i].indexOf(card), 1);
                    }
                }
                if (this.grpArr[i].length > 0) {
                    if (i >= this.numOfGrps)
                        lastX += this.SpaceBWCard + 1 * scaleFactor;
                    else {
                        if (this.totCards == 22)
                            lastX += this.SpaceBWCard + 22 * scaleFactor;
                        else
                            lastX += this.SpaceBWCard + 37 * scaleFactor;
                    }
                }
                if (i < this.numOfGrps) {
                    if (this.grpArr[i].length > 0) {
                        if ((this._gameRoom._serverRoom.groupId != "tournamentGameRoom") && this._gameRoom._serverRoom.getVariable("cashOrPromo") && (this._gameRoom._serverRoom.getVariable("cashOrPromo").value != "CASH"))   //(totCards==22)&&
                        {
                            if ((this.totCards == 22) || (this.grpArr[i].length > 2))
                                this.showGrpValidity(i);
                        }
                        else {

                            if ((this.totCards == 22) || (this.grpArr[i].length > 2))
                                this.showGroup(i);
                        }
                    }
                }
            }
        }

        this._lastCardX = lastX;
        this.setUserCardPosition(lastX);

        if (this._selectedCardArray.length > 0) {
            this.discardBtn.x = this.showBtn.x = this.groupBtn.x = this._selectedCardArray[this._selectedCardArray.length - 1].x;
            this.ungroupBtn.x = this._selectedCardArray[this._selectedCardArray.length - 1].x + 52 * scaleFactor;
        }

        this.calValidGrps();
        this.calculateScore();

        if (this._gameRoom.showValidity) {
            if (this.totCards == 22)
                this._gameRoom.showValidity.checkValidity();
            else
                this._gameRoom.showValidity.checkValidity13();
        }
        this.SortBtn.hide(false);
        this.isCardAnimating = false;
    },
    showGrpValidity: function (j) {
        var cardStrng = "";
        var card;
        for (var i = 0; i < this.grpArr[j].length; i++) {
            card = this.grpArr[j][i];
            cardStrng = cardStrng + card._name + ",";
        }
        cardStrng = cardStrng.substr(0, cardStrng.length - 1);
        if (cardStrng != "") {
            var validity;
            var _wildStr = "";
            if (this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD)) {
                _wildStr = this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD).value;
            }
            if (this.totCards == 22)
                validity = GroupValidity.checkFrGroup(cardStrng, _wildStr, this.totCards);
            else
                validity = GroupValidity.checkFrGroup13(cardStrng, _wildStr, this.totCards);
            var totalWidth;
            totalWidth = (this.grpArr[j][this.grpArr[j].length - 1].x - this.grpArr[j][0].x) - (this.cardWidth / 2);
            var symWidth = 40 + totalWidth;//_symGrp.txt.textWidth;
            var _symGrp = new ShowSeq(validity);
            _symGrp.showValidity(validity);
            _symGrp.x = this.grpArr[j][0].x + ((this.cardWidth + symWidth) / 4);
            if (this.totCards == 14)
                _symGrp.y = -3 * scaleFactor;
            this._groupSprite.addChild(_symGrp);
        }

    },
    showGroup: function (i) {
        var totalWidth;
        var sPos;
        sPos = this.grpArr[i][0].x + (this.grpArr[i][this.grpArr[i].length - 1].x - this.grpArr[i][0].x) / 2;
        totalWidth = (this.grpArr[i][this.grpArr[i].length - 1].x - this.grpArr[i][0].x) + (this.cardWidth / 2);

        var _nwGrp = new GroupSymbol(sPos, totalWidth);
        _nwGrp.x = sPos;
        if (this.totCards == 14)
            _nwGrp.y = -3 * scaleFactor;
        this._groupSprite.addChild(_nwGrp);
    },
    checkGroupValidity: function () {
        for (var i = 0; i < this.numOfGrps + this.numOfUngrp - 1; i++) {
            if (this.grpArr[i].length == 1) {
                this.grpArr[i][0].grNo = this.numOfGrps + this.numOfUngrp - 1;
                this.grpArr[this.numOfGrps + this.numOfUngrp - 1].push(this.grpArr[i][0]);
                this.grpArr[i] = [];
            }
        }
    },
    calValidGrps: function () {
        var card;
        var cardStrng = "";

        this._numOfTunnela = 0;
        this._numOfDoublee = 0;
        this._numOfPureSeq = 0;
        this._numOfSeq = 0;
        this._numOfImpSeq = 0;
        this._numOfInvalid = 0;
        var validity;

        for (var j = 0; j < this.numOfGrps; j++) {
            cardStrng = "";
            for (var i = 0; i < this.grpArr[j].length; i++) {
                card = this.grpArr[j][i];
                cardStrng = cardStrng + card._name + ",";
            }
            cardStrng = cardStrng.substr(0, cardStrng.length - 1);
            if (cardStrng != "") {
                var _wildStr = "";
                if (this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD)) {
                    _wildStr = this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD).value;
                }
                if (this.totCards == 22)
                    validity = GroupValidity.checkFrGroup(cardStrng, _wildStr, this.totCards);
                else
                    validity = GroupValidity.checkFrGroup13(cardStrng, _wildStr, this.totCards);

                switch (validity) {
                    case "Doublee":
                    {
                        this._numOfDoublee += 1;
                        break;
                    }
                    case "Tunnela":
                    {
                        this._numOfTunnela += 1;
                        break;
                    }
                    case "Pure Seq":
                    {
                        this._numOfPureSeq += 1;
                        break;
                    }
                    case "Seq":
                    {
                        this._numOfSeq += 1;
                        this._numOfImpSeq += 1;
                        break;
                    }
                    case "Set":
                    {
                        this._numOfSeq += 1;
                        break;
                    }
                    case "Invalid":
                    {
                        this._numOfInvalid += 1;
                        break;
                    }
                }
            }
        }
    },
    calculateScore: function () {
        if (this.totCards == 22)
            return;

        var i;
        var j;
        var cardStr;
        var card;
        var score = 0;
        var groupScore = 0;
        var cardScore = 0;
        for (i = 0; i < this.numOfGrps + this.numOfUngrp; i++) {
            for (j = 0; j < this.grpArr[i].length; j++) {
                card = this.grpArr[i][j];
                if (this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD) && GroupValidity.setJoker(card._name, this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD).value, this.totCards))
                    card._cardScore = 0;
                else if ((card._cardNum == 1) || (card._cardNum == 11) || (card._cardNum == 12) || (card._cardNum == 13))
                    card._cardScore = 10;
                else
                    card._cardScore = card._cardNum;
            }
        }
        if (((this._numOfPureSeq == 1) && (this._numOfImpSeq >= 1)) || (this._numOfPureSeq > 1)) {
            for (i = 0; i < this.numOfGrps; i++) {
                cardStr = "";
                groupScore = 0;
                if (this.grpArr[i].length > 0) {
                    for (j = 0; j < this.grpArr[i].length; j++) {
                        card = this.grpArr[i][j];
                        cardStr = cardStr + card._name + ",";
                        groupScore = groupScore + card._cardScore;
                    }
                    cardStr = cardStr.substr(0, cardStr.length - 1);
                    if (cardStr != "") {
                        var validity;
                        var _wildStr = "";
                        if (this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD)) {
                            _wildStr = this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD).value;
                        }
                        if (this.totCards == 22)
                            validity = GroupValidity.checkFrGroup(cardStr, _wildStr, this.totCards);
                        else
                            validity = GroupValidity.checkFrGroup13(cardStr, _wildStr, this.totCards);
                        if (validity == "Invalid") {
                            score = score + groupScore;
                        }
                    }
                }
            }
            for (i = this.numOfGrps; i < (this.numOfGrps + this.numOfUngrp); i++) {
                groupScore = 0;
                if (this.grpArr[i].length > 0) {
                    for (j = 0; j < this.grpArr[i].length; j++) {
                        card = this.grpArr[i][j];
                        groupScore = groupScore + card._cardScore;
                    }
                    score = score + groupScore;
                }
            }
        }
        else if ((this._numOfPureSeq == 1) && (this._numOfImpSeq < 1)) {
            for (i = 0; i < this.numOfGrps; i++) {
                cardStr = "";
                groupScore = 0;
                if (this.grpArr[i].length > 0) {
                    for (j = 0; j < this.grpArr[i].length; j++) {
                        card = this.grpArr[i][j];
                        //trace(card._cardScore);
                        cardStr = cardStr + card._name + ",";
                        groupScore = groupScore + card._cardScore;
                    }
                    cardStr = cardStr.substr(0, cardStr.length - 1);
                    if (cardStr != "") {
                        var validity;
                        var _wildStr = "";
                        if (this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD)) {
                            _wildStr = this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD).value;
                        }
                        if (this.totCards == 22)
                            validity = GroupValidity.checkFrGroup(cardStr, _wildStr, this.totCards);
                        else
                            validity = GroupValidity.checkFrGroup13(cardStr, _wildStr, this.totCards);
                        if ((validity == "Invalid") || (validity != "Pure Seq")) {
                            score = score + groupScore;
                        }
                    }
                }
            }
            for (i = this.numOfGrps; i < (this.numOfGrps + this.numOfUngrp); i++) {
                groupScore = 0;
                if (this.grpArr[i].length > 0) {
                    for (j = 0; j < this.grpArr[i].length; j++) {
                        card = this.grpArr[i][j];
                        groupScore = groupScore + card._cardScore;
                    }
                    score = score + groupScore;
                }
            }
        }
        else {
            for (i = 0; i < (this.numOfGrps + this.numOfUngrp); i++) {
                groupScore = 0;
                if (this.grpArr[i].length > 0) {
                    for (j = 0; j < this.grpArr[i].length; j++) {
                        card = this.grpArr[i][j];
                        groupScore = groupScore + card._cardScore;
                    }
                    score = score + groupScore;
                }
            }
        }
        if (score > 80)
            score = 80;
        if (this._gameRoom.showValidity)
            this._gameRoom.showValidity.setScore(score);
    },
    setUserCardPosition: function (lastX) {
        var dis = (this.endP.x - lastX) / 2;
        var card;
        for (var i = 0; i < this.cardStack.length; i++) {
            card = this.cardStack[i];
            card.x = card.x + dis;
        }
        this._groupSprite.x = this._groupSprite.x + dis;
        this._groupSprite.y = GameConstants.cardsDefaultPosY - (this.cardHeight / 2) - 13 * scaleFactor;
    },
    onActivity: function (action) {
        this.discardBtn.hide(true);
        this.showBtn.hide(true);
        this.showCard.hide(true);

        var sendObj = {};
        switch (action) {
            case "show":
            {
                if (this._selectedCardArray[0]) {
                    this.discardBtn.hide(false);
                    this.showBtn.hide(false);
                    this.showCard.hide(false);
                    this._cardToShow = this._selectedCardArray[0];

                    if (!this.showConfirmPopup) {
                        this.showConfirmPopup = new ShowConfirmation(this);
                        this.addChild(this.showConfirmPopup, GameConstants.popupZorder, "showConfirmPopup");
                    }
                }

                break;
            }
            case "discard":
            {
                if (this._selectedCardArray[0]) {
                    this._discardClicked = true;
                    sendObj.cmd = "discard";
                    sendObj.dc = this._selectedCardArray[0]._name;
                    this._cardToShow = this._selectedCardArray[0];
                    var tempArr = new Array();
                    for (var i = 0; i < this.grpArr.length; i++) {
                        var cardGArr = this.grpArr[i];
                        var cardTemp;
                        var cardGTempArr = new Array();
                        for (var j = 0; j < cardGArr.length; j++) {
                            var cardTemp = cardGArr[j];
                            cardGTempArr.push(cardTemp);
                        }
                        tempArr.push(cardGTempArr);
                    }
                    tempArr[this._cardToShow.grNo].splice(tempArr[this._cardToShow.grNo].indexOf(this._cardToShow), 1);
                    var groupCards = this.getTempGroupCardsStr(tempArr);
                    sendObj.grpCards = groupCards;
                    this._gameRoom.handleUserResp(sendObj);
                }
                break;
            }
            case "drop":
            {
                this.dropVisible();

                // if (this.DropBtn.isCheckboxEnabled && !this.DropBtn.isCheckboxSelected) {
                // this.DropBtn.currentState = Drop.DROP_BUTTON_CHECKBOX_SELECTED;
                //this.DropBtn.setState(Drop.DROP_BUTTON_CHECKBOX_SELECTED);
                // }
                //else
                if (this.DropBtn.isCheckboxEnabled && this.DropBtn.isCheckboxSelected) {
                    // this.DropBtn.currentState = Drop.DROP_BUTTON_CHECKBOX;
                    this._dropObj = null;
                    this.DropBtn.check.gotoAndStop(2);
                    return;
                }

                // this.dropConfirmPopup && this.dropConfirmPopup.removeFromParent(true);

                if (!this.dropConfirmPopup) {
                    this.dropConfirmPopup = new DropConfirmation(this);
                    this.addChild(this.dropConfirmPopup, GameConstants.popupZorder, "dropConfirmPopup");
                    break;
                }

            }
        }
    },
    getTempGroupCardsStr: function (tempArr) {
        var str = "";
        var card;
        var allGrpCardsArr = [];
        var allUnGrpCardsArr = [];
        var numOfTempGrps = this.numOfGrps;
        for (var a = 0; a < numOfTempGrps; a++) {
            allGrpCardsArr = allGrpCardsArr.concat(tempArr[a]);
        }
        for (var b = numOfTempGrps; b < tempArr.length; b++) {
            allUnGrpCardsArr = allUnGrpCardsArr.concat(tempArr[b]);
        }
        for (var i = 0; i < allGrpCardsArr.length; ++i) {
            card = allGrpCardsArr[i];
            str += card._name;
            if (i < allGrpCardsArr.length - 1) {
                if (card.grNo == allGrpCardsArr[i + 1].grNo)
                    str += ",";
                else {
                    str += ";";
                }
            }
        }
        str += "|";
        for (i = 0; i < allUnGrpCardsArr.length; ++i) {
            card = allUnGrpCardsArr[i];
            str += card._name;
            if (i < allUnGrpCardsArr.length - 1) {
                if (card.grNo == allUnGrpCardsArr[i + 1].grNo)
                    str += ",";
                else {
                    str += ":";
                }
            }
        }
        return str;
    },
    isMyDrop: function (crtFrm) {
        crtFrm = parseInt(crtFrm);
        if (crtFrm == 1 || crtFrm == 2 || crtFrm == 3 || crtFrm == 4 || crtFrm == 5) {
            this.DropBtn.setState(crtFrm);

            if (crtFrm == 1 && this._dropObj) {
                this._gameRoom.sendTurnResp(this._dropObj);
                return true;
            }
            if (crtFrm == 2) {
                if (this.DropBtn.check.currentFrame == 1) {
                    this.DropBtn.check.gotoAndStop(1);// checked
                    return false;
                }
                this.DropBtn.check.gotoAndStop(2); // unchecked
                return false;
            }
            return false;
        } else {
            throw new Error("Incorrect value in isMyDrop");
        }
    },
    dropYesBtnHandler: function () {
        if (this._discardClicked) {
            this.dropConfirmPopup.removeFromParent(true);
            this.dropConfirmPopup = null;
            return;
        }
        if (this.DropBtn.currentState == 2) {
            this.DropBtn.check.gotoAndStop(3 - this.DropBtn.check.currentFrame);
        }

        /*if (this.DropBtn.isCheckboxEnabled && !this.DropBtn.isCheckboxSelected) {
         this.DropBtn.check.gotoAndStop(1);
         }
         else if (this.DropBtn.isCheckboxEnabled && this.DropBtn.isCheckboxSelected) {
         this.DropBtn.setState(Drop.DROP_BUTTON_CHECKBOX);
         return;
         }*/

        var sendObj = {};
        sendObj.cmd = "drop";
        sendObj.dc = "null";
        this._dropObj = sendObj;
        this._gameRoom.handleUserResp(sendObj);
        this.dropConfirmPopup.removeFromParent(true);
        this.dropConfirmPopup = null;
    },
    dropNoBtnHandler: function () {
        this.dropConfirmPopup.removeFromParent(true);
        this.dropConfirmPopup = null;
        this._dropObj = null;
    },
    showNoBtnHandler: function () {
        //hide showpopUP
        this.showConfirmPopup && this.showConfirmPopup.removeFromParent();
        this._cardToShow.setSelected(false);
        this._cardToShow = null;
        this.reArrangeCards();

        this._selectedCardArray = [];

        this.SortBtn.pauseListener(false);
        if (this._gameRoom._currentTurnID == this._gameRoom._myId) {
            this.activatePanel();
        }

        this.discardBtn.hide(true);
        this.showBtn.hide(true);
        this.showCard.hide(true);
    },
    showCardHandler: function (card) {
        this.discardBtn.hide(true);
        this.showBtn.hide(true);
        this.showCard.hide(true);

        var sendObj = {};
        sendObj.cmd = "show";
        sendObj.dc = card._name;
        var groupCards = this.getGroupCardsStr();
        sendObj.grpCards = groupCards;
        //this.sendGroupCards(groupCards, false);

        this._gameRoom.handleUserResp(sendObj);
        this._isShowReqSent = true;

        card.setPosition(this.showCard.getPosition());
        this.SortBtn.pauseListener(true);
    },
    getGroupCardsStr: function () {
        var str = "";
        var card;
        var allGrpCardsArr = [];
        var allUnGrpCardsArr = [];
        for (var a = 0; a < this.numOfGrps; a++) {
            allGrpCardsArr = allGrpCardsArr.concat(this.grpArr[a]);
        }
        for (var b = this.numOfGrps; b < this.numOfGrps + this.numOfUngrp; b++) {
            allUnGrpCardsArr = allUnGrpCardsArr.concat(this.grpArr[b]);
        }
        for (var i = 0; i < allGrpCardsArr.length; ++i) {
            card = allGrpCardsArr[i];
            str += card._name;
            if (i < allGrpCardsArr.length - 1) {
                if (card.grNo == allGrpCardsArr[i + 1].grNo)
                    str += ",";
                else {
                    str += ";";
                }
            }
        }
        str += "|";
        for (i = 0; i < allUnGrpCardsArr.length; ++i) {
            card = allUnGrpCardsArr[i];
            str += card._name;
            if (i < allUnGrpCardsArr.length - 1) {
                if (card.grNo == allUnGrpCardsArr[i + 1].grNo)
                    str += ",";
                else {
                    str += ":";
                }
            }
        }
        return str;
    },
    hideShowPopUp: function () {

    },
    setPickCard: function (cardName, fromPosition) {

        this.DropBtn.hide(true);
        this._discardClicked = true;
        this.isCardAnimating = true;

        var cardToPosition = this.getPickedCardPos();

        var card = new Card(cardName, false, this);
        card.y = GameConstants.cardsPositionY;
        card.setJoker(this._gameRoom._wildcardStr, this._gameRoom._jokerSymVisble);
        card._cardName = "card" + this.totCards;
        card._isEnabled = true;
        card.setDirtyCard(true);
        card._addListener();

        //new line
        card.setScale(0.87);
        card.setPosition(fromPosition);

        this.addChild(card, GameConstants.cardMinZorder + this.totCards);
        this.cardStack.push(card);

        this.grpArr[this.numOfGrps + this.numOfUngrp - 1].push(card);
        card.grNo = this.numOfGrps + this.numOfUngrp - 1;

        this._discardClicked = false;

        var scaleFactor;
        if (this.totCards == 22)
            scaleFactor = 0.8;
        else
            scaleFactor = 1;

        if (!cc.game._paused) {
            var cardEaseTime = 0.20;
            card.runAction(cc.sequence(
                cc.spawn(
                    cc.moveTo(cardEaseTime, cardToPosition),
                    cc.scaleTo(cardEaseTime, scaleFactor)
                ),
                cc.callFunc(function () {
                    this.unGroupBtnHandler();
                }, this)));
        } else {
            card.setPosition(cardToPosition);
            card.setScale(scaleFactor);
            this.unGroupBtnHandler();
        }
    },
    getNumOfUnGroupCards: function () {
        var count = 0;
        for (var b = this.numOfGrps; b < this.grpArr.length; b++) {
            for (var x = 0; x < this.grpArr[b].length; x++) {
                count += 1;
            }
        }
        return count;
    },
    checkForJokerGrp: function () {
        var count = 0;
        for (var a = 0; a < this.numOfGrps; a++) {
            if (this.grpArr[a].length == 8) {
                count = 0;
                for (var y = 0; y < this.grpArr[a].length; y++) {
                    if (this.grpArr[a][y]._isJoker) {
                        count += 1;
                    }
                }
                if (count == 8)
                    return true;
            }
        }
        return false;
    },
    meldHandler: function () {
        var ungrpd = 0;
        var noOf2CardGrp = 0;
        var meldTxt = "";
        for (var i = this.numOfGrps; i < this.grpArr.length; i++) {
            ungrpd = ungrpd + this.grpArr[i].length;
        }
        if (this.totCards == 22) {
            meldTxt = "This is How your cards are being MELD in group.\nAre you sure you want to declare your cards?";
        }
        else {
            for (var a = 0; a < this.numOfGrps; a++) {
                if (this.grpArr[a].length == 2) {
                    noOf2CardGrp = noOf2CardGrp + 1;
                }
            }
            if (noOf2CardGrp > 0) {
                if (ungrpd > 0) {
                    meldTxt = "In 13 cards Rummy, Minimum of 3 cards are required in a group while melding & " + String(ungrpd) + " of your cards are not grouped.\nAre you sure you want to declare?";
                }
                else {
                    meldTxt = "In 13 cards rummy 2 card group are not valid while melding.\nAre you sure you want to declare your cards?";
                }
            }
            else {
                if (ungrpd > 0) {
                    meldTxt = ungrpd + " of your cards are not grouped!!\nAre you sure you want to declare your cards?";
                }
                else {
                    meldTxt = "This is How your cards are being MELD in group.\nAre you sure you want to declare your cards?";
                }
            }
        }
        this.meldConfirmPopUp && this.meldConfirmPopUp.removeFromParent(true);
        this.meldConfirmPopUp = new MeldConfirm(this._gameRoom.meldingPanel, meldTxt);
        this._gameRoom.addChild(this.meldConfirmPopUp, GameConstants.popupZorder, "meldConfirmPopUp");
    },
    killDistribution: function () {
        SoundManager.removeSound(SoundManager.cardDistribute);
        for (var a = 0; a < this.cardStack.length; a++) {
            this.cardStack[a].stopAllActions();
        }
    },
    onExit: function () {
        this._super();
        this.release();
    }
});

var NormalButton = cc.Sprite.extend({
    ctor: function (name, buttonName, gameType) {
        this._super(spriteFrameCache.getSpriteFrame(name + gameType + '.png'));
        buttonName && this.setName(buttonName);
        return true;
    },
    pauseListener: function (bool) {
        this._isEnabled = !bool;
    },
    hide: function (bool) {
        if (bool)
            this.setLocalZOrder(0);
        else
            this.setLocalZOrder(2);
        this.setVisible(!bool);
        this._isEnabled = !bool;
    }
});