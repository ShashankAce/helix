/**
 * Created by stpl on 11/30/2016.
 */
var Result_21 = cc.LayerColor.extend({
    _gameRoom: null,
    result_pop: null,
    _leaveOk: null,
    ctor: function (params, gameRoomContext) {
        this._super(cc.color(0, 0, 0, 150 * scaleFactor));
        this._gameRoom = gameRoomContext;
        this.result_pop = new Result_Pop(this._gameRoom, params);
        this.result_pop.setPosition(cc.p(this.width / 2, this.height / 2 - 5 * scaleFactor));
        this.addChild(this.result_pop, 2, "result_pop");

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                cc.log("Blank listener in Result_21");

                applicationFacade._controlAndDisplay.closeAllTab();
                this._gameRoom.discardPanel.hideDiscardPanel();
                this._gameRoom.discardButton._isEnabled && this.discardButton.isPressed(false);
                this._gameRoom.settingPanel.setVisible(false);
                this._gameRoom.gameInfoSprite.hidePopup();

                return true;
            }.bind(this)
        }, this);

        return true;
    },
    setWildCard: function (str) {
        this.result_pop.setWildCard(str);
    },
    leaveRoom: function () {
        if (this._leaveOk) {
            var Obj = {isNext: true};
            this._gameRoom.sendReqToServer(SFSConstants.CMD_PLAY_RESP, Obj);
        }
        this.result_pop.stopResultTimer();
    },
    onExit: function () {
        this._super();
        this.result_pop.stopResultTimer();
        if (applicationFacade._controlAndDisplay._21CardRules)
            applicationFacade._controlAndDisplay._21CardRules.removeFromParent(true);
    }
});
var Result_Pop = Popup.extend({
    _name: null,
    _valueCards: null,
    _gameRoom: null,
    _wildCardStr: null,
    waitTxt: null,
    _validityObj: null,
    ccTimeOutManager: null,
    ctor: function (gameRoomContext, params) {
        this._super(new cc.Size(950 * scaleFactor, 590 * scaleFactor), gameRoomContext);
        this.ccTimeOutManager = new CCTimeOutManager();
        this._validityObj = {};
        this._gameRoom = gameRoomContext;
        this._valueCards = [];
        this.waitTxt = null;
        this._wildCardStr = null;

        this.gameType = this._gameRoom._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
        if (this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD)) {
            this._wildCardStr = this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD).value;
        }

        cc.log("WildcardString 21:->" + this._wildCardStr);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        this.headerLeft.setContentSize(cc.size(this.width, 50 * scaleFactor));
        this.headerLabel.setPosition(cc.p(20 * scaleFactor, this.height - this.headerLeft.height / 2));
        this.headerLeft.setAnchorPoint(0.5, 0);
        this.headerLeft.setPosition(cc.p(this.width / 2, this.height - this.headerLeft.height));

        var buttonModel = cc.Sprite.extend({
            ctor: function (label) {
                this._super(spriteFrameCache.getSpriteFrame("ResultGreyButton.png"));

                this.label = new cc.LabelTTF(label, "RobotoBold", 15 * 2 * scaleFactor);
                this.label.setScale(0.5);
                this.label.setPosition(this.width / 2, this.height / 2);
                this.addChild(this.label, 1);

                return true;
            },
            isSelected: function (bool) {
                if (bool) {
                    this.label.setColor(cc.color(255, 255, 255));
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("ResultGreyButton.png"));
                } else {
                    this.label.setColor(cc.color(182, 207, 210));
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("ResultBlueButton.png"));
                }
            }
        });

        this.summaryBtn = new buttonModel("SUMMARY");
        this.summaryBtn.setPosition(cc.p(750, this.height - 39 * scaleFactor));
        this.addChild(this.summaryBtn, 1, "SUMMARY");
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.summaryBtn);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.summaryBtn);

        this.allCardBtn = new buttonModel("ALL CARDS");
        this.allCardBtn.setPosition(cc.p(875, this.height - 39 * scaleFactor));
        this.addChild(this.allCardBtn, 1, "AllCARDS");
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.allCardBtn);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.allCardBtn);

        this.allCardLayer = new AllCardLayer(this, params);
        this.allCardLayer.setAnchorPoint(0, 0);
        this.allCardLayer.setPosition(cc.p(0, 70 * scaleFactor));
        this.addChild(this.allCardLayer, 1);

        this.summaryLayer = new SummaryLayer(this, params);
        this.summaryLayer.setAnchorPoint(0, 0);
        this.summaryLayer.setPosition(cc.p(0, 70 * scaleFactor));
        this.addChild(this.summaryLayer, 1);

        this.loadResult(params);

        var bottomDrawNode = new cc.DrawNode();
        bottomDrawNode.drawRect(cc.p(0, 0), cc.p(this.width - 2, 70 * scaleFactor), cc.color(230, 240, 245), 1, cc.color(230, 240, 245));
        bottomDrawNode.setPosition(cc.p(1 * scaleFactor, 1 * scaleFactor));
        this.addChild(bottomDrawNode, 0);

        var DoNotDealNextBtn = cc.Sprite.extend({
            _result: null,
            _name: "DO_NOT_DEAL_BTN",
            _isChecked: null,
            ctor: function (context) {
                this._super(spriteFrameCache.getSpriteFrame("ResultDonotDeal.png"));
                this._result = context;
                this._isChecked = false;
                return true;
            },
            isSelected: function (bool) {
                if (bool) {
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("ResultDonotDealCheck.png"));
                } else {
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("ResultDonotDeal.png"));
                }
            },
            toggle: function () {
                this._isChecked = !this._isChecked;
                this.isSelected(this._isChecked);
            },
            hide: function (bool) {
                this.setVisible(!bool);
                this._isEnabled = !bool
            },
            resultDonotDealBtnHandler: function () {
                this.toggle();

                var _isNext;
                _isNext = this._isChecked != true;
                var obj = {};
                obj["isNext"] = _isNext;
                this._result._gameRoom.sendReqToServer(SFSConstants.CMD_PLAY_RESP, obj);
            }
        });

        this.waitTxt = new cc.LabelTTF("", "RobotoRegular", 15 * 2 * scaleFactor);
        this.waitTxt.setScale(0.5);
        this.waitTxt.setColor(cc.color(0, 0, 0));
        this.waitTxt.setPosition(cc.p(this.width / 2, 35 * scaleFactor));
        this.addChild(this.waitTxt, 2);

        if (this.gameType.indexOf("Point") >= 0) {
            this.doNotDealNextBtn = new DoNotDealNextBtn(this);
            this.doNotDealNextBtn.setPosition(cc.p(795 * scaleFactor, 35 * scaleFactor));
            this.addChild(this.doNotDealNextBtn, 5);
            this.doNotDealNextBtn.hide(true);

            if (this._gameRoom.GameDonotDealBtn._isChecked)
                this.doNotDealNextBtn.toggle();

            if (this._gameRoom._myId == -1) {
                this.doNotDealNextBtn.hide(true);
            } else {
                if (!this._gameRoom._isWaiting)
                    this.doNotDealNextBtn.hide(false);
            }
            cc.eventManager.addListener(this.popupTouchListener.clone(), this.doNotDealNextBtn);
        }

        this.switchResultPage("SUMMARY");

        this.setResultTime(params);

    },
    setResultTime: function (data) {
        var resultTime = data["resultTime"];
        this.waitTxt && this.waitTxt.setString("Please wait while game starts in " + resultTime + " secs");
        this.ccTimeOutManager.scheduleInterval(xyz.bind(this), 1000, 0, "resultTimer");

        function xyz() {
            if (resultTime > 1) {
                resultTime = resultTime - 1;
                this.waitTxt && this.waitTxt.setString("Please wait while game starts in " + resultTime + " secs");
            }
            else {
                this.ccTimeOutManager.unScheduleInterval("resultTimer");
                this.waitTxt && this.waitTxt.setString("Please wait...");
            }
            if (this.gameType.indexOf("Point") >= 0) {
                if (resultTime == 5 && this._myId != 1 && !this._gameRoom._isWatching && this.doNotDealNextBtn.isVisible()) {
                    var _isNext;
                    _isNext = this.doNotDealNextBtn._isChecked != true;

                    var sObj = {};
                    sObj["isNext"] = _isNext;
                    console.log(sObj);
                    this._gameRoom.sendReqToServer(SFSConstants.CMD_PLAY_RESP, sObj);
                }
            }
        }
    },
    showInvalidDeclaration: function () {

        this.invalidDeclaration = new cc.Sprite(spriteFrameCache.getSpriteFrame("invalidBtn.png"));
        this.invalidDeclaration.setPosition(cc.p(910 * scaleFactor, 35 * scaleFactor));
        this.addChild(this.invalidDeclaration, 5, "invalidDeclaration");

        cc.eventManager.addListener(this.popupMouseListener.clone(), this.invalidDeclaration);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.invalidDeclaration);

    },
    stopResultTimer: function () {
        this.ccTimeOutManager.unScheduleInterval("resultTimer");
    },
    setWildCard: function (wildCrdStr) {

        var gameTypeStr = this._gameRoom._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
        if (gameTypeStr == "Point-NoJoker") {
            // this.joker_txt.setVisible(false);
        }
        else {
            // this.joker_txt.setVisible(true);
        }
        this.addJoker(wildCrdStr);

    },
    addJoker: function (str) {

        var jkr, dwnJkr, upJkr, aceJkr;

        if (str && str != "") {

            var clipper = new ccui.Layout();
            clipper.setContentSize(cc.size(150 * scaleFactor, 75 * scaleFactor));
            clipper.setClippingEnabled(true);
            clipper.setPosition(cc.p(15 * scaleFactor, 1 * scaleFactor));
            this.addChild(clipper, 4);

            var down_joker_label = new cc.LabelTTF("DOWN", "RobotoBold", 10 * 2 * scaleFactor);
            down_joker_label.setScale(0.5);
            down_joker_label.setColor(cc.color(0, 0, 0));
            clipper.addChild(down_joker_label, 4);

            var joker_label = new cc.LabelTTF("JOKER", "RobotoBold", 10 * 2 * scaleFactor);
            joker_label.setScale(0.5);
            joker_label.setColor(cc.color(0, 0, 0));
            clipper.addChild(joker_label, 4);

            var up_joker_label = new cc.LabelTTF("UP", "RobotoBold", 10 * 2 * scaleFactor);
            up_joker_label.setScale(0.5);
            up_joker_label.setColor(cc.color(0, 0, 0));
            clipper.addChild(up_joker_label, 4);

            jkr = new CardModel(str);
            jkr.setJoker(this._wildCardStr, false);
            jkr.setScale(0.6);

            dwnJkr = new CardModel(jkr._dwnJkr);
            dwnJkr.setJoker(this._wildCardStr, false);
            dwnJkr.setScale(0.5);

            upJkr = new CardModel(jkr._upJkr);
            upJkr.setJoker(this._wildCardStr, false);
            upJkr.setScale(0.5);

            if (str == "Joker") {
                clipper.setContentSize(cc.size(200 * scaleFactor, 70 * scaleFactor));

                dwnJkr.setPosition(cc.p(25 * scaleFactor, 13 * scaleFactor));
                jkr.setPosition(cc.p(75 * scaleFactor, 20 * scaleFactor));
                upJkr.setPosition(cc.p(175 * scaleFactor, 13 * scaleFactor));

                down_joker_label.setPosition(cc.p(25 * scaleFactor, 46 * scaleFactor));
                joker_label.setPosition(cc.p(75 * scaleFactor, 59 * scaleFactor));
                up_joker_label.setPosition(cc.p(175 * scaleFactor, 46 * scaleFactor));

                var ace_joker_label = new cc.LabelTTF("ACE", "RobotoBold", 10 * 2 * scaleFactor);
                ace_joker_label.setScale(0.5);
                ace_joker_label.setColor(cc.color(0, 0, 0));
                ace_joker_label.setPosition(cc.p(125 * scaleFactor, 59 * scaleFactor));
                clipper.addChild(ace_joker_label, 4);

                aceJkr = new CardModel("S#A");
                aceJkr.setScale(0.6);
                aceJkr.setPosition(cc.p(125 * scaleFactor, 20 * scaleFactor));
                clipper.addChild(aceJkr, 1);

            } else {

                down_joker_label.setPosition(cc.p(25 * scaleFactor, 46 * scaleFactor));
                joker_label.setPosition(cc.p(75 * scaleFactor, 59 * scaleFactor));
                up_joker_label.setPosition(cc.p(125 * scaleFactor, 46 * scaleFactor));

                dwnJkr.setPosition(cc.p(25 * scaleFactor, 13 * scaleFactor));
                jkr.setPosition(cc.p(75 * scaleFactor, 20 * scaleFactor));
                upJkr.setPosition(cc.p(125 * scaleFactor, 13 * scaleFactor));
            }

            clipper.addChild(dwnJkr, 1);
            clipper.addChild(jkr, 1);
            clipper.addChild(upJkr, 1);

        }
    },
    loadResult: function (params) {
        this.headerLabel.setString("Result - Table ID: " + this._gameRoom.roomId + " | Round Time: " + params["rndTime"] + " s.");

        /*this.namePopup = new UserNamePlaceHolder("blank");
         this.namePopup.setPosition(cc.p(132, 450));
         this.addChild(this.namePopup, 10);*/

        /*this.jokerNode = this.resultNode.getChildByName("jokerHolder").getChildByName("jokerNode");
         this.jokerNode.removeAllChildren(true);
         var sprite = new CardModel("S#A" || this._gameRoom.deckPanel.wildCardName);
         sprite.setScale(0.6);
         this.jokerNode.addChild(sprite);*/

    },
    /*onMouseMoveCallBack: function (event) {
     var target = event.getCurrentTarget();
     var locationInNode = target.convertToNodeSpace(event.getLocation());
     var targetSize = target.getContentSize();
     var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
     if (cc.rectContainsPoint(targetRect, locationInNode)) {
     switch (target._name) {
     case "resultList":
     var name = target.playerName.name;
     this.initNamePopup(true, name, target.y);
     break;
     default:
     this.initNamePopup(!1);
     }
     return true;
     }
     return !1;
     },*/
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        switch (target._name) {
            case "SUMMARY":
                this.switchResultPage(target._name);
                break;
            case "AllCARDS" :
                this.switchResultPage(target._name);
                break;
            case "DO_NOT_DEAL_BTN" :
                this.doNotDealNextBtn.resultDonotDealBtnHandler();
                break;
            case "invalidDeclaration":
                this.showDeclareInfo();
                cc.log("Invalid declaration clicked");
                break;
        }
    },
    showDeclareInfo: function () {
        this.invalidDeclr && this.invalidDeclr.removeFromParent();
        this.invalidDeclr = new InvalidDeclaration(this._gameRoom);
        this.invalidDeclr._init(this._validityObj);
        this.addChild(this.invalidDeclr, 10, "invalidDeclr");
    },
    show21CardRule: function () {
        applicationFacade._controlAndDisplay.show21CardRules();
    },
    initNamePopup: function (bool, name, posY) {
        /*if (bool) {
         this.namePopup.setVisible(true);
         this.namePopup.nameString.setString(name);
         this.namePopup.setPositionY(posY);
         } else {
         this.namePopup.setVisible(false);
         }*/
    },
    switchResultPage: function (type) {
        if (type == "SUMMARY") {
            this.summaryBtn.isSelected(true);
            this.allCardBtn.isSelected(false);

            this.summaryLayer.setVisible(true);
            this.allCardLayer.setVisible(false);

        } else {
            this.allCardBtn.isSelected(true);
            this.summaryBtn.isSelected(false);

            this.allCardLayer.setVisible(true);
            this.summaryLayer.setVisible(false);
        }
    },
    /*toggleDeal: function () {
     this.doNotDealNextBtn.toggle();
     }*/
});