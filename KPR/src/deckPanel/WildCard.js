/**
 * Created by stpl on 11/30/2016.
 */
'use strict';
var WildCard = cc.Node.extend({
    _name: "WildCard",
    _joker: null,
    _deckPanel: null,
    _isEnabled: null,
    _gameRoom: null,
    ctor: function (context, _gameRoom) {
        this._super();
        this._gameRoom = _gameRoom;
        this._deckPanel = context;

        this.setPosition(cc.p(this.width / 2, this.height / 2));
        this.dummyCard = new cc.Sprite(spriteFrameCache.getSpriteFrame("selected_state.png"));
        this.setContentSize(this.dummyCard.getContentSize());
        this.addChild(this.dummyCard, 3);

        return true;
    },
    clearAll: function () {
        this._joker && this._joker.removeFromParent(true);
        this._joker = null;

        this.jokr_label && this.jokr_label.removeFromParent(true);
        this.jokr_label = null;
    },
    initJoker: function (cardName, context) {
        this.clearAll();
        this.removeToolTip();

        if (!cardName || cardName == "") {
            return;
        }
        this.jokr_label = new cc.LabelTTF("Joker", "RobotoRegular", 24);
        this.jokr_label.setScale(0.5);
        this.jokr_label.setPosition(cc.p(-2, this.height / 2 + 5));
        this.setName("Joker");
        this.addChild(this.jokr_label, 1);
        this.dummyCard.removeFromParent(true);

        this._deckPanel._wildCardName = cardName;

        this._joker = new CardModel(cardName);
        this._joker.setScale(0.8);

        this.toolTipButtonSprite = new cc.Sprite(spriteFrameCache.getSpriteFrame("toolTipButton.png"));
        this.toolTipButtonSprite.setPosition(cc.p(this._joker.width / 2 + 20, this._joker.height / 2 + 30));
        this.toolTipButtonSprite.hover = function (bool) {
            if (this._isEnabled)
                this.toolLayout.setVisible(bool);
        }.bind(this);

        this._joker.addChild(this.toolTipButtonSprite, 2, "toolTipButtonSprite");
        this.addChild(this._joker);

        this.createToolTip();
        cc.eventManager.addListener(this._deckPanel.deckPanelMouseListener.clone(), this.toolTipButtonSprite);
    },
    createToolTip: function () {
        this.toolLayout && this.toolLayout.removeFromParent(true);
        this.toolLayout = null;

        this.toolLayout = new JokerInfoSprite(this._gameRoom, this);
        this.toolLayout.setPosition(360, 420 - this.toolLayout.height / 2);
        this._deckPanel.addChild(this.toolLayout, GameConstants.jokerToolTipZorder);
    },
    removeToolTip: function () {
        this.toolTipButtonSprite && this.toolTipButtonSprite.removeFromParent(true);
        this.toolTipButtonSprite = null;
        this.toolLayout && this.toolLayout.removeFromParent(true);
        this.toolLayout = null;
    },
    removeJoker: function () {
        this._joker && this._joker.removeFromParent(true);
        this._joker = null;
    },
    hide: function (bool) {
        this.setVisible(!bool);
        this._isEnabled = !bool;
    },
    pauseListener: function (bool) {
        this._isEnabled = !bool;
        this.toolTipButtonSprite && this.toolTipButtonSprite.pauseListener(bool);
    }
});

var JokerInfoSprite = cc.Scale9Sprite.extend({
    _wildCardName: null,
    _hovering: null,
    _wildCrd: null,
    ctor: function (gameRoom, wildCard) {
        this._super(spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png"), cc.rect(0, 0, 50, 50));
        this._hovering = false;
        this._gameRoom = gameRoom;
        this._wildCrd = wildCard;

        this.setCapInsets(cc.rect(16, 16, 16, 16));
        this.setColor(cc.color(0, 0, 0));
        this.setContentSize(cc.size(350, 115));
        this.setCascadeColorEnabled(false);
        this.setVisible(false);

        this.arrowSprite = new cc.Sprite(spriteFrameCache.getSpriteFrame("arrow.png"));
        this.arrowSprite.setRotation(90);
        this.arrowSprite.setColor(cc.color(0, 0, 0));
        this.arrowSprite.setPosition(cc.p(cc.p(this.width / 2, this.height)));
        this.addChild(this.arrowSprite);

        var label = new cc.LabelTTF("Joker Cards shown can be used in place of any other card", "RobotoRegular", 8 * 2);
        label.setScale(0.75);
        label.setPosition(cc.p(this.width / 2, this.height / 2 + this.height / 4 + 8));
        this.addChild(label, 1);

        this.setJokerInfo();

        return true;
    },
    setJokerInfo: function () {
        var crdsStr;
        if (this._wildCrd._joker._cardStr == "Joker")
            crdsStr = "A";
        else
            crdsStr = this._wildCrd._joker._cardStr.split("#")[1];
        var suitArr = ["S", "H", "C", "D"];

        var margin = 5, width;
        if (this._gameRoom.totCards == 21) width = this.width / (suitArr.length + 5);
        else width = this.width / (suitArr.length);

        var card = new DummyCard("Joker");
        this.addChild(card);
        card.setPosition(cc.p(width, this.height / 2 - 10));
        width += card.width + margin;

        var crdPos = 0;
        for (var i = 0; i < suitArr.length; i++) {
            card = new DummyCard(suitArr[i] + "#" + crdsStr);
            /*crdPos += card.width + 5;
             card.x = crdPos;*/
            card.setPosition(cc.p(width, this.height / 2 - 10));
            width += card.width + margin;
            this.addChild(card);
        }
        if (this._gameRoom.totCards == 21) {
            card = new DummyCard(this._wildCrd._joker._dwnJkr);
            card.setJoker(this._wildCrd._joker._cardStr, this._gameRoom._jokerSymVisble);
            /*crdPos += card.width + 5;
             card.x = crdPos;*/
            card.setPosition(cc.p(width, this.height / 2 - 10));
            width += card.width + margin;
            this.addChild(card);

            card = new DummyCard(this._wildCrd._joker._upJkr);
            card.setJoker(this._wildCrd._joker._cardStr, this._gameRoom._jokerSymVisble);
            /*crdPos += card.width + 5;
             card.x = crdPos;*/
            card.setPosition(cc.p(width, this.height / 2 - 10));
            width += card.width + margin;
            this.addChild(card);
        }
    }
});