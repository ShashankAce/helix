/**
 * Created by stpl on 11/16/2016.
 */
var Result_13 = cc.LayerColor.extend({
    _gameRoom: null,
    _leaveOk: null,
    splitData: null,
    playerInfo: null,
    resultTime: null,
    timerText: null,
    resultNode: null,
    playerHeader: null,
    resultHeader: null,
    gameScoreHeader: null,
    totalSoreHeader: null,
    SplitAndRejoinBtn: null,

    ctor: function (params, gameRoomContext, isLastResult) {
        this._super(cc.color(0, 0, 0, 150));
        this._gameRoom = gameRoomContext;

        this.playerInfo = [];
        this.splitData = null;
        this.gameType = this._gameRoom._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
        this.resultTime = params["resultTime"];

        this.ccTimeOutManager = new CCTimeOutManager();

        this.resultNode = new Result_13Pop(this);
        this.resultNode.setPosition(this.width / 2, this.height / 2 - 5 * scaleFactor);
        this.addChild(this.resultNode, 1, "resultNode");

        this.totalSoreHeader = this.resultNode.totalScoreHeader;

        this.roundHeader = this.resultNode.roundLabel;
        // this.roundTimeHeader = this.resultNode.roundTimeLabel;

        if (params["pl1"].hasOwnProperty("totSc")) {
            this.totalSoreHeader.setString("Total Score");
        } else {
            this.totalSoreHeader.setString("Chips");
        }

        this.timerText = this.resultNode.timerText;

        var Button = CreateBtn.extend({
            _symbolName: null,
            ctor: function (spriteName, spriteOverName, buttonName) {
                this._super(spriteName, spriteOverName, buttonName);
                this._isEnabled = false;
                this.setName(buttonName);
                this.setVisible(false);
            },
            setState: function (spriteName, spriteOverName, buttonName) {
                this._isEnabled = true;
                this.setVisible(true);
                this.spriteName = spriteName;
                this.spriteOverName = spriteOverName;
                this.setName(buttonName);
                this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteName + ".png"));
            },
            hide: function (bool) {
                this.setVisible(!bool);
                this._isEnabled = !bool;
            }
        });

        this.SplitAndRejoinBtn = new Button("", "", "");
        this.SplitAndRejoinBtn.setPosition(cc.p(870, 68));
        this.addChild(this.SplitAndRejoinBtn, 2);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        if (this.gameType.indexOf("Point") >= 0) {
            this.ResDonotDeal = new DonotDealSprite(this);
            this.ResDonotDeal.setPosition(cc.p(800, 68));
            this.addChild(this.ResDonotDeal, 3);
            this.ResDonotDeal.hide(true);

            if (this._gameRoom.GameDonotDealBtn._isChecked)
                this.ResDonotDeal.initBtnState();

            if (this._gameRoom._myId == -1) {
                this.ResDonotDeal.hide(true);
            } else {
                if (!this._gameRoom._isWaiting)
                    this.ResDonotDeal.hide(false);
            }

            cc.eventManager.addListener(this.resultLayerMouseListener.clone(), this.ResDonotDeal);
            cc.eventManager.addListener(this.resultLayerTouchListener.clone(), this.ResDonotDeal);
        }

        this.loadResult(params, isLastResult);
        this.addList(params);

        if (!isLastResult)
            this.setResultTime(params);

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                cc.log("Blank listener in Result_13");

                applicationFacade._controlAndDisplay.closeAllTab();
                this._gameRoom.discardPanel.hideDiscardPanel();
                this._gameRoom.discardButton._isEnabled && this._gameRoom.discardButton.isPressed(false);
                this._gameRoom.settingPanel.setVisible(false);
                this._gameRoom.gameInfoSprite.hidePopup();

                return true;
            }.bind(this)
        }, this);

    },
    loadResult: function (params, isLastResult) {

        this.resultNode.headerLabel.setString("Result - Table ID: " + this._gameRoom.roomId);

        var round;
        if (isLastResult)
            round = this._gameRoom._currentRound - 1;
        else
            round = this._gameRoom._currentRound;

        /*if (this.gameType.indexOf("Point") < 0) {
         this.resultNode.setRoundNo(round);
         }*/

        var roundTime = params.rndTime + " s.";
        this.resultNode.setRoundNoAndTime(round, roundTime);

        /*this.namePopup = new UserNamePlaceHolder("blank");
         this.namePopup.setPosition(cc.p(132, 450));
         this.addChild(this.namePopup, 10);*/

    },
    setWildCard: function (wildCrd) {
        this.resultNode.addJoker(wildCrd);
    },
    setResultTime: function (data) {
        var resultTime = data["resultTime"];
        var text = "Please wait while game starts in ";
        this.resultNode.timerText.setString(text + resultTime + " secs");
        this.ccTimeOutManager.scheduleInterval(xyz.bind(this), 1000, 0, "resultTimer");
        function xyz() {

            if (resultTime > 1) {
                resultTime = resultTime - 1;
                this.resultNode.timerText.setString(text + resultTime + " secs");
            }
            else {
                this.ccTimeOutManager.unScheduleInterval("resultTimer");
                this.resultNode.timerText.setString("Please wait...");
            }

            if (this.gameType.indexOf("Point") >= 0) {
                if (resultTime == 5 && this._myId != 1 && !this._gameRoom._isWatching && this.ResDonotDeal.isVisible()) {
                    var _isNext;
                    _isNext = this.ResDonotDeal._isChecked != true;

                    var sObj = {};
                    sObj["isNext"] = _isNext;
                    console.log(resultTime);
                    console.log(sObj);
                    this._gameRoom.sendReqToServer(SFSConstants.CMD_PLAY_RESP, sObj);
                }
                /* if (resultTime == 0 && this._gameRoom._serverRoom.==ServerCommunication.getInstance()._gameRoomName)
                 {
                 this._gameRoom._leaveAllowed = false;
                 //trace("_gameRoom._leaveAllowed"+_gameRoom._leaveAllowed)
                 }*/
            }
            // TODO req in point game
            /* if (resultTime == 5) {
             if (this.gameType.split("-")[0] == sfsClient.TAG_POINTS) {
             sfsClient.playResp(KHEL_CONSTANTS.IS_NEXT_POINTS);
             }
             }*/
        }
    },
    stopResultTimer: function () {
        this.ccTimeOutManager.unScheduleInterval("resultTimer");
    },
    getResultPlayerInfo: function (params) {
        this.playerInfo = [];
        for (var i = 1; i <= parseInt(params.tot); i++) {
            var resultPlsInfo = new ResultPlInfo();
            var playerId = params["pl" + i]["id"];
            var isRejoin = params["pl" + i][SFSConstants.FLD_REJOIN];
            var isSplit = params["pl" + i]["isSplit"];
            var meldCards = params["pl" + i]["meldCards"];
            var name = params["pl" + i]["name"];
            var resultType = params["pl" + i]["resultType"];
            var roundScore = params["pl" + i]["rndSc"];
            var totalScore = 0;
            if (params["pl" + i]["totSc"])
                totalScore = (params["pl" + i]["totSc"]);
            var amount = null;
            if (params["pl" + i][SFSConstants.FLD_AMOUNT])
                amount = params["pl" + i][SFSConstants.FLD_AMOUNT];

            resultPlsInfo.setResultPlInfo(name, meldCards, resultType, roundScore, isSplit, totalScore, playerId, amount, isRejoin);
            this.playerInfo.push(resultPlsInfo);

            var player = this._gameRoom.getSeatObjByUserName(name);
            if (player) {
                player.player_score = totalScore;
                player.updateScore(totalScore);
            }
        }
    },
    addList: function (params) {
        this.getResultPlayerInfo(params);
        for (var k = 0; k < parseInt(params.tot); k++) {
            if (this.playerInfo[k].getPlayerId() == this._gameRoom._myId) {
                if (this.playerInfo[k].getIsSplit()) {
                    if (this.checkForSplitButton(params["pl" + (k + 1)]["isSplit"])) {
                        this.SplitAndRejoinBtn.setState("SplitBtn", "SplitBtnOver", "SplitBtn"); // visible true
                        this.SplitAndRejoinBtn.hide(false);
                        this._gameRoom.isSplitVisible = true;
                        this.splitData = params["pl" + (k + 1)]["isSplit"] /*this.playerInfo[k].getIsSplit()*/;
                    }
                }
                if (this.playerInfo[k].getIsRejoin() == true && !this._gameRoom._isWatching) { // visible rejoin button
                    this.SplitAndRejoinBtn.setState("rebuy", "rebuyOver", "ReJoinBtn");
                    this.SplitAndRejoinBtn.hide(false);
                }
            }
        }
        for (var i = 0; i < parseInt(params.tot); i++) {
            var resultStrip = new ResultStrip13(this, params["pl" + (i + 1)], i);
            var dy = 457 * scaleFactor - i * 70 * scaleFactor;
            resultStrip.setPosition(cc.p(this.resultNode.width / 2, dy - i));
            this.resultNode.addChild(resultStrip, 5);
            cc.eventManager.addListener(this.resultLayerMouseListener.clone(), resultStrip);
            cc.eventManager.addListener(this.resultLayerTouchListener.clone(), resultStrip);
        }
    },
    checkForSplitButton: function (splitStr) {
        if (!splitStr || splitStr == "")
            return false;
        var str = splitStr.split(",");
        for (var i = 0; i < str.length; i++) {
            var str1 = str[i].split(":");
            if (str1[0] == (this._gameRoom._myId + "")) {
                // TODO visible split button
                return true;
            }
        }
        return false;
    },
    initMouseListener: function () {
        this.resultLayerMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            swallowTouches: true,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this)
        });
        this.resultLayerMouseListener.retain();
        cc.eventManager.addListener(this.resultLayerMouseListener, this.SplitAndRejoinBtn);
        // cc.eventManager.addListener(this.resultLayerMouseListener.clone(), this.reJoinBtn);
    },
    initTouchListener: function () {
        this.resultLayerTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                switch (target._name) {
                    case "resultList":
                        var name = target.playerName.name;
                        this.initNamePopup(true, name, target.y);
                        break;
                    case "SplitBtn" :
                        if (target._isEnabled)
                            this.splitBtnHandler();
                        break;
                    case "ReJoinBtn" :
                        if (target._isEnabled)
                            this.reJoinBtnHandler();
                        break;
                    case "ResultDonotDealBtn" :
                        if (target._isEnabled)
                            this.ResDonotDeal.resultDonotDealBtnHandler();
                        break;
                    case "closeBtn" :
                        this._gameRoom.closeResult();
                        break;
                }
            }.bind(this)
        });
        this.resultLayerTouchListener.retain();
        cc.eventManager.addListener(this.resultLayerTouchListener, this.SplitAndRejoinBtn);
        // cc.eventManager.addListener(this.resultLayerTouchListener.clone(), this.reJoinBtn);
    },
    initCloseBtn: function () {
        var closeBtn = new cc.Sprite(spriteFrameCache.getSpriteFrame("ResultClose.png"));
        closeBtn.setPosition(cc.p(this.resultNode.width - 3 * scaleFactor, this.resultNode.height - 3 * scaleFactor));
        this.resultNode.addChild(closeBtn, 20, "closeBtn");
        cc.eventManager.addListener(this.resultLayerMouseListener.clone(), closeBtn);
        cc.eventManager.addListener(this.resultLayerTouchListener.clone(), closeBtn);
    },
    splitBtnHandler: function () {
        this.splitPopup = new SplitPanel(this, this.splitData);
        this.splitPopup.setPosition(this._gameRoom.width / 2, this._gameRoom.height / 2);
        this._gameRoom.addChild(this.splitPopup, GameConstants.splitZorder, "splitPopup");
        this.SplitAndRejoinBtn.hide(true);
    },
    reJoinBtnHandler: function () {
        this.SplitAndRejoinBtn.hide(true);

        var sentObj = {};
        this._gameRoom.sendReqToServer("rejoin", sentObj);
    },
    initNamePopup: function (bool, name, posY) {
        /*if (bool) {
         this.namePopup.setVisible(true);
         this.namePopup.nameString.setString(name);
         this.namePopup.setPositionY(posY);
         } else {
         this.namePopup.setVisible(false);
         }*/
    },
    leaveRoom: function () {
        if (this._leaveOk) {
            var Obj = {};
            Obj.isNext = true;
            this._gameRoom.sendReqToServer(SFSConstants.CMD_PLAY_RESP, Obj);
        }
        this.stopResultTimer();
    },
    onExit: function () {
        this._super();
        this.stopResultTimer();
    }
});

var ResultStrip13 = ccui.Layout.extend({
    _name: "resultList",
    _index: null,
    _result: null,
    ctor: function (context, Ginfo, index) {
        this._super();
        this._index = index;
        this._result = context;

        var Info = Ginfo;
        this.setContentSize(cc.size(858 * scaleFactor, 69 * scaleFactor));
        this.setAnchorPoint(0.5, 0.5);
        this.setLocalZOrder(0);
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);

        /*if (index % 2 == 0)
         this.setBackGroundColor(cc.color(227, 227, 227));
         else
         this.setBackGroundColor(cc.color(245, 245, 245));*/

        this.setBackGroundColor(cc.color(245, 245, 245));
        if (applicationFacade._myId == Info["id"])
            this.setBackGroundColor(cc.color(227, 227, 227));

        var line = new cc.DrawNode();
        line.drawSegment(cc.p(115 * scaleFactor, this.height), cc.p(115 * scaleFactor, 0), 0.5, cc.color(220, 220, 220));
        this.addChild(line);

        var line2 = new cc.DrawNode();
        line2.drawSegment(cc.p(214 * scaleFactor, this.height), cc.p(214 * scaleFactor, 0), 0.5, cc.color(220, 220, 220));
        this.addChild(line2);

        var line3 = new cc.DrawNode();
        line3.drawSegment(cc.p(594 * scaleFactor, this.height), cc.p(594 * scaleFactor, 0), 0.5, cc.color(220, 220, 220));
        this.addChild(line3);

        var line4 = new cc.DrawNode();
        line4.drawSegment(cc.p(724 * scaleFactor, this.height), cc.p(724 * scaleFactor, 0), 0.5, cc.color(220, 220, 220));
        this.addChild(line4);

        var bottomLine = new cc.DrawNode();
        bottomLine.drawSegment(cc.p(1, 0), cc.p(this.width, 0), 0.5, cc.color(220, 220, 220));
        this.addChild(bottomLine);

        this.playerName = new PlayerName(Info.name, this);
        this.playerName.setPosition(cc.p(this.width / 16, this.height / 2));
        this.addChild(this.playerName, 2);

        var prizeIcon, prizeText = "";
        if (Info.resultType != "") {
            switch (Info.resultType) {
                case "drop":
                    prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("DropTableIcon.png"));
                    prizeText = "Dropped";
                    break;
                case "Left":
                    prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("LeftTableIcon.png"));
                    prizeText = "Left";
                    break;
                case "wrongShow" :
                    prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("wrongShow.png"));
                    prizeText = "WrongShow";
                    break;
                case "validShow" :
                    prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("ValidShowIcon.png"));
                    prizeText = "ValidShow";
                    break;
            }
        }
        if (String(Info.resultType).toLowerCase() == "winner" || this._result.gameType.indexOf("Point") >= 0 && Info.resultType == "" && Info['rndSc'] == 0) {
            prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("prize.png"));
            prizeText = "Winner";
            this.setBackGroundColor(cc.color(231, 239, 188));
        }

        if (prizeIcon) {
            prizeIcon.setPosition(cc.p(this.width / 8 + this.width / 16, this.height / 2 + 10 * scaleFactor));
            this.addChild(prizeIcon);
        } else {
            cc.log("Prize information not available");
        }

        var prizeLabel = new cc.LabelTTF(prizeText, "RobotoRegular", 2 * 12 * scaleFactor);
        prizeLabel.setScale(0.5);
        prizeLabel.setAnchorPoint(0.5, 0.5);
        prizeLabel.setColor(cc.color(0, 0, 0));
        prizeLabel.setPosition(cc.p(this.width / 8 + this.width / 16, this.height / 3 - 10 * scaleFactor));
        this.addChild(prizeLabel);

        var gameScore = new cc.LabelTTF((Info.rndSc).toString(), "RobotoRegular", 14 * 2 * scaleFactor);
        gameScore.setScale(0.5);
        gameScore.setColor(cc.color(0, 0, 0));
        gameScore.setPosition(cc.p(659 * scaleFactor, this.height / 2));
        this.addChild(gameScore);

        var totalScore = new cc.LabelTTF("", "RobotoRegular", 14 * 2 * scaleFactor);
        totalScore.setScale(0.5);
        totalScore.setColor(cc.color(0, 0, 0));
        totalScore.setPosition(cc.p(792 * scaleFactor, this.height / 2));
        this.addChild(totalScore);

        if (Info.hasOwnProperty("totSc")) {
            totalScore.setString((Info.totSc).toString());  // in case of pool
        } else {
            totalScore.setString((Info.amt).toString());  // in case of point
        }

        var meldCardString = Info.meldCards;
        if (meldCardString != "null") {
            this.addMeldCard(meldCardString);
        }
        return true;
    },
    addMeldCard: function (cardsString) {
        var groupArray,
            unGroupArray,
            tmpCards,
            cardsPipe;
        cc.log(cardsString);

        if (cardsString.indexOf("|") == -1) {
            unGroupArray = cardsString.split(":");
            groupArray = [];
        }
        else {
            cardsPipe = (cardsString).split("|");
            groupArray = cardsPipe[0].split(";");
            unGroupArray = cardsPipe[1].split(":");
        }
        var card;
        var xInt = 250 * scaleFactor, cardCount = 0;
        for (var i = 0; i < groupArray.length; i++) {
            if (groupArray[i] != "") {
                tmpCards = groupArray[i].split(",");
                for (var j = 0; j < tmpCards.length; j++) {
                    card = new DummyCard(tmpCards[j]);
                    card.setTag(i);
                    card.x = xInt;
                    card.y = this.height / 2;
                    xInt = xInt + 14 * scaleFactor;
                    ++cardCount;
                    this.addChild(card, cardCount);
                }
                xInt = card.x + 50 * scaleFactor;
            }
        }
        for (var i = 0; i < unGroupArray.length; i++) {
            if (unGroupArray[i] != "") {
                tmpCards = unGroupArray[i].split(",");
                for (var j = 0; j < tmpCards.length; j++) {
                    card = new DummyCard(tmpCards[j]);
                    card.setTag(i);
                    card.x = xInt;
                    card.y = this.height / 2;
                    xInt = xInt + 14 * scaleFactor;
                    ++cardCount;
                    this.addChild(card, cardCount);
                }
            }
        }
    }
});

var UserNamePlaceHolder = cc.Scale9Sprite.extend({
    ctor: function (playerName) {
        this._super(spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png"), cc.rect(0, 0, 50 * scaleFactor, 50 * scaleFactor));
        this.setVisible(false);

        var pointer = new cc.Sprite(spriteFrameCache.getSpriteFrame("arrowBorder.png"));
        pointer.setAnchorPoint(cc.p(0.5, 0));
        pointer.setRotation(-90);
        pointer.setPosition(cc.p(this.width, this.height - 5 * scaleFactor));
        this.addChild(pointer, 2);

        this.nameString = new cc.LabelTTF(playerName, "RobotoRegular", 2 * 13 * scaleFactor); //Full player name to be displayed in placeholder
        this.nameString.setScale(0.5);
        this.setContentSize(this.nameString.width + 60 * scaleFactor, this.nameString.height);
        this.nameString.setColor(cc.color(255, 255, 255));
        this.nameString.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(this.nameString, 3);

        return true;
    }
});

var PlayerName = cc.LabelTTF.extend({
    name: null,
    _resultList: null,
    ctor: function (playerName, context) {
        if (!playerName) {
            playerName = "default";
        }
        var name;
        if (playerName.length > 12) {
            name = playerName.substring(0, 12) + "..";
            this.name = playerName;
        } else {
            name = playerName;
            this.name = playerName;
        }

        this._super(name, "RobotoRegular", 14 * 2 * scaleFactor);
        this._resultList = context;

        this.setScale(0.5);
        this.setColor(cc.color(0, 0, 0));
        return true;
    }
});

var DonotDealSprite = cc.Sprite.extend({
    _result: null,
    ctor: function (context) {
        this._super(spriteFrameCache.getSpriteFrame("ResultDonotDeal.png"));

        this._result = context;
        this.setName("ResultDonotDealBtn");

        this._isEnabled = true;
        this._isChecked = false;
        this.setVisible(true);

        return true;
    },
    initBtnState: function () {
        this._isChecked = true;
        this._isChecked ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("ResultDonotDealCheck.png")) : this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("ResultDonotDeal.png"));
    },
    hide: function (bool) {
        this.setVisible(!bool);
        this._isEnabled = !bool
    },
    hideToggle: function () {
        // this._isEnabled = !this._isEnabled;
        this._isChecked = !this._isChecked;
    },
    resultDonotDealBtnHandler: function () {
        this.hideToggle();
        this._isChecked ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("ResultDonotDealCheck.png")) : this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("ResultDonotDeal.png"));

        var _isNext;
        _isNext = this._isChecked != true;
        var obj = {};
        obj["isNext"] = _isNext;
        console.log(obj);
        this._result._gameRoom.sendReqToServer(SFSConstants.CMD_PLAY_RESP, obj);
    }
});