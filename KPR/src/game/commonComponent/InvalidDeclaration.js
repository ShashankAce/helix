/**
 * Created by stpl on 12/16/2016.
 */
"use strict";
var InvalidDeclaration = Popup.extend({
    _name: "InvalidDeclaration",

    SpaceBWCard: null,
    MaxSlotsOneSide: null,
    _gameRoom: null,
    _selectedCardArray: null,
    grpArr: null,
    cardsGroupArr: null,
    _cardsToMove: null,
    _changedGrps: null,
    i: null,
    j: null,
    _initX: null,
    _initY: null,
    wildCrd: null,
    _groupSprite: null,
    totCards: null,
    numOfGrps: null,
    numOfUngrp: null,
    minNumOfCardIngrp: null,
    maxNumOfCardIngrp: null,
    _wildCardstr: null,
    _groupY: null,
    impureCount: null,
    pureCount: null,
    setCount: null,
    tunnelasCount: null,
    doubleesCount: null,
    jokerCount: null,
    numOfJokers: null,
    maxJokersInGrp: null,
    cardStack: null,
    defaultYPos: null,
    _startPos: null,
    startP: null,
    endP: null,
    _hovering: null,

    ctor: function (_gameRoom) {
        this._super(new cc.Size(860 * scaleFactor, 380 * scaleFactor), _gameRoom);

        this._hovering = false;
        this.SpaceBWCard = 30;
        this.MaxSlotsOneSide = 2;
        this._selectedCardArray = [];
        this.grpArr = [];
        this.cardsGroupArr = [];
        this._cardsToMove = null;
        this._changedGrps = [];
        this.i = 0;
        this.j = 0;
        this._initX = 0;
        this._initY = 0;
        this.wildCrd = null;
        this._groupSprite = null;
        this.totCards = 22;
        this.numOfGrps = 4;
        this.numOfUngrp = 5;
        this.minNumOfCardIngrp = 3;
        this.maxNumOfCardIngrp = 5;
        this._wildCardstr = "";
        this._groupY = 45;
        this.impureCount = 0;
        this.pureCount = 0;
        this.setCount = 0;
        this.tunnelasCount = 0;
        this.doubleesCount = 0;
        this.jokerCount = 0;
        this.numOfJokers = 0;
        this.maxJokersInGrp = 0;
        this.cardStack = [];
        this.defaultYPos = 230;
        this._startPos = cc.p(0, 0);
        this.startP = cc.p(0, 0);
        this.endP = cc.p(this.width, 0);

        this.headerLabel.setString("Invalid Declaration");

        this.closeBtn = new cc.Sprite(spriteFrameCache.getSpriteFrame("ResultClose.png"));
        this.closeBtn.setPosition(cc.p(this.width, this.height));
        this.addChild(this.closeBtn, 50, "closeBtn");

        this.txt = new cc.LabelTTF("Need help how to declare valid show?", "RobotoRegular", 18 * 2 * scaleFactor, cc.TEXT_ALIGNMENT_CENTER);
        this.txt.setScale(0.5);
        this.txt.setColor(cc.color(69, 69, 69));
        this.txt.setPosition(cc.p(this.width / 2, 90 * scaleFactor));
        this.addChild(this.txt, 2);

        this.rule_btn = new CreateBtn("ViewRuleBtn", "ViewRuleBtnOver", "rule_btn");
        this.rule_btn.setPosition(cc.p(this.width / 2, 50 * scaleFactor));
        this.addChild(this.rule_btn, 2);

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), this.rule_btn);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.rule_btn);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.closeBtn);

        return true;
    },
    _init: function (dataObj) {

        this.totCards = 22;
        this.numOfGrps = 8;
        this.numOfUngrp = 5;
        this.minNumOfCardIngrp = 2;
        this.maxNumOfCardIngrp = 8;
        this.SpaceBWCard = 21 * scaleFactor;

        this.grpArr = [];

        for (var i = 0; i < (this.numOfGrps + this.numOfUngrp); i++) {
            this.grpArr[i] = [];
        }

        this.distributeCards(dataObj);
        this.closeBtn._isEnabled = true;
        this.rule_btn._isEnabled = true;
        this._groupY = 45 * scaleFactor;
    },
    distributeCards: function (dataObj) {
        this._wildCardstr = this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD).value;

        var melderCardGroupArr;
        var cardCount = 1;
        var groupCount = 0;
        var unGroupCount = this.numOfGrps;
        var card;
        var groupStr = "";
        var unGroupStr = "";
        var groupArray = [];
        var unGroupArray = [];
        var cardStr = "";
        var cardArr = [];

        if ((dataObj.cards == "") || (dataObj.cards == null))
            cardStr = this._gameRoom.cardString;
        else
            cardStr = dataObj.cards;

        if ((dataObj.str == null) || (dataObj.cards == null)) {
            this.setVisible(false);
            //todo remove btn this._gameRoom.resultPanel21.invalidBtn.visible = false;
            return;
        }
        if (cardStr.indexOf("|") == -1) {
            unGroupArray = cardStr.split(":");
            groupArray = [];
        }
        else if (cardStr.indexOf("|") == 0) {
            cardStr = cardStr.slice(1);
            unGroupArray = cardStr.split(":");
            groupArray = [];
        }
        else if (cardStr.indexOf("|") == cardStr.length - 1) {
            cardStr = cardStr.slice(0, cardStr.length - 1);
            groupArray = cardStr.split(";");
            unGroupArray = [];
        }
        else {
            melderCardGroupArr = cardStr.split("|");
            if (melderCardGroupArr.length == 1) {
                unGroupStr = melderCardGroupArr[0];
                unGroupArray = unGroupStr.split(":");
                groupArray = [];
                unGroupCount = this.numOfGrps + this.numOfUngrp - 1;
            }
            else {
                groupStr = melderCardGroupArr[0];
                unGroupStr = melderCardGroupArr[1];
                if (melderCardGroupArr[0] != "") {
                    groupArray = groupStr.split(";");
                }
                if (melderCardGroupArr[1] != "") {
                    unGroupArray = unGroupStr.split(":");
                }
            }
        }
        for (var i = 0; i < groupArray.length; i++) {
            if (groupArray[i] != "") {
                cardArr = groupArray[i].split(",");
                this.numOfJokers = 0;
                for (var k = 0; k < cardArr.length; k++) {
                    card = this.getCard(cardArr[k], false);
                    card.setJoker(this._wildcardStr, this._gameRoom._jokerSymVisble);
                    card._cardName = "card" + (cardCount);
                    card.grNo = (groupCount > (this.numOfGrps + this.numOfUngrp - 1)) ? (this.numOfGrps + this.numOfUngrp - 1) : groupCount;
                    card.setPosition(this.startP.x + ((cardCount - 1) * this.SpaceBWCard), this.defaultYPos);
                    card._initialPosition = card.getPosition();
                    this.addChild(card, GameConstants.cardMinZorder + (cardCount - 1));
                    this.cardStack.push(card);
                    this.grpArr[(groupCount > (this.numOfGrps + this.numOfUngrp - 1)) ? (this.numOfGrps + this.numOfUngrp - 1) : groupCount].push(card);// = arrToPush;
                    cardCount++;
                }
            }
            groupCount++;
        }

        for (var j = 0; j < unGroupArray.length; j++) {
            if (unGroupArray[j] != "") {
                cardArr = unGroupArray[j].split(",");
                this.numOfJokers = 0;
                for (var k = 0; k < cardArr.length; k++) {
                    card = this.getCard(cardArr[k], false);
                    card.setJoker(this._wildcardStr, this._gameRoom._jokerSymVisble);
                    card._cardName = "card" + (cardCount);
                    card.grNo = (unGroupCount > (this.numOfGrps + this.numOfUngrp - 1)) ? (this.numOfGrps + this.numOfUngrp - 1) : unGroupCount;
                    card.setPosition(this.startP.x + ((cardCount - 1) * this.SpaceBWCard), this.defaultYPos);
                    card._initialPosition = card.getPosition();
                    this.addChild(card, GameConstants.cardMinZorder + (cardCount - 1));
                    this.grpArr[(unGroupCount > (this.numOfGrps + this.numOfUngrp - 1)) ? (this.numOfGrps + this.numOfUngrp - 1) : unGroupCount].push(card);
                    cardCount++;
                }

                unGroupCount++;
            }
        }
        this.reArrangeCards(dataObj.str, groupArray.length);
    },
    getCard: function (name, flipped) {
        return new Card(name, flipped, this);
    },
    reArrangeCards: function (str, count) {

        this.cardStack = [];
        if (this._groupSprite)
            this._groupSprite.removeFromParent();

        this._groupSprite = new cc.Node();
        this.addChild(this._groupSprite, GameConstants.cardMinZorder + 5);

        var lastX = this.startP.x;
        var card;
        var i;
        var j;
        this._numOfJokers = 0;
        var cardCount = 0;
        var shrtStr = str.split(":");
        var newArr = [];

        for (i = 0; i < this.numOfGrps + this.numOfUngrp; i++) {
            if ((this.grpArr[i]) && (this.grpArr[i].length > 0)) {
                this.grpArr[i] = this.removeDuplicate(this.grpArr[i]);
                for (j = 0; j < this.grpArr[i].length; j++) {
                    card = this.grpArr[i][j];
                    this.cardStack.push(card);
                    if (card._isJoker)
                        this._numOfJokers += 1;
                    if (card.grNo == i) {
                        if (card._isSelected)
                            card.y = GameConstants.cardsPositionY;
                        else
                            card.y = GameConstants.cardsPositionY;
                        card._initialPosition = card.getPosition();
                        lastX += this.SpaceBWCard;
                        card.setLocalZOrder(GameConstants.cardMinZorder + cardCount);
                        cardCount += 1;
                        card.x = lastX;
                    }
                    else {
                        this.grpArr[i].splice(this.grpArr[i].indexOf(card), 1);
                    }
                }

                if (this.grpArr[i].length > 0) {
                    if (i >= this.numOfGrps)
                        lastX += this.SpaceBWCard + 6;
                    else
                        lastX += this.SpaceBWCard + 27;
                }

                if (i < this.numOfGrps) {
                    if (this.grpArr[i].length > 0) {
                        if (shrtStr[i] == "6") {
                            /*todo if (this.grpArr[i].length >= 8)
                             this.joker_btn.gotoAndStop(2);
                             else
                             this.joker_btn.gotoAndStop(1);*/
                        }
                        this.showGroup(i, shrtStr[i]);
                    }
                }
                else {
                    newArr = newArr.concat(this.grpArr[i]);
                }
            }
        }

        var btn;
        if (newArr.length > 0) {
            btn = new InvalidBtn();
            btn.setAnchorPoint(0, 0.5);
            btn.x = newArr[0].x - newArr[0].width / 4;
            btn.width = (newArr[newArr.length - 1].x - newArr[0].x) + (newArr[0].width * (2 / 3));
            if (btn.width < 80) {
                btn.width = 80;
            }
            btn._setText("Ungrouped");
            this._groupSprite.addChild(btn);
        }
        this.setUserCardPosition(lastX);

    },
    setUserCardPosition: function (lastX) {
        var dis = (this.endP.x - lastX) / 2;
        var card;
        for (var i = 0; i < this.cardStack.length; i++) {
            card = this.cardStack[i];
            card.x = card.x + dis;
        }
        this._groupSprite.x = this.startP.x + this._groupSprite.x + dis;
        this._groupSprite.y = this.defaultYPos - 75 * scaleFactor;
    },
    showGroup: function (i, str) {
        var btn;
        var baseWidth;
        var defaultWidth = 70;
        if (str == "0") {
            baseWidth = (this.grpArr[i][this.grpArr[i].length - 1].x - this.grpArr[i][0].x) + (defaultWidth * (2 / 3));
            btn = new InvalidBtn(baseWidth);
            btn.x = this.grpArr[i][0].x + (this.grpArr[i][this.grpArr[i].length - 1].x - this.grpArr[i][0].x) / 2;
            btn._setText("Invalid");
        }
        else {

            baseWidth = (this.grpArr[i][this.grpArr[i].length - 1].x - this.grpArr[i][0].x) + (defaultWidth * (2 / 3));
            btn = new ValidBtn(baseWidth);
            btn.x = this.grpArr[i][0].x + (this.grpArr[i][this.grpArr[i].length - 1].x - this.grpArr[i][0].x) / 2;

            if (str == "1")
                btn._setText("Seq");
            else if (str == "2")
                btn._setText("Pure Seq");
            else if (str == "3")
                btn._setText("Set");
            else if (str == "4")
                btn._setText("Tunnela");
            else if (str == "5")
                btn._setText("Doublee");
            else if (str == "6")
                btn._setText("Jokers");
        }
        this._groupSprite.addChild(btn);
    },
    removeDuplicate: function (arr) {
        var i;
        var j;
        for (i = 0; i < arr.length - 1; i++) {
            for (j = i + 1; j < arr.length; j++) {
                if (arr[i] === arr[j]) {
                    arr.splice(j, 1);
                }
            }
        }
        return arr;
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        switch (target._name) {
            case "closeBtn":
                this.removeFromParent(true);
                break;
            case "rule_btn":
                this.parent.show21CardRule();
                break;
        }
    }
});

var InvalidBtn = cc.Scale9Sprite.extend({
    ctor: function (baseWidth) {
        this._super(spriteFrameCache.getSpriteFrame("RedBase.png"));
        this.width = baseWidth;

        this.txt = new cc.LabelTTF("Invalid", "RobotoRegular", 17 * scaleFactor, cc.TEXT_ALIGNMENT_CENTER);
        this.txt.setScale(0.7);
        this.txt.setColor(cc.color(255, 255, 255));
        this.txt.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(this.txt, 2);
    },
    _setText: function (str) {
        this.txt.setString(str);
        this.txt.setPosition(cc.p(this.width / 2, this.height / 2));
    }
});

var ValidBtn = cc.Scale9Sprite.extend({
    ctor: function (baseWidth) {
        this._super(spriteFrameCache.getSpriteFrame("ValidBase.png"));
        this.width = baseWidth;

        this.txt = new cc.LabelTTF("", "RobotoRegular", 17 * scaleFactor, cc.TEXT_ALIGNMENT_CENTER);
        this.txt.setScale(0.7);
        this.txt.setColor(cc.color(255, 255, 255));
        this.txt.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(this.txt, 2);
    },
    _setText: function (str) {
        this.txt.setString(str);
        this.txt.setPosition(cc.p(this.width / 2, this.height / 2));
    }
});