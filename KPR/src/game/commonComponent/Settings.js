/**
 * Created by stpl on 11/23/2016.
 */
"use strict";
var Settings = cc.Scale9Sprite.extend({
    _gameRoom: null,
    _isEnabled: null,
    ctor: function (sourceNameString, parent) {
        this._super(spriteFrameCache.getSpriteFrame("SettingBg.png"), cc.rect(0, 0, 211 * scaleFactor, 111 * scaleFactor));
        this.setCapInsets(cc.rect(50 * scaleFactor, 40 * scaleFactor, 50 * scaleFactor, 40 * scaleFactor));
        this.setCascadeColorEnabled(false);

        this._gameRoom = parent;

        this.setVisible(true);

        if (sourceNameString == "Lobby") {
            this.listArrayString = ["My Accounts", "Promotions"];
            this.iconArray = ["Account.png", "Promotions.png"];
            this.iconOverArray = ["Account_over.png", "Promotions_over.png"];
            this.menuName = ["myAccount", "promotions"];

            this.setContentSize(cc.size(200 * scaleFactor, 75 * scaleFactor));
            this.setPosition(cc.p(this._gameRoom.width - 102 * scaleFactor, this._gameRoom.height - 60 * scaleFactor));
        }
        else {
            var gameType = this._gameRoom._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
            if (gameType.indexOf("Point") >= 0) {
                this.listArrayString = ["My Accounts", "First Hand History", "Promotion"];
                this.iconArray = ["Account.png", "History.png", "Promotions.png"];
                this.iconOverArray = ["Account_over.png", "History_over.png", "Promotions_over.png"];
                this.menuName = ["myAccount", "history", "promotions"];

                this.setContentSize(cc.size(200 * scaleFactor, 110 * scaleFactor));
                this.setPosition(cc.p(this._gameRoom.width - 102 * scaleFactor, this._gameRoom.height - 80 * scaleFactor));

            } else {
                this.listArrayString = ["My Accounts", "First Hand History", "Last Result", "Score Board ", "Promotion"];
                this.iconArray = ["Account.png", "History.png", "LastResult.png", "ScoreboardIcon.png", "Promotions.png"];
                this.iconOverArray = ["Account_over.png", "History_over.png", "LastResult_over.png", "ScoreboardIcon_over.png", "Promotions_over.png"]
                this.menuName = ["myAccount", "history", "lastResult", "scoreboard", "promotions"];

                this.setContentSize(cc.size(200 * scaleFactor, 170 * scaleFactor));
                this.setPosition(cc.p(this._gameRoom.width - 102 * scaleFactor, this._gameRoom.height - 110 * scaleFactor));
            }
        }
        this.listClassArray = [];

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        this.initPanel();
    },
    initPanel: function () {
        var pos = 30 * scaleFactor;
        var margin = 40 * scaleFactor;
        var menu;
        for (var i = 0; i < this.listArrayString.length; i++) {
            menu = new MenuSetting(this.iconArray[i], this.iconOverArray[i], this.menuName[i], this.listArrayString[i]);
            menu.setPosition(cc.p(cc.p((200 - 198) / 2, this.height - margin - (pos * i))));
            this.addChild(menu, 5);
            cc.eventManager.addListener(this.settingMouseListener.clone(), menu);
            cc.eventManager.addListener(this.settingTouchListener.clone(), menu);
        }
    },
    initMouseListener: function () {
        this.settingMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            swallowTouches: true,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode) && this.isVisible()) {
                    !target._hovering && target.hover(true);
                    target._hovering = true;
                    return true;
                }
                this.isVisible() && target._hovering && target.hover(false);
                target._hovering = false;
                return false;
            }.bind(this)
        });
        this.settingMouseListener.retain();
    },
    initTouchListener: function () {
        var that = this;
        this.settingTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode) && that.isVisible()) {
                    return true;
                }
                return false;
            },
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                switch (target.getName()) {
                    case "myAccount" :
                        cc.sys.openURL("https://www.khelplayrummy.com/my-profile");
                        break;
                    case "history" :
                        that._gameRoom.historyBtnHandler();
                        break;
                    case "lastResult" :
                        that._gameRoom.lastResBtnHandler();
                        break;
                    case "scoreboard" :
                        that._gameRoom.scoreBoardBtnHandler();
                        break;
                    case "promotions" :
                        cc.sys.openURL("https://www.khelplayrummy.com/promotions");
                        break;
                }
            }
        });
        this.settingTouchListener.retain();
    },
    handleBtnClick: function (targetName) {
        switch (targetName) {
            //case "myAccount":
        }
    },
    onExit: function () {
        this._super();
        this.release();
    }

});

var MenuSetting = ccui.Layout.extend({
    iconImage: null,
    iconOver: null,
    _hovering: null,
    _isEnabled: null,
    ctor: function (spriteName, spriteOverName, _name, labeltxt) {
        this._super();

        //this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        //this.setBackGroundColor(cc.color(255,0,0));

        this.setContentSize(cc.size(198 * scaleFactor, 28 * scaleFactor));
        this.setClippingEnabled(true);
        this.setName(_name);

        this._hovering = false;
        this._isEnabled = true;
        this.iconImage = spriteName;
        this.iconOver = spriteOverName;

        var margin = 15 * scaleFactor;

        this.hoverState = new cc.Sprite(spriteFrameCache.getSpriteFrame("SettingBgOver.png"));
        this.hoverState.setPosition(cc.p(this.width / 2, this.height / 2));
        this.hoverState.setVisible(false);
        this.addChild(this.hoverState);

        this.icon = new cc.Sprite(spriteFrameCache.getSpriteFrame(this.iconImage));
        this.icon.setPosition(cc.p(margin + margin / 2, this.height / 2));
        this.addChild(this.icon);

        this.label = new cc.LabelTTF(labeltxt, "RobotoBold", 12 * 2 * scaleFactor);
        this.label.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
        this.label.setAnchorPoint(cc.p(0, 0.5));
        this.label.setScale(0.5);
        this.label.setPosition(cc.p(40 * scaleFactor, this.height / 2));
        this.addChild(this.label);

        var line2 = new cc.DrawNode();
        line2.drawSegment(cc.p(margin, 0), cc.p(this.width - margin, 0), 0.5, cc.color(70, 69, 69));
        this.addChild(line2);
    },
    hover: function (bool) {
        if (bool) {
            this.icon.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.iconOver));
            this.label.setColor(cc.color(250, 165, 0));
            this.hoverState.setVisible(true);
        }
        else {
            this.icon.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.iconImage));
            this.label.setColor(cc.color(255, 255, 255));
            this.hoverState.setVisible(false);
        }
    }
});