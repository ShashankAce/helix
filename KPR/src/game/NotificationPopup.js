/**
 * Created by stpl on 11/30/2016.
 */

var NotificationPopup = cc.Scale9Sprite.extend({
    _gameRoom: null,
    ctor: function (context) {
        this._super(spriteFrameCache.getSpriteFrame("smileyBorder.png"), cc.rect(0, 0, 41 * scaleFactor, 41 * scaleFactor));
        this.setCapInsets(cc.rect(13 * scaleFactor, 13 * scaleFactor, 13 * scaleFactor, 13 * scaleFactor));
        this.setPosition(cc.p(100 * scaleFactor, 200 * scaleFactor));

        var msg = "fgyuiui iyggf ioyyuu";
        this.addMsg(msg);
    },
    addMsg: function (msg) {
        var label = new cc.LabelTTF(msg, "RobotoBold", 17 * scaleFactor );
        label.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(label);
    },
});
