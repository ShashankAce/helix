/**
 * Created by stpl on 11/27/2016.
 */
var TossLayer = cc.Layer.extend({
    _gameRoom: null,
    ctor: function (context, playerInfo, tossWinnerPlayerId, isWaitingBool) {
        this._super();
        this._gameRoom = context;
        var tossWinnerPlayerName, tossMsg;
        for (var key in playerInfo) {
            var tossCardSprite = new CardModel(playerInfo[key]["tossCard"], false);
            tossCardSprite.setScale(0.7);
            var pos = this._gameRoom._tossCardPosition[this._gameRoom.noOfPlayer][playerInfo[key]["seatPosition"] - 1];
            tossCardSprite.setPosition(pos);
            this.addChild(tossCardSprite, GameConstants.tossZorder);
        }

        if (!isWaitingBool || isWaitingBool == "") {
            var layout = cc.LayerGradient.extend({
                ctor: function () {
                    this._super(cc.color(0, 0, 0), cc.color(0, 0, 0));
                    var size = cc.winSize;
                    this.setPosition(cc.p(90 * scaleFactor, 290 * scaleFactor));
                    this.setContentSize(cc.size(800 * scaleFactor, 80 * scaleFactor));
                    this.setColorStops([{p: 0, color: new cc.Color(0, 0, 0, 0)},
                        {p: .5, color: cc.color.BLACK},
                        {p: 1, color: new cc.Color(0, 0, 0, 0)}]);
                    //this.setStartOpacity(255);
                    //this.setEndOpacity(0);
                    this.setVector(cc.p(-1, 0));
                }
            });
            var ll = new layout();

            this.addChild(ll, 10);
            // TODO add ony when self player is not in waiting state


            /* var layout = cc.LayerGradient.extend({
             ctor: function () {
             this._super(cc.color(0, 0, 0), cc.color(0, 0, 0));
             var size = cc.winSize;
             this.setPosition(cc.p(478, 280));
             this.setContentSize(cc.size(420, 90));
             this.setStartOpacity(0);
             this.setEndOpacity(255);
             this.setVector(cc.p(-1, 0));
             }
             });
             var ll2 = new layout();
             this.addChild(ll2, 10);
             */

            var label1 = new cc.LabelTTF("Rearranging seats please wait.", "RobotoRegular", 18 * 2 * scaleFactor);
            label1.setScale(0.5);
            label1.setPosition(cc.p(this.width / 2 - 15 * scaleFactor, 340 * scaleFactor));
            this.addChild(label1, 12);

            var label2 = new cc.LabelTTF(" You will be seated in centre bottom seat.", "RobotoRegular", 18 * 2 * scaleFactor);
            label2.setScale(0.5);
            label2.setPosition(cc.p(this.width / 2 - 15 * scaleFactor, 320 * scaleFactor));
            this.addChild(label2, 12);
        }

        if (tossWinnerPlayerId == this._gameRoom._myId) {
            tossMsg = "You win the toss for seat, and will play the first turn."
        } else {
            for (var k = 0; k <= this._gameRoom._playerArray.length; k++) {
                if (this._gameRoom._playerArray[k]._playerId == tossWinnerPlayerId) {
                    tossWinnerPlayerName = this._gameRoom._playerArray[k]._playerName;
                    break;
                }
            }
            tossMsg = tossWinnerPlayerName + ", wins the toss for seat, and will play the first turn."
        }
        var tossMsgLabel = new cc.LabelTTF(tossMsg, "RobotoRegular", 18 * 2 * scaleFactor);
        tossMsgLabel.setScale(0.5);
        tossMsgLabel.setPosition(cc.p(this.width / 2 - 15 * scaleFactor, 253 * scaleFactor));
        this.addChild(tossMsgLabel, 12);

        return true;
    },
    removeTossLayer: function () {
        this.removeFromParent(true);
    }
});