var Videos = Popup.extend({
    ctor: function (videoPath) {

        this._super(new cc.Size(755 * scaleFactor, 452 * scaleFactor), this);
        this.setVisible(true);

        this.headerLeft._setWidth(this.headerLeft.width - 2 * scaleFactor);
        this.headerLeft.setPositionX(this.headerLeft.getPositionX() + 1 * scaleFactor);
        this.headerLeft.setPositionY(this.headerLeft.getPositionY() - 2 * scaleFactor);

        this.text = new cc.LabelTTF("PICK", "RobotoBold", 15 * 2 * scaleFactor);
        this.text.setAnchorPoint(0, 0.5);
        this.text.setScale(0.5);
        this.text.setPosition(20 * scaleFactor, this.headerLeft.height / 2);
        this.headerLeft.addChild(this.text);

        this.setPosition(this.width / 2, this.height / 2);
        //this.addChild(this.pop);

        this.crossX = new cc.Sprite(spriteFrameCache.getSpriteFrame("ResultClose.png"));
        this.crossX.setAnchorPoint(0.5, 0.5);
        this.crossX.setName("CrossX");

        this.crossX.setPosition(this.width - 3 * scaleFactor, this.height - 3 * scaleFactor);
        this.addChild(this.crossX);

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {

                this.setVisible(false);
                this.videoOne.stop();
                this.videoOne.removeFromParent();

                return true;
            }.bind(this)
        }, this.crossX);

        this.videoOne = new ccui.VideoPlayer(videoPath);
        this.videoOne.setContentSize(800 * scaleFactor, 399 * scaleFactor);
        this.videoOne.setPosition(cc.p(this.width / 2, this.height / 2 - 20 * scaleFactor));
        this.setScale(1);
        this.addChild(this.videoOne);

        return true;
    },
    onEnterTransitionDidFinish: function () {
        this._super();
        this.videoOne.play();
    }
});