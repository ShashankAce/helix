/**
 * Created by stpl on 9/19/2016.
 */

var loginForm = cc.Class.extend({
    ctor: function () {
        if (Utils.isDebug) {
            var body = document.body;
            var loginFormContainer = document.createElement("div");
            loginFormContainer.id = "loginFormContainer";
            loginFormContainer.zIndex = 5;
            loginFormContainer.style.backgroundColor = "#9B7937";
            loginFormContainer.style.height = "100%";
            loginFormContainer.style.width = "100%";
            loginFormContainer.style.top = 0;
            loginFormContainer.style.position = "absolute";
            loginFormContainer.style.textAlign = "center";

            var logo = document.createElement("img");
            logo.src = "res/khelplaylogo.png";
            logo.id = "gameLogo";
            logo.style.marginTop = "1em";
            loginFormContainer.appendChild(logo);

            var loginLabel = document.createElement("p");
            loginLabel.id = "loginLabel";
            loginLabel.textContent = "Login to your account";
            loginFormContainer.appendChild(loginLabel);

            var userInputDiv = document.createElement("div");
            userInputDiv.style.fontSize = "1em";
            var passInputDiv = document.createElement("div");
            passInputDiv.style.fontSize = "1em";

            this.usernameInput = document.createElement("input");
            this.usernameInput.type = 'text';
            this.usernameInput.id = "username";
            this.usernameInput.placeholder = "username";
            this.usernameInput.style.margin = "1em";

            userInputDiv.appendChild(this.usernameInput);
            loginFormContainer.appendChild(userInputDiv);

            this.passwordInput = document.createElement("input");
            this.passwordInput.type = 'password';
            this.passwordInput.id = "password";
            this.passwordInput.placeholder = "password";
            this.passwordInput.style.margin = "1em";

            this.usernameInput.value = "alpha";
            this.passwordInput.value = "963210";

            passInputDiv.appendChild(this.passwordInput);
            loginFormContainer.appendChild(passInputDiv);

            var forgotPassword = document.createElement("p");
            forgotPassword.id = "forgotPassword";
            forgotPassword.textContent = "Forgot Password ?";
            loginFormContainer.appendChild(forgotPassword);

            var loginLabel = document.createElement("p");
            loginLabel.id = "register";
            loginLabel.textContent = "New ? Register";
            loginFormContainer.appendChild(loginLabel);

            var playButton = document.createElement("div");
            playButton.id = "playGame";
            playButton.style.background = "red";
            playButton.style.display = "inline-block";
            playButton.style.padding = "10px 20px";
            playButton.style.borderRadius = "5px";
            playButton.style.boxShadow = "1px 1px 1px 1px #6d1010";
            playButton.style.color = "#fff";
            playButton.style.fontFamily = "sans-serif";
            playButton.style.cursor = "pointer";

            playButton.addEventListener("click", this.connectWeaver.bind(this));

            var playButtonText = document.createElement("p");
            playButtonText.textContent = "LOGIN";
            playButtonText.style.margin = 0;

            playButton.appendChild(playButtonText);
            loginFormContainer.appendChild(playButton);

            var loadingText = document.createElement("p");
            loadingText.textContent = "Loading....";
            loadingText.style.color = "#fff";
            loadingText.style.fontSize = "30px";

            var loaderCell = document.createElement("div");
            loaderCell.style.display = "table-cell";
            loaderCell.style.verticalAlign = "middle";
            loaderCell.style.width = "100%";
            loaderCell.style.height = "100%";
            loaderCell.appendChild(loadingText);

            this.loader = document.createElement("div");
            this.loader.id = "html5loader"
            this.loader.style.display = "table";
            this.loader.style.width = "100%";
            this.loader.style.height = "100%";
            this.loader.style.top = 0;
            this.loader.style.left = 0;
            this.loader.style.textAlign = "center";
            this.loader.style.position = "absolute";
            this.loader.style.background = "rgba(0, 0, 0, 0.32)";

            this.loader.appendChild(loaderCell);
            loginFormContainer.appendChild(this.loader);
            body.appendChild(loginFormContainer);
        }
        this.appVersionRequest();
    },
    showHtml5Loader: function (bool) {
        if (!bool) {
            this.loader.style.display = "none";
        } else {
            this.loader.style.display = "table";
        }
    },
    appVersionRequest: function () {

        this.showHtml5Loader(true);

        var data = {
            os: gameConfig.os,
            appType: gameConfig.versionAppType,
            domainName: KPR.GAME_SERVER_URL,
            currAppVer: gameConfig.appVersion,
        };
        if (gameConfig.playerToken) {
            data.playerToken = gameConfig.playerToken;
        }
        var url = "userData=" + JSON.stringify(data) + "&url=" + WeaverRequestUrl.versionControl;
        AjaxCommunication.post(url, this.appVersionResponse.bind(this));
    },
    appVersionResponse: function (resp) {
        if (resp.errorCode == 0) {
            KPR.versionData = resp;
            gameConfig.sfsHost = KPR.versionData.gameEngineInfo.RUMMY_2X.serverIp[0];
            if (Utils.isDebug) {
                this.showHtml5Loader(false);
            } else {
                this.triggerAction();
            }
        }
    },
    loginClicked: function () {
        this.connectWeaver();
    },
    sfxInit: function () {
        sfsClient.initClient(gameConfig.sfsHost, gameConfig.sfsPort);
    },
    nickNameResp: function () {
        SFSConstants.NICK_NAME = KPR.triggerActionData.nickName;
        this.connectSfs();
    },
    connectWeaver: function () {
        
        this.showHtml5Loader(true);
        var username = this.usernameInput.value;
        var password = this.passwordInput.value;

        var data1 = {
            userName: username,
            password: MD5(MD5(password) + KHEL_CONSTANTS.LOGIN_TOKEN),
            loginToken: KHEL_CONSTANTS.LOGIN_TOKEN,
            domainName: KHEL_CONSTANTS.DOMAIN_NAME,
            requestIp: KHEL_CONSTANTS.REQUEST_IP,
            deviceId: "",
            deviceType: gameConfig.deviceType,
            userAgent: navigator.userAgent,
            appType: gameConfig.appType[2],
            currAppVer: gameConfig.appVersion,
        };

        var postData = 'userData=' + JSON.stringify(data1) + '&url=' + WeaverRequestUrl.playerLogin;
        AjaxCommunication.post(postData, this.afterWeaverLogin.bind(this));
    },
    afterWeaverLogin: function (resp) {

        if (resp.errorCode == 0) {
            KPR.weaverLoginData = resp;
            gameConfig.playerToken = KPR.weaverLoginData.playerToken;
            gameConfig.saveGameData();
            this.triggerAction();
        }
    },
    triggerAction: function () {

        if (!Utils.isDebug) {
            KPR.weaverLoginData = parent.GR_LOGIN_DATA.login_data;
        }
        var url = KPR.versionData.gameEngineInfo.RUMMY_2X.serverUrl + "/" + KPR.GAME_ENGINE_SUB_URL;
        var data = {
            "merchantKey": "4",
            "secureKey": KPR.secureKey,
            "device": gameConfig.deviceType,
            "token": KPR.weaverLoginData.playerToken
        };
        var userData = JSON.stringify(data);
        var postData = 'userData=' + userData + '&url=' + url + "&request='triggerAction'";
        AjaxCommunication.post(postData, this.afterTrigger.bind(this));
    },
    afterTrigger: function (resp) {
        KPR.triggerActionData = resp;
        SFSConstants.NICK_NAME = KPR.triggerActionData.nickName;
        this.sfxInit();
    }
});