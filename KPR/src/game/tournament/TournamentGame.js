/**
 * Created by stpl on 9/16/2016.
 */
"use strict";
var TournamentBaseGame = MiddleBaseGame.extend({
    _serverRoom: null,
    _seatObjMap: null,
    _currentTurnSeat: null,
    _waitingSeatObjMap: null,
    _bgImg: null,
    _lastSmile: null,
    _smileTimeOut: null,
    validityObj: null,
    _tabMc: null,
    _trnmentInfoMC: null,
    _resultSrc: null,
    parentRoom: null,
    _dummyObj: null,
    _tournamentGamePopup: null,

    _hasDistributionComp: null,
    _dropped: null,
    leaveTimeOutCnt: null,
    totCards: null,
    cardString: null,
    _14Card: null,
    openCard: null,
    _currentRound: null, 	// same as current deal in tournament
    _maxDeal: null,
    _currentLevel: null,
    _maxLevel: null,

    reportVars: null,
    _notificationHideTimeOut: null,
    _leaveTableByPlayer: null,

    _lastRoomId: null,
    _levelTime: null,
    _qualified: null,
    _currentRoomState: null,
    _canLeaveRoom: null,
    _roomId: null,
    _myId: null,

    _myRank: null,
    _myChip: null,


    ctor: function (room) {
        this._super(room);

        this._jokerSymVisble = false;
        this._seatShiftOffset = 0;
        this._canLeaveRoom = true;
        this._extraTimeToggle = false;
        this._isExtraTurnTime = false;
        this._isWatching = true;
        this._dropped = false;
        this._showMeld = true;
        this._stopAllTimer = false;
        this._leaveAllowed = true;
        this._waitTime = 0;
        this._bgCounter = 0;
        this._isPlayerConnect = false;
        this._isShowDistribution = true;
        this._connectCount = 0;
        this._requestToSever = true;
        this._isRoomActive = true;
        this._leaveTableByPlayer = false;
        this.lastPickCard = "";
        this._outputStr = "";
        this._roomId = room.id;

        this._hasDistributionComp = false;
        this._leftPlArr = [];
        this._currentTurnID = 0;
        this.leaveTimeOutCnt = 0;
        this.totCards = 13;
        this.cardString = "";
        this._14Card = "";
        this.openCard = "";
        this._currentRound = 0; 	// same as current deal in tournament
        this._maxDeal = 0;
        this._currentLevel = 0;
        this._maxLevel = 0;
        this.reportVars = [];
        this._notificationHideTimeOut = 0;
        this._lastRoomId = 0;
        this._levelTime = 0;
        this._qualified = false;
        this._isWaiting = false;
        this._currentRoomState = "";
        this._totalSeats = 0;

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        this.initHeader();

        this._userCardPanel = new UserCardPanel(this);
        this.addChild(this._userCardPanel, GameConstants.userCardPanelZorder, "_userCardPanel");

        this.deckPanel = new DeckPanel(this);
        this.addChild(this.deckPanel, GameConstants.deckPanelZorder, "deckPanel");

        this.setRoomVars();
        this.setJokerVisble(room);

        this._tournamentGamePopup = new TournamentPopUpManager(this);

        this.initRoom();
        this.setTournamentInfo();

        if (room.getVariable("isDummyRoom") && room.getVariable("isDummyRoom").value) {
            this.setDefaultProfile();
        }

        if (room.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Time") {
            this.levelTimer = new cc.LabelTTF("", "RobotoBold", 28, cc.TEXT_ALIGNMENT_CENTER);
            this.levelTimer.setPosition(cc.p(815 * scaleFactor, this.height - 12.5 * scaleFactor - yMargin));
            this.levelTimer.setScale(0.5);
            this.levelTimer.setColor(cc.color(250, 165, 0));
            this.addChild(this.levelTimer, 2, "levelTimer");
            this.updateLevelTimer();
        }
        if (room.getVariable(SFSConstants._MAXPLAYERS))
            this._totalSeats = room.getVariable(SFSConstants._MAXPLAYERS).value;

        this.totCards = parseInt(room.getVariable(SFSConstants.FLD_NUMOFCARD).value);


        cc.log("New Tournament BaseGame is initiated");
        return true;
    },
    initHeader: function () {
        this._super();
        this._isNoBal = false;
    },
    noTableBal: function (dataObj) {
        this._isNoBal = true;
        this._mySeatIndex = parseInt(dataObj["seat"]);
        this._isBetPopUpEnable = true;
    },
    clseBtnHandler: function () {
        // todo stage.removeEventListener(KeyboardEvent.KEY_DOWN, reportKeyDown);
        if (this._isNoBal) {
            var canObj = {};
            this.sendReqToServer("noBalCancel", canObj);
        }
        this._mySeatIndex = -1;
    },
    setJokerVisble: function (room) {
        if (room.getVariable("cashOrPromo") && room.getVariable("cashOrPromo").value == "CASH" && room.getVariable(SFSConstants.FLD_NUMOFCARD).value == 21)
            this._jokerSymVisble = false;
        else
            this._jokerSymVisble = true;
    },
    setDefaultProfile: function () {

        this._myId = parseInt(rummyData.playerId);
        var seatData = {};
        seatData.uName = applicationFacade._myUName;
        seatData.plId = this._myId;
        seatData.points = 0;
        seatData.ssid = 6;
        seatData.tsid = 6;
        seatData.url = rummyData.path.avatarImage;
        seatData.drop = false;
        seatData.disConnect = false;
        seatData.autoPlay = false;
        seatData.wrongShow = false;

        if (this._playerArray[5]) {
            this._playerArray[5].allotSeat(seatData);
            this._playerArray[5]._seatPosition = 6;
        }

        this.disableAllChairs();
        this.removeExtraPlayers();

        cc.eventManager.removeListener(this.baseGameMouseListener.clone(), this.gameInfoSprite);
        cc.eventManager.removeListener(this.baseGameTouchListener.clone(), this.gameInfoSprite);

        this._isRoomActive = false;

        this._trnmentInfoBtn && this._trnmentInfoBtn.removeFromParent(true);
        this.prizeCup && this.prizeCup.removeFromParent(true);
        this._trnmentInfoBtn = null;
        this.prizeCup = null;
    },
    _joinRoom: function (seatObj) {
        this._mySeatIndex = parseInt(seatObj.seatNo);
    },
    updateLevelTimer: function () {
        if (!this.parentRoom)
            this.parentRoom = sfsClient.getRoomById(this._serverRoom.getVariable(SFSConstants.PARENT_EXT_ROOM_ID).value);
        if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Round")
            return;

        var date = applicationFacade._controlAndDisplay._serverDate;
        var serverTime = date.getTime();// ((date.minutes * 60) + date.seconds) * 1000;
        if (!this.parentRoom.getVariable(SFSConstants.LEVEL_END_TIME))
            return;

        var levelEndTime = parseInt(this.parentRoom.getVariable(SFSConstants.LEVEL_END_TIME).value);
        this._levelTime = levelEndTime - serverTime;
        if (this._levelTime < 0) {
            this._levelTime = 0;
            return;
        }

        var timeObj = DateNTime.getTimeObject(this._levelTime);
        this.levelTimer.setString(timeObj["M"] + ":" + timeObj["S"]);
        // this._tournamentGamePopup.setLevelTime(timeObj["M"] + ":" + timeObj["S"]);
    },
    setRoomVars: function () {


        if (!this._serverRoom.getVariable(SFSConstants._ROOMSTATE))
            return;
        var roomState = this._serverRoom.getVariable(SFSConstants._ROOMSTATE).value;
        if (roomState == SFSConstants.RS_CUTTHEDECK)
            return;
        if ((this._serverRoom.getVariable(SFSConstants._WAITTIME)) && (roomState != SFSConstants.RS_RESULT)) {
            if (roomState == SFSConstants.RS_JOIN) {
                if (!this.parentRoom)
                    this.parentRoom = sfsClient.getRoomById(this._serverRoom.getVariable(SFSConstants.PARENT_EXT_ROOM_ID).value);
                this._tournamentGamePopup.removeNotification();
                if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Round")
                    this._tournamentGamePopup.getTournamentStartPopUp(this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value, "Round: " + this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value + "/" + this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL).value, "Deals to play: " + this._maxDeal.value);
                else
                    this._tournamentGamePopup.getTournamentStartPopUp(this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value, "Level: " + this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value + "/" + this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL).value, "Point Value: " + this._serverRoom.getVariable(SFSConstants._POINTVALUE).value);
            }
            else
                this._currentTurnID = parseInt(this._serverRoom.getVariable(SFSConstants._TURNUID).value);
            this.scheduleWaitTime();
        }
    },
    initScreen: function () {
        this.setRoomProperties();
        this._stopAllTimer = false;
        this._isPlayerConnect = false;
        this._isShowDistribution = true;
        this._currentTurnID = -2;
        this._leftPlArr = [];
        this._smileTimeOut = {};
        this.showValidity.hide(true);
        this.deckPanel.hideAllItems(true);
        this.pickWaitMC.setVisible(false);
        this.messagePopUp && this.messagePopUp.removeFromParent(true);
        this.scoreBoardPanel && this.scoreBoardPanel.setVisible(false);
        this.settingPanel.setVisible(false);
        this.removeClosedCard();
        this.openCard = "";
        if (this._serverRoom.getVariable("isDummyRoom") && this._serverRoom.getVariable("isDummyRoom").getValue()) {
            this.setDefaultProfile();
        }
        if (this._serverRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Time") {
            this.levelTimer.setVisible(true);
            this.updateLevelTimer();
        }
        else
            this.levelTimer.setVisible(false);

        this._qualified = false;
        this._isWaiting = false;
        this.initRoom();
    },
    setRoomProperties: function () {
        this._roomId = this._serverRoom.id;
        this.totCards = this._serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;
        this._totalSeats = this._serverRoom.getVariable(SFSConstants._MAXPLAYERS).value;
        this.setJokerVisble(this._serverRoom);

        this.deckPanel.initPanel();
        // todo this.reportProblem.init(this._serverRoom);
        if (!this.parentRoom)
            this.parentRoom = sfsClient.getRoomById(this._serverRoom.getVariable(TOURNAMENT_CONSTANTS.PARENT_EXT_ROOM_ID).value);
        this.gameInfopopup && this.gameInfopopup.updateGameInfo();
    },
    updateHeader: function () {
        var gameInfoTxt = "";
        this._isNoBal = false;
        this.parentRoom = sfsClient.getRoomById(this._serverRoom.getVariable(SFSConstants.PARENT_EXT_ROOM_ID).value);
        var torName;
        if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME))
            torName = this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value;
        else
            torName = "";
        var torId = this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_ID).value;
        var level = "";
        if (this.parentRoom.getVariable(SFSConstants.CUR_LEVEL) && this.parentRoom.getVariable(SFSConstants.MAX_LEVEL)) {
            level = this.parentRoom.getVariable(SFSConstants.CUR_LEVEL).value + "/" + this.parentRoom.getVariable(SFSConstants.MAX_LEVEL).value;
        }
        else {
            level = this._currentLevel + "/" + this._maxLevel;
        }
        var roomId = "";
        if (this._serverRoom.getVariable(SFSConstants.ROOM_ID))
            roomId = this._serverRoom.getVariable(SFSConstants.ROOM_ID).value;
        else
            roomId = "----";
        if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Round")
            gameInfoTxt = torName + " - " + torId + " |  Table ID: " + roomId + " |  Round: " + level;
        else
            gameInfoTxt = torName + " - " + torId + " |  Table ID: " + roomId + " |  Level: " + level;

        // this.tableId = roomId;
        this._gameInfoTF.setString(gameInfoTxt);
        this.updateHeaderPosition();
    },
    sendReqToServer: function (cmd, reqObj) {
        if (!this._requestToSever)
            return;
        if ((this._myId == -1 && this._isWatching ) && (cmd == "connectAgain")) {
            applicationFacade.updateExternalLobbyBalance();
            this.leaveRoomByMyself();
            return;
        }
        if (this._myId != -1)
            reqObj.id = this._myId;

        if ((cmd == SFSConstants.TOURNAMENT_RE_JOIN_REQ || cmd == SFSConstants.TOURNAMENT_INFO) && this.parentRoom)
            sfsClient.sendTournamentReq(cmd, reqObj, this._serverRoom.getVariable(SFSConstants.PARENT_EXT_ROOM_ID).value);
        else
            sfsClient.sendReqFromTourRoom(cmd, reqObj, this._roomId);
    },
    processRoomStatus: function (dataObj, room) {
        console.log('Inside room status');

        var discardStr = "";
        this.gameInfoSprite.gameInfopopup.setTurnTime(dataObj.displayTurnTime);
        if (this._tournamentGamePopup)
            this._tournamentGamePopup.removeNotification();

        this._currentRoomState = this._serverRoom.getVariable(SFSConstants._ROOMSTATE).value;
        if (this._serverRoom.getVariable(SFSConstants._TURNUID))
            this._currentTurnID = parseInt(this._serverRoom.getVariable(SFSConstants._TURNUID).value);
        if (dataObj.rndNo)
            this._currentRound = dataObj.rndNo;

        this._maxDeal = dataObj["maxDeal"];	// max deal
        this._currentLevel = dataObj["currentLevel"];
        this._maxLevel = dataObj["maxLevel"];
        if (!this._serverRoom.getVariable(SFSConstants.PARENT_EXT_ROOM_ID))
            this._serverRoom._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(SFSConstants.PARENT_EXT_ROOM_ID, dataObj["parentExtId"]));

        if (!this.parentRoom)
            this.parentRoom = sfsClient.getRoomById(dataObj["parentExtId"]);

        cc.log("level1 :" + this._currentLevel);
        this.parentRoom._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(SFSConstants.CUR_DEAL, this._currentRound));
        this.parentRoom._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(SFSConstants.MAX_DEAL, this._maxDeal));
        this.parentRoom._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(SFSConstants.CUR_LEVEL, this._currentLevel));
        this.parentRoom._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(SFSConstants.MAX_LEVEL, this._maxLevel));

        if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Round")
            this._tabMc.subMiniTable.setTableId("Round: " + this._currentLevel + "/" + this._maxLevel);
        else
            this._tabMc.subMiniTable.setTableId("Level: " + this._currentLevel + "/" + this._maxLevel);

        this.updateHeader();
        if (this._currentRoomState == SFSConstants.RS_JOIN) {
            this._tournamentGamePopup.removeNotification();
            if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Round")
                this._tournamentGamePopup.getTournamentStartPopUp(this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value, "Round: " + this._currentLevel + "/" + this._maxLevel, "Deals to play: " + this._maxDeal, dataObj["timer"]);
            else
                this._tournamentGamePopup.getTournamentStartPopUp(this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value, "Level: " + this._currentLevel + "/" + this._maxLevel, "Point Value: " + this._serverRoom.getVariable(SFSConstants._POINTVALUE).value, dataObj["timer"]);
        }

        var isMyautoPl = false;
        isMyautoPl = this.parseSeat(dataObj, room);
        if (this._currentRoomState != SFSConstants.RS_JOIN) {
            this.rotateSeats();

            if (this._myId == -1) {
                this._tabMc.setIcon(2);
            }
            else
                this._tabMc.setIcon(1);
        }
        else
            this._tabMc.setIcon(1);
        if (this._serverRoom.getVariable(SFSConstants._DISCARD))
            discardStr = this._serverRoom.getVariable(SFSConstants._DISCARD).value;


        /*todo if ((discardStr) && (discardStr != ""))
         {
         discrdBtn.buttonMode = true;
         handleBtnListener(discrdBtn, true, discardBtnHandler);
         }*/

        if (this._currentRoomState == SFSConstants.RS_GAMESTART) {
            if (this._myId == -1) {

                this._leaveAllowed = true;

                if (this.historyPanel)
                    this.historyPanel._isEnabled = false;
            }
            else {
                this._leaveAllowed = false;
                if (this.historyPanel)
                    this.historyPanel._isEnabled = true;
            }
        }
        if (this._currentRoomState == SFSConstants.RS_CUTTHEDECK) {
            this.deckPanel.initPanel();
            this.deckPanel.hideAllItems(true);
            if (this._myId == -1) {
                this._leaveAllowed = true;
                this._canLeaveRoom = true;
                if (this.historyPanel)
                    this.historyPanel._isEnabled = false;

                var noOfPl = this._serverRoom.getVariable(SFSConstants.DATA_NOOFPLS).value;
                var self = this;
                var timer = parseInt(dataObj["timer"]) + noOfPl;
                setTimeout(function () {
                    self.watchPlrTurnHandler();
                }, timer * 1000);
            }
            else {
                this._leaveAllowed = false;
                this._canLeaveRoom = false;
                if (this.historyPanel)
                    this.historyPanel._isEnabled = true;
            }
        }
        var seatObj;

        if (this._currentRoomState == SFSConstants.RS_MELD) {
            this.initClosedCards();
            this.arrangeClosedCard();
        }
        if (this._currentRoomState == SFSConstants.RS_WATCH || this._currentRoomState == SFSConstants.RS_MELD)
            this._currentTurnID = parseInt(this._serverRoom.getVariable(SFSConstants._TURNUID).value);

        if (this._currentRoomState == SFSConstants.RS_WATCH) {
            this._currentTurnID = parseInt(this._serverRoom.getVariable(SFSConstants._TURNUID).value);
            this._leaveAllowed = true;
            this._hasDistributionComp = true;
            this.deckPanel.initPanel();
            this.deckPanel.setWildCard();
            this.deckPanel.hideAllItems(false);
            this._userCardPanel.initPanel();
            this.deckPanel.openDeck.setOpenDeckCards();
            this.deckPanel._animComp = true;
            this.handleCurrentTurn(parseInt(dataObj["timer"]));
            seatObj = this.getSeatObjByUser(this._myId);
            if (this._myId == -1 || (seatObj && seatObj._seatPosition != -1 && seatObj.isWrongShow)) {
                this.initClosedCards();
                this.arrangeClosedCard();
                this._userCardPanel.hideAllItems(true);
                if (this.historyPanel)
                    this.historyPanel._isEnabled = false;
            }
            else {

                if (!this._dropped)
                    this._userCardPanel.hideAllItems(false);
                if (this.historyPanel)
                    this.historyPanel._isEnabled = true;
                //todo setting button
            }
        }
        if (this._currentRoomState == SFSConstants.RS_MELD) {
            this._hasDistributionComp = true;
            this.deckPanel.initPanel();
            this.deckPanel.setWildCard();
            seatObj = this.getSeatObjByUser(this._myId);
            if ((this._myId == -1) || (seatObj && seatObj._seatPosition != -1 && seatObj.isWrongShow)) {
                this.initMessagePop();
                this.messagePopUp.setMsg("Melding in progress!!!\nPlease wait while players are melding... ");
                if (this.historyPanel)
                    this.historyPanel._isEnabled = false;
            }
            else if (this.historyPanel)
                this.historyPanel._isEnabled = true;
        }
        if (this._currentRoomState != SFSConstants.RS_JOIN)
            this.removeExtraPlayers();

        if (isMyautoPl)
            this.enableAutoPlay();
        if (!this._isWatching)
            this.disableAllChairs();
    },
    watchPlrTurnHandler: function () {
        cc.log("watchPlrTurnHandler");
        this._hasDistributionComp = true;
        this.deckPanel.setWildCard();
        this.deckPanel.setOpenDeckCards(false);
        this.handleCurrentTurn();
        this.deckPanel.hideAllItems(false);
    },
    seatConfirmResponse: function (data, room) {
        this._super(data, room);

        this._currentRoomState = this._serverRoom.getVariable(SFSConstants._ROOMSTATE).value;
        var userId = parseInt(data[SFSConstants._USERID]);
        var seatPosition = parseInt(data[SFSConstants.FLD_SEATNO]);
        var seatId = seatPosition;
        var userName = data[SFSConstants.FLD_UNAME];
        var userScore = data[SFSConstants._NUM];
        var _isNext = ((data["isNext"] && data["isNext"].toString()) == "true" ? "isNext" : "");

        var isConfirmed = data[SFSConstants.MSG].toLowerCase() == "confirmed";
        var isWaiting = false;

        if (data.hasOwnProperty(SFSConstants.FLD_IS_NEXT))
            isWaiting = data[SFSConstants.FLD_IS_NEXT];

        if (seatPosition == this._mySeatIndex) {
            this.clseBtnHandler(null);
        }

        var indx = seatPosition - 1;
        if (this._currentRoomState == SFSConstants.RS_JOIN) {
            if (userName == rummyData.nickName) {
                this._myId = parseInt(userId);
                this._isWatching = false;

                this._isWaiting = isWaiting;
                applicationFacade._myUName = userName;
                applicationFacade._myId = this._myId;
                this.disableAllChairs();

                //todo mainProfileMC._shiftNoBackup = _totalSeats - seatId;
                this.showNotification();
                this._mySeatIndex = seatPosition;
                this.selfSeatPosition = seatPosition;
                this.shiftedPositionByLength = this.noOfPlayer - seatPosition;
            }
        } else {
            cc.log("handleSeatConfResp other state", userName, applicationFacade._myUName);

            if (userName == rummyData.nickName) {
                this.disableAllChairs();
                this._isWatching = false;
                applicationFacade._myUName = userName;
                this._tabMc.setIcon(1);
                this.showNotification();
                this._mySeatIndex = seatPosition;
                this.selfSeatPosition = seatPosition;
            }
            else {

                if (this._myId != -1 && data["isSeatRotate"] == "Y") {
                    seatPosition = this.getShiftedSeatId(seatPosition);
                }

                else {
                    if (this._myId != -1 && this._currentRoomState != SFSConstants.RS_GAMESTART) {
                        seatPosition = this.getShiftedSeatId(seatPosition);
                    }
                }
            }
        }

        if (indx > -1 && this._playerArray[indx]) {
            if (this._playerArray[indx].parent == null)
                this.addChild(this._playerArray[indx], GameConstants.playerZorder);

            this._playerArray[indx].updateInfo(seatPosition, userId, userName, userScore, !1, !1, !1, !1, !1);
            this._playerArray[indx].takeSeat(userId, userName, userScore);
            this._playerArray[indx]._seatPosition = seatPosition;
            this._playerArray[indx].setScore(userScore);
        }

        if (data["isSeatRotate"] == "Y") {
            if (userName == rummyData.nickName) {
                this._myId = userId;
                this.shiftedPositionByLength = this.noOfPlayer - seatId;
                this.rotateSeats();
            }
        }
    },
    makeRandomPlayer: function (seatPosition) {

    },
    disableLeave: function () {
        this._super();
        this._tournamentGamePopup.removePopUp();
    },
    processWaitTimer: function (param, room) {

        this._leftPlArr = [];
        this._isShowDistribution = true;
        this.cutDeckPanel = null;
        this._leaveAllowed = true;

        this.cutDeckPanel && this.cutDeckPanel.removeFromParent(true);
        this.result && this.result.removeFromParent(true);
        this.winner && this.winner.removeFromParent(true);
        this._tournamentGamePopup.removeNotification();

        if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL) && this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL)) {
            if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Round")
                this._tournamentGamePopup.getTournamentStartPopUp(this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value, "Round: " + this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value + "/" + this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL).value, "Deals to play: " + this._maxDeal);
            else
                this._tournamentGamePopup.getTournamentStartPopUp(this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value, "Level: " + this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value + "/" + this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL).value, "Point Value: " + this._serverRoom.getVariable(SFSConstants._POINTVALUE).value);
        }
        else {
            if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Round")
                this._tournamentGamePopup.getTournamentStartPopUp(this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value, "Round: " + this._currentLevel + "/" + this._maxLevel, "Deals to play: " + this._maxDeal);
            else if ((this._serverRoom.getVariable(SFSConstants._POINTVALUE)) && (this._serverRoom.getVariable(SFSConstants._POINTVALUE).value != undefined))
                this._tournamentGamePopup.getTournamentStartPopUp(this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value, "Level: " + this._currentLevel + "/" + this._maxLevel, "Point Value: " + this._serverRoom.getVariable(SFSConstants._POINTVALUE).value);
        }

        if (!this._serverRoom.getVariable(SFSConstants._WAITTIME) || !this._serverRoom.getVariable(SFSConstants._WAITTIME).value)
            this._serverRoom._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(SFSConstants._WAITTIME, param["waitTime"]));
        if (!this.winner)
            this.scheduleWaitTime();


        // if(parentRoom.getVariable(TournamentConstant.TOURNAMENT_GRP_TYPE).getValue()=="Round")
        //     _tournamentGamePopup.getRoundTournamentStartPopUp(parentRoom.getVariable(TournamentConstant.TRNAMENT_NAME).getValue(),"Round: "+parentRoom.getVariable(TournamentConstant.CUR_LEVEL).getValue() + "/" + parentRoom.getVariable(TournamentConstant.MAX_LEVEL).getValue(),"Deals to play: "+parentRoom.getVariable(TournamentConstant.MAX_DEAL).getValue() );
        // else
        //     _tournamentGamePopup.getRoundTournamentStartPopUp(parentRoom.getVariable(TournamentConstant.TRNAMENT_NAME).getValue(),"Level: "+parentRoom.getVariable(TournamentConstant.CUR_LEVEL).getValue() + "/" + parentRoom.getVariable(TournamentConstant.MAX_LEVEL).getValue(),"Point Value: "+_serverRoom.getVariable(TournamentConstant.POINT_VALUE).getValue());
        // scheduleWaitTime();

    },
    processCutTheDeckCmd: function (dataObj) {
        this.initNewRound(dataObj);
        this.disableAllChairs();
        this.rotateSeats();
    },
    initNewRound: function (dataObj) {
        this._super(dataObj);
        this._qualified = false;

        this.removeExtraPlayers();
        this._tournamentGamePopup.removeNotification();
        this._tournamentGamePopup.removePopUp();

        this.winner && this.winner.removeFromParent(true);
        this._tabMc.setIcon(1);

        this._currentRound = dataObj["rndNo"];
        this.parentRoom._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(SFSConstants.CUR_DEAL, this._currentRound));

        if (this._currentRoomState != SFSConstants.CMD_CUTTHEDECK)
            this.stopWaitTimer();

        // todo
        /*if (this._currentRoomState == SFSConstants.CMD_WATCH) {
         if (_myId != -1) {
         historyBtnMC.btn_mc.buttonMode = true;
         handleBtnListener(settingMC.historyBtn, true, historyBtnHandler);
         }
         else {
         historyBtnMC.btn_mc.buttonMode = false;
         }
         }*/
    },
    processCutTheDeckResp: function (data) {
        this._super(data);
        if (!this._isShowDistribution || this._connectCount == 1) {
            if (this._connectCount == 1)
                this._connectCount = 2;
        }

    },
    processFreeSeat: function (param) {

        var name = param[SFSConstants.FLD_NAME];
        var obj = this.getSeatObjByUserName(name);

        if (obj) {
            window.clearInterval(this._userCardPanel.cardDistributeInterval);
            if (this._currentRoomState == SFSConstants.RS_MELD) {
                if (obj._playerId == this._currentTurnID) {
                    this._extraTimeToggle = false;
                }
            }
            if (this._currentRoomState == SFSConstants.RS_WATCH) {
                this.removeFreeSeatPlayer(obj);
                // this.removeFreeSeatPlayer(obj);
                /*if (obj._playerId == this._currentTurnID) {
                 }*/
            }

            if (parseInt(obj._playerId) != this._myId)
                this.showNotification(obj.player_name + " left the table.", true);

            obj.leaveSeat();
            this.removeClosedCardForSpecificPlayer(obj);
            this.updatePlayerInfo(KHEL_CONSTANTS.USER_LEFT, obj, false, false, false, false, false);

            var leftPlStr;
            if (this._leftPlArr.indexOf(name) == -1)
                this._leftPlArr.push(name);

            leftPlStr = this._leftPlArr.join(",");
            if ((leftPlStr) && (leftPlStr != "") && (this._currentRoomState != SFSConstants.RS_RESULT)) {
                leftPlStr += " left the table!!";
                this.showNotification(leftPlStr, true);
            }
        }
    },
    removeFreeSeatPlayer: function (player) {
        var playerId = player._playerId;
        for (var i = 0; i < this._playerArray.length; i++) {
            if (playerId == this._playerArray[i]._playerId) {
                this._playerArray[i].leaveSeat();
                this._playerArray[i].clearAllState();
                this.removeChild(this._playerArray[i]);
                this._playerArray[i].parent = null;
                return;
            }
        }
    },
    removeExtraPlayers: function () {
        var k = this._playerArray.length;
        while (k--) {
            if (!this._playerArray[k]._sittingStatus) {
                this._playerArray[k].leaveSeat();
                this._playerArray[k].clearAllState();
                this.removeChild(this._playerArray[k]);
                this._playerArray[k].parent = null;
            }
        }
    },
    handlePlayerOver: function (dataObj) {
        if (!dataObj["str"])
            return;
        var plOver = (dataObj["str"].toString()).split(",");
        if (plOver.length == 0)
            return;
        var profMC;
        var seatObj;
        for (var i = 0; i < plOver.length; i++) {
            seatObj = this.getSeatObjByUserName(plOver[i]);
            if (seatObj && seatObj.seat_id != -1) {
                seatObj.leaveSeat();
            }
        }
    },
    showWaitTimer: function (remainTime) {
        if (this._stopAllTimer) {
            this.stopWaitTimer();
            return;
        }
        if (this._currentRoomState == SFSConstants.RS_JOIN) {
            if (!this._leaveAllowed)
                return;
            this._tournamentGamePopup.setTime(remainTime.toString());
            if (remainTime == 0) {
                this.disableLeave();
            }
        }
        if (this._currentRoomState == SFSConstants.RS_WATCH) {
            if (this._currentTurnSeat)
                this._currentTurnSeat.setTime(remainTime);
            this.setTabTimer(remainTime);
            if ((remainTime == 1 || remainTime == 3) && this._hasDistributionComp && this._userCardPanel.isVisible()) {
                if (this._currentTurnID == this._myId && this._myId != -1) {
                    if (!this._userCardPanel._isShowReqSent) {
                        var groupCards = this._userCardPanel.getGroupCardsStr();
                        this._userCardPanel.sendGroupCards(groupCards, true);
                    }
                }
            }
        }
        if (this._currentRoomState == SFSConstants.RS_CUTTHEDECK) {
            //todo cutDeckMC.setTime(remainTime);
            // initial timer is set by default in initCutDeckPanel
        }
        if (this._currentRoomState == SFSConstants.RS_MELD) {
            if (this._userCardPanel.isVisible()) {
                if (this._currentTurnSeat)
                    this._currentTurnSeat.setTime(remainTime);
                if (this._tabMc.timerTxt)
                    this._tabMc.timerTxt.setString(remainTime.toString());
                if (((remainTime == 1) || (remainTime == 3))) {
                    if (this._myId != -1) {
                        var groupCards = this._userCardPanel.getGroupCardsStr();
                        this._userCardPanel.sendGroupCards(groupCards, true);
                    }
                }
                if (remainTime == 0) {
                    if (this._extraTimeToggle) {
                        this.meldingPanel.sendMeldCards();
                    }
                }
            }
            else {
                if (this._extraTimeToggle) {
                    if (this.meldingPanel && this.meldingPanel._currentFrame == 5)
                        this.meldingPanel.updateStrTimer(remainTime);
                }
            }
        }
    },
    processExtraTimeUserList: function (dataObj) {
        this.showMeldMessage(dataObj);
    },
    showMeldMessage: function (dataObj) {
        //todo this.wrongShow && wrongShowMC.visible = false;
        if (!this._dropped && !this._isWatching && (this._myId != -1)) {
            this.messagePopUp && this.messagePopUp.removeFromParent(true);

            var userList = dataObj["extraTimeUserList"].split(",");
            var str;
            if (userList.length == 1)
                str = "Please wait " + dataObj["extraTimeUserList"] + " is using extra timer.";
            else
                str = "Please wait " + dataObj["extraTimeUserList"] + " are using extra timer.";
            //str = dataObj["maxExtraTime"];

            this.meldingPanel.setVisible(true);
            this.meldingPanel.setMeldMsg(5, str, dataObj["maxExtraTime"]);
            this.scheduleWaitTime(SFSConstants.ETT_MELDEXTRATIMER, parseInt(dataObj["maxExtraTime"]));
            if (this._userCardPanel.isVisible()) {
                var groupCards = this._userCardPanel.getGroupCardsStr();
                this._userCardPanel.sendGroupCards(groupCards, true);

                this._userCardPanel.setVisible(false);
                var seatObj = this.getSeatObjByUser(this._myId);
                if (seatObj)
                    seatObj.hideTimer();
            }
            this.meldingPanel.removeJoker();
        }
        else {
            this.initMessagePop();
            this.messagePopUp.setMsg("Melding in progress!!!\nPlease wait while players are melding... ");
        }
    },
    showNotification: function (msgStr) {
        if (!msgStr)
            return;

        this._super();

        this.notification = new GameNotification(msgStr);
        this.notification.setPosition(cc.p(55 * scaleFactor, 590 * scaleFactor));
        this.addChild(this.notification, GameConstants.notificationZorder, "notification");
        this.notification.tween();
    },
    updatePoints: function (obj) {
        var profObj = this.getSeatObjByUser(obj["pId"]);
        if (profObj && profObj != -1) {
            if (obj["totPoints"])
                profObj.setScore(Number(obj["totPoints"]));

            if (obj["rndPoints"])
                profObj.setRndPoints(obj["rndPoints"]);
        }
    },
    roomVariableUpdate: function (event) {
        var room = event.room;
        var changedVars = event[SFSConstants._CHANGEDVARS];
        if (!this._isRoomActive)
            return;

        for (var s in changedVars) {

            if (changedVars[s] == SFSConstants.CUR_DEAL) {
                this._trnmentInfoMC && this._trnmentInfoMC.showCurrentDeal();

            } else if (changedVars[s] == SFSConstants._TURNUID) {
                this.stopWaitTimer();
                this._tabMc.subMiniTable.turnIndicator.setVisible(false);
                this._tabMc.subMiniTable.extraMC.setVisible(false);
                this._currentTurnID = parseInt(this._serverRoom.getVariable(SFSConstants._TURNUID).value);

                if (this._myId != -1 && !this.autoPlayPopup) {
                    this._tabMc.setIcon(1);
                    if (applicationFacade._currScr == this)
                        this._tabMc.startGlow(1);
                    else if (this._currentTurnID == this._myId)
                        this._tabMc.startGlow(3);
                }
                if (this._currentTurnSeat) {
                    this._currentTurnSeat.hideTimer();
                }
                //this.handleCurrentTurn();
                if (this._connectCount == 0)
                    this.handleCurrentTurn();
                if (this._connectCount == 1)
                    this._connectCount = 2;
            } else if (changedVars[s] == SFSConstants._DISCARD) {
                this.updateDiscardHistory();
            } else if (changedVars[s] == SFSConstants._OD) {
                this.ODCards(room.variables[SFSConstants._OD].value);
                if (this._tournamentGamePopup.isNotfOn())
                    this.deckPanel.hideAllItems(true);
            } else if (changedVars[s] == SFSConstants._ROOMSTATE) {
                this._currentRoomState = this._serverRoom.getVariable(SFSConstants._ROOMSTATE).value;
                if (this._currentRoomState == SFSConstants.RS_GAMESTART) {
                    this.winner && this.winner.removeFromParent(true);
                    this.leaveTablePopup && this.leaveTablePopup.removeFromParent(true);
                    if (this._myId == -1 && !this._isWaiting) {
                        this._leaveAllowed = true;
                        this._canLeaveRoom = true;
                        this._tabMc.setIcon(2);
                    }
                    else {
                        this._leaveAllowed = false;
                        this._canLeaveRoom = true;
                    }
                    //todo mainProfileMC.joinVisible(true);
                }
                if (this._currentRoomState == SFSConstants.RS_CUTTHEDECK) {
                    //todo this.reArrangePopUpMC.visible = false;
                    this.scheduleWaitTime();
                    this._leaveAllowed = false;
                }
                if (this._currentRoomState == SFSConstants.RS_RESULT) {
                    this.stopWaitTimer();
                    if (this._myId != -1)
                        this.showNotification();
                    this._leftPlArr = [];
                    this.scoreBoardPanel && this.scoreBoardPanel.setVisible(false);
                    this._userCardPanel.meldConfirmPopUp && this._userCardPanel.meldConfirmPopUp.removeFromParent(true);
                    this._leaveAllowed = true;
                }
                if (this._currentRoomState == SFSConstants.RS_MELD) {
                    this.scheduleWaitTime();
                    this._leaveAllowed = true;
                }
                if (this._currentRoomState == SFSConstants.RS_WATCH) {
                    this.messagePopUp && this.messagePopUp.removeFromParent(true);
                    this._userCardPanel.meldConfirmPopUp && this._userCardPanel.meldConfirmPopUp.removeFromParent(true);
                    if (this._leftPlArr) {
                        var leftPlStr = this._leftPlArr.join(",");
                        var gameTypeStr = this._serverRoom.getVariable(SFSConstants._GAMETYPE).value;
                        if ((leftPlStr) && (leftPlStr != "") && gameTypeStr.search("Pool") != -1)
                            this.showNotification((leftPlStr + " left the table!!"), true);
                        else
                            this._leftPlArr = [];
                    }
                    if ((this._myId != -1) && (!this._dropped) && this._hasDistributionComp) {
                        this._userCardPanel.hideAllItems(false);
                        this._userCardPanel.meldConfirmPopUp && this._userCardPanel.meldConfirmPopUp.removeFromParent(true);
                        this.deckPanel.hideAllItems(false);
                    }
                    this._leaveAllowed = true;
                }
            }
            else if (changedVars[s] == SFSConstants._NOOFPLAYERS) {
                this.showWaiting();
                //todo this.updateNoOfPlayrs();
            }
            else if (changedVars[s] == SFSConstants._WILDCARD) {
                var wildCardName = room.variables[SFSConstants._WILDCARD] && room.variables[SFSConstants._WILDCARD].value;
                this.wildCard(wildCardName);
            } else if (changedVars[s] == SFSConstants._DEALERID) {
                this.updateDealer(room.variables[SFSConstants._DEALERID].value);
            }
            else if (changedVars[s] == SFSConstants._TURNTIME) {
                //todo mainProfileMC.setTurnTime();
            }
        }
    },
    handleCurrentTurn: function (turnTime, extraStr) {
        if (extraStr === undefined)
            extraStr = null;

        SoundManager.removeSound(SoundManager.extratimesound);

        this.pickWaitMC.setVisible(false);
        this.deckPanel.disable();
        this._userCardPanel && this._userCardPanel.dropConfirmPopup && this._userCardPanel.dropConfirmPopup.removeFromParent(true);
        this._userCardPanel._isShowReqSent = false;
        this._isExtraTurnTime = false;
        if (this.meldingPanel && !this._dropped)  /// if wrong show then game vill continue btwn other plyers
        {
            this.meldingPanel.removeFromParent();
            this._userCardPanel.setVisible(true);
            this._userCardPanel.dropVisible();
            this.deckPanel.setVisible(true);
        }
        if (this._hasDistributionComp) {

            var seatObj = this.getSeatObjByUser(this._currentTurnID);
            if (seatObj && seatObj._seatPosition != -1) {
                this._currentTurnSeat = seatObj;
                this._tabMc && this._tabMc.subMiniTable.setTurnIndicator(this._serverRoom.getVariable(SFSConstants._MAXPLAYERS).value, this._currentTurnSeat._seatPosition);
                if (this._extraTimeToggle)
                    this.scheduleWaitTime(SFSConstants.ETT_MELDEXTRATIMER, turnTime);
                else
                    this.scheduleWaitTime(extraStr, turnTime);

                if (this._currentTurnID == this._myId) {

                    SoundManager.playSound(SoundManager.myturnsound, false);

                    this.thumbnailBlink();
                    this._userCardPanel.dropConfirmPopup && this._userCardPanel.dropConfirmPopup.message.setString("Are you sure you want to drop?");
                    var card;
                    if (this._currentRoomState != SFSConstants.RS_MELD) {
                        this._userCardPanel.resetFilter();
                    }
                    if (!this._userCardPanel.isMyDrop(1) && !this.autoPlayPopup) {
                        this.deckPanel.enable();
                        this._userCardPanel.dropVisible();
                    }

                    //todo check deck panel enabled condition in case of autoplay and normal game
                    var numOfCard = this._serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;
                    if (parseInt(numOfCard) == 13) {
                        if (this._userCardPanel.cardStack.length != 0 && this._userCardPanel.cardStack.length < 14) {
                            this.deckPanel.enable();
                            this.deckPanel.glowEnabled(true);
                        }
                    } else {
                        if (this._userCardPanel.cardStack.length != 0 && this._userCardPanel.cardStack.length < 22) {
                            this.deckPanel.enable();
                            this.deckPanel.glowEnabled(true);
                        }
                    }
                }
                else {
                    if (this._myId != -1 && !this._isWaiting && !this._isWatching) {
                        this._userCardPanel.dropConfirmPopup && this._userCardPanel.dropConfirmPopup.message.setString("Are you sure you want to drop on your turn?");
                        this._userCardPanel.isMyDrop(2);
                        this._userCardPanel.dropVisible();
                        this.deckPanel.glowEnabled(false);
                    }
                }
                if (this._userCardPanel.showConfirmPopup) {
                    this._userCardPanel.showConfirmPopup.removeFromParent(true);
                    if (!this._userCardPanel.cardStack[this.totCards + 1]) {
                        this._userCardPanel.showBtn.hide(true);
                        this._userCardPanel.discardBtn.hide(true);
                    }
                }
            }
            if (this._dropped) {
                this.deckPanel.hideAllItems(false);
            }
        }
    },
    leaveBtnHandler: function () {

        if (this._currentRoomState == SFSConstants.RS_JOIN && this.confirmationPopup && this._leaveAllowed) {
            if (this._requestToSever) {
                this.sendReqToServer(SFSConstants.CMD_CLICK_LEAVE_TABLE, {});
                this.leaveRoomByMyself();
            }
            return;
        }

        if (!this._leaveAllowed) {
            return;
        }
        this._mySeatIndex = -1;

        if (!this.leaveTablePopup) {
            var tId = this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_ID).value.toString() + "?";
            this.leaveTablePopup = new LeaveTable(this.tableId, this, true);
            this.leaveTablePopup.message.setString("Are you sure you want to leave the tournament ID " + tId);
            this.addChild(this.leaveTablePopup, GameConstants.leaveZorder, "leaveTablePopup");
        }
    },
    handleResult: function (dataObj) {
        SoundManager.removeSound(SoundManager.extratimesound);
        this._tournamentGamePopup.removePopUpExceptRebuy();
        this.result && this.result.removeFromParent(true);
        this.WrongShowMsg && this.WrongShowMsg.removeWrongShowLayer();
        this.deckPanel.wildcard.removeToolTip();
        this.meldingPanel && this.meldingPanel.removeFromParent(true);
        this.leaveTablePopup && this.leaveTablePopup.removeFromParent(true);
        this._userCardPanel.meldConfirmPopUp && this._userCardPanel.meldConfirmPopUp.removeFromParent(true);
        this.settingPanel.isVisible() && this.settingPanel.setVisible(false);

        this._isWaiting = false;
        this.isDropped = false;
        this.isautoPlay = false;
        this.isWrongShow = false;
        this.isDisconnected = false;
        this._isShowDistribution = true;
        this.lastPickCard = "";

        this.removeClosedCard();
        this.stopWaitTimer();

        for (var i = 0; i < this._playerArray.length; i++) {
            this._playerArray[i].clearAllState();
        }

        if (this.historyPanel) {
            this.historyPanel._historyCards = "";
            this.historyPanel.removeFromParent(true);
        }

        var txtStr = "";
        var currentLvl = "", maxLvl = "", curDeal = "", maxDeal = "";

        if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Round") {

            if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL)) {
                currentLvl = this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value;
            }
            /*else {
             currentLvl = this._currentLevel;
             }*/

            if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL)) {
                maxLvl = this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL).value;
            }
            /*else {
             maxLvl = this._maxLevel;
             }*/

            if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_DEAL)) {
                curDeal = this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_DEAL).value;
            }
            /*else {
             curDeal = this._currentRound;
             }*/

            if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_DEAL)) {
                maxDeal = this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_DEAL).value;
            }
            /*else {
             maxDeal = this._maxDeal;
             }*/

            txtStr = "Round - " + currentLvl + "/" + maxLvl + " | Deal : " + curDeal + "/" + maxDeal;
        }
        else {

            if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL))
                txtStr = "Level - " + this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value;

            if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL))
                txtStr += "/" + this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL).value;

            var myProf = this.getSeatObjByUser(this._myId);
            if (myProf && myProf.player_rank != -1)
                txtStr = txtStr + " | Rank : " + myProf.player_rank + "/" + dataObj["maxRank"];
            else {
                txtStr = txtStr + " | Rank : " + this._trnmentInfoMC._myRank + "/" + dataObj["maxRank"];
            }
        }

        /*for (var i = 1; i <= parseInt(dataObj.tot); i++) {
         var totChip = dataObj["pl" + i]["totalChip"];
         var name = dataObj["pl" + i]["name"];
         var prof = this.getSeatObjByUserName(name);
         prof.setScore(totChip);
         }*/

        // if (dataObj["txtStr"] && dataObj["txtStr"] == "")
        dataObj.txtStr = txtStr;

        this.showResult(dataObj, true, false);

        if (this._serverRoom.getVariable(SFSConstants._WILDCARD) && this._serverRoom.getVariable(SFSConstants._WILDCARD).value)
            this.result.setWildCard(this._serverRoom.getVariable(SFSConstants._WILDCARD).value);
        else
            this.result.setWildCard("");

        //todo wrongShowMC.text = "";
    },
    showResult: function (param, _showTime, _isPool) {
        //todo check with flash
        /*todo discardBtnMC.close();
         discardBtnMC.gotoAndStop(1);
         */
        this.discardButton.isPressed(false);
        this.discardButton._isEnabled = false;
        this.discardPanel && this.discardPanel.hideDiscardPanel();
        /*todo this.scorePanel.visible = false;

         SoundManager.callBack.removeSound("extratimesound");*/

        this.stopWaitTimer();
        this.sendUpdateTab();

        //todo mainProfileMC.clearSeatObject();
        /*todo toolTip.visible = false;
         toolTip.txt.text = "";*/

        this._isShowDistribution = true;
        this.messagePopUp && this.messagePopUp.removeFromParent(true);
        this.meldingPanel && this.meldingPanel.removeFromParent(true);
        this._userCardPanel.meldConfirmPopUp && this._userCardPanel.meldConfirmPopUp.removeFromParent(true);

        this.deckPanel.hideAllItems(true);
        this._userCardPanel.clearCards();
        this._userCardPanel.hideAllItems(true);
        //todo dropPopUp.visible = false;

        this.lastPickCard = "";

        this.historyPanel && this.historyPanel.removeFromParent(true);
        this.scoreBoardPanel && this.scoreBoardPanel.setVisible(false);

        this.setDealerpos(false);

        for (var i = 0; i < this._playerArray.length; i++) {
            this._playerArray[i].hideTimer();
            this._playerArray[i].removeIcon();
        }

        this._currentTurnSeat = null;
        this.autoPlayPopup && this.autoPlayPopup.removeFromParent(true);

        var numOfCard = this._serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;
        cc.log("result", param);

        this.removeClosedCard();

        this.result = new Result_Tour(this, param, _showTime, _isPool);
        this.addChild(this.result, GameConstants.gameResultZorder, "result");

    },
    handleDistributionCompl: function () {

        this.deckPanel.hideAllItems(false);
        this.arrangeClosedCard();

        if (this._serverRoom.getVariable(SFSConstants._ROOMSTATE).value && (this._serverRoom.getVariable(SFSConstants._ROOMSTATE).value == SFSConstants.RS_RESULT)) {
            this._userCardPanel.clearCards();
            return;
        }
        if (this._userCardPanel._killDistribution) {
            //this._userCardPanel.killDistribution();
            return;
        }
        this._hasDistributionComp = true;

        if (this._myId != -1) {
            if (this.historyPanel)
                this.historyPanel._isEnabled = true;
        }
        else {
            if (this.historyPanel)
                this.historyPanel._isEnabled = false;
            this._userCardPanel.hideAllItems(true);
        }
        // this.deckPanel.openDeck && this.deckPanel.openDeck.hide(true);
        var ODStr = this._serverRoom.getVariable(SFSConstants.OPEN_DECK_CARD).value;
        var cardsArr = ODStr.split(",");

        if (cardsArr[0] != this.lastPickCard) {
            this.deckPanel.openDeck.setOpenDeckCards();
        }

        this.deckPanel.setVisible(true);
        var obj;
        for (var i = 0; i < this._playerArray.length; ++i) {
            obj = this._playerArray[i];
            if (obj && obj._seatPosition != -1 && !obj._isNext && !obj.isWrongShow) {
                //if (this._myId != obj._playerId )
                //todo show closecard for obj._seatPosition
                //closedCardsMainMC.closedCardsGroupMC.getChildByName("closedCardsMC" + obj._seatPosition).visible = true;
            }
        }
        this.discardButton._isEnabled = true;

        if (this._currentRoomState != SFSConstants.RS_MELD)
            this.handleCurrentTurn();
    },
    showLevelStatusMsg: function (dataObj) {
        if (this._result && this._serverRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Time") {
            this._result.updateRank(dataObj["rank"]);
        }
        var statusArr = dataObj["str"].split("#");
        cc.log('levelCompleteStatus', dataObj);
        var self = this;
        this._qualified = false;

        switch (statusArr[0] + "") {
            case "eliminate":
            {
                this._tournamentGamePopup.getLostRoundPopUp(statusArr[1], statusArr[2]);
                this._tournamentGamePopup.getLostRoundNotification(statusArr[2]);
                this.freeAllSeat();
                this.setDefaultProfile();
                //todo mainProfileMC.joinVisible(true);
                this.deckPanel.hideAllItems(true);

                cc.log("remove thumbnail:", dataObj["eliminateTime"]);
                var et = parseInt(dataObj["eliminateTime"]);
                setTimeout(function () {
                    self.leaveRoomByMyself();
                }, et * 1000);

                break;
            }
            case "eliminateWithPrize":
            {
                this._tournamentGamePopup.getRoundWinnerPopUp(statusArr[1], statusArr[2], this.getPrize(statusArr[3], statusArr[4]));
                this._tournamentGamePopup.getWinnerRoundNotification(statusArr[2], this.getPrize(statusArr[3], statusArr[4]));
                this.freeAllSeat();
                this.setDefaultProfile();
                this.deckPanel.hideAllItems(true);
                break;
            }
            case "qualifier":
            {
                this._qualified = true;
                this.freeAllSeat();
                console.log("Qualifier");
                this.setDefaultProfile();
                console.log("SetDefaultProfile");
                if (this._serverRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Time") {
                    this._tournamentGamePopup.getQualifyPopup("Time");
                    var profle = this.getSeatObjByUser(this._myId);
                    if (profle && profle._seatPosition != -1)
                        profle.updateRank(dataObj["rank"], dataObj["chips"]);

                    this._myRank = dataObj["rank"];
                    this._myChip = dataObj["chips"];

                    if (this._trnmentInfoMC)
                        this._trnmentInfoMC.updateRank(dataObj["rank"], dataObj["chips"]);
                }
                else
                    this._tournamentGamePopup.getQualifyPopup("Round");//(' 3', _levelTime);
                //todo this.joinVisible(true);

                this._userCardPanel.hideAllItems(true);
                this.deckPanel.hideAllItems(true);
                break;
            }
            case "winner":
            {
                this.levelTimer && this.levelTimer.setVisible(false);

                SoundManager.removeSound(SoundManager.validshow);
                SoundManager.removeSound(SoundManager.wrongshow);
                SoundManager.playSound(SoundManager.finalwinningifme);

                this._tournamentGamePopup.removePopUp();
                this._tournamentGamePopup.setTourWinnerPopUP(statusArr[1], this.getPrize(statusArr[2], statusArr[3]));
                this.removeAllPlayers();

                break;
            }
            case "waitForTournamentFinish":
            {
                if (dataObj["rank"] && this._serverRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Time") {
                    this._tournamentGamePopup.getWaitForTournamentFinishPopUp(dataObj["rank"]);
                    this._tournamentGamePopup.getWaitForTournamentFinishNot(dataObj["rank"]);
                }
                if (this._serverRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Time") {
                    this._myRank = dataObj["rank"];
                    this._myChip = dataObj["chips"];
                    if (this._trnmentInfoMC)
                        this._trnmentInfoMC.updateRank(dataObj["rank"], dataObj["chips"]);
                }
                this._isWaiting = true;
                this.freeAllSeat();
                this.setDefaultProfile();

                if (dataObj["eliminateTime"]) {
                    var time = parseInt(dataObj["eliminateTime"]);
                    setTimeout(function () {
                        self.leaveRoomByMyself();
                    }, time * 1000)
                }
                this.deckPanel.hideAllItems(true);
                break;
            }
        }
        this._tabMc.subMiniTable.turnIndicator.setVisible(false);
        this._tabMc.subMiniTable.extraMC.setVisible(false);
        this._tabMc.subMiniTable.setTimer("");
    },
    getPrize: function (przStr, przVal) {
        var formatStrArr = przStr.split("|");
        var przFrmtStrArr = przVal.split("|");
        var j = 0;
        var prizeStr = "";
        for (j = 0; j < formatStrArr.length; j++) {
            if (j == formatStrArr.length - 1)
                prizeStr += przFrmtStrArr[j] + "";
            else
                prizeStr += przFrmtStrArr[j] + "+";
        }
        if ((formatStrArr[0] + "") == "Chip")
            prizeStr = "`" + prizeStr;
        return prizeStr;
    },
    handleTieBreaker: function (dataObj) {
        this._isRoomActive = false;
        applicationFacade._parentRoomIds[this._serverRoom.getVariable(SFSConstants.PARENT_EXT_ROOM_ID).value] = this._roomId;
        var txt = "You will play Tie Breaker Deal with " + dataObj["playerNameList"] + " to resolve ties. Please wait for your Tie Breaker Deal will start shortly.";
        this._tournamentGamePopup.getTieBreakerPopUp(txt, "~4 Min");
    },
    handleWaitForTieBreaker: function (dataObj) {
        var txt = "Please wait for Tie Breaker Deal.";
        this._tournamentGamePopup.getTieBreakerPopUp(txt, "~4 Min");
    },
    removeResult: function (dataObj) {
        if (this.result) {
            this.result.stopResultTimer();
            this.result.removeFromParent();
        }
        this._tournamentGamePopup.removePopUp();
    },
    setLevelEndWarning: function (dataObj) {
        this._tournamentGamePopup.getLevelTimeOverPopup("1");
    },
    updatePlayerRank: function (dataObj) {
        this.updateAllRank(dataObj);
        if (this._isWaiting)
            this._tournamentGamePopup.updateRank(dataObj);
        var myProf = this.getSeatObjByUser(this._myId);
        if (this._result && myProf && this._serverRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Time") {
            this._result.updateRank(myProf.player_rank);
        }
    },
    handleIsRejoin: function (dataObj) {
        this.removeClosedCard();
        this._tournamentGamePopup.getRebuyPopup(dataObj["rejoinChips"], dataObj["rejoinChipAmount"], dataObj["rejoinFrom"]);
    },
    handleTourRejoinResp: function (dataObj) {
        this.removeClosedCard();
        this._tournamentGamePopup.getRebuyDonePopup(dataObj["chips"]);
    },
    handleDropWaiting: function (dataObj) {
        this._tournamentGamePopup.getDropWaiting();
        var pro = this.getSeatObjByUser(this._myId);
        if (pro)
            pro.updateRank(dataObj["rank"], dataObj["chip"]);
        this._myRank = dataObj["rank"];
        this._myChip = dataObj["chips"];
        if (this._trnmentInfoMC)
            this._trnmentInfoMC.updateRank(dataObj["rank"], dataObj["chip"]);
    },
    handleTeaTime: function (dataObj) {
        //todo handle tea time
        this._tournamentGamePopup.getBreakTimePopup(1000);
    },
    addRebuyCash: function (dataObj) {
        this._tournamentGamePopup.getAddCashReBuyPopUp();
    },
    updateDeal: function (dataObj) {

    },
    handleLevelExtraTime: function (dataObj) {
        this.showNotification("Level extra time start.", true, 10000);
    },
    updatePlayerInfo: function (userType, player, isDropped, isDisconnected, isAutoPlay, isWrongShow, isWaiting) {
        switch (userType) {
            case KHEL_CONSTANTS.USER_ICON_UPDATE :
                if (isDisconnected) {
                    // player.setIcon("Disconnected", "autoPlay");
                } else if (isAutoPlay) {
                    player.setIcon("Auto Mode", "autoPlay");
                    if (parseInt(player._playerId) == this._myId /*&& !this.reconnectBtn.isVisible()*/) { // TODO reconnect button
                        this.showAutoPlayMessage();
                    }
                } else if (isWrongShow) {
                    player.setIcon("Wrong Show", "wrongShow");
                    if (parseInt(player._playerId) != this._myId)
                        this.removeClosedCardForSpecificPlayer(player);
                } else if (isDropped) {
                    player.setIcon("Dropped", "DropIcon");
                } else if (isWaiting) {
                    player.setIcon("Waiting", "WaitingIcon");
                } else {
                    player.removeIcon();
                }
                break;
            case KHEL_CONSTANTS.USER_LEFT:
                if (this.confirmationPopup) {
                    this.confirmationPopup._leaveTable();
                }
        }
    },
    showWaiting: function () {
        var noOfPl = this._serverRoom.getVariable(SFSConstants._NOOFPLAYERS).value;
        if (noOfPl <= 1)
            this._userCardPanel && this._userCardPanel.killDistribution();
        if (noOfPl == 1) {
            if (this._tabMc)
                this._tabMc.subMiniTable.turnIndicator.setVisible(false);
        }
    },
    leaveRoomByMyself: function () {
        this._leaveTableByPlayer = true;
        SoundManager.removeSound(SoundManager.extratimesound);
        if (!(this._serverRoom.getVariable("isDummyRoom") && this._serverRoom.getVariable("isDummyRoom").value) && sfsClient.isMeInRoom(this._roomId))
            this._requestToSever = sfsClient.roomLeave(this._serverRoom);
        else {
            applicationFacade.OnRoomLeft(this._roomId);
            this._requestToSever = false;
        }
        this.stopWaitTimer();
        this.result && this.result.leaveRoom();
        this._stopAllTimer = true;
        this._requestToSever = sfsClient.roomLeave(this._serverRoom);
        // this._playerArray = null;

        /*if (this._serverRoom.id == sfsClient.currentUser.id && this.tableId == applicationFacade._activeTableId) {
         if (applicationFacade._gameMap[this.tableId]) {
         this.tableId && applicationFacade._controlAndDisplay.removeRoomTab(this.tableId);
         }
         }*/
    },
    setTournamentInfo: function () {

        this._trnmentInfoBtn = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("smileyBorder.png"), cc.rect(0, 0, 41 * scaleFactor, 41 * scaleFactor));
        this._trnmentInfoBtn.setCapInsets(cc.rect(16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor, 16 * scaleFactor));
        this._trnmentInfoBtn.setContentSize(cc.size(50 * scaleFactor, 52 * scaleFactor));
        this._trnmentInfoBtn.setName("tournamentInfo");
        this._trnmentInfoBtn.setPosition(cc.p(17 * scaleFactor, this.height - 62 * scaleFactor));

        this.prizeCup = new cc.Sprite(spriteFrameCache.getSpriteFrame("prize.png"));
        this.prizeCup.setPosition(cc.p(20 * scaleFactor, this.height - 62 * scaleFactor));
        this.prizeCup.setName("tournamentInfo");
        this.addChild(this._trnmentInfoBtn, GameConstants.cardMinZorder - 1);
        this.addChild(this.prizeCup, GameConstants.cardMinZorder + 1);

        cc.eventManager.addListener(this.baseGameTouchListener.clone(), this._trnmentInfoBtn);

        if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Round")
            this._trnmentInfoMC = new RoundTournamentInfo(this, {});
        else
            this._trnmentInfoMC = new TimeTournamentInfo(this, {});

        this.addChild(this._trnmentInfoMC, GameConstants.cardMinZorder, "_trnmentInfoMC");

    },
    handleInfoClicked: function () {
        if (this._trnmentInfoMC.isVisible())
            this._trnmentInfoMC.showTourInf(false);
        else
            this.sendReqToServer(SFSConstants.TOURNAMENT_INFO, {});
    },
    handleTourInfo: function (dataObj) {
        this._trnmentInfoMC.showTournamentInfo(dataObj);
    },
    processGameOver: function (data) {
        cc.log("\n\nwinner data" + data);
        SoundManager.removeSound(SoundManager.extratimesound);
        this._currentRound = 0;
        this.updateHeader();
        this.stopWaitTimer();
        this.initPopup();

        this._isWatching = true;

        this._tournamentGamePopup.removePopUp();
        this.result && this.result.waitTxt.setVisible(false);

        this.winner = new Winner(data, this);
        this.addChild(this.winner, GameConstants.winnerScreenZorder, "winner");

        this._myId = -1;

        for (var i = 0; i < this._playerArray.length; i++) {
            cc.eventManager.removeListener(this._playerArray[i], true);
            this._playerArray[i].removeFromParent(true);
        }
        this._playerArray = [];
        this.initRoom();

    },
    updateAllRank: function (obj) {
        var plyrsRank = obj["rankStr"].split(";");
        var id;
        var profle;
        for (var j = 0; j < plyrsRank.length; j++) {
            id = plyrsRank[j].split(":")[0];
            if (id == this._myId) {
                this._myRank = plyrsRank[j].split(":")[1];
                this._myChip = plyrsRank[j].split(":")[2];
                if (this._trnmentInfoMC)
                    this._trnmentInfoMC.updateRank(this._myRank, this._myChip);
            }
            profle = this.getSeatObjByUser(id);
            if (profle && profle._seatPosition != -1)
                profle.updateRank(plyrsRank[j].split(":")[1], plyrsRank[j].split(":")[2]);
        }
    },
    updateRoom: function (room) {
        this._serverRoom = room;
        this._isRoomActive = true;
        this._leaveTableByPlayer = false;
        this.requestToSever = true;
        this.initPopup();
        this.initScreen();
        this.updateHeader();
        this._tabMc.setIcon(1);
        this._tabMc.subMiniTable.turnIndicator.setVisible(false);
        this._tabMc.subMiniTable.extraMC.setVisible(false);
        this._tabMc.subMiniTable.setTimer("");

        this._tabMc.startGlow(1);
        this._tabMc.label.setString(this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value);
        if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL) && this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL)) {
            if (this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Round")
                this._tabMc.subMiniTable.setTableId("Round: " + this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value + "/" + this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL).value);
            else
                this._tabMc.subMiniTable.setTableId("Level: " + this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value + "/" + this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL).value);
        }
        this._tabMc.subMiniTable.setGameTF(this.parentRoom.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value);
        this._tabMc.subMiniTable.addTableplayers(this._totalSeats);

        var bgClr = (this.totCards == 21) ? cc.color(56, 35, 8) : cc.color(33, 38, 17);
        this._tabMc.setBgColor(bgClr);
        if (this.result) {
            this.result.stopResultTimer();
            this.result.removeFromParent(true);
            this.result = null;
        }
        this._trnmentInfoMC && this._trnmentInfoMC.updateRoom(room);
    },
    joinSeat: function (seatObj) {
        this._mySeatIndex = parseInt(seatObj.seatNo);
        this.selfSeatPosition = this._mySeatIndex;
        console.log('Seat position changed ' + this.selfSeatPosition);
        this._isNoBal = false;
        var obj = {};
        obj[SFSConstants.FLD_SEATNO] = parseInt(seatObj.seatNo);
        this.sendReqToServer(SFSConstants.CMD_CONFIRM, obj);
    }
});