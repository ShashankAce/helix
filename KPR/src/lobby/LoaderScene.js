/**
 * Created by stpl on 5/8/2017.
 */
var LoaderScene = cc.Scene.extend({
    _className: "LoaderScene",
    _interval: null,
    _label: null,
    cb: null,
    target: null,
    ctor: function () {
        this._super();

        var bg = new cc.Sprite(loaderRes.bg);
        bg.setPosition(cc.p(cc.winSize.width / 2, cc.winSize.height / 2));
        this.addChild(bg, 1);

        this.cardNodes = new cc.Node();
        this.cardNodes.setPosition(cc.p(cc.winSize.width / 2, cc.winSize.height / 2));
        this.addChild(this.cardNodes, 5);

        this.space = 80 * scaleFactor;

        this.club = this.getCard(loaderRes.club, cc.size(85 * scaleFactor, 85 * scaleFactor));
        this.club.setPosition(cc.p(0, 0));
        this.cardNodes.addChild(this.club, 1);
        this.club.clipper.addChild(new cc.Sprite(loaderRes.clubFilled));

        this.diamond = this.getCard(loaderRes.diamond, cc.size(73 * scaleFactor, 103 * scaleFactor));
        this.diamond.setScale(0.5);
        this.diamond.setPosition(cc.p(this.space, 0));
        this.cardNodes.addChild(this.diamond, 1);
        this.diamond.clipper.addChild(new cc.Sprite(loaderRes.diamondFilled));

        this.heart = this.getCard(loaderRes.heart, cc.size(95 * scaleFactor, 82 * scaleFactor));
        this.heart.setScale(0.5);
        this.heart.setPosition(cc.p(this.space * 2, 0));
        this.cardNodes.addChild(this.heart, 1);
        this.heart.clipper.addChild(new cc.Sprite(loaderRes.heartFilled));

        this.spade = this.getCard(loaderRes.spade, cc.size(91 * scaleFactor, 95 * scaleFactor));
        this.spade.setScale(0.5);
        this.spade.setPosition(cc.p(this.space * 3, 0));
        this.cardNodes.addChild(this.spade, 1);
        this.spade.clipper.addChild(new cc.Sprite(loaderRes.spadeFilled));

        return true;
    },
    getCard: function (image, size) {

        var node = new cc.Node();
        node.size = size;

        var spr = new cc.Sprite(image);
        node.addChild(spr, 1);

        var drawNode = new cc.DrawNode();
        drawNode.drawRect(cc.p(-size.width / 2 - 5, -size.height + -size.height / 2 - 5), cc.p(size.width / 2, -size.height / 2), cc.color(255, 255, 255), 1, cc.color(255, 255, 255));

        node.clipper = new cc.ClippingNode();
        node.clipper.setStencil(drawNode);
        node.addChild(node.clipper, 2);

        return node;
    },
    updateCards: function (percent) {

        if (percent <= 25) {
            this.club.clipper.stencil.y = this.club.size.height * (percent / 100 * 4);

        } else if (percent > 25 && percent <= 50) {

            if (!this.diamond.isMoved) {
                this.club.clipper.stencil.y = this.club.size.height;
                this.cardNodes.runAction(cc.moveBy(0.2, -this.space, 0));
                this.club.runAction(cc.scaleTo(0.2, 0.5, 0.5));
                this.diamond.runAction(cc.scaleTo(0.2, 1, 1));
                this.diamond.isMoved = true;
            }

            this.diamond.clipper.stencil.y = this.diamond.size.height * ((percent - 25) / 100 * 4);

        } else if (percent > 50 && percent <= 75) {

            if (!this.heart.isMoved) {
                this.cardNodes.stopAllActions();
                this.club.stopAllActions();
                this.diamond.stopAllActions();

                this.cardNodes.x = -this.space + cc.winSize.width / 2;
                this.club.setScale(0.5);
                this.diamond.setScale(1);
                this.diamond.clipper.stencil.y = this.diamond.size.height;

                this.cardNodes.runAction(cc.moveBy(0.2, -this.space, 0));
                this.diamond.runAction(cc.scaleTo(0.2, 0.5, 0.5));
                this.heart.runAction(cc.scaleTo(0.2, 1, 1));
                this.heart.isMoved = true;
            }
            this.heart.clipper.stencil.y = this.heart.size.height * ((percent - 50) / 100 * 4);

        } else if (percent > 75 && percent <= 100) {
            if (!this.spade.isMoved) {
                this.cardNodes.stopAllActions();
                this.diamond.stopAllActions();
                this.heart.stopAllActions();

                this.cardNodes.x = -this.space * 2 + cc.winSize.width / 2;
                this.diamond.setScale(0.5);
                this.heart.setScale(1);
                this.heart.clipper.stencil.y = this.heart.size.height;

                this.cardNodes.runAction(cc.moveBy(0.2, -this.space, 0));
                this.heart.runAction(cc.scaleTo(0.2, 0.5, 0.5));
                this.spade.runAction(cc.scaleTo(0.2, 1, 1));
                this.spade.isMoved = true;
            }
            this.spade.clipper.stencil.y = this.spade.size.height * ((percent - 75) / 100 * 4);
        }
    },
    init: function () {
        var self = this;
        var label = self._label = new cc.LabelTTF("Loading... 0%", "Arial", 15);
        label.setPosition(cc.pAdd(cc.visibleRect.center, cc.p(0, -80)));
        label.setColor(cc.color(180, 180, 180));
        this.addChild(self._label, 10);
        return true;
    },

    _initStage: function (img, centerPos) {
        var self = this;
        var texture2d = self._texture2d = new cc.Texture2D();
        texture2d.initWithElement(img);
        texture2d.handleLoadedTexture();
        var logo = self._logo = new cc.Sprite(texture2d);
        logo.setScale(cc.contentScaleFactor());
        logo.x = centerPos.x;
        logo.y = centerPos.y;
        self._bgLayer.addChild(logo, 10);
    },
    /**
     * custom onEnter
     */
    onEnter: function () {
        var self = this;
        cc.Node.prototype.onEnter.call(self);
        self.schedule(self._startLoading, 0.3);
    },
    /**
     * custom onExit
     */
    onExit: function () {
        cc.Node.prototype.onExit.call(this);
        var tmpStr = "Loading... 0%";
        this._label.setString(tmpStr);
    },

    /**
     * init with resources
     * @param {Array} resources
     * @param {Function|String} cb
     * @param {Object} target
     */
    initWithResources: function (resources, cb, target) {
        if (cc.isString(resources))
            resources = [resources];
        this.resources = resources || [];
        this.cb = cb;
        this.target = target;
    },

    _startLoading: function () {
        var self = this;
        self.unschedule(self._startLoading);
        var res = self.resources;
        cc.loader.load(res,
            function (result, count, loadedCount) {
                var percent = (loadedCount / count * 100) | 0;
                percent = Math.min(percent, 100);
                self._label.setString("Loading... " + percent + "%");
                self.updateCards(percent);
            }, function () {
                if (self.cb)
                    self.cb.call(self.target);

                self.cardNodes.stopAllActions();
                self.heart.stopAllActions();
                self.spade.stopAllActions();

                self.cardNodes.x = -self.space * 3 + cc.winSize.width / 2;
                self.heart.setScale(0.5);
                self.spade.setScale(1);
                self.spade.clipper.stencil.y = self.spade.size.height;

            });
    },

    _updateTransform: function () {
        this._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
        this._bgLayer._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
        this._label._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
        this._logo._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
    }
});
/**
 * <p>cc.LoaderScene.preload can present a loaderScene with download progress.</p>
 * <p>when all the resource are downloaded it will invoke call function</p>
 * @param resources
 * @param cb
 * @param target
 * @returns {cc.LoaderScene|*}
 * @example
 * //Example
 * cc.LoaderScene.preload(g_resources, function () {
        cc.director.runScene(new HelloWorldScene());
    }, this);
 */
LoaderScene.preload = function (resources, cb, target) {
    var _cc = cc;
    if (!_cc.loaderScene) {
        _cc.loaderScene = new LoaderScene();
        _cc.loaderScene.init();
        cc.eventManager.addCustomListener(cc.Director.EVENT_PROJECTION_CHANGED, function () {
            _cc.loaderScene._updateTransform();
        });
    }
    _cc.loaderScene.initWithResources(resources, cb, target);

    cc.director.runScene(_cc.loaderScene);
    return _cc.loaderScene;
};