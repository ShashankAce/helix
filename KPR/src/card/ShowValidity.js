/**
 * Created by stpl on 12/7/2016.
 */

var ShowValidityLayout = ccui.Layout.extend({
    _gameRoom: null,
    _dropDownSelection: "Pure Sequence",
    _toggleBtnArr: [],
    ctor: function (context) {
        this._super();
        this._gameRoom = context;
        this.setClippingEnabled(true);
        this.setContentSize(cc.size(460 * scaleFactor, (43 + 95) * scaleFactor));

        /*this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
         this.setBackGroundColor(cc.color(255, 0, 255));
         this.setBackGroundColorOpacity(100);*/

        return true;
    },
    _init: function () {
        if (this.showValidity) {
            this.removeAllChildren(true);
        }
        this.showValidity = new ShowValidity(this._gameRoom, this);
        this.showValidity.setPosition(cc.p(-this.showValidity.width / 2 + 30 * scaleFactor, this.showValidity.height / 2));
        this.showValidity._isEnabled = true;
        this.addChild(this.showValidity, 2, 'showValidity');

    },
    toggleValidity: function (bool) {
        if (bool) {
            //show
            this.showValidity.runAction(cc.moveTo(0.3, cc.p(this.showValidity.width / 2, this.showValidity.height / 2)));
        } else {
            //hide
            this.showValidity.runAction(cc.moveTo(0.3, cc.p(-this.showValidity.width / 2 + 30, this.showValidity.height / 2)));
        }
    },
    hide: function (bool) {

        if (this.showValidity)
            this.showValidity._isEnabled = !bool;
        this.setVisible(!bool);

    },
    remove: function () {
        this.removeAllChildren(true);
    },
    checkValidity: function () {
        this.showValidity && this.showValidity.checkValidity()
    },
    checkValidity13: function () {
        this.showValidity && this.showValidity.checkValidity13()
    },
    setScore: function (score) {
        this.showValidity && this.showValidity.setScore(score);
    }
});
var ShowValidity = cc.Sprite.extend({
    noOfCard: null,
    _gameRoom: null,
    _delegate: null,
    _finishBtn: null,
    _finishX: 0,
    _leftX: 0,
    _startP: 0,
    _endP: 0,
    _isOpened: null,
    _isEnabled: null,
    validityDropDownMenu: null,
    validityDropDown: null,
    ctor: function (gameRoomContext, showValidityLayout) {
        this._gameRoom = gameRoomContext;
        this._delegate = showValidityLayout;
        this._isOpened = false;
        this._isEnabled = true;
        this._name = "showValidity";

        this.initEventListener();

        this.noOfCard = this._gameRoom._serverRoom.getVariable(SFSConstants.FLD_NUMOFCARD).value;
        if (this.noOfCard == 13) {
            this._super(spriteFrameCache.getSpriteFrame("ShowValidityBg13.png"));
            this._finishX = this.width - 30 * scaleFactor - 50 * scaleFactor;
            this._leftX = 285 * scaleFactor;
            this._startP = 77 * scaleFactor;
            this.dis = 130 * scaleFactor;
        }
        else {
            this._super(spriteFrameCache.getSpriteFrame("ShowValidityBg21.png"));
            this._finishX = this.width - 30 * scaleFactor - 151 * scaleFactor;
            this._leftX = 285 * scaleFactor;
            this._startP = 20 * scaleFactor;
            this.dis = 232 * scaleFactor;

            var ValidityDropDownMenu = cc.Scale9Sprite.extend({
                _name: "validityMenu",
                ctor: function () {
                    this._super(spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png"), cc.rect(0, 0, 50 * scaleFactor, 50 * scaleFactor));
                    this.setCapInsets(cc.rect(13 * scaleFactor, 13 * scaleFactor, 13 * scaleFactor, 13 * scaleFactor));
                    this.setContentSize(cc.size(110 * scaleFactor, 28 * scaleFactor));

                    // this.setColor(cc.color(255,255,255));
                    // this.setCascadeColorEnabled(false);
                    // this.setCascadeOpacityEnabled(false);

                    this.label = new cc.LabelTTF("Pure Sequence", "RobotoBold", 11 * 2 * scaleFactor);
                    this.label.setScale(0.5);
                    this.label.setColor(cc.color(0, 0, 0));
                    this.label.setPosition(cc.p(this.width / 2 - 5 * scaleFactor, this.height / 2));
                    this.label._name = "label";
                    this.addChild(this.label, 1);


                    var arrow = new cc.Sprite(spriteFrameCache.getSpriteFrame("myTableArrow.png"));
                    arrow.setRotation(180);
                    arrow.setColor(cc.color(0, 0, 0));
                    arrow.setPosition(cc.p(this.width - 10 * scaleFactor, this.height / 2));
                    this.addChild(arrow, 1);

                    return true;
                }
            });
            this.validityDropDownMenu = new ValidityDropDownMenu();
            this.validityDropDownMenu.setPosition(cc.p(this.width - this.validityDropDownMenu.width / 2 - 30 * scaleFactor - 8 * scaleFactor, this.height / 2));
            this.addChild(this.validityDropDownMenu, 5);

            cc.eventManager.addListener(this.ValidityListener.clone(), this.validityDropDownMenu);

            this.validityDropDown = new ValidityDropDown();
            this.validityDropDown.setAnchorPoint(0, 0);
            this.validityDropDown.setPosition(cc.p(this.width - this.validityDropDown.width - 30 * scaleFactor - 8 * scaleFactor, this.height - 5));
            this.addChild(this.validityDropDown, 5);

        }
        this.createStrip();
    },
    createStrip: function () {

        this._finishBtn = new cc.Sprite(spriteFrameCache.getSpriteFrame("showValidityDis.png"));
        this._finishBtn.setPosition(cc.p(this._finishX, 21 * scaleFactor));
        this._finishBtn.setScale(0.9);
        this.addChild(this._finishBtn, 2);

        this._finishDoneBtn = new cc.Sprite(spriteFrameCache.getSpriteFrame("showValidity.png"));
        this._finishDoneBtn.setPosition(cc.p(this._finishX, 21 * scaleFactor));
        this._finishDoneBtn.setScale(0.9);
        this.addChild(this._finishDoneBtn, 3);
        this._finishDoneBtn.setVisible(false);

        this.leftArrow = new cc.Sprite(spriteFrameCache.getSpriteFrame("ShowValidityButton.png"));
        this.leftArrow.setRotation(0);
        this.leftArrow._isEnabled = true;
        this.leftArrow._name = "leftArrow";
        this.leftArrow.setPosition(cc.p(this.width - this.leftArrow.width / 2, 21 * scaleFactor));
        this.addChild(this.leftArrow, 2);

        this.glow = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("showGlow.png"));
        this.glow.setPosition(cc.p(this._finishX, 21 * scaleFactor));
        this.glow.setScale(1, 0.9);
        this.glow.setVisible(false);
        this.addChild(this.glow, 1, "glow");

        cc.eventManager.addListener(this.ValidityListener.clone(), this.leftArrow);

        if (this.noOfCard == 13) {
            this.score_txt = new cc.LabelTTF("0", "RobotoBold", 24 * 2 * scaleFactor);
            this.score_txt.setPosition(cc.p(33 * scaleFactor, 15 * scaleFactor));
            this.score_txt.setColor(cc.color(255, 255, 255));
            this.score_txt.setScale(0.5);
            this.addChild(this.score_txt, 2);
            this.setShowValidity13();
        }
        else {
            this.setShowValidity();
        }
    },
    startGlow: function (bool) {
        this._finishDoneBtn.setVisible(bool);
        if (bool) {
            this.glow.setVisible(true);
            this.glow.stopAllActions();
            this.glow.setColor(cc.color(199, 222, 47));
            this.glow.runAction(cc.sequence(cc.fadeIn(0.5), cc.fadeOut(0.5)).repeatForever());
        } else {
            this.glow.stopAllActions();
            this.glow.setVisible(false);
        }
    },
    initEventListener: function (element) {
        this.ValidityListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                if (this._isEnabled) {
                    switch (target.getName()) {
                        case "leftArrow":
                            if (this._isOpened) {
                                this._isOpened = false;
                                this.leftArrow.setRotation(0);
                                this._delegate.toggleValidity(false);
                                if (this.validityDropDown && this.validityDropDown._isEnabled) {
                                    this.validityDropDown.hideToggle();
                                }
                            } else {
                                this._isOpened = true;
                                this.leftArrow.setRotation(180);
                                this._delegate.toggleValidity(true);
                            }
                            break;
                        case "validityMenu":
                            this.validityDropDown && this.validityDropDown.hideToggle();
                            // todo
                            break;
                    }
                }
            }.bind(this)
        });
        this.ValidityListener.retain();
    },
    setShowValidity: function () {
        if ((this._gameRoom._myId != -1) && !this._gameRoom._dropped) {
            this.startGlow(false);

            this._toggleBtnArr = [];

            if (this._toggleSprite)
                this._toggleSprite.removeFromParent();

            this._toggleSprite = new cc.Node();
            this.addChild(this._toggleSprite, 5);

            var wordTxt;
            var numOfCircles;
            var wordTxt1;
            var numOfCircles1 = 0;

            if (this._delegate._dropDownSelection == "Doublee") {
                wordTxt = "D";
                numOfCircles = 8;
            }
            else if (this._delegate._dropDownSelection == "Jokers") {
                wordTxt = "J";
                numOfCircles = 8;
            }
            else if (this._delegate._dropDownSelection == "Tunnelas") {
                wordTxt = "T";
                numOfCircles = 3;
            }
            else if (this._delegate._dropDownSelection == "Pure Sequence") {
                wordTxt = "PS";
                numOfCircles = 3;
                wordTxt1 = "S";
                numOfCircles1 = 4;
            }
            var mov;
            var pos = 0;
            var i;
            var name;
            var labelTxt;
            var movX;
            for (i = 0; i < numOfCircles; i++) {
                name = "btn" + (i + 1);
                labelTxt = wordTxt + (i + 1);
                movX = this._startP + (i * (this.dis / (numOfCircles + numOfCircles1)));
                mov = new CircleMenu(name, labelTxt, cc.p(movX, this.height / 2));
                mov.toggle(false);
                this._toggleSprite.addChild(mov, 5);
                this._toggleBtnArr.push(mov);
            }
            for (i = 0; i < numOfCircles1; i++) {
                name = "btn" + (numOfCircles + i + 1);
                labelTxt = wordTxt1 + (i + 1);
                movX = this._startP + ((numOfCircles + i) * (this.dis / (numOfCircles + numOfCircles1)));
                mov = new CircleMenu(name, labelTxt, cc.p(movX, this.height / 2));
                mov.toggle(false);
                this._toggleSprite.addChild(mov, 5);
                this._toggleBtnArr.push(mov);
            }
            this.checkValidity();
        }
    },
    handleDropDownSelection: function (selected) {
        this._delegate._dropDownSelection = selected;
        this.setShowValidity();
        this.validityDropDown.hideToggle();
        this.validityDropDownMenu.label.setString(selected);
    },
    checkValidity: function () {
        var i;
        var mov;
        var c = 0;
        this.startGlow(false);
        switch (this._delegate._dropDownSelection) {
            case "Doublee":
            {
                c = this._gameRoom._userCardPanel._numOfDoublee;
                if (c >= 8) {
                    c = 8;
                    this.startGlow(true);
                }
                break;
            }
            case "Jokers":
            {
                c = this._gameRoom._userCardPanel._numOfJokers;
                if (c >= 8) {
                    c = 8;
                    //trace(this._gameRoom._userCardPanel.checkForJokerGrp());
                    if (this._gameRoom._userCardPanel.checkForJokerGrp())
                        this.startGlow(true);
                    else {
                        //right_arrow.mc.info.visible = true;
                        this.startGlow(false);
                    }
                }
                break;
            }
            case "Tunnelas":
            {
                c = this._gameRoom._userCardPanel._numOfTunnela;
                if (c >= 3) {
                    c = 3;
                    this.startGlow(true);
                }
                break;
            }
            case "Pure Sequence":
            {
                if ((this._gameRoom._userCardPanel._numOfPureSeq + this._gameRoom._userCardPanel._numOfTunnela) >= 3)
                    c = this._gameRoom._userCardPanel._numOfPureSeq + this._gameRoom._userCardPanel._numOfTunnela + this._gameRoom._userCardPanel._numOfSeq;
                else
                    c = this._gameRoom._userCardPanel._numOfPureSeq + this._gameRoom._userCardPanel._numOfTunnela;
                if ((c >= 3) && (this._gameRoom._userCardPanel._numOfInvalid == 0) && (this._gameRoom._userCardPanel._numOfDoublee == 0)) {
                    if (this._gameRoom._userCardPanel.getLastCard()) {
                        if (this._gameRoom._userCardPanel.getNumOfUnGroupCards() <= 1) {
                            this.startGlow(true);
                            c = 7;
                        }
                    }
                    else {
                        if (this._gameRoom._userCardPanel.getNumOfUnGroupCards() == 0) {
                            this.startGlow(true);
                            c = 7;
                        }
                    }
                }
                break;
            }
        }

        for (var j = 0; j < this._toggleBtnArr.length; j++) {
            mov = this._toggleBtnArr[j];//this.getChildByName("btn" + i);
            if (j < c)
                mov && mov.toggle(true);
            else
                mov && mov.toggle(false);
        }
        /*for (i = 0; i < c; i++) {
         if (this._toggleSprite) {
         mov = this._toggleBtnArr[i];//this._toggleSprite.getChildByName("btn" + i);
         if (mov)
         mov.toggle(true);
         }
         }*/
    },
    setShowValidity13: function () {
        if ((this._gameRoom._myId != -1) && !this._gameRoom._dropped) {
            this.startGlow(false);
            this._toggleBtnArr = [];

            if (this._toggleSprite)
                this._toggleSprite.removeFromParent();

            this._toggleSprite = new cc.Node();
            this.addChild(this._toggleSprite, 5);

            //initListeners();
            var wordTxt;
            var numOfCircles = 0;
            var wordTxt1;
            var numOfCircles1 = 0;

            wordTxt = "PS";
            numOfCircles = 1;
            wordTxt1 = "S";
            numOfCircles1 = 3;

            var mov;
            var pos = 0;

            var i;
            var name;
            var labelTxt;
            var movX;
            for (i = 0; i < numOfCircles; i++) {
                name = "btn" + (i + 1);
                labelTxt = wordTxt + (i + 1);
                movX = this._startP + (i * (this.dis / (numOfCircles + numOfCircles1)));
                mov = new CircleMenu(name, labelTxt, cc.p(movX, this.height / 2));
                mov.toggle(false);
                this._toggleSprite.addChild(mov, 5);
                this._toggleBtnArr.push(mov);
            }
            for (i = 0; i < numOfCircles1; i++) {
                name = "btn" + (numOfCircles + i + 1);
                labelTxt = wordTxt1 + (i + 1);
                movX = this._startP + ((numOfCircles + i) * (this.dis / (numOfCircles + numOfCircles1)));
                mov = new CircleMenu(name, labelTxt, cc.p(movX, this.height / 2));
                mov.toggle(false);
                this._toggleSprite.addChild(mov, 5);
                this._toggleBtnArr.push(mov);
            }
            this.checkValidity13();
        }
    },
    checkValidity13: function () {
        var i;
        var mov;
        var c = 0;
        this.startGlow(false);
        c = 0;
        if (((this._gameRoom._userCardPanel._numOfPureSeq == 1) && (this._gameRoom._userCardPanel._numOfImpSeq >= 1)) || (this._gameRoom._userCardPanel._numOfPureSeq > 1))
            c = this._gameRoom._userCardPanel._numOfPureSeq + this._gameRoom._userCardPanel._numOfSeq;
        else
            c = this._gameRoom._userCardPanel._numOfPureSeq;
        if ((c > 1) && (this._gameRoom._userCardPanel._numOfInvalid == 0)) {
            if (this._gameRoom._userCardPanel.getLastCard()) {
                if (this._gameRoom._userCardPanel.getNumOfUnGroupCards() <= 1) {
                    this.startGlow(true);
                    c = 4;
                }
            }
            else {
                if (this._gameRoom._userCardPanel.getNumOfUnGroupCards() == 0) {
                    this.startGlow(true);
                    c = 4;
                }
            }
        }

        for (var j = 0; j < this._toggleBtnArr.length; j++) {
            mov = this._toggleBtnArr[j];//this.getChildByName("btn" + i);
            if (j < c)
                mov && mov.toggle(true);
            else
                mov && mov.toggle(false);
        }

        /*for (i = 0; i < c; i++) {
         if (this._toggleSprite) {
         mov = this._toggleBtnArr[i];//this.getChildByName("btn" + i);
         if (mov)
         mov.toggle(true);
         }
         }*/
        if (false)
            this.setScore("0");
        else
            this._gameRoom._userCardPanel.calculateScore();
    },
    setScore: function (score) {
        if (typeof score == "number" || typeof score == "string")
            this.score_txt.setString(score + "");
    }
});
var CircleMenu = cc.Sprite.extend({
    _isEnabled: false,
    _toggle: false,
    ctor: function (_name, labeltxt, pos) {
        this._super(spriteFrameCache.getSpriteFrame("ValidToggleBtn.png"));

        this.setName(_name);
        this.setPosition(pos);

        this.label = new cc.LabelTTF(labeltxt, "RobotoBold", 11 * 2 * scaleFactor);
        this.label.setScale(0.5);
        this.label._setBoundingWidth(this.width * 2 * scaleFactor);
        this.label.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.label.setAnchorPoint(cc.p(0, 0.5));
        this.label.setColor(cc.color(0, 0, 0));
        this.label.setPosition(cc.p(0, this.height / 2 - yMargin));
        this.addChild(this.label, 2);

        return true;
    },
    toggle: function (bool) {
        // this._toggle = !this._toggle;
        if (bool) {
            this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("ValidToggleBtnDone.png"));
        }
        else {
            this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("ValidToggleBtn.png"));
        }
    }
});

var ValidityDropDown = cc.Scale9Sprite.extend({
    _isEnabled: null,
    _listener: null,
    ctor: function () {
        this._super(spriteFrameCache.getSpriteFrame("whiteBoxPopUp.png"), cc.rect(0, 0, 50 * scaleFactor, 50 * scaleFactor));
        this.setCapInsets(cc.rect(13 * scaleFactor, 13 * scaleFactor, 13 * scaleFactor, 13 * scaleFactor));
        this.setContentSize(cc.size(110 * scaleFactor, 90 * scaleFactor));

        this._isEnabled = false;
        this.setVisible(false);

        this.initEventListener();

        var buttons = ["Tunnelas", "Doublees", "Jokers", "Pure Sequence"];
        var y = this.height - 3 * scaleFactor;

        for (var a = 0; a < buttons.length; a++) {
            var list = this.getList(buttons[a]);
            y -= list.height + 1 * scaleFactor;
            list.setPosition(cc.p(1 * scaleFactor, y));
            this.addChild(list, 1);

            cc.eventManager.addListener(this._listener.clone(), list);
        }

        return true;
    },
    hideToggle: function () {
        this._isEnabled = !this._isEnabled;
        this.setVisible(!this.isVisible());
    },
    getList: function (name) {
        var List = ccui.Layout.extend({
            _name: null,
            ctor: function (name) {
                this._super();
                this.setContentSize(cc.size(108 * scaleFactor, 20 * scaleFactor));

                this._name = name;

                this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
                this.setColor(cc.color(230, 230, 230));
                this.setCascadeColorEnabled(false);
                this.setCascadeOpacityEnabled(false);

                var label = new cc.LabelTTF(name, "RobotoBold", 11 * 2 * scaleFactor);
                label.setColor(cc.color(0, 0, 0));
                label.setScale(0.5);
                label.setPosition(cc.p(this.width / 2, this.height / 2));
                this.addChild(label, 1);
            }
        });
        return new List(name);
    },
    initEventListener: function () {
        this._listener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    if (this._isEnabled) {
                        switch (target._name) {
                            case "Tunnelas":
                                this.parent.handleDropDownSelection("Tunnelas");
                                break;
                            case "Doublees":
                                this.parent.handleDropDownSelection("Doublee");
                                break;
                            case "Jokers":
                                this.parent.handleDropDownSelection("Jokers");
                                break;
                            case "Pure Sequence" :
                                this.parent.handleDropDownSelection("Pure Sequence");
                                break;
                        }
                    }
                    return true;
                }
                return false;

            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();

            }.bind(this)
        });
        this._listener.retain();
    }
});