/**
 * Created by stpl on 9/27/2016.
 */
"use strict";
var HowToPlayDropDown = ccui.Layout.extend({

    blockSize: null,
    _name: "HowToPlay",
    _controNdisplay: null,

    ctor: function (tabHeight, context) {

        this._super();
        this._controNdisplay = context;
        this.blockSize = {width: 150 * scaleFactor, height: 30 * scaleFactor};
        this.listLength = 0;
        this.listContents = 7;
        var fontSize = 11 * scaleFactor, fontFamily = "RobotoRegular", tabSpace = 1 * scaleFactor;

        this.setAnchorPoint(0, 1);
        this.setContentSize(cc.size(this.blockSize.width, this.blockSize.height * this.listContents + (tabSpace * 6)));

        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.setBackGroundColor(cc.color(81, 81, 81));

        this.rightLine = new cc.DrawNode();
        this.rightLine.drawSegment(cc.p(this.width, 1) * scaleFactor, cc.p(this.width, this.height - 1 * scaleFactor), 1, cc.color(81, 81, 81));
        this.addChild(this.rightLine);


        this.listArrayString = ["13 Cards Game Demo", "How to pick card?", "How to discard?",
            "How to group cards?", "How to show?", "21 Cards Rules", "21 Cards Tips"];
        this.listArrayString.reverse();
        this.listClassArray = [];

        for (var i = 0; i < this.listArrayString.length; i++) {
            this.listClassArray.push(this.getLabelBlock(this.listArrayString[i], fontSize, fontFamily));
            this.addChild(this.listClassArray[i]);
        }

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        return true;
    },
    getLabelBlock: function (string, fontSize, fontFamily) {

        var label = new cc.LabelTTF(string, fontFamily, fontSize * 2);
        label.setScale(0.5);
        label.setTag(1);
        label.setPosition(cc.p(this.blockSize.width / 2, this.blockSize.height / 2 - yMargin));

        var tabDistance = 0;

        if (this.listLength != 0) {
            tabDistance = 1 * scaleFactor;
        } else tabDistance = 0;

        var labelBg = new ccui.Layout();
        labelBg.setName(string);
        labelBg.setAnchorPoint(0, 0);
        labelBg.setBackGroundColorType(ccui.Layout.BG_COLOR_GRADIENT);
        labelBg.setContentSize(cc.size(this.blockSize.width, this.blockSize.height));
        labelBg.setPosition(cc.p(0, (this.blockSize.height * this.listLength) + (tabDistance * this.listLength)));
        labelBg.setBackGroundColor(cc.color(51, 51, 51), cc.color(51, 51, 51));

        labelBg.addChild(label);
        labelBg.hover = function (bool) {
            bool ? label.setColor(cc.color(255, 176, 14)) : label.setColor(cc.color(255, 255, 255));
            bool ? labelBg.setBackGroundColor(cc.color(95, 95, 95), cc.color(65, 65, 65)) : labelBg.setBackGroundColor(cc.color(51, 51, 51), cc.color(51, 51, 51));
        };

        this.listLength++;

        return labelBg;
    },
    show21CardRules: function () {
        this._controNdisplay.show21CardRules();
    },
    show21CardTips: function () {
        this._controNdisplay.show21CardTips();
    },
    initMouseListener: function () {
        this.howToPlayMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            swallowTouches: true,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    !target._hovering && target.hover(true);
                    target._hovering = true;
                    return true;
                }
                target._hovering && target.hover(false);
                target._hovering = false;
                return false;
            }
        });
        this.howToPlayMouseListener.retain();
        for (var i = 0; i < this.listClassArray.length; i++) {
            cc.eventManager.addListener(this.howToPlayMouseListener.clone(), this.listClassArray[i]);
        }
    },
    initTouchListener: function () {
        this.howToPlayTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                this._controNdisplay.closeAllTab();
                var demo;
                switch (target._name) {
                    case "13 Cards Game Demo" :
                        demo = new CardGameDemo_13();
                        this._controNdisplay.addChild(demo, 5);
                        break;

                    case "How to pick card?":
                        demo = new PickDemo();
                        this._controNdisplay.addChild(demo, 5);
                        break;

                    case "How to discard?":
                        demo = new DiscardDemo();
                        this._controNdisplay.addChild(demo, 5);
                        break;

                    case "How to group cards?":
                        demo = new GroupCardDemo();
                        this._controNdisplay.addChild(demo, 5);
                        break;

                    case "How to show?":
                        demo = new ShowDemo();
                        this._controNdisplay.addChild(demo, 5);
                        break;

                    case "21 Cards Rules":
                        this.show21CardRules();
                        break;

                    case "21 Cards Tips":
                        this.show21CardTips();
                        break;

                }

            }.bind(this)
        });
        this.howToPlayTouchListener.retain();
        for (var i = 0; i < this.listClassArray.length; i++) {
            cc.eventManager.addListener(this.howToPlayTouchListener.clone(), this.listClassArray[i]);
        }
    }
});
var HelpDropDown = ccui.Layout.extend({
    blockSize: null,
    _controNdisplay: null,
    _name: "Help",
    ctor: function (tabHeight, context) {

        this._super();
        this._controNdisplay = context;
        this.blockSize = {width: 150 * scaleFactor, height: 30 * scaleFactor};
        this.listLength = 0;
        this.listContents = 4;
        var fontSize = 11 * scaleFactor, fontFamily = "RobotoRegular", tabSpace = 1 * scaleFactor;

        this.setAnchorPoint(0, 1);
        this.setContentSize(cc.size(this.blockSize.width, this.blockSize.height * this.listContents + (tabSpace * 3)));

        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.setBackGroundColor(cc.color(81, 81, 81));


        this.rightLine = new cc.DrawNode();
        this.rightLine.drawSegment(cc.p(this.width, 1 * scaleFactor), cc.p(this.width, this.height - 1 * scaleFactor), 1, cc.color(81, 81, 81));
        this.addChild(this.rightLine);

        this.leftLine = new cc.DrawNode();
        this.leftLine.drawSegment(cc.p(0, 0), cc.p(0, this.height), 1, cc.color(81, 81, 81));
        this.addChild(this.leftLine);

        this.bottomLine = new cc.DrawNode();
        this.bottomLine.drawSegment(cc.p(0, 0), cc.p(this.width, 0), 1, cc.color(81, 81, 81));
        this.addChild(this.bottomLine);

        this.listArrayString = ["Contact Us", "Rummy FAQ's", "Rules", "Support Chat"];
        this.listClassArray = [];

        for (var i = 0; i < this.listArrayString.length; i++) {
            this.listClassArray.push(this.getLabelBlock(this.listArrayString[i], fontSize, fontFamily));
            this.addChild(this.listClassArray[i]);
        }

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        return true;
    },
    showRulesDropDown: function (target) {
        if (this.RulesDropDown == null || this.RulesDropDown._isOn == false) {
            target.setBackGroundColor(cc.color(50, 50, 50));
            this.RulesDropDown = new RulesDropDown(this.tabHeight, this);
            this.RulesDropDown.setPosition(cc.p(150 * scaleFactor, 93 * scaleFactor));
            this.RulesDropDown._isOn = true;
            this.addChild(this.RulesDropDown);
        } else {
            this.RulesDropDown.removeFromParent(true);
            this.RulesDropDown._isOn = false;
        }
    },
    initMouseListener: function () {
        this.helpMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            swallowTouches: true,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    !target._hovering && target.hover(true);
                    target._hovering = true;
                    return true;
                }
                target._hovering && target.hover(false);
                target._hovering = false;
                return false;
            }
        });
        this.helpMouseListener.retain();
        for (var i = 0; i < this.listClassArray.length; i++) {
            cc.eventManager.addListener(this.helpMouseListener.clone(), this.listClassArray[i]);
        }
    },
    initTouchListener: function () {
        this.helpTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                switch (target.getChildByTag(1)._name) {
                    case "Contact Us" :
                        this._controNdisplay.closeAllTab();
                        cc.sys.openURL(applicationFacade.baseUrl +"/contact-us");
                        break;

                    case "Rummy FAQ's":
                        this._controNdisplay.closeAllTab();
                        cc.sys.openURL(applicationFacade.baseUrl +"/rummy-faq");
                        break;

                    case "Rules":
                        this.showRulesDropDown(target);
                        break;

                    case "Support Chat":
                        this._controNdisplay.closeAllTab();
                        window.parent.postMessage("openChatWindow","*");
                        break;
                }
                applicationFacade.updateExternalLobbyBalance();
            }.bind(this)
        });

        this.helpTouchListener.retain();
        for (var i = 0; i < this.listClassArray.length; i++) {
            cc.eventManager.addListener(this.helpTouchListener.clone(), this.listClassArray[i]);
        }
    },
    getLabelBlock: function (string, fontSize, fontFamily) {

        var label = new cc.LabelTTF(string, fontFamily, fontSize * 2);
        label.setScale(0.5);
        label.setPosition(cc.p(this.blockSize.width / 2, this.blockSize.height / 2 - yMargin));
        label.setTag(1);
        label.setName(string);

        var labelBg = new ccui.Layout();

        var tabDistance = 0;
        if (this.listLength != 0) {
            tabDistance = 1 * scaleFactor;
        } else tabDistance = 0;

        labelBg.setAnchorPoint(0, 0);
        labelBg.setBackGroundColorType(ccui.Layout.BG_COLOR_GRADIENT);
        labelBg.setContentSize(cc.size(this.blockSize.width, this.blockSize.height));
        labelBg.setPosition(cc.p(0, (this.blockSize.height * this.listLength) + (tabDistance * this.listLength)));
        labelBg.setBackGroundColor(cc.color(51, 51, 51), cc.color(51, 51, 51));
        labelBg.addChild(label);

        labelBg.hover = function (bool) {
            bool ? label.setColor(cc.color(255, 176, 14)) : label.setColor(cc.color(255, 255, 255));
            bool ? labelBg.setBackGroundColor(cc.color(95, 95, 95), cc.color(65, 65, 65)) : labelBg.setBackGroundColor(cc.color(51, 51, 51), cc.color(51, 51, 51));
        };

        if (string == "Rules") {
            this.arrow = new cc.Sprite(spriteFrameCache.getSpriteFrame("arrow.png"));
            this.arrow.setScale(0.3);
            this.arrow.setTag(5);
            this.arrow.setAnchorPoint(0.5, 0.5);
            this.arrow.setPosition(cc.p(labelBg.width - 10 * scaleFactor, labelBg.height / 2));
            this.arrow.setRotation(180);
            labelBg.addChild(this.arrow, 2);
        }

        this.listLength++;

        return labelBg;
    }
});

var MenuLayout = ccui.Layout.extend({
    _hovering: null,
    _isEnabled: null,
    _isSelected: null,
    ctor: function (string, tabHeight, tabMargin, fontSize, fontFamily, layoutColor) {
        this._super();

        this._hovering = false;

        var label = new cc.LabelTTF(string, "RobotoRegular", fontSize * 2);
        label.setName(string.replace(/ /g, "") + "Label");
        label.setColor(cc.color(207, 207, 207));
        label.setScale(0.5);
        label.setTag(1);
        this.labelTxt = label;
        this.setName(string.replace(/ /g, ""));
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.setContentSize(cc.size(label.width / 2 + tabMargin, tabHeight));
        this.setBackGroundColor(cc.color(0, 0, 0));

        this._isEnabled = true;
        label.setPosition(cc.p(this.width / 2, this.height / 2 - yMargin));


        if (string == "Inbox") {
            this.mailIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("mailIcon.png"));
            this.mailIcon.setAnchorPoint(0.5, 0.5);
            this.mailIcon.setTag(2);
            this.setBackGroundColor(cc.color(0, 0, 0));
            this.setContentSize(cc.size(label.width / 2 + tabMargin + 20 * scaleFactor, tabHeight));
            label.setPosition(cc.p(this.width / 2 + 10 * scaleFactor, this.height / 2 - yMargin));
            this.mailIcon.setPosition(cc.p(this.width / 2 - 20 * scaleFactor, this.height / 2 - yMargin));
            this.addChild(this.mailIcon);
        }

        this.addChild(label);

    },
    hide: function (bool) {
        this._isEnabled = !bool;
        this.setVisible(!bool);
    },
    setSelected: function (bool) {
        if (bool) {
            this.setBackGroundColor(cc.color(50, 50, 50));
            this.labelTxt.setColor(cc.color(255, 176, 14));
        } else {
            this.setBackGroundColor(cc.color(0, 0, 0));
            this.labelTxt.setColor(cc.color(255, 255, 255));
        }
        this._isSelected = bool;
    }
});
var LobbyTab = cc.Sprite.extend({
    _roomId: null,
    _isDisabled: null,
    _name: "lobbyTab",
    _hovering: null,
    ctor: function () {
        this._super(spriteFrameCache.getSpriteFrame("lobbyButton.png"));
        this._isDisabled = !1;
        this.setName('lobbyTab');
        this._hovering = false;
        this._roomId = 0;
        this.setPosition(cc.p(this.width / 2, this.height / 2));
    },
    hover: function (bool) {
        if (this._isDisabled == !1) {
            bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("lobbyButton.png")) : this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("lobbyButton.png"));
        }
    },
    setDisabled: function (bool) {
        bool && (this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("lobbyButtonDisabled.png")), this._isDisabled = !0) || (this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("lobbyButton.png")), this._isDisabled = !1);
    },
    isDisabled: function () {
        return this._isDisabled;
    },
    hide: function (bool) {
        this.setVisible(!bool);
        this._isDisabled = bool;
    }
});

var RulesDropDown = ccui.Layout.extend({
    blockSize: null,
    _controNdisplay: null,
    _name: "Rules",
    ctor: function (tabHeight, context) {

        this._super();
        this._controNdisplay = context._controNdisplay;
        this.blockSize = {width: 150 * scaleFactor, height: 30 * scaleFactor};
        this.listLength = 0;
        this.listContents = 3;
        var fontSize = 11 * scaleFactor, fontFamily = "RobotoRegular", tabSpace = 1 * scaleFactor;

        this.setAnchorPoint(0, 1);
        this.setContentSize(cc.size(this.blockSize.width, this.blockSize.height * this.listContents + (tabSpace * 3)));

        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.setBackGroundColor(cc.color(81, 81, 81));

        this.rightLine = new cc.DrawNode();
        this.rightLine.drawSegment(cc.p(this.width, 0), cc.p(this.width, this.height), 1, cc.color(81, 81, 81));
        this.addChild(this.rightLine);

        this.bottomLine = new cc.DrawNode();
        this.bottomLine.drawSegment(cc.p(0, 0), cc.p(this.width, 0), 1, cc.color(81, 81, 81));
        this.addChild(this.bottomLine);


        this.listArrayString = ["Point Rummy", "Pool Rummy", "21 Card Rummy"]; //List of strings which will appear as part of Rules DropDown
        this.listClassArray = [];

        for (var i = 0; i < this.listArrayString.length; i++) {
            this.listClassArray.push(this.getLabelBlock(this.listArrayString[i], fontSize, fontFamily));
            this.addChild(this.listClassArray[i]);  //Adding the game names to the dropdown
        }

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        return true;
    },
    initTouchListener: function () {
        this.rulesTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                this._controNdisplay.closeAllTab();
                /*Shubham*/
                if (target.getChildren()[0]._originalText == "Rules")
                    this.showRulesDropDown(target);

                if (target.getChildren()[0]._originalText.toLowerCase() == "21 card rummy")
                    cc.sys.openURL(applicationFacade.baseUrl +"/21-cards-rummy-rules");

                if (target.getChildren()[0]._originalText == "Pool Rummy")
                    cc.sys.openURL(applicationFacade.baseUrl +"/pool-rummy");

                if (target.getChildren()[0]._originalText == "Point Rummy")
                    cc.sys.openURL(applicationFacade.baseUrl +"/point-rummy");

                return true;

            }.bind(this),
        });

        this.rulesTouchListener.retain();
        for (var i = 0; i < this.listClassArray.length; i++) {
            cc.eventManager.addListener(this.rulesTouchListener.clone(), this.listClassArray[i]);
        }
    },
    initMouseListener: function () {
        this.RulesListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            swallowTouches: true,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    !target._hovering && target.hover(true);
                    target._hovering = true;
                    return true;
                }
                target._hovering && target.hover(false);
                target._hovering = false;
                return false;
            }.bind(this)
        });
        this.RulesListener.retain();
        for (var i = 0; i < this.listClassArray.length; i++) {
            cc.eventManager.addListener(this.RulesListener.clone(), this.listClassArray[i]);
        }
    },
    getLabelBlock: function (string, fontSize, fontFamily) {

        var label = new cc.LabelTTF(string, fontFamily, fontSize * 2);
        label.setScale(0.5);
        label.setPosition(cc.p(this.blockSize.width / 2, this.blockSize.height / 2 - yMargin));

        var tabDistance = 0;

        if (this.listLength != 0) {
            tabDistance = 1 * scaleFactor;
        } else tabDistance = 0;

        var labelBg = new ccui.Layout();
        labelBg.setAnchorPoint(0, 0);
        labelBg.setBackGroundColorType(ccui.Layout.BG_COLOR_GRADIENT);
        labelBg.setContentSize(cc.size(this.blockSize.width, this.blockSize.height));
        labelBg.setPosition(cc.p(0, (this.blockSize.height * this.listLength) + (tabDistance * this.listLength)));
        labelBg.setBackGroundColor(cc.color(51, 51, 51), cc.color(51, 51, 51));
        labelBg.addChild(label);

        labelBg.hover = function (bool) {
            bool ? label.setColor(cc.color(255, 176, 14)) : label.setColor(cc.color(255, 255, 255));
            bool ? labelBg.setBackGroundColor(cc.color(95, 95, 95), cc.color(65, 65, 65)) : labelBg.setBackGroundColor(cc.color(51, 51, 51), cc.color(51, 51, 51));
        };

        this.listLength++;

        return labelBg;
    }
});