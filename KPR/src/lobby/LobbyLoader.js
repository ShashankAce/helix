/**
 * Created by stpl on 4/24/2017.
 */
var LobbyLoader = cc.Node.extend({
    ctor: function () {
        this._super();

        this.setPosition(cc.p(375 * scaleFactor, 200 * scaleFactor));

        var message = new cc.LabelTTF("Please wait while opening the tables", "RobotoRegular", 15 * 2);
        message.setScale(0.7);
        message.y = 20 * scaleFactor;
        message.setColor(cc.color(69, 69, 69));
        this.addChild(message, 1);

        var clipper = new ccui.Layout();
        clipper.setContentSize(cc.size(300 * scaleFactor, 15 * scaleFactor));
        clipper.setPosition(cc.p(-clipper.width / 2, -20 * scaleFactor));
        clipper.setClippingEnabled(true);
        this.addChild(clipper, 1);

        var loaderBase = new cc.Sprite(spriteFrameCache.getSpriteFrame("loaderBarbase.png"));
        loaderBase.setPosition(cc.p(clipper.width / 2, clipper.height / 2));
        clipper.addChild(loaderBase, 1);

        loaderBase.runAction(cc.sequence(cc.moveBy(0.5, 29, 0), cc.callFunc(function () {
            loaderBase.setPosition(cc.p(clipper.width / 2, clipper.height / 2));
        }, clipper)).repeatForever());

    }
});