var TournamentDetailStatusSymbol = StatusSymbol.extend({
    _name: "statusSym",
    _currentState: null,
    _parentClass: null,
    status: null,
    ctor: function (context, pos) {
        this._super(context, pos);
        this._parentClass = context;
        cc.eventManager.addListener(this._parentClass.popupMouseListener.clone(), this.statusBtn);
        cc.eventManager.addListener(this._parentClass.popupTouchListener.clone(), this.statusBtn);

        return true;
    },
    setState: function (room, isMyTournament, buttonType) {
        var status = room.getVariable("status").value;
        var regNum = room.getVariable("regNum").value;
        var maxRegNum = room.getVariable("maxRegNum").value;
        var regStartTime = room.getVariable("regStartTime").value;
        regStartTime = moment(regStartTime).format('DD MMM,hh:mm a');

        var currentTimeInSec = (new Date).getTime();
        var currentDateAndTime = moment(currentTimeInSec).format('DD MMM,hh:mm a');

        var startTimeArr = regStartTime.split(",");
        var currentTimeArr = currentDateAndTime.split(",");

        var time = "";
        if (startTimeArr[0] == currentTimeArr[0]) {
            var day = "Today";
            time += startTimeArr[1] + " " + day;
        } else {
            time = startTimeArr[0] + " " + startTimeArr[1];
        }
        // this.removeAllChildren(true);

        var flag = false, enable = false;
        var spriteName = null, spriteOverName = null, buttonName = null;

        switch (status.toUpperCase().toString()) {
            case TOURNAMENT_CONSTANTS.REGISTERING :
                if (isMyTournament) {
                    flag = true;
                    enable = true;
                    if (buttonType) {
                        spriteName = "quitDetail";
                        spriteOverName = "quitDetailover";
                    } else {
                        spriteName = "quit";
                        spriteOverName = "quit";
                    }
                    buttonName = "Quit_Enable";
                } else {
                    flag = true;
                    enable = true;
                    if (buttonType) {
                        spriteName = "JoinNow";
                        spriteOverName = "JoinNowOver";
                    } else {
                        spriteName = "detailJoin";
                        spriteOverName = "detailJoinHover";
                    }
                    buttonName = "Join_Enable";
                }
                break;
            case TOURNAMENT_CONSTANTS.OPEN :
                flag = true;
                enable = false;
                if (buttonType) {
                    spriteName = "JoinNowDisable";
                    spriteOverName = "";
                    this.statusBtn._regStartTime = time;
                } else {
                    spriteName = "detailJoinDisable";
                    spriteOverName = "";
                }
                buttonName = "Join_Disable";
                break;
            case TOURNAMENT_CONSTANTS.RUNNING :
                if (isMyTournament) {
                    if (AllTournamentRoomListStrip.ALREADY_JOIN_ROOM.indexOf(String(room.id)) != -1) {
                        flag = true;
                        enable = true;
                        if (buttonType) {
                            spriteName = "takeseatDetail";
                            spriteOverName = "takeseatDetailover";
                        } else {
                            spriteName = "takeseatbig";
                            spriteOverName = "takeseathoverbig";
                        }
                        buttonName = "TakeSeat_Enable";
                    } else {
                        flag = false;
                    }
                } else {
                    flag = false;
                }
                break;
            case TOURNAMENT_CONSTANTS.FREEZE :
                if (isMyTournament) {
                    if (AllTournamentRoomListStrip.ALREADY_JOIN_ROOM.indexOf(String(room.id)) != -1) {
                        flag = true;
                        enable = true;
                        if (buttonType) {
                            spriteName = "takeseatDetailDisable";
                            spriteOverName = "takeseatDetailDisable";
                        } else {
                            spriteName = "takeseatdisablebig";
                            spriteOverName = "takeseatdisablebig";
                        }
                        buttonName = "TakeSeat_Disable";
                    } else {
                        flag = false;
                    }
                } else {
                    flag = false;
                }
                break;
            case TOURNAMENT_CONSTANTS.COMPLETED :
            case TOURNAMENT_CONSTANTS.CANCELLED :
            default :
                flag = false;
        }

        if (buttonType)
            this.statusText.setColor(cc.color(0, 0, 0));
        else  this.statusText.setColor(cc.color(166, 189, 0));
        if (flag) {
            if (status.toLowerCase() == "registering" && regNum == maxRegNum) {
                if (isMyTournament) {
                    if (buttonType) {
                        spriteName = "quitDetail";
                        spriteOverName = "quitDetailover";
                    } else {
                        spriteName = "quit";
                        spriteOverName = "quit";
                    }
                    buttonName = "Quit_Enable";
                    this.status = "WAITING";
                } else {
                    if (buttonType) {
                        spriteName = "waitlistDetail";
                        spriteOverName = "waitlistDetailover";
                    } else {
                        spriteName = "waitbig";
                        spriteOverName = "waitbighover";
                    }
                    buttonName = "Waiting_Enable";
                    this.status = "WAITING";
                }
            } else {
                this.status = "JOIN";
            }
            this.statusBtn.hide(false);
            this.statusText.setString("");
            this.statusBtn.setSprite(spriteName, spriteOverName);
            this.statusBtn.setName(buttonName.split("_")[0]);
        } else {
            this.statusBtn.hide(true);
            this.statusText.setString(status);
        }

        if (enable) {
            this.statusBtn._isEnabled = true;
        } else {
            this.statusBtn._isEnabled = false;
        }
    }
});

