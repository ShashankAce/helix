/**
 * Created by Shashank on 6/7/2016.
 */
"use strict";
var GameConstants = {};

GameConstants.cardsPositionY = 235;
GameConstants.cardsDefaultPosY = GameConstants.cardsPositionY;

// zOrders
GameConstants.playerZorder = 2;
GameConstants.notificationZorder = 5;
GameConstants.tossZorder = 9;
GameConstants.deckPanelZorder = 10;
GameConstants.jokerToolTipZorder = 11;
GameConstants.cutDeckPanelZorder = 12;
GameConstants.historyPanelZorder = 20;
GameConstants.cardMinZorder = 40;
GameConstants.meldConfirmZorder = 45;
GameConstants.userCardPanelZorder = 46;
GameConstants.tNoficZorder = 47;
GameConstants.autoPlayZorder = 48;
GameConstants.confirmPopupZorder = 49;
GameConstants.gameResultZorder = 50;
GameConstants.discardPanelZorder = 51;
GameConstants.settingZorder = 52;
GameConstants.splitZorder = 53;
GameConstants.winnerScreenZorder = 55;
GameConstants.tournamentDetailZorder = 60;
GameConstants.tourPopupZorder = 65;
GameConstants.popupZorder = 80;
GameConstants.noBalPop = 85;
GameConstants.betConfirmationpopupZorder = 85;
GameConstants.tieBreakerZorder = 86;
GameConstants.leaveZorder = 90;
GameConstants.headerZorder = 95;


GameConstants.openDeckCardScaleValue = 0.87;

GameConstants.extraCardSpace = 20;
GameConstants.cardSpace = 32;
GameConstants.cardWidth = 79;
GameConstants.cardHeight = 110;
GameConstants.cardWidth21 = 61;
GameConstants.cardHeight21 = 85.8;
GameConstants.meldCardScaleFactor = 1;
GameConstants.resultCardScaleFactor = 0.5;
GameConstants.cardHalf = (GameConstants.cardWidth / 2) * 0.5;
GameConstants.cardFrameHeight = 205;

GameConstants.popButtonMargin = 40;

GameConstants.unGroupCardPosRect = {
    x: GAME_SIZE.width - 120,
    y: 0,
    width: 120,
    height: 120
};
GameConstants.closedCardPos_13 = {
    2: [cc.p(565, 500),
        cc.p(565, 140)],

    4: [cc.p(870, 364),
        cc.p(565, 500),
        cc.p(138, 364),
        cc.p(565, 140)],

    6: [cc.p(870, 364),
        cc.p(795, 500),
        cc.p(565, 505),
        cc.p(326, 505),
        cc.p(138, 364),
        cc.p(565, 140)]
};