/**
 * Created by stpl on 1/30/2017.
 */
var Notification = cc.Layer.extend({
    _gameRoom: null,
    ctor: function (context, width, height) {
        this._super();
        this._gameRoom = context;

        var layout = cc.LayerGradient.extend({
            ctor: function (context) {
                this._super(cc.color(0, 0, 0), cc.color(0, 0, 0));
                var size = cc.winSize;
                this.setContentSize(cc.size(width, height));
                this.setPosition(cc.p((cc.winSize.width - width) / 2, (cc.winSize.height - height) / 2));
                this.setColorStops([{p: 0, color: new cc.Color(0, 0, 0, 0)},
                    {p: .5, color: cc.color.BLACK},
                    {p: 1, color: new cc.Color(0, 0, 0, 0)}]);
                this.setVector(cc.p(-1, 0));
            }
        });
        var gradient = new layout(this);
        this.addChild(gradient, 1);

    }
});
var StartPopup = Notification.extend({
    _name: "StartPopup",
    ctor: function (context) {
        this._super(context, 700, 150);

        this.tmntNameTF = new cc.LabelTTF("Tournament", "RobotoBold", 16 * 2 * scaleFactor);
        this.tmntNameTF.setScale(0.5);
        this.tmntNameTF.setColor(cc.color(255, 255, 255));
        this.tmntNameTF.setPosition(cc.p(this.width / 2, this.height / 2 + 55 * scaleFactor));
        this.addChild(this.tmntNameTF, 5);

        this.rndLvlTF = new cc.LabelTTF("Tournament", "RobotoRegular", 14 * 2 * scaleFactor);
        this.rndLvlTF.setScale(0.5);
        this.rndLvlTF.setColor(cc.color(163, 163, 163));
        this.rndLvlTF.setPosition(cc.p(this.width / 2, this.height / 2 + 20 * scaleFactor));
        this.addChild(this.rndLvlTF, 5);

        this.pntDealTF = new cc.LabelTTF("Tournament", "RobotoRegular", 14 * 2 * scaleFactor);
        this.pntDealTF.setScale(0.5);
        this.pntDealTF.setColor(cc.color(163, 163, 163));
        this.pntDealTF.setPosition(cc.p(this.width / 2, this.height / 2 - 10 * scaleFactor));
        this.addChild(this.pntDealTF, 5);

        var gameStrtTF = new cc.LabelTTF("Game Starts in -", "RobotoRegular", 14 * 2 * scaleFactor);
        gameStrtTF.setScale(0.5);
        gameStrtTF.setAnchorPoint(1, 0.5);
        gameStrtTF.setColor(cc.color(163, 163, 163));
        gameStrtTF.setPosition(cc.p(this.width / 2 + gameStrtTF.width / 6, this.height / 2 - 45 * scaleFactor));
        this.addChild(gameStrtTF, 5);

        this.timer_txt = new cc.LabelTTF("", "RobotoRegular", 14 * 2 * scaleFactor);
        this.timer_txt.setScale(0.5);
        this.timer_txt.setAnchorPoint(0, 0.5);
        this.timer_txt.setColor(cc.color(195, 218, 33));
        this.timer_txt.setPosition(cc.p(this.width / 2 + gameStrtTF.width / 6, this.height / 2 - 45 * scaleFactor));
        this.addChild(this.timer_txt, 5);

    },
    setTime: function (val) {
        this.timer_txt.setString(" " + val);
    },
    setDealPnt: function (val) {
        this.pntDealTF.setString(val);
    },
    setRndLvl: function (val) {
        this.rndLvlTF.setString(val);
    },
    setTrnmntName: function (val) {
        this.tmntNameTF.setString(val);
    }

});
var BreakTimePopup = Notification.extend({
    _name: "BreakTimePopup",
    ctor: function (context, str) {
        this._super(context, 700 * scaleFactor, 110 * scaleFactor);

        this.tmntNameTF = new cc.LabelTTF("Tournament", "RobotoBold", 16 * 2 * scaleFactor);
        this.tmntNameTF.setScale(0.5);
        this.tmntNameTF.setColor(cc.color(255, 255, 255));
        this.tmntNameTF.setPosition(cc.p(this.width / 2, this.height / 2 + 30 * scaleFactor));
        this.addChild(this.tmntNameTF, 5);

        this.rndLvlTF = new cc.LabelTTF("Tournament break time in progress", "RobotoRegular", 14 * 2 * scaleFactor);
        this.rndLvlTF.setScale(0.5);
        this.rndLvlTF.setColor(cc.color(163, 163, 163));
        this.rndLvlTF.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(this.rndLvlTF, 5);

        var gameStrtTF = new cc.LabelTTF("Next level begins in -", "RobotoRegular", 14 * 2 * scaleFactor);
        gameStrtTF.setScale(0.5);
        gameStrtTF.setAnchorPoint(1, 0.5);
        gameStrtTF.setColor(cc.color(163, 163, 163));
        gameStrtTF.setPosition(cc.p(this.width / 2, this.height / 2 - 30 * scaleFactor));
        this.addChild(gameStrtTF, 5);

        this.timer_txt = new cc.LabelTTF("", "RobotoRegular", 14 * 2 * scaleFactor);
        this.timer_txt.setScale(0.5);
        this.timer_txt.setAnchorPoint(0, 0.5);
        this.timer_txt.setColor(cc.color(195, 218, 33));
        this.timer_txt.setPosition(cc.p(this.width / 2 + 5 * scaleFactor, this.height / 2 - 30 * scaleFactor));
        this.addChild(this.timer_txt, 5);

    },
    setTime: function (val) {
        this.timer_txt.setString(val + " Sec");
    },
    setTrnmntName: function (val) {
        this.tmntNameTF.setString(val);
    }
});

var QualifyPopup = Notification.extend({
    _name: "QualifyPopup",
    ctor: function (context, str) {
        this._super(context, 700, 110);

        var message = new cc.LabelTTF(str, "RobotoRegular", 17 * 2 * scaleFactor);
        message.setScale(0.5);
        message._setBoundingWidth(this.width * 2 - 300 * scaleFactor);
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        message.setColor(cc.color(163, 163, 163));
        message.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(message, 5);

    }
});

var LostNotification = Notification.extend({
    _name: "LostNotification",
    ctor: function (context, str) {
        this._super(context, 700 * scaleFactor, 110 * scaleFactor);

        var message = new cc.LabelTTF(str, "RobotoRegular", 17 * 2 * scaleFactor);
        message.setScale(0.5);
        message._setBoundingWidth(this.width * 2 - 300 * scaleFactor);
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        message.setColor(cc.color(163, 163, 163));
        message.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(message, 5);

    }
});


var WinnerNotification = Notification.extend({
    _name: "LostNotification",
    ctor: function (context, str) {
        this._super(context, 700 * scaleFactor, 110 * scaleFactor);

        var message = new cc.LabelTTF(str, "RupeeFordian", 17 * 2 * scaleFactor);
        message.setScale(0.5);
        message._setBoundingWidth(this.width * 2 - 300 * scaleFactor);
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        message.setColor(cc.color(163, 163, 163));
        message.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(message, 5);

    }
});