/**
 * Created by stpl on 9/16/2016.
 */
var FilterParser = cc.LayerColor.extend({
    _isEnabled: null,
    _touchListener: null,
    mFGameName: null,
    mFMaxPlayer: null,
    mFTurnType: null,
    mFGameCashOrPromo: null,
    tournamentDataParser: null,
    selectedSubGameType: null,
    selectedFindGameType: null,
    _lobby: null,
    sRoomPlsList: null,
    mGameList: null,
    cash13PoolArr: null,
    cash13PointArr: null,
    promo13PoolArr: null,
    promo13PointArr: null,
    cash13DealsArr: null,
    promo13DealsArr: null,
    cash21PointArr: null,
    promo21PoolArr: null,
    cash21PoolArr: null,
    promo21PointArr: null,
    roomListArray: null, // contains all filtered array
    // cacheInfoList: null,
    mgameTypeArray: null,
    mMaxPlayerArray: null,
    mTurnTypeArray: null,
    mEntryFeeArray: null,
    mBetAmountArray: null,
    sFinalGamesList: null,
    displayRoomList: null,
    selectCheckboxObj: {},
    checkboxArray: null,
    selectedcheckboxGameTypeArr: null,
    selectedcheckboxPlayersArr: null,
    selectedcheckboxTurnTypeArr: null,
    defaultGameType: null,
    // defaultSubGameType: null,
    _name: null,
    ctor: function (lobby) {

        this._super(cc.color(0, 0, 0));
        this._lobby = lobby;
        this._isEnabled = true;
        this.sRoomPlsList = [];
        this.mGameList = [];
        this.cash13PoolArr = [];
        this.cash13PointArr = [];
        this.promo13PoolArr = [];
        this.promo13PointArr = [];
        this.cash13DealsArr = [];
        this.promo13DealsArr = [];
        this.cash21PointArr = [];
        this.promo21PoolArr = [];
        this.cash21PoolArr = [];
        this.promo21PointArr = [];
        this.roomListArray = [];
        this.mgameTypeArray = [];
        this.mMaxPlayerArray = [];
        this.mTurnTypeArray = [];
        this.mEntryFeeArray = [];
        this.mBetAmountArray = [];
        this.sFinalGamesList = [];
        this.displayRoomList = [];
        this.selectCheckboxObj = {};
        this.checkboxArray = [];
        this.selectedcheckboxGameTypeArr = [];
        this.selectedcheckboxPlayersArr = [];
        this.selectedcheckboxTurnTypeArr = [];
        this.defaultGameType = "CASH";
        this._name = "FilterParser";

        var filterParserLayerSize = cc.size(760 * scaleFactor, 435 * scaleFactor);
        this.setContentSize(filterParserLayerSize);

        this.filterStripSize = cc.size(this.width, 70 * scaleFactor);
        this.tabMargin = 10 * scaleFactor;
        this.tabSpace = 2 * scaleFactor;
        this.listViewHeaderFontSize = 11 * scaleFactor;
        this.fontSize = 12 * scaleFactor;

        this.subGameTypeTabsHeight = 35 * scaleFactor;
        this.subGameTypeTabsWidth = 161 * scaleFactor;
        this.subGameTypeTabsMargin = 9 * scaleFactor;
        this.subGameTypeFontSize = 10 * scaleFactor;
        this.listHeaderSize = cc.size(760 * scaleFactor, 30 * scaleFactor);
        this.findGameTabsHeight = 145 * scaleFactor;

        var filterStripSize = cc.size(759.5 * scaleFactor, 70 * scaleFactor);
        var filterOptionsBg = new cc.DrawNode();
        filterOptionsBg.drawRect(cc.p(0, 330), cc.p(filterStripSize.width, 400 - 3), cc.color(255, 255, 255), 0, cc.color(255, 255, 255));
        this.addChild(filterOptionsBg);

        this.rangeSlider = new ccui.RangedSlider();
        this.rangeSlider.loadBarTexture("SliderWhite.png", ccui.Widget.PLIST_TEXTURE);
        this.rangeSlider.loadSlidBallTextures("Slider_knob.png", "Slider_knob.png", "Slider_knob.png", ccui.Widget.PLIST_TEXTURE);
        this.rangeSlider.loadProgressBarTexture(res.table.sliderBar);
        this.rangeSlider.addEventListener(this.setEntryFeeValue.bind(this));
        this.addChild(this.rangeSlider, 10);

        this.entryFeeText = new cc.LabelTTF("", "RobotoRegular", 12 * 2);
        this.entryFeeText.setColor(cc.color(0, 0, 0));
        this.entryFeeText.setScale(0.5);
        this.addChild(this.entryFeeText, 10);

        this.minValue = new cc.LabelTTF("10", "RobotoRegular", 12 * 2);
        this.minValue.setColor(cc.color(0, 0, 0));
        this.minValue.setScale(0.5);
        this.addChild(this.minValue, 10);

        this.maxValue = new cc.LabelTTF("100", "RobotoRegular", 12 * 2);
        this.maxValue.setColor(cc.color(0, 0, 0));
        this.maxValue.setScale(0.5);
        this.addChild(this.maxValue, 10);

        this.miniRoom = new MiniRoom(this);
        this.miniRoom.setPosition(cc.p(this.width + 1, 0));
        this.addChild(this.miniRoom, 1);

        this.roomListManager = new RoomListManager(this);
        this.addChild(this.roomListManager.listView, 0);

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    cc.log("Blank Listener in " + this._name);
                    applicationFacade._controlAndDisplay.closeAllTab();
                    applicationFacade._lobby.closeAllTab();
                    /*if (this._lobby.myTablePanel) {
                     this._lobby.myTablePanel.removeFromParent(true);
                     this._lobby.myTableTab.hover(false);
                     }
                     this._lobby.lobbySettingPanel.setVisible(false);*/
                    return true;
                }
                return false;
            }.bind(this)
        }, this);

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();
        return true;
    },
    pauseListener: function (bool) {
        if (bool) {
            cc.eventManager.pauseTarget(this, true);
        } else {
            cc.eventManager.resumeTarget(this, true);
        }
        this._isEnabled = !bool;
        this.roomListManager.pauseListener(bool);
    },
    createPracticeAndCashSubGameTab: function () {
        this.findGameDrawNode && this.findGameDrawNode.removeFromParent(true);

        if (this.poolRummyTab) {
            cc.eventManager.removeListener(this.poolRummyTab);
            cc.eventManager.removeListener(this.pointRummyTab);
            cc.eventManager.removeListener(this.dealsRummyTab);
            cc.eventManager.removeListener(this.pointRummy21cardsTab);
            this.poolRummyTab.removeFromParent(true);
            this.pointRummyTab.removeFromParent(true);
            this.dealsRummyTab.removeFromParent(true);
            this.pointRummy21cardsTab.removeFromParent(true);

            this.poolRummyTab = null;
            this.pointRummyTab = null;
            this.dealsRummyTab = null;
            this.pointRummy21cardsTab = null;
        }
        this.poolRummyTab = this.getSubGameTypeTab("13", "POOL RUMMY");
        this.poolRummyTab.setAnchorPoint(0, 0);
        this.subGameTypeTabsHeight = this.poolRummyTab.height + 2;
        this.poolRummyTab.setPosition(cc.p(this.subGameTypeTabsMargin, this.height - this.subGameTypeTabsHeight));

        this.pointRummyTab = this.getSubGameTypeTab("13", "POINT RUMMY");
        this.pointRummyTab.setAnchorPoint(0, 0);
        this.pointRummyTab.setPosition(cc.p(this.subGameTypeTabsWidth + this.subGameTypeTabsMargin * 2, this.height - this.subGameTypeTabsHeight));

        this.dealsRummyTab = this.getSubGameTypeTab("13", "DEALS RUMMY");
        this.dealsRummyTab.setAnchorPoint(0, 0);
        this.dealsRummyTab.setPosition(cc.p(this.subGameTypeTabsWidth * 2 + this.subGameTypeTabsMargin * 3, this.height - this.subGameTypeTabsHeight));

        this.pointRummy21cardsTab = this.getSubGameTypeTab("21", "POINT RUMMY", 21);
        this.pointRummy21cardsTab.setAnchorPoint(0, 0);
        this.pointRummy21cardsTab.setPosition(cc.p(this.subGameTypeTabsWidth * 3 + this.subGameTypeTabsMargin * 4, this.height - this.subGameTypeTabsHeight));

        this.addChild(this.poolRummyTab);
        this.addChild(this.pointRummyTab);
        this.addChild(this.dealsRummyTab);
        this.addChild(this.pointRummy21cardsTab);

        cc.eventManager.addListener(this.filterParserMouseListener.clone(), this.poolRummyTab);
        cc.eventManager.addListener(this.filterParserMouseListener.clone(), this.pointRummyTab);
        cc.eventManager.addListener(this.filterParserMouseListener.clone(), this.dealsRummyTab);
        cc.eventManager.addListener(this.filterParserMouseListener.clone(), this.pointRummy21cardsTab);

        cc.eventManager.addListener(this.filterParserTouchListener.clone(), this.poolRummyTab);
        cc.eventManager.addListener(this.filterParserTouchListener.clone(), this.pointRummyTab);
        cc.eventManager.addListener(this.filterParserTouchListener.clone(), this.dealsRummyTab);
        cc.eventManager.addListener(this.filterParserTouchListener.clone(), this.pointRummy21cardsTab);
    },
    createFindGameTab: function () {

        if (this.poolRummyTab) {

            cc.eventManager.removeListener(this.poolRummyTab);
            cc.eventManager.removeListener(this.pointRummyTab);
            cc.eventManager.removeListener(this.dealsRummyTab);
            cc.eventManager.removeListener(this.pointRummy21cardsTab);

            this.poolRummyTab.removeFromParent(true);
            this.pointRummyTab.removeFromParent(true);
            this.dealsRummyTab.removeFromParent(true);
            this.pointRummy21cardsTab.removeFromParent(true);
        }

        this.poolRummyTab = null;
        this.pointRummyTab = null;
        this.dealsRummyTab = null;
        this.pointRummy21cardsTab = null;

        for (var i = 0; i < this.checkboxArray.length; i++) {
            this.checkboxArray[i].removeFromParent(true);
        }
        this.checkboxArray = [];

        this.findGameDrawNode = new cc.DrawNode();
        this.findGameDrawNode.drawRect(cc.p(0, 0), cc.p(this.listHeaderSize.width, this.findGameTabsHeight), cc.color(208, 197, 167), 1, cc.color(208, 197, 167));
        this.findGameDrawNode.setPosition(cc.p(0, this.height - this.findGameTabsHeight));//cc.winSize.height - this.findGameTabsHeight
        this.addChild(this.findGameDrawNode, 1, "findGameDrawNode");

        var drawpoly1 = new cc.DrawNode();
        var vertexArr1 = [];
        vertexArr1.push(cc.p(0, 0), cc.p(0, 145), cc.p(132, 145), cc.p(155, 72), cc.p(132, 0));
        drawpoly1.drawPoly(vertexArr1, cc.color(237, 227, 196), 0.3, cc.color(237, 227, 196));
        this.findGameDrawNode.addChild(drawpoly1, 1);

        var drawpoly2 = new cc.DrawNode();
        vertexArr1 = [];
        vertexArr1.push(cc.p(132, 0), cc.p(155, 72), cc.p(132, 145), cc.p(317, 145), cc.p(340, 72), cc.p(317, 0));
        drawpoly1.drawPoly(vertexArr1, cc.color(227, 215, 180), 0.3, cc.color(227, 215, 180));
        this.findGameDrawNode.addChild(drawpoly2, 1);

        var drawpoly3 = new cc.DrawNode();
        vertexArr1 = [];
        vertexArr1.push(cc.p(317, 0), cc.p(340, 72), cc.p(317, 145), cc.p(477, 145), cc.p(500, 72), cc.p(477, 0));
        drawpoly1.drawPoly(vertexArr1, cc.color(218, 206, 174), 0.3, cc.color(218, 206, 174));
        this.findGameDrawNode.addChild(drawpoly3, 1);

        this.addGameTypeTab();
        this.addSubGameTypeTab();
    },

    addGameTypeTab: function () {
        this.cashType = this.createSpriteWithOver("grey", "green", "CASH", "Cash");
        this.cashType.setPosition(cc.p(20 + this.cashType.width / 2, this.findGameTabsHeight - 16 - this.cashType.height / 2));
        this.findGameDrawNode.addChild(this.cashType, 3);
        this.cashType.hover(true);

        this.practiceType = this.createSpriteWithOver("grey", "green", "PROMO", "Practice");
        this.practiceType.setPosition(cc.p(20 + this.practiceType.width / 2, this.findGameTabsHeight - (25 + this.cashType.height + this.practiceType.height / 2 )));
        this.findGameDrawNode.addChild(this.practiceType, 3);

        cc.eventManager.addListener(this.filterParserMouseListener.clone(), this.cashType);
        cc.eventManager.addListener(this.filterParserMouseListener.clone(), this.practiceType);

        cc.eventManager.addListener(this.filterParserTouchListener.clone(), this.cashType);
        cc.eventManager.addListener(this.filterParserTouchListener.clone(), this.practiceType);
    },
    addSubGameTypeTab: function () {
        this.PoolCard13 = this.createSpriteWithOver("grey1", "green1", "POOL_13", "13 Card Pool");

        var width = 20 * scaleFactor + this.cashType.width + 74 * scaleFactor + this.PoolCard13.width / 2;
        var height = this.findGameTabsHeight - 16 * scaleFactor - this.PoolCard13.height / 2;

        this.PoolCard13.setPosition(cc.p(width, height));
        this.findGameDrawNode.addChild(this.PoolCard13, 3);
        this.PoolCard13.hover(true);
        height -= 7 + this.PoolCard13.height;

        this.PointsCard13 = this.createSpriteWithOver("grey1", "green1", "POINT_13", "13 Card Points");
        this.PointsCard13.setPosition(cc.p(width, height));
        this.findGameDrawNode.addChild(this.PointsCard13, 3);
        height -= 7 + this.PoolCard13.height;

        this.DealCard13 = this.createSpriteWithOver("grey1", "green1", "DEALS_13", "13 Card Deals");
        this.DealCard13.setPosition(cc.p(width, height));
        this.findGameDrawNode.addChild(this.DealCard13, 3);
        height -= 7 + this.PoolCard13.height;

        this.PointsCard21 = this.createSpriteWithOver("grey1", "green1", "POINT_21", "21 Card Points");
        this.PointsCard21.setPosition(cc.p(width, height));
        this.findGameDrawNode.addChild(this.PointsCard21, 3);

        cc.eventManager.addListener(this.filterParserMouseListener.clone(), this.PoolCard13);
        cc.eventManager.addListener(this.filterParserMouseListener.clone(), this.PointsCard13);
        cc.eventManager.addListener(this.filterParserMouseListener.clone(), this.DealCard13);
        cc.eventManager.addListener(this.filterParserMouseListener.clone(), this.PointsCard21);

        cc.eventManager.addListener(this.filterParserTouchListener.clone(), this.PoolCard13);
        cc.eventManager.addListener(this.filterParserTouchListener.clone(), this.PointsCard13);
        cc.eventManager.addListener(this.filterParserTouchListener.clone(), this.DealCard13);
        cc.eventManager.addListener(this.filterParserTouchListener.clone(), this.PointsCard21);
    },
    initFindGameTabProp: function () {
        this.entryFeeText.setPosition(cc.p(550 * scaleFactor + this.rangeSlider.width / 2, 302 * scaleFactor - yMargin));
        this.minValue.setPosition(cc.p(545 * scaleFactor, 342 * scaleFactor - yMargin));
        this.maxValue.setPosition(cc.p(695 * scaleFactor, 342 * scaleFactor - yMargin));
        this.rangeSlider.setPosition(cc.p(550 * scaleFactor + this.rangeSlider.width / 2, 322 * scaleFactor - yMargin));
        this.selectedFindGameType = this.cashType;
        this.selectedSubGameType = this.PoolCard13;
    },
    initCashPromoTabProp: function () {
        this.entryFeeText.setPosition(cc.p(660 * scaleFactor, 345 * scaleFactor));
        this.minValue.setPosition(cc.p(585 * scaleFactor, 385 * scaleFactor));
        this.maxValue.setPosition(cc.p(730 * scaleFactor, 385 * scaleFactor));
        this.rangeSlider.setPosition(cc.p(665 * scaleFactor, 365 * scaleFactor));
        this.selectedSubGameType = this.poolRummyTab;
    },
    createSpriteWithOver: function (spriteName, spriteOverName, buttonName, labelName) {
        var Sprite = cc.Sprite.extend({
            spriteOverName: null,
            spriteName: null,
            _isSelected: null,
            _isEnabled: null,
            _hovering: null,
            ctor: function (spriteName, spriteOverName, buttonName) {
                this._super(spriteFrameCache.getSpriteFrame(spriteName + ".png"));
                this.spriteName = spriteName;
                this.spriteOverName = spriteOverName;

                this._hovering = false;
                this._isSelected = false;

                this.label = new cc.LabelTTF(labelName, "RobotoRegular", 18);
                this.label.setColor(cc.color(0, 0, 0));
                this.label.setScale(0.65);
                this.label.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
                this.label.setAnchorPoint(0, 0.5);
                this.label.setPosition(cc.p(30 * scaleFactor, this.height / 2 - yMargin));
                this.addChild(this.label, 5);

                this.setName(buttonName);

                return true;
            },
            hover: function (bool) {
                if (bool) {
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteOverName + ".png"));
                    this.label.setColor(cc.color(255, 255, 255));
                } else {
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteName + ".png"))
                    this.label.setColor(cc.color(0, 0, 0));
                }
            },
            pauseListener: function (bool) {
                this._isEnabled = !bool;
            },
            hide: function (bool) {
                this.setVisible(!bool);
                this._isEnabled = !bool;
            }
        });
        return new Sprite(spriteName, spriteOverName, buttonName, labelName);
    },
    getSubGameTypeTab: function (noOfCards, subGameTypeName, cardValue) {
        var tab = cc.Sprite.extend({
            _defaultImage: null,
            _hovering: null,
            ctor: function (noOfCards, subGameTypeName, cardValue, subGameTypeTabsMargin, subGameTypeTabsHeight, subGameTypeFontSize) {
                this._hovering = false;
                this._defaultImage = "";
                cardValue != 21 ? cardValue = this._defaultImage = "gameSubMenuDefault.png" : this._defaultImage = "gameSubMenuDefault21Card.png";
                this._super(spriteFrameCache.getSpriteFrame(this._defaultImage));

                this.setAnchorPoint(0, 0);
                this.setPosition(cc.p(subGameTypeTabsMargin, this.height - subGameTypeTabsHeight));
                this.setName(subGameTypeName.split(" ")[0] + "_" + noOfCards.split(" ")[0]);

                var noOfCardsLabel = new cc.LabelTTF(noOfCards, "RobotoBold", subGameTypeFontSize * 2);
                noOfCardsLabel.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
                noOfCardsLabel.setScale(0.5);
                noOfCardsLabel.setPosition(cc.p(22 * scaleFactor, 24 * scaleFactor));

                var noOfCardsLabel2 = new cc.LabelTTF("CARD", "RobotoBold", subGameTypeFontSize * 2);
                noOfCardsLabel2.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
                noOfCardsLabel2.setScale(0.5);
                noOfCardsLabel2.setPosition(cc.p(22 * scaleFactor, 14 * scaleFactor));

                var subGameLabel = new cc.LabelTTF(subGameTypeName, "RobotoBold", subGameTypeFontSize * 2.5);
                subGameLabel.setDimensions(cc.size(240 * scaleFactor, 30 * scaleFactor));
                subGameLabel.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
                subGameLabel.setScale(0.5);
                subGameLabel.setPosition(cc.p((this.width + 40 * scaleFactor) / 2, this.height / 2));

                this.addChild(noOfCardsLabel, 1);
                this.addChild(noOfCardsLabel2, 1);
                this.addChild(subGameLabel, 1);

            },
            hover: function (bool) {
                if (bool) {
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("gameSubMenuSelected.png"));
                } else {
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this._defaultImage));
                }
                this.setAnchorPoint(0, 0);
            }
        });

        return new tab(noOfCards, subGameTypeName, cardValue, this.subGameTypeTabsMargin, this.subGameTypeTabsHeight, this.subGameTypeFontSize);
    },
    initPracticeAndCashGameFilter: function (gameTypeArray, playerArray, turnTypeArray, entryFeeArray) {
        // sort all arrays
        gameTypeArray.sort();
        playerArray.sort(function (a, b) {
            return a - b;
        });
        turnTypeArray.sort();
        entryFeeArray.sort(function (a, b) {
            return a - b;
        });

        for (var i = 0; i < this.checkboxArray.length; i++) {
            this.checkboxArray[i].removeFromParent(true);
        }
        this.checkboxArray = [];

        if (this._lobby.selectedGameType._name == "FINDGAME") {
            this.setCheckBoxForFindGame(gameTypeArray, playerArray, turnTypeArray, entryFeeArray);
        } else {
            this.setCheckBoxForRemainingAll(gameTypeArray, playerArray, turnTypeArray, entryFeeArray);
        }
        this.minValue.setString(entryFeeArray[0]);
        this.maxValue.setString(entryFeeArray[entryFeeArray.length - 1]);

    },
    setCheckBoxForFindGame: function (gameTypeArray, playerArray, turnTypeArray, entryFeeArray) {
        var margin = 20 * scaleFactor;
        var gameTypeCheckboxWidth = 20 * scaleFactor + this.cashType.width + 74 * scaleFactor + this.PoolCard13.width + 85 * scaleFactor;
        var playerCheckboxWidth = 541 * scaleFactor;
        var turnTypeCheckboxWidth = 641 * scaleFactor;
        var height = this.findGameTabsHeight - 24 * scaleFactor;
        var checkboxWithLabel;

        if (gameTypeArray.length != 0) {
            for (var i = 0; i < gameTypeArray.length; i++) {
                checkboxWithLabel = new CheckBoxModuleWithLabel("gameTypeFilter", gameTypeArray[i], margin, this);
                checkboxWithLabel.setPosition(cc.p(gameTypeCheckboxWidth, height));
                this.checkboxArray.push(checkboxWithLabel);
                this.findGameDrawNode.addChild(checkboxWithLabel, 2);
                height -= 26;
            }
        }
        height = this.findGameTabsHeight - 24 * scaleFactor;
        if (playerArray.length != 0) {
            for (var i = 0; i < playerArray.length; i++) {
                checkboxWithLabel = new CheckBoxModuleWithLabel("playerFilter", playerArray[i] + " Players", margin, this);
                checkboxWithLabel.setPosition(cc.p(playerCheckboxWidth, height));
                this.checkboxArray.push(checkboxWithLabel);
                this.findGameDrawNode.addChild(checkboxWithLabel);
                height -= 26 * scaleFactor;
            }
        }
        height = this.findGameTabsHeight - 24 * scaleFactor;
        if (turnTypeArray.length != 0) {
            for (var i = 0; i < turnTypeArray.length; i++) {
                checkboxWithLabel = new CheckBoxModuleWithLabel("turnTypeFilter", turnTypeArray[i] + " Turn", margin, this);
                checkboxWithLabel.setPosition(cc.p(turnTypeCheckboxWidth, height));
                this.checkboxArray.push(checkboxWithLabel);
                this.findGameDrawNode.addChild(checkboxWithLabel);
                height -= 26 * scaleFactor;

                cc.eventManager.addListener(this.filterParserMouseListener.clone(), checkboxWithLabel);
            }
        }
    },
    setCheckBoxForRemainingAll: function (gameTypeArray, playerArray, turnTypeArray, entryFeeArray) {
        var typeChangeMargin = 5 * scaleFactor,
            initialMargin = 18 * scaleFactor,
            headerCheckBoxMargin = 18 * scaleFactor,
            margin = 13 * scaleFactor,
            xPos = 0,
            yPos = this.height - 70 * scaleFactor;
        var checkboxWithLabel, playersLabel;

        if (gameTypeArray.length > 2)
            yPos = this.height - 55 * scaleFactor;

        if (gameTypeArray.length != 0) {
            var gameTypeY = yPos;
            var gameTypeX = 0;
            if (!(gameTypeArray.length == 1)) {
                for (var i = 0; i < gameTypeArray.length; i++) {
                    checkboxWithLabel = new CheckBoxModuleWithLabel("gameTypeFilter", gameTypeArray[i], margin, this);
                    if (i == 0) {
                        xPos = initialMargin;
                        gameTypeX = xPos;
                    } else if (i == 2 && gameTypeArray.length > 2) {
                        gameTypeX = initialMargin;
                        gameTypeY = this.height - 85 * scaleFactor;
                    } else {
                        gameTypeX = xPos;
                    }
                    checkboxWithLabel.setPosition(cc.p(gameTypeX, gameTypeY));
                    this.checkboxArray.push(checkboxWithLabel);
                    this.addChild(checkboxWithLabel);
                    if (i != 2)
                        xPos += typeChangeMargin + checkboxWithLabel.width;
                }
            } else {
                xPos += initialMargin;
            }
        } else {
            xPos += initialMargin;
        }

        if (playerArray.length != 0) {
            if (!(playerArray.length == 1)) {
                playersLabel = new cc.LabelTTF("Players:", "RobotoBold", 20 * scaleFactor);
                playersLabel.setScale(0.7);
                playersLabel.setAnchorPoint(0, 0.5);
                playersLabel.setPosition(cc.p(xPos, yPos - yMargin));
                playersLabel.setColor(cc.color(0, 0, 0));
                this.checkboxArray.push(playersLabel);
                this.addChild(playersLabel);

                for (var i = 0; i < playerArray.length; i++) {
                    checkboxWithLabel = new CheckBoxModuleWithLabel("playerFilter", playerArray[i], margin, this);
                    if (i == 0) {
                        xPos += playersLabel.width / 1.7 + headerCheckBoxMargin;
                    }
                    checkboxWithLabel.setPosition(cc.p(xPos, yPos));
                    this.checkboxArray.push(checkboxWithLabel);
                    this.addChild(checkboxWithLabel);
                    xPos += typeChangeMargin + checkboxWithLabel.width;
                }
            }
        }
        if (gameTypeArray.length == 0 && playerArray.length == 0) {
            xPos = 0;
            xPos += initialMargin;
        } else if (gameTypeArray.length == 1 && playerArray.length == 1 && (this.selectedSubGameType._name == "POINT_13" || this.selectedSubGameType._name == "POINT_21")) {
            xPos = 0;
            xPos += initialMargin;
        }
        if (this._lobby.selectedGameType._name == "CASH" && (this.selectedSubGameType._name == "POOL_13" || this.selectedSubGameType._name == "DEALS_13")) {
            return;
        } else {
            if (turnTypeArray.length != 0) {
                if (!(turnTypeArray.length == 1)) {
                    playersLabel = new cc.LabelTTF("Turn:", "RobotoBold", 20 * scaleFactor);
                    playersLabel.setScale(0.7);
                    playersLabel.setAnchorPoint(0, 0.5);
                    playersLabel.setPosition(cc.p(xPos, yPos - yMargin));
                    playersLabel.setColor(cc.color(0, 0, 0));
                    this.addChild(playersLabel);
                    this.checkboxArray.push(playersLabel);

                    for (var i = 0; i < turnTypeArray.length; i++) {
                        checkboxWithLabel = new CheckBoxModuleWithLabel("turnTypeFilter", turnTypeArray[i], margin, this);
                        if (i == 0) {
                            xPos += playersLabel.width / 2 + headerCheckBoxMargin;
                        }
                        checkboxWithLabel.setPosition(cc.p(xPos, yPos));
                        this.checkboxArray.push(checkboxWithLabel);
                        this.addChild(checkboxWithLabel);
                        xPos += typeChangeMargin + checkboxWithLabel.width;

                        cc.eventManager.addListener(this.filterParserMouseListener.clone(), checkboxWithLabel);
                    }
                }
            }
        }
    },
    setEntryFeeValue: function (sender, type) {
        switch (type) {
            case ccui.RangedSlider.EVENT_PERCENT_CHANGED_END:
                var percent = this.rangeSlider.getPercent();
                var entryFee;
                if (this.selectedSubGameType._name == "POOL_13" || this.selectedSubGameType._name == "DEALS_13") {
                    entryFee = this.getEntryFeeArray();
                } else {
                    entryFee = this.getBetAmountArray();
                }

                // entryFee = ["5", "10", "15", "25", "50", "100", "200", "250", "500", "1000", "1500", "2000", "3000", "5000"];

                if (entryFee.length == 0)
                    return;

                var valueAtPercentage = Math.floor(100 / (entryFee.length - 1));

                var minModValue = parseInt(percent.min % valueAtPercentage);
                var maxModValue = parseInt(percent.max % valueAtPercentage);

                var minDivideValue = parseInt(percent.min / valueAtPercentage);
                var maxDivideValue = parseInt(percent.max / valueAtPercentage);

                if (minModValue > valueAtPercentage / 2 && minDivideValue != 0 && minDivideValue == maxDivideValue) {
                    minDivideValue -= 1;
                }

                if (maxModValue > valueAtPercentage / 2 || (maxDivideValue != entryFee.length - 1 && minDivideValue == maxDivideValue)) {
                    maxDivideValue += 1;
                }

                if (maxDivideValue == 0)
                    maxDivideValue += 1;
                else if (minDivideValue == (entryFee.length - 1))
                    minDivideValue -= 1;

                if (maxDivideValue >= entryFee.length)
                    maxDivideValue = entryFee.length - 1;

                var minEntryFeeValue = entryFee[minDivideValue];
                var maxEntryFeeValue = entryFee[maxDivideValue];

                this.minValue.setString(minEntryFeeValue);
                this.maxValue.setString(maxEntryFeeValue);

                var listArray = this.roomListManager.getSelectedList();
                listArray = this.roomListManager.getFinalFilteredListByCheckbox(listArray);
                listArray = this.getSortedByEntryFee(listArray, minEntryFeeValue, maxEntryFeeValue);

                if (this._lobby.selectedGameType._name == "FINDGAME") {
                    this.roomListManager.insertRoomList(this.widthArray, listArray, "FINDGAME");
                } else {
                    this.roomListManager.insertRoomList(this.widthArray, listArray, "CASH");
                }
                break;
            case ccui.RangedSlider.EVENT_PERCENT_CHANGED:

                var percent = this.rangeSlider.getPercent();
                var entryFee;
                if (this.selectedSubGameType._name == "POOL_13" || this.selectedSubGameType._name == "DEALS_13") {
                    entryFee = this.getEntryFeeArray();
                } else {
                    entryFee = this.getBetAmountArray();
                }

                if (entryFee.length == 0)
                    return;

                var valueAtPercentage = Math.floor(100 / (entryFee.length - 1));

                var minModValue = parseInt(percent.min % valueAtPercentage);
                var maxModValue = parseInt(percent.max % valueAtPercentage);

                var minDivideValue = parseInt(percent.min / valueAtPercentage);
                var maxDivideValue = parseInt(percent.max / valueAtPercentage);

                if (minModValue > valueAtPercentage / 2 && minDivideValue != 0 && minDivideValue == maxDivideValue) {
                    minDivideValue -= 1;
                }

                if (maxModValue > valueAtPercentage / 2 || (maxDivideValue != entryFee.length - 1 && minDivideValue == maxDivideValue)) {
                    maxDivideValue += 1;
                }

                if (maxDivideValue == 0)
                    maxDivideValue += 1;
                else if (minDivideValue == (entryFee.length - 1))
                    minDivideValue -= 1;

                if (maxDivideValue >= entryFee.length)
                    maxDivideValue = entryFee.length - 1;

                var minEntryFeeValue = entryFee[minDivideValue];
                var maxEntryFeeValue = entryFee[maxDivideValue];

                this.minValue.setString(minEntryFeeValue);
                this.maxValue.setString(maxEntryFeeValue);
                break;
        }
    },
    selectedStateEvent: function (sender, type) {
        var checkboxType = sender._name;
        var status;
        switch (type) {
            case  ccui.CheckBox.EVENT_UNSELECTED:
                status = "unselected";
                this.getfilteredListByCheckbox(checkboxType, status);
                break;
            case ccui.CheckBox.EVENT_SELECTED:
                status = "selected";
                this.getfilteredListByCheckbox(checkboxType, status);
                break;
            default:
                break;
        }
    },
    getfilteredListByCheckbox: function (checkboxType, status) {
        var filterArray = [];
        switch (checkboxType.split("-")[0]) {
            case "gameTypeFilter" :
                if (status == "selected") {
                    this.selectedcheckboxGameTypeArr.push(checkboxType.split("-")[1]);
                } else {
                    var index = this.selectedcheckboxGameTypeArr.indexOf(checkboxType.split("-")[1]);
                    this.selectedcheckboxGameTypeArr.splice(index, 1);
                }
                break;
            case "playerFilter" :
                if (status == "selected") {
                    this.selectedcheckboxPlayersArr.push(checkboxType.split("-")[1]);
                } else {
                    var index = this.selectedcheckboxPlayersArr.indexOf(checkboxType.split("-")[1]);
                    this.selectedcheckboxPlayersArr.splice(index, 1);
                }
                break;
            case "turnTypeFilter" :
                if (status == "selected") {
                    this.selectedcheckboxTurnTypeArr.push(checkboxType.split("-")[1]);
                } else {
                    var index = this.selectedcheckboxTurnTypeArr.indexOf(checkboxType.split("-")[1]);
                    this.selectedcheckboxTurnTypeArr.splice(index, 1);
                }
                break;
        }
        this.roomListManager.filterSubGameTypeByCheckBoxSelected(checkboxType);
    },
    showListViewHeader: function (bool) {
        this.listHeaderNode && this.listHeaderNode.removeFromParent(true);

        this.widthArray = [];
        this.listHeaderNode = new ccui.Layout();
        this.listHeaderNode.setContentSize(cc.size(this.listHeaderSize.width, this.listHeaderSize.height));
        this.listHeaderNode.setBackGroundColorType(ccui.Layout.BG_COLOR_GRADIENT);
        this.listHeaderNode.setBackGroundColor(cc.color(111, 111, 111), cc.color(88, 88, 88));
        // this.listHeaderNode.drawRect(cc.p(0, this.listHeaderSize.height), cc.p(this.listHeaderSize.width, 0), cc.color(111, 111, 111), 0, cc.color(88, 88, 88));
        this.listHeaderNode.setName("listHeaderNode");

        if (this._lobby.selectedGameType._name == "FINDGAME") {
            this.listHeaderNode.setPosition(cc.p(0, 260 * scaleFactor));
        } else {
            this.listHeaderNode.setPosition(cc.p(0, 300 * scaleFactor));
        }
        this.addChild(this.listHeaderNode);

        var widthPerTab = this.listHeaderSize.width / 9;
        var width = 60 * scaleFactor;
        var listHeaderTextArr;
        if (this.selectedSubGameType._name == "POOL_13" || this.selectedSubGameType._name == "DEALS_13") {
            // pool or deal case
            this.entryFeeText.setString("Buy-In");
            listHeaderTextArr = ["Game Type", "Buy-In", "Prize", "Turn Time", "Players", "Status", "Action", "Total Players"];  // Pool and Deal
        } else {
            // point case
            this.entryFeeText.setString("Min Buy-In");
            listHeaderTextArr = ["GameType", "Point Value", "Min Buy-In", "Turn Time", "Players", "Status", "Action", "Total Players"]; // point
        }

        var gameTypeLayout = this.getRoomListHeaderLayout(listHeaderTextArr[0], this.listHeaderSize.height, this.tabMargin, this.listViewHeaderFontSize, "RobotoMedium");
        gameTypeLayout.setPosition(cc.p(width, this.listHeaderNode.height / 2 - yMargin));
        width += widthPerTab + this.tabSpace;
        this.widthArray.push(gameTypeLayout.getPositionX()/* + gameTypeLayout.width / 2*/);

        var entryFeeLayout = this.getRoomListHeaderLayout(listHeaderTextArr[1], this.listHeaderSize.height, this.tabMargin, this.listViewHeaderFontSize, "RobotoMedium");
        entryFeeLayout.setPosition(cc.p(width + this.tabSpace, this.listHeaderNode.height / 2 - yMargin));
        width += widthPerTab + this.tabSpace;
        this.widthArray.push(entryFeeLayout.getPositionX() /*+ entryFeeLayout.width / 2*/);

        var prizeLayout = this.getRoomListHeaderLayout(listHeaderTextArr[2], this.listHeaderSize.height, this.tabMargin, this.listViewHeaderFontSize, "RobotoMedium");
        prizeLayout.setPosition(cc.p(width, this.listHeaderNode.height / 2 - yMargin));
        width += widthPerTab + this.tabSpace;
        this.widthArray.push(prizeLayout.getPositionX() /*+ prizeLayout.width / 2*/);

        var turnTimeLayout = this.getRoomListHeaderLayout(listHeaderTextArr[3], this.listHeaderSize.height, this.tabMargin, this.listViewHeaderFontSize, "RobotoMedium");
        turnTimeLayout.setPosition(cc.p(width, this.listHeaderNode.height / 2 - yMargin));
        width += widthPerTab + this.tabSpace;
        this.widthArray.push(turnTimeLayout.getPositionX() /*+ turnTimeLayout.width / 2*/);

        var playersLayout = this.getRoomListHeaderLayout(listHeaderTextArr[4], this.listHeaderSize.height, this.tabMargin, this.listViewHeaderFontSize, "RobotoMedium");
        playersLayout.setPosition(cc.p(width, this.listHeaderNode.height / 2 - yMargin));
        width += widthPerTab + this.tabSpace;
        this.widthArray.push(playersLayout.getPositionX() /*+ playersLayout.width / 2*/);

        var statusLayout = this.getRoomListHeaderLayout(listHeaderTextArr[5], this.listHeaderSize.height, this.tabMargin, this.listViewHeaderFontSize, "RobotoMedium");
        statusLayout.setPosition(cc.p(width - 10 * scaleFactor, this.listHeaderNode.height / 2 - yMargin));
        width += widthPerTab + this.tabSpace;
        this.widthArray.push(statusLayout.getPositionX() /*+ statusLayout.width / 2*/);

        var actionLayout = this.getRoomListHeaderLayout(listHeaderTextArr[6], this.listHeaderSize.height, this.tabMargin, this.listViewHeaderFontSize, "RobotoMedium");
        actionLayout.setPosition(cc.p(width - 10 * scaleFactor, this.listHeaderNode.height / 2 - yMargin));
        width += widthPerTab + this.tabSpace;
        this.widthArray.push(actionLayout.getPositionX() /*+ actionLayout.width / 2*/);

        var totalPlayersLayout = this.getRoomListHeaderLayout(listHeaderTextArr[7], this.listHeaderSize.height, this.tabMargin, this.listViewHeaderFontSize, "RobotoMedium");
        totalPlayersLayout.setPosition(cc.p(width - 10 * scaleFactor, this.listHeaderNode.height / 2 - yMargin));
        this.widthArray.push(totalPlayersLayout.getPositionX());

        //width += turnTimeLayout.width;
        this.listHeaderNode.addChild(gameTypeLayout, 2);
        this.listHeaderNode.addChild(entryFeeLayout, 2);
        this.listHeaderNode.addChild(prizeLayout, 2);
        this.listHeaderNode.addChild(turnTimeLayout, 2);
        this.listHeaderNode.addChild(playersLayout, 2);
        this.listHeaderNode.addChild(statusLayout, 2);
        this.listHeaderNode.addChild(actionLayout, 2);
        this.listHeaderNode.addChild(totalPlayersLayout, 2);

    },
    getRoomListHeaderLayout: function (string, height, tabMargin, fontSize, fontFamily) {
        var label = new cc.LabelTTF(string, fontFamily, fontSize * 2);
        label.setName(string.replace(/ /g, "") + "Label");
        label.setScale(0.65);
        return label;
    },
    /* initCustomEventListener: function () {
     var _listener1 = cc.EventListener.create({
     event: cc.EventListener.CUSTOM,
     eventName: "TURN_TYPE",
     callback: function (event) {
     this.createToolTip(event.getUserData());
     }.bind(this)
     });
     cc.eventManager.addListener(_listener1, 1);
     },
     createToolTip: function (dataArray) {
     /!*if(!dataArray[0])
     return;*!/
     if (!this.spriteHover && dataArray[0]) {
     this.spriteHover = new TurnTypeToolTip(dataArray);
     this._lobby.addChild(this.spriteHover, 40, "spriteHover");
     }
     this.spriteHover && this.spriteHover.setVisible(dataArray[0]);

     var target = dataArray[2];
     if (target && dataArray[0]) {
     var locationInNode = target.convertToWorldSpace(cc.p(0, 0));
     locationInNode.x += target.width / 2;
     locationInNode.y += 55 * scaleFactor;
     this.spriteHover.setPosition(locationInNode);
     this.spriteHover.label.setString(dataArray[1]);
     }
     },
     showToolTip: function (bool, msg, target, size) {
     var event = new cc.EventCustom("TURN_TYPE");
     event.setUserData([bool, msg, target, size]);
     cc.eventManager.dispatchEvent(event);
     },*/
    createRoomList: function (displayRoomListData) {
        var tabType;
        displayRoomListData = this.roomListManager.getFinalFilteredListByCheckbox(displayRoomListData);
        if (this._lobby.selectedGameType._name == "FINDGAME") {
            tabType = "FINDGAME";
            this.roomListManager.insertRoomList(this.widthArray, displayRoomListData, tabType);
        } else {
            tabType = "CASH";
            this.roomListManager.insertRoomList(this.widthArray, displayRoomListData, tabType);
        }
    },
    initMouseListener: function () {
        this.filterParserMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            swallowTouches: true,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    !target._hovering && target.hover(true);

                    if (!target._hovering && target._name == "turnTypeFilter") {
                        var time;
                        var turnType = target.getChildren()[1]._originalText;

                        if (turnType == "Fast" || turnType == "Fast Turn")
                            time = 30;
                        else
                            time = 40;
                        var msg = "      You will get \n" + time + " sec Turn Time";
                        target.showToolTip(true, msg, cc.size(130, 45));
                    }

                    target._hovering = true;
                    return true;
                } else {
                    if (target._hovering && target._name == "turnTypeFilter")
                        target.showToolTip(false, msg, cc.size(130, 45));

                    if (this.selectedSubGameType != target && this.selectedFindGameType != target) {
                        target._hovering && target.hover(false);
                        target._hovering = false;
                    }
                    return false;
                }
            }.bind(this)
        });
        this.filterParserMouseListener.retain();
    },
    initTouchListener: function () {
        this.filterParserTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode) && applicationFacade._activeTableId == 0) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                applicationFacade._controlAndDisplay.closeAllTab();
                applicationFacade._lobby.closeAllTab();
                if (target != this._lobby.selectedGameType && target != this.selectedSubGameType) {
                    this.subGameTypeListener(target);
                }
            }.bind(this)
        });
        this.filterParserTouchListener.retain();
    },
    setSelectedGameType: function (target, gameTypeName) {
        if (gameTypeName == "CASH") {
            switch (target._name) {
                case "POOL_13":
                    this.selectedSubGameType = target;
                    this.initDynamicFilter(this.cash13PoolArr);
                    break;
                case "POINT_13":
                    this.selectedSubGameType = target;
                    this.initDynamicFilter(this.cash13PointArr);
                    break;
                case "DEALS_13":
                    this.selectedSubGameType = target;
                    this.initDynamicFilter(this.cash13DealsArr);
                    break;
                case "POINT_21":
                    this.selectedSubGameType = target;
                    this.initDynamicFilter(this.cash21PointArr);
                    break;
            }
        } else {
            switch (target._name) {
                case "POOL_13":
                    this.selectedSubGameType = target;
                    this.initDynamicFilter(this.promo13PoolArr);
                    break;
                case "POINT_13":
                    this.selectedSubGameType = target;
                    this.initDynamicFilter(this.promo13PointArr);
                    break;
                case "DEALS_13":
                    this.selectedSubGameType = target;
                    this.initDynamicFilter(this.promo13DealsArr);
                    break;
                case "POINT_21":
                    this.selectedSubGameType = target;
                    this.initDynamicFilter(this.promo21PointArr);
                    break;
            }
        }
    },
    subGameTypeListener: function (target) {
        this.rangeSlider.setBarPercentage({min: 0, max: 100});

        this.selectedSubGameType.hover(false);
        if (this.selectedSubGameType && this.selectedSubGameType != target) {
            this.selectedSubGameType.hover(false);
            target.hover(true);
        }
        var selectedGameTypeName;
        if (this._lobby.selectedGameType._name == "PROMO") {
            this._isEnabled = true;
            this._lobby.gameInfoLabel.setString("Game Info");
            selectedGameTypeName = this._lobby.selectedGameType._name;
            this.setSelectedGameType(target, selectedGameTypeName);
            this.showListViewHeader(true);
        } else if (this._lobby.selectedGameType._name == "CASH") {
            this._isEnabled = true;
            this._lobby.gameInfoLabel.setString("Game Info");
            selectedGameTypeName = this._lobby.selectedGameType._name;
            this.setSelectedGameType(target, selectedGameTypeName);
            this.showListViewHeader(true);
        } else if (this._lobby.selectedGameType._name == "FINDGAME") {
            this._isEnabled = true;
            this._lobby.gameInfoLabel.setString("Game Info");
            if (target._name == "PROMO" || target._name == "CASH") {
                // this._lobby.selectedGameType.hover(false);
                this.selectedFindGameType.hover(false);
                this.selectedFindGameType = target;
                this.selectedFindGameType.hover(true);
                this.setSelectedGameType(this.selectedSubGameType, target._name);
                this.showListViewHeader(true);
            } else {
                selectedGameTypeName = this.selectedFindGameType._name;
                this.setSelectedGameType(target, selectedGameTypeName);
                this.showListViewHeader(true);
            }
        }
        this.selectedSubGameType.hover(true);
        // target.hover(true);
    },
    initDynamicFilter: function (filterArray) {

        this.roomListManager._listViewSelected = false;
        this.roomListManager.listView.removeAllChildrenWithCleanup(true);
        this.showListViewHeader(false);

        if (filterArray instanceof Array && filterArray.length != 0) {
            this.selectedcheckboxGameTypeArr = [];
            this.selectedcheckboxPlayersArr = [];
            this.selectedcheckboxTurnTypeArr = [];
            this.getFilterTypeArray(filterArray);

            var roomEntryFeeArray;
            if (this.selectedSubGameType._name == "POOL_13" || this.selectedSubGameType._name == "DEALS_13") {
                // pool or deal case
                roomEntryFeeArray = this.getEntryFeeArray();
            } else {
                // point case
                roomEntryFeeArray = this.getBetAmountArray();
            }
            var roomGameTypeArray = this.getGameTypeArray();
            var roomMaxPlayersArray = this.getMaxPlayerArray();
            var roomTurnTypeArray = this.getTurnTypeArray();

            this.initPracticeAndCashGameFilter(roomGameTypeArray, roomMaxPlayersArray, roomTurnTypeArray, roomEntryFeeArray);
            this.showListViewHeader(true);
            // this.promo13PointArr = this.getDefaultSortedArray(this.promo13PointArr);
            this.createRoomList(filterArray);
        }
    },
    handleTotalPlayer: function (totalplslist) {

        totalplslist = (totalplslist === undefined) ? this.sRoomPlsList : totalplslist;

        if (totalplslist != null) {
            if (Object.keys(sfsClient.roomListHandler.roomListObj).length != 0)
                this.setGameData(totalplslist);
            this.sRoomPlsList = totalplslist;
        }

        /*if(this._lobby.selectedGameType && this._lobby.tournamentTab && this._lobby.selectedGameType == this._lobby.tournamentTab){
         this._lobby.initGameTypeTab(this._lobby.selectedGameType);
         console.log("tournament tab");
         return;
         }*/
    },
    updateRoomPrize: function (respObj) {
        var expectedPrize = respObj["expectedPrize"];
        var groupId = Number(respObj["commonId"]);
        var roomId = Number(respObj["roomId"]);
        var room = sfsClient.getRoomById(roomId);
        room && room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable("totalPrize", expectedPrize));
        this.roomListManager.updateRoomPrize(groupId);
    },
    updateRoomSeated: function (params) {
        var selectedGameIndex;
        var roomId = parseInt(params[SFSConstants.DATA_ROOMID]);
        var noofPls = parseInt(params[SFSConstants.DATA_NOOFPLS]);
        var seatedPls = params[SFSConstants.DATA_SEATEDPLS];
        var cashOrPromo, gameType, noOfCard, index;
        if (sfsClient.roomListHandler.roomListObj.hasOwnProperty(roomId)) {
            var tableId = sfsClient.roomListHandler.roomListObj[roomId].id;
            var info;
            for (var i = 0; i < this.mGameList.length; i++) {
                info = this.mGameList[i];
                if (info.getmTableId() == tableId) {
                    cashOrPromo = info.getmCashOrPromo();
                    gameType = info.getmGameType();
                    noOfCard = info.getmNumOfCard();
                    info.setSeatedPls(seatedPls);
                    info.setmTotalPlayers("" + noofPls);
                    break;
                }
            }
        }
        index = this.onRoomListUpdate(cashOrPromo, gameType, noOfCard);
        this.roomListArray[index] = this.getDefaultSortedArray(this.roomListArray[index]);

        selectedGameIndex = this.getCurrentSelectedGameType();
        if (selectedGameIndex == index) {
            var listArray = this.roomListManager.getFinalFilteredListByCheckbox(this.roomListArray[index]);
            var minValue = this.minValue.getString();
            var maxValue = this.maxValue.getString();
            listArray = this.getSortedByEntryFee(listArray, minValue, maxValue);
            this.roomListManager.updateListData(listArray);
            this.miniRoom.initMiniRoom(this.roomListManager._selectedItem);
        }
    },

    getCurrentSelectedGameType: function () {
        var selectedGameIndex;
        if (this.selectedSubGameType._name.split("_")[1] == "13") {
            switch (this._lobby.selectedGameType._name) {
                case "CASH" :
                    if (this.selectedSubGameType._name.split("_")[0] == "POOL") {
                        selectedGameIndex = 4;
                    } else if (this.selectedSubGameType._name.split("_")[0] == "DEALS") {
                        selectedGameIndex = 5;
                    } else if (this.selectedSubGameType._name.split("_")[0] == "POINT") {
                        selectedGameIndex = 6;
                    }
                    break;
                case "PROMO" :
                    if (this.selectedSubGameType._name.split("_")[0] == "POOL") {
                        selectedGameIndex = 0;
                    } else if (this.selectedSubGameType._name.split("_")[0] == "DEALS") {
                        selectedGameIndex = 1;
                    } else if (this.selectedSubGameType._name.split("_")[0] == "POINT") {
                        selectedGameIndex = 2;
                    }
                    break;
            }
        } else {
            switch (this._lobby.selectedGameType._name) {
                case "CASH" :
                    if (this.selectedSubGameType._name.split("_")[0] == "POINT") {
                        selectedGameIndex = 7;
                    }
                    break;
                case "PROMO" :
                    if (this.selectedSubGameType._name.split("_")[0] == "POINT") {
                        selectedGameIndex = 3;
                    }
                    break;
            }
        }
        return selectedGameIndex;
    },
    updateTable: function (str, tableId) {
        var selectedGameIndex;
        var cashOrPromo, gameType, noOfCard;
        var parts = str.split(SFSConstants.COLON_DELIMITER);
        for (var i = 0; i < this.mGameList.length; i++) {
            var info = this.mGameList[i];
            if (info.getmCommonId() == parts[0]) {
                cashOrPromo = info.getmCashOrPromo();
                gameType = info.getmGameType();
                noOfCard = info.getmNumOfCard();
                info.setmRoomId(tableId);
                info.setmTableId(parseInt(parts[1]));
                info.setmTotalPlayers("0");
                break;
            }
        }
        // this.onRoomListUpdate1(str);
        var index = this.onRoomListUpdate(cashOrPromo, gameType, noOfCard);
        this.roomListArray[index] = this.getDefaultSortedArray(this.roomListArray[index]);

        selectedGameIndex = this.getCurrentSelectedGameType();
        if (selectedGameIndex == index) {
            var listArray = this.roomListManager.getFinalFilteredListByCheckbox(this.roomListArray[index]);
            var minValue = this.minValue.getString();
            var maxValue = this.maxValue.getString();
            listArray = this.getSortedByEntryFee(listArray, minValue, maxValue);
            this.roomListManager.updateListData(listArray);
            this.miniRoom.initMiniRoom(this.roomListManager._selectedItem);
        }

        /*var index = this.onRoomListUpdate(cashOrPromo, gameType, noOfCard);
         this.updateList(index, "", parts[0], i);*/
    },
    updateGroupTotalPlayers: function (str) {
        var selectedGameIndex;
        var cashOrPromo, gameType, noOfCard;
        var parts = str.split(SFSConstants.COLON_DELIMITER);
        for (var i = 0; i < this.mGameList.length; i++) {
            var info = this.mGameList[i];
            if (info.getmCommonId() == parts[0]) {
                cashOrPromo = info.getmCashOrPromo();
                gameType = info.getmGameType();
                noOfCard = info.getmNumOfCard();
                info.setmGroupTotalPlayers(parts[1]);
                break;
            }
        }

        var index = this.onRoomListUpdate(cashOrPromo, gameType, noOfCard);
        this.roomListArray[index] = this.getDefaultSortedArray(this.roomListArray[index]);

        selectedGameIndex = this.getCurrentSelectedGameType();
        if (selectedGameIndex == index) {
            var listArray = this.roomListManager.getFinalFilteredListByCheckbox(this.roomListArray[index]);
            var minValue = this.minValue.getString();
            var maxValue = this.maxValue.getString();
            listArray = this.getSortedByEntryFee(listArray, minValue, maxValue);
            this.roomListManager.updateListData(listArray);
            this.miniRoom.initMiniRoom(this.roomListManager._selectedItem);
        }
        /*var index = this.onRoomListUpdate(cashOrPromo, gameType, noOfCard);
         this.updateList(index, "", parts[0], i);*/
    },
    onRoomListUpdate: function (cashOrPromo, gameType, noOfCard) {
        var j = 0;
        var index;

        if (noOfCard == "13") {
            if (cashOrPromo == "PROMO") {
                if (gameType.indexOf(SFSConstants.TAG_POOL) >= 0 && !(gameType.indexOf(SFSConstants.GT_BEST3) >= 0)) {
                    index = 0;
                } else if (gameType.indexOf(SFSConstants.TAG_POOL) >= 0 && (gameType.indexOf(SFSConstants.GT_BEST3) >= 0)) {
                    index = 1;
                } else if (gameType.indexOf(SFSConstants.TAG_POINTS) >= 0) {
                    index = 2;
                }
            } else {
                if (gameType.indexOf(SFSConstants.TAG_POOL) >= 0 && !(gameType.indexOf(SFSConstants.GT_BEST3) >= 0)) {
                    index = 4;
                } else if (gameType.indexOf(SFSConstants.TAG_POOL) >= 0 && (gameType.indexOf(SFSConstants.GT_BEST3) >= 0)) {
                    index = 5;
                } else if (gameType.indexOf(SFSConstants.TAG_POINTS) >= 0) {
                    index = 6;
                }
            }
        } else {
            if (cashOrPromo == "PROMO") {
                index = 3;
            } else {
                index = 7;
            }
        }
        return index;
    },
    setGameData: function (totalPlList) {
        this.mGameList = [];
        // copy roomInfo data in mTotalPIList
        var mTotalPlList = totalPlList;
        var commonIdReferrence = [];
        for (var i in sfsClient.roomListHandler.roomListObj) {
            var tableId = sfsClient.roomListHandler.roomListObj[i].id;
            var tableName = sfsClient.roomListHandler.roomListObj[i].name;

            var roomId = (sfsClient.roomListHandler.roomListObj[i].variables.roomId == null ?
                "tableId null: " : sfsClient.roomListHandler.roomListObj[i].variables.roomId.value);
            var gameType = (sfsClient.roomListHandler.roomListObj[i].variables.gameType == null ?
                "gametype null: " : sfsClient.roomListHandler.roomListObj[i].variables.gameType.value);
            var maxPlayers = (sfsClient.roomListHandler.roomListObj[i].variables.maxPlayers == null ?
                "MaxPlayers null: " : sfsClient.roomListHandler.roomListObj[i].variables.maxPlayers.value);
            var roomState = (sfsClient.roomListHandler.roomListObj[i].variables.roomState == null ?
                "Roomstate null: " : sfsClient.roomListHandler.roomListObj[i].variables.roomState.value);
            var entryFee = (sfsClient.roomListHandler.roomListObj[i].variables.entryFee == null ?
                "0" : sfsClient.roomListHandler.roomListObj[i].variables.entryFee.value);
            var totalPrize = (sfsClient.roomListHandler.roomListObj[i].variables.totalPrize == null ?
                "0" : sfsClient.roomListHandler.roomListObj[i].variables.totalPrize.value);
            var turnType = (sfsClient.roomListHandler.roomListObj[i].variables.turnType == null ?
                "TurnType null: " : sfsClient.roomListHandler.roomListObj[i].variables.turnType.value);
            var totalPlayers = (sfsClient.roomListHandler.roomListObj[i].variables.noOfPlayers == null ?
                "0" : sfsClient.roomListHandler.roomListObj[i].variables.noOfPlayers.value);
            var seatedPlayers = (sfsClient.roomListHandler.roomListObj[i].variables.seatedPls == null ?
                "0" : sfsClient.roomListHandler.roomListObj[i].variables.seatedPls.value);
            var betAmount = (sfsClient.roomListHandler.roomListObj[i].variables.betAmt == null ?
                "0" : sfsClient.roomListHandler.roomListObj[i].variables.betAmt.value);
            var pointValue = (sfsClient.roomListHandler.roomListObj[i].variables.pointValue == null ?
                "0" : sfsClient.roomListHandler.roomListObj[i].variables.pointValue.value);
            var commonId = (sfsClient.roomListHandler.roomListObj[i].variables.commonId == null ?
                "0" : sfsClient.roomListHandler.roomListObj[i].variables.commonId.value);
            var mCashOrPromo = (sfsClient.roomListHandler.roomListObj[i].variables.cashOrPromo == null ?
                "CashorPromo null: " : sfsClient.roomListHandler.roomListObj[i].variables.cashOrPromo.value);
            var numOfCard = (sfsClient.roomListHandler.roomListObj[i].variables.numOfCard == null ?
                "0" : sfsClient.roomListHandler.roomListObj[i].variables.numOfCard.value);

            if (mTotalPlList[commonId] && !(commonIdReferrence.indexOf(commonId) >= 0)
                && mTotalPlList[commonId].getTableId() == ("" + tableId)) {
                commonIdReferrence.push(commonId);

                if ((gameType.split("-")[0].toLowerCase() == "point") && roomState.toLowerCase() == "watch") {
                    totalPlayers = "0";
                }
                var gameInfo = new GameInfo();
                gameInfo.setGameInfo(tableId, roomId, tableName, (gameType.toString()), maxPlayers, roomState,
                    (entryFee.toString()), totalPrize, turnType, totalPlayers, seatedPlayers,
                    mCashOrPromo, betAmount, pointValue, commonId, mTotalPlList[commonId].getNoOfPlayers(), numOfCard);
                this.mGameList.push(gameInfo);
            }
            // this.getFinalList(this.mFGameName, this.mFMaxPlayer, this.mFTurnType, this.mFGameCashOrPromo);
        }
        this.getFilteredList();
    },
    getFilteredList: function () {
        this.promo13PoolArr = [];
        this.promo13DealsArr = [];
        this.promo13PointArr = [];
        this.promo21PointArr = [];

        this.cash13PoolArr = [];
        this.cash13DealsArr = [];
        this.cash13PointArr = [];
        this.cash21PointArr = [];
        this.roomListArray = [];

        for (var key = 0; key < this.mGameList.length; key++) {
            var gameType = this.mGameList[key].getmGameType();
            if (this.mGameList[key].getmCashOrPromo() != null || this.mGameList[key].getmCashOrPromo() != "") {
                if (this.mGameList[key].getmCashOrPromo() == SFSConstants.TAG_CASH) {
                    if (this.mGameList[key].getmNumOfCard() == "13") {
                        if (gameType.indexOf(SFSConstants.TAG_POOL) >= 0) {
                            if (gameType.indexOf("101") >= 0 || gameType.indexOf("201") >= 0) {
                                this.cash13PoolArr.push(this.mGameList[key]);
                            } else {
                                this.cash13DealsArr.push(this.mGameList[key]);
                            }
                        } else {
                            this.cash13PointArr.push(this.mGameList[key]);
                        }
                    } else {
                        if (gameType.indexOf(SFSConstants.TAG_POOL) >= 0) {
                            this.cash21PoolArr.push(this.mGameList[key]);
                        } else {
                            this.cash21PointArr.push(this.mGameList[key]);
                        }
                    }

                } else {
                    if (this.mGameList[key].getmNumOfCard() == "13") {
                        if (gameType.indexOf(SFSConstants.TAG_POOL) >= 0) {
                            if (gameType.indexOf("101") >= 0 || gameType.indexOf("201") >= 0) {
                                this.promo13PoolArr.push(this.mGameList[key]);
                            } else {
                                this.promo13DealsArr.push(this.mGameList[key]);
                            }

                        } else {
                            this.promo13PointArr.push(this.mGameList[key]);
                        }
                    } else {
                        if (gameType.indexOf(SFSConstants.TAG_POOL) >= 0) {
                            this.promo21PoolArr.push(this.mGameList[key]);
                        } else {
                            this.promo21PointArr.push(this.mGameList[key]);
                        }
                    }
                }
            }
        }

        this.promo13PoolArr = this.getDefaultSortedArray(this.promo13PoolArr);
        this.promo13DealsArr = this.getDefaultSortedArray(this.promo13DealsArr);
        this.promo13PointArr = this.getDefaultSortedArray(this.promo13PointArr);
        this.promo21PointArr = this.getDefaultSortedArray(this.promo21PointArr);

        this.cash13PoolArr = this.getDefaultSortedArray(this.cash13PoolArr);
        this.cash13DealsArr = this.getDefaultSortedArray(this.cash13DealsArr);
        this.cash13PointArr = this.getDefaultSortedArray(this.cash13PointArr);
        this.cash21PointArr = this.getDefaultSortedArray(this.cash21PointArr);

        this.roomListArray.push(this.promo13PoolArr, this.promo13DealsArr, this.promo13PointArr, this.promo21PointArr,
            this.cash13PoolArr, this.cash13DealsArr, this.cash13PointArr, this.cash21PointArr);

        if (this._lobby.selectedGameType != this._lobby.tournamentTab) {
            this._lobby.initGameTypeTab(this._lobby.cashGameTab, true);
            this._lobby.initSubGameTypeTab(this._lobby.cashGameTab);
            this.showListViewHeader(true);
            this.initDynamicFilter(this.cash13PoolArr);
        }
    },

    getGameTypeArray: function () {
        return this.mgameTypeArray;
    },
    setGameTypeArray: function (cacheGameTypeList) {
        this.mgameTypeArray = cacheGameTypeList;
    },
    getMaxPlayerArray: function () {
        return this.mMaxPlayerArray;
    },
    setMaxPlayerArray: function (cachePlayerList) {
        this.mMaxPlayerArray = cachePlayerList;
    },

    getTurnTypeArray: function () {
        return this.mTurnTypeArray;
    },
    setTurnTypeArray: function (cachePlayerList) {
        this.mTurnTypeArray = cachePlayerList;
    },

    getEntryFeeArray: function () {
        return this.mEntryFeeArray;
    },
    setEntryFeeArray: function (cachePlayerList) {
        this.mEntryFeeArray = cachePlayerList;
    },
    getBetAmountArray: function () {
        return this.mBetAmountArray;
    },

    setBetAmountArray: function (cacheGameTypeList) {
        this.mBetAmountArray = cacheGameTypeList;
    },

    getFilterTypeArray: function (filteredList) {
        var cacheGameTypeList = [];
        var cachePlayerList = [];
        var cacheTurnTypeList = [];
        var cacheEntryFeeList = [];
        var cacheBetAmountList = [];

        for (var key = 0; key < filteredList.length; key++) {

            var gameType = filteredList[key].getmGameType().split("-");
            var maxPlayer = filteredList[key].getmMaxPlayers();
            var turnType = filteredList[key].getmTurnType();

            if (!(cacheGameTypeList.indexOf(gameType[1]) >= 0)) {
                cacheGameTypeList.push(gameType[1]);
            }
            if (!(cachePlayerList.indexOf(maxPlayer) >= 0)) {
                cachePlayerList.push(maxPlayer);
            }
            if (!(cacheTurnTypeList.indexOf(turnType) >= 0)) {
                cacheTurnTypeList.push(turnType);
            }
            if (this.selectedSubGameType._name == "POOL_13" || this.selectedSubGameType._name == "DEALS_13") {
                var entryFee = filteredList[key].getmEntryFee();
                if (!(cacheEntryFeeList.indexOf(entryFee) >= 0)) {
                    cacheEntryFeeList.push(entryFee);
                }
            } else {
                var betAmount = filteredList[key].getmBetAmount();
                if (!(cacheBetAmountList.indexOf(betAmount) >= 0)) {
                    cacheBetAmountList.push(betAmount);
                }
            }
        }

        this.setGameTypeArray(cacheGameTypeList);
        this.setMaxPlayerArray(cachePlayerList);
        this.setTurnTypeArray(cacheTurnTypeList);
        this.setEntryFeeArray(cacheEntryFeeList);
        this.setBetAmountArray(cacheBetAmountList);
    },

    getDefaultSortedArray: function (listArray) {
        var finalSortedArray = [];

        if (listArray instanceof Array && listArray.length != 0) {
            finalSortedArray = listArray;
            // getmTotalPlayers(), getmGroupTotalPlayers(), getmEntryFee(), getmtTotalPrize(), getmGameType()
            var subGameType = finalSortedArray[0].getmGameType();
            finalSortedArray.sort(function (a, b) {
                if ((a.mTotalPlayers - b.getmTotalPlayers()) == 0) {
                    if ((a.getmGroupTotalPlayers() - b.getmGroupTotalPlayers()) == 0) {
                        if (subGameType.indexOf(SFSConstants.TAG_POOL) >= 0) {
                            if ((a.getmEntryFee() - b.getmEntryFee()) == 0) {
                                if ((a.getmtTotalPrize() - b.getmtTotalPrize()) == 0) {
                                    var firstGameType = a.getmGameType().split("-")[1], secondGameType = b.getmGameType().split("-")[1];
                                    if (firstGameType < secondGameType) //sort string ascending
                                        return -1
                                    if (firstGameType > secondGameType)
                                        return 1
                                    return 0
                                    // return a.getmtTotalPrize() - b.getmtTotalPrize();
                                }
                                return a.getmtTotalPrize() - b.getmtTotalPrize();  // 2nd
                            }  // 1st
                            return a.getmEntryFee() - b.getmEntryFee();
                        } else if (subGameType.indexOf(SFSConstants.TAG_POINTS) >= 0) {
                            if ((a.getmPointValue() - b.getmPointValue()) == 0) {
                                if ((a.getmBetAmount() - b.getmBetAmount()) == 0) {
                                    var firstGameType = a.getmGameType().split("-")[1], secondGameType = b.getmGameType().split("-")[1];
                                    if (firstGameType < secondGameType) //sort string ascending
                                        return -1;
                                    if (firstGameType > secondGameType)
                                        return 1;
                                    return 0;
                                    // return a.getmtTotalPrize() - b.getmtTotalPrize();
                                }
                                return a.getmBetAmount() - b.getmBetAmount();  // 2nd
                            }  // 1st
                            return a.getmPointValue() - b.getmPointValue();
                        }

                    }
                    return b.getmGroupTotalPlayers() - a.getmGroupTotalPlayers();
                }
                return b.getmTotalPlayers() - a.getmTotalPlayers();
            });
        }
        return finalSortedArray;
    },
    getSortedByEntryFee: function (listArray, minAmt, maxAmt) {
        var finalSortedArrayByEntryfee = [];
        for (var key = 0; key < listArray.length; key++) {
            if (this.selectedSubGameType._name == "POOL_13" || this.selectedSubGameType._name == "DEALS_13") {
                if (listArray[key].mEntryFee >= minAmt && listArray[key].mEntryFee <= maxAmt) {
                    finalSortedArrayByEntryfee.push(listArray[key]);
                }
            } else {
                if (listArray[key].mBetAmount >= minAmt && listArray[key].mBetAmount <= maxAmt) {
                    finalSortedArrayByEntryfee.push(listArray[key]);
                }
            }
        }

        return finalSortedArrayByEntryfee;
    }
}), CheckBoxModuleWithLabel = ccui.Layout.extend({
    _filterParser: null,
    _name: null,
    _hovering: null,
    ctor: function (checkBoxType, nameOfCheckbox, margin, context) {
        this._super();
        this.setAnchorPoint(0, 0.5);
        // this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        // this.setBackGroundColor(cc.color(255, 255, 50));

        this._filterParser = context;
        this._name = checkBoxType;
        this._hovering = false;

        var label = new cc.LabelTTF(nameOfCheckbox, "RobotoRegular", 18);
        label.setColor(cc.color(0, 0, 0));
        label.setScale(0.7);

        var checkBox = new ccui.CheckBox();
        var chechboxName = nameOfCheckbox.toString().split(" ");

        if (chechboxName.indexOf("Best") >= 0)
            checkBox.setName(checkBoxType + "-" + nameOfCheckbox);
        else {

            checkBox.setName(checkBoxType + "-" + chechboxName[0]);
        }
        checkBox.setTouchEnabled(true);
        checkBox.loadTextures("checkBox.png",
            "checkBoxSelected.png", "checkBoxSelected.png", "checkBoxSelected.png",
            "checkBoxSelected.png", ccui.Widget.PLIST_TEXTURE);

        this.setContentSize(cc.size((label.width / 1.5) + checkBox.width + margin, checkBox.height));

        checkBox.setPosition(cc.p(checkBox.width / 2, this.height / 2));
        checkBox.addEventListener(this._filterParser.selectedStateEvent, this._filterParser);

        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(checkBox.x + margin, this.height / 2 - yMargin));

        this.addChild(checkBox, 1);
        this.addChild(label, 1);

        if (checkBoxType == "turnTypeFilter") {
            this._hovering = true;
        }
    },
    hover: function () {

    },
    showToolTip: function (bool, msg, size) {
        if (!this.spriteHover && bool) {
            this.spriteHover = new TurnTypeToolTip(msg, size);
            this._filterParser._lobby.addChild(this.spriteHover, 40, "spriteHover");
        }
        this.spriteHover && this.spriteHover.setVisible(bool);

        if (bool) {
            var locationInNode = this.convertToWorldSpace(cc.p(0, 0));
            locationInNode.x += this.width / 2;
            locationInNode.y += 55 * scaleFactor;
            this.spriteHover.setPosition(locationInNode);
            this.spriteHover.label.setString(msg);
        }
    }
});
var TurnTypeToolTip = cc.Scale9Sprite.extend({
    ctor: function (msg, size) {
        this._super(spriteFrameCache.getSpriteFrame("rect.png"), cc.rect(0, 0, 30, 30));
        this.setColor(cc.color(255, 238, 215));
        this.setCapInsets(cc.rect(9, 9, 9, 9));
        this.setCascadeColorEnabled(false);
        this.setVisible(true);

        this.setContentSize(size);

        var arrow = new cc.Sprite(spriteFrameCache.getSpriteFrame("arrowBorder.png"));
        arrow.setPosition(cc.p(this.width / 2 - 2 * scaleFactor, -arrow.height / 2 + 6 * scaleFactor));
        arrow.setRotation(-90);
        this.addChild(arrow, 1);

        this.label = new cc.LabelTTF(msg, "RobotoRegular", 26);
        this.label.setScale(0.5);
        this.label.setHorizontalAlignment(ccui.TEXT_ALIGNMENT_CENTER);
        this.label.setAnchorPoint(cc.p(0, 0.5));
        this.label.setColor(cc.color(0, 0, 0));
        this.label._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        this.label.setPosition(10 * scaleFactor, this.height / 2);
        this.addChild(this.label, 3);

        return true;
    }
});