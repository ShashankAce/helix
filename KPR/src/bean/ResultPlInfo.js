/**
 * Created by stpl on 6/21/2016.
 */
var ResultPlInfo = function () {

    this.rName = null;
    this.rMeldCards = null;
    this.rResultType = null;
    this.rRoundScore = null;
    this.rTotalScore = null;
    this.rIsSplit = null;
    this.rPlayerId = null;
    this.rAmt = null;
    this.rIsRejoin = null;

};
ResultPlInfo.prototype.setResultPlInfo = function (name, meldCards, resultType, roundScore, isSplit, totalScore, playerId, amt, isRejoin) {
    this.rName = name;
    this.rMeldCards = meldCards;
    this.rResultType = resultType;
    this.rRoundScore = roundScore;
    this.rTotalScore = totalScore;
    this.rIsSplit = isSplit != "false";
    this.rPlayerId = playerId;
    this.rAmt = amt;
    this.rIsRejoin = isRejoin;
};

ResultPlInfo.prototype.getName = function () {
    return this.rName;
};

ResultPlInfo.prototype.setName = function (name) {
    this.rName = name;
};

ResultPlInfo.prototype.getMeldCards = function () {
    return this.rMeldCards;
};

ResultPlInfo.prototype.setMeldCards = function (meldCards) {
    this.rMeldCards = meldCards;
};

ResultPlInfo.prototype.getResultType = function () {
    return this.rResultType;
};

ResultPlInfo.prototype.setResultType = function (resultType) {
    this.rResultType = resultType;
};

ResultPlInfo.prototype.getRoundScore = function () {
    return this.rRoundScore;
};

ResultPlInfo.prototype.setRoundScore = function (roundScore) {
    this.rRoundScore = roundScore;
};

ResultPlInfo.prototype.getTotalScore = function () {
    return this.rTotalScore;
};

ResultPlInfo.prototype.setTotalScore = function (totalScore) {
    this.rTotalScore = totalScore;
};

ResultPlInfo.prototype.getIsSplit = function () {
    return this.rIsSplit;
};

ResultPlInfo.prototype.setIsSplit = function (isSplit) {
    this.rIsSplit = isSplit;
};

ResultPlInfo.prototype.getPlayerId = function () {
    return this.rPlayerId;
};

ResultPlInfo.prototype.setPlayerId = function (playerId) {
    this.rPlayerId = playerId;
};

ResultPlInfo.prototype.getAmt = function () {
    return this.rAmt;
};

ResultPlInfo.prototype.setAmt = function (amt) {
    this.rAmt = amt;
};

ResultPlInfo.prototype.getIsRejoin = function () {
    return this.rIsRejoin;
};

ResultPlInfo.prototype.setIsRejoin = function (isRejoin) {
    this.rIsRejoin = isRejoin;
};