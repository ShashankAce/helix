/**
 * Created by stpl on 11/7/2016.
 */
var SmileyAndChatNew = cc.Node.extend({
    chatIcon: null,
    smileyIcon: null,
    _gameRoom: null,
    currentSmileyNo: null,
    textField: null,
    smiley: null,
    index: null,
    richText: null,
    ccTimeOutManager: null,
    ctor: function (context) {
        this._super();
        this.smiley = false;
        this.index = 0;
        this.ccTimeOutManager = new CCTimeOutManager();

        /*this.richText = new ccui.RichText();
         this.richText.ignoreContentAdaptWithSize(false);
         this.richText.setContentSize(cc.size(227 * scaleFactor, 5 * scaleFactor));*/

        this._gameRoom = context;
        var mainOuter = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("smileyBorder.png"), cc.rect(0, 0, 41 * scaleFactor, 41 * scaleFactor));
        mainOuter.setAnchorPoint(cc.p(0, 0.5));
        mainOuter.setCapInsets(cc.rect(13 * scaleFactor, 13 * scaleFactor, 13 * scaleFactor, 13 * scaleFactor));
        mainOuter.setContentSize(44 * scaleFactor, 100 * scaleFactor);
        mainOuter.setPosition(-6 * scaleFactor, 100 * scaleFactor);
        this.addChild(mainOuter);

        var verticalLine = new cc.Sprite(spriteFrameCache.getSpriteFrame("horizontalHair.png"));
        verticalLine.setRotation(-90);
        verticalLine.setPosition(cc.p(6 * scaleFactor, 50 * scaleFactor));
        verticalLine.setColor(cc.color(63, 63, 63));
        verticalLine.setScale(1.4, 1);
        mainOuter.addChild(verticalLine);

        var horizontalLine = new cc.Sprite(spriteFrameCache.getSpriteFrame("horizontalHair.png"));
        horizontalLine.setPosition(cc.p(-6 * scaleFactor, 50 * scaleFactor));
        horizontalLine.setColor(cc.color(63, 63, 63));
        horizontalLine.setScaleY(0.5);
        horizontalLine.setContentSize(cc.size(44 * scaleFactor, 1 * scaleFactor));
        mainOuter.addChild(horizontalLine);

        this.chatLayout = new ccui.Layout();
        this.chatLayout.setName("chatLayout");
        this.chatLayout.setAnchorPoint(cc.p(0, 0));
        this.chatLayout.setContentSize(38 * scaleFactor, 45 * scaleFactor);
        this.chatLayout.setPosition(cc.p(6 * scaleFactor, 50 * scaleFactor));
        mainOuter.addChild(this.chatLayout);

        this.chatIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("chat.png"));
        this.chatIcon.setPosition(cc.p(19 * scaleFactor, 25 * scaleFactor));
        this.chatLayout.addChild(this.chatIcon);

        this.smileyLayout = new ccui.Layout();
        this.smileyLayout.setName("smileyLayout");
        this.smileyLayout.setAnchorPoint(cc.p(0, 0));
        this.smileyLayout.setContentSize(38 * scaleFactor, 45 * scaleFactor);
        this.smileyLayout.setPosition(cc.p(6 * scaleFactor, 0));
        mainOuter.addChild(this.smileyLayout);

        this.smileyIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("smiley.png"));
        this.smileyIcon.setPosition(cc.p(19 * scaleFactor, 25 * scaleFactor));
        this.smileyLayout.addChild(this.smileyIcon);

        this.closeBtn = new ButtonSprite("closeBlack.png", "closeButton", this);
        this.closeBtn.setPosition(cc.p(272 * scaleFactor, 150 * scaleFactor));
        this.closeBtn.hide(true);
        this.addChild(this.closeBtn, 100);

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        cc.eventManager.addListener(this.smileyAndChatMouseListener.clone(), this.chatLayout);
        cc.eventManager.addListener(this.smileyAndChatMouseListener.clone(), this.smileyLayout);
        cc.eventManager.addListener(this.smileyAndChatMouseListener.clone(), this.closeBtn);

        cc.eventManager.addListener(this.smileyAndChatTouchListener.clone(), this.chatLayout);
        cc.eventManager.addListener(this.smileyAndChatTouchListener.clone(), this.smileyLayout);
        cc.eventManager.addListener(this.smileyAndChatTouchListener.clone(), this.closeBtn);


        this.addChatContainer();
        this.addSmileyContainer();

    },
    addChatContainer: function () {

        this.chatOuter = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("smileyBorder.png"), cc.rect(0, 0, 41 * scaleFactor, 41 * scaleFactor));
        this.chatOuter.setAnchorPoint(cc.p(0, 0.5));
        this.chatOuter.setCapInsets(cc.rect(13 * scaleFactor, 13 * scaleFactor, 13 * scaleFactor, 13 * scaleFactor));
        this.chatOuter.setContentSize(240 * scaleFactor, 100 * scaleFactor);
        this.chatOuter.setPosition(35 * scaleFactor, 100 * scaleFactor);
        this.chatOuter.setVisible(false);
        this.addChild(this.chatOuter, 2);

        this.listView = new ccui.ListView();
        this.listView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        this.listView.setGravity(ccui.ListView.GRAVITY_LEFT);
        this.listView.setScrollBarAutoHideEnabled(false);
        this.listView.setItemsMargin(0);
        this.listView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.listView.setBackGroundColor(cc.color(255, 255, 255));
        this.listView.setContentSize(cc.size(227 * scaleFactor, 60 * scaleFactor));
        this.listView.setPosition(cc.p(7 * scaleFactor, 35 * scaleFactor));
        this.chatOuter.addChild(this.listView, 50);


        var chatBg = new cc.DrawNode();
        chatBg.drawRect(cc.p(0, 0), cc.p(165 * scaleFactor, 25 * scaleFactor), cc.color(255, 255, 255), 1, cc.color(255, 255, 255));
        chatBg.setPosition(cc.p(8 * scaleFactor, 6 * scaleFactor));
        this.chatOuter.addChild(chatBg, 20, "chatBg");
        cc.eventManager.addListener(this.smileyAndChatTouchListener.clone(), chatBg);

        this.chatEditBox = new cc.TextFieldTTF("", cc.size(165 * scaleFactor, 16 * scaleFactor), cc.TEXT_ALIGNMENT_LEFT, "Arial", 14 * scaleFactor);

        // this.chatEditBox.setPlaceHolder("click here to type");

        this.chatEditBox.setPosition(cc.p(92 * scaleFactor, 19 * scaleFactor));
        this.chatEditBox.setFontFillColor(cc.color(0, 0, 0));
        this.chatEditBox.setDelegate(this);
        this.chatOuter.addChild(this.chatEditBox, 50, "chatEditBox");
        cc.eventManager.addListener(this.smileyAndChatTouchListener.clone(), this.chatEditBox);

        /*this.chatEditBox = new cc.EditBox(cc.size(166, 31), new cc.Scale9Sprite(res.table.editbox), new cc.Scale9Sprite(res.table.editbox));
         this.chatEditBox.setInputFlag(cc.EDITBOX_INPUT_MODE_SINGLELINE);
         this.chatEditBox.setMaxLength(20);
         this.chatEditBox.setFontSize(14);
         this.chatEditBox.setDelegate(this);
         this.chatEditBox.setPlaceHolder("Click here to type");
         this.chatEditBox._edTxt.style.display = "none";
         this.chatEditBox.setPosition(cc.p(89, 17));
         this.chatOuter.addChild(this.chatEditBox, 50);*/

        this.sendBtn = new Button9ScaleSprite("sendButton", this);
        this.sendBtn.setPosition(cc.p(205 * scaleFactor, 19 * scaleFactor));
        this.sendBtn.hide(false);
        this.chatOuter.addChild(this.sendBtn, 50);

        cc.eventManager.addListener(this.smileyAndChatMouseListener.clone(), this.sendBtn);
        cc.eventManager.addListener(this.smileyAndChatTouchListener.clone(), this.sendBtn);

    },
    removeChatOuter: function () {
        this.chatOuter.removeFromParent(true);
        this.chatOuter = null;
    },
    onTextFieldAttachWithIME: function () {
    },
    onTextFieldDetachWithIME: function () {
    },
    onTextFieldDeleteBackward: function () {
    },
    onTextFieldInsertText: function (sender, text, len) {
        if (text == "\n") {
            this.sendButtonListener();
        }
    },
    onClickTrackNode: function (clicked) {
        if (this._gameRoom._myId != -1) {
            var textField = this.chatEditBox;
            clicked && textField.attachWithIME();
            !clicked && textField.detachWithIME();
            textField.setPlaceHolder("|");
        }
    },
    addSmileyContainer: function () {
        this.smileyOuter = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("smileyBorder.png"), cc.rect(0, 0, 41 * scaleFactor, 41 * scaleFactor));
        this.smileyOuter.setAnchorPoint(cc.p(0, 0.5));
        this.smileyOuter.setCapInsets(cc.rect(13 * scaleFactor, 13 * scaleFactor, 13 * scaleFactor, 13 * scaleFactor));
        this.smileyOuter.setContentSize(240 * scaleFactor, 100 * scaleFactor);
        this.smileyOuter.setPosition(35 * scaleFactor, 100 * scaleFactor);
        this.smileyOuter.setVisible(false);
        this.addChild(this.smileyOuter, 5);

        var width = 5 * scaleFactor, height = 0;
        for (var i = 1; i < 11; i++) {

            var smileyContainer = new cc.Sprite(spriteFrameCache.getSpriteFrame("smileyBorder.png"));
            smileyContainer.setName("smileyContainer_" + i);
            smileyContainer.hover = function (bool) {
                bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("smileyBorderSelected.png")) : this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("smileyBorder.png"));
            };
            if (i == 6 || i == 1) {
                width = smileyContainer.width / 2 + 6 * scaleFactor;
            } else if (i > 6) {
                width += smileyContainer.width + 6 * scaleFactor;
            }

            if (i >= 6) {
                height = smileyContainer.height / 2 + 6 * scaleFactor;
            } else if (i > 1 && i < 6) {
                height = 50 + smileyContainer.height / 2 + 2 * scaleFactor;
                width += smileyContainer.width + 6 * scaleFactor;
            }
            if (i == 1)
                height = 50 + smileyContainer.height / 2 + 2 * scaleFactor;
            smileyContainer.setPosition(cc.p(width, height));
            this.smileyOuter.addChild(smileyContainer);

            var smiley = new cc.Sprite(spriteFrameCache.getSpriteFrame("smiley" + i + ".png"));
            smiley.setScale(0.35);
            smiley.setPosition(cc.p(smileyContainer.width / 2, smileyContainer.height / 2));
            smileyContainer.addChild(smiley, 1);

            cc.eventManager.addListener(this.smileyAndChatMouseListener.clone(), smileyContainer);
            cc.eventManager.addListener(this.smileyAndChatTouchListener.clone(), smileyContainer);
        }
    },
    removeSmileyOuter: function () {
        this.smileyOuter.removeFromParent(true);
        this.smileyOuter = null;
    },
    initMouseListener: function () {
        this.smileyAndChatMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this)
        });
        this.smileyAndChatMouseListener.retain();
    },
    initTouchListener: function () {
        this.smileyAndChatTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                cc.log("listener");
                var target = event.getCurrentTarget();
                var targetName = target._name.split("_");
                if (target.isVisible()) {
                    switch (targetName[0]) {
                        case "chatLayout":
                            this.chatListener();
                            break;
                        case "smileyLayout":
                            this.smileyListener();
                            break;
                        case "closeButton":
                            if (target._isEnabled)
                                this.closeButtonListener();
                            break;
                        case "sendButton":
                            if (target._isEnabled)
                                this.sendButtonListener();
                            break;
                        case "smileyContainer":
                            if (target._isEnabled) {
                                if (this.currentSmileyNo != targetName[1]) {
                                    this.smileyButtonListener(targetName[1]);
                                    this.currentSmileyNo = targetName[1];
                                }
                            }
                            break;
                        case "chatEditBox":
                            this.onClickTrackNode(true);
                            break;
                        case "chatBg" :
                            this.onClickTrackNode(true);
                            break;
                    }
                }
                return true;
            }.bind(this)
        });
        this.smileyAndChatTouchListener.retain();
    },
    handleChat: function (senderName, msg) {
        if (this.chatOuter && !this.chatOuter.isVisible()) {
            if (this.smileyOuter.isVisible()) {
                this.smileyIcon.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("smiley.png"));
                // this.removeSmileyOuter();
                this.smileyOuter.setVisible(false);
                this.removeListenerOnSmiley();
            }
            this.closeBtn.hide(false);
            this.chatIcon.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("chatSelected.png"));
            /*if(!this.chatOuter)
             this.addChatContainer();*/
            this.chatOuter.setVisible(true);
            if (this._gameRoom._myId == -1) {
                this.chatEditBox.setPlaceHolder("You are out of chat!!");
            }
            // this.chatEditBox.setPlaceHolder("Click here to type");
            // this.chatEditBox._edTxt.style.display = "block";
            this.sendBtn.hide(false);
            var self = this;
            setTimeout(function () {
                self.onClickTrackNode(true);
            }, 1000);
        }

        var font = 15 * scaleFactor;
        var richText = new ccui.RichText();
        richText.ignoreContentAdaptWithSize(true);

        var text1 = new ccui.RichElementText(1, cc.color.BLACK, 255, " " + senderName + ":", "RobotoBold", font);
        var text2 = new ccui.RichElementText(1, cc.color.BLACK, 255, msg, "RobotoRegular", font);

        richText.pushBackElement(text1);
        richText.pushBackElement(text2);
        richText.formatText(); // force recalculate the content size
        var contentSize = richText.getVirtualRendererSize(); // The content size
        var multiple = Math.ceil(contentSize.width / 210);
        richText.ignoreContentAdaptWithSize(false);

        if (multiple > 1)
            richText.setContentSize(cc.size(210 * scaleFactor, 17.5 * multiple * scaleFactor));
        else
            richText.setContentSize(cc.size(210 * scaleFactor, 17 * multiple * scaleFactor));

        this.listView.pushBackCustomItem(richText);
        this.listView.scrollToPercentVertical(100, 0.5, true);
    },
    /*handleChat: function (senderName, msg) {
     if (!this.chatOuter.isVisible()) {
     this.closeBtn.hide(false);
     this.chatIcon.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("chatSelected.png"));
     this.chatOuter.setVisible(true);
     if (this._gameRoom._myId == -1) {
     this.chatEditBox.setPlaceHolder("You are out of chat!!");
     }
     // this.chatEditBox.setPlaceHolder("Click here to type");
     // this.chatEditBox._edTxt.style.display = "block";
     this.sendBtn.hide(false);
     setTimeout(function () {
     this.onClickTrackNode(true);
     }.bind(this), 1000);
     }

     var lineBreak;

     /!*
     if (senderName.length + msg.length + 2 > 20)
     msg += '\n';*!/
     var font = 12 * scaleFactor;

     var richText = new ccui.RichText();
     richText.ignoreContentAdaptWithSize(false);
     richText.setContentSize(cc.size(227 * scaleFactor, 13 * scaleFactor));

     var text1 = new ccui.RichElementText(1, cc.color.BLACK, 255, " " + senderName + ":", "RobotoBold", font);
     var text2 = new ccui.RichElementText(1, cc.color.BLACK, 255, msg, "RobotoRegular", font);
     var text3 = new ccui.RichElementText(1, cc.color.BLACK, 255, "\n", "RobotoRegular", font);
     // var text3 = new ccui.RichElementText(1, cc.color.BLACK, 255, lineBreak, "RobotoRegular", font);
     // richText.setContentSize(cc.size(200, 20), 10);

     richText.pushBackElement(text1);
     richText.pushBackElement(text2);
     richText.pushBackElement(text3);
     // richText.setScale(0.5);
     this.listView.pushBackCustomItem(richText);
     // cc.log("lines count" + richText.line.count);

     this.listView.scrollToPercentVertical(100, 0.2, true);
     },*/
    addListenerOnSmiley: function () {
        for (var i = 1; i < 11; i++) {
            var smiley = this.smileyOuter.getChildByName("smileyContainer_" + i);
            smiley._isEnabled = true;
            cc.eventManager.addListener(this.smileyAndChatMouseListener.clone(), smiley);
            cc.eventManager.addListener(this.smileyAndChatTouchListener.clone(), smiley);
        }
    },
    removeListenerOnSmiley: function () {
        for (var i = 1; i < 11; i++) {
            var smiley = this.smileyOuter.getChildByName("smileyContainer_" + i);
            smiley._isEnabled = false;
        }
    },
    chatListener: function () {
        this.smiley && this.smiley.hover(false);
        if (this._gameRoom._myId == -1) {
            if (this._gameRoom.smileyAndChatDisableToolTip) {
                this._gameRoom.smileyAndChatDisableToolTip.removeFromParent(true);
                this._gameRoom.smileyAndChatDisableToolTip = null;
            } else {
                this._gameRoom.showSmileyAndChatDisableToolTip();
            }
            return;
        }
        this._gameRoom.smileyAndChatDisableToolTip && this._gameRoom.smileyAndChatDisableToolTip.removeFromParent(true);
        this.smileyIcon.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("smiley.png"));
        this.removeListenerOnSmiley();

        this.chatOuter.setLocalZOrder(5);
        this.smileyOuter.setLocalZOrder(0);

        if (this.smileyOuter.isVisible()) {
            this.smileyOuter.setVisible(false);
        }
        if (!this.chatOuter.isVisible()) {
            this.closeBtn.hide(false);
            this.chatIcon.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("chatSelected.png"));
            this.chatOuter.setVisible(true);
            if (this._gameRoom._myId == -1) {
                this.chatEditBox.setPlaceHolder("You are out of chat!!");
            } else {
                this.chatEditBox.setPlaceHolder("Click here to type");
            }
            // this.chatEditBox.setPlaceHolder("Click here to type");
            // this.chatEditBox._edTxt.style.display = "block";
            this.sendBtn.hide(false);
        } else {
            this.chatIcon.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("chat.png"));
            this.chatOuter.setVisible(false);
            // this.chatEditBox._edTxt.style.display = "none";
            this.closeBtn.hide(true);
            this.sendBtn.hide(true);
        }
    },
    smileyListener: function () {
        this.smiley && this.smiley.hover(false);
        if (this._gameRoom._myId == -1) {
            if (this._gameRoom.smileyAndChatDisableToolTip) {
                this._gameRoom.smileyAndChatDisableToolTip.removeFromParent(true);
                this._gameRoom.smileyAndChatDisableToolTip = null;
            } else {
                this._gameRoom.showSmileyAndChatDisableToolTip();
            }
            return;
        }
        this._gameRoom.smileyAndChatDisableToolTip && this._gameRoom.smileyAndChatDisableToolTip.removeFromParent(true);
        this.sendBtn.hide(true);
        this.chatIcon.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("chat.png"));

        this.chatOuter.setLocalZOrder(0);
        this.smileyOuter.setLocalZOrder(5);

        if (this.chatOuter.isVisible()) {
            this.chatOuter.setVisible(false);
            // this.chatEditBox._edTxt.style.display = "none";
        }
        if (!this.smileyOuter.isVisible()) {
            this.closeBtn.hide(false);
            this.addListenerOnSmiley();
            this.smileyIcon.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("smileySelected.png"));
            this.smileyOuter.setVisible(true);
        } else {
            this.smileyIcon.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("smiley.png"));
            this.removeListenerOnSmiley();
            this.smileyOuter.setVisible(false);
            this.closeBtn.hide(true);
        }
    },
    smileyButtonListener: function (smileyNumber) {
        var smiley = this.smileyOuter.getChildByName("smileyContainer_" + parseInt(smileyNumber));
        if (!this.smiley) {
            smiley.hover(true);
            this.smiley = smiley;
        }
        if (this.smiley && this.smiley != smiley) {
            this.smiley.hover(false);
            this.smiley = smiley;
            smiley.hover(true);
        }
        this.setSmileySelectedIntervalTimer(this.smiley);
        // TODO color on smiley
        this.smileyReq(smileyNumber);
    },
    setSmileySelectedIntervalTimer: function (smiley) {
        var time = 10;
        this.ccTimeOutManager.unScheduleInterval("smileySelectedTimer");
        this.ccTimeOutManager.scheduleInterval(xyz.bind(this), 1000, 0, "smileySelectedTimer");
        function xyz() {
            if (time > 1) {
                time = time - 1;
            } else {
                this.ccTimeOutManager.unScheduleInterval("smileySelectedTimer");
                smiley.hover(false);
            }
        }
    },
    smileyReq: function (smileyNumber) {
        var obj = {};
        obj[SFSConstants.FLD_UNAME] = applicationFacade._myUName;
        obj[SFSConstants.FLD_SMILE] = KHEL_CONSTANTS.SMILEY + parseInt(smileyNumber);
        this._gameRoom.sendReqToServer(SFSConstants.CMD_SMILEREQ, obj);
    },
    closeButtonListener: function () {
        this.sendBtn.hide(true);
        this.removeListenerOnSmiley();
        if (this.chatOuter.isVisible()) {
            this.chatIcon.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("chat.png"));
            this.chatOuter.setVisible(false);
            // this.chatEditBox._edTxt.style.display = "none";
        }
        if (this.smileyOuter.isVisible()) {
            this.smileyIcon.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("smiley.png"));
            this.smileyOuter.setVisible(false);
        }
        this.closeBtn.hide(true);
    },
    sendButtonListener: function () {
        var msg = this.chatEditBox.getString();
        this.chatEditBox.setString("");
        if (this._gameRoom._myId == -1) {
            this.chatEditBox.setPlaceHolder("You are out of chat!!");
        } else {
            this.sendChatReq(msg);
            var self = this;
            setTimeout(function () {
                self.onClickTrackNode(true);
            }, 500)
        }
    },
    sendChatReq: function (msg) {
        var obj = {};
        sfsClient.sendChatMsg(msg, this._gameRoom._serverRoom);
        obj["msg"] = msg;
        this._gameRoom.sendReqToServer(SFSConstants.CMD_CHAT_INFO, obj);
    },
    resetChat: function () {
        this.listView.removeAllChildrenWithCleanup(true);
        this.chatEditBox.setString('');
        if (this._gameRoom._myId == -1) {
            this.chatEditBox.setPlaceHolder("You are out of chat!!");
        } else {
            this.chatEditBox.setPlaceHolder("click here to type");
        }
        // this.chatEditBox.setPlaceHolder("click here to type");
        this.closeButtonListener();
    }
});
var ButtonSprite = cc.Sprite.extend({
    _smileyAndChat: null,
    ctor: function (spriteName, ButtonName, context) {
        this._super(spriteFrameCache.getSpriteFrame(spriteName));

        this._smileyAndChat = context;
        this.setName(ButtonName);

        this._isEnabled = true;
        this.setVisible(true);
        return true;
    },
    hide: function (bool) {
        this.setVisible(!bool);
        this._isEnabled = !bool
    }
});
var Button9ScaleSprite = cc.Scale9Sprite.extend({
    _smileyAndChat: null,
    ctor: function (ButtonName, context) {

        this._super(spriteFrameCache.getSpriteFrame("submitRollover.png"), cc.rect(0, 0, 76 * scaleFactor, 26 * scaleFactor));
        this.setCapInsets(cc.rect(15 * scaleFactor, 11 * scaleFactor, 15 * scaleFactor, 11 * scaleFactor));
        this.setContentSize(cc.size(58 * scaleFactor, 26 * scaleFactor));
        this.setCascadeColorEnabled(false);
        this.setName(ButtonName);

        var text = new cc.LabelTTF("SEND", "RobotoBold", 12 * 2 * scaleFactor);
        text.setScale(0.5);
        text.setPosition(cc.p(this.width / 2, this.height / 2));
        text.setColor(cc.color(255, 255, 255));
        this.addChild(text, 1);

        this._isEnabled = true;
        this.setVisible(true);
        return true;
    },
    hide: function (bool) {
        this.setVisible(!bool);
        this._isEnabled = !bool
    },

});


