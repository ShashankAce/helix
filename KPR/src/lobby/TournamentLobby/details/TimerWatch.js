/**
 * Created by stpl on 1/19/2017.
 */
var CountDownTimer = cc.Node.extend({
    ctor: function () {
        this._super();

        /******************/

        var timeBg1 = new cc.Sprite(spriteFrameCache.getSpriteFrame("TimeBase.png"));
        timeBg1.setPosition(cc.p(-50 * scaleFactor, 0));
        this.addChild(timeBg1);

        var daysLabel = new cc.LabelTTF("Days", "RobotoRegular", 15 * scaleFactor);
        daysLabel.setScale(0.7);
        daysLabel.setPosition(cc.p(timeBg1.x, timeBg1.y - 30 * scaleFactor));
        daysLabel.setColor(cc.color(70, 70, 70));
        this.addChild(daysLabel, 2);

        var separator = new cc.LabelTTF(":", "RobotoRegular", 20 * scaleFactor);
        separator.setScale(0.7);
        separator.setPosition(cc.p(timeBg1.x + 25 * scaleFactor, timeBg1.y));
        separator.setColor(cc.color(70, 70, 70));
        this.addChild(separator, 2);

        /******************/

        var timeBg2 = new cc.Sprite(spriteFrameCache.getSpriteFrame("TimeBase.png"));
        timeBg2.setPosition(cc.p(0, 0));
        this.addChild(timeBg2);

        var hourLabel = new cc.LabelTTF("Hours", "RobotoRegular", 15 * scaleFactor);
        hourLabel.setScale(0.7);
        hourLabel.setColor(cc.color(70, 70, 70));
        hourLabel.setPosition(cc.p(timeBg2.x, timeBg2.y - 30 * scaleFactor));
        this.addChild(hourLabel, 2);

        var separator2 = new cc.LabelTTF(":", "RobotoRegular", 20 * scaleFactor);
        separator2.setScale(0.7);
        separator2.setPosition(cc.p(timeBg2.x + 25 * scaleFactor, timeBg1.y));
        separator2.setColor(cc.color(60, 60, 60));
        this.addChild(separator2, 2);

        /******************/

        var timeBg3 = new cc.Sprite(spriteFrameCache.getSpriteFrame("TimeBase.png"));
        timeBg3.setPosition(cc.p(50 * scaleFactor, 0));
        this.addChild(timeBg3);

        var minuteLabel = new cc.LabelTTF("Min", "RobotoRegular", 15 * scaleFactor);
        minuteLabel.setScale(0.7);
        minuteLabel.setColor(cc.color(60, 60, 60));
        minuteLabel.setPosition(cc.p(timeBg3.x, timeBg3.y - 30 * scaleFactor));
        this.addChild(minuteLabel, 2);

        /******************/

        this.day = new cc.LabelTTF("00", "RobotoRegular", 23 * scaleFactor);
        this.day.setScale(0.7);
        this.day.setPosition(timeBg1.getPosition());
        this.addChild(this.day, 2);

        this.hour = new cc.LabelTTF("00", "RobotoRegular", 23 * scaleFactor);
        this.hour.setScale(0.7);
        this.hour.setPosition(timeBg2.getPosition());
        this.addChild(this.hour, 2);

        this.minute = new cc.LabelTTF("00", "RobotoRegular", 23 * scaleFactor);
        this.minute.setScale(0.7);
        this.minute.setPosition(timeBg3.getPosition());
        this.addChild(this.minute, 2);

    },
    setTime: function (time) {

    }
});