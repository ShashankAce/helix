"use strict";
var ReportProblem = cc.Class.extend({
    ctor: function () {

        var ReportProblemContainer = '<div class="rptProblemFormWrap" id="rptProblemForm">' +
            '<div class="rpt-panel">' +
            '<div class="rpt-panel-header clearfix">' +
            '<div class="fl">Report A Problem</div>' +
            '<div class="fr"><span class="c-icon" id="minimize"><img src=' + res.reportProblem.minus + '></span>' +
            '<span class="c-icon" id="closeBtn">' +
            '<img src=' + res.reportProblem.plus + '></span></div></div>' +
            '<div class="rpt-panel-body">' +
            '<div class="successMsg" id="successBody"><div><img src=' + res.reportProblem.success + ' alt="" /></div>File And Data <br/>' +
            'Submitted Successfully..</div>' +
            '<ul id="formContainer"><li class="clearfix"><div class="leftColm fl"><label>Game: </label> <span id="roomType">Lobby</span></div>' +
            '<div class="rightColm fr text-right"><label>Table ID: </label><span id="tableId">0</span></div></li>' +
            '<li class="clearfix"><div class="leftColm fl"><label>Issue Category: </label></div>' +
            '<div class="rightColm  fr"><select id="soflow"><option>Disconnection</option><option>GamePlay</option>' +
            '<option>Melding</option><option>Point Calculation </option>' +
            '<option>Winning</option><option>Others</option></select></div></li><li class="clearfix"><div class="leftColm fl">' +
            '<label>Comments: </label></div><div class="rightColm  fr"><textarea id="comment"></textarea></div></li>' +
            '<li class="clearfix"><div class="leftColm fl"><label>Upload File: </label></div>' +
            '<div class="rightColm  fr"><input type="file" name="issueImage" accept="image/png, image/jpeg" id="fileInput"/></div></li>' +
            '<li class="clearfix">' +
            '<div class="leftColm fl"></div>' +
            '<div class="rightColm  fr"><input type="submit" value="SUBMIT" id="submitBtn"/>' +
            '<input type="reset" value="RESET" id="resetBtn"/>' +
            '</div></li>' +
            '<li class="clearfix"><div class="leftColm fl"></div><div class="rightColm  fr">' +
            '<div class="progressBar" id="progressTxt">' +
            '</div></div></li>' +
            '</ul></div></div></div>';

        var gameContainer = document.getElementById("Cocos2dGameContainer");
        gameContainer.insertAdjacentHTML('beforeend', ReportProblemContainer);
    },
    init: function () {

        this.rptProblemDiv = document.getElementById("rptProblemForm");
        this.rptProblemDiv.style.display = "";

        this.rpMiniBtn = document.getElementById("minimize");
        this.rpMiniBtn.addEventListener("click", this.minimize.bind(this));

        this.rpCloseBtn = document.getElementById("closeBtn");
        this.rpCloseBtn.addEventListener("click", this.close.bind(this));

        this.rpGameType = document.getElementById("roomType");
        this.rpTableId = document.getElementById("tableId");

        this.rpSelectedIssue = document.getElementById("soflow");
        this.rpComment = document.getElementById("comment");

        this.rpSubmitBtn = document.getElementById("submitBtn");
        this.rpSubmitBtn.addEventListener("click", this.submitIssue.bind(this));

        this.rpResetBtn = document.getElementById("resetBtn");
        this.rpResetBtn.addEventListener("click", this.resetIssue.bind(this));

        this.rpFileInput = document.getElementById("fileInput");

        this.rpSucessBody = document.getElementById("successBody");
        this.rpFormContainer = document.getElementById("formContainer");
        this.rpProgressTxt = document.getElementById("progressTxt");

        this.resetIssue();

    },
    show: function (gameType, tableId) {
        if (parseInt(tableId) == 0)
            tableId = '-&nbsp';

        if (this.rptProblemDiv.style.display == "block") {
            this.rptProblemDiv.style.display = "";
            return;
        }

        this.rpFormContainer.style.display = "block";
        this.rpSucessBody.style.display = "";
        this.rpProgressTxt.style.display = "";

        this.gameType = gameType;
        this.tableId = tableId;
        this.rpGameType.innerHTML = gameType;
        this.rpTableId.innerHTML = tableId;
        this.rptProblemDiv.style.display = "block";
    },
    update: function (gameType, tableId) {
        this.gameType = gameType;
        this.tableId = tableId;
        this.rpGameType.innerHTML = gameType;
        this.rpTableId.innerHTML = tableId;
    },
    minimize: function () {
        this.rptProblemDiv.style.display = "";
    },
    close: function () {
        this.rptProblemDiv.style.display = "";
        this.resetIssue();
    },
    submitIssue: function () {
        this.issueObj = {comment: "", fileBool: false};
        var issue = this.rpSelectedIssue.value;
        var comment = this.rpComment.value;
        var fileBool = this.rpFileInput.files.length != 0;

        this.issueObj = {};
        this.issueObj.comment = comment;
        this.issueObj.fileBool = fileBool;

        this.rpProgressTxt.style.display = "";

        if (!comment && !fileBool) { // no comment or file
            this.rpProgressTxt.style.display = "block";
            this.rpProgressTxt.innerHTML = "Please write a comment or upload a file.";
            return;
        } else {
            this.rpProgressTxt.style.display = "block";
            this.rpProgressTxt.innerHTML = "Uploading...";
        }

        var serverPath = connectionConfig.webServerPath + "ImageUploadServlet";
        // var hPath = 'http://192.168.132.11:8080/RummyGameEngine2X/ImageUploadServlet';
        // var ash = 'http://192.168.132.125:8080/RummyGameEngine2X/ImageUploadServlet';

        var requestedData, contentType;
        if (this.rpFileInput.files.length == 0) {
            requestedData = 'gameName=' + this.gameType + '&roomId=' + this.tableId + '&playerId=' +
                rummyData.playerId + '&category=' + String(issue).toLocaleLowerCase() + '&msg=' + comment;
            contentType = true;
        } else {
            requestedData = new FormData();
            requestedData.append("file", this.rpFileInput.files[0]);
            requestedData.append("gameName", this.gameType + '');
            requestedData.append("roomId", this.tableId + '');
            requestedData.append("playerId", rummyData.playerId + '');
            requestedData.append("category", String(issue).toLocaleLowerCase());
            requestedData.append("msg", comment + '');
            // var sBoundary = "---------------------------" + Date.now().toString(16);
            // cntntType = "multipart\/form-data; boundary=" + sBoundary;
        }

        var xhr = new XMLHttpRequest();
        var that = this;
        xhr.addEventListener("progress", function (e) {
            that.updateProgress(e);
        });
        xhr.addEventListener("load", function (e) {
            that.transferComplete(e);
        });
        xhr.addEventListener("error", function (e) {
            that.transferFailed(e);
        });
        xhr.addEventListener("abort", function (e) {
            that.transferCanceled(e);
        });

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                try {
                    if (!xhr.responseText == "") {
                        var resp = JSON.parse(xhr.responseText);
                        if (!resp.errorCode) {
                            resp.errorCode = 0;
                        }
                        cc.log('Success- ' + resp);
                    }
                    else {
                        cc.log(xhr.status);
                    }
                }
                catch (exception) {
                    cc.log("Code Exception :" + exception + "\n Response :" + xhr.responseText);
                }
            }
        };
        xhr.open("POST", serverPath, true);
        contentType && xhr.setRequestHeader("Content-Type", 'application/x-www-form-urlencoded');
        xhr.send(requestedData);
    },
    updateProgress: function (oEvent) {
        if (oEvent['lengthComputable']) {
            var percentComplete = oEvent['loaded'] / oEvent['total'];
            cc.log(percentComplete);
            // ...
        } else {
            // Unable to compute progress information since the total size is unknown
        }
    },
    transferComplete: function (param) {
        cc.log("The transfer is complete.");

        var timeLapArray = [20, 30, 40, 10];
        if (this.issueObj.comment && !this.issueObj.fileBool)
            timeLapArray = [5];

        this.rpProgressTxt.innerHTML = "Uploading...";
        var percent = 0;
        var that = this;
        var time = timeLapArray[Math.floor(Math.random() * timeLapArray.length)];
        var uploadInt = setInterval(function () {
            percent = percent + 2;

            if (that.issueObj.comment && !that.issueObj.fileBool) {
                that.rpProgressTxt.innerHTML = "Uploading...";
            } else {
                if (percent > 100)
                    that.rpProgressTxt.innerHTML = "Uploading(" + 100 + ")";
                else
                    that.rpProgressTxt.innerHTML = "Uploading(" + percent + ")";
            }

            if (percent >= 100) {
                window.clearInterval(uploadInt);
                that.rpFormContainer.style.display = "";
                that.rpSucessBody.style.display = "block";
                that.rpComment.value = "";
                that.rpFileInput.value = "";
                that.rpSelectedIssue.value = "Others";
            }
        }, time);
    },
    transferFailed: function (param) {
        cc.log("An error occurred while transferring the file.");
        this.transferComplete(true);
        // return;
        // this.rpFormContainer.style.display = "";
        // this.rpSucessBody.style.display = "block";
    },
    transferCanceled: function (evt) {
        cc.log("The transfer has been canceled by the user.");
    },
    resetIssue: function () {
        this.rpComment.value = "";
        this.rpFileInput.value = "";
        this.rpSelectedIssue.value = "Others";
        this.rpProgressTxt.style.display = "";
        this.rpProgressTxt.innerHTML = "";
        this.rpFormContainer.style.display = "block";
        this.rpSucessBody.style.display = "";
    }
});


// upload JPEG files
function UploadFile(file) {

    var xhr = new XMLHttpRequest();
    if (xhr.upload && file.type == "image/jpeg" && file.size <= $id("MAX_FILE_SIZE").value) {

        // progress bar
        xhr.upload.addEventListener("progress", function (e) {
            var pc = parseInt(100 - (e.loaded / e.total * 100));
            progress.style.backgroundPosition = pc + "% 0";
        }, false);


        // file received/failed
        xhr.onreadystatechange = function (e) {
            if (xhr.readyState == 4) {
                progress.className = (xhr.status == 200 ? "success" : "failure");
            }
        };


        // start upload
        xhr.open("POST", $id("upload").action, true);
        xhr.setRequestHeader("X-FILENAME", file.name);
        xhr.send(file);

    }

}

