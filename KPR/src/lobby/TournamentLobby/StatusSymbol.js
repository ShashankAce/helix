/**
 * Created by stpl on 1/20/2017.
 */
var StatusSymbol = cc.Node.extend({
    _name: "statusSym",
    _currentState: null,
    _parentClass: null,
    status: null,
    ctor: function (context, pos) {
        this._super();
        this._parentClass = context;
        // create all buttons and labels
        // var pos = cc.p(150,0);

        var CommonBtn = CreateBtn.extend({
            _symbolName: null,
            ctor: function (buttonName) {
                this._super("OkBtn", "OkBtnOver");
                this._symbolName = "Symbol_Btn";

                if (buttonName) {
                    if (buttonName.indexOf("_") >= 0) {
                        if (buttonName.split("_")[1] == "Enable")
                            this._isEnabled = true;
                        this._isEnabled = false;
                    } else this._isEnabled = true;
                    this.setName(buttonName.split("_")[0]);
                }
            },
            setSprite: function (spriteName, spriteOverName, buttonName) {
                this.spriteName = spriteName;
                this.spriteOverName = spriteOverName;
                var spriteFrame = spriteFrameCache.getSpriteFrame(this.spriteName + ".png");
                this.initWithSpriteFrame(spriteFrame);
            }
        });

        this.statusBtn = new CommonBtn();
        this.statusBtn.setPosition(cc.p(0, 0));
        this.addChild(this.statusBtn, 2);

        this.statusText = new cc.LabelTTF("", "RobotoRegular", 24);
        this.statusText._isEnabled = true;
        this.statusText.setScale(0.5);
        this.statusText.setPosition(cc.p(0, 0));
        this.statusText.setColor(cc.color(255, 255, 255));
        this.addChild(this.statusText, 2);

        return true;

    },
    // flag true for lobby button type and false for mini room button type
    setState: function (room, _myTournament, buttonType) {
        var status = room.getVariable("status").value;
        var regNum = room.getVariable("regNum").value;
        var maxRegNum = room.getVariable("maxRegNum").value;
        var regStartTime = room.getVariable("regStartTime").value;
        regStartTime = moment(regStartTime).format('DD MMM,hh:mm a');

        var currentTimeInSec = (new Date).getTime();
        var currentDateAndTime = moment(currentTimeInSec).format('DD MMM,hh:mm a');

        var startTimeArr = regStartTime.split(",");
        var currentTimeArr = currentDateAndTime.split(",");

        var time = "";
        if (startTimeArr[0] == currentTimeArr[0]) {
            var day = "Today";
            time += startTimeArr[1] + " " + day;
        } else {
            time = startTimeArr[0] + " " + startTimeArr[1];
        }
        // this.removeAllChildren(true);

        var flag = false, enable = false;
        var spriteName = null, spriteOverName = null, buttonName = null;

        switch (status.toUpperCase().toString()) {
            case TOURNAMENT_CONSTANTS.REGISTERING :
                if (_myTournament) {
                    flag = true;
                    enable = true;
                    if (buttonType) {
                        spriteName = "QuitBar";
                        spriteOverName = "Quit_over";
                    } else {
                        spriteName = "quit";
                        spriteOverName = "quit";
                    }
                    buttonName = "Quit_Enable";
                } else {
                    flag = true;
                    enable = true;
                    if (buttonType) {
                        spriteName = "joinButton";
                        spriteOverName = "joinButtonOver";
                    } else {
                        spriteName = "detailJoin";
                        spriteOverName = "detailJoinHover";
                    }
                    buttonName = "Join_Enable";
                }
                break;
            case TOURNAMENT_CONSTANTS.OPEN :
                flag = true;
                enable = false;
                if (buttonType) {
                    spriteName = "joinDisable";
                    spriteOverName = "";
                    this.statusBtn._regStartTime = time;
                } else {
                    spriteName = "detailJoinDisable";
                    spriteOverName = "";
                }
                buttonName = "Join_Disable";
                break;
            case TOURNAMENT_CONSTANTS.RUNNING :
                if (_myTournament) {
                    if((AllTournamentRoomListStrip.ALREADY_JOIN_ROOM.indexOf(String(room.id)) != -1)){
                        flag = true;
                        enable = true;
                        if (buttonType) {
                            spriteName = "takeSeat";
                            spriteOverName = "takeSeatOver";
                        } else {
                            spriteName = "takeSeatBig";
                            spriteOverName = "takeSeatOverBig";
                        }
                        buttonName = "TakeSeat_Enable";
                    }else{
                        flag = false;
                    }
                } else {
                    flag = false;
                }
                break;
            case TOURNAMENT_CONSTANTS.FREEZE :
                if (_myTournament) {
                    if((AllTournamentRoomListStrip.ALREADY_JOIN_ROOM.indexOf(String(room.id)) != -1)){
                        flag = true;
                        enable = false;
                        if (buttonType) {
                            spriteName = "takeSeatDisable";
                            spriteOverName = "takeSeatDisable";
                        } else {
                            spriteName = "takeSeatDisableBig";
                            spriteOverName = "takeSeatDisableBig";
                        }
                        buttonName = "TakeSeat_Disable";
                    }
                } else {
                    flag = false;
                }
                break;
            case TOURNAMENT_CONSTANTS.COMPLETED :
            case TOURNAMENT_CONSTANTS.CANCELLED :
            default :
                //enable = false;
                flag = false;
        }

        if (buttonType)
            this.statusText.setColor(cc.color(0, 0, 0));
        else  this.statusText.setColor(cc.color(166, 189, 0));
        if (flag) {
            if (status.toLowerCase() == "registering" && regNum == maxRegNum) {
                if (_myTournament) {
                    if (buttonType) {
                        spriteName = "QuitBar";
                        spriteOverName = "Quit_over";
                    } else {
                        spriteName = "quit";
                        spriteOverName = "quit";
                    }
                    buttonName = "Quit_Enable";
                    this.status = "WAITING";
                } else {
                    if (buttonType) {
                        spriteName = "wait";
                        spriteOverName = "waitHover";
                    } else {
                        spriteName = "waitbig";
                        spriteOverName = "waitbighover";
                    }
                    buttonName = "Waiting_Enable";
                    this.status = "WAITING";

                   /* flag = true;
                    enable = false;*/
                }
            } else {
                this.status = "JOIN";
            }
            this.statusBtn.hide(false);
            this.statusText.setString("");
            this.statusBtn.setSprite(spriteName, spriteOverName);
            this.statusBtn.setName(buttonName.split("_")[0]);
        } else {
            this.statusBtn.hide(true);
            this.statusText.setString(status);
        }

        if (enable) {
            this.statusBtn._isEnabled = true;
        } else {
            this.statusBtn._isEnabled = false;
        }
    }
});

