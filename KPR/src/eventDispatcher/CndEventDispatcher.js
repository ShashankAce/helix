/**
 * Created by stpl on 9/27/2016.
 */
var CndEventDispatcher = cc.Class.extend({
    _gameInterface: null,
    ctor: function (context) {
        this._gameInterface = context;
    },
    dispatchSFSEvents: function (cmd, params) {
        switch (cmd) {
            case SFSConstants.SERVER_TIME:
                this._gameInterface.setServerTime(params["time"]);
                break;
            case SFSConstants.CMD_ROOMINFO:
                /*if (dataObj.getInt("time") > 0)
                 setMessage(dataObj.getInt("time"));
                 else
                 if (dataObj.getUtfString("str")=="")
                 {
                 serverMsgMC.gotoAndStop(2);
                 serverMsgMC.visible = true;
                 }*/

                break;
            case SFSConstants.VIDEO_RESP:
                // handleVideoResp(dataObj);
                break;
            case SFSConstants.SERVER_RESTART:
                /*
                 if (dataObj.getInt("time") > 0)
                 setMessage(dataObj.getInt("time"));
                 */
                break;
            case SFSConstants.CMD_BONUSTOCASH:
                /*if(timeOut)
                 clearTimeout(timeOut);
                 bonusToCashPopUp.gotoAndStop(1);
                 bonusToCashPopUp.mc.close_btn.addEventListener(MouseEvent.CLICK, closeBonusCashPopUp);
                 //Main.getInstance()._contDispPanel.bonusToCashPopUp.mc.txt1.text = "Congratulations!\nYour bonus is successfully converted to `" + dataObj.amt+".";
                 bonusToCashPopUp.mc.txt1.text = "Rs. " + dataObj.getDouble("amt") + " has been disbursed from your";
                 bonusToCashPopUp.play();
                 bonusToCashPopUp.visible = true;
                 timeOut = setTimeout(closeBonusCashPopUp,10000,null);
                 */
                break;
            case SFSConstants.CMD_HC_SFS_RESTART:
                if (parseInt((params["time"])) > 0)
                    this._gameInterface.setServerRestartMessage(60 * parseInt((params["time"])));
                break;
        }

    }
});