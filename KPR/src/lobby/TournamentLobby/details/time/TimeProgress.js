/**
 * Created by stpl on 1/20/2017.
 */
var TimeProgress = cc.Layer.extend({

    _tournamentDetail: null,

    ctor: function (context, room) {
        this._tournamentDetail = context;

        this._super();
        this.trnmntRoom = room;
        this.rId = room.id;
        this.setContentSize(cc.size(720 * scaleFactor, 390 * scaleFactor));
        this.setPosition(cc.p(220 * scaleFactor, 3 * scaleFactor));

        this.initHeader();
        this.initLevelProgressListView();

        /* var roundBtn;
         var totRound = this._tournamentDetail.TOT_ROUND;
         var curRound = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value;*/

    },
    initHeader: function () {

        var headerStrip = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("pentagon.png"));
        headerStrip.setCapInsets(cc.rect(5, 7, 5, 7));
        headerStrip.setContentSize(cc.size(719 * scaleFactor, 45 * scaleFactor));
        headerStrip.setFlippedX(true);
        headerStrip.setAnchorPoint(0, 0.5);
        headerStrip.setColor(cc.color(200, 200, 200));
        headerStrip.setCascadeColorEnabled(false);
        headerStrip.setPosition(cc.p(720 * scaleFactor, 362 * scaleFactor));
        this.addChild(headerStrip, 5);

        var levelSprite = new cc.Scale9Sprite(spriteFrameCache.getSpriteFrame("pentagon.png"));
        levelSprite.setCapInsets(cc.rect(5, 7, 5, 7));
        levelSprite.setContentSize(cc.size(170 * scaleFactor, 45 * scaleFactor));
        levelSprite.setAnchorPoint(0, 0.5);
        levelSprite.setPosition(cc.p(1 * scaleFactor, 362 * scaleFactor));
        this.addChild(levelSprite, 10);

        var label;
        label = new cc.LabelTTF("Level", "RobotoBold", 20 * scaleFactor);
        label.setScale(0.7);
        label.setPosition(cc.p(45 * scaleFactor, 362 * scaleFactor));
        this.addChild(label, 12);

        label = new cc.LabelTTF("Rank", "RobotoBold", 20 * scaleFactor);
        label.setScale(0.7);
        label.setPosition(cc.p(210 * scaleFactor, 362 * scaleFactor));
        this.addChild(label, 12);

        label = new cc.LabelTTF("Players", "RobotoBold", 20 * scaleFactor);
        label.setScale(0.7);
        label.setPosition(cc.p(292 * scaleFactor, 362 * scaleFactor));
        this.addChild(label, 12);

        label = new cc.LabelTTF("Chips", "RobotoBold", 20 * scaleFactor);
        label.setScale(0.7);
        label.setPosition(cc.p(389 * scaleFactor, 362 * scaleFactor));
        this.addChild(label, 12);

        label = new cc.LabelTTF("Status", "RobotoBold", 20 * scaleFactor);
        label.setScale(0.7);
        label.setPosition(cc.p(473 * scaleFactor, 362 * scaleFactor));
        this.addChild(label, 12);


    },
    initLevelProgressListView: function () {

        var rect = new cc.DrawNode();
        rect.drawRect(cc.p(1 * scaleFactor, 46 * scaleFactor), cc.p(719 * scaleFactor, (339 + 46) * scaleFactor), cc.color(162, 162, 162), 1, cc.color(162, 162, 162));
        this.addChild(rect, 0);

        this.progressLevelListView = new ccui.ListView();
        this.progressLevelListView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        this.progressLevelListView.setGravity(ccui.ListView.GRAVITY_CENTER_HORIZONTAL);
        this.progressLevelListView.setScrollBarAutoHideEnabled(true);
        this.progressLevelListView.setTouchEnabled(true);
        this.progressLevelListView.setItemsMargin(0);
        this.progressLevelListView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.progressLevelListView.setBackGroundColor(cc.color(255, 255, 255));
        this.progressLevelListView.setContentSize(cc.size(158 * scaleFactor, 293 * scaleFactor));
        this.progressLevelListView.setPosition(cc.p(1 * scaleFactor, 46 * scaleFactor));
        this.progressLevelListView.addEventListener(this.progressListViewListener.bind(this));
        this.addChild(this.progressLevelListView, 1);

        this.progressListView = new ccui.ListView();
        this.progressListView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        this.progressListView.setGravity(ccui.ListView.GRAVITY_CENTER_HORIZONTAL);
        this.progressListView.setScrollBarAutoHideEnabled(true);
        this.progressListView.setTouchEnabled(true);
        this.progressListView.setItemsMargin(0);
        this.progressListView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.progressListView.setBackGroundColor(cc.color(255, 255, 255));
        this.progressListView.setContentSize(cc.size(559 * scaleFactor, 293 * scaleFactor));
        this.progressListView.setPosition(cc.p(160 * scaleFactor, 46 * scaleFactor));
        this.addChild(this.progressListView, 1);

        this.timeToolTip = new ToolTip(cc.size(150 * scaleFactor, 46 * scaleFactor), cc.color(255, 238, 215));
        this.timeToolTip.setAnchorPoint(1, 1);
        this.addChild(this.timeToolTip, 10, 555);
        this.timeToolTip.setPosition(cc.p(this.progressListView.x + 350 * scaleFactor, this.progressListView.y + 200 * scaleFactor));
        this.timeToolTip.setText("Please select level");
        this.timeToolTip.setVisible(true);

        var structStr = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.STRUCTURE).value;
        if (structStr && (structStr == "" || structStr == " "))
            return;

        var tempStrArr = structStr.split(";");
        var tempCurLevel;

        var _isShowRoundIcon = false;
        var curLevel = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value;
        this.progressLevelListView.removeAllChildrenWithCleanup(true);


        var index = 0;
        for (var i = tempStrArr.length, j; i > 0; i--) {
            _isShowRoundIcon = false;
            var rndDisplayArr = tempStrArr[i-1].split(",");
            tempCurLevel = (rndDisplayArr[0]).toString();
            if (curLevel == parseInt(tempCurLevel))
                _isShowRoundIcon = true;


            j = new ProgressLevel({}, this, i, index, _isShowRoundIcon);
            this.progressLevelListView.pushBackCustomItem(j);
            index++;
        }
    },
    progressListViewListener: function (sender, type) {
        switch (type) {
            case ccui.ListView.ON_SELECTED_ITEM_END :
                var listViewEx = sender;
                var item = listViewEx.getItem(listViewEx.getCurSelectedIndex());
                this.updateListViewItem(item);
                break;
            default:
                break;
        }
    },
    updateListViewItem: function (item) {
        this.selectedLevel && this.selectedLevel.changeState(false);
        this.selectedLevel = item;
        item.onClickHandler();
    },
    showCurrentRound: function () {
        var roundList = this.progressLevelListView.getChildren();
        for (var i = 0; i < roundList.length; i++) {
            roundList[i].showRound(false);
        }
        var curRound = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value;
        roundList = this.progressLevelListView.getChildByTag(curRound);
        roundList.showRound(true);
    },
    cleanUpLevelProgress: function () {
        this.progressListView.removeAllChildrenWithCleanup(true);
        this.selectedLevel && this.selectedLevel.changeState(false);
        this.removeChildByTag(555, true);
        this.timeToolTip = new ToolTip(cc.size(150 * scaleFactor, 46 * scaleFactor), cc.color(255, 238, 215));
        this.timeToolTip.setAnchorPoint(1, 1);
        this.addChild(this.timeToolTip, 10, 555);
        this.timeToolTip.setPosition(cc.p(this.progressListView.x + 350 * scaleFactor, this.progressListView.y + 200 * scaleFactor));
        this.timeToolTip.setText("Please select level");
        this.timeToolTip.setVisible(true);
    },
    showLevelProgress: function (dataObj) {
        this.progressListView.removeAllChildrenWithCleanup(true);
        this.removeChildByTag(555, true);
        if (dataObj["msgStr"] == "-1") {
            var statusInfoStr = dataObj["levelInfo"];
            var statusInfoArr = statusInfoStr.split(";");

            for (var i = 0, j; i < statusInfoArr.length; i++) {
                j = new TimeProgressBar(statusInfoArr[i], this, i);
                this.progressListView.pushBackCustomItem(j);
            }
        } else {
            this.timeToolTip = new ToolTip(cc.size(170 * scaleFactor, 46 * scaleFactor), cc.color(255, 238, 215));
            this.timeToolTip.setAnchorPoint(1, 1);
            this.addChild(this.timeToolTip, 10, 555);
            this.timeToolTip.setPosition(cc.p(this.progressListView.x + 350 * scaleFactor, this.progressListView.y + 200 * scaleFactor));
            this.timeToolTip.setText(dataObj["msgStr"]);
            this.timeToolTip.setVisible(true);
        }
    }
});

var TimeProgressBar = ccui.Layout.extend({
    _parentClass: null,
    ctor: function (params, context, index) {
        this._super();
        this._parentClass = context;

        this.setContentSize(cc.size(558 * scaleFactor, 30 * scaleFactor));
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.setCascadeColorEnabled(false);

        if (index % 2 == 0) {
            this.defaultColor = cc.color(235, 235, 235);
        } else this.defaultColor = cc.color(255, 255, 255);
        this.setBackGroundColor(this.defaultColor);

        var progressArr = (params).split(",");

        var label;
        label = new cc.LabelTTF(progressArr[0], "RobotoRegular", 18 * scaleFactor);
        label.setColor(cc.color(0, 0, 0));
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(38 * scaleFactor, this.height / 2));
        this.addChild(label, 5);

        if (progressArr[1].length > 10) {
            progressArr[1] = progressArr[1].substring(0, 10) + '..';
        }
        label = new cc.LabelTTF(progressArr[1], "RobotoRegular", 18 * scaleFactor);
        label.setColor(cc.color(0, 0, 0));
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(110 * scaleFactor, this.height / 2));
        this.addChild(label, 5);

        label = new cc.LabelTTF(progressArr[2], "RobotoRegular", 18 * scaleFactor);
        label.setColor(cc.color(0, 0, 0));
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(212 * scaleFactor, this.height / 2));
        this.addChild(label, 5);
        if (progressArr[4]) {
            label = new cc.LabelTTF(PrizeStringCalculator.calculateStructPrzStr(progressArr[4]), "RupeeFordian", 15 * scaleFactor);
        } else {
            label = new cc.LabelTTF(progressArr[3], "RupeeFordian", 15 * scaleFactor);
        }
        label.setColor(cc.color(0, 0, 0));
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.8);
        label.setPosition(cc.p(292 * scaleFactor, this.height / 2));
        this.addChild(label, 5);
    },
    changeState: function (state) {
        if (state == 1) this.setColor(this.defaultColor);
        else this.setBackGroundColor(cc.color(167, 181, 69));
    }
});

var ProgressLevel = ccui.Layout.extend({
    _parentClass: null,
    ctor: function (params, context, index, levelNo, _isShowRoundIcon) {
        this._super();
        this._parentClass = context;

        this.setContentSize(cc.size(157 * scaleFactor, 30 * scaleFactor));
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_GRADIENT);
        this.setCascadeColorEnabled(false);
        this.setTouchEnabled(true);
        this.setTag(index);

        if (index % 2 == 0) {
            this.defaultColor = cc.color(235, 235, 235);
        } else this.defaultColor = cc.color(255, 255, 255);

        this.setBackGroundColor(this.defaultColor, this.defaultColor);

        this.level = new cc.LabelTTF("Level " + (index), "RobotoRegular", 20 * scaleFactor);
        this.level.setScale(0.6);
        this.level.setColor(cc.color(0, 0, 0));
        this.level.setPosition(cc.p(45 * scaleFactor, this.height / 2));
        this.addChild(this.level, 1, parseInt(index));

        this.arrow = new cc.Sprite(spriteFrameCache.getSpriteFrame('arrow.png'));
        this.arrow.setFlippedX(true);
        this.arrow.setScale(0.3);
        this.arrow.setPosition(cc.p(this.width - 15 * scaleFactor, this.height / 2));
        this.arrow.setVisible(false);
        this.addChild(this.arrow, 1);

        if (_isShowRoundIcon)
            this.showRound(true);


    },
    showRound: function (bool) {
        if (bool) {
            this.currentRoundIcon && this.currentRoundIcon.removeFromParent(true);
            this.currentRoundIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame('CurrentRoundIcon.png'));
            this.currentRoundIcon.setPosition(cc.p(this.currentRoundIcon.width / 2, this.height / 2));
            this.addChild(this.currentRoundIcon, 5, 'currentRoundIcon');
        } else {
            this.currentRoundIcon && this.currentRoundIcon.removeFromParent(true);
        }
    },
    onClickHandler: function () {
        this.changeState(true);
        var levelObj = {levelNo: this.level.getTag()};
        sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.LEVEL_PRGRSS_INFO, levelObj, this._parentClass.rId);
    },
    changeState: function (pressed) {
        if (pressed) {
            this.setBackGroundColor(cc.color(133, 151, 9), cc.color(102, 116, 1));
            this.arrow.setVisible(true);
            this.level.setColor(cc.color(255, 255, 255));
        } else {
            this.setBackGroundColor(this.defaultColor, this.defaultColor);
            this.arrow.setVisible(false);
            this.level.setColor(cc.color(0, 0, 0));
        }
    }
});