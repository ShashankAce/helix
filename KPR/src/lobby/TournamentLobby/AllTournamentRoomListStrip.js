/**
 * Created by stpl on 1/5/2017.
 */
var AllTournamentRoomListStrip = ccui.Layout.extend({
    _name: "AllTournamentRoomListStrip",
    _selectedColor: null,
    _defaultColor: null,
    stripListener: null,
    _isSelected: null,
    tournamentRoomListManager: null,

    room: null,
    _tmntStatus: null,
    roomId: null,
    priorityNum: null,
    _myTournament: null,
    status: null,

    ctor: function (widthArray, room, index, context, myTournament) {
        this._super();
        !myTournament && (myTournament = 0);
        this._myTournament = myTournament;
        this.tournamentRoomListManager = context;
        this.room = room;
        this.roomId = room.id;
        this.status = room.getVariable("status").value;
        this.priorityNum = TOURNAMENT_CONSTANTS.DEFAULT_TOURNAMENT;
        if (this._myTournament == 1) {
            this.priorityNum = TOURNAMENT_CONSTANTS.MY_TOURNAMENT;
        }

        this._selectedColor = cc.color(223, 230, 182);
        this._isSelected = !1;

        this.setTouchEnabled(true);
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.setContentSize(cc.size(755 * scaleFactor, 30 * scaleFactor));
        this.setTag(this.roomId);
        this._name += " " + index;

        if (index % 2 == 0) {
            this._defaultColor = cc.color(255, 255, 255);
        } else {
            this._defaultColor = cc.color(236, 236, 236);
        }

        this.setBackGroundColor(this._defaultColor);

        this.initBar(widthArray, room, index, context);
    },
    isMeInTournament: function () {
        var _isMe = false;
        if (this._myTournament == 1) {
            _isMe = true;
            this.priorityNum = TOURNAMENT_CONSTANTS.MY_TOURNAMENT;
        }
        return _isMe;
    },
    removeEvents: function () {

    },
    updateJoinBtn: function () {
        this.removeEvents();
        var status = this.room.getVariable("status").value;
        var tourName = this.room.getVariable("tournamentName").value;
        this._tmntStatus = status;
        /* if (this._tmntStatus == "Cancelled")
         var message = this.tournamentRoomListManager.getCancelTournamentMsg(this.room, status);*/
        var _isMeInRoom = this.isMeInTournament();

        switch (status.toUpperCase()) {
            //join disable
            case TOURNAMENT_CONSTANTS.OPEN:
                this.priorityNum = TOURNAMENT_CONSTANTS.OPEN_TOURNAMENT;
                this.room.priority = 2;
                this.statusSym.setState(this.room, false, true);
                this.statusSym.statusBtn._regStartTime = moment(this.room.getVariable(TOURNAMENT_CONSTANTS.REG_START_TIME).value).format('DD MMM,hh:mm a');
                break;

            // join enable for registring
            case TOURNAMENT_CONSTANTS.REGISTERING:
                this.statusSym.setState(this.room, _isMeInRoom, true);
                this.room.priority = 1;
                /*if (_isMeInRoom) {
                 this.statusSym.statusBtn.setSprite("QuitBar", "Quit_over");
                 this.statusSym.statusBtn.setName("Quit");
                 this.statusSym.statusBtn._isEnabled = true;
                 }
                 else {
                 this.statusSym.statusBtn.setSprite("joinButton", "joinButtonOver");
                 this.statusSym.statusBtn.setName("Join");
                 this.statusSym.statusBtn._isEnabled = true;
                 this.priorityNum = TOURNAMENT_CONSTANTS.REGISTRING_TOURNAMENT;
                 }*/
                break;
            // registration closed
            case TOURNAMENT_CONSTANTS.FREEZE:
                this.statusSym.setState(this.room, _isMeInRoom, true);
                this.room.priority = 3;
                /*if (_isMeInRoom ) {
                 if(AllTournamentRoomListStrip.ALREADY_JOIN_ROOM.indexOf(String(this.room.id)) != -1){
                 this.statusSym.statusText && this.statusSym.statusText.setString("");
                 this.statusSym.statusBtn.setSprite("takeSeatDisable", "takeSeatDisable");
                 this.statusSym.statusBtn.setName("TakeSeat");
                 this.statusSym.statusBtn._isEnabled = false;
                 }else{
                 this.priorityNum = TOURNAMENT_CONSTANTS.FREEZ_TOURNAMENT;
                 this.statusBtn && this.statusBtn.hide(true);
                 this.statusSym.statusText.setString(status);
                 }

                 } else {
                 this.priorityNum = TOURNAMENT_CONSTANTS.FREEZ_TOURNAMENT;
                 this.statusBtn && this.statusBtn.hide(true);
                 this.statusSym.statusText.setString(status);
                 }*/
                break;
            // tournament running
            case TOURNAMENT_CONSTANTS.RUNNING:
                this.statusSym.setState(this.room, _isMeInRoom, true);
                this.room.priority = 4;
                /*  if (_isMeInRoom) {
                 if(AllTournamentRoomListStrip.ALREADY_JOIN_ROOM.indexOf(String(this.room.id)) != -1){
                 this.statusSym.statusText && this.statusSym.statusText.setString("");
                 this.statusSym.statusBtn.setSprite("takeSeat", "takeSeatOver");
                 this.statusSym.statusBtn.setName("TakeSeat");
                 this.statusSym.statusBtn._isEnabled = true;
                 }else{
                 this.priorityNum = TOURNAMENT_CONSTANTS.RUNNING_TOURNAMENT;
                 this.statusBtn && this.statusBtn.hide(true);
                 this.statusSym.statusText.setString(status);
                 }
                 }
                 else {
                 this.priorityNum = TOURNAMENT_CONSTANTS.RUNNING_TOURNAMENT;
                 this.statusBtn && this.statusBtn.hide(true);
                 this.statusSym.statusText.setString(status);
                 }*/
                break;
            //	tournament complete
            case TOURNAMENT_CONSTANTS.COMPLETED:
                cc.log("completed");
                this.statusSym.setState(this.room, _isMeInRoom, true);
                if (this.priorityNum != TOURNAMENT_CONSTANTS.MY_TOURNAMENT){
                    this.priorityNum = TOURNAMENT_CONSTANTS.COMPLETED_TOURNAMENT;
                    this.room.priority = 5;
                }

                break;
            case TOURNAMENT_CONSTANTS.CANCELLED:
                this.statusSym.setState(this.room, _isMeInRoom, true);
                /* if (this._myTournament) {
                 var size = new cc.Size(500, 170);
                 this.tournamentRoomListManager.tourCommonPopup && this.tournamentRoomListManager.tourCommonPopup.removeFromParent(true);
                 this.tournamentRoomListManager.tourCommonPopup = new TourCommonPopup(message, tourName, size, this, true);
                 this.tournamentRoomListManager.tournamentLobby.addChild(this.tournamentRoomListManager.tourCommonPopup, GameConstants.popupZorder);
                 }*/
                if (this.priorityNum != TOURNAMENT_CONSTANTS.MY_TOURNAMENT){
                    this.priorityNum = TOURNAMENT_CONSTANTS.CANCELLED_TOURNAMENT;
                    this.room.priority = 6;
                }

                break;
        }
    },
    updateWaitPls: function (rmVar, val) {
        this.room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(rmVar, val));
    },
    setHelpText: function (helpStr) {
        if (!helpStr)
            return;
        var helpArr = helpStr.split(";");
        for (var i = 0; i < helpArr.length; i++) {
            var entryFeeHelpText, prizeHelpText, ineligibleText;
            switch (helpArr[i].split("-")[0].toString()) {
                case "e":
                    entryFeeHelpText = helpArr[i].split("-")[1];
                    this.room[TOURNAMENT_CONSTANTS.ENTRY_FEE_HELP_TXT] = entryFeeHelpText;
                    break;
                case "p":
                    prizeHelpText = helpArr[i].split("-")[1];
                    this.room[TOURNAMENT_CONSTANTS.PRIZE_HELP_TXT] = prizeHelpText;
                    break;
                case "i":
                    ineligibleText = helpArr[i].split("-")[1];
                    this.room[TOURNAMENT_CONSTANTS.INELIGIBLE_HELP_TXT] = ineligibleText;
                    break;
            }
        }
        //this.addQuesMark();
    },
    showToolTip: function (bool, msg, target, size) {
        var event = new cc.EventCustom("QUESTION_MARK_EVENT");
        event.setUserData([bool, msg, target, size]);
        cc.eventManager.dispatchEvent(event);
    },
    showToolTipJoin: function (bool, msg, target, size) {
        var event = new cc.EventCustom("STATUS_SYMBOL_EVENT");
        event.setUserData([bool, msg, target, size]);
        cc.eventManager.dispatchEvent(event);
    },
    addQuesMark: function (pos, msg, context) {
        var QuesSprite = cc.Sprite.extend({
            _allTourRoomListStrip: null,
            _name: "QuesMark",
            _isEnabled: null,
            toolTipMsg: null,
            _hovering: null,
            ctor: function (pos, context) {
                this._super(spriteFrameCache.getSpriteFrame("toolTipButton.png"));
                this._allTourRoomListStrip = context;
                this._hovering = false;
                this.setPosition(pos);
                this.toolTipMsg = msg;
                this._isEnabled = false;
                // this.setRotation(-90);

                return true;
            },
            hover: function (bool) {
            }
        });
        var question = new QuesSprite(pos, this);
        this.addChild(question, 3);
        cc.eventManager.addListener(this.tournamentRoomListManager.trnmtRoomListManagerMouseListener.clone(), question);
    },
    addNewTag: function () {
        var newTag = new cc.Sprite(spriteFrameCache.getSpriteFrame("TournamentNew.png"));
        newTag.setAnchorPoint(0, 0.5);
        newTag.setPosition(cc.p(-3 * scaleFactor, this.height / 2));
        this.addChild(newTag, 3);
    }
});
AllTournamentRoomListStrip.prototype.initBar = function (widthArray, room, index, tournamentRoomListManager) {
    this.tournamentRoomListManager = tournamentRoomListManager;
    this._name += " " + index;

    var fontSize = 25 * scaleFactor;

    if (room.getVariable(TOURNAMENT_CONSTANTS.NEW)) {
        this.addNewTag();
    }

    var tourName = new cc.LabelTTF(room.getVariable("tournamentName").value, "RobotoRegular", fontSize);
    tourName.setDimensions(cc.size(300 * scaleFactor, this.height));
    tourName.setAnchorPoint(0, 0.5);
    tourName.setScale(0.5);
    tourName.setName("tourName");
    tourName.setPosition(cc.p(widthArray[0], this.height / 2));
    tourName.setColor(cc.color(0, 0, 0));
    this.addChild(tourName);

    // Chip|Ticket,10|1:KPR Cash Card 50"
    var entryFeeArr = this.tournamentRoomListManager.getEntryFeeType(room);
    if (entryFeeArr[0].toLowerCase() != "free")
        entryFeeArr[0] = "`" + entryFeeArr[0];

    room["DisplayEntryFee"] = entryFeeArr;
    var entryFeeValue = new cc.LabelTTF(entryFeeArr[0], "RupeeFordian", fontSize - 1);
    entryFeeValue.setScale(0.5);
    entryFeeValue.setAnchorPoint(0, 0.5);
    entryFeeValue.setName("entryFee");
    entryFeeValue.setPosition(cc.p(widthArray[1], this.height / 2));
    entryFeeValue.setColor(cc.color(0, 0, 0));
    this.addChild(entryFeeValue);

    if (room["EntryFeeText"]) {
        var pos = cc.p(entryFeeValue.x + entryFeeValue.width / 2 + 10 * scaleFactor, entryFeeValue.height / 2 + 5 * scaleFactor);
        this.addQuesMark(pos, room["EntryFeeText"], this);
    }


    var prizeStr = this.tournamentRoomListManager.getPrizeType(room);
    if (prizeStr.length > 20) {
        prizeStr = prizeStr.substring(0, 20);
    }
    var przLabel = new cc.LabelTTF('`' + prizeStr, "RupeeFordian", fontSize - 1);
    przLabel.setAnchorPoint(0, 0.5);
    przLabel.setPosition(cc.p(widthArray[2], this.height / 2));
    przLabel.setName("prizeValue");
    przLabel.setScale(0.5);
    przLabel.setColor(cc.color(0, 0, 0));
    this.addChild(przLabel, 2);

    if (room["PrizeText"]) {
        var pos = cc.p(przLabel.x + przLabel.width / 2 + 10 * scaleFactor, this.height / 2 + 5 * scaleFactor);
        this.addQuesMark(pos, room["PrizeText"], this);
    }


    var joinedStr = room.getVariable("regNum").value + "/" + room.getVariable("maxRegNum").value;
    var prizeFont = fontSize;
    if (joinedStr.length > 10) {
        prizeFont -= 2;
    }
    this.joinedValue = new cc.LabelTTF(joinedStr, "RobotoRegular", prizeFont);
    this.joinedValue.setScale(0.5);
    this.joinedValue.setAnchorPoint(0, 0.5);
    this.joinedValue.setName("joined");
    this.joinedValue.setPosition(cc.p(widthArray[3], this.height / 2));
    this.joinedValue.setColor(cc.color(0, 0, 0));
    this.addChild(this.joinedValue);

    if (!room["StartTimeInUIFormat"]) {
        this.tournamentRoomListManager.startTimeInUIFormat(room);
    }
    var starttimeValue = new cc.LabelTTF(room.StartTimeInUIFormat, "RobotoRegular", fontSize);  // roomInfo.mMaxPlayers
    starttimeValue.setScale(0.5);
    starttimeValue.setAnchorPoint(0, 0.5);
    starttimeValue.setName("startTime");
    starttimeValue.setPosition(cc.p(widthArray[4], this.height / 2));
    starttimeValue.setColor(cc.color(0, 0, 0));
    this.addChild(starttimeValue);

    this.detailSprite = new DetailSprite("Details", "RobotoRegular", fontSize, this);
    this.detailSprite.setPosition(cc.p(700 * scaleFactor, this.height / 2));
    this.addChild(this.detailSprite);

    cc.eventManager.addListener(this.tournamentRoomListManager.trnmtRoomListManagerMouseListener.clone(), this.detailSprite);
    cc.eventManager.addListener(this.tournamentRoomListManager.trnmtRoomListManagerTouchListener.clone(), this.detailSprite);
    this.detailSprite.grandParent = this;

    this.statusSym = new StatusSymbol(this);
    this.statusSym.setPosition(cc.p(widthArray[5] + 20 * scaleFactor, this.height / 2));
    this.addChild(this.statusSym, 2);

    if (this.statusSym.statusBtn) {
        cc.eventManager.addListener(this.tournamentRoomListManager.trnmtRoomListManagerMouseListener.clone(), this.statusSym.statusBtn);
        cc.eventManager.addListener(this.tournamentRoomListManager.trnmtRoomListManagerTouchListener.clone(), this.statusSym.statusBtn);
    }
    this.statusSym.statusBtn.grandParent = this;

    this.roomId = room.id;
    var myTournament = this._myTournament || false;
    this.statusSym && this.statusSym.setState(room, myTournament, true);
    this.status = this.statusSym.status; // After changing register to join
    // this.detailSprite.status = this.status;

};
AllTournamentRoomListStrip.prototype.createSpriteWithOver = function (spriteName, spriteOverName, buttonName, pos) {
    var Sprite = cc.Sprite.extend({
        spriteOverName: null,
        spriteName: null,
        _isEnabled: null,
        _hovering: null,
        ctor: function (spriteName, spriteOverName, buttonName, pos) {
            this._super(spriteFrameCache.getSpriteFrame(spriteName + ".png"));
            this.spriteName = spriteName;
            this.spriteOverName = spriteOverName;
            this._hovering = false;

            pos && this.setPosition(pos);
            if (buttonName.split("_")[1] == "Enable")
                this._isEnabled = true;
            this._isEnabled = false;

            this.setName(buttonName.split("_")[0]);

            return true;
        },
        hover: function (bool) {
            if (this._isEnabled) {
                bool ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteOverName + ".png")) :
                    this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame(this.spriteName + ".png"));
            }
        },
        pauseListener: function (bool) {
            this._isEnabled = !bool;
        },
        hide: function (bool) {
            this.setVisible(!bool);
            this._isEnabled = !bool;
        }
    });
    return new Sprite(spriteName, spriteOverName, buttonName, pos);
};
AllTournamentRoomListStrip.prototype.setSelected = function (bool) {
    if (bool) {
        this.tournamentRoomListManager.selectedAllTourRoomId = this.roomId;
    }
    bool && (this.setBackGroundColor(this._selectedColor), this._isSelected = !0) || (this.setBackGroundColor(this._defaultColor), this._isSelected = !1);
};

var DetailSprite = cc.LabelTTF.extend({
    _parentClass: null,
    _isEnabled: null,
    _hovering: null,
    ctor: function (LabelName, fontFamily, fontSize, context) {
        this._super(LabelName, fontFamily, fontSize);
        this._parentClass = context;
        this.setName(LabelName);
        this.setScale(0.5);
        this.setColor(cc.color(191, 106, 1));
        this.setCascadeColorEnabled(true);
        this._hovering = false;
        this._isEnabled = true;

        var line = new cc.Sprite(spriteFrameCache.getSpriteFrame("horizontalHair.png"));
        line.setColor(cc.color(255, 255, 255));
        line.setScaleX(1.1);
        line.setPosition(cc.p(40 * scaleFactor, 0));
        this.addChild(line, 1);

        return true;
    },
    hover: function (bool) {
        bool ? this.setColor(cc.color(0, 0, 0)) :
            this.setColor(cc.color(191, 106, 1));
    },
    pauseListener: function (bool) {
        this._isEnabled = !bool;
    }
});

AllTournamentRoomListStrip.ALREADY_JOIN_ROOM = null;

/*RoomListStrip.prototype._joinRoom = function () {
 var room = sfsClient.getRoomById(this.tableId);
 if (room) {
 this.joinRoomHandler(room);
 } else {
 var dataObj = {cId: this.commonId};
 sfsClient.sendReqFromLobby(SFSConstants.CMD_ROOM_JOIN_REQUEST, dataObj);
 }
 cc.log("RoomListStrip Join Room");
 };*/
/*
 AllTournamentRoomListStrip.prototype.joinBtnHandler = function () {
 cc.log("Join Req");
 var status = this.status;
 var tRoomId = this.roomId;
 if (status == TOURNAMENT_CONSTANTS.STATUS_JOIN || status == TOURNAMENT_CONSTANTS.STATUS_WAITING) {

 var reqObj = {};
 /!*if (tRoomId != -1)
 reqObj["feeType"] = "Chip";*!/
 sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.PLAYER_ELIGIBLE_REQ, reqObj, tRoomId);
 }
 /!*else if (status == TOURNAMENT_CONSTANTS.STATUS_QUIT) {
 sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.PLAYER_ELIGIBLE_REQ, reqObj, tRoomId);
 } else if (status == TOURNAMENT_CONSTANTS.STATUS_TAKESEAT) {
 sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.PLAYER_ELIGIBLE_REQ, reqObj, tRoomId);
 }*!/
 };
 AllTournamentRoomListStrip.prototype.quitBtnHandler = function () {

 this.tournamentRoomListManager.processQuitPopup(this.roomId);
 };
 AllTournamentRoomListStrip.prototype.takeSeatBtnHandler = function () {
 cc.log("TakeSeat Req");
 var status = this.status;
 var tRoomId = this.roomId;
 var reqObj = {};
 if (tRoomId != -1)
 reqObj["feeType"] = "Chip";
 sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_TAKESEAT_REQUEST, reqObj, tRoomId);
 };
 AllTournamentRoomListStrip.prototype.waitingBtnHandler = function () {
 // this.watchReq();
 cc.log("Waiting Req");
 };
 AllTournamentRoomListStrip.prototype.detailsBtnHandler = function () {
 cc.log("Details Req");
 //closeDetailHandler(null);
 var tRoomId = this.roomId;
 var reqObj = {};
 if (tRoomId != -1)
 reqObj["roomId"] = tRoomId;
 sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.DETAIL_INFO, reqObj, tRoomId);
 };
 */