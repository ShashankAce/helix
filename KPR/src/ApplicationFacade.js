"use strict";
var sfsClient = null;
var rummyData = null;
var connectionConfig = null;
var ApplicationFacade = cc.Scene.extend({
//////////////////////////////////
    _name: "ApplicationFacade",
    _lobby: null,
    _controlAndDisplay: null,
    _gameMap: null,
    _gameMapArray: null,
    _tournamentMap: null,
    _activeTableId: null,
    _maxJoinRooms: null,
    _maxRooms: null,
    _lobbySeatObject: null,
    _playerId: null,
    _myUName: null,
    _myId: null,
    _mixpanelToggle: null,
    _currScr: null,
    _tourOpenRooms: null,
    _tempJoinRooms: null,
    _disconnectTime: null,
    _connectionChecker: null,
    _httpStatus: null,
    _isLogout: null,
    _isLogoutRequested: null,
    _myIdName: null,
    _parentRoomIds: null,
    baseUrl: null,
    // _tourRoomLeaveAllowed : null,

    ctor: function () {
        this._super();

        this._gameMap = {};
        this._gameMapArray = [];
        this._lobbySeatObject = {};
        this._parentRoomIds = {};
        this._mixpanelToggle = false;
        this._maxJoinRooms = 3;
        this._maxRooms = 5;
        this._tourOpenRooms = [];
        this._tempJoinRooms = [];
        this._myId = -1;
        this._disconnectTime = 0;
        this._httpStatus = "";
        this._isLogout = false;
        this._isLogoutRequested = false;
        this._myIdName = "";
        this.baseUrl = "https://www.khelplayrummy.com";

        sfsClient = new SFS2XClient();

        this.initControlAndDisplay();
        this.initLobby();
        this.reportProblem = new ReportProblem();

        this.background = new cc.Sprite(res.table.background13card);
        this.background.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(this.background, 3, "background");
        this.background.setVisible(false);
        this.background.setScale(1.01);

        var urlParams = this.parseURLParams(window.location.href);
        var configPath = "Configuration/ConnectionConfig.json?v=" + new Date().getTime();
        cc.loader.loadJson(configPath, function (error, data) {
            if (!error) {
                this.saveConfiguration(data);

                if (urlParams && urlParams["paramString"]) {
                    var baseUrl = connectionConfig.webServerPath;

                    var paramString = urlParams["paramString"][0];
                    paramString = decodeURIComponent(paramString);
                    var dataUrl = baseUrl + "welcomeCocosPlayer.action?paramString=" + paramString;
                    AjaxCommunication.requestUrl(dataUrl, "GET", function (resp) {

                        rummyData = new RummyData(resp);
                        user = rummyData.userName;
                        this.initGame(resp);

                    }.bind(this));
                }
                else if (urlParams && urlParams["playerId"]) {
                    cc.loader.loadJson("Configuration/RummyData.json", function (error, resp) {
                        rummyData = new RummyData(resp);
                        user = rummyData.userName;
                        rummyData.playerId = urlParams["playerId"][0];
                        rummyData.userName = rummyData.nickName = urlParams["nickName"][0];

                        this.initGame(resp);
                    }.bind(this));
                }
                /*else {
                 cc.loader.loadJson("Configuration/RummyData.json", function (error, resp) {
                 rummyData = new RummyData(resp);
                 this.initGame(resp);
                 }.bind(this));
                 }*/
            }
        }.bind(this));

        return true;
    },
    initGame: function (resp) {
        if (resp.nickName != "")
            this.initSocketConnection();
        else {
            this.requestNickName();
            return;
        }
        this._isLogout = false;
        this._currScr = this._lobby;
        this._lobby.initLobby();
        this.reportProblem.init();
        this.initLobbyBanner();
    },
    initLobbyBanner: function () {
        if (rummyData['rummyPlaceHolders'] && rummyData['rummyPlaceHolders']['lobbyLoad'] && rummyData['rummyPlaceHolders']['lobbyLoad']['placeHolder']) {
            var lobbyLoadBanner = new LobbyLoadBanner(rummyData['rummyPlaceHolders']['lobbyLoad']['placeHolder']);
            lobbyLoadBanner.setPosition(cc.p(cc.winSize.width / 2, cc.winSize.height / 2));
            this.addChild(lobbyLoadBanner, 45);
        }
    },
    handleLoginResp: function (dataObj) {
        this._myId = -1;
        this._playerId = dataObj["playerId"];
        this._myUName = dataObj["uName"];
        this._myIdName = this._playerId + ":" + this._myUName;
        this.initConnectionChecker();
    },
    saveConfiguration: function (data) {
        connectionConfig = new ConnectionConfig(data);
    },
    requestNickName: function () {
        var nickName = new NickNameLayer(this);
        this.addChild(nickName, 50);
    },
    initSocketConnection: function () {

        this._myUName = rummyData.nickName;
        this._myId = parseInt(rummyData.playerId);

        // this._lobby.initLobby();
        sfsClient.initClient(connectionConfig.ip, connectionConfig.port);

    },
    reportProblemHandler: function () {
        if (this._currScr._roomId == 0) {
            this.reportProblem.show("Lobby", "-");
        } else {
            this.reportProblem.show(this._currScr._serverRoom.getVariable("gameType").value, this._currScr.roomId);
        }
    },
    updateReportProblem: function () {
        if (this._currScr._roomId == 0) {
            this.reportProblem.update("Lobby", "-");
        } else {
            this.reportProblem.update(this._currScr._serverRoom.getVariable("gameType").value, this._currScr.roomId);
        }
    },
    parseURLParams: function (url) {
        // this.baseUrl = rummyData.logoPath || (url.split("/")[0] + url.split("/")[1] + url.split("/")[2]);
        this.baseUrl = (url.split("/")[0] + url.split("/")[1] + "//" + url.split("/")[2]);
        var queryStart = url.indexOf("?") + 1,
            queryEnd = url.indexOf("#") + 1 || url.length + 1,
            query = url.slice(queryStart, queryEnd - 1),
            pairs = query.replace(/\+/g, " ").split("&"),
            parms = {},
            i, n, v, nv;

        if (query === url || query === "") return;

        for (i = 0; i < pairs.length; i++) {
            nv = pairs[i].split("=", 2);
            n = decodeURIComponent(nv[0]);
            v = decodeURIComponent(nv[1]);

            if (!parms.hasOwnProperty(n)) parms[n] = [];
            parms[n].push(nv.length === 2 ? v : null);
        }
        return parms;

    },
    initLobby: function () {
        this._lobby = new Lobby();
        this.addChild(this._lobby, 0);
        this._activeTableId = 0;
    },
    handleOpenThumbnail: function (room, dataObj) {
        room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable("gameType", "Point-Joker"));
        room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable("isDummyRoom", true));
        room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(SFSConstants.PARENT_EXT_ROOM_ID, room.id));
        room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(SFSConstants.DATA_ROOMID, room.id));
        room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable("maxPlayers", 6));
        room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable("numOfCard", 13));
        if (dataObj) {
            console.log("level3 :" + dataObj["currLevel"]);

            room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL, dataObj["currLevel"]));
            room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL, dataObj["maxLevel"]));
        }
        this.initTable(room);
        this._parentRoomIds[room.id + ""] = room.id;
    },
    handleCloseThumbnail: function (room) {
        var gameRoom = this.getGameRoomByParentId(room.id);
        if (gameRoom)
            gameRoom.leaveRoomByMyself();
    },
    handleClearTable: function (room, dataObj) {
        var gameRoom = this.getGameRoomByParentId(room.id);
        if (gameRoom) {
            gameRoom.leaveRoomByMyself();
        }
        this.handleOpenThumbnail(room);
    },
    checkMaxTable: function () {
        var roomArray = Object.keys(this._gameMap);
        if (roomArray.length < this._maxJoinRooms) {
            return true;
        }
        this.showMaxJoinPop();
        return false;
    },
    getGameRoomByParentId: function (parentId) {
        var gameRoom;
        for (var key in this._gameMap) {
            gameRoom = this._gameMap[key];
            if (gameRoom) {
                if (gameRoom._serverRoom.groupId == "tournamentGameRoom" && parseInt(gameRoom._serverRoom.getVariable(SFSConstants.PARENT_EXT_ROOM_ID).value) == parentId) {
                    return gameRoom;
                }
                if (gameRoom._serverRoom.groupId == "tournamentRoom" && parseInt(gameRoom._serverRoom.id) == parentId) {
                    return gameRoom;
                }
            }
        }
        return null;
    },
    getGameRoomById: function (rid) {
        var roomScr;
        for (var key in this._gameMap) {
            roomScr = this._gameMap[key];
            if (roomScr && roomScr.roomId == rid)
                return roomScr;
        }
        return null;
    },
    setActiveGameRoom: function (curntRoom) {
        var rmId = curntRoom.id;
        var gameRoom = this.getGameRoomById(rmId);
        if (gameRoom) {
            this._currScr = gameRoom;
            return true;
        }
        var pRmId = curntRoom.getVariable(SFSConstants.PARENT_EXT_ROOM_ID).value;
        var oldRmId = this._parentRoomIds[pRmId];
        if (oldRmId != 0) {
            gameRoom = this.getGameRoomByParentId(pRmId);
            if (gameRoom) {
                this._controlAndDisplay.removeRoomTab(gameRoom.tableId);
                delete this._parentRoomIds[pRmId];
                return false;
            }
        }
        return false;
    },
    initTable: function (room) {

        var gameRoom = this.getGameRoomById(room.id);

        if (gameRoom && gameRoom._dummyObj) {
            gameRoom.updateRoom(room);
            seatObj = this._lobbySeatObject[room.id + ""];
            if (seatObj) {
                seatObj.wasDummyRoom = true;
                gameRoom._joinRoom(seatObj);
                this._lobbySeatObject[room.id + ""] = null;
            }
            return;
        }
        if (!this.isNextRoomJoin(room))
            return;

        var tableId = room.id,
            game = null;

        if (room.groupId == "gameRoom" || room.groupId == "tournamentGameRoom" || room.groupId == "tournamentRoom") {

            if (this._gameMap[tableId]) {
                var table = this._gameMap[tableId];
                this._controlAndDisplay.removeRoomTab(tableId);
            }

            if (room.groupId == "gameRoom") {
                var gameType = room.variables.gameType.value;
                gameType = gameType.split("-")[0];
                if (gameType == "Pool" || gameType == "Deal") {
                    game = new PoolGame(room);
                } else {
                    game = new PointGame(room);
                }
            }
            else {
                if (this.setActiveGameRoom(room))
                    return;
                game = new TournamentBaseGame(sfsClient.getRoomById(tableId));
            }
            this._lobby.zIndex = 1;

            if (this._gameMap[game.tableId])
                delete this._gameMap[game.tableId];

            var ind = this._gameMapArray.indexOf(game.tableId);
            if (ind > -1)
                this._gameMapArray.splice(ind, 1);

            this._gameMap[game.tableId] = game;
            this._gameMapArray.push(game.tableId);

            cc.log("{gameMap :" + this._gameMap);
            this._activeTableId = room.id;
            this._currScr = game;
            var roomArray = Object.keys(this._gameMap);
            this.addChild(game, roomArray.length * 2 + 2);
            this.background.setVisible(true);
            this.background.setLocalZOrder(this._currScr.zIndex - 1);

            var seatObj = this._lobbySeatObject[tableId + ""];
            if (seatObj) {
                game._joinRoom(seatObj);
                this._lobbySeatObject[tableId + ""] = null;
            }

            this._lobby.myTablePanel && this._lobby.myTablePanel.initMyTablePanel();
            // this.updateReportProblem();

            //_contDispPanel.updateBgTab(true);
            //gameRoom._tabMc = _contDispPanel.addNewRoomTab(_gameRoomsMap.length, "Room" + room.name, rId, { totSeats:room.getVariable("maxPlayers").getValue() } );
            //_lobbyScr.upDateMyTable();
            //_contDispPanel.showlobbyBtn(true);

            // } else {
            //     this._gameMap[tableId].setLocalZOrder(roomArray.length + 1);
            //     this._activeTableId = tableId;
            // }
            this._controlAndDisplay.updateLobbyTab();
            this.activateListener(tableId);
        }
    },
    isNextRoomJoin: function (room) {

        if (!this._gameMapArray) // if game room not exist
            return false;

        if (this._gameMapArray.length >= this._maxRooms) // if game room full already 5 tables join
            return false;

        if (room.groupId == "tournamentGameRoom" || room.groupId == "tournamentRoom") // if game room not full and tournament room join
            return true;

        var gameRoom;
        var normalRoomJoins = 0;

        if (room.groupId == "gameRoom") {

            for (var key in this._gameMap) {
                gameRoom = this._gameMap[key];
                if (gameRoom && gameRoom._serverRoom.groupId == "gameRoom") {
                    normalRoomJoins++;
                    if (gameRoom._roomId == room.id) {
                        this.OnRoomLeft(gameRoom._roomId);
                        return true;
                    }
                }
            }
            if (normalRoomJoins < this._maxRooms)
                return true;
            else
                return false;
        }
        return false;
    },
    noRoomHandler: function (params) {

        /* var noRoom = new NoRoomPop(params.msg, this, true);
         this._lobby.addChild(noRoom, GameConstants.noBalPop, "noRoom");
         return;

         var GameRoom = this.getGameRoomById(params["roomId"]);
         cc.log("noRoomHandler", GameRoom, params["roomId"], params["seatNo"], params["str"]);
         if (params["str"] == "noRoom" && GameRoom) {
         GameRoom.setNoRoomPopUp();
         this.joinDummyRoom(params);
         return;
         }
         if (params["joinType"] == "instant") {
         // _instantPlayMap[dataObj.getLong("roomId")] = "instant";
         }*/

    },
    joinDummyRoom: function () {

    },
    removeTable: function (id) {

        this.removeChild(this._gameMap[id], true);
        delete this._gameMap[id];

        var index = this._gameMapArray.indexOf(id);
        this._gameMapArray.splice(index, 1);

        this._lobby.myTablePanel && this._lobby.myTablePanel.initMyTablePanel();

        var roomLength = Object.keys(this._gameMap);
        if (roomLength.length < 1) {
            this.switchScreen(0);
        } else {
            // var roomArray = Object.keys(this._gameMap);
            // this.switchScreen(this._gameMap[roomArray[roomArray.length - 1]]);
            this.switchScreen(this._gameMapArray[this._gameMapArray.length - 1]);
        }

        // this.updateReportProblem();
        this.updateExternalLobbyBalance();
    },
    removeRooms: function () {
        if (this._isRemoveAll)
            return;

        this.k = true;
        var gameRoom;
        if (this._gameMap) {
            for (var key in this._gameMap) {
                gameRoom = this._gameMap[key];
                gameRoom.stopAllTimers();
                gameRoom.removeFromParent(true);
                this.OnRoomLeft(gameRoom._roomId, true);
            }
        }
        this._gameMap = {};
        this._lobbySeatObject = {};
    },
    OnRoomLeft: function (rid, removeAll) {

        var roomScr;
        for (var key in this._gameMap) {
            roomScr = this._gameMap[key];
            if (roomScr && roomScr._roomId == rid)
                break;
            roomScr = null;
        }

        if (!roomScr)
            return;

        if (!removeAll && roomScr._serverRoom && roomScr._serverRoom.groupId == "tournamentGameRoom") {
            if (!roomScr._leaveTableByPlayer) {
                roomScr._isRoomActive = false;
                this._parentRoomIds[roomScr._serverRoom.getVariable(SFSConstants.PARENT_EXT_ROOM_ID).value + ""] = rid;
                // in case of tournament game we do not exit user
                return;
            }
        }

        if (this._gameMap[rid])
            applicationFacade._controlAndDisplay.removeRoomTab(rid);

        this.updateReportProblem();
        this.updateExternalLobbyBalance();
        roomScr = null;
    },
    initControlAndDisplay: function () {

        this._controlAndDisplay = new ControlAndDisplay();
        this.addChild(this._controlAndDisplay, 20);

    },
    setServerMsg: function (cmd, params) {
        this._controlAndDisplay.handleResp(cmd, params);
    },
    saveTournamentRoomIds: function () {
        this._tourOpenRooms = [];
        var gameRoom;
        for (var key in this._gameMap) {
            gameRoom = this._gameMap[key];
            if (gameRoom && gameRoom._serverRoom.groupId == "tournamentGameRoom") {
                this._tourOpenRooms.push(gameRoom.parentRoom.id);
            }
        }
    },
    activateListener: function (roomId) {
        if (roomId == 0)
            applicationFacade._lobby.pauseListener(false);
        else
            applicationFacade._lobby.pauseListener(true);

        for (var key in this._gameMap) {
            if (parseInt(key) == roomId)
                this._gameMap[key].pauseListener(false);
            else
                this._gameMap[key].pauseListener(true);
        }
    },
    handleLobbyExtensionResponse: function (cmd, room, params) {
        this._lobby.handleResp(cmd, room, params);
        if (cmd == SFSConstants.ROOM_JOIN_RESPONSE) {
            this.handleJoinRoomRequest(room, params);
        }
    },
    switchScreen: function (target) {

        var roomArray = Object.keys(this._gameMap),
            roomId = target,
            index = 2;

        if (roomId != parseInt(roomId, 10))
            throw new Error("Both radio can not be off. error-code:BC234");
        if (roomId != 0) {
            // gameRoom
            this._lobby.zIndex = 0;
            for (var key in this._gameMap) {
                if (parseInt(key) == roomId) {
                    this._gameMap[key].zIndex = (roomArray.length * 2 + 2);
                    this._currScr = this._gameMap[key];
                    this._currScr.setCurrentScreen();
                } else {
                    this._gameMap[key].zIndex = index;
                    index += 2;
                }
            }
            this.background.setVisible(true);
            this.background.setLocalZOrder(this._currScr.zIndex - 1);
            this._controlAndDisplay.lobbyTab.setDisabled(!1);
            this._activeTableId = roomId;
            this.activateListener(roomId);
        } else {
            // lobby
            for (var key in this._gameMap) {
                this._gameMap[key].zIndex = index;
                index += 2;
            }
            this._controlAndDisplay.lobbyTab.setDisabled(!0);
            this._lobby.setLocalZOrder(index);
            this._currScr = this._lobby;
            this._activeTableId = 0;
            this.activateListener(0);
            this.background.setVisible(false);
            // this.background.setLocalZOrder(this._lobby.zIndex - 1);
        }
    },
    handleLeaveTable: function (target) {
        var roomId = target._roomId;
        if (roomId != 0) {
            this._gameMap[roomId] && this._gameMap[roomId].leaveBtnHandler();
        }
    },
    handleJoinRoomRequest: function (params) {
        var gameR = this._gameMap[params.roomId];
        /*if (dataObj.getUtfString("str") == "noRoom" && gameR)
         {
         gameR.setNoRoomPopUp();
         joinDummyRoom();
         return;
         }

         if (dataObj.getUtfString("joinType") == "instant")
         {
         _instantPlayMap[dataObj.getLong("roomId")] = "instant";
         }
         //trace("gameR",gameR)
         if (!gameR)
         {
         if (dataObj.getLong("seatNo"))
         joinRoomFromLobby({ seatNo:dataObj.getLong("seatNo") },dataObj.getLong("roomId"));
         else
         {
         var sObj:SFSObject=new SFSObject();
         sObj.putUtfString("cmd","RoomJoin");
         sObj.putInt("id",dataObj.getLong("roomId"));
         lobbyCmdHandler(sObj);
         }
         }
         else
         {
         trace("dataObj.roomId",dataObj.getLong("roomId"),dataObj.getLong("seatNo"))
         if (dataObj.getLong("seatNo"))
         _lobbySeatObject[dataObj.getLong("roomId") + ""] = { seatNo:dataObj.getLong("seatNo") };
         var uid:int = ServerCommunication.getInstance()._sfsClient.mySelf.id;
         var room:Room= ServerCommunication.getInstance().getRoomById(dataObj.getLong("roomId"));
         if (room && (!room.getUserById(uid)))
         {
         ServerCommunication.getInstance().roomJoin(dataObj.getLong("roomId"));
         }
         else
         {
         if (dataObj.getLong("seatNo"))
         {
         var sitObj:SFSObject= new SFSObject();
         sitObj.putLong("seatNo",dataObj.getLong("seatNo"));
         gameR.joinSeat(sitObj);
         }
         _contDispPanel.joinRoomHandler(dataObj.getLong("roomId"));
         }
         }*/
    },
    handleTableExtensionResponse: function (cmd, room, params) {
        cc.log("handleTableExtensionResponse", cmd);
        if (room) {
            var tableId = room.id;
            if (this._gameMap[tableId]) {
                this._gameMap[tableId].handleResp(cmd, room, params);
            }
        }
    },
    handleTournamentRoomResp: function (cmd, room, params) {
        var gameRoom = this.getGameRoomByParentId(room.id);
        if (gameRoom)
            gameRoom.handleResp(cmd, room, params);
    },
    handleGameRoomChat: function (rid, sender, msg) {
        if (rid) {
            if (this._gameMap[rid]) {
                this._gameMap[rid].handleChat(sender, msg);
            }
        }
    },
    handleTimeUpdate: function () {
        //todo update detail timer
        // _lobbyScr._flrPrser._trnmntDynamicTab._trnmntRmListMgr.updateRemainingTime();
        if (this._gameMap) {
            var roomArray = Object.keys(this._gameMap);
            for (var j = 0; j < roomArray.length; ++j) {
                if (roomArray[j])
                    if (this._gameMap[roomArray[j]] && (this._gameMap[roomArray[j]]._serverRoom.groupId == "tournamentGameRoom" || this._gameMap[roomArray[j]]._serverRoom.groupId == "tournamentRoom"))
                        this._gameMap[roomArray[j]].updateLevelTimer();
            }
        }
    },
    handleCNDExtensionResponse: function (cmd, room, params) {
        this._controlAndDisplay.handleResp(cmd, room, params);
    },
    handleTournamentExtensionResponse: function (cmd, room, params) {
        cc.log("tournament cmd: ", cmd);
        cc.log("param: ", params);
        if (cmd == SFSConstants.CMD_UPDATE_BAL)
            this.handleLobbyExtensionResponse(cmd, room, params);
        else if (cmd == SFSConstants.OPEN_THUMBNAIL)
            this.handleOpenThumbnail(room, params);
        else if (cmd == SFSConstants.REMOVE_THUMBNAIL)
            this.handleCloseThumbnail(room);
        else if (cmd == SFSConstants.LEVEL_CMPLT_STATUS ||
            cmd == SFSConstants.UPDATE_PLAYER_RANK ||
            cmd == SFSConstants.IS_REJOIN ||
            cmd == SFSConstants.TOURNAMENT_RE_JOIN_RESP ||
            cmd == SFSConstants.DROP_WAITING ||
            cmd == SFSConstants.TEA_TIME ||
            cmd == SFSConstants.ADD_REBUY_CASH_RESP ||
            cmd == SFSConstants.UPDATE_DEAL ||
            cmd == SFSConstants.LEVEL_EXTRA_TIME_NOTIFICATION ||
            cmd == SFSConstants.TOURNAMENT_INFO) {
            this.handleTournamentRoomResp(cmd, room, params);
        }
        else if (cmd == SFSConstants.LEVEL_END_TIME)
            room._setVariable(new SFS2X.Entities.Variables.SFSRoomVariable(SFSConstants.LEVEL_END_TIME, params["time"]));
        else if (cmd == SFSConstants.CLEAR_TABLE)
            this.handleClearTable(room, params);
        else
            this._lobby.tournamentLobby.handleResp(cmd, room, params);

    },
    joinRoomFromLobby: function (room) {
        if (room.groupId == "tournamentGameRoom") {
            var pRmId = room.getVariable(SFSConstants.PARENT_EXT_ROOM_ID).value;
            if (this._gameMap[pRmId]) {
                this._controlAndDisplay.removeRoomTab(pRmId);
            }
        }
        this.initTable(room);
    },
    joinRoomFromMiniRoom: function (room, seatId) {
        var gameRoom;
        var isRoomJoinable;
        var uid = sfs.mySelf.id;
        if (room) {
            var u = room.getUserById(uid);
            if (!u) {
                var sentObj = {
                    cmd: "RoomJoin",
                    id: room.id
                };
                isRoomJoinable = this.lobbyCmdHandler(sentObj);
                if (isRoomJoinable)
                    this._lobbySeatObject[room.id + ""] = seatId;
            }
            else {
                // show existing table new table is not opened
                if (this._gameMap[room.id]) {
                    gameRoom = this._gameMap[room.id];
                    gameRoom._joinRoom(seatId);
                    this._controlAndDisplay.joinRoomHandler(room.id); //screen is switched to new table
                }
            }
        }
        //this.initTable(room, seatId);
        //!seatId && sfsClient.joinRoom(roomBar, seatId) || sfsClient.joinRoom(roomBar);
        // this.initTable(roomBar, playerPosition);
    },
    lobbyCmdHandler: function (obj, seatId) {
        cc.log("lobbyCmdHandler", this._maxJoinRooms);
        var roomArray = Object.keys(this._gameMap);
        if (roomArray.length < this._maxJoinRooms) {
            var cmd = obj.cmd;
            var id = obj.id;
            var commonId = obj.commonId;
            if (cmd == "RoomJoin") {
                sfsClient.joinRoom(id, seatId);
            }
            if (cmd == "watch") {
                var dataObj = {};
                dataObj[SFSConstants.FLD_COMMON_ID] = commonId;
                sfsClient.sendReqFromLobby(SFSConstants.CMD_WATCH, dataObj);
            }
            if (cmd == "DisConnected") {
                //initDummyGameRoom(obj.r);
            }
            return true;
        } else {
            this.showMaxJoinPop();
        }
        return false;
    },
    takeSeatFromLobby: function (roomId) {
        var gRoom = this.getGameRoomByParentId(roomId);
        if (gRoom) {
            this._controlAndDisplay.joinRoomHandler(gRoom._roomId);
            return true;
        }
        var roomArray = Object.keys(this._gameMap);
        if (roomArray.length >= this._maxRooms) {
            this.showMaxJoinPop();
            return true;
        }
        return false;
    },

    updateExternalLobbyBalance: function () {
        window.parent.postMessage("updatePlayerBalance", "*");
    },
    showMaxJoinPop: function () {
        this.maxJoinTablePopup && this.maxJoinTablePopup.removeFromParent(true);
        this.maxJoinTablePopup = new MaxJoinTablePopup(this);
        this._currScr.addChild(this.maxJoinTablePopup, GameConstants.popupZorder, "maxJoinTablePopup");
        cc.log("Max Table Joined");
    },
    controlHandler: function (obj) {
        var cmd = obj.cmd;
        var id;
        if (cmd == "tabChange") {
            id = obj.id;
            cc.log("tabChange " + id);
            this.switchScreen(id);

            /*_currScr.visible = false;
             if (id <= 0)
             _currScr = _lobbyScr;
             else
             {
             _currScr = _gameRoomsMap[id - 1];
             _currScr.setDefaultBet();
             }
             _currScr.visible = true;
             if (obj.getBool("isPopup"))
             _currScr.leaveBtnHandler(null);
             */
        }
        if (cmd == "tabClose") {
        }
        if (cmd == "leaveRoom") {
            id = obj.rId;
            //_contDispPanel.tabClickHandler(null,id);
            // this.switchScreen(id);
        }
        if (cmd == "updatePlBalance") {
            /*_myCashBalance = Number(obj.cash);
             _myChipBalance = Number(obj.promo);
             if(_mixpanelToggle)
             GameMixPanel.getMixPanel().trackBalance(_myCashBalance);
             callExternalInterface();*/
            this.updateExternalLobbyBalance();
            return;
        }
        /*set lobby button visibility
         if (_currScr == _lobbyScr)
         {
         _contDispPanel.showlobbyBtn(false);
         }
         else
         {
         _contDispPanel.showlobbyBtn(true);
         }*/
        this.updateThumbnail();
    },
    disableRoomsChat: function (bool) {
        if (this._gameMap) {
            var roomArray = Object.keys(this._gameMap);
            for (var j = 0; j < roomArray.length; ++j) {
                /*todo if (roomArray[j])
                 roomArray[j].disableChat(bool);*/
            }
        }
    },
    setLobbyLoadTimer: function () {
        if (this._controlAndDisplay) {
            this._controlAndDisplay.setLoadTime();
        }
    },
    handleConnection: function (status) {
        if (status) {
            this._httpStatus = "success";
        }
        else {
            this._httpStatus = "error";
            this._isRemoveAll = false;
        }
        this._controlAndDisplay.setConnectionStatus(status);
        this.disableRoomsChat(status);
    },
    updateThumbnail: function () {
        if (this._gameMap) {
            var roomArray = Object.keys(this._gameMap);
            for (var j = 0; j < roomArray.length; ++j) {
                if (roomArray[j])
                    this._gameMap[roomArray[j]].setCurrentScreen();
            }
        }
    },
    instantPlayBtnHandler: function (obj) {
        var roomArray = Object.keys(this._gameMap);
        if (roomArray.length < this._maxJoinRooms)
            sfsClient.sendReqFromLobby("InstantPlay", obj);
        else {
            this.showMaxJoinPop();
        }
    },
    realPlayBtnHandler: function (sfsObj) {
        var roomArray = Object.keys(this._gameMap);
        if (roomArray.length < this._maxJoinRooms)
            sfsClient.sendReqFromLobby("playForReal", sfsObj);
        else
            this.showMaxJoinPop();
    },
    pauseListener: function (bool) {
        if (bool)
            cc.eventManager.pauseTarget(this, true);
        else cc.eventManager.resumeTarget(this, true);
    },
    logoutHandler: function () {
        this._isLogout = true;
        cc.eventManager.pauseTarget(this, true);
        this._controlAndDisplay.removeAllRoom();
        this._controlAndDisplay.updateLobbyTab();
        this._connectionChecker.stopTimer();
        this._controlAndDisplay.disconnection && this._controlAndDisplay.disconnection.removeFromParent(true);
        this.logoutPop && this.logoutPop.removeFromParent(true);
        this.logoutPop = new Logout();
        this.addChild(this.logoutPop, 80, "logoutPop");
    },
    onSuccesResponse: function (type) {
        var connectCode = type;
        if (this._httpStatus == connectCode)
            return;

        this._httpStatus = connectCode;
        this._controlAndDisplay.setConnectionStatus(true);
        this.sendConnectionOk();
    },
    onErrorResponse: function (type) {
        var connectCode = type;
        if (this._httpStatus == connectCode)
            return;
        this._httpStatus = connectCode;
        this._controlAndDisplay.setConnectionStatus(false);
        this._disconnectTime = this._connectionChecker._discType;
        //this.disableCards();
    },
    sendConnectionOk: function () {
        this._disconnectTime += ":" + this._connectionChecker._discTime * 1000;
        if (this._lobby) {
            var sfsObj = {};
            sfsObj["discData"] = this._disconnectTime;
            sfsClient.sendReqFromLobby(SFSConstants.CMD_CONNECTAGAIN, sfsObj);
            //todo this._lobby.filterParser._trnmntDynamicTab._trnmntRmListMgr.removeDetailPopUp();
        }
        if (this._gameMap) {
            var roomArray = Object.keys(this._gameMap);
            for (var j = 0; j < roomArray.length; ++j) {
                if (roomArray[j])
                    this._gameMap[roomArray[j]].sendConnectionOk(false);
            }
        }
    },
    initConnectionChecker: function () {
        if (this._connectionChecker) {
            if (!this._isLogout)
                this._connectionChecker.startTimer();
            return;
        }
        this._connectionChecker = new ConnectionChecker();
        this._connectionChecker.setParameter(rummyData.preConnection);
    }
});