/**
 * Created by stpl on 11/30/2016.
 */
var OpenDeck = cc.Sprite.extend({
    _name: "OpenDeck",
    _gameRoom: null,
    _deckPanel: null,
    _openCrd: null,
    _openCrd2: null,
    _isEnabled: null,
    glowCard: null,
    glowTag: 500,
    _hovering: null,
    ctor: function (context) {
        this._deckPanel = context;
        this._gameRoom = context._gameRoom;
        this._isEnabled = false;
        this._hovering = false;
        this._super(spriteFrameCache.getSpriteFrame("selected_state.png"));

        var label = new cc.LabelTTF("Open Deck", "RobotoRegular", 15 * scaleFactor);
        label.setScale(0.8);
        label.setPosition(cc.p(this.width / 2, -9 * scaleFactor));
        this.addChild(label, 1);

        this.glowCard = new cc.Sprite(spriteFrameCache.getSpriteFrame("CardGlow.png"));
        this.glowCard.setScale(1.05);
        this.glowCard.setPosition(cc.p(this.width / 2 + 2.5 * scaleFactor, this.height / 2));
        this.glowCard.setVisible(false);
        this.addChild(this.glowCard, -1);

        return true;
    },
    glowEnabled: function (bool) {
        cc.log("OpenDeckGlowActive->" + bool);
        if (bool) {
            this.glowCard.setVisible(true);
            var glowSequence = cc.sequence(cc.fadeIn(0.5), cc.fadeOut(0.5));
            glowSequence.tag = this.glowTag;
            this.glowCard.runAction(glowSequence.repeatForever());

        } else {
            this.glowCard.stopActionByTag(this.glowTag);
            this.glowCard.setVisible(false);
        }
    },
    setOpenDeckCards: function (cardsStr) {

        if (!cardsStr && this._gameRoom._serverRoom.getVariable(SFSConstants.OPEN_DECK_CARD)) {
            cardsStr = this._gameRoom._serverRoom.getVariable(SFSConstants.OPEN_DECK_CARD).value;
        }

        if (this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD)) {
            this._wildCardName = this._gameRoom._serverRoom.getVariable(SFSConstants._WILDCARD).value;
        }
        var cardSpr, cardSpr1;

        if (!cardsStr)
            return;

        var cardsArr = cardsStr.split(",");  // istcard,2nd card
        if (cardsArr.length == 0)
            return;

        if (cardsArr != null) {
            this.removeAllCard();
        }
        for (var i = 0; i < cardsArr.length; i++) {
            if (cardsArr[i] != "null") {
                cardSpr = new CardModel(cardsArr[i]);
                cardSpr.setScale(0.87);
                var pos = this.getPosition();
                cardSpr.setPosition(cc.p(pos.x - 1 * scaleFactor, pos.y + 2 * scaleFactor));
                cardSpr._cardName = "openCrd";
                cardSpr.setVisible(this.isVisible());
                if (i == 0) {
                    this._openCrd = cardSpr;
                }
                if (i == 1) {
                    this._openCrd2 = cardSpr;
                }
                this._deckPanel.addChild(cardSpr, 5 - i);
            }
        }

        if (this._openCrd) {
            if (this._gameRoom.totCards == 21) {
                this._openCrd.setJoker(this._wildCardName, this._gameRoom._jokerSymVisble);
            }
            else {
                if (this.wildcard && (this.wildcard._cardNum == 0) && (this._openCrd._cardNum == 1))
                    this._openCrd._isJoker = true;
                if ((this._openCrd._cardNum == 0) || (this.wildcard && this._openCrd._cardNum == this.wildcard._cardNum))
                    this._openCrd._isJoker = true;
            }
        }
    },
    addCardAfterDiscard: function (cardName) {
        /*var card = new CardModel(cardName);
         card.setScale(0.9);
         card.setPosition(cc.p(this.width / 2, this.height / 2));
         this.addChild(card);


         var moveA = new cc.MoveTo(0.2, this.deckPanel.openDeck.getPosition());
         var scaleA = new cc.ScaleTo(0.2, 0.7, 0.7);
         discardedCard.runAction(
         cc.sequence(
         cc.spawn(moveA, scaleA),
         cc.callFunc(function () {
         this.deckPanel.openDeck.addCardAfterDiscard(discardedCard.getName());
         discardedCard.removeFromParent(!0);
         }, this)
         )
         );*/
    },
    removeSingleCard: function (cardName) {
        if (this._openCrd && this._openCrd._name == cardName) {
            this._deckPanel.removeChild(this._openCrd, true);
            this._openCrd = null;
            return;
        }
        if (this._openCrd2 && this._openCrd2._name == cardName) {
            this._deckPanel.removeChild(this._openCrd2, true);
            this._openCrd2 = null;
        }
    },
    removeAllCard: function () {
        if (this._openCrd) {
            this._deckPanel.removeChild(this._openCrd, true);
            this._openCrd = null;
        }
        if (this._openCrd2) {
            this._deckPanel.removeChild(this._openCrd2, true);
            this._openCrd2 = null;
        }
    },
    pauseListener: function (bool) {
        this._isEnabled = !bool;
    },
    hide: function (bool) {
        this.setVisible(!bool);
        this._openCrd && this._openCrd.setVisible(!bool);
        this._openCrd2 && this._openCrd2.setVisible(!bool);
        if (bool == true) {
            this._isEnabled = !bool;
        }
    },
    hover: function (bool) {

    }
});