/**
 * Created by stpl on 11/24/2016.
 */
var Winner = cc.Layer.extend({
    _gameRoom: null,
    ctor: function (params, context) {
        this._super();
        this._gameRoom = context;

        // params = {"id": 11, "amt": 8.5, "gameDuration": 118204, "rejoinStr": "11:0:0.0"};
        this.ccTimeOutManager = new CCTimeOutManager();

        var glow = new cc.Sprite(res.winner.winnerGlow);
        glow.setPosition(cc.p(this.width / 2 + 1 * scaleFactor, this.height / 2));
        glow.setOpacity(130);
        glow.setScale(1.1, 1);
        this.addChild(glow);


        var winnerIcon = new cc.Sprite(res.winner.winnerLogo);
        winnerIcon.setPosition(cc.p(500 * scaleFactor, 480 * scaleFactor));
        winnerIcon.setScale(0.90);
        this.addChild(winnerIcon, 5);

        this.textField = new WinnerTextField(params, this._gameRoom);
        this.addChild(this.textField, 2);

        SoundManager.removeSound(SoundManager.validshow);
        SoundManager.removeSound(SoundManager.wrongShow);

        if (this._gameRoom._myId != -1) {

            this.yesButton = new CreateBtn("winnerYesBtn", "winnerYesBtnOver");
            this.yesButton.setName("WinnerYes");
            this.yesButton.setPosition(cc.p(this.width / 2 + 80 * scaleFactor, 180 * scaleFactor));
            this.yesButton.setVisible(true);
            this.addChild(this.yesButton, 3);

            this.noButton = new CreateBtn("winnerNoBtn", "winnerNoBtnOver");
            this.noButton.setName("WinnerNo");
            this.noButton.setPosition(cc.p(this.width / 3 + 100 * scaleFactor, 180 * scaleFactor));
            this.noButton.setVisible(true);
            this.addChild(this.noButton, 3);

            SoundManager.playSound(SoundManager.finalwinningifme, false);

            if ('mouse' in cc.sys.capabilities) {
                this.initMouseListener();
            }
            this.initTouchListener();
        }

        this.setTimer();
        // this.createFireWork();

        return true;
    },
    createFireWork: function () {
        var particle = new cc.ParticleSystem(res.winExp.winExp_plist);
        particle.setPosition(cc.p(this.width / 2, this.height / 2 + 20));
        this.addChild(particle);

    },
    setTimer: function () {

        this._gameRoom.result.timerText && this._gameRoom.result.timerText.removeFromParent(true);
        this._gameRoom.result.resultTimeText && this._gameRoom.result.resultTimeText.removeFromParent(true);

        var time = 7;
        this.ccTimeOutManager.scheduleInterval(xyz.bind(this), 1000, "winnerTimer");
        function xyz() {
            if (time > 1) {
                time = time - 1;
            } else {
                if (this._gameRoom._myId != -1 && this.yesButton.isVisible() && this.noButton.isVisible()) {
                    this.yesButton.hide(true);
                    this.noButton.hide(true);
                    this.textField.wMsg.setString("To play again, visit the lobby!!");
                }
                this.ccTimeOutManager.unScheduleInterval("winnerTimer");
            }
        }
    },
    initMouseListener: function () {
        var listener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    !target._hovering && target.hover(true);
                    target._hovering = true;
                    return true;
                }
                target._hovering && target.hover(false);
                target._hovering = false;
                return false;
            }.bind(this)
        });
        listener.retain();
        cc.eventManager.addListener(listener, this.yesButton);
        cc.eventManager.addListener(listener.clone(), this.noButton);
    },
    initTouchListener: function () {
        var listenerTouch = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                if (target._isEnabled) {
                    if (target.getName() == "WinnerYes") {
                        this.yesButtonOnClick();
                    } else if (target.getName() == "WinnerNo") {
                        this.noButtonOnClick();
                    }
                }
            }.bind(this)
        });
        listenerTouch.retain();
        cc.eventManager.addListener(listenerTouch, this.yesButton);
        cc.eventManager.addListener(listenerTouch.clone(), this.noButton);
    },
    yesButtonOnClick: function () {
        this._gameRoom.result.ccTimeOutManager.unScheduleInterval("resultTimer");
        this._gameRoom.result.removeFromParent(true);
        this._gameRoom.freeAllSeat();
        this._gameRoom.enableAllChairs();
        this.ccTimeOutManager.unScheduleInterval("winnerTimer");
        this._gameRoom._myId = -1;
        this._gameRoom.sendReqToServer("reConfirm", {});
        this.removeFromParent(true);
    },
    noButtonOnClick: function () {
        this.ccTimeOutManager.unScheduleInterval("winnerTimer");
        this.removeFromParent(true);
    },
    onExit: function () {
        this._super();
        this.ccTimeOutManager.unScheduleInterval("winnerTimer");
        this._gameRoom.result && this._gameRoom.result.ccTimeOutManager.unScheduleInterval("resultTimer");
    }
});

var WinnerTextField = cc.Scale9Sprite.extend({
    _gameRoom: null,
    ctor: function (params, gameRoom) {
        this._gameRoom = gameRoom;

        this._super(res.winner.winnerField, cc.rect(0, 0, 100 * scaleFactor, 100 * scaleFactor));
        this.setCapInsets(cc.rect(32 * scaleFactor, 32 * scaleFactor, 32 * scaleFactor, 32 * scaleFactor));
        this.setContentSize(cc.size(390 * scaleFactor, 290 * scaleFactor));
        this.setPosition(cc.p(cc.winSize.width / 2, cc.winSize.height / 2));

        var winArr = String(params["id"]).split(",");

        if (winArr.length > 1) {
            this.initMultiPlayer(params);
        } else {
            this.initSinglePlayer(params);
        }
    },
    initSinglePlayer: function (params) {

        var winArr = String(params["id"]).split(",");
        var prizeArr = String(params["amt"]).split(",");

        var player = this._gameRoom.getSeatObjByUser(winArr[0]);
        var playerName = player._playerName;

        var margin = 40 * scaleFactor, upperMargin = 60 * scaleFactor;
        var winnerName = new cc.LabelTTF(playerName + " has won\n" + prizeArr[0] + " chips", "RobotoRegular", 30 * 2 * scaleFactor);
        winnerName._setBoundingWidth(700 * scaleFactor);
        winnerName.setAnchorPoint(0.5, 1);
        winnerName.setScale(0.5);
        winnerName.setColor(cc.color(255, 196, 7));
        winnerName.setPosition(cc.p(this.width / 2, this.height - upperMargin));
        winnerName.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.addChild(winnerName, 1);

        if (this._gameRoom._myId != -1) {
            this.wMsg = new cc.LabelTTF("Do you want to play another Game?", "RobotoRegular", 22 * 2 * scaleFactor);
            this.wMsg.setScale(0.5);
            this.wMsg.setPosition(cc.p(this.width / 2, 80 * scaleFactor));
            this.addChild(this.wMsg, 1);
        }

        var _winning = "";
        var _meWon = false;
        if (winArr[0] == this._gameRoom._myId) {
            _winning = prizeArr[0];
            _meWon = true;
        }
        this._gameRoom.handleGameFinishEvent(_meWon, _winning, params);
    },
    initMultiPlayer: function (params) {

        var winArr = String(params["id"]).split(",");
        var prizeArr = String(params["amt"]).split(",");
        var playerLabel;
        var difference = 40 * scaleFactor;
        var margin = 40 * scaleFactor;
        var upperMargin = 80 * scaleFactor;
        var _winning = "";
        var _meWon = false;
        var lineDiff = 20 * scaleFactor;

        var player, playerName, bottomLine;
        var chips;
        for (var i = 0; i < winArr.length; i++) {

            player = this._gameRoom.getSeatObjByUser(winArr[i]);
            if (!player) {
                cc.log(winArr[i]);
                throw new Error("Player Empty");
            }
            playerName = player.getPlayer_name();
            // playerName = "rummy_1";
            playerLabel = new cc.LabelTTF(playerName + " ", "RobotoRegular", 20 * 2 * scaleFactor);
            playerLabel.setAnchorPoint(0, 0.5);
            playerLabel.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
            playerLabel.setScale(0.5);
            playerLabel.setColor(cc.color(255, 196, 7));
            playerLabel.setPosition(cc.p(margin, this.height - upperMargin - difference * i));
            this.addChild(playerLabel, 1);

            chips = new cc.LabelTTF(prizeArr[i] + " chips", "RobotoRegular", 20 * 2 * scaleFactor);
            chips.setAnchorPoint(1, 0.5);
            chips.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
            chips.setScale(0.5);
            chips.setColor(cc.color(255, 196, 7));
            chips.setPosition(cc.p(this.width - margin, this.height - upperMargin - difference * i));
            this.addChild(chips, 1);

            bottomLine = new cc.DrawNode();
            bottomLine.drawSegment(cc.p(margin, this.height - upperMargin - difference * i - lineDiff),
                cc.p(this.width - margin, this.height - upperMargin - difference * i - lineDiff), 0.5, cc.color(67, 46, 16));
            this.addChild(bottomLine, 1);

            if (winArr[i] == this._gameRoom._myId) {
                _winning = prizeArr[i];
                _meWon = true;
            }
        }

        if (this._gameRoom._myId != -1) {
            this.wMsg = new cc.LabelTTF("Do you want to play another Game?", "RobotoRegular", 22 * 2 * scaleFactor);
            this.wMsg.setScale(0.5);
            this.wMsg.setPosition(cc.p(this.width / 2, 80 * scaleFactor * scaleFactor));
            this.addChild(this.wMsg, 1);
        }
        this._gameRoom.handleGameFinishEvent(_meWon, _winning, params);
    }
});