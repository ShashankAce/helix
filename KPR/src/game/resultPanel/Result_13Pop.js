/**
 * Created by stpl on 3/21/2017.
 */
var Result_13Pop = Popup.extend({
    ctor: function (gameRoomContext) {
        this._super(new cc.Size(860 * scaleFactor, 580 * scaleFactor), gameRoomContext);

        this.roundLabel = new cc.LabelTTF("", "RobotoBold", 13 * 2 * scaleFactor);
        this.roundLabel.setScale(0.60);
        this.roundLabel.setAnchorPoint(1, 0.5);
        this.roundLabel.setPosition(cc.p(840 * scaleFactor, this.height - this.headerLeft.height / 2));
        this.addChild(this.roundLabel, 2);

        var greyNode = new cc.DrawNode();
        greyNode.drawRect(cc.p(0, 492 * scaleFactor), cc.p(860 * scaleFactor, 544 * scaleFactor), cc.color(102, 102, 102), 1, cc.color(102, 102, 102));
        this.addChild(greyNode, 2);

        greyNode = new cc.DrawNode();
        greyNode.drawRect(cc.p(1 * scaleFactor, 1 * scaleFactor), cc.p(859 * scaleFactor, 72 * scaleFactor), cc.color(230, 240, 245), 1, cc.color(230, 240, 245));
        this.addChild(greyNode, 2);

        var headerLabels;
        headerLabels = new cc.LabelTTF("Player", "RobotoBold", 25 * scaleFactor);
        headerLabels.setScale(0.6);
        headerLabels.setPosition(cc.p(50 * scaleFactor, 518 * scaleFactor));
        this.addChild(headerLabels, 2);

        headerLabels = new cc.LabelTTF("Result", "RobotoBold", 25 * scaleFactor);
        headerLabels.setScale(0.6);
        headerLabels.setPosition(cc.p(170 * scaleFactor, 518 * scaleFactor));
        this.addChild(headerLabels, 2);

        headerLabels = new cc.LabelTTF("GameScore", "RobotoBold", 25 * scaleFactor);
        headerLabels.setScale(0.6);
        headerLabels.setPosition(cc.p(655 * scaleFactor, 518 * scaleFactor));
        this.addChild(headerLabels, 2);

        this.totalScoreHeader = new cc.LabelTTF("TotalScore", "RobotoBold", 25 * scaleFactor);
        this.totalScoreHeader.setScale(0.6);
        this.totalScoreHeader.setPosition(cc.p(793 * scaleFactor, 518 * scaleFactor));
        this.addChild(this.totalScoreHeader, 2);

        this.timerText = new cc.LabelTTF("", "RobotoRegular", 24 * scaleFactor);
        this.timerText.setColor(cc.color(0, 0, 0));
        this.timerText.setScale(0.6);
        this.timerText.setPosition(cc.p(this.width / 2, 33 * scaleFactor));
        this.addChild(this.timerText, 10);

    },
    setRoundNoAndTime: function (rndNo, roundTime) {
        if (this._gameRoom.gameType.indexOf("Point") >= 0)
            this.roundLabel.setString("Round Time : " + roundTime);
        else
            this.roundLabel.setString("Round : " + rndNo + " | " + "Round Time : " + roundTime);
    },
    addJoker: function (str) {
        var aceJkr;
        if (str && str != "") {

            var clipper = new ccui.Layout();
            clipper.setContentSize(cc.size(50 * scaleFactor, 50 * scaleFactor));
            clipper.setClippingEnabled(true);
            clipper.setAnchorPoint(0, 0);
            clipper.setPosition(cc.p(15 * scaleFactor, 1 * scaleFactor));
            this.addChild(clipper, 4);

            var joker_label = new cc.LabelTTF("JOKER", "RobotoBold", 10 * 2 * scaleFactor);
            joker_label.setScale(0.5);
            joker_label.setColor(cc.color(0, 0, 0));
            joker_label.setPosition(cc.p(37 * scaleFactor, 57 * scaleFactor));
            this.addChild(joker_label, 4);

            var jkr = new CardModel(str);
            jkr.setScale(0.6);
            jkr.setPosition(cc.p(25 * scaleFactor, 15 * scaleFactor));


            if (str == "Joker") {
                clipper.setContentSize(cc.size(100 * scaleFactor, 50 * scaleFactor));

                aceJkr = new CardModel("S#A");
                aceJkr.setScale(0.6);
                aceJkr.setPosition(cc.p(25 * scaleFactor, 15 * scaleFactor));
                clipper.addChild(aceJkr, 1);

                joker_label.setPosition(cc.p(42 * scaleFactor, 57 * scaleFactor));
                jkr.setPosition(cc.p(40 * scaleFactor, 15 * scaleFactor));
                clipper.addChild(jkr, 2);
            } else {
                clipper.addChild(jkr);
            }

        }
    }
});