/**
 * Created by stpl on 12/14/2016.
 */

var Result_21_AllCard_List = ccui.Layout.extend({
    _name: "Result_21_AllCard_List",
    _gameRoom: null,
    _allCardLayer: null,
    playerName: null,
    ctor: function (result21, data, index) {
        this._super();

        this._result21 = result21;
        this._gameRoom = result21._gameRoom;

        this.setContentSize(cc.size(949 * scaleFactor, 69 * scaleFactor));
        this.setAnchorPoint(0, 1);
        this.setLocalZOrder(0);
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);

        if (index % 2 == 0)
            this.setBackGroundColor(cc.color(227, 227, 227));
        else
            this.setBackGroundColor(cc.color(245, 245, 245));

        var line1 = new cc.DrawNode();
        line1.drawSegment(cc.p(115 * scaleFactor, 0), cc.p(115 * scaleFactor, this.height - 1 * scaleFactor), 0.5, cc.color(200, 200, 200));
        this.addChild(line1);

        var line2 = new cc.DrawNode();
        line2.drawSegment(cc.p(218 * scaleFactor, 0), cc.p(218 * scaleFactor, this.height - 1 * scaleFactor), 0.5, cc.color(200, 200, 200));
        this.addChild(line2);

        var line3 = new cc.DrawNode();
        line3.drawSegment(cc.p(835 * scaleFactor, 0), cc.p(835 * scaleFactor, this.height - 1 * scaleFactor), 0.5, cc.color(200, 200, 200));
        this.addChild(line3);

        var bottomLine = new cc.DrawNode();
        bottomLine.drawSegment(cc.p(1 * scaleFactor, 0), cc.p(this.width - 1 * scaleFactor, 0), 0.5, cc.color(220, 220, 220));
        this.addChild(bottomLine);

        var nm = data["name"];
        nm = nm.substr(0, 12);
        this.playerName = new PlayerName(nm, this);
        this.playerName.setAnchorPoint(cc.p(0, 0.5));
        this.playerName.setPosition(cc.p(16 * scaleFactor, this.height / 2));
        this.addChild(this.playerName, 2);

        var prizeIcon, prizeText = "";
        if (data.resultType != "") {
            switch (data.resultType) {
                case "drop":
                    prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("DropTableIcon.png"));
                    prizeText = "Dropped";
                    break;
                case "Left":
                    prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("LeftTableIcon.png"));
                    prizeText = "Left";
                    break;
                case "winner" :
                    prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("prize.png"));
                    prizeText = "Winner";
                    this.setBackGroundColor(cc.color(231, 239, 188));
                    break;
                case "wrongShow" :
                    prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("wrongShow.png"));
                    prizeText = "WrongShow";
                    break;
                case "validShow" :
                    prizeIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("ValidShowIcon.png"));
                    prizeText = "ValidShow";
                    break;
                default:
                    break;
            }
            if (prizeIcon) {
                prizeIcon.setPosition(cc.p(165, this.height / 2 + 10 * scaleFactor));
                this.addChild(prizeIcon);
            } else {
                cc.log("Prize information not available error-code RS210");
            }

            var prizeLabel = new cc.LabelTTF(prizeText, "RobotoRegular", 2 * 12 * scaleFactor);
            prizeLabel.setScale(0.5);
            prizeLabel.setAnchorPoint(0.5, 0.5);
            prizeLabel.setColor(cc.color(0, 0, 0));
            prizeLabel.setPosition(cc.p(165 * scaleFactor, this.height / 3 - 10 * scaleFactor));
            this.addChild(prizeLabel, 1);
        }

        var pointsWon = new cc.LabelTTF(data["ptWon"], "RobotoRegular", 14 * 2 * scaleFactor);
        pointsWon.setScale(0.5);
        pointsWon.setColor(cc.color(0, 0, 0));
        pointsWon.setPosition(cc.p(890 * scaleFactor, this.height / 2));
        this.addChild(pointsWon, 1);

        this.addMeldCard(data);
        return true;
    },
    addMeldCard: function (data) {

        var groupArray,
            unGroupArray,
            cardsPipe,
            tmpCards,
            valueCard = [];
        var cardSpace = 14 * scaleFactor;
        var grpSpace = 45 * scaleFactor;

        if (data["meldCards"] != "null") {

            var meldCards = data["meldCards"];
            var resultType = data["resultType"];
            this._result21._gameRoom._userCardPanel.cardString = meldCards;

            if (meldCards.indexOf("|") == -1) {
                unGroupArray = meldCards.split(":");
                groupArray = [];
            }
            else {
                cardsPipe = meldCards.split("|");
                groupArray = cardsPipe[0].split(";");
                unGroupArray = cardsPipe[1].split(":");
            }

            var card;
            var xInt = 260 * scaleFactor, cardCount = 0;
            for (var i = 0; i < groupArray.length; i++) {
                if (groupArray[i] && groupArray[i] != "") {
                    tmpCards = groupArray[i].split(",");
                    for (var j = 0; j < tmpCards.length; j++) {
                        card = new DummyCard(tmpCards[j]);
                        card.setJoker(this._result21._wildCardStr, this._gameRoom._jokerSymVisble);
                        if (resultType != "wrongShow" && card._isJoker) {
                            valueCard.push(tmpCards[j]);
                        }
                        card.setTag(i);
                        card.x = xInt;
                        card.y = this.height / 2;
                        xInt = xInt + cardSpace;
                        ++cardCount;
                        this.addChild(card, cardCount);
                    }
                    // xInt = card.x + card.width / 2 + 10;
                    xInt = card.x + grpSpace;
                }
            }
            tmpCards = null;
            for (var k = 0; k < unGroupArray.length; k++) {
                if (unGroupArray[k] && unGroupArray[k] != "") {
                    tmpCards = unGroupArray[k].split(",");
                    for (var l = 0; l < tmpCards.length; l++) {
                        card = new DummyCard(tmpCards[l]);
                        card.setJoker(this._result21._wildCardStr, this._gameRoom._jokerSymVisble);
                        if (resultType != "wrongShow" && card._isJoker) {
                            valueCard.push(tmpCards[l]);
                        }
                        card.setTag(k);
                        card.x = xInt;
                        card.y = this.height / 2;
                        xInt = xInt + cardSpace;
                        ++cardCount;
                        this.addChild(card, cardCount);
                    }
                }
            }
        }
        this._result21._valueCards.push(valueCard);
    }
});