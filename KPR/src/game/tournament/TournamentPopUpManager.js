/**
 * Created by stpl on 1/30/2017.
 */
var TournamentPopUpManager = cc.Class.extend({
    _gameRoom: null,
    _popUp: null,
    _notification: null,
    _rejoinWaitTime: null,
    entryFeeCheck: null,
    _roomId: null,
    _reJoinFrom: null,
    _feeType: null,
    _entryFee: null,

    ctor: function (context) {
        this._gameRoom = context;

        this.entryFeeCheck = 0;
        this._roomId = -1;
        this._reJoinFrom = "";
        this._feeType = "Chip";
        this._entryFee = 0;

    },
    removePopUp: function () {
        this._popUp && this._popUp.removeFromParent();
    },
    removeNotification: function () {
        this._notification && this._notification.removeFromParent();
    },
    getTournamentStartPopUp: function (TmntName, round, deal, time) {
        this.removePopUp();
        this._popUp = new StartPopup(this);
        this._gameRoom.addChild(this._popUp, GameConstants.tNoficZorder, "_popUp");
        this._popUp.setTrnmntName(TmntName ? TmntName : "Welcome Tournament");
        this._popUp.setRndLvl(round ? round : "1");
        this._popUp.setDealPnt(deal ? deal : "6");
        this._popUp.setTime(time ? time : "50 Sec");
    },
    getMaxTablePopUp: function (str) {
        this.removePopUp();
        this._popUp = new MaxTournamentPopup(this, str);
        this._gameRoom.addChild(this._popUp, GameConstants.popupZorder, "_popUp");
    },
    getTieBreakerPopUp: function (str, time) {
        this.removePopUp();
        this._popUp = new TieBreakerPopup(this, str);
        this._gameRoom.addChild(this._popUp, GameConstants.tieBreakerZorder, "_popUp");
    },
    getRoundWinnerPopUp: function (tablePos, tournamentPos, amt) {
        this.removePopUp();
        var str = "You have finished " + tablePos + " position on this table and the tournament in " + tournamentPos + " position.\nYour account will be credited with " + amt + " shortly.\nGood luck for your next tournament."
        this._popUp = new RndLvlWinnerPopup(this, str);
        this._gameRoom.addChild(this._popUp, GameConstants.popupZorder, "_popUp");
    },
    getLevelWinnerPopUp: function (position, amt) {
        this.removePopUp();
        var str = "You have finished the tournament in " + position + " position.\nYour account will be credited with " + amt + " shortly.\nGood luck for your next tournament.";
        this._popUp = new RndLvlWinnerPopup(this, str);
        this._gameRoom.addChild(this._popUp, GameConstants.popupZorder, "_popUp");
    },
    getLostRoundPopUp: function (tablePos, tournamentPos) {
        this.removePopUp();
        var str = "You have finished " + tablePos + " position on this table and the tournament in " + tournamentPos + " position.\nGood luck for your next tournament.";
        this._popUp = new LostPopup(this._gameRoom, str);
        this._gameRoom.addChild(this._popUp, GameConstants.popupZorder, "_popUp");
    },
    getLostRoundNotification: function (torPos) {
        this.removeNotification();
        var str = "You have finished the tournament in " + torPos + " position.\nGood luck for your next tournament.";
        this._notification = new LostNotification(this._gameRoom, str);
        this._gameRoom.addChild(this._notification, GameConstants.tNoficZorder, "_notification");
    },
    getDropWaiting: function () {
        this.removeNotification();
        var str = "Please wait till we have the results of the other tables.";
        var str2=  "Please wait while the next deal starts";
        this._notification = new LostNotification(this._gameRoom, str2);
        this._gameRoom.addChild(this._notification, GameConstants.tNoficZorder, "_notification");
    },
    getWinnerRoundNotification: function (torPos, amt) {
        this.removeNotification();
        var str = "You have finished the tournament in " + torPos + " position.\nYour account will be credited with " + amt + " shortly.\nGood luck for your next tournament.";
        this._notification = new WinnerNotification(this._gameRoom, str);
        this._gameRoom.addChild(this._notification, GameConstants.tNoficZorder, "_notification");
    },
    getWaitForTournamentFinishPopUp: function (rank) {
        this.removePopUp();
        var str = "Your tournament ends here! Your final rank and winnings will be announced at the end of this tournament. You may check for the same in the tournament detail section.\n Your current rank is: " + rank + "\nGood luck for next tournament.";
        this._popUp = new LostPopup(this, str);
        this._gameRoom.addChild(this._popUp, GameConstants.popupZorder, "_popUp");
    },
    getWaitForTournamentFinishNot: function (rank) {
        this.removeNotification();
        var str = "Your tournament ends here! Your final rank and winnings will be announced at the end of this tournament. You may check for the same in the tournament detail section.\n Your current rank is: " + rank + "\nGood luck for next tournament.";
        this._notification = new LostNotification(this._gameRoom, str);
        this._gameRoom.addChild(this._notification, GameConstants.tNoficZorder, "_notification");
    },
    getEliminatedPopUp: function () {
        this.removePopUp();
        var str = "You are eliminated from the tournament as you do not\nhave sufficient chips to continue.";
        this._popUp = new EliminatedPopup(this, str);
        this._gameRoom.addChild(this._popUp, GameConstants.popupZorder, "_popUp");
    },
    getRebuyDonePopup: function (amt, time) {
        this.removePopUp();
        var str = amt + " chips have been credited to your chip account balance. \nPlease wait for the next deal to start.";
        // this._popUp = new RebuyDonePopup(this, str);
        // this._gameRoom.addChild(this._popUp, GameConstants.popupZorder, "_popUp");
        this._reBuySucess = new RebuyDonePopup(this, str);
        this._gameRoom.addChild(this._reBuySucess, GameConstants.popupZorder, "_reBuySucess");
    },
    getEntryFeeType: function (amt) {
        var entryFee = amt;
        var feeType, feeArray = [];

        // "FREE","Chip,10","Ticket,1:GR Cash Card","Free","Chip|Ticket,100|1:GR Cash Card"

        // Chip|Ticket,10|1:KPR Cash Card 50"
        if (entryFee.indexOf(TOURNAMENT_CONSTANTS.ENTRYFEE_CHIP) >= 0) {
            entryFee = entryFee.split(",");
            if (entryFee[1].indexOf("|") >= 0) {
                entryFee = entryFee[1].split("|")[0];
                feeType = "ChipAndTicket";
            } else {
                entryFee = entryFee[1];
                feeType = "Chip";
            }
        } else if (entryFee.indexOf(TOURNAMENT_CONSTANTS.ENTRYFEE_TICKET) >= 0 && !entryFee.indexOf(TOURNAMENT_CONSTANTS.ENTRYFEE_CHIP) >= 0) {
            entryFee = TOURNAMENT_CONSTANTS.ENTRYFEE_TICKET;
            feeType = "Ticket";
        } else {
            feeType = "Free";
        }
        feeArray.push(entryFee, feeType);
        return feeArray;
    },
    getRebuyPopup: function (chip, amt, rejoinFrom) {
        this._reJoinFrom = "";
        this._rejoinWaitTime = 10;
        this._reJoinFrom = rejoinFrom;
        this.removePopUp();

        var tempArr = amt.split(",");
        var formatArr = tempArr[0].split("|");
        var feeArr = tempArr[1].split("|");
        var amountStr = "";
        var entryFee2 = "";

        // "FREE","Chip,10","Ticket,1:GR Cash Card","Free","Chip|Ticket,100|1:GR Cash Card"

        // var amountStr = this.getEntryFeeType(amt);
        /* this._popUp = new RebuyCashPopup(this._gameRoom, chip, amountStr);
         this._gameRoom.addChild(this._popUp, GameConstants.popupZorder, "_popUp");*/
        if (formatArr.length == 1) {
            if (formatArr[0] == "Chip") {
                amountStr = "`" + feeArr[0];
            }
            else {
                //_entryFee = -1;
                this._entryFee = formatArr[0];
                if (parseInt(feeArr[0].split(":")[0]) == 1)
                    amountStr = formatArr[0];
                else
                    amountStr = feeArr[0].split(":")[0] + " " + formatArr[0];
            }
            this._popUp = new RebuyCashPopup(this._gameRoom, chip, amountStr);
            this._gameRoom.addChild(this._popUp, GameConstants.popupZorder, "_popUp");
        }
        else {
            amountStr = "`" + feeArr[0];
            this._entryFee = feeArr[0];
            if (parseInt(feeArr[1].split(":")[0]) == 1) {
                entryFee2 = formatArr[1];
            }
            else {
                entryFee2 = feeArr[1].split(":")[0] + " " + formatArr[1];
            }
            this._popUp = new RebuyTktPopup(this._gameRoom, chip, amountStr, entryFee2);
            this._gameRoom.addChild(this._popUp, GameConstants.popupZorder, "_popUp");
        }
    },
    /*todo updateRank(dataObj:SFSObject):void
     {
     var plyrsRank:Array = dataObj.getUtfString("rankStr").split(";");
     for (var j:int = 0;j< plyrsRank.length;j++ )
     {
     id = plyrsRank[j].split(":")[0];
     if (id == _gameRoom._myId)
     {
     if(_notification && _notification.notf_txt)
     _notification.notf_txt.text = "Your tournament ends here! Your final rank and winnings will be announced at the end of this tournament. You may check for the same in the tournament detail section.\n Your current rank is: " + plyrsRank[j].split(":")[1] + "\nGood luck for next tournament.";
     }
     }
     }*/
    getBreakTimePopup: function (time) {
        this.removePopUp();
        this._popUp = new BreakTimePopup();
        this._popUp.setTime(time);
        this._gameRoom.addChild(this._popUp, GameConstants.tNoficZorder, "_popUp");
    },
    getAddCashReBuyPopUp: function () {
        this.removePopUp();
        this._popUp = new AddCashReBuyPopUp();
        //todo this._popUp.setTime(time);
        this._gameRoom.addChild(this._popUp, GameConstants.popupZorder, "_popUp");
    },
    rejoinReq: function (str) {

        /*    var name = this._selectedRadioBtn && this._selectedRadioBtn._name;
         var entryFeeType;
         if(this.entryFeeArr[1] == "ChipAndTicket"){
         if (name == "RadioOne")
         entryFeeType = "Chip";
         else if (name == "RadioTwo")
         entryFeeType = "Ticket";
         }else entryFeeType = this.entryFeeArr[1];

         // TODO check by mycashBalance
         var dataObj = {};
         dataObj["feeType"] = entryFeeType;
         sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.REGISTRAION_REQ, dataObj, this.roomId);
         SoundManager.removeSound(SoundManager.extratimesound);

         cc.log("req" + dataObj);*/


        var sentObj = {};
        sentObj.isRejoin = str;
        sentObj.rejoinFrom = this._reJoinFrom;
        if (this._entryFee == -1)
            this._feeType = "Ticket";
        if (rummyData.cashBalance >= parseInt(this._entryFee)) {
            sentObj.feeType = this._feeType;
            this._gameRoom.sendReqToServer(SFSConstants.TOURNAMENT_RE_JOIN_REQ, sentObj);
        } else {
            if (this._entryFee == -1)
                this.getInsufficientTicketPopUp();
            else
                this.getAddCashPopUp();
        }
        cc.log("rejoin" + sentObj);

        this._reJoinFrom = "";
        if (str == "no") {
            if (this._gameRoom.result)
                this._gameRoom.result._isBuyNoClicked = true;
        }
        this.removePopUp();
    },
    getLevelTimeOverPopup: function (time) {
        this.removePopUp();
        var str = "The allotted time of 30 minutes for this level is over. You have just " + this._gameRoom.levelTimer.getString() + " to wrap up this game and declare the show.\nYour meld screen will pop up after " + time + " minute to finish this game.\n\nYour break time between level has also been forfeited.";
        this._popUp = new LevelTimeOverPopup(this, str);
        this._gameRoom.addChild(this._popUp, GameConstants.popupZorder, "_popUp");
    },
    setTime: function (time) {
        if (this._popUp && this._reJoinFrom == "") {
            if (this._popUp.timer_txt)
                this._popUp.setTime(time + " Sec");
        }
        if (this._notification)
            if (this._notification.timer_txt)
                this._notification.setTime(" ~" + time);
    },
    setLevelTime: function (time) {
        if (this._popUp && this._reJoinFrom == "") {
            {
                this._rejoinWaitTime -= 1;
                if (this._popUp.timer_txt)
                    this._popUp.setTime("0" + this._rejoinWaitTime);
                if (this._rejoinWaitTime == 0)
                    this.rejoinReq("no");
            }
            if (this._popUp && this._reJoinFrom == "") {
                if (this._popUp.timer_txt)
                    this._popUp.setTime(time + " Sec");
            }
            if (this._notification)
                if (this._notification.timer_txt)
                    this._notification.setTime(" ~" + time);
        }
    },
    setTourWinnerPopUP: function (pos, amt) {
        this.removePopUp();
        this.removeNotification();
        var txt = "You have finished the tournament at " + pos + " position. Your account will be credited with " + amt;
        var winner = new TournamentWinner(txt);
        this._gameRoom.addChild(winner, GameConstants.winnerScreenZorder);
    },
    getQualifyPopup: function (type) {
        this.removeNotification();
        var str;
        if (type == "Round")
            str = "Please wait... Games on other tables are still in progress.\nNote: Do not close the window. Game will start shortly";
        else
            str = "Please wait... Games on other tables are still in progress.\nNote: Do not close the window, this table game will commence shortly.";
        this._notification = new QualifyPopup(this._gameRoom, str);
        this._gameRoom.addChild(this._notification, GameConstants.tNoficZorder);
    },
    getInsufficientTicketPopUp: function () {
        this.removePopUp();
        this._popUp = new InsufficientTicketPopUp(this, str);
        this._gameRoom.addChild(this._popUp, GameConstants.popupZorder, "_popUp");
    },
    getAddCashPopUp: function () {
        this.removePopUp();
        this._popUp = new AddCashReBuyPopUp(this, str);
        this._gameRoom.addChild(this._popUp, GameConstants.popupZorder, "_popUp");
    },
    getPrizeMoney: function (przStr, przAmt) {
        this.formatStrArr = przStr.split("|");
        this.przFrmtStrArr = przAmt.split("|");
        var j = 0;
        var prizeStr = "";
        for (j = 0; j < this.formatStrArr.length; j++) {
            if (j == this.formatStrArr.length - 1)
                prizeStr += this.przFrmtStrArr[j] + "";
            else
                prizeStr += this.przFrmtStrArr[j] + "+";
        }
        return prizeStr;
    },
    isNotfOn: function () {
        if (this._notification != null)
            return true;
        else
            return false;
    },
    updateRank: function (dataObj) {
        var plyrsRank = dataObj["rankStr"].split(";");
        for (var j = 0; j < plyrsRank.length; j++) {
            id = plyrsRank[j].split(":")[0];
            if (id == this._gameRoom._myId) {
                if (this._notification && this._notification.message)
                    this._notification.message.setString("Your tournament ends here! Your final rank and winnings will be announced at the end of this tournament. You may check for the same in the tournament detail section.\n Your current rank is: " + plyrsRank[j].split(":")[1] + "\nGood luck for next tournament.");
            }
        }
    },
    removePopUpExceptRebuy: function () {
        if (this._popUp && this._popUp._name == "rebuy") {
            //trace("rebuy popup visibile");
        }
        else
            this.removePopUp();
    }
});