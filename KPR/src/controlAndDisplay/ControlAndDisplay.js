/**
 * Created by stpl on 9/16/2016.
 */
var ControlAndDisplay = cc.Layer.extend({
    _name: "controlLayer",
    _gameTabMap: null,
    _onlineStatus: null,
    _gameTabMapArray: null,
    controlLayerMouseListener: null,
    controlLayerTouchListener: null,
    _loadTimer: null,
    _httpLoadTime: null,
    disconnectionStatus: null,

    ctor: function () {

        this._super();

        this._gameTabMap = {};
        this._gameTabMapArray = [];
        this._onlineStatus = false;
        this._loadTimer = -2;
        this._httpLoadTime = -2;
        this.disconnectionStatus = false;
        // this.cndEventDispatcher = new CndEventDispatcher(this);
        this.tabHeight = 25 * scaleFactor;
        this.buttomTabHeight = 28 * scaleFactor;
        this.tabMargin = 15 * scaleFactor;
        this.tabSpace = 1 * scaleFactor;
        this.fontSize = 12 * scaleFactor;
        this.fontFamily = "RobotoRegular";

        this.makeUpperStrip(this.tabHeight);
        this.makeMenuButtons(this.tabHeight, this.tabMargin, this.tabSpace, this.fontSize, this.fontFamily);
        this.makeBottomMenuButtons(this.tabMargin, this.tabSpace, this.fontSize, this.fontFamily);

        this.lobbyTab = new LobbyTab();
        this.addChild(this.lobbyTab);
        this.updateLobbyTab();

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();
        return true;
    },
    updateTime: function () {
        this._serverDate.setTime(this._serverDate.getTime() + 1000 + (24 / 900));
        var date = this.getServerTimeStr();
        this._serverDateNTime.setString(date);

        applicationFacade.handleTimeUpdate();
    },
    setServerTime: function (time) {
        this._serverDate = new Date(time + 2000);
        var date = this.getServerTimeStr();
        this._serverDateNTime.setString(date);

        this.timeInterval && window.clearInterval(this.timeInterval);
        this.timeInterval = setInterval(function () {
            this.updateTime();
        }.bind(this), 1000);
    },
    getServerTimeStr: function () {
        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        return this._serverDate.getDate() + " " + months[this._serverDate.getMonth()] + ", " + this._serverDate.getFullYear() + "  |  " + this._serverDate.toLocaleTimeString('en-US') + "  |";
    },
    setServerRestartMessage: function (time) {
        // this.disconnectionStatus = true;
        applicationFacade.pauseListener(true);
        this.serverRestartPopup = new serverRestartPopup();
        this.serverRestartPopup.setTime(time);
        applicationFacade.addChild(this.serverRestartPopup, 80, "serverRestartPopup");

        this._serverDate = new Date(time + 2000);
        var date = this.getServerTimeStr();
        this._serverDateNTime.setString(date);

        this.serverRestarTtimeInterval && window.clearInterval(this.serverRestarTtimeInterval);
        this.serverRestarTtimeInterval = setInterval(function () {
            this.updateTime();
        }.bind(this), 1000);

    },
    setLoadTime: function () {
        this._loadTimer = 0;
        this._httpLoadTime = 0;
    },
    updateLobbyTab: function () {
        var roomArray = Object.keys(this._gameTabMap);
        if (roomArray.length < 1) {
            this.lobbyTab.setVisible(!1);
        } else {
            this.lobbyTab.setVisible(!0);
        }
    },
    makeUpperStrip: function (tabHeight) {

        var leftInnerStripWidth = 124 * scaleFactor;

        var leftTabBg = new cc.DrawNode();
        leftTabBg.drawRect(cc.p(0, tabHeight), cc.p(leftInnerStripWidth, 0), cc.color(50, 50, 50), 0, cc.color(0, 0, 0));
        leftTabBg.setPosition(cc.p(0, cc.winSize.height - tabHeight));

        this.addChild(leftTabBg);

    },
    makeMenuButtons: function (tabHeight, tabMargin, tabSpace, fontSize, fontFamily) {
        this.howToPlayTab = this.getMenuBlock("How To Play", tabHeight, tabMargin, fontSize, "RobotoBold");
        this.howToPlayTab.setPosition(cc.p(0, cc.winSize.height - tabHeight));

        this.helpTab = this.getMenuBlock("Help", tabHeight, tabMargin, fontSize, "RobotoBold");
        this.helpTab.setPosition(cc.p(this.howToPlayTab.width + tabSpace, cc.winSize.height - tabHeight));

        this.addChild(this.howToPlayTab);
        this.addChild(this.helpTab);
    },
    makeBottomMenuButtons: function (tabMargin, tabSpace, fontSize, fontFamily) {
        fontSize = 11 * scaleFactor;

        this._serverDateNTime = new cc.LabelTTF("", fontFamily, fontSize * 2);
        this._serverDateNTime.setAnchorPoint(1, 0.5);
        this._serverDateNTime.setScale(0.5);
        this._serverDateNTime.setPosition(cc.p(815 * scaleFactor, 14 * scaleFactor - yMargin));

        this.status = new cc.LabelTTF("Connected", fontFamily, fontSize * 2);
        this.status.setScale(0.5);
        this.status.setPosition(cc.p(855 * scaleFactor, 14 * scaleFactor - yMargin));

        this.statusSymbol = new cc.Sprite.create(spriteFrameCache.getSpriteFrame("whiteDot.png"));
        this.statusSymbol.setScale(0.8);
        this.statusSymbol.setColor(cc.color(166, 188, 23));
        this.statusSymbol.setPosition(cc.p(896 * scaleFactor, 14 * scaleFactor - yMargin));

        this.ReportAProblemLabel = new MakeReportIcon(tabMargin);
        this.ReportAProblemLabel.setName("ReportAProblem");

        this.SupportIcon = new MakeSupportIcon(tabMargin, this);
        this.SupportIcon.setName("SupportIcon");

        this.SoundLabel = new MakeSoundIcon(tabMargin);

        this.addChild(this.status);
        this.addChild(this.ReportAProblemLabel);
        this.addChild(this._serverDateNTime);
        this.addChild(this.SoundLabel);
        this.addChild(this.SupportIcon);
        this.addChild(this.statusSymbol);
        this.setConnected(false);

    },
    setConnected: function (bool) {
        if (bool) {
            if(this.status.getString() == "Connected")
                return;
            this.statusSymbol.setColor(cc.color(166, 188, 23));
            this.status.setString("Connected");

        } else {
            this.statusSymbol.setColor(cc.color(255, 50, 0));
            this.status.setString("Disconnected");
        }
    },
    getMenuBlock: function (string, tabHeight, tabMargin, fontSize, fontFamily) {
        return new MenuLayout(string, tabHeight, tabMargin, fontSize, fontFamily);
    },
    showHowToPlayDropDown: function () {
        this.closeAllTab();
        this.howToPlayDropDown = new HowToPlayDropDown(this.tabHeight, this);
        this.howToPlayDropDown.setPosition(cc.p(0, cc.winSize.height - this.tabHeight - yMargin));
        this.addChild(this.howToPlayDropDown, 2, 'howToPlayDropDown');
        this.howToPlayTab.setSelected(true);
    },
    showHelpDropDown: function () {
        this.closeAllTab();
        this.helpDropDown = new HelpDropDown(this.tabHeight, this);
        this.helpDropDown.setPosition(cc.p(this.helpTab.x, cc.winSize.height - this.tabHeight - yMargin));
        this.addChild(this.helpDropDown, 2, 'helpDropDown');
        this.helpTab.setSelected(true);
    },
    clearTab: function () {
        this.howToPlayTab.setSelected(false);
        this.helpTab.setSelected(false);
    },
    closeAllTab: function () {
        this.clearTab();
        if (this.howToPlayDropDown) {
            this.howToPlayDropDown.removeFromParent(true);
        }
        if (this.helpDropDown) {
            this.helpDropDown.removeFromParent(true);
        }
    },
    switchScreen: function (target) {
        var sfsObj = {};
        sfsObj.cmd = "tabChange";
        sfsObj.id = target;
        applicationFacade.controlHandler(sfsObj);
    },
    SupportIconClickListener: function (event) {
        window.parent.postMessage("openChatWindow", "*");
    },
    handleResp: function (cmd, params) {
        // this.cndEventDispatcher.dispatchSFSEvents(cmd, params);
        switch (cmd) {
            case SFSConstants.SERVER_TIME:
                this.setServerTime(params["time"]);
                break;
            case SFSConstants.CMD_ROOMINFO:
                /*if (dataObj.getInt("time") > 0)
                 setMessage(dataObj.getInt("time"));
                 else
                 if (dataObj.getUtfString("str")=="")
                 {
                 serverMsgMC.gotoAndStop(2);
                 serverMsgMC.visible = true;
                 }*/

                break;
            case SFSConstants.VIDEO_RESP:
                // handleVideoResp(dataObj);
                break;
            case SFSConstants.SERVER_RESTART:
                /*
                 if (dataObj.getInt("time") > 0)
                 setMessage(dataObj.getInt("time"));
                 */
                break;
            case SFSConstants.CMD_BONUSTOCASH:
                /*if(timeOut)
                 clearTimeout(timeOut);
                 bonusToCashPopUp.gotoAndStop(1);
                 bonusToCashPopUp.mc.close_btn.addEventListener(MouseEvent.CLICK, closeBonusCashPopUp);
                 //Main.getInstance()._contDispPanel.bonusToCashPopUp.mc.txt1.text = "Congratulations!\nYour bonus is successfully converted to `" + dataObj.amt+".";
                 bonusToCashPopUp.mc.txt1.text = "Rs. " + dataObj.getDouble("amt") + " has been disbursed from your";
                 bonusToCashPopUp.play();
                 bonusToCashPopUp.visible = true;
                 timeOut = setTimeout(closeBonusCashPopUp,10000,null);
                 */
                break;
            case SFSConstants.CMD_HC_SFS_RESTART:
                if (parseInt((params["time"])) > 0)
                    this.setServerRestartMessage(60 * parseInt((params["time"])));
                break;
        }
    },
    soundBtnHandler: function (event) {
        this.SoundLabel.toggleSound();
    },
    show21CardRules: function () {
        if (!this._21CardRules) {
            this._21CardRules = new TwentyOneRules(this);
            this.addChild(this._21CardRules, 5, "_21CardRules");
        }
    },
    show21CardTips: function () {
        if (!this._21CardTips) {
            this._21CardTips = new TwentyOneTips(this);
            this.addChild(this._21CardTips, 5, "_21CardTips");
        }
    },
    initMouseListener: function () {
        this.controlLayerMouseListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(event.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    !target._hovering && target.hover(true);
                    target._hovering = true;
                    return true;
                }
                target._hovering && target.hover(false);
                target._hovering = false;
                return false;
            }.bind(this)
        });
        this.controlLayerMouseListener.retain();
        cc.eventManager.addListener(this.controlLayerMouseListener.clone(), this.lobbyTab);
        cc.eventManager.addListener(this.controlLayerMouseListener.clone(), this.ReportAProblemLabel);
        cc.eventManager.addListener(this.controlLayerMouseListener.clone(), this.SoundLabel);
        cc.eventManager.addListener(this.controlLayerMouseListener.clone(), this.SupportIcon);

    },
    initTouchListener: function () {
        this.controlLayerTouchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var targetSize = target.getContentSize();
                var targetRect = cc.rect(0, 0, targetSize.width, targetSize.height);
                if (cc.rectContainsPoint(targetRect, locationInNode)) {
                    return true;
                }
                return false;
            }.bind(this),
            onTouchEnded: function (touch, event) {
                var target = event.getCurrentTarget();
                switch (target._name) {
                    case "HowToPlay":
                        if (this.howToPlayTab._isSelected) {
                            this.closeAllTab();
                            return;
                        }
                        this.showHowToPlayDropDown();
                        break;
                    case "Help":
                        if (this.helpTab._isSelected) {
                            this.closeAllTab();
                            return;
                        }
                        this.showHelpDropDown();
                        break;
                    case "lobbyTab":
                        applicationFacade._controlAndDisplay.closeAllTab();
                        applicationFacade._lobby.closeAllTab();
                        !target.isDisabled() &&
                        this.switchScreen(0);
                        break;
                    case "gameTable":
                        applicationFacade._controlAndDisplay.closeAllTab();
                        applicationFacade._lobby.closeAllTab();
                        this.switchScreen(target._roomId);
                        break;
                    case "ReportAProblem":
                        applicationFacade._controlAndDisplay.closeAllTab();
                        applicationFacade._lobby.closeAllTab();
                        applicationFacade.reportProblemHandler();
                        break;
                    case "SupportIcon":
                        applicationFacade._controlAndDisplay.closeAllTab();
                        applicationFacade._lobby.closeAllTab();
                        this.SupportIconClickListener(event);
                        break;
                    case "Sound":
                        applicationFacade._controlAndDisplay.closeAllTab();
                        applicationFacade._lobby.closeAllTab();
                        this.soundBtnHandler(event);
                }
            }.bind(this)
        });
        this.controlLayerTouchListener.retain();
        cc.eventManager.addListener(this.controlLayerTouchListener, this.howToPlayTab);
        cc.eventManager.addListener(this.controlLayerTouchListener.clone(), this.helpTab);
        cc.eventManager.addListener(this.controlLayerTouchListener.clone(), this.lobbyTab);
        cc.eventManager.addListener(this.controlLayerTouchListener.clone(), this.ReportAProblemLabel);
        cc.eventManager.addListener(this.controlLayerTouchListener.clone(), this.SoundLabel);
        cc.eventManager.addListener(this.controlLayerTouchListener.clone(), this.SupportIcon);
    },
    /*
     * @param id = 4001
     * @param name = table1
     * */
    addRoomTab: function (name, rid, rooom) {

        var entryFee;
        var gameType;
        var gameVal;
        var r;
        var roomInfoTxt;
        var tab;
        if (rid != 0) {
            r = rooom;
            if (r.groupId == sfsClient.GAME_ROOMS_GROUP_NAME) {

                var gameTypeStr = r.getVariable(SFSConstants._GAMETYPE).value;
                gameType = gameTypeStr.split("-")[0];
                gameVal = gameTypeStr.split("-")[1];

                if (gameVal.indexOf("Best") == -1)
                    gameVal = gameType + " " + gameVal;
                //tabMC.table.gameInfo.gameTF.text = gameVal;
                if (gameType != "Pool") {
                    gameVal = r.getVariable(SFSConstants.FLD_NUMOFCARD).value + " - " + gameType;
                    entryFee = r.getVariable(SFSConstants._BET_AMOUNT).value;
                }
                else
                    entryFee = r.getVariable(SFSConstants._ENTRYFEE).value;
                if (r.getVariable(SFSConstants._CASH_OR_PROMO).value == "CASH")
                    roomInfoTxt = gameVal + " | `" + entryFee;
                else
                    roomInfoTxt = gameVal + " | " + entryFee;
                if (r.getVariable(SFSConstants.FLD_NUMOFCARD).value == 13)
                    tab = new GameTab(rid, roomInfoTxt, cc.color(33, 38, 17), this, 13);
                else
                    tab = new GameTab(rid, roomInfoTxt, cc.color(56, 35, 8), this, 21);
                this.addChild(tab);
                tab.subMiniTable.addMiniTableElements(r.getVariable(SFSConstants._MAXPLAYERS).value, gameVal, "Table Id: " + r.getVariable(SFSConstants._ROOMID).value);
                //this.adjustTabsPosition();
            }
            else {

                var parntRm = sfsClient.getRoomById(parseInt(r.getVariable(SFSConstants.PARENT_EXT_ROOM_ID).value));
                var tableIdTxt;
                roomInfoTxt = parntRm.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value;
                gameVal = parntRm.getVariable(TOURNAMENT_CONSTANTS.TRNAMENT_NAME).value;

                if (parntRm.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE).value == "Round") {
                    if (parntRm.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL) && parntRm.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL))
                        tableIdTxt = "Round: " + parntRm.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value + "/" + parntRm.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL).value;
                }
                else {
                    if (parntRm.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL) && parntRm.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL))
                        tableIdTxt = "Level: " + parntRm.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value + "/" + parntRm.getVariable(TOURNAMENT_CONSTANTS.MAX_LEVEL).value;
                }

                if (r.getVariable(SFSConstants.FLD_NUMOFCARD).value == 13)
                    tab = new GameTab(rid, roomInfoTxt, cc.color(33, 38, 17), this, 13);
                else
                    tab = new GameTab(rid, roomInfoTxt, cc.color(56, 35, 8), this, 21);
                this.addChild(tab);
                tab.subMiniTable.addMiniTableElements(r.getVariable(SFSConstants._MAXPLAYERS).value, gameVal, tableIdTxt);

            }
        }
        this._gameTabMapArray.push(rid);
        this._gameTabMap[rid] = {id: rid, name: name, tab: tab};
        this.adjustTabsPosition();
        this.lobbyTab.setDisabled(!1);
        return tab;
    },
    removeRoomTab: function (id) {

        id = parseInt(id);
        var tab = this._gameTabMap[id].tab;
        this.removeChild(tab, true);
        delete this._gameTabMap[id];

        var index = this._gameTabMapArray.indexOf(id);
        this._gameTabMapArray.splice(index, 1);

        this.adjustTabsPosition();
        applicationFacade.removeTable(id);
        this.updateLobbyTab();

    },
    removeAllRoom: function () {
        var tab;
        for (var key in this._gameTabMap) {
            tab = this._gameTabMap[key].tab;
            this.removeChild(tab, true);
            applicationFacade.removeTable(key);
        }
        this._gameTabMap = {};
        this._gameTabMapArray = [];
    },
    adjustTabsPosition: function () {
        var index = 0, newPosX = 0, margin = 6, initialPos = (this.lobbyTab.width + margin) * scaleFactor, width = 0;

        for (var a = 0; a < this._gameTabMapArray.length; a++) {
            var key = this._gameTabMapArray[a];
            var posX = initialPos + width;
            this._gameTabMap[key].tab.setPosition(cc.p(posX, 0));
            index++;
            width += this._gameTabMap[key].tab.width + margin;
        }

        /*for (var key in this._gameTabMap) {
         var posX = initialPos + width;
         this._gameTabMap[key].tab.setPosition(cc.p(posX, 0));
         index++;
         width += this._gameTabMap[key].tab.width + margin;
         }*/
    },
    getNewTabPosition: function (tab) {
        var margin = 7 * scaleFactor;
        var initialPos = this.lobbyTab.width + margin;
    },
    updateRoomTab: function () {

    },
    showLobbyBtn: function (bool) {
        this.lobbyTab.hide(bool);
    },
    getTabIndexByRoomId: function (rid) {
        for (var idx in this._gameTabMap) {
            if (this._gameTabMap[idx].id == rid) {
                return parseInt(idx);
            }
        }
        return -1;
    },
    joinRoomHandler: function (rid) {
        var index = this.getTabIndexByRoomId(rid);
        if (index < 0)
            return;
        var sfsObj = {cmd: "tabChange", id: index};
        applicationFacade.controlHandler(sfsObj);
        //activeRoomTab(index);
    },
    setConnectionStatus: function (status, isFromPingCal) {
        this._onlineStatus = status;
        if (status) {
            var flag;
            if (isFromPingCal)
                flag = isFromPingCal;
            else flag = false;
            if (!flag){
                this.serverRestartPopup && this.serverRestartPopup.removeFromParent(true);
                this.serverRestartPopup = null;
                this.setConnected(this._onlineStatus);
                applicationFacade.pauseListener(false);
                // return;
            }
            if (this.disconnectionStatus) {
                applicationFacade.saveTournamentRoomIds();

                // do not delete || copyright by shashank
                for (var tableId in this._gameTabMap) {
                    this.removeRoomTab(tableId);
                }

                this.disconnectionStatus = false;
                var self = this;
                setTimeout(function () {
                    self.disconnection &&   self.disconnection.removeFromParent(true);
                    self.setConnected(self._onlineStatus);
                }, 2000);
            }else{
                this.setConnected(this._onlineStatus);
            }
        }
        else {
            if (!(this.log) || (this.log && !this.log.isVisible())) {
                this.setConnected(this._onlineStatus);
                sfsClient.cleanUp();
                this.disconnectionStatus = true;
                if (!this.disconnection && (!this.serverRestartPopup || !this.serverRestartPopup.isVisible())) {

                    this.disconnection = new DisconnectionPopup(20, this);
                    this.addChild(this.disconnection, 21, "disconnection");
                }
            }
        }
    }
});
var MakeSoundIcon = cc.Sprite.extend({
    _hovering: null,
    _isSoundEnabled: null,
    ctor: function (tabmargin) {
        this._super(spriteFrameCache.getSpriteFrame("soundIcon.png"));
        this.setPosition(cc.winSize.width - 20 * scaleFactor, tabmargin);
        this.setAnchorPoint(0.5, 0.5);
        this.setVisible(true);
        this.setName("Sound");
        this._isSoundEnabled = true;
        this._hovering = false;

        this.SoundtPlaceHolder = new ccui.Layout();
        this.SoundtPlaceHolder.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.SoundtPlaceHolder.setColor(cc.color(0, 0, 0));
        this.SoundtPlaceHolder.setPosition(cc.p(this.width / 2 - 30 * scaleFactor, this.height / 2 + 20 * scaleFactor));
        this.SoundtPlaceHolder.setVisible(false);
        this.label = new cc.LabelTTF("Sound", "RobotoRegular", 25 * scaleFactor);

        this.SoundtPlaceHolder.setContentSize(cc.size(50 * scaleFactor, 20 * scaleFactor));
        this.label.setScale(0.5);
        this.SoundtPlaceHolder.addChild(this.label);

        this.triangleBelowSoundHolder = new cc.Sprite(spriteFrameCache.getSpriteFrame("arrowBorder.png"));
        this.triangleBelowSoundHolder.setRotation(-90);
        this.triangleBelowSoundHolder.setAnchorPoint(0.5, 0.5);
        this.triangleBelowSoundHolder.setPosition(cc.p(this.SoundtPlaceHolder.width / 2, -3 * scaleFactor));
        this.triangleBelowSoundHolder.setColor(cc.color(0, 0, 0));

        this.SoundtPlaceHolder.addChild(this.triangleBelowSoundHolder);

        this.label.setPosition(cc.p(this.SoundtPlaceHolder.width / 2, this.SoundtPlaceHolder.height / 2));

        this.addChild(this.SoundtPlaceHolder, 1);

        this.soundOff = new cc.DrawNode();
        this.soundOff.drawSegment(cc.p(0, 0), cc.p(this.width, this.height), 1, cc.color(255, 0, 0));
        this.soundOff.setVisible(false);
        this.addChild(this.soundOff, 5);

        return true;
    },
    hover: function (bool) {
        this.SoundtPlaceHolder.setVisible(bool);
    },
    toggleSound: function () {
        if (this._isSoundEnabled) {
            this.soundOff.setVisible(true);
            SoundManager.muteAllSound(true);
        } else {
            this.soundOff.setVisible(false);
            SoundManager.muteAllSound(false);
        }
        this._isSoundEnabled = !this._isSoundEnabled;
    }
});
var MakeSupportIcon = cc.Sprite.extend({
    _hovering: null,
    ctor: function (tabmargin) {

        this._super();
        this.setContentSize(cc.size(20 * scaleFactor, 20 * scaleFactor));
        this.setPosition(cc.p((cc.winSize.width - 80 * scaleFactor), tabmargin * scaleFactor));
        this.setAnchorPoint(0.5, 0.5);
        this._hovering = false;

        this.supportPlaceHolder = new ccui.Layout();
        this.supportPlaceHolder.setContentSize(cc.size(55 * scaleFactor, 25 * scaleFactor));
        this.supportPlaceHolder.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.supportPlaceHolder.setColor(cc.color(0, 0, 0));
        this.supportPlaceHolder.setAnchorPoint(0.5, 0.5);
        this.supportPlaceHolder.setPosition(cc.p(this.width / 2 - 5 * scaleFactor, this.height / 2 + 35 * scaleFactor));
        this.supportPlaceHolder.setVisible(false);

        this.label = new cc.LabelTTF("Support", "RobotoRegular", 25 * scaleFactor);
        this.label.setPosition(cc.p(this.supportPlaceHolder.width / 2, this.supportPlaceHolder.height / 2));

        this.label.setScale(0.5);
        this.supportPlaceHolder.addChild(this.label);

        this.triangleBelowSupportHolder = new cc.Sprite(spriteFrameCache.getSpriteFrame("arrowBorder.png"));
        this.triangleBelowSupportHolder.setRotation(-90);
        this.triangleBelowSupportHolder.setAnchorPoint(0.5, 0.5);
        this.supportPlaceHolder.addChild(this.triangleBelowSupportHolder);
        this.triangleBelowSupportHolder.setPosition(cc.p(this.supportPlaceHolder.width / 2, -3 * scaleFactor));
        this.triangleBelowSupportHolder.setColor(cc.color(0, 0, 0));

        this.icon = new cc.Sprite(spriteFrameCache.getSpriteFrame("supportIcon.png"));
        this.icon.setPosition(cc.p(this.width / 2, this.height / 2));

        this.addChild(this.icon);
        this.addChild(this.supportPlaceHolder);
    },
    hover: function (bool) {
        this.supportPlaceHolder.setVisible(bool);

    }
});
var MakeReportIcon = ccui.Layout.extend({
    _hovering: null,
    ctor: function (tabmargin) {

        this._super();
        this.setContentSize(cc.size(20 * scaleFactor, 20 * scaleFactor));
        this.setPosition(cc.p((cc.winSize.width - 50 * scaleFactor), tabmargin * scaleFactor));
        this.setAnchorPoint(0.5, 0.5);

        this._hovering = false;
        this.reportPlaceHolder = new ccui.Layout();
        this.reportPlaceHolder.setContentSize(cc.size(110 * scaleFactor, 25 * scaleFactor));
        this.reportPlaceHolder.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.reportPlaceHolder.setColor(cc.color(0, 0, 0));
        this.reportPlaceHolder.setAnchorPoint(0.5, 0.5);
        this.reportPlaceHolder.setPosition(cc.p(this.width / 2 - 5 * scaleFactor, this.height / 2 + 35 * scaleFactor));
        this.reportPlaceHolder.setVisible(false);

        this.label = new cc.LabelTTF("Report A Problem", "RobotoRegular", 25 * scaleFactor);
        this.label.setPosition(cc.p(this.reportPlaceHolder.width / 2, this.reportPlaceHolder.height / 2));

        this.label.setScale(0.5);
        this.reportPlaceHolder.addChild(this.label);

        this.triangleBelowReportHolder = new cc.Sprite(spriteFrameCache.getSpriteFrame("arrowBorder.png"));
        this.triangleBelowReportHolder.setRotation(-90);
        this.reportPlaceHolder.addChild(this.triangleBelowReportHolder);
        this.triangleBelowReportHolder.setPosition(cc.p(this.reportPlaceHolder.width / 2, -3 * scaleFactor));
        this.triangleBelowReportHolder.setColor(cc.color(0, 0, 0));

        this.icon = new cc.Sprite(spriteFrameCache.getSpriteFrame("reportProblem.png"));
        this.icon.setPosition(cc.p(this.width / 2, this.height / 2));

        this.addChild(this.icon);
        this.addChild(this.reportPlaceHolder);
    },
    hover: function (bool) {
        this.reportPlaceHolder.setVisible(bool);

    }
});