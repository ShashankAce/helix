/**
 * Created by Shashank on 6/7/2016.
 */
"use strict";
var sfs = null;
var SFS2XClient = cc.Class.extend({
    _name: null,
    LOBBY_ROOM_NAME: null,
    GAME_ROOMS_GROUP_NAME: null,
    TAG_CASH: null,
    TAG_PROMO: null,
    TAG_POOL: null,
    TAG_DEAL: null,
    TAG_POINTS: null,
    SFS_RoomID: null,
    _lobbyLoadTime: null,
    _roomList: null,
    roomInfoTmp: null,
    sFinalGamesList: null,
    mSeatedPLayers: null,
    cacheInfoList: null,
    SFS_RoundNo: null,
    selfseatNo: null,
    roomUpdates: null,
    mSeatedPlayersAvatar: null,
    SFS_SEAT_ID: null,
    listToDisplay: null,
    mgameTypeArray: null,
    isReConnecting: null,
    mFGameName: null,
    mFMaxPlayer: null,
    mFTurnType: null,
    mFGameCashOrPromo: null,
    playerconnectRcvdTime: null,
    promoBalance: null,
    cashBalance: null,
    customTimeOutClass: null,
    timeoutCount: null,
    isConnected: null,
    pingStartTime: null,
    playerConnectRcvdTime: null,
    isDisconnectedToast: null,
    sceneName: null,
    ispostPing: null,
    isSessionNotValid: null,
    _loginPath: null,
    _pingType: null,
    _tournamentRoomArr: null,
    tourIndexNo: null,

    ctor: function () {

        this._name = "SFS2XClient";
        this.LOBBY_ROOM_NAME = "Lobby";
        this.GAME_ROOMS_GROUP_NAME = "gameRoom";
        this.TAG_CASH = "CASH";
        this.TAG_PROMO = "PROMO";
        this.TAG_POOL = "Pool";
        this.TAG_DEAL = "Deal";
        this.TAG_POINTS = "Point";
        this.SFS_RoomID = -1;
        this._lobbyLoadTime = 0;
        this._roomList = {};
        this.roomInfoTmp = null;
        this.sFinalGamesList = [];
        this.mSeatedPLayers = [];
        this.cacheInfoList = [];
        this.SFS_RoundNo = 0;
        this.selfseatNo = 0;
        this.roomUpdates = {};
        this.mSeatedPlayersAvatar = {};
        this.SFS_SEAT_ID = null;
        this.listToDisplay = [];
        this.mgameTypeArray = [];
        this.isReConnecting = false;
        this.mFGameName = null;
        this.mFMaxPlayer = null;
        this.mFTurnType = null;
        this.mFGameCashOrPromo = null;
        this.playerconnectRcvdTime = 0;
        this.promoBalance = 0;
        this.cashBalance = 0;
        this.customTimeOutClass = null;
        this.timeoutCount = 0;
        this.isConnected = false;
        this.pingStartTime = null;
        this.playerConnectRcvdTime = null;
        this.isDisconnectedToast = false;
        this.sceneName = null;
        this.ispostPing = false;
        this.isSessionNotValid = false;
        this._loginPath = "";
        this._pingType = "round";
        this._tournamentRoomArr = [];
        this.tourIndexNo = 0;
    },
    initClient: function (host, port) {

        cc.log("SmartFox Application started");

        var config = {};
        config.host = host;
        config.port = gameConfig.useSSL == true ? 8843 : 8888;
        config.useSSL = gameConfig.useSSL;
        config.zone = SFSConstants.SFS_ZONE;
        config.debug = false;//cc.game.config.debugMode > 0;
        this._lobbyLoadTime = new Date();

        sfs = new SmartFox(config);

        sfs.ExtensionRemoteDebug = false;
        //sfs.LogLevel.setLevel(DEBUG);
        //sfs.logger.setDebugMode(true);

        // Add event listeners
        sfs.addEventListener(SFS2X.SFSEvent.CONNECTION, this.onConnection.bind(this), this);
        sfs.addEventListener(SFS2X.SFSEvent.CONNECTION_LOST, this.onConnectionLost.bind(this), this);

        sfs.addEventListener(SFS2X.SFSEvent.LOGIN, this.onLogin.bind(this), this);
        sfs.addEventListener(SFS2X.SFSEvent.LOGIN_ERROR, this.onLoginError.bind(this), this);
        sfs.addEventListener(SFS2X.SFSEvent.LOGOUT, this.onLogOut.bind(this), this);

        //SFS Room Events
        sfs.addEventListener(SFS2X.SFSEvent.ROOM_JOIN, this.onRoomJoin.bind(this), this);
        sfs.addEventListener(SFS2X.SFSEvent.ROOM_JOIN_ERROR, this.onRoomJoinError.bind(this), this);
        sfs.addEventListener(SFS2X.SFSEvent.ROOM_ADD, this.onRoomAdd.bind(this), this);
        sfs.addEventListener(SFS2X.SFSEvent.ROOM_REMOVE, this.onRoomDelete.bind(this), this);
        //Room Events
        sfs.addEventListener(SFS2X.SFSEvent.USER_ENTER_ROOM, this.onUserEnterRoom.bind(this), this);
        sfs.addEventListener(SFS2X.SFSEvent.USER_EXIT_ROOM, this.onUserExitRoom.bind(this), this);
        sfs.addEventListener(SFS2X.SFSEvent.USER_COUNT_CHANGE, this.onUserCountChange.bind(this), this);
        sfs.addEventListener(SFS2X.SFSEvent.ROOM_VARIABLES_UPDATE, this.onRoomVariableUpdate.bind(this), this);

        sfs.addEventListener(SFS2X.SFSEvent.EXTENSION_RESPONSE, this.onExtensionResponse.bind(this), this);

        sfs.addEventListener(SFS2X.SFSEvent.ROOM_GROUP_SUBSCRIBE, this.onGroupSubscribed.bind(this), this);
        sfs.addEventListener(SFS2X.SFSEvent.ROOM_GROUP_UNSUBSCRIBE, this.onGroupUnsubscribed.bind(this), this);
        sfs.addEventListener(SFS2X.SFSEvent.ROOM_GROUP_SUBSCRIBE_ERROR, this.onGroupSubscribeError.bind(this), this);
        sfs.addEventListener(SFS2X.SFSEvent.ROOM_GROUP_UNSUBSCRIBE_ERROR, this.onGroupUnsubscribeError.bind(this), this);

        sfs.addEventListener(SFS2X.SFSEvent.PUBLIC_MESSAGE, this.onPublicMessage.bind(this), this);
        sfs.addEventListener(SFS2X.SFSEvent.PING_PONG, this.onPingPong.bind(this), this);

        sfs.addEventListener(SFS2X.SFSEvent.USER_VARIABLES_UPDATE, this.onUserVarsUpdate.bind(this), this);

        sfs.connect();

        return true;
    },
    onConnection: function (event) {
        if (event.success) {
            if (applicationFacade._connectionChecker) {
                applicationFacade._connectionChecker.pingResp("any");
            }
            applicationFacade.handleConnection(true);
            if ((applicationFacade._myUName != "") && (applicationFacade._myUName != "null")) {
                cc.log("Connected to SmartFoxServer 2X!");
                this.loginRequest();
            }
            else
                applicationFacade.requestNickName();
        }
        else {
            var error = "Connection failed: " + (event.errorMessage ? event.errorMessage + " (code " + event.errorCode + ")" : "Is the server running at all?");
            cc.log(error);
            if (!applicationFacade._connectionChecker) {
                applicationFacade.setLobbyLoadTimer();
                applicationFacade.handleConnection(false);
                this.connectSfs();
            }
        }
    },
    onConnectionLost: function (event) {
        applicationFacade.handleConnection(false);
    },
    onLogin: function (event) {
        sfs.enableLagMonitor(true, 4, 10);

        cc.log("Login successful!" +
            "\n\tZone: " + event.zone +
            "\n\tUser: " + event.user.name
        );
        //  sfs.send(new SFS2X.Requests.System.LeaveRoomRequest(sfs.getRoomByName("TournamentLobby")));
        //sfs.send(new SFS2X.Requests.System.UnsubscribeRoomGroupRequest("tournamentLobby"));
        this.currentUser = event.user;

        if (!sfs.roomManager.containsGroup(this.GAME_ROOMS_GROUP_NAME)) {
            // sfs.send(new SFS2X.Requests.System.SubscribeRoomGroupRequest(this.GAME_ROOMS_GROUP_NAME));
        }


    },
    ping: function (context) {
        this.pingStartTime = new Date().getTime();
        var pingTimeOutCount;
        if (this.sceneName == "lobby") {
            pingTimeOutCount = parseInt(rummyData.lobbyTimeoutCount);
        } else {
            pingTimeOutCount = parseInt(rummyData.timeoutCount);
        }

        if (this.timeoutCount != pingTimeOutCount) {
            cc.log("pingCount:" + this.timeoutCount);
            sfs.send(new SFS2X.Requests.System.ExtensionRequest(SFSConstants.CMD_PING, {}, null));
            cc.log("sfs connection:" + sfs.isConnected());
        } else {
            // show disconnected popup
            cc.log("disconnected timer starts");
            this.isDisconnectedToast = true;
            if (!sfs.isConnected()) {
                this.postPingTimer();
            } else {
                // cc.game.isPaused() == false means forground
                if (cc.game.isPaused()) { // background case
                    // only in mobile case always
                    this.logoutTimer(context);
                } else {
                    // start timer for 25 sec disconnectCount
                    this.disconnectTimer(context);
                }
            }
        }
        ++this.timeoutCount;
    },
    logoutTimer: function (context) {
        var logoutTime = 0;
        var logoutCount;
        if (cc.director.getRunningScene()._name == "lobby") {
            logoutCount = parseInt(rummyData.lobbyLogoutTime);
        } else {
            logoutCount = parseInt(rummyData.gameLogoutTime);
        }
        cc.log("logout count is:" + logoutCount);
        this.customTimeOutClass.scheduleInterval(function () {
            if (logoutTime == logoutCount) {
                if (sfs.isConnected()) {
                    sfsClient.logoutRequest();
                    sfs.disconnect();
                }
                this.customTimeOutClass.unScheduleInterval("lobbyPing");
                this.customTimeOutClass.unScheduleInterval("logoutCountTimer");
                // cc.game.isPaused()
            }
            logoutCount++;
        }.bind(this), 1000, context, "logoutCountTimer");
    },
    disConnect: function () {
        if (sfs.isConnected()) {
            sfsClient.logoutRequest();
            sfs.disconnect();
        }
        this.releaseSfs();
    },
    releaseSfs: function () {
        if (cc.director.getRunningScene()._name == "gameTable") {
            // this.updateServerInfo(rummyData.serverTime, false);
        }
        if (cc.director.getRunningScene()._name == "lobby") {
            //this.updateServerInfo(rummyData.serverTime, false);
        }
        this.timeoutCount = 0;
    },
    disconnectTimer: function (context) {
        var discCount = 0;
        var disconnectCount;
        if (cc.director.getRunningScene()._name == "lobby") {
            disconnectCount = parseInt(rummyData.lobbyDisconnectCount);
        } else {
            disconnectCount = parseInt(rummyData.disconnectCount);
        }
        cc.log("server disconnect count is:" + disconnectCount);
        this.customTimeOutClass.scheduleInterval(function () {
            if (disconnectCount == discCount) {
                this.disConnect();
                this.customTimeOutClass.unScheduleInterval("lobbyPing");
                this.customTimeOutClass.unScheduleInterval("disconnectCountTimer");
                this.postPingTimer();
            }
            discCount++;
            cc.log("disconnect count is:" + discCount);
        }.bind(this), 1000, context, "disconnectCountTimer");
    },
    postPingTimer: function () {
        this.ispostPing = true;
        cc.log("Post Ping Initiated");
        // create postping timer to connect
        this.customTimeOutClass.unScheduleTimeOut("postPingTimer");
        this.customTimeOutClass.scheduleTimeOut(function () {
            if (sfs.isConnected()) {
                var timer = new Date().getTime() - this.pingStartTime;
                this.connectAgain(timer);

            } else {
                var url = KPR.versionData.gameEngineInfo.RUMMY_2X.serverUrl + "/" + KPR.GAME_ENGINE_SUB_URL;
                var data = {
                    "merchantKey": "4",
                    "secureKey": KPR.secureKey,
                    "device": Utils.deviceType,
                    "token": rummyData.playerToken
                };
                var userData = JSON.stringify(data);
                var postData = 'userData=' + userData + '&url=' + url + "&request='triggerAction'";
                AjaxCommunication.post(postData, this.afterTrigger.bind(this));
            }
        }.bind(this), parseInt(rummyData.postPingTime) * 1000, "postPingTimer");
    },
    hideDisconnectToast: function () {
        if (this.isDisconnectedToast) {
            this.isDisconnectedToast = false;
        }
    },
    sfxInit: function () {
        sfsClient.initClient(SFSConstants.HOST, SFSConstants.PORT);
    },
    connectSfs: function () {
        sfs.connect();
    },
    afterTrigger: function (resp) {
        if (resp.errorCode == 0) {
            this.hideDisconnectToast();
            this.sfxInit();
            this.connectSfs();
            // sfs.connect();
            this.customTimeOutClass.unScheduleTimeOut("postPingTimer");
            // this.requestPing(this.sceneName, cc.director.getRunningScene());
        } else {
            // connection error
            // this.postPingTimer();
        }
    },
    countPingTimer: function () {
        this.timeoutCount = 0;
        this.hideDisconnectToast();
        this.customTimeOutClass.unScheduleInterval("disconnectCountTimer");
    },
    connectAgain: function (discDuration) {
        var obj = {};
        var str = "";
        str = SFSConstants.FLD_PING;
        str = str + SFSConstants.COLON_DELIMITER + discDuration.toString();
        obj[SFSConstants.FLD_DISCDATA] = str;

        if (this.SFS_RoomID != -1) {
            sfs.send(new SFS2X.Requests.System.ExtensionRequest(SFSConstants.CMD_CONNECTAGAIN, obj, sfs.getRoomById(sfsClient.SFS_RoomID)));
        } else {
            sfs.send(new SFS2X.Requests.System.ExtensionRequest(SFSConstants.CMD_CONNECTAGAIN_LOBBY, obj, sfs.getRoomByName(SFSConstants.LOBBY_ROOM_NAME)));
        }

    },
    onLoginError: function (event) {
        var error = "Login error: " + event.errorMessage + " (code " + event.errorCode + ")";
        cc.log(error);
    },
    onLogOut: function (event) {
        applicationFacade.logoutHandler();
    },
    onRoomJoin: function (event) {
        cc.log("Room joined: " + event.room.name + "\nRoom id:" + event.room.id + "\nGroupId : " + event.room.groupId);
        //trace("on joinroom", room.groupId, _tounamentName, _tounamentLobbyName);
        if (event.room.name == "TournamentLobby" || event.room.groupId == "tournamentRoom" || event.room.groupId == "tournamentLobby")
            return;
        if (event.room.name == this.LOBBY_ROOM_NAME) {
            this.populateRoomsList();
        } else {
            this.roomUpdates = event.room;
            this.SFS_RoomID = event.room.id;

            var roomArray = Object.keys(applicationFacade._gameMap);
            if (roomArray.length < applicationFacade._maxRooms) {
                applicationFacade.joinRoomFromLobby(event.room);
                applicationFacade._lobby.sendJoinRoomRequestForNextRoom(event.room.id);

                /* if (event.room.name != "TournamentLobby") {
                 if (applicationFacade._tourOpenRooms) {
                 var reqObj = {};
                 if (applicationFacade._tourOpenRooms[this.tourIndexNo]) {
                 sfsClient.sendTournamentReq(TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_TAKESEAT_REQUEST, reqObj, applicationFacade._tourOpenRooms[this.tourIndexNo]);
                 this.tourIndexNo++;
                 }
                 }
                 }*/
            } else {
                sfs.send(new SFS2X.Requests.System.LeaveRoomRequest(sfs.getRoomById(this.SFS_RoomID)));
                cc.log("leave Tournament : " + event.room.name);
            }
        }
    },
    onRoomJoinError: function (event) {
        // cc.log("Room join error: " + event.errorMessage + " (code: " + event.errorCode + ")", true);
    },
    onRoomAdd: function (event) {
        var room = event.room;
        if (room.groupId == "gameRoom" && room.variables.hasOwnProperty("domainId") && (room.variables.domainId.value.indexOf(rummyData.domainId) >= 0)) {
            this.roomListHandler.addRoom(room.id, room);
            cc.log(room);
            cc.log("room Add: " + room.id);
            cc.log("room Add Table iD: " + room.getVariable("roomId").value);
            // applicationFacade._lobby.onRoomListUpdate1(room);
        }
        else if (room.groupId == "tournamentRoom" && room.variables.hasOwnProperty("domainId") && (room.variables.domainId.value.indexOf(rummyData.domainId) >= 0)) {
            cc.log("Room added");
            this.tournamentRoomListHandler.addRoom(room.id, room);
            this._tournamentRoomArr.push(this.tournamentRoomListHandler.tournamentRoomListObj[room.id]);
            // applicationFacade._lobby.tournamentLobby.onRoomAdd(room);
            applicationFacade._lobby.tournamentLobby.handleTournamentList(this._tournamentRoomArr, true);
            cc.log(room);
            // this.onTournamentRoomListUpdate();
        }
    },
    onRoomDelete: function (event) {
        var room = event.room;
        if (room.groupId == "gameRoom" && room.variables.hasOwnProperty("domainId") && (room.variables.domainId.value.indexOf(rummyData.domainId) >= 0)) {
            this.roomListHandler.deleteRoom(room.id);
            /*for(var i = 0; i<this.mGameList.length; i++){
             if(this.mGameList[i].getmTableId() == room.id){
             this.mGameList.splice(i, 1);
             }
             }*/
            if (this.roomListHandler.getRoomList != null) {
                // this.onRoomListUpdate();
            }
        }
        else if (room.groupId == "tournamentRoom" && room.variables.hasOwnProperty("domainId") && (room.variables.domainId.value.indexOf(rummyData.domainId) >= 0)) {
            this._tournamentRoomArr.splice(this.tournamentRoomListHandler.tournamentRoomListObj[room.id], 1);
            this.tournamentRoomListHandler.deleteRoom(room.id);
            applicationFacade._lobby.tournamentLobby.tourRoomListManager.onRoomDeleted(room);
            // applicationFacade._lobby.tournamentLobby.handleTournamentList(this._tournamentRoomArr);
        }
    },
    onUserEnterRoom: function (event) {
        var room = event.room;
        var user = event.user;
        cc.log("User " + user.name + " just joined Room " + room.name + sfs.mySelf);
    },
    onUserExitRoom: function (event) {
        /*var room = event.room;
         var user = event.user;

         cc.log("User exit Room" + user);
         if (room && user && user.id == this.currentUser.id) {
         KHEL_CONSTANTS.IS_NEXT_POINTS = true;
         if (applicationFacade._gameMap[room.id]) {
         // if (room.groupId != "tournamentGameRoom" && applicationFacade._tourRoomLeaveAllowed)
         room.id && applicationFacade._controlAndDisplay.removeRoomTab(room.id);
         }
         }*/
        cc.log("User exit Room" + event.user);
        var evt = event;
        var rm = evt.room;
        var rid = rm.id;
        var u = evt.user;
        if (rm && u && (u.id == sfs.mySelf.id)) {
            // rm.removeUser(u);
            applicationFacade.OnRoomLeft(rid);
            if (applicationFacade._tempJoinRooms.indexOf(rid + "") != -1)
                applicationFacade._tempJoinRooms.splice(rid + "", 1);
        }

    },
    onUserCountChange: function (event) {
        var room = event.room;
        var uCount = event.uCount;
        var sCount = event.sCount;
        // cc.log("Room: " + room.name + " now contains " + uCount + " users and " + sCount + " spectators");
    },
    onRoomVariableUpdate: function (event) {
        var room = event.room;
        if (applicationFacade._gameMap[room.id]) {
            applicationFacade._gameMap[room.id].roomVariableUpdate(event);
        }

        // Check if the "gameStarted" variable was changed
        /*if (changedVars.indexOf("gameRoom") != -1)
         {
         if (room.getVariable("gameRoom") == true)
         cc.log("gameRoom started");
         else
         cc.log("gameRoom stopped");
         }*/
    },
    onGroupSubscribed: function (event) {
        var roomList = [], tempTournamentRoomArr = [];

        if (event.groupId == "tournamentRoom") {
            this._tournamentRoomArr = [];
            tempTournamentRoomArr = sfs.getRoomListFromGroup("tournamentRoom");
            for (var i = 0; i < tempTournamentRoomArr.length; i++) {
                if (rummyData["domainId"] && tempTournamentRoomArr[i].variables.domainId.value.indexOf(rummyData.domainId) >= 0) {
                    this._tournamentRoomArr.push(tempTournamentRoomArr[i]);
                }
            }
            if (this._tournamentRoomArr.length != 0) {
                this.tournamentRoomListHandler.setRoomList(this._tournamentRoomArr);
                applicationFacade._lobby.handleTournamentList(this._tournamentRoomArr, false);
            }
            var roomZone = sfs.getRoomByName("TournamentLobby");
            sfs.send(new SFS2X.Requests.System.ExtensionRequest("tournamentRequest.tournamentDataReq", {}, roomZone));
            var tourOpenRooms = applicationFacade._tourOpenRooms;
            if (tourOpenRooms.length > 0) {
                this.sendTournamentSubscribeReq();
            }
        } else if (event.groupId == "gameRoom") {
            this.roomListHandler.roomListObj = {};
            if (applicationFacade._lobby && applicationFacade._lobby.filterParser)
                applicationFacade._lobby.filterParser.mGameList = [];
            var tmpRoomList = sfs.getRoomListFromGroup("gameRoom");
            for (var i = 0; i < tmpRoomList.length; i++) {
                if (rummyData.hasOwnProperty("domainId") && tmpRoomList[i].variables.domainId.value.indexOf(rummyData.domainId) >= 0) {
                    roomList.push(tmpRoomList[i]);
                }
            }
            if (roomList.length != 0) {
                this.roomListHandler.setRoomList(roomList);
                applicationFacade._lobby.handleTotalPlayer();
                applicationFacade._lobby.sendJoinRoomRequestForNextRoom();
            }

        } else if (event.groupId == "tournamentGameRoom") {
            if (applicationFacade && applicationFacade._tourOpenRooms) {
                var i = 0;
                var a = setInterval(function () {
                    if (applicationFacade._tourOpenRooms[i]) {
                        var reqObj = {};
                        console.log("tournament" + applicationFacade._tourOpenRooms[i]);
                        this.sendTournamentReq(TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_TAKESEAT_REQUEST, reqObj, applicationFacade._tourOpenRooms[i]);
                        i++;
                    } else {
                        window.clearInterval(a);
                    }
                }.bind(this), 1000);


                /*
                 for (var i = 0; i < applicationFacade._tourOpenRooms.length; i++) {
                 var reqObj = {};
                 console.log("tournament" + applicationFacade._tourOpenRooms[i]);
                 this.sendTournamentReq(TOURNAMENT_CONSTANTS.CMD_TOURNAMENT_TAKESEAT_REQUEST, reqObj, applicationFacade._tourOpenRooms[i]);
                 }*/

            }
        }
    },
    sendTournamentSubscribeReq: function () {
        /* if(!sfs.roomManager.containsGroup("tournamentLobby"))
         sfs.send(new SFS2X.Requests.System.SubscribeRoomGroupRequest("tournamentLobby"));*/

        if (!sfs.roomManager.containsGroup("tournamentRoom"))
            sfs.send(new SFS2X.Requests.System.SubscribeRoomGroupRequest("tournamentRoom"));
        if (!sfs.roomManager.containsGroup("tournamentGameRoom"))
            sfs.send(new SFS2X.Requests.System.SubscribeRoomGroupRequest("tournamentGameRoom"));
    },
    onGroupSubscribeError: function (event) {
        cc.log("Group subscription failed: " + event.errorMessage + "\n");
        if (!sfs.roomManager.containsGroup(this.GAME_ROOMS_GROUP_NAME))
            sfs.send(new SFS2X.Requests.System.SubscribeRoomGroupRequest(this.GAME_ROOMS_GROUP_NAME));
    },
    onGroupUnsubscribed: function (event) {
        cc.log("Group unsubscribed: " + event.groupId + "\n");
    },
    onGroupUnsubscribeError: function (event) {
        cc.log("Group unsubscribing failed: " + event.errorMessage + "\n");
    },
    getRoomById: function (id) {
        return sfs.getRoomById(parseInt(id));
    },
    onExtensionResponse: function (event) {
        var params = event.params;
        var cmd = event.cmd;

        /*if (cmd == SFSConstants.CMD_PLAYERDISCONNECTED) { //lobby, gametable
         this.processPlayerDisconnected(params);
         }*/
        applicationFacade._controlAndDisplay.setConnectionStatus(true, true);

        if (cmd != SFSConstants.CMD_PINGRESP) {
            cc.log("onExtensionResponse", cmd, event);
        }
        if (cmd == SFSConstants.CMD_PINGRESP) {
            applicationFacade._connectionChecker.pingResp("ping");
            return;
        }
        if (applicationFacade._connectionChecker)
            applicationFacade._connectionChecker.pingResp("any");
        if (cmd == SFSConstants.CMD_PLAYERCONNECT) {
            this.ispostPing = false;
            this.playerConnectRcvdTime = new Date().getTime();
            this.playerconnectRcvdTime = new Date().getTime();
        }
        if (cmd == SFSConstants.SERVER_RESTART) {
            cc.log("Server restart");
            var time = params["time"];
            this.startShutDownTimer(parseInt(time));
        }
        if (cmd == "logResp") {
            applicationFacade.handleLoginResp(params);
        }

        var room = null;
        if (event.sourceRoom)
            room = sfs.getRoomById(parseInt(event.sourceRoom));

        !room && cc.log("sfs room not found for ", event.sourceRoom);

        if (room && (room.groupId == "tournamentRoom" || room.groupId == "tournamentLobby")) {
            applicationFacade.handleTournamentExtensionResponse(cmd, room, params);
            return;
        }
        if (cmd == SFSConstants.CMD_ROOMPLSUPDATE || cmd == SFSConstants.CMD_ROOMINFO || cmd == SFSConstants._ROOMSTATE
            || cmd == SFSConstants.CMD_ROOM_GROUP_STR || cmd == SFSConstants.CMD_GROUP_TOT_PLS || cmd == SFSConstants.CMD_ZONEPLS
            || cmd == SFSConstants.CMD_CASHRESP || cmd == SFSConstants.CMD_PROMORESP || cmd == SFSConstants.BAL || cmd == SFSConstants.SERVER_RESTART
            || cmd == SFSConstants.CMD_UPDATE_BAL || cmd == SFSConstants.CMD_INSTANTJOIN || cmd == SFSConstants.IMAGE_STATUS || cmd == SFSConstants.BACKGROUND_UPDATE
            || cmd == SFSConstants.BG_TABLE_NAME || cmd == SFSConstants.DECK_UPDATE || cmd == SFSConstants.SERVER_TIME || cmd == SFSConstants.VIDEO_RESP
            || cmd == SFSConstants.ROOM_JOIN_RESPONSE || cmd == SFSConstants.CMD_roomPrize || cmd == SFSConstants.CMD_HC_SFS_RESTART || cmd == SFSConstants.CMD_NO_ROOM) {
            applicationFacade.handleLobbyExtensionResponse(cmd, room, params);

            if ((cmd == SFSConstants.SERVER_RESTART) || (cmd == SFSConstants.CMD_ROOMINFO)
                || cmd == SFSConstants.BACKGROUND_UPDATE || cmd == SFSConstants.BG_TABLE_NAME || cmd == SFSConstants.DECK_UPDATE
                || cmd == SFSConstants.SERVER_TIME || cmd == SFSConstants.VIDEO_RESP || cmd == SFSConstants.CMD_BONUSTOCASH || cmd == SFSConstants.CMD_HC_SFS_RESTART) {
                applicationFacade.setServerMsg(cmd, params);
            }

        } else {
            if (room)applicationFacade.handleTableExtensionResponse(cmd, room, params);
        }

    },
    roomLeave: function (room) {
        if (applicationFacade._controlAndDisplay._onlineStatus) {
            sfs.send(new SFS2X.Requests.System.LeaveRoomRequest(room));
            return false;
        }
        return true;
    },
    startShutDownTimer: function (time) {
        var tempScene = cc.director.getRunningScene()._name;
        if (tempScene == " lobby" && tempScene != "gameTable") {

        }
    },
    updateLobbyBal: function (data) {

        this.promoBalance = data[SFSConstants.GAMEIANT_PROMO];
        this.cashBalance = data[SFSConstants.GAMEIANT_CASH];
        // update weaver login balance
        rummyData.playerLoginInfo.walletBean.practiceBalance = this.promoBalance;
        rummyData.playerLoginInfo.walletBean.cashBalance = this.cashBalance;
        this.checkbalAmount(this.promoBalance, this.cashBalance);

    },
    onPublicMessage: function (event) {
        var sender = event.sender;
        var senderName = sender.name;
        var msg = event.message;
        var fromRoom = event.room;
        applicationFacade.handleGameRoomChat(fromRoom.id, senderName, msg);

    },
    joinRoom: function (roomId, seatId) {
        var obj = {};
        obj["cId"] = roomId;
        obj["seatNo"] = seatId;
        if (seatId) {
            sfs.send(new SFS2X.Requests.System.JoinRoomRequest(roomId, obj, -1, true));
        } else {
            sfs.send(new SFS2X.Requests.System.JoinRoomRequest(roomId, null, -1, true));
        }
        cc.log("table room joined");
    },
    populateRoomsList: function () {
        var rooms = sfs.roomManager.getRoomList();
        var rooms1 = sfs.getRoomListFromGroup(this.GAME_ROOMS_GROUP_NAME);
        var source = [];
        cc.log("room length " + rooms.length);
        // var selectedRoom = ($("#roomList").jqxListBox("selectedIndex") > -1 ?
        // $("#roomList").jqxListBox("getSelectedItem").originalItem.roomObj.id
        // : null);
        var selectedIndex;
        var index = 0;

        if (sfs.lastJoinedRoom != null && sfs.lastJoinedRoom.name == this.LOBBY_ROOM_NAME) {
            for (var r in rooms) {
                var room = rooms[r];
                //cc.log(room.isGame);
                if (room.isGame && !room.isPasswordProtected && !room.isHidden) {
                    var players = room.getUserCount();
                    var maxPlayers = room.maxUsers;
                    var spectators = room.getSpectatorCount();
                    var maxSpectators = room.maxSpectators;
                    //sfs.send(new SFS2X.Requests.System.JoinRoomRequest("ashhh"));
                    /*
                     * var item = {}; item.html = "<div><p class='itemTitle game'>
                     * <strong>" + room.name + "</strong></p>" + "<p class='itemSub'>Players: " +
                     * players + "/" + maxPlayers + " | Spectators: " +
                     * spectators + "/" + maxSpectators + "</p></div>"
                     * item.title = room.name; item.roomObj = room;
                     */
                    // source.push(item);

                    // if (room.id == selectedRoom)
                    // selectedIndex = index;

                    index++;
                }
            }
        }
    },
    loginRequest: function () {

        var tablet = /Tablet|iPad/i.test(navigator.userAgent);
        var duration = parseInt(( new Date() - this._lobbyLoadTime ) / 1000);
        var deviceType;
        if (tablet) {
            deviceType = "Tablet";
        } else if (cc.sys.isMobile) {
            deviceType = "Mobile";
        } else {
            deviceType = "PC";
        }
// "device", "osType", "browser", "model",  "platform", "api"
        var params = {
            "playerId": parseInt(rummyData.playerId),
            "lobbyLoadTime": duration,
            "api": gameConfig.appVersion,
            "platform": "HTML5" + gameConfig.appVersion,
            "device": deviceType,
            "osType": cc.sys.os
        };
        cc.log("Login Initiated!" +
            "\n\tPlayerId: " + params.playerId +
            "\n\tDevice: " + params.device +
            "\n\tDuration:" + params.lobbyLoadTime);

        cc.log("OutgoingData" +
            "\n\tNickName: " + rummyData.nickName +
            "\n\tSFS password: " + SFSConstants.SFS_PASSWORD +
            "\n\tSFS Zone:" + SFSConstants.SFS_ZONE);

        sfs.send(new SFS2X.Requests.System.LoginRequest(
            rummyData.nickName,
            SFSConstants.SFS_PASSWORD,
            params,
            SFSConstants.SFS_ZONE));
    },
    joinSeatPoint: function (seatId, isNoBal, betAmt) {
        var obj = {};
        obj[SFSConstants.FLD_SEATNO] = parseInt(seatId);
        obj[SFSConstants.FLD_IS_NO_BAL] = isNoBal;
        obj[SFSConstants.FLD_AMOUNT] = betAmt;
        sfs.send(new SFS2X.Requests.System.ExtensionRequest(SFSConstants.CMD_CONFIRM, obj, sfs.getRoomById(this.SFS_RoomID)));
    },
    sendChatMsg: function (msg, room) {
        sfs.send(new SFS2X.Requests.System.PublicMessageRequest(msg, null, room));
    },
    logoutRequest: function () {
        cc.log("logout request initiated");
        sfs.send(new SFS2X.Requests.System.LogoutRequest());
    },
    roomListHandler: {
        roomListObj: {},
        addRoom: function (id, roomObj) {
            this.roomListObj[id] = roomObj;
            var g = this.roomListObj[id]
        },
        deleteRoom: function (id) {
            delete this.roomListObj[id];
        },
        setRoomList: function (roomListArray) {
            for (var i = 0; i < roomListArray.length; i++) {
                this.roomListObj[roomListArray[i].id] = roomListArray[i];
            }
        }
    },
    tournamentRoomListHandler: {
        tournamentRoomListObj: {},
        addRoom: function (id, roomObj) {
            this.tournamentRoomListObj[id] = roomObj;
        },
        deleteRoom: function (id) {
            delete this.tournamentRoomListObj[id];
        },
        setRoomList: function (tRoomListArray) {
            for (var i = 0; i < tRoomListArray.length; i++) {
                this.tournamentRoomListObj[tRoomListArray[i].id] = tRoomListArray[i];
            }
        }
    },
    cleanUp: function () {
        this.tournamentRoomListHandler.tournamentRoomListObj = {};
        // this.roomListHandler.roomListObj = {};
        //  this._tournamentRoomArr = [];
    },
    playResp: function (nextRound) {
        var obj = {};
        obj[SFSConstants.FLD_IS_NEXT] = nextRound;
        sfs.send(new SFS2X.Requests.System.ExtensionRequest(SFSConstants.CMD_PLAY_RESP, obj, sfs.getRoomById(this.SFS_RoomID)));
    },
    sendReqFromLobby: function (cmd, dataObj) {
        var r;
        if (sfs) {
            r = sfs.getRoomByName("Lobby");
            if (r && r.id != -1)
                sfs.send(new SFS2X.Requests.System.ExtensionRequest("lobbyRequest." + cmd, dataObj, r));
        }
    },
    sendReqFromRoom: function (cmd, dataObj, roomId) {
        cc.log("sendReqFromRoom---->", cmd);
        var r;
        if (sfs) {
            cc.log(roomId);
            r = sfs.getRoomById(roomId);
            if (r && r.id != -1)
                sfs.send(new SFS2X.Requests.System.ExtensionRequest("roomRequest." + cmd, dataObj, r));
        }
    },
    sendReqFromTourRoom: function (cmd, dataObj, roomId) {
        cc.log("sendReqFromRoom---->", cmd);
        var r;
        dataObj.xml = "xml";
        dataObj.roomId = roomId;
        if (sfs) {
            cc.log(roomId);
            r = sfs.getRoomById(roomId);
            if (r && r.id != -1)
                sfs.send(new SFS2X.Requests.System.ExtensionRequest("tournamentGameRequest." + cmd, dataObj, r));
        }
    },
    sendTournamentReq: function (cmd, dataObj, roomId) {
        dataObj["sfsId"] = roomId;
        var roomZone = sfs.getRoomByName("TournamentLobby");
        var room = sfs.getRoomById(roomId);
        if (room && cmd == TOURNAMENT_CONSTANTS.PLAYER_ELIGIBLE_REQ) {
            if (room.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_ID).value == 16106 ||
                room.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_ID).value == 16102 ||
                room.getVariable(TOURNAMENT_CONSTANTS.TOURNAMENT_ID).value == 16103) {
                applicationFacade._lobby.tournamentLobby.tournamentRoomListManager.getPlayerNonFlashPopUp(room);
            } else
                sfs.send(new SFS2X.Requests.System.ExtensionRequest("tournamentRequest." + cmd, dataObj, roomZone));

        } else
            sfs.send(new SFS2X.Requests.System.ExtensionRequest("tournamentRequest." + cmd, dataObj, roomZone));
    },
    onPingPong: function (event) {
        // cc.log("onPingPong>>>>>>>", event);
    },
    pingZone: function (cmd, dataObj) {
        if (cmd == undefined)
            cmd = "zoneRequest.ping";
        if (sfs.isConnected()) {
            var roomZone = sfs.getRoomByName("RummyZoneExt");
            sfs.send(new SFS2X.Requests.System.ExtensionRequest(cmd, dataObj, roomZone));
        }
    },
    disconnectUser: function () {
        sfs.disconnect();
    },
    isMeInRoom: function (rid) {
        var flag;
        var rm = sfs.getRoomById(rid);
        var me;
        if (rm) {
            if (rm.containsUser(sfs.mySelf))
                return true;
        }
        return false;
    },

    onUserVarsUpdate: function (event) {
        var user = event.user;
        cc.log("user variable update");
    }
});
/*
 getInstance: function () {
 if (!this._sharedGame) {
 this._sharedGame = new SFS2XClient();
 } else {
 return this._sharedGame;
 }
 return null;
 },*/
