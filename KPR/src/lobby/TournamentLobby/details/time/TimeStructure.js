/**
 * Created by stpl on 1/16/2017.
 */
var TimeStructure = cc.Layer.extend({
    ctor: function (context, room) {

        this._super();

        this._tournamentDetail = context;

        this.setContentSize(cc.size(720 * scaleFactor, 390 * scaleFactor));
        this.setPosition(cc.p(220 * scaleFactor, 3 * scaleFactor));

        var rect = new cc.DrawNode();
        rect.drawRect(cc.p(1 * scaleFactor, 0), cc.p(719 * scaleFactor, 30 * scaleFactor), cc.color(235, 235, 235), 1, cc.color(cc.color(235, 235, 235)));
        rect.setPosition(cc.p(0, this.height - 31 * scaleFactor));
        this.addChild(rect, 1);

        // on header strip
        var verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(75 * scaleFactor, 359 * scaleFactor), cc.p(75 * scaleFactor, 389 * scaleFactor), 0.5, cc.color(255, 255, 255));
        this.addChild(verticalLine, 2);

        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(247 * scaleFactor, 359 * scaleFactor), cc.p(247 * scaleFactor, 389 * scaleFactor), 0.5, cc.color(255, 255, 255));
        this.addChild(verticalLine, 2);

        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(328 * scaleFactor, 359 * scaleFactor), cc.p(328 * scaleFactor, 389 * scaleFactor), 0.5, cc.color(255, 255, 255));
        this.addChild(verticalLine, 2);

        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(417 * scaleFactor, 359 * scaleFactor), cc.p(417 * scaleFactor, 389 * scaleFactor), 0.5, cc.color(255, 255, 255));
        this.addChild(verticalLine, 2);

        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(586 * scaleFactor, 359 * scaleFactor), cc.p(586 * scaleFactor, 389 * scaleFactor), 0.5, cc.color(255, 255, 255));
        this.addChild(verticalLine, 2);

        var label;
        label = new cc.LabelTTF("Level", "RobotoBold", 18 * scaleFactor);
        label.setScale(0.7);
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(20 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        rect.addChild(label);

        label = new cc.LabelTTF("Schedule Time", "RobotoBold", 18 * scaleFactor);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(92 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        rect.addChild(label);

        label = new cc.LabelTTF("Point Value", "RobotoBold", 18 * scaleFactor);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(254 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        rect.addChild(label);

        label = new cc.LabelTTF("Qualify", "RobotoBold", 18 * scaleFactor);
        label.setScale(0.7);
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(350 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        rect.addChild(label);

        label = new cc.LabelTTF("Re-Buy", "RobotoBold", 18 * scaleFactor);
        label.setScale(0.7);
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(435 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        rect.addChild(label);

        label = new cc.LabelTTF("Reload Chips", "RobotoBold", 18 * scaleFactor);
        label.setScale(0.7);
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(607 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        rect.addChild(label);

        this.timeStructureListView = new ccui.ListView();
        this.timeStructureListView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        this.timeStructureListView.setGravity(ccui.ListView.GRAVITY_CENTER_HORIZONTAL);
        this.timeStructureListView.setTouchEnabled(true);
        this.timeStructureListView.setItemsMargin(5);
        this.timeStructureListView.setScrollBarAutoHideEnabled(true);
        this.timeStructureListView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.timeStructureListView.setBackGroundColor(cc.color(255, 255, 255));
        this.timeStructureListView.setContentSize(cc.size(this.width - 2 * scaleFactor, 290 * scaleFactor));
        this.timeStructureListView.setPosition(cc.p(1 * scaleFactor, 60 * scaleFactor));

        this.addChild(this.timeStructureListView);

        this.initRoom(room);
    },
    initRoom: function (room) {

        var grpType = TOURNAMENT_CONSTANTS.TOURNAMENT_GRP_TYPE;
        this.trnmntRoom = room;

        var structStr = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.STRUCTURE).value;
        var curLevel = this.trnmntRoom.getVariable(TOURNAMENT_CONSTANTS.CUR_LEVEL).value;

        if (structStr && (structStr == "" || structStr == " "))
            return;

        var tempStrArr = structStr.split(";");
        var levelStrArr = tempStrArr.reverse();
        var levelDisplayArr;
        var totLevel = levelStrArr.length;

        var breakStrip;
        this._tournamentDetail.TOT_ROUND = totLevel;
        this.timeStructureListView.removeAllChildrenWithCleanup(true);

        for (var i = 0; i < totLevel; i++) {
            levelDisplayArr = levelStrArr[i].split(",");
            if (levelDisplayArr[6]) {
                breakStrip = new TeaBreak(this, levelDisplayArr[6]);
                this.timeStructureListView.pushBackCustomItem(breakStrip);
            }
            var strip = new TimeStructureBar(this, levelDisplayArr);
            this.timeStructureListView.pushBackCustomItem(strip);

            if (curLevel == parseInt(levelDisplayArr[0]))
                strip.setState(2);
            else
                strip.setState(1);

        }
    }
});
var TimeStructureBar = ccui.Layout.extend({
    ctor: function (context, levelDisplayArr) {
        this._super();
        this.setContentSize(cc.size(context.width, 30 * scaleFactor));
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.setBackGroundColor(cc.color(235, 235, 235));

        var verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(75 * scaleFactor, 0), cc.p(75 * scaleFactor, this.height), 0.5, cc.color(220, 220, 220));
        this.addChild(verticalLine, 2);

        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(245 * scaleFactor, 0), cc.p(245 * scaleFactor, this.height), 0.5, cc.color(220, 220, 220));
        this.addChild(verticalLine, 2);

        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(328 * scaleFactor, 0), cc.p(328 * scaleFactor, this.height), 0.5, cc.color(220, 220, 220));
        this.addChild(verticalLine, 2);

        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(417 * scaleFactor, 0), cc.p(417 * scaleFactor, this.height), 0.5, cc.color(220, 220, 220));
        this.addChild(verticalLine, 2);

        verticalLine = new cc.DrawNode();
        verticalLine.drawSegment(cc.p(586 * scaleFactor, 0), cc.p(586 * scaleFactor, this.height), 0.5, cc.color(220, 220, 220));
        this.addChild(verticalLine, 2);

        this.setName("strip" + levelDisplayArr[0]);

        this.labelArr = [];

        var label;
        label = new cc.LabelTTF("Level " + levelDisplayArr[0], "RobotoRegular", 16 * scaleFactor);
        label.setScale(0.7);
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(20 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label);
        this.labelArr.push(label);

        label = new cc.LabelTTF(levelDisplayArr[1], "RobotoRegular", 16 * scaleFactor);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(92 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label);
        this.labelArr.push(label);

        label = new cc.LabelTTF(levelDisplayArr[2], "RobotoRegular", 16 * scaleFactor);
        label.setAnchorPoint(0, 0.5);
        label.setScale(0.7);
        label.setPosition(cc.p(254 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label);
        this.labelArr.push(label);

        label = new cc.LabelTTF(levelDisplayArr[3], "RobotoRegular", 16 * scaleFactor);
        label.setScale(0.7);
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(350 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label);
        this.labelArr.push(label);

        label = new cc.LabelTTF("Re-Buy", "RupeeFordian", 16 * scaleFactor);
        label.setScale(0.7);
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(435 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label);
        this.labelArr.push(label);

        if (levelDisplayArr[4] == "N/A")
            label.setString(levelDisplayArr[4]);
        else
            label.setString(PrizeStringCalculator.calculateStructPrzStr(levelDisplayArr[4]));

        label = new cc.LabelTTF(levelDisplayArr[5], "RobotoRegular", 16 * scaleFactor);
        label.setScale(0.7);
        label.setAnchorPoint(0, 0.5);
        label.setPosition(cc.p(607 * scaleFactor, 15 * scaleFactor));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label);
        this.labelArr.push(label);

    },
    setState: function (stateInteger) {
        var i;
        if (stateInteger == 1) {
            this.setBackGroundColor(cc.color(235, 235, 235));
            for (i = 0; i < this.labelArr.length; i++) {
                this.labelArr[i].setColor(cc.color(0, 0, 0));
            }
        }
        else if (stateInteger == 2) {
            this.setBackGroundColor(cc.color(124, 141, 7));
            for (i = 0; i < this.labelArr.length; i++) {
                this.labelArr[i].setColor(cc.color(255, 255, 255));
            }
        }
    }
});
var TeaBreak = ccui.Layout.extend({
    ctor: function (context, time) {
        this._super();
        this.setContentSize(cc.size(context.width, 30 * scaleFactor));
        this.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.setBackGroundColor(cc.color(235, 222, 206));

        var label = new cc.LabelTTF(time + " Minute Break Time", "RobotoRegular", 16 * scaleFactor);
        label.setScale(0.7);
        label.setAnchorPoint(1, 0.5);
        label.setPosition(cc.p(674 * scaleFactor, this.height / 2));
        label.setColor(cc.color(0, 0, 0));
        this.addChild(label);

        var tea = new cc.Sprite(spriteFrameCache.getSpriteFrame("Coffee.png"));
        tea.setPosition(cc.p(690 * scaleFactor, this.height / 2));
        this.addChild(tea);

    }
});