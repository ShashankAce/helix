/**
 * Created by stpl on 12/5/2016.
 */
"use strict";
var GroupValidity = {
    cardsArr: [],
    _wildCardStr: "",
    totCards: null,
    ctor: function () {

    },
    checkFrGroup: function (str, _wildCrd, totCrd) {
        this._wildCardStr = _wildCrd;
        this.totCards = totCrd;
        var cardArr;
        cardArr = str.split(",");
        if (cardArr.length == 2) {
            if ((cardArr[0] == cardArr[1]) || ((cardArr[0] == "Joker") && (cardArr[1] == "Joker")))
                return "Doublee";
            else
                return "Invalid";
        }
        else if ((cardArr.length == 3) && (((cardArr[0] == cardArr[1]) && (cardArr[1] == cardArr[2])) || ((cardArr[0] == "Joker") && (cardArr[1] == "Joker") && (cardArr[2] == "Joker"))))
            return "Tunnela";
        else if (cardArr.length == 8) {
            var nonJokerCount = 0;
            for (var i = 0; i < cardArr.length; i++) {
                if (!this.setJoker(cardArr[i], this._wildCardStr, this.totCards))
                    nonJokerCount = nonJokerCount + 1;
            }
            if (nonJokerCount == 0)
                return "Jokers";
        }
        if ((cardArr.length > 2) && (cardArr.length <= 8)) {
            var cardObj = [];
            var diffNumCount = 0;
            var diffSuitCount = 0;

            for (i = 0; i < cardArr.length; i++) {
                var num = cardArr[i].split("#")[1];
                if (num == "A")
                    num = "1";
                else if (num == "K")
                    num = "13";
                else if (num == "Q")
                    num = "12";
                else if (num == "J")
                    num = "11";
                var obj = {};
                obj.cardStr = cardArr[i];
                obj.suit = cardArr[i].split("#")[0];
                obj.numArr = parseInt(num);
                obj.isJoker = this.setJoker(cardArr[i], this._wildCardStr, this.totCards);

                cardObj.push(obj);
            }
            var str = this.checkFrSeq(cardObj);
            if (str != "Invalid")
                return str;
            else
                return this.checkFrSet(cardObj);
        }
        return "Invalid";
    },
    checkFrSeq: function (cardObj) {
        cardObj = this.sortOnNumArr(cardObj);
        if (this.isSuitSame(cardObj)) {
            cardObj = this.sortOnNumArr(cardObj);
            var count = 0;
            if ((cardObj[0].numArr == 1)) {
                if (cardObj[1].numArr >= 7)//A,K,Q,J,10
                {
                    cardObj[0].numArr = 14;
                    cardObj = this.sortOnNumArr(cardObj);
                }
            }
            for (var i = 0; i < cardObj.length - 1; i++) {
                if (cardObj[i].numArr != (cardObj[i + 1].numArr - 1))
                    count += 1;
            }
            if (count == 0)
                return "Pure Seq";
        }

        var nonJokerCards = [];
        var jokerCards = [];

        for (var i = 0; i < cardObj.length; i++) {
            if (cardObj[i].isJoker) {
                jokerCards.push(cardObj[i]);
            }
            else {
                nonJokerCards.push(cardObj[i]);
            }
        }

        nonJokerCards = this.sortOnNumArr(nonJokerCards);

        if ((nonJokerCards.length == 1) || (nonJokerCards.length == 0))
            return "Seq";

        if ((nonJokerCards.length > 1) && (nonJokerCards[0].numArr == 1)) {
            if (nonJokerCards[1].numArr >= 7) {
                nonJokerCards[0].numArr = 14;
                nonJokerCards = this.sortOnNumArr(nonJokerCards);
            }
        }

        if (this.isSuitSame(nonJokerCards)) {
            if (this.isNumSame(nonJokerCards) || this.isAnyCardSame(nonJokerCards))
                return "Invalid";
            else {
                var lastNum = nonJokerCards[0].numArr + cardObj.length - 1;
                if (jokerCards.length > 0) {
                    if (nonJokerCards[nonJokerCards.length - 1].numArr <= lastNum)
                        return "Seq";
                }
                else if (nonJokerCards[nonJokerCards.length - 1].numArr == lastNum)
                    return "Seq";
                else
                    return "Invalid";
            }
        }

        return "Invalid";
    },
    checkFrSet: function (cardObj) {
        cardObj = this.sortOnNumArr(cardObj);
        var nonJokerCards = [];
        var jokerCards = [];

        for (var i = 0; i < cardObj.length; i++) {
            if (cardObj[i].isJoker) {
                jokerCards.push(cardObj[i]);
            }
            else {
                nonJokerCards.push(cardObj[i]);
            }
        }
        if ((this.isNumSame(nonJokerCards)) && (nonJokerCards.length < 5)) {
            if (this.isSuitSame(nonJokerCards) || this.isAnyCardSame(nonJokerCards))
                return "Invalid";
            var count = 0;
            for (i = 0; i < nonJokerCards.length - 1; i++) {
                if (nonJokerCards[i].suit == nonJokerCards[i + 1].suit)
                    count += 1;
            }
            if (count == 0)
                return "Set";
            else
                return "Invalid";
        }
        else
            return "Invalid";
    },
    checkFrGroup13: function (str, _wildCrd, totCrd) {
        this._wildCardStr = _wildCrd;
        this.totCards = totCrd;
        var cardArr;
        cardArr = str.split(",");
        if (cardArr.length <= 2) {
            return "Invalid";
        }
        if ((cardArr.length > 2) && (cardArr.length < 8)) {
            var cardObj = [];
            var diffNumCount = 0;
            var diffSuitCount = 0;

            for (var i = 0; i < cardArr.length; i++) {
                var num = cardArr[i].split("#")[1];
                if (num == "A")
                    num = "1";
                else if (num == "K")
                    num = "13";
                else if (num == "Q")
                    num = "12";
                else if (num == "J")
                    num = "11";
                var obj = {};
                obj.cardStr = cardArr[i];
                obj.suit = cardArr[i].split("#")[0];
                obj.numArr = parseInt(num);
                obj.isJoker = this.setJoker(cardArr[i], this._wildCardStr, this.totCards);

                cardObj.push(obj);
            }
            var str = this.checkFrSeq13(cardObj);
            if (str != "Invalid")
                return str;
            else
                return this.checkFrSet(cardObj);
        }
        return "Invalid";
    },
    checkFrSeq13: function (cardObj) {
        cardObj = this.sortOnNumArr(cardObj);
        var i;
        if (this.isSuitSame(cardObj)) {
            cardObj = this.sortOnNumArr(cardObj);
            var count = 0;

            if ((cardObj[0].numArr == 1)) {
                if (cardObj[1].numArr > 7)//A,K,Q,J,10
                {
                    cardObj[0].numArr = 14;
                    cardObj = this.sortOnNumArr(cardObj);
                }
            }
            for (i = 0; i < cardObj.length - 1; i++) {
                if (cardObj[i].numArr != (cardObj[i + 1].numArr - 1))
                    count += 1;
            }
            if (count == 0)
                return "Pure Seq";
        }

        var nonJokerCards = [];
        var jokerCards = [];

        for (i = 0; i < cardObj.length; i++) {
            if (cardObj[i].isJoker) {
                jokerCards.push(cardObj[i]);
            }
            else {
                nonJokerCards.push(cardObj[i]);
            }
        }

        nonJokerCards = this.sortOnNumArr(nonJokerCards);

        if ((nonJokerCards.length == 1) || (nonJokerCards.length == 0))
            return "Seq";

        if ((nonJokerCards.length > 1) && (nonJokerCards[0].numArr == 1)) {
            if (nonJokerCards[1].numArr > 7) {
                nonJokerCards[0].numArr = 14;
                nonJokerCards = this.sortOnNumArr(nonJokerCards);
            }
        }

        if (this.isSuitSame(nonJokerCards)) {
            if (this.isNumSame(nonJokerCards) || this.isAnyCardSame(nonJokerCards))
                return "Invalid";
            else {
                var lastNum = nonJokerCards[0].numArr + cardObj.length - 1;
                if (jokerCards.length > 0) {
                    if (nonJokerCards[nonJokerCards.length - 1].numArr <= lastNum)
                        return "Seq";
                }
                else if (nonJokerCards[nonJokerCards.length - 1].numArr == lastNum)
                    return "Seq";
                else
                    return "Invalid";
            }
        }

        return "Invalid";
    },
    setJoker: function (cardStr, wildCrd, totCrds) {
        this._wildCardStr = wildCrd;
        this.totCards = totCrds;
        if (this.totCards == 22)
            return this.calJoker21(cardStr);
        else
            return this.calJoker13(cardStr);
    },
    calJoker13: function (cardStr) {
        var isJoker;
        if (!this._wildCardStr || this._wildCardStr == "")
            return false;
        if (cardStr == "Joker")
            return true;
        if (this._wildCardStr == "Joker")    // K-A-2
        {
            if (cardStr.search("A") != -1) //joker  frame no 3
            {
                isJoker = true;
            }
            return isJoker;
        }
        var crdArr = this._wildCardStr.split("#");
        var suitStr = crdArr[0];
        var cardNo = crdArr[1];
        if (cardStr.search(cardNo) != -1) {
            isJoker = true;
        }
        return isJoker;
    },
    calJoker21: function (cardStr) {
        var isJoker;
        if (!this._wildCardStr || this._wildCardStr == "")
            return false;
        if (cardStr == "Joker")
            return true;
        if (this._wildCardStr == "Joker")    // K-A-2
        {
            if (cardStr.search("A") != -1) //joker  frame no 3
            {
                isJoker = true;
            }
            if (cardStr == "S#2") //upjoker  frame no 1
            {
                isJoker = true;
            }
            if (cardStr == "S#K") //dwnjoker  frame no 2
            {
                isJoker = true;
            }
            return isJoker;
        }
        var crdArr = this._wildCardStr.split("#");
        var suitStr = crdArr[0];
        var cardNo = crdArr[1];
        var upJ;
        var dwnJ;
        if (cardStr.search(cardNo) != -1) {
            isJoker = true;
        }
        if (cardStr.search(suitStr) != -1) {
            if (cardNo == "A") {
                upJ = suitStr + "#" + "2";
                dwnJ = suitStr + "#" + "K";
            }
            else if (cardNo == "J") {
                upJ = suitStr + "#" + "Q";
                dwnJ = suitStr + "#" + "10";
            }
            else if (cardNo == "Q") {
                upJ = suitStr + "#" + "K";
                dwnJ = suitStr + "#" + "J";

            }
            else if (cardNo == "K") {
                upJ = suitStr + "#" + "A";
                dwnJ = suitStr + "#" + "Q";
            }
            else {
                var no = parseInt(cardNo);
                upJ = suitStr + "#" + (no == 10 ? "J" : (no + 1) + "");
                dwnJ = suitStr + "#" + (no == 2 ? "A" : (no - 1) + "");
            }
            if (upJ == cardStr) // up joker
            {
                isJoker = true;
            }
            if (dwnJ == cardStr) // dwn joker
            {
                isJoker = true;
            }
        }
        return isJoker;
    },
    isSuitSame: function (arr) {
        arr = this.sortOnSuit(arr);
        var count = 0;
        for (var i = 0; i < arr.length - 1; i++) {
            if (arr[i].suit != arr[i + 1].suit) {
                count += 1;
            }
        }
        if (count == 0)
            return true;
        else
            return false;
    },
    isNumSame: function (arr) {
        arr = this.sortOnNumArr(arr);
        var count = 0;
        for (var i = 0; i < arr.length - 1; i++) {
            if (arr[i].numArr != arr[i + 1].numArr) {
                count += 1;
            }
        }
        if (count == 0)
            return true;
        else
            return false;
    },
    isAnyCardSame: function (arr) {
        var count = 0;
        arr = this.sortOnCardStr(arr);
        for (var i = 0; i < arr.length - 1; i++) {
            if (arr[i].cardStr == arr[i + 1].cardStr) {
                count += 1;
            }
        }
        arr = this.sortOnNumArr(arr);
        if (count == 0)
            return false;
        else
            return true;
    },
    sortOnCardStr: function (arr) {
        arr.sort(function (a, b) {
            return a.cardStr == b.cardStr ? 0 : a.cardStr < b.cardStr ? -1 : 1;
        });
        /*arr.sort(function (a, b) {
         var value = a.cardStr - b.cardStr;
         return (value);
         });*/
        return arr;
    },
    sortOnNumArr: function (arr) {
        arr.sort(function (a, b) {
            var value = a.numArr - b.numArr;
            return (value);
        });
        return arr;
    },
    sortOnSuit: function (arr) {
        arr.sort(function (a, b) {
            return a.suit == b.suit ? 0 : a.suit < b.suit ? -1 : 1;
        });
        /*arr.sort(function (a, b) {
         var value = a.suit - b.suit;
         return (value);
         });*/
        return arr;
    }
};

var GroupSymbol = cc.Node.extend({
    ctor: function (startP, totalWidth) {
        this._super();

        this.txt = new cc.LabelTTF("GROUPED", "RobotoBold", 11 * 2 * scaleFactor);
        this.txt.setColor(new cc.Color(84, 122, 1));
        this.txt.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.txt.setScale(0.5);
        this.txt.y = 0;
        this.addChild(this.txt, 1);

        var x = this.txt.width / 4 - 5, y = 0;
        var width = (totalWidth / 2 + 10) - this.txt.width / 4;
        var height = 15;
        var r = x + width;
        var b = y + height;
        var radius = 3;

        var vertices = [
            {x: x + radius, y: y},
            {x: r - radius, y: y},
            {cpx: r, cpy: y, x: r, y: y + radius},
            {x: r, y: b - radius}
        ];
        var roundRect1 = new cc.DrawNode(); // right side
        roundRect1.drawArc(vertices, 1, radius, '', cc.color(84, 122, 1), false);
        this.addChild(roundRect1, 20);

        x = -this.txt.width / 4 + 5;
        y = 0;
        height = 15;
        r = x - width;
        b = y + height;
        radius = 3;

        var vertices2 = [
            {x: x - radius, y: y},
            {x: r + radius, y: y},
            {cpx: r, cpy: y, x: r, y: y + radius},
            {x: r, y: b - radius}
        ];

        var roundRect2 = new cc.DrawNode(); // left side
        roundRect2.drawArc(vertices2, 1, radius, '', cc.color(84, 122, 1), false);
        this.addChild(roundRect2, 20);
    }
});


var ShowSeq = cc.Scale9Sprite.extend({
    _name: "ShowSeq",
    _gWidth: 75,
    ctor: function () {
        this._super(spriteFrameCache.getSpriteFrame("SeqBgCheck.png"), cc.rect(0, 0, 75 * scaleFactor, 21 * scaleFactor));
        this.setCapInsets(cc.rect(24 * scaleFactor, 6 * scaleFactor, 24 * scaleFactor, 6 * scaleFactor));
        this.setContentSize(cc.size(75 * scaleFactor, 21 * scaleFactor));


        this.txt = new cc.LabelTTF("", "RobotoBold", 20 * scaleFactor);
        this.txt.setPosition(cc.p(45 * scaleFactor, this.height / 2));
        this.txt.setColor(new cc.Color(133, 139, 135));
        this.txt.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
        this.txt.setScale(0.5);
        this.addChild(this.txt, 1);
    },
    showValidity: function (validity) {
        this.check = new cc.Sprite(spriteFrameCache.getSpriteFrame("SeqBgCheck.png"));
        this.txt.setString(validity);
        /*var size = this.txt.getContentSize();
         this._gWidth = size + 40;
         this.setContentSize(size + 40, 21);*/
        validity == "Invalid" ? this.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("SeqBgCross.png")) :
            this.check.initWithSpriteFrame(spriteFrameCache.getSpriteFrame("SeqBgCheck.png"));
    }
});
