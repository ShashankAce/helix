/**
 * Created by stpl on 1/30/2017.
 */
"use strict";
var MaxTournamentPopup = Popup.extend({
    _name: "MaxTournamentPopup",
    _txt: null,
    ctor: function (context, str) {
        this._super(new cc.Size(450 * scaleFactor, 190 * scaleFactor), context);
        this._txt = str;
        this.headerLabel.setString("Maximum Table");

        var message = new cc.LabelTTF(str, "RobotoRegular", 15 * 2 * scaleFactor, cc.TEXT_ALIGNMENT_CENTER);
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        message.setScale(0.5);
        message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        message.setColor(cc.color(49, 49, 49));
        message.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(message, 10);

        var okButton = new CreateBtn("OkBtn", "OkBtnOver", "OkButton");
        okButton.setPosition(cc.p(this.width / 2, 40 * scaleFactor));
        this.addChild(okButton, 2, 100);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), okButton);
        cc.eventManager.addListener(this.popupTouchListener.clone(), okButton);
        return true;
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        if (target.getTag() == 100) {
            this.removeFromParent(true);
        }
    }
});
var TieBreakerPopup = Popup.extend({
    _name: "TieBreakerPopup",
    _txt: null,
    ctor: function (context, str) {
        this._super(new cc.Size(450 * scaleFactor, 130 * scaleFactor), context, true);
        this._txt = str;
        this.headerLabel.setString("Tie Breaker");

        var message = new cc.LabelTTF(str, "RobotoRegular", 15 * 2 * scaleFactor);
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        message.setScale(0.5);
        message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        message.setColor(cc.color(49, 49, 49));
        message.setPosition(cc.p(this.width / 2, this.whiteSpace / 2));
        this.addChild(message, 10);

        var closeButton = new CreateBtn("ResultClose", "ResultClose", "closeButton");
        closeButton.setPosition(this.width, this.height);
        closeButton.setName("closeButton");
        this.addChild(closeButton, 2, 50);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), closeButton);
        cc.eventManager.addListener(this.popupTouchListener.clone(), closeButton);
        return true;
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        if (target.tag == 50) {
            this.removeFromParent(true);
        }
    }
});

var RndLvlWinnerPopup = Popup.extend({
    _name: "RndLvlWinnerPopup",
    _txt: null,
    ctor: function (context, str) {
        this._super(new cc.Size(410 * scaleFactor, 200 * scaleFactor), context);
        this._txt = str;
        this.headerLabel.setString("Congratulations !!!");

        var message = new cc.LabelTTF(str, "RupeeFordian", 15 * 2 * scaleFactor, cc.TEXT_ALIGNMENT_CENTER);
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        message.setScale(0.5);
        message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        message.setColor(cc.color(49, 49, 49));
        message.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(message, 10);

        var okButton = new CreateBtn("OkBtn", "OkBtnOver", "OkButton");
        okButton.setPosition(cc.p(this.width / 2, 40 * scaleFactor));
        okButton.setName("OkButton");
        this.addChild(okButton, 2);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), okButton);
        cc.eventManager.addListener(this.popupTouchListener.clone(), okButton);
        return true;
    },
    onTouchEndedCallBack: function (touch, event) {
        this.removeFromParent(true);
    }
});
var LostPopup = Popup.extend({
    _name: "LostPopup",
    _txt: null,
    ctor: function (context, str) {
        this._super(new cc.Size(590 * scaleFactor, 190 * scaleFactor), context);
        this._txt = str;
        this.headerLabel.setString("KhelPlayRummy Message");
        cc.log("Message:", str);

        var message = new cc.LabelTTF(str, "RobotoRegular", 15 * 2 * scaleFactor, cc.TEXT_ALIGNMENT_CENTER);
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        message.setScale(0.5);
        message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        message.setColor(cc.color(49, 49, 49));
        message.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(message, 10);

        var okButton = new CreateBtn("OkBtn", "OkBtnOver", "OkButton");
        okButton.setPosition(cc.p(this.width / 2, 35 * scaleFactor));
        okButton.setName("OkButton");
        this.addChild(okButton, 2);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), okButton);
        cc.eventManager.addListener(this.popupTouchListener.clone(), okButton);
        return true;
    },
    onTouchEndedCallBack: function (touch, event) {
        this.removeFromParent(true);
    }
});
var EliminatedPopup = Popup.extend({
    _name: "EliminatedPopup",
    _txt: null,
    ctor: function (context, str) {
        this._super(new cc.Size(450 * scaleFactor, 180 * scaleFactor), context);
        this._txt = str;
        this.headerLabel.setString("Eliminated");

        var message = new cc.LabelTTF(str, "RobotoRegular", 15 * 2 * scaleFactor, cc.TEXT_ALIGNMENT_CENTER);
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        message.setScale(0.5);
        message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        message.setColor(cc.color(49, 49, 49));
        message.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(message, 10);

        var okButton = new CreateBtn("OkBtn", "OkBtnOver", "OkButton");
        okButton.setPosition(cc.p(this.width / 2, 40 * scaleFactor));
        okButton.setName("OkButton");
        this.addChild(okButton, 2);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), okButton);
        cc.eventManager.addListener(this.popupTouchListener.clone(), okButton);
        return true;
    },
    onTouchEndedCallBack: function (touch, event) {
        this.removeFromParent(true);
    }
});
var RebuyDonePopup = Popup.extend({
    _name: "RebuyDonePopup",
    _txt: null,
    ctor: function (context, str) {
        this._super(new cc.Size(450 * scaleFactor, 130 * scaleFactor), context);
        this._txt = str;
        this.headerLabel.setString("Account credit successful");

        var message = new cc.LabelTTF(str, "RobotoRegular", 15 * 2 * scaleFactor);
        message.setScale(0.5);
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        message.setColor(cc.color(49, 49, 49));
        message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        message.setPosition(cc.p(this.width / 2, this.whiteSpace / 2));
        this.addChild(message, 10);

        this.setPosition(cc.p(cc.winSize.width / 2, cc.winSize.height / 2 + 100 * scaleFactor));
        return true;
    }
});
var RebuyCashPopup = Popup.extend({
    _name: "rebuy",
    _txt: null,
    ctor: function (context, str, amountStr, size, flag) {
        if (!size)
            this._super(new cc.Size(450 * scaleFactor, 240 * scaleFactor), context);
        else
            this._super(size, context);
        this._txt = str;
        this.headerLabel.setString("Rebuy Chips");

        var message = new cc.LabelTTF("You don’t have enough chips to continue game/tournament.", "RobotoRegular", 15 * 2 * scaleFactor);
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        message.setScale(0.5);
        message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        message.setColor(cc.color(49, 49, 49));
        message.setPosition(cc.p(this.width / 2, this.height - 60 * scaleFactor));
        this.addChild(message, 10);

        var richText = new ccui.RichText();

        var txt = new ccui.RichElementText(1, cc.color(49, 49, 49), 255, "Do you want to buy " + str + " chips for ", "RobotoRegular", 15 * 2 * scaleFactor);
        if (!flag) {
            this.amtTF = new ccui.RichElementText(1, cc.color(49, 49, 49), 255, amountStr, "RupeeFordian", 15 * 2 * scaleFactor);
            richText.pushBackElement(txt);
            richText.pushBackElement(this.amtTF);
        } else if (flag == "true") {
            richText.pushBackElement(txt);
        }

        richText.setPosition(cc.p(this.width / 2, this.height - 83 * scaleFactor));
        richText.setScale(0.5);
        this.addChild(richText, 5);

        this.loaderIcon = new cc.Sprite(spriteFrameCache.getSpriteFrame("loader_grey.png"));
        this.loaderIcon.setScale(1.2);
        this.loaderIcon.setColor(cc.color(110, 134, 3));
        this.loaderIcon.setPosition(cc.p(this.width / 2, this.height - 130 * scaleFactor));
        this.addChild(this.loaderIcon, 12);
        this.loaderIcon.runAction(cc.rotateBy(1, 360, 0).repeatForever());

        var timeOut = this._gameRoom._rejoinWaitTime || 10;
        this.timeTF = new cc.LabelTTF(timeOut, "RobotoRegular", 13 * 2 * scaleFactor);
        this.timeTF.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.timeTF.setScale(0.5);
        this.timeTF.setColor(cc.color(110, 134, 3));
        this.timeTF.setPosition(cc.p(this.width / 2, this.height - 130 * scaleFactor));
        this.addChild(this.timeTF, 5);

        this.secondTF = new cc.LabelTTF("Seconds Remaining", "RobotoRegular", 13 * 2 * scaleFactor);
        this.secondTF.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this.secondTF.setScale(0.5);
        this.secondTF.setColor(cc.color(110, 134, 3));
        this.secondTF.setPosition(cc.p(this.width / 2, 75 * scaleFactor));
        this.addChild(this.secondTF, 5);

        this.showYes = new CreateBtn("yesBtn", "yesBtnOver", "showYes");
        this.showYes.setPosition(cc.p(this.width / 2 - GameConstants.popButtonMargin, 40 * scaleFactor));
        this.addChild(this.showYes, 5);

        this.showNo = new CreateBtn("noBtn", "noBtnOver", "showNo");
        this.showNo.setPosition(this.width / 2 + GameConstants.popButtonMargin, 40 * scaleFactor);
        this.addChild(this.showNo, 5);

        var loadTimer = setInterval(function () {
            this.timeTF.setString(timeOut);
            if (timeOut == 0 || timeOut < 0) {
                window.clearInterval(loadTimer);
                this._gameRoom._tournamentGamePopup.rejoinReq("no");
                this.removeFromParent(true);
            }
            --timeOut;
        }.bind(this), 1000);

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), this.showYes);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.showNo);

        cc.eventManager.addListener(this.popupTouchListener.clone(), this.showYes);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.showNo);
        return true;
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        switch (target._name) {
            case "showYes":
                this._gameRoom._tournamentGamePopup.rejoinReq("yes");
                this.removeFromParent(true);
                break;
            case "showNo":
                this._gameRoom._tournamentGamePopup.rejoinReq("no");
                this.removeFromParent(true);
                break;
        }
    }
});
var RebuyTktPopup = RebuyCashPopup.extend({
    _name: "rebuyTktPopup",
    ctor: function (context, str, amountStr) {
        this._super(context, str, amountStr, new cc.Size(500 * scaleFactor, 320 * scaleFactor), "true");

        this.radioOne = new RadioButton(this, "RadioOne");
        this.radioOne.setScale(0.98);
        this.radioOne.setSelected(true);
        this.radioOne.setPosition(cc.p(155 * scaleFactor, 170 * scaleFactor));
        this.addChild(this.radioOne);
        this._selectedRadioBtn = this.radioOne;

        var entryFee = new cc.LabelTTF(amountStr, "RupeeFordian", 15 * 2 * scaleFactor);
        entryFee.setScale(0.5);
        entryFee.setColor(cc.color(0, 0, 0));
        entryFee.setAnchorPoint(0, 0.5);
        entryFee.setPosition(175 * scaleFactor, 170 * scaleFactor);
        this.addChild(entryFee);

        var circle = new cc.Sprite(spriteFrameCache.getSpriteFrame("whiteRound.png"));
        circle.setColor(cc.color(59, 95, 116));
        circle.setPosition(cc.p(250 * scaleFactor, 170 * scaleFactor));
        this.addChild(circle);

        var orText = new cc.LabelTTF("OR", "RobotoRegular", 12 * 2 * scaleFactor);
        orText.setScale(0.5);
        orText.setColor(cc.color(255, 255, 255));
        orText.setAnchorPoint(0.5, 0.5);
        orText.setPosition(circle.width / 2, circle.height / 2);
        orText.setScale(0.5);
        circle.addChild(orText, 1);

        this.radioTwo = new RadioButton(this, "RadioTwo");
        this.radioTwo.setScale(0.98);
        this.radioTwo.setSelected(false);
        this.radioTwo.setPosition(cc.p(295 * scaleFactor, 170 * scaleFactor));
        this.addChild(this.radioTwo);

        var ticket = new cc.LabelTTF("Ticket", "RobotoRegular", 15 * 2 * scaleFactor);
        ticket.setScale(0.5);
        ticket.setColor(cc.color(0, 0, 0));
        ticket.setAnchorPoint(0, 0.5);
        ticket.setPosition(315 * scaleFactor, 170 * scaleFactor);
        this.addChild(ticket);

        var line1 = new cc.DrawNode();
        line1.drawSegment(cc.p(0, 140 * scaleFactor), cc.p(500 * scaleFactor, 140 * scaleFactor), 0.5, cc.color(210, 210, 210));
        this.addChild(line1);

        var line2 = new cc.DrawNode();
        line2.drawSegment(cc.p(0, 200 * scaleFactor), cc.p(500 * scaleFactor, 200 * scaleFactor), 0.5, cc.color(210, 210, 210));
        this.addChild(line2);

        this.loaderIcon.setPosition(cc.p(this.width / 2, this.height - 210 * scaleFactor));
        this.timeTF.setPosition(cc.p(this.width / 2, this.height - 210 * scaleFactor));

        cc.eventManager.addListener(this.popupMouseListener.clone(), this.radioOne);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.radioTwo);

        cc.eventManager.addListener(this.popupTouchListener.clone(), this.radioOne);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.radioTwo);
    },
    radioBtnListener: function (sender) {
        this._selectedRadioBtn = sender;
        if (!this.radioTwo._isRadioSelected && !this.radioOne._isRadioSelected) {
            sender.setSelected(true);
        } else {
            this.radioOne.setSelected(!this.radioOne._isRadioSelected);
            this.radioTwo.setSelected(!this.radioTwo._isRadioSelected);
        }

        if (sender._name == "RadioTwo" && sender._isRadioSelected)
            this._gameRoom._tournamentGamePopup._entryFee = -1;
    },
    onTouchEndedCallBack: function (touch, event) {
        this._super(touch, event);
        var target = event.getCurrentTarget();
        switch (target._name) {
            case "RadioOne":
                target.click();
                break;
            case "RadioTwo":
                target.click();
                break;
        }
    }
});

var AddCashReBuyPopUp = Popup.extend({
    _name: "AddCashReBuyPopUp",
    _txt: null,
    ctor: function (context) {
        this._super(new cc.Size(500 * scaleFactor, 180 * scaleFactor), context, true);
        this.headerLabel.setString("Add Cash");

        var message = new cc.LabelTTF("Oops! Looks like you do not have sufficient cash in your account\n  to join this game. Would you like to add cash to your account?", "RobotoRegular", 15 * 2 * scaleFactor);
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        message.setScale(0.5);
        message.setColor(cc.color(49, 49, 49));
        message.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(message, 10);

        this.addCash = new CreateBtn("addCash", "addCashOver", "addCashButton");
        this.addCash.setPosition(cc.p(this.width / 2 - GameConstants.popButtonMargin - 5, 40 * scaleFactor));
        this.addChild(this.addCash, 5);

        this.showNo = new CreateBtn("noBtn", "noBtnOver", "showNo");
        this.showNo.setPosition(this.width / 2 + GameConstants.popButtonMargin + 5, 40 * scaleFactor);
        this.addChild(this.showNo, 5);

        if ('mouse' in cc.sys.capabilities) {
            this.initMouseListener();
        }
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), this.addCash);
        cc.eventManager.addListener(this.popupMouseListener.clone(), this.showNo);

        cc.eventManager.addListener(this.popupTouchListener.clone(), this.addCash);
        cc.eventManager.addListener(this.popupTouchListener.clone(), this.showNo);

        return true;
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        var targetName = target.getName();
        switch (target._name) {
            case "addCashButton":
                window.parent.postMessage("openCashPopup", "*");
                break;
            case "showNo":
                this.removeFromParent(true);
                break;
        }
    }
});
var LevelTimeOverPopup = Popup.extend({
    _name: "LevelTimeOverPopup",
    _txt: null,
    ctor: function (context, str) {
        this._super(new cc.Size(550 * scaleFactor, 230 * scaleFactor), context);
        this._txt = str;
        this.headerLabel.setString("Attention Players of Table No. 123456");

        var message = new cc.LabelTTF(str, "RobotoRegular", 15 * 2 * scaleFactor);
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        message.setScale(0.5);
        message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        message.setColor(cc.color(49, 49, 49));
        message.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(message, 10);

        var okButton = new CreateBtn("OkBtn", "OkBtnOver", "OkButton");
        okButton.setPosition(cc.p(this.width / 2, 40 * scaleFactor));
        okButton.setName("OkButton");
        this.addChild(okButton, 2);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), okButton);
        cc.eventManager.addListener(this.popupTouchListener.clone(), okButton);
        return true;
    },
    onTouchEndedCallBack: function (touch, event) {
        this.removeFromParent(true);
    },
    setHeader: function (str) {
        this.headerLabel.setString(str);
    }
});
var InsufficientTicketPopUp = Popup.extend({
    _name: "InsufficientTicketPopUp",
    _txt: null,
    ctor: function (context) {
        this._super(new cc.Size(500 * scaleFactor, 180 * scaleFactor), context, true);
        this.headerLabel.setString("Eliminated");

        var message = new cc.LabelTTF("You do not have valid ticket in your account to\njoin this tournament.", "RobotoRegular", 15 * 2 * scaleFactor);
        message.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        message.setScale(0.5);
        message._setBoundingWidth(this.width * 2 - 10 * scaleFactor);
        message.setColor(cc.color(49, 49, 49));
        message.setPosition(cc.p(this.width / 2, this.height / 2));
        this.addChild(message, 10);

        var okButton = new CreateBtn("OkBtn", "OkBtnOver", "OkButton");
        okButton.setPosition(cc.p(this.width / 2, 40 * scaleFactor));
        okButton.setName("OkButton");
        this.addChild(okButton, 2);

        var closeButton = new CreateBtn("ResultClose", "ResultClose", "closeButton");
        closeButton.setPosition(cc.p(this.width, this.height));
        closeButton.setName("closeButton");
        this.addChild(closeButton, 2);

        if ('mouse' in cc.sys.capabilities)
            this.initMouseListener();
        this.initTouchListener();

        cc.eventManager.addListener(this.popupMouseListener.clone(), okButton);
        cc.eventManager.addListener(this.popupTouchListener.clone(), okButton);

        cc.eventManager.addListener(this.popupMouseListener.clone(), closeButton);
        cc.eventManager.addListener(this.popupTouchListener.clone(), closeButton);
        return true;
    },
    onTouchEndedCallBack: function (touch, event) {
        var target = event.getCurrentTarget();
        var targetName = target.getName();
        switch (target._name) {
            case "OkButton":
                this.removeFromParent(true);
                break;
            case "closeButton":
                this.removeFromParent(true);
                break;
        }
    }
});

var TournamentWinner = cc.Layer.extend({
    _gameRoom: null,
    ctor: function (txt) {
        this._super();

        var glow = new cc.Sprite(res.winner.winnerGlow);
        glow.setPosition(cc.p(this.width / 2 + 1 * scaleFactor, this.height / 2));
        glow.setOpacity(130);
        glow.setScale(1.1, 1);
        this.addChild(glow);

        var winnerIcon = new cc.Sprite(res.winner.winnerLogo);
        winnerIcon.setPosition(cc.p(500 * scaleFactor, 480 * scaleFactor));
        this.addChild(winnerIcon, 5);

        var TournamentTextField = cc.Scale9Sprite.extend({
            _gameRoom: null,
            ctor: function (str) {

                this._super(res.winner.winnerField, cc.rect(0, 0, 100 * scaleFactor, 100 * scaleFactor));
                this.setCapInsets(cc.rect(32 * scaleFactor, 32 * scaleFactor, 32 * scaleFactor, 32 * scaleFactor));
                this.setContentSize(cc.size(390 * scaleFactor, 290 * scaleFactor));
                this.setPosition(cc.p(cc.winSize.width / 2, cc.winSize.height / 2));

                var winnerName = new cc.LabelTTF(str, "RupeeFordian", 22 * 2 * scaleFactor);
                winnerName._setBoundingWidth(this.width * 2 - 100 * scaleFactor);
                winnerName.setScale(0.5);
                winnerName.setPosition(cc.p(this.width / 2, this.height / 2));
                winnerName.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
                this.addChild(winnerName, 1);

            }
        });

        this.textField = new TournamentTextField(txt);
        this.addChild(this.textField, 2);
    }
});